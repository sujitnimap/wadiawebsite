﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OperationStatus
{
    [Serializable]
    public class COperationStatus
    {
        public string Message;
        public bool Success;
        public Exception Exception;
        public int InsertedRowId;
        public COperationStatus() { }

        public COperationStatus(bool bSuccess, string strMessage, Exception ex)
            : this(bSuccess, strMessage, ex, int.MinValue)
        {
        }

        public COperationStatus(bool bSuccess, string strMessage, Exception ex, int iInsertedRowId)
        {
            Success = bSuccess;
            Message = strMessage;
            Exception = ex;
            InsertedRowId = iInsertedRowId;
        }
    }
}
