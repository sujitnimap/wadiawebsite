﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using OperationStatus;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CSponsorARunForMarathonDB
    {
        public abstract IList<sponsorARunForMarathon> GetSponsorARunForMarathonInfo(int sponsorARunForMarathonID);

        public abstract IList<sponsorARunForMarathon> GetSponsorARunForMarathonInfos();

        public COperationStatus InsertSponsorARunForMarathonDetail(sponsorARunForMarathon objsponsorARunForMarathon)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.sponsorARunForMarathons.Add(objsponsorARunForMarathon);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Sponsor A Run For Marathon inserted Successfully", null, Convert.ToInt32(objsponsorARunForMarathon.SponsorARunForMarathonID));
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;

                //return new COperationStatus(false, ex.Message, ex);
            }
            //catch (Exception ex)
            //{
            //    return new COperationStatus(false, ex.Message, ex);
            //}
            finally
            {

            }
        }
    }
}
