﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CCareerDB
    {

        public abstract IList<career> GetCareerData(string flag);

        public COperationStatus InsertCareer(career Objcareer)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.careers.Add(Objcareer);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Your resume submitted successfully", null, Convert.ToInt32(Objcareer.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdateCareer(career Objcareer)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var careermod = datacontext.careers.Where(p => p.id == Objcareer.id).SingleOrDefault();
                    {
                        careermod.name = Objcareer.name;
                        careermod.email = Objcareer.email;
                        careermod.education = Objcareer.education;
                        careermod.address = Objcareer.address;
                        careermod.mobile = Objcareer.mobile;
                        careermod.resumefilename = Objcareer.resumefilename;
                        careermod.modifydate = DateTime.Now;

                        //datacontext.ourevents.Attach(Objourevent);
                        //datacontext.ObjectStateManager.ChangeObjectState(Objourevent, EntityState.Modified);
                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Career updated successfully", null, Convert.ToInt32(Objcareer.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }


        public COperationStatus Deletecareer(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deletecareer = datacontext.careers.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deletecareer.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "User details deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }

    }
}
