﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CArtgallaryDB
    {
        public abstract DataTable GetArtgallaryData(int id);
        public abstract DataTable GetArtgallaryData(string flag, string type);

        public abstract DataTable GetArtgallarygridData(string flag);
        public abstract DataTable GetArtgallarygridDatabycategory(string flag, string id);

        public COperationStatus InsertArtgallary(artgallary Objartgallary)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.artgallaries.Add(Objartgallary);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Gallary insertd Successfully", null, Convert.ToInt32(Objartgallary.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdateArtgallary(artgallary Objartartgallary)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var artgallarymod = datacontext.artgallaries.Where(p => p.id == Objartartgallary.id).SingleOrDefault();
                    {
                        if (Objartartgallary.thumbnail == "n/a")
                        {
                            artgallarymod.imagenm = Objartartgallary.imagenm;
                            artgallarymod.albumname = Objartartgallary.albumname;
                            artgallarymod.categorynm = Objartartgallary.categorynm;
                            artgallarymod.subcategorynm = Objartartgallary.subcategorynm;
                            artgallarymod.modifydate = DateTime.Now;
                            artgallarymod.cratedate = Objartartgallary.cratedate;
                            artgallarymod.description = Objartartgallary.description;
                        }
                        else
                        {
                            artgallarymod.imagenm = Objartartgallary.imagenm;
                            artgallarymod.albumname = Objartartgallary.albumname;
                            artgallarymod.categorynm = Objartartgallary.categorynm;
                            artgallarymod.subcategorynm = Objartartgallary.subcategorynm;
                            artgallarymod.thumbnail = Objartartgallary.thumbnail;
                            artgallarymod.modifydate = DateTime.Now;
                            artgallarymod.cratedate = Objartartgallary.cratedate;
                            artgallarymod.description = Objartartgallary.description;
                        }

                        //datacontext.ourevents.Attach(Objourevent);
                        //datacontext.ObjectStateManager.ChangeObjectState(Objourevent, EntityState.Modified);
                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Gallary Updated Successfully", null, Convert.ToInt32(Objartartgallary.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }


        public COperationStatus DeleteArtgallary(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deleteartgallary = datacontext.artgallaries.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deleteartgallary.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Gallary Deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }
    }
}
