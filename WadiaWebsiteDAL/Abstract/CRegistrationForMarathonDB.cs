﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using OperationStatus;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CRegistrationForMarathonDB
    {
        public abstract IList<registrationForMarathon> GetRegistrationForMarathonInfo(int registrationForMarathonID);

        public abstract IList<registrationForMarathon> GetRegistrationForMarathonInfos();

        public COperationStatus InsertRegistrationForMarathonDetail(registrationForMarathon objRegistrationForMarathon)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {

                    ObjDataContext.registrationForMarathons.Add(objRegistrationForMarathon);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Registration Marathon inserted Successfully", null, Convert.ToInt32(objRegistrationForMarathon.RegistrationForMarathonID));
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;

                //return new COperationStatus(false, ex.Message, ex);
            }
            //catch (Exception ex)
            //{
            //    return new COperationStatus(false, ex.Message, ex);
            //}
            finally
            {

            }
        }
    }
}
