﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CPressmediaDB
    {


        public abstract IList<pressmedia> GetMediaData(string flag);
        public abstract IList<pressmedia> GetMediaDatabyid(int mediaid);

        public COperationStatus InsertPressmedia(pressmedia Objpressmedia)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.pressmedias.Add(Objpressmedia);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Press-Media insertd Successfully", null, Convert.ToInt32(Objpressmedia.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdatePressmedia(pressmedia Objpressmedia)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var Pressmediamod = datacontext.pressmedias.Where(p => p.id == Objpressmedia.id).SingleOrDefault();
                    {

                        Pressmediamod.headline = Objpressmedia.headline;
                        Pressmediamod.link = Objpressmedia.link;
                        Pressmediamod.date = Objpressmedia.date;
                        Pressmediamod.source = Objpressmedia.source;

                        //datacontext.ourevents.Attach(Objourevent);
                        //datacontext.ObjectStateManager.ChangeObjectState(Objourevent, EntityState.Modified);
                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Press-Media Updated Successfully", null, Convert.ToInt32(Objpressmedia.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }


        public COperationStatus DeletePressmedia(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deletepressmedia = datacontext.pressmedias.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deletepressmedia.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Press-Media Deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }
    }
}
