﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CAdminUserDB
    {
        public abstract IList<adminuser> fn_getAdminUserList();
        public abstract adminuser fn_getAdminUserByID(int iAdminUserID);
        public abstract adminuser fn_checkAdminLogin(string userName, string password);
        public abstract adminuser fn_checkAdminPassword(string password, int iAdminID);

        //  WadiaDBEntities objDataContext = new WadiaDBEntities();
        
        public COperationStatus fn_addAdminUser(adminuser objAdminUser)
        {
            string strPassword = EncryptionHelper.Encrypt(objAdminUser.adminuser_password);

            try
            {
                using (WadiaDBEntities objDataContext = new WadiaDBEntities())
                {
                    objDataContext.adminusers.Add(objAdminUser);
                    objDataContext.SaveChanges();
                    return new COperationStatus(true, "AdminUser Added Successfully", null, objAdminUser.adminuser_id);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {
            }

        }

        public COperationStatus fn_updateAdminUser(adminuser objAdminUser)
        {
            try
            {
                using (WadiaDBEntities objDataContext = new WadiaDBEntities())
                {
                    adminuser adminUser = objDataContext.adminusers.Where(db => db.adminuser_id.Equals(objAdminUser.adminuser_id)).FirstOrDefault();
                    adminUser.adminuser_password = EncryptionHelper.Encrypt(objAdminUser.adminuser_password);
                    adminUser.adminuser_username = objAdminUser.adminuser_username;

                    objDataContext.SaveChanges();
                    return new COperationStatus(true, "AdminUser Updated Successfully", null, objAdminUser.adminuser_id);
                }

            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {
            }

        }


        public COperationStatus fn_updatePassWord(adminuser objAdminUser)
        {
            try
            {
                using (WadiaDBEntities objDataContext = new WadiaDBEntities())
                {
                    adminuser adminUser = objDataContext.adminusers.Where(db => db.adminuser_id.Equals(objAdminUser.adminuser_id)).FirstOrDefault();
                    adminUser.adminuser_password = EncryptionHelper.Encrypt(objAdminUser.adminuser_password);


                    objDataContext.SaveChanges();
                    return new COperationStatus(true, "AdminUser Updated Successfully", null, objAdminUser.adminuser_id);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {
            }

        }

        public COperationStatus fn_deleteAdminUser(adminuser objAdminUser)
        {
            try
            {
                using (WadiaDBEntities objDataContext = new WadiaDBEntities())
                {
                    objAdminUser = objDataContext.adminusers.Where(db => db.adminuser_id.Equals(objAdminUser.adminuser_id)).FirstOrDefault();
                    objDataContext.adminusers.Remove(objAdminUser);
                    objDataContext.SaveChanges();
                    return new COperationStatus(true, "AdminUser Deleted Successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {
            }

        }
    }
}
