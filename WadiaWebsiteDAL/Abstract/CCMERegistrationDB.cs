﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OperationStatus;
using System.Data.Entity.Validation;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CCMERegistrationDB
    {
        public COperationStatus InsertCMERegisteredDetail(CMERegistration objCMERegistration)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.CMERegistrations.Add(objCMERegistration);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "CME Registration inserted Successfully", null, Convert.ToInt32(objCMERegistration.CMEId));
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;

                // return new COperationStatus(false, ex.Message, ex);
            }
            //catch (Exception ex)
            //{
            //    return new COperationStatus(false, ex.Message, ex);
            //}
            finally
            {

            }
        }

        public COperationStatus InsertReproductiveInfertilityClinicDetail(Reproductive_Infertility_Clinic_Detail objReproductiveInfertilityClinic)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.Reproductive_Infertility_Clinic_Detail.Add(objReproductiveInfertilityClinic);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Reproductive and Infertility clinic detail inserted Successfully", null, Convert.ToInt32(objReproductiveInfertilityClinic.RICId));
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;

                // return new COperationStatus(false, ex.Message, ex);
            }
            //catch (Exception ex)
            //{
            //    return new COperationStatus(false, ex.Message, ex);
            //}
            finally
            {

            }
        }

    }
}
