﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CCasestudyDB
    {

        public abstract IList<casestudy> GetCasestudyData(string flag);
        public abstract IList<casestudy> GetCasestudyDatabyid(int id);
        
        public COperationStatus InsertCasestudy(casestudy Objcasestudy)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.casestudies.Add(Objcasestudy);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Casestudy submitted successfully", null, Convert.ToInt32(Objcasestudy.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdateCasestudy(casestudy Objcasestudy)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var casestudymod = datacontext.casestudies.Where(p => p.id == Objcasestudy.id).SingleOrDefault();
                    {
                        if (Objcasestudy.filepath == "#")
                        {
                            casestudymod.title = Objcasestudy.title;
                            casestudymod.description = Objcasestudy.description;
                            casestudymod.modifydate = DateTime.Now;
                        }
                        else
                        {
                            casestudymod.title = Objcasestudy.title;
                            casestudymod.description = Objcasestudy.description;
                            casestudymod.filepath = Objcasestudy.filepath;
                            casestudymod.isfileactive = Objcasestudy.isfileactive;
                            casestudymod.modifydate = DateTime.Now;
                        }

                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Casestudy updated successfully", null, Convert.ToInt32(Objcasestudy.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }


        public COperationStatus DeleteCasestudy(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deletecasestudy = datacontext.casestudies.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deletecasestudy.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Casestudy deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }

    }
}
