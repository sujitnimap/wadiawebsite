﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
   public abstract class CVideoDB
    {

       public abstract IList<videogallary> GetgallaryvideoData(int catid);
       public abstract DataTable GetVideogridDatabycategory(string flag, string id);

       public COperationStatus Insertgallaryvideo(gallaryvideo Objgallaryvideo)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.gallaryvideos.Add(Objgallaryvideo);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Video added successfully", null, Convert.ToInt32(Objgallaryvideo.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }


       public COperationStatus Deletegallaryvideo(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deletegallaryvideo = datacontext.gallaryvideos.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deletegallaryvideo.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Video deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }


    }
}
