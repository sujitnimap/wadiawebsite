﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OperationStatus;
using System.Data.Entity.Validation;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CAlumniEducationDB
    {
        public COperationStatus InsertAlumniEducation(AlumniEducation objAlumniEducation)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.AlumniEducations.Add(objAlumniEducation);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Alumni Education inserted Successfully", null, Convert.ToInt32(objAlumniEducation.AlEduId));
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;

                // return new COperationStatus(false, ex.Message, ex);
            }
            //catch (Exception ex)
            //{
            //    return new COperationStatus(false, ex.Message, ex);
            //}
            finally
            {

            }
        }
    }
}
