﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CCMERegistrationMasterDB
    {
        public abstract IList<CMERegistrationMaster> GetCMERegistrationMasterData(string flag);
        public abstract IList<CMERegistrationMaster> GetCMERegistrationMasterDatabyid(int id);
        public abstract IList<CMERegistrationMaster> GetCMERegistrationMasterDataByFeesCategory(string flag, string Cat);

        public COperationStatus InsertCMERegistrationMaster(CMERegistrationMaster ObjCMERegistrationMaster)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.CMERegistrationMasters.Add(ObjCMERegistrationMaster);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "CME Registration Master added successfully", null, Convert.ToInt32(ObjCMERegistrationMaster.Id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdateCMERegistrationMaster(CMERegistrationMaster ObjCMERegistrationMaster)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var cMERegistrationMastermod = datacontext.CMERegistrationMasters.Where(p => p.Id == ObjCMERegistrationMaster.Id).SingleOrDefault();
                    {
                        cMERegistrationMastermod.RegFeesCategory = ObjCMERegistrationMaster.RegFeesCategory;
                        cMERegistrationMastermod.LastRegDate = ObjCMERegistrationMaster.LastRegDate;
                        cMERegistrationMastermod.ConsultantFees = ObjCMERegistrationMaster.ConsultantFees;
                        cMERegistrationMastermod.PgStudentFees = ObjCMERegistrationMaster.PgStudentFees;
                        cMERegistrationMastermod.IsActive = ObjCMERegistrationMaster.IsActive;
                        cMERegistrationMastermod.Flag = ObjCMERegistrationMaster.Flag;
                        cMERegistrationMastermod.LastModify = DateTime.Now;                        
                        datacontext.SaveChanges();
                    }
                    return new COperationStatus(true, "CME Registration Master updated successfully", null, Convert.ToInt32(ObjCMERegistrationMaster.Id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }


        public COperationStatus DeleteCMERegistrationMaster(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deleteCMERegistration = datacontext.CMERegistrationMasters.Where(p => p.Id == Id).SingleOrDefault();
                    {
                        deleteCMERegistration.IsActive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "CME Registration Master deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }

    }
}
