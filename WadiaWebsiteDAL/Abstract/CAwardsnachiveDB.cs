﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
   public abstract class CAwardsnachiveDB
    {

       public abstract IList<awardnachievement> GetAwardData(string flag);


        public COperationStatus Insertawardnachievement(awardnachievement Objawardnachievement)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.awardnachievements.Add(Objawardnachievement);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Award added successfully", null, Convert.ToInt32(Objawardnachievement.id));
                }
            }
            catch (DbEntityValidationException ex)
            {                
                //contsinfo.Message = ex.Message.ToString();
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);
                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                return new COperationStatus(false, fullErrorMessage, ex);
            }
            //catch (Exception ex)
            //{
            //    return new COperationStatus(false, ex.Message, ex);
            //}
            finally
            {

            }
        }

        public COperationStatus Updateawardnachievement(awardnachievement Objawardnachievement)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var awardnachievementmod = datacontext.awardnachievements.Where(p => p.id == Objawardnachievement.id).SingleOrDefault();
                    {
                        awardnachievementmod.description = Objawardnachievement.description;
                        awardnachievementmod.year = Objawardnachievement.year;
                        awardnachievementmod.doctornm = Objawardnachievement.doctornm;
                        awardnachievementmod.categorynm = Objawardnachievement.categorynm;
                        awardnachievementmod.categoryid = Objawardnachievement.categoryid;
                        awardnachievementmod.modifydate = DateTime.Now;

                        //datacontext.ourevents.Attach(Objourevent);
                        //datacontext.ObjectStateManager.ChangeObjectState(Objourevent, EntityState.Modified);
                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Award updated successfully", null, Convert.ToInt32(Objawardnachievement.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }


        public COperationStatus Deleteawards(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deleteaward = datacontext.awardnachievements.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deleteaward.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Award deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }



    }
}
