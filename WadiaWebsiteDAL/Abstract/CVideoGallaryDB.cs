﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CVideoGallaryDB
    {

        public abstract DataTable GetVideogallaryData(int id);

        public abstract DataTable GetVideogallaryData(string flag);

        public abstract DataTable GetVideogallaryData(string flag, string type);

        public COperationStatus InsertVideogallary(videogallary Objvideogallary)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.videogallaries.Add(Objvideogallary);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Video insertd Successfully", null, Convert.ToInt32(Objvideogallary.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdateVideogallary(videogallary Objartvideogallary)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var videogallarymod = datacontext.videogallaries.Where(p => p.id == Objartvideogallary.id).SingleOrDefault();
                    {
                        videogallarymod.categoryid = Objartvideogallary.categoryid;
                        videogallarymod.name = Objartvideogallary.name;
                        videogallarymod.categorynm = Objartvideogallary.categorynm;
                        videogallarymod.videolink = Objartvideogallary.videolink;
                        videogallarymod.modifieddate = DateTime.Now;

                        //datacontext.ourevents.Attach(Objourevent);
                        //datacontext.ObjectStateManager.ChangeObjectState(Objourevent, EntityState.Modified);
                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Video Updated Successfully", null, Convert.ToInt32(Objartvideogallary.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }


        public COperationStatus DeleteVideogallary(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deletevideogallaries = datacontext.videogallaries.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deletevideogallaries.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Video Deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }

    }
}
