﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CEventDB
    {
        public abstract IList<ourevent> GetEventData(string flag);

        public abstract IList<ourevent> GetEventlistDatabyEventId(int eventid);

        public abstract DataTable GetEventDatabyEventId(int eventid);

        public abstract IList<subscribe> GetSubscribeData();

        public COperationStatus InsertEvent(ourevent Objourevent)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.ourevents.Add(Objourevent);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Event inserted successfully", null, Convert.ToInt32(Objourevent.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdateEvent(ourevent Objourevent)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var articlemod = datacontext.ourevents.Where(p => p.id == Objourevent.id).SingleOrDefault();
                    {
                        articlemod.eventname = Objourevent.eventname;
                        articlemod.startdate = Objourevent.startdate;
                        articlemod.enddate = Objourevent.enddate;
                        articlemod.description = Objourevent.description;
                        articlemod.starttime = Objourevent.starttime;
                        articlemod.endtime = Objourevent.endtime;
                        articlemod.location = Objourevent.location;
                        articlemod.modifydate = DateTime.Now;

                        //datacontext.ourevents.Attach(Objourevent);
                        //datacontext.ObjectStateManager.ChangeObjectState(Objourevent, EntityState.Modified);
                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Event updated successfully", null, Convert.ToInt32(Objourevent.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }


        public COperationStatus DeleteEvent(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deleteEvent = datacontext.ourevents.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deleteEvent.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Event deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }


        public COperationStatus InsertSubcribe(subscribe Objsubscribe)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.subscribes.Add(Objsubscribe);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Thanks for subscription..!!", null, Convert.ToInt32(Objsubscribe.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }


    }
}
