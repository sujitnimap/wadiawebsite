﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using OperationStatus;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CPaymentDetailsDB
    {
        public abstract IList<paymentDetail> GetPaymentDetailInfo(int paymentDetailID);

        public abstract IList<paymentDetail> GetPaymentDetails();


        public abstract IList<Reproductive_Infertility_Clinic_Detail> GetReproductiveInfertilityPaymentDetailsInfo(int paymentDetailID);
        public abstract IList<Reproductive_Infertility_Clinic_Detail> GetReproductiveInfertilityPaymentDetails();

        public COperationStatus InsertPaymentDetail(paymentDetail ObjPaymentDetails)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {

                    ObjDataContext.paymentDetails.Add(ObjPaymentDetails);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Payment details inserted Successfully", null, Convert.ToInt32(ObjPaymentDetails.PaymentDetailID));
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;

                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdateReproductiveInfertilityPaymentDetail(int _RICID, string _transactionId, string _paymentStatus)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    var _paymentDetailsdata = ObjDataContext.Reproductive_Infertility_Clinic_Detail.Where(db => db.RICId == _RICID).FirstOrDefault();

                    _paymentDetailsdata.PaymentStatus = _paymentStatus;
                    _paymentDetailsdata.TransactionId = _transactionId;
                    _paymentDetailsdata.ModifiedDate = DateTime.Now;

                    ObjDataContext.SaveChanges();

                    return new COperationStatus(true, "Payment updated Successfully", null, Convert.ToInt32(_paymentDetailsdata.RICId));
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;

                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }
    }
}
