﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CTenderDB
    {
        public abstract IList<tender> GetTenderData(string flag);
        public abstract IList<tender> GetUpcomingTenderData(string flag);
        public abstract IList<tender> GetPassTenderData(string flag);

        public abstract DataTable GetUcomingTenderData(string flag);
        public abstract DataTable GetpsTenderData(string flag);

        public abstract IList<tender> GetTenderDatabyid(int tendeerid);

        public COperationStatus InsertTender(tender Objtender)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.tenders.Add(Objtender);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Tender insertd Successfully", null, Convert.ToInt32(Objtender.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdateTender(tender Objarttender)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var tendermod = datacontext.tenders.Where(p => p.id == Objarttender.id).SingleOrDefault();
                    {
                        if (Objarttender.downloadlink == "n/a")
                        {
                            tendermod.name = Objarttender.name;
                            tendermod.description = Objarttender.description;
                            tendermod.modifydate = DateTime.Now;
                            tendermod.openingdate = Objarttender.openingdate;
                            tendermod.expirydate = Objarttender.expirydate;
                        }
                        else
                        {
                            tendermod.name = Objarttender.name;
                            tendermod.description = Objarttender.description;
                            tendermod.downloadlink = Objarttender.downloadlink;
                            tendermod.modifydate = DateTime.Now;
                            tendermod.openingdate = Objarttender.openingdate;
                            tendermod.expirydate = Objarttender.expirydate;
                        }
                        //datacontext.ourevents.Attach(Objourevent);
                        //datacontext.ObjectStateManager.ChangeObjectState(Objourevent, EntityState.Modified);
                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Tender Updated Successfully", null, Convert.ToInt32(Objarttender.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }


        public COperationStatus DeleteTender(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deletetender = datacontext.tenders.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deletetender.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Tender Deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }

    }
}
