﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CCMEDB
    {
        public abstract IList<cme> GetcmeData(string flag);
        public abstract IList<cme> GetcmeDatabyid(int id);

        public COperationStatus InsertCME(cme ObjCme)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.cmes.Add(ObjCme);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "CME added successfully", null, Convert.ToInt32(ObjCme.Id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdateCME(cme ObjCme)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var cMEmod = datacontext.cmes.Where(p => p.Id == ObjCme.Id).SingleOrDefault();
                    {
                        cMEmod.RegFeesCategory = ObjCme.RegFeesCategory;
                        cMEmod.LastRegDate = ObjCme.LastRegDate;
                        cMEmod.RegCategory = ObjCme.RegCategory;
                        cMEmod.Fees = ObjCme.Fees;
                        cMEmod.IsActive = ObjCme.IsActive;
                        cMEmod.Flag = ObjCme.Flag;
                        cMEmod.LastModify = DateTime.Now;
                        datacontext.SaveChanges();
                    }
                    return new COperationStatus(true, "CME updated successfully", null, Convert.ToInt32(ObjCme.Id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }

        public COperationStatus DeleteCME(int Id)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deleteCME = datacontext.cmes.Where(p => p.Id == Id).SingleOrDefault();
                    {
                        deleteCME.IsActive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "CME deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }
    }
}
