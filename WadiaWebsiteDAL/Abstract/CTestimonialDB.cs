﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CTestimonialDB
    {
        public abstract IList<testimonial> GetTestimonialData(string flag);

        public COperationStatus InsertTestimonial(testimonial Objtestimonial)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.testimonials.Add(Objtestimonial);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Testimonial inserted Successfully", null, Convert.ToInt32(Objtestimonial.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdateTestimonial(testimonial Objtestimonial)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var testimonialmod = datacontext.testimonials.Where(p => p.id == Objtestimonial.id).SingleOrDefault();
                    {
                        if (Objtestimonial.imageurl == "n/a")
                        {
                            testimonialmod.testimonialnm = Objtestimonial.testimonialnm;
                            testimonialmod.testimonialdescrp = Objtestimonial.testimonialdescrp;
                            testimonialmod.designation = Objtestimonial.designation;
                            testimonialmod.createby = Objtestimonial.createby;
                            testimonialmod.createdate = Objtestimonial.createdate;
                            testimonialmod.modifydate = DateTime.Now;
                            testimonialmod.categorynm = Objtestimonial.categorynm;
                            testimonialmod.categoryid = Objtestimonial.categoryid;
                        }
                        else
                        {
                            testimonialmod.testimonialnm = Objtestimonial.testimonialnm;
                            testimonialmod.testimonialdescrp = Objtestimonial.testimonialdescrp;
                            testimonialmod.designation = Objtestimonial.designation;
                            testimonialmod.createby = Objtestimonial.createby;
                            testimonialmod.createdate = Objtestimonial.createdate;
                            testimonialmod.modifydate = DateTime.Now;
                            testimonialmod.imageurl = Objtestimonial.imageurl;
                            testimonialmod.categorynm = Objtestimonial.categorynm;
                            testimonialmod.categoryid = Objtestimonial.categoryid;
                        }

                        //datacontext.ourevents.Attach(Objourevent);
                        //datacontext.ObjectStateManager.ChangeObjectState(Objourevent, EntityState.Modified);
                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Testimonial Updated Successfully", null, Convert.ToInt32(Objtestimonial.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }


        public COperationStatus DeleteTestimonial(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deleteTestimonial = datacontext.testimonials.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deleteTestimonial.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Testimonial Deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }

    }
}
