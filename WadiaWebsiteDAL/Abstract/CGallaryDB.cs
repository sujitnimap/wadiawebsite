﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CGallaryDB
    {

        public abstract IList<gallaryimg> GetgallaryimgData(int catid);

        public COperationStatus Insertgallaryimg(gallaryimg Objgallaryimg)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.gallaryimgs.Add(Objgallaryimg);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Image added successfully", null, Convert.ToInt32(Objgallaryimg.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }


        public COperationStatus Deletegallaryimg(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deletegallaryimg = datacontext.gallaryimgs.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deletegallaryimg.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Image deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }

    }
}
