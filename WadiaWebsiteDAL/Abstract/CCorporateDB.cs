﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CCorporateDB
    {

        public abstract IList<corporategiving> GetCorporateData(string flag);

        public COperationStatus Insertcorporate(corporategiving Objcorporate)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.corporategivings.Add(Objcorporate);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Corporate added successfully", null, Convert.ToInt32(Objcorporate.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus Updatecorporate(corporategiving Objcorporate)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var corporatemod = datacontext.corporategivings.Where(p => p.id == Objcorporate.id).SingleOrDefault();
                    {
                        if (Objcorporate.logoimg == "n/a")
                        {
                            corporatemod.description = Objcorporate.description;
                            corporatemod.modifydate = DateTime.Now;
                        }
                        else
                        {
                            corporatemod.logoimg = Objcorporate.logoimg;
                            corporatemod.description = Objcorporate.description;
                            corporatemod.modifydate = DateTime.Now;
                        }
                        //datacontext.ourevents.Attach(Objourevent);
                        //datacontext.ObjectStateManager.ChangeObjectState(Objourevent, EntityState.Modified);
                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Corporate updated successfully", null, Convert.ToInt32(Objcorporate.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }


        public COperationStatus Deletecorporate(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deletecorporate = datacontext.corporategivings.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deletecorporate.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Corporate deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }


    }
}
