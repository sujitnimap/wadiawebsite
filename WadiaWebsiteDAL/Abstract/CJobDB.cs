﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CJobDB
    {

        public abstract IList<job> GetJobData(string flag);

        public COperationStatus InsertJob(job Objjob)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.jobs.Add(Objjob);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Job insertd Successfully", null, Convert.ToInt32(Objjob.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus UpdateJob(job Objjob)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var jobmod = datacontext.jobs.Where(p => p.id == Objjob.id).SingleOrDefault();
                    {
                        jobmod.title = Objjob.title;
                        jobmod.description = Objjob.description;
                        jobmod.modifydate = DateTime.Now;

                        //datacontext.ourevents.Attach(Objourevent);
                        //datacontext.ObjectStateManager.ChangeObjectState(Objourevent, EntityState.Modified);
                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Job Updated Successfully", null, Convert.ToInt32(Objjob.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }


        public COperationStatus DeleteJob(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deletejob = datacontext.jobs.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deletejob.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Job Deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }



    }
}
