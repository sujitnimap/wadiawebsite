﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WadiaWebsiteDAL.Abstract
{
    public abstract class CCategoryDB
    {

        public abstract IList<category> GetCategoryData(string flag, string type);
        public abstract IList<category> GetCategoryDatabyid(int id);

        public COperationStatus Insertcategory(category Objcategory)
        {
            try
            {
                using (WadiaDBEntities ObjDataContext = new WadiaDBEntities())
                {
                    ObjDataContext.categories.Add(Objcategory);
                    ObjDataContext.SaveChanges();
                    return new COperationStatus(true, "Catgory added successfully", null, Convert.ToInt32(Objcategory.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
            finally
            {

            }
        }

        public COperationStatus Updatecategory(category Objcategory)
        {
            try
            {
                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var categorymod = datacontext.categories.Where(p => p.id == Objcategory.id).SingleOrDefault();
                    {
                        if (Objcategory.thumbnail == "n/a")
                        {
                            categorymod.categorynm = Objcategory.categorynm;
                            categorymod.description = Objcategory.description;
                            categorymod.createdate = Objcategory.createdate;
                            categorymod.modifydate = DateTime.Now;
                            categorymod.type = Objcategory.type;
                        }
                        else
                        {
                            categorymod.categorynm = Objcategory.categorynm;
                            categorymod.description = Objcategory.description;
                            categorymod.createdate = Objcategory.createdate;
                            categorymod.modifydate = DateTime.Now;
                            categorymod.type = Objcategory.type;
                            categorymod.thumbnail = Objcategory.thumbnail;
                        }
                        //datacontext.ourevents.Attach(Objourevent);
                        //datacontext.ObjectStateManager.ChangeObjectState(Objourevent, EntityState.Modified);
                        datacontext.SaveChanges();
                    }

                    return new COperationStatus(true, "Catgory updated successfully", null, Convert.ToInt32(Objcategory.id));
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }

            finally
            {

            }
        }


        public COperationStatus Deletecategory(int Id)
        {
            try
            {

                using (WadiaDBEntities datacontext = new WadiaDBEntities())
                {
                    var deletecategory = datacontext.categories.Where(p => p.id == Id).SingleOrDefault();
                    {
                        deletecategory.isactive = false;
                    }
                    datacontext.SaveChanges();
                    return new COperationStatus(true, "Catgory deleted successfully", null);
                }
            }
            catch (Exception ex)
            {
                return new COperationStatus(false, ex.Message, ex);
            }
        }

    }
}
