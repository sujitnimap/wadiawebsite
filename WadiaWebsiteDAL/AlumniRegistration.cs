//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WadiaWebsiteDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class AlumniRegistration
    {
        public int AlumniId { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string PermanentAdd { get; set; }
        public string Mobile { get; set; }
        public string TelNo { get; set; }
        public string Email { get; set; }
        public string ProfAddress { get; set; }
        public string ProfTelNo { get; set; }
        public Nullable<int> TotalAmount { get; set; }
        public System.DateTime LastModify { get; set; }
        public string RegistrationCategory { get; set; }
    }
}
