//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WadiaWebsiteDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CMERegistrationMaster
    {
        public int Id { get; set; }
        public string RegFeesCategory { get; set; }
        public Nullable<System.DateTime> LastRegDate { get; set; }
        public string ConsultantFees { get; set; }
        public string PgStudentFees { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Flag { get; set; }
        public Nullable<System.DateTime> LastModify { get; set; }
    }
}
