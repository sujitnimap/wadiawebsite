﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Reflection;
using System.ComponentModel;
using System.Net.Mail;
using System.Configuration;


public class CollectionHelper
{

    private CollectionHelper()
    {
    }

    public static DataTable ConvertTo<T>(IList<T> list)
    {
        DataTable table = CreateTable<T>();
        Type entityType = typeof(T);
        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);
        if (list != null)
            foreach (T item in list)
            {
                DataRow row = table.NewRow();

                foreach (PropertyDescriptor prop in properties)
                {
                    if (prop.GetValue(item) != null)
                        row[prop.Name] = prop.GetValue(item);
                    else
                        row[prop.Name] = DBNull.Value;
                }

                table.Rows.Add(row);
            }

        return table;
    }

    public static IList<T> ConvertTo<T>(IList<DataRow> rows)
    {
        IList<T> list = null;

        if (rows != null)
        {
            list = new List<T>();

            foreach (DataRow row in rows)
            {
                T item = CreateItem<T>(row);
                list.Add(item);
            }
        }

        return list;
    }

    public static IList<T> ConvertTo<T>(DataTable table)
    {
        if (table == null)
        {
            return null;
        }

        List<DataRow> rows = new List<DataRow>();

        foreach (DataRow row in table.Rows)
        {
            rows.Add(row);
        }

        return ConvertTo<T>(rows);
    }

    public static T CreateItem<T>(DataRow row)
    {
        T obj = default(T);
        if (row != null)
        {
            obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in row.Table.Columns)
            {
                PropertyInfo prop = obj.GetType().GetProperty(column.ColumnName);
                try
                {
                    object value = row[column.ColumnName];
                    prop.SetValue(obj, value, null);
                }
                catch
                {
                    // You can log something here
                    throw;
                }
            }
        }

        return obj;
    }

    public static DataTable CreateTable<T>()
    {
        Type entityType = typeof(T);
        DataTable table = new DataTable(entityType.Name);
        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

        foreach (PropertyDescriptor prop in properties)
        {
            Type pt1 = prop.PropertyType;
            if (pt1.IsGenericType && pt1.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                pt1 = Nullable.GetUnderlyingType(pt1);
            }
            table.Columns.Add(prop.Name, pt1);
        }
        return table;
    }

    public static DataTable GetDataTable(IEnumerable eObject)
    {
        DataTable dt = new DataTable();
        foreach (object obj in eObject)
        {
            Type t = obj.GetType();
            PropertyInfo[] pis = t.GetProperties();

            if (dt.Columns.Count == 0)
            {
                foreach (PropertyInfo pi in pis)
                {
                    Type pt1 = pi.PropertyType;
                    if (pt1.IsGenericType && pt1.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        pt1 = Nullable.GetUnderlyingType(pt1);
                    }
                    dt.Columns.Add(pi.Name, pt1);
                }
            }
            DataRow dr = dt.NewRow();
            foreach (PropertyInfo pi in pis)
            {
                if (pi.GetValue(obj, null) == null & pi.PropertyType.Name == "Nullable`1")
                {
                    if (pi.PropertyType.FullName == "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                    {
                        pi.SetValue(obj, DateTime.Now, null);
                    }
                    else
                        if (pi.PropertyType.FullName == "System.Nullable`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                        {
                            pi.SetValue(obj, 0, null);
                        }
                }
                object value = pi.GetValue(obj, null);


                dr[pi.Name] = value;
            }
            dt.Rows.Add(dr);
        }
        return dt;
    }

    public static bool sendMail(string strTo, string strSubject, string strName, string usernm, string strmesssge, string mobileno)
    {
        try
        {
            strName = strName == "" ? "" : ("Dear Sir/Madam ,");
            StringBuilder mailbody = new StringBuilder();
            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();

            mailClient.Host = ConfigurationManager.AppSettings["SMTP_MAIL_SERVER"];
            mailClient.Port = int.Parse(ConfigurationManager.AppSettings["port"]);
            //network credentials
            string strMailUserName = ConfigurationManager.AppSettings["FROM_ADDR"];
            string strMailPassword = ConfigurationManager.AppSettings["FROM_ADDR_PASS"];
            string strWebSiteURL = ConfigurationManager.AppSettings["LinkTosite"];

            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = true;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = true;

            string strFromMail = ConfigurationManager.AppSettings["FROM_ADDR"];
            MailAddress fromAddress = new MailAddress(strFromMail, "Wadia (Administrator)");
            message.From = fromAddress;

            //to mail address
            message.To.Add(strTo);
            message.Subject = "Wadia : " + strSubject;

            mailbody.AppendFormat("<p>" + strName + "<br/><br/>" +
                                  "<p>User Name : " + usernm + "<br/>" +
                                  "<br/> Phone NO. : " + mobileno + "<br/></p>" +
                                  "<br/> Query : " + strmesssge + "<br/></p>");

            mailbody.AppendFormat("<br/><br/><span>Regards,<br /><a href='" + strWebSiteURL + "' >Admin</a></span></div></div>");

            message.Body = mailbody.ToString();
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;
            return true;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }


    public static bool MarathonsendMail(string strTo, string strSubject, string strName, string strmesssge)
    {
        try
        {

            strName = strName == "" ? "" : ("Dear Sir/Madam ,");
            StringBuilder mailbody = new StringBuilder();
            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();

            mailClient.Host = ConfigurationManager.AppSettings["SMTP_MAIL_SERVER"];
            mailClient.Port = int.Parse(ConfigurationManager.AppSettings["port"]);
            //network credentials
            string strMailUserName = ConfigurationManager.AppSettings["FROM_ADDR"];
            string strMailPassword = ConfigurationManager.AppSettings["FROM_ADDR_PASS"];
            string strWebSiteURL = ConfigurationManager.AppSettings["LinkTosite"];

            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = true;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = true;

            string strFromMail = ConfigurationManager.AppSettings["FROM_ADDR"];
            MailAddress fromAddress = new MailAddress(strFromMail, "Wadia (Administrator)");
            message.From = fromAddress;

            //to mail address
            message.To.Add(strTo);
            message.Subject = "Wadia : " + strSubject;

            mailbody.AppendFormat(strmesssge);
            //mailbody.AppendFormat("<br/><br/><span>Regards,<br /><a href='" + strWebSiteURL + "' >Admin</a></span></div></div>");

            message.Body = mailbody.ToString();

            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;
            return true;

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            throw ex;            
        }
    }

    public static bool AlumniSendMail(string strTo, string strSubject, string strName, string strmesssge)
    {
        try
        {

            strName = strName == "" ? "" : ("Dear Sir/Madam ,");
            StringBuilder mailbody = new StringBuilder();
            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();

            mailClient.Host = ConfigurationManager.AppSettings["SMTP_MAIL_SERVER"];
            mailClient.Port = int.Parse(ConfigurationManager.AppSettings["port"]);
            //network credentials
            string strMailUserName = ConfigurationManager.AppSettings["FROM_ADDR"];
            string strMailPassword = ConfigurationManager.AppSettings["FROM_ADDR_PASS"];
            string strWebSiteURL = ConfigurationManager.AppSettings["LinkTosite"];

            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = true;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = true;

            string strFromMail = ConfigurationManager.AppSettings["FROM_ADDR"];
            MailAddress fromAddress = new MailAddress(strFromMail, "Wadia (Administrator)");
            message.From = fromAddress;

            //to mail address
            message.To.Add(strTo);
            message.Subject = "Wadia : " + strSubject;

            mailbody.AppendFormat(strmesssge);
            //mailbody.AppendFormat("<br/><br/><span>Regards,<br /><a href='" + strWebSiteURL + "' >Admin</a></span></div></div>");

            message.Body = mailbody.ToString();

            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;
            return true;

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            throw ex;
        }
    }

}