﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CVideo : CVideoDB
    {

        private static CVideo cVideo = null;

        public static CVideo Instance()
        {
            if (cVideo == null)
            {
                cVideo = new CVideo();
            }
            return cVideo;
        }


        public override IList<videogallary> GetgallaryvideoData(int catid)
        {
            IList<videogallary> gallaryvideodata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    gallaryvideodata = dataContext.videogallaries.Where(db => db.isactive == true && db.categoryid == catid).OrderBy(db => db.createdate).ToList<videogallary>();
                    return gallaryvideodata;
                }

                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public override DataTable GetVideogridDatabycategory(string flag, string id)
        {

            DataTable dt = new DataTable();
            int catid = Convert.ToInt32(id);

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {

                    var query = from od in dataContext.videogallaries
                                join om in dataContext.categories on od.categoryid equals om.id
                                where od.isactive == true && od.flag == flag && od.categoryid == catid
                                select new
                                {
                                    om.categorynm,
                                    od.id,
                                    od.name,
                                };

                    dt = CollectionHelper.GetDataTable(query);

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


    }
}
