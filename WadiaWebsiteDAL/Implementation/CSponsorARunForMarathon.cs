﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
     public class CSponsorARunForMarathon : CSponsorARunForMarathonDB 
    {
         private static CSponsorARunForMarathon cSponsorARunForMarathon = null;

         public static CSponsorARunForMarathon Instance()
        {
            if (cSponsorARunForMarathon == null)
            {
                cSponsorARunForMarathon = new CSponsorARunForMarathon();
            }
            return cSponsorARunForMarathon;
        }

         public override IList<sponsorARunForMarathon> GetSponsorARunForMarathonInfo(int sponsorARunForMarathonID)
        {
            IList<sponsorARunForMarathon> sponsorARunForMarathondata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    sponsorARunForMarathondata = dataContext.sponsorARunForMarathons.Where(db => db.SponsorARunForMarathonID == sponsorARunForMarathonID).ToList<sponsorARunForMarathon>();
                    return sponsorARunForMarathondata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

         public override IList<sponsorARunForMarathon> GetSponsorARunForMarathonInfos()
         {
             IList<sponsorARunForMarathon> sponsorARunForMarathondata = null;

             try
             {
                 using (WadiaDBEntities dataContext = new WadiaDBEntities())
                 {
                     sponsorARunForMarathondata = dataContext.sponsorARunForMarathons.Where(db => db.SponsorARunForMarathonID > 27).ToList<sponsorARunForMarathon>();
                     return sponsorARunForMarathondata;
                 }
             }
             catch (Exception ex)
             {
                 throw ex;
             }
             finally
             {

             }
         }

     }

    
}
