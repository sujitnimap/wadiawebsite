﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OperationStatus;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{   
    public class COneDayAlRegistration : COneDayAlRegistrationDB
    {
        private static COneDayAlRegistration cOneDayAlRegistration = null;

        public static COneDayAlRegistration Instance()
        {
            if (cOneDayAlRegistration == null)
            {
                cOneDayAlRegistration = new COneDayAlRegistration();
            }
            return cOneDayAlRegistration;
        }

        public IList<OneDayAlRegistration> GetOneDayAlRegistration(int OneDayAlId)
        {
            IList<OneDayAlRegistration> oneDayAlRegistrationData = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    oneDayAlRegistrationData = dataContext.OneDayAlRegistrations.Where(db => db.OneDayAlId == OneDayAlId).ToList<OneDayAlRegistration>();
                    return oneDayAlRegistrationData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public IList<OneDayAlRegistration> GetOneDayAlRegistrationList()
        {
            IList<OneDayAlRegistration> oneDayAlRegistrationData = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    oneDayAlRegistrationData = dataContext.OneDayAlRegistrations.ToList<OneDayAlRegistration>();
                    return oneDayAlRegistrationData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

    }
}
