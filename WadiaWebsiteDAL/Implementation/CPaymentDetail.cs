﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CPaymentDetail : CPaymentDetailsDB
    {
        private static CPaymentDetail cPaymentDetail = null;

        public static CPaymentDetail Instance()
        {
            if (cPaymentDetail == null)
            {
                cPaymentDetail = new CPaymentDetail();
            }
            return cPaymentDetail;
        }

        public override IList<paymentDetail> GetPaymentDetailInfo(int paymentDetailID)
        {
            IList<paymentDetail> paymentDetailsdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    paymentDetailsdata = dataContext.paymentDetails.Where(db => db.ChildDonateOnlineID == paymentDetailID).ToList<paymentDetail>();
                    return paymentDetailsdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override IList<paymentDetail> GetPaymentDetails()
        {
            IList<paymentDetail> paymentDetailsdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    paymentDetailsdata = dataContext.paymentDetails.OrderByDescending((db => db.PaymentDetailID)).ToList<paymentDetail>();
                    return paymentDetailsdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }



        public override IList<Reproductive_Infertility_Clinic_Detail> GetReproductiveInfertilityPaymentDetailsInfo(int paymentDetailID)
        {
            IList<Reproductive_Infertility_Clinic_Detail> paymentDetailsdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    paymentDetailsdata = dataContext.Reproductive_Infertility_Clinic_Detail.Where(db => db.RICId == paymentDetailID).ToList<Reproductive_Infertility_Clinic_Detail>();
                    return paymentDetailsdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public override IList<Reproductive_Infertility_Clinic_Detail> GetReproductiveInfertilityPaymentDetails()
        {
            IList<Reproductive_Infertility_Clinic_Detail> paymentDetailsdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    paymentDetailsdata = dataContext.Reproductive_Infertility_Clinic_Detail.OrderByDescending((db => db.RICId)).ToList<Reproductive_Infertility_Clinic_Detail>();
                    return paymentDetailsdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }



}
