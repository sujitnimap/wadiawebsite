﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OperationStatus;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CRegisteredAdultForMarathon :CRegisteredAdultForMarathonDB 
    {
        private static CRegisteredAdultForMarathon cRegisteredAdultForMarathon = null;

        public static CRegisteredAdultForMarathon Instance()
        {
            if (cRegisteredAdultForMarathon == null)
            {
                cRegisteredAdultForMarathon = new CRegisteredAdultForMarathon();
            }
            return cRegisteredAdultForMarathon;
        }


        public IList<registeredAdultForMarathon> GetRegisteredAdultForMarathon(int registrationForMarathonID)
        {

            IList<registeredAdultForMarathon> registrationForMarathondata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    registrationForMarathondata = dataContext.registeredAdultForMarathons.Where(db => db.RegistrationForMarathonID == registrationForMarathonID).ToList<registeredAdultForMarathon>();
                    return registrationForMarathondata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

    }
}
