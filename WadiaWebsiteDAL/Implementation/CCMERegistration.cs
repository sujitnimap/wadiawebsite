﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OperationStatus;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CCMERegistration : CCMERegistrationDB
    {
        private static CCMERegistration cCMERegistration = null;

        public static CCMERegistration Instance()
        {
            if (cCMERegistration == null)
            {
                cCMERegistration = new CCMERegistration();
            }
            return cCMERegistration;
        }
        

        public IList<CMERegistration> GetCMERegistration(int cMEId)
        {
            IList<CMERegistration> cMERegistrationdata = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    cMERegistrationdata = dataContext.CMERegistrations.Where(db => db.CMEId == cMEId).ToList<CMERegistration>();
                    return cMERegistrationdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public IList<CMERegistration> GetCMERegistrationList(string FormCategory)
        {
            IList<CMERegistration> cMERegistrationdata = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    cMERegistrationdata = dataContext.CMERegistrations.Where(db => db.FromCategory == FormCategory).ToList<CMERegistration>();
                    return cMERegistrationdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public IList<Reproductive_Infertility_Clinic_Detail> GetReproductiveInfertilityClinic(int RICId)
        {
            IList<Reproductive_Infertility_Clinic_Detail> reproductiveInfertilityClinicdata = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    reproductiveInfertilityClinicdata = dataContext.Reproductive_Infertility_Clinic_Detail.Where(db => db.RICId == RICId).ToList<Reproductive_Infertility_Clinic_Detail>();
                    return reproductiveInfertilityClinicdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }
}
