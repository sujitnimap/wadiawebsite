﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CArtgallary : CArtgallaryDB
    {

        private static CArtgallary cArtgallary = null;

        public static CArtgallary Instance()
        {
            if (cArtgallary == null)
            {
                cArtgallary = new CArtgallary();
            }
            return cArtgallary;
        }


        public override DataTable GetArtgallarygridData(string flag)
        {

            DataTable dt = new DataTable();

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {

                    var query = from od in dataContext.artgallaries
                                join om in dataContext.categories on od.categorynm equals om.id
                                where od.isactive == true && od.flag== flag
                                select new
                                {
                                    om.categorynm,
                                    od.id,
                                    od.description,
                                    od.albumname,
                                    od.imagenm,
                                    od.subcategorynm,
                                    od.thumbnail,
                                    od.cratedate
                                };

                    dt = CollectionHelper.GetDataTable(query);

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override DataTable GetArtgallarygridDatabycategory(string flag,string id)
        {

            DataTable dt = new DataTable();
            int catid = Convert.ToInt32(id);

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {

                    var query = from od in dataContext.artgallaries
                                join om in dataContext.categories on od.categorynm equals om.id
                                where od.isactive == true && od.flag == flag && od.categorynm == catid orderby od.cratedate descending
                                select new
                                {
                                    om.categorynm,
                                    od.id,
                                    od.description,
                                    od.albumname,
                                    od.imagenm,
                                    od.subcategorynm,
                                    od.thumbnail
                                };

                    dt = CollectionHelper.GetDataTable(query);

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override DataTable GetArtgallaryData(int id)
        {
            DataTable dt;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {

                    var query = from od in dataContext.artgallaries
                                join om in dataContext.categories on od.categorynm equals om.id
                                where od.isactive == true && od.id == id
                                select new
                                {
                                    om.categorynm,
                                    catname = od.categorynm,
                                    od.id,
                                    od.description,
                                    od.albumname,
                                    od.imagenm,
                                    od.subcategorynm,
                                    od.thumbnail,
                                    od.cratedate
                                };
                    dt = CollectionHelper.GetDataTable(query);

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override DataTable GetArtgallaryData(string flag,string type)
        {
            DataTable dt;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {

                    var query = from od in dataContext.artgallaries
                                join om in dataContext.categories on od.categorynm equals om.id
                                where od.isactive == true && od.flag == flag && om.type == type
                                select new
                                {
                                    om.categorynm,
                                    catname = od.categorynm,
                                    od.id,
                                    od.description,
                                    od.albumname,
                                    od.imagenm,
                                    od.subcategorynm,
                                    od.thumbnail
                                };
                    dt = CollectionHelper.GetDataTable(query);

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }
}
