﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CVideoGallary : CVideoGallaryDB
    {

        private static CVideoGallary cVideoGallary = null;

        public static CVideoGallary Instance()
        {
            if (cVideoGallary == null)
            {
                cVideoGallary = new CVideoGallary();
            }
            return cVideoGallary;
        }


        public override DataTable GetVideogallaryData( string flag,string type)
        {

            DataTable dt = new DataTable();

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {

                    var query = from od in dataContext.videogallaries
                                join om in dataContext.categories on od.categoryid equals om.id
                                where od.isactive == true && od.flag == flag && om.type == type
                                select new
                                {                                    
                                    od.categoryid,
                                    om.categorynm,
                                    od.id,
                                    od.videolink,
                                    od.name
                                };

                    dt = CollectionHelper.GetDataTable(query);

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override DataTable GetVideogallaryData(int id)
        {
            DataTable dt;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {

                    var query = dataContext.gallaryvideos.Where(db => db.categoryid == id && db.isactive == true);

                    dt = CollectionHelper.GetDataTable(query);

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override DataTable GetVideogallaryData(string flag)
        {
            DataTable dt;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {

                    var query = from od in dataContext.videogallaries
                                join om in dataContext.categories on od.categoryid equals om.id
                                where od.isactive == true && od.flag == flag
                                select new
                                {
                                    od.categoryid,
                                    om.categorynm,
                                    od.id,
                                    od.videolink,
                                    od.name
                                };
                    dt = CollectionHelper.GetDataTable(query);

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


    }
}
