﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
   public class CCorporate : CCorporateDB
    {
       private static CCorporate cCorporate = null;

       public static CCorporate Instance()
        {
            if (cCorporate == null)
            {
                cCorporate = new CCorporate();
            }
            return cCorporate;
        }

       public override IList<corporategiving> GetCorporateData(string flag)
       {
           IList<corporategiving> corporatedata = null;

           try
           {
               using (WadiaDBEntities dataContext = new WadiaDBEntities())
               {
                   corporatedata = dataContext.corporategivings.Where(db => db.isactive == true && db.flag == flag).OrderBy(db => db.id).ToList<corporategiving>();
                   return corporatedata;
               }

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {

           }
       }

    }
}
