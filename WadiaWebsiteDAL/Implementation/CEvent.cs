﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CEvent : CEventDB
    {

        private static CEvent cEvent = null;

        public static CEvent Instance()
        {
            if (cEvent == null)
            {
                cEvent = new CEvent();
            }
            return cEvent;
        }

        public override IList<ourevent> GetEventData(string flag)
        {
            IList<ourevent> eventdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    eventdata = dataContext.ourevents.Where(db => db.isactive == true && db.flag == flag).OrderBy(db => db.createdate).ToList<ourevent>();
                    return eventdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override DataTable GetEventDatabyEventId(int eventid)
        {
            DataTable eventdata = new DataTable();

            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand("sp_GetEventDatabyEventId", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@eventid", eventid);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Open();
                    da.Fill(eventdata);
                    conn.Close();
                }
                return eventdata;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public override IList<ourevent> GetEventlistDatabyEventId(int eventid)
        {
            IList<ourevent> eventdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    eventdata = dataContext.ourevents.Where(db => db.isactive == true && db.id == eventid).OrderBy(db => db.createdate).ToList<ourevent>();
                    return eventdata;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }



        public override IList<subscribe> GetSubscribeData()
        {
            IList<subscribe> subscribedata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    subscribedata = dataContext.subscribes.OrderBy(db => db.createdate).ToList<subscribe>();
                    return subscribedata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }
}
