﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
   public  class CPressmedia : CPressmediaDB
    {

       private static CPressmedia cPressmedia = null;

       public static CPressmedia Instance()
        {
            if (cPressmedia == null)
            {
                cPressmedia = new CPressmedia();
            }
            return cPressmedia = new CPressmedia();
        }



       public override IList<pressmedia> GetMediaData(string flag)
       {

           IList<pressmedia> pressmediadata = null;

           try
           {
               using (WadiaDBEntities dataContext = new WadiaDBEntities())
               {
                   pressmediadata = dataContext.pressmedias.Where(db => db.isactive == true && db.flag == flag).OrderBy(db => db.createdate).ToList<pressmedia>();
                   return pressmediadata;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {

           }
       }

       public override IList<pressmedia> GetMediaDatabyid(int mediaid)
       {

           IList<pressmedia> pressmediadata = null;

           try
           {
               using (WadiaDBEntities dataContext = new WadiaDBEntities())
               {
                   pressmediadata = dataContext.pressmedias.Where(db => db.id == mediaid && db.isactive == true).OrderBy(db => db.createdate).ToList<pressmedia>();
                   return pressmediadata;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {

           }
       }



    }
}
