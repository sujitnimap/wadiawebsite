﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CGallary : CGallaryDB
    {

        private static CGallary cGallary = null;

        public static CGallary Instance()
        {
            if (cGallary == null)
            {
                cGallary = new CGallary();
            }
            return cGallary;
        }



        public override IList<gallaryimg> GetgallaryimgData( int catid)
        {
            IList<gallaryimg> gallaryimgimgdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    gallaryimgimgdata = dataContext.gallaryimgs.Where(db => db.isactive == true && db.categoryid == catid).OrderBy(db => db.createdate).ToList<gallaryimg>();
                    return gallaryimgimgdata;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


    }
}
