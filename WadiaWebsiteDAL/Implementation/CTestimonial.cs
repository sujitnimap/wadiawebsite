﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public  class CTestimonial : CTestimonialDB
    {
        private static CTestimonial cTestimonial = null;

        public static CTestimonial Instance()
        {
            if (cTestimonial == null)
            {
                cTestimonial = new CTestimonial();
            }
            return cTestimonial;
        }


        public override IList<testimonial> GetTestimonialData(string flag)
        {
            IList<testimonial> testimonialdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    testimonialdata = dataContext.testimonials.Where(db => db.isactive == true && db.flag == flag).OrderBy(db => db.createdate).ToList<testimonial>();
                    return testimonialdata;
                }

                //using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ToString()))
                //{
                //    SqlCommand cmd = new SqlCommand("sp_GetTestimonialData", conn);
                //    cmd.CommandType = CommandType.StoredProcedure;
                //    SqlDataAdapter da = new SqlDataAdapter(cmd);
                //    conn.Open();
                //    da.Fill(testimonialdata);
                //    conn.Close();
                //}

                //return testimonialdata;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


    }
}
