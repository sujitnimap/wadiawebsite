﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CCasestudy : CCasestudyDB
    {

        private static CCasestudy cCasestudy = null;

        public static CCasestudy Instance()
        {
            if (cCasestudy == null)
            {
                cCasestudy = new CCasestudy();
            }
            return cCasestudy;
        }


        public override IList<casestudy> GetCasestudyData(string flag)
        {

            IList<casestudy> casestudydata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    casestudydata = dataContext.casestudies.Where(db => db.isactive == true && db.flag == flag).OrderBy(db => db.createdate).ToList<casestudy>();
                    return casestudydata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override IList<casestudy> GetCasestudyDatabyid(int id)
        {

            IList<casestudy> casestudydata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    casestudydata = dataContext.casestudies.Where(db => db.isactive == true && db.id == id).OrderBy(db => db.createdate).ToList<casestudy>();
                    return casestudydata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }
}
