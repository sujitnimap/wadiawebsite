﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CChildDonateOnline : CChildDonateOnlineDB 
    {
        private static CChildDonateOnline cChildDonateOnline = null;

        public static CChildDonateOnline  Instance()
        {
            if (cChildDonateOnline == null)
            {
                cChildDonateOnline = new CChildDonateOnline ();
            }
            return cChildDonateOnline;
        }


        public override IList<childDonateOnline> GetOnlineDonationInfo(int donateOnlineID)
        {
            IList<childDonateOnline> childDonateOnlinedata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    childDonateOnlinedata = dataContext.childDonateOnlines.Where(db => db.Id == donateOnlineID).ToList<childDonateOnline>();
                    return childDonateOnlinedata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override IList<childDonateOnline> GetOnlineDonationInfos()
        {
            IList<childDonateOnline> childDonateOnlinedata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    childDonateOnlinedata = dataContext.childDonateOnlines.OrderByDescending(db => db.Id).ToList<childDonateOnline>();
                    return childDonateOnlinedata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

    }




}