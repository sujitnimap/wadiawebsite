﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CAwardsnachive : CAwardsnachiveDB
    {

        private static CAwardsnachive cAwardsnachive = null;

        public static CAwardsnachive Instance()
        {
            if (cAwardsnachive == null)
            {
                cAwardsnachive = new CAwardsnachive();
            }
            return cAwardsnachive;
        }


        public override IList<awardnachievement> GetAwardData(string flag)
        {
            IList<awardnachievement> awarddata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    awarddata = dataContext.awardnachievements.Where(db => db.isactive == true && db.flag == flag).OrderBy(db => db.id).ToList<awardnachievement>();
                    return awarddata;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {

            }
        }




    }
}
