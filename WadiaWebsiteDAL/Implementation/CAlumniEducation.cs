﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OperationStatus;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CAlumniEducation : CAlumniEducationDB
    {
        private static CAlumniEducation cAlumniEducation = null;

        public static CAlumniEducation Instance()
        {
            if (cAlumniEducation == null)
            {
                cAlumniEducation = new CAlumniEducation();
            }
            return cAlumniEducation;
        }

        public IList<AlumniEducation> GetAlumniEducation(int AlumniId)
        {
            IList<AlumniEducation> alumniEducationData = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    alumniEducationData = dataContext.AlumniEducations.Where(db => db.AlumniId == AlumniId).ToList<AlumniEducation>();
                    return alumniEducationData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public IList<AlumniEducation> GetAlumniEducationList()
        {
            IList<AlumniEducation> alumniEducationData = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    alumniEducationData = dataContext.AlumniEducations.ToList<AlumniEducation>();
                    return alumniEducationData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

    }
}
