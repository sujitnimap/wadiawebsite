﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CJob : CJobDB
    {

        private static CJob cJob = null;

        public static CJob Instance()
        {
            if (cJob == null)
            {
                cJob = new CJob();
            }
            return cJob;
        }


        public override IList<job> GetJobData(string flag)
        {
            IList<job> jobdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    jobdata = dataContext.jobs.Where(db => db.isactive == true && db.flag == flag).OrderByDescending(db => db.id).ToList<job>();
                    return jobdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

    }
}
