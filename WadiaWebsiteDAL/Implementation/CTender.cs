﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace WadiaWebsiteDAL.Implementation
{
    public class CTender : CTenderDB
    {

        private static CTender cTender = null;

        public static CTender Instance()
        {
            if (cTender == null)
            {
                cTender = new CTender();
            }
            return cTender;
        }

        public override IList<tender> GetTenderData(string flag)
        {

            IList<tender> tenderdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    tenderdata = dataContext.tenders.Where(db => db.isactive == true && db.flag == flag).OrderBy(db => db.createdate).ToList<tender>();
                    return tenderdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override IList<tender> GetUpcomingTenderData(string flag)
        {

            IList<tender> tenderdata = null; ;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    tenderdata = dataContext.tenders.Where(db => db.isactive == true && db.flag == flag && db.expirydate >= DateTime.Now).OrderBy(db => db.createdate).ToList<tender>();
                    return tenderdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public override DataTable GetUcomingTenderData(string flag)
        {

            DataTable tenderdata = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection( ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand("sp_Getupcometenderdata", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@flag", flag);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    da.Fill(tenderdata);


                    return tenderdata;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override DataTable GetpsTenderData(string flag)
        {

            DataTable tenderdata = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand("sp_Getpasstenderdata", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@flag", flag);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(tenderdata);


                    return tenderdata;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override IList<tender> GetPassTenderData(string flag)
        {

            IList<tender> tenderdata = null;

            //string date = DateTime.Now.ToString("yyyy-MM-dd");
            //DateTime d = Convert.ToDateTime(date);

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    tenderdata = dataContext.tenders.Where(db => db.isactive == true && db.flag == flag && db.expirydate < DateTime.Now).OrderBy(db => db.createdate).ToList<tender>();
                    return tenderdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override IList<tender> GetTenderDatabyid(int tendeerid)
        {

            IList<tender> tenderdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    tenderdata = dataContext.tenders.Where(db =>db.id==tendeerid && db.isactive == true).OrderBy(db => db.createdate).ToList<tender>();
                    return tenderdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


    }
}
