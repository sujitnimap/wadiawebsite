﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CRegisteredChildForMarathon : CRegisteredChildForMarathonDB 
    {
        private static CRegisteredChildForMarathon cRegisteredChildForMarathon = null;

        public static CRegisteredChildForMarathon Instance()
        {
            if (cRegisteredChildForMarathon == null)
            {
                cRegisteredChildForMarathon = new CRegisteredChildForMarathon();
            }
            return cRegisteredChildForMarathon;
        }


        public IList<registeredChildForMarathon> GetRegisteredChildForMarathon(int registrationForMarathonID)
        {

            IList<registeredChildForMarathon> registeredChildForMarathon = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    registeredChildForMarathon = dataContext.registeredChildForMarathons.Where(db => db.RegistrationForMarathonID == registrationForMarathonID).ToList<registeredChildForMarathon>();
                    return registeredChildForMarathon;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    }
}
