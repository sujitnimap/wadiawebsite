﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
   public class CCategory : CCategoryDB
    {

       private static CCategory cCategory = null;

       public static CCategory Instance()
        {
            if (cCategory == null)
            {
                cCategory = new CCategory();
            }
            return cCategory;
        }


       public override IList<category> GetCategoryData(string flag,string type)
       {
           IList<category> categorydata = null;

           try
           {
               using (WadiaDBEntities dataContext = new WadiaDBEntities())
               {
                   categorydata = dataContext.categories.Where(db => db.isactive == true && db.flag == flag && db.type == type).OrderBy(db => db.createdate).ToList<category>();
                   return categorydata;
               }

           }
           catch (Exception ex)
           {
               
               throw ex;
           }
           finally
           {

           }
       }

       public override IList<category> GetCategoryDatabyid(int id)
       {
           IList<category> categorydata = null;

           try
           {
               using (WadiaDBEntities dataContext = new WadiaDBEntities())
               {
                   categorydata = dataContext.categories.Where(db => db.isactive == true && db.id == id).OrderBy(db => db.categorynm).ToList<category>();
                   return categorydata;
               }

           }
           catch (Exception ex)
           {

               throw ex;
           }
           finally
           {

           }
       }

    }
}
