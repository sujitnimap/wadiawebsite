﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CCME : CCMEDB
    {
        private static CCME cCME = null;

        public static CCME Instance()
        {
            if (cCME == null)
            {
                cCME = new CCME();
            }
            return cCME;
        }

        public override IList<cme> GetcmeData(string flag)
        {
            IList<cme> cmeData = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    cmeData = dataContext.cmes.Where(db => db.IsActive == true && db.Flag == flag).OrderBy(db => db.Id).ToList<cme>();

                    return cmeData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public override IList<cme> GetcmeDatabyid(int id)
        {
            IList<cme> cCMEdata = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    cCMEdata = dataContext.cmes.Where(db => db.IsActive == true && db.Id == id).OrderBy(db => db.Id).ToList<cme>();
                    return cCMEdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

    }
}
