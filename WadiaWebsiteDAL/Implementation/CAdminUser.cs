﻿using WadiaWebsiteDAL.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WadiaWebsiteDAL.Abstract
{
    public class CAdminUser : CAdminUserDB
    {
        private static CAdminUser _objAdminUser = null;

        //   WadiaDBEntities objDataContext = new WadiaDBEntities();

        private CAdminUser()
        {
        }

        public static CAdminUser Instance()
        {
            if (_objAdminUser == null)
            {
                _objAdminUser = new CAdminUser();
            }
            return _objAdminUser;
        }

        public override IList<adminuser> fn_getAdminUserList()
        {
            IList<adminuser> objAdminUser = null;

            try
            {
                using (WadiaDBEntities objDataContext = new WadiaDBEntities())
                {
                    objAdminUser = objDataContext.adminusers.ToList<adminuser>();
                    return objAdminUser;
                }
            }
            catch (Exception ex)
            {
                return null; //new COperationStatus(false, ex.Message, ex);
            }

            finally
            {                
            }

        }

        public override adminuser fn_getAdminUserByID(int iAdminUserID)
        {
            adminuser objAdminUser = null;

            try
            {
                using (WadiaDBEntities objDataContext = new WadiaDBEntities())
                {
                    objAdminUser = objDataContext.adminusers.Where(db => db.adminuser_id.Equals(iAdminUserID)).FirstOrDefault();
                    return objAdminUser;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
            }

        }

        public override adminuser fn_checkAdminLogin(string userName, string password)
        {
            string strPassword = EncryptionHelper.Encrypt(password);
            adminuser objAdminUser = new adminuser();
            try
            {
                using (WadiaDBEntities objDataContext = new WadiaDBEntities())
                {
                    objAdminUser = objDataContext.adminusers.Where(db => db.adminuser_username.Equals(userName) && db.adminuser_password.Equals(strPassword)).FirstOrDefault();
                    return objAdminUser;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
            }

        }


        public override adminuser fn_checkAdminPassword(string password, int iAdminID)
        {
            string strPassword = EncryptionHelper.Encrypt(password);
            adminuser objAdminUser = null;

            try
            {
                using (WadiaDBEntities objDataContext = new WadiaDBEntities())
                {
                    objAdminUser = objDataContext.adminusers.Where(db => db.adminuser_password.Equals(strPassword) && db.adminuser_id.Equals(iAdminID)).FirstOrDefault();
                    return objAdminUser;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
            }

        }

    }
}
