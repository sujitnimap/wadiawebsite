﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CRegistrationForMarathon : CRegistrationForMarathonDB
    {
        private static CRegistrationForMarathon cRegistrationForMarathon = null;

        public static CRegistrationForMarathon Instance()
        {
            if (cRegistrationForMarathon == null)
            {
                cRegistrationForMarathon = new CRegistrationForMarathon();
            }
            return cRegistrationForMarathon;
        }

        public override IList<registrationForMarathon> GetRegistrationForMarathonInfo(int registrationForMarathonID)
        {
            IList<registrationForMarathon> registrationForMarathondata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    registrationForMarathondata = dataContext.registrationForMarathons.Where(db => db.RegistrationForMarathonID == registrationForMarathonID).ToList<registrationForMarathon >();
                    return registrationForMarathondata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override IList<registrationForMarathon> GetRegistrationForMarathonInfos()
        {
            IList<registrationForMarathon> registrationForMarathondata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    registrationForMarathondata = dataContext.registrationForMarathons.Where(db => db.RegistrationForMarathonID > 513).OrderByDescending(db => db.RegistrationForMarathonID).ToList();
                    return registrationForMarathondata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
    
    }

   
}
