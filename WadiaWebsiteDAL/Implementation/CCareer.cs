﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CCareer : CCareerDB
    {

        private static CCareer cCareer = null;

        public static CCareer Instance()
        {
            if (cCareer == null)
            {
                cCareer = new CCareer();
            }
            return cCareer;
        }


        public override IList<career> GetCareerData(string flag)
        {
            IList<career> careerdata = null;

            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    careerdata = dataContext.careers.Where(db => db.isactive == true && db.flag == flag).OrderBy(db => db.createdate).ToList<career>();
                    return careerdata;
                }

                //using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ToString()))
                //{
                //    SqlCommand cmd = new SqlCommand("sp_GetCareerData", conn);
                //    cmd.CommandType = CommandType.StoredProcedure;
                //    SqlDataAdapter da = new SqlDataAdapter(cmd);
                //    conn.Open();
                //    da.Fill(careerdata);
                //    conn.Close();
                //}

                //return careerdata;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


    }
}
