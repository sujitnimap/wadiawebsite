﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OperationStatus;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CAlumniRegistration : CAlumniRegistrationDB
    {
        private static CAlumniRegistration cAlumniRegistration = null;

        public static CAlumniRegistration Instance()
        {
            if (cAlumniRegistration == null)
            {
                cAlumniRegistration = new CAlumniRegistration();
            }
            return cAlumniRegistration;
        }

        public IList<AlumniRegistration> GetAlumniRegistration(int AlumniId)
        {
            IList<AlumniRegistration> alumniRegistrationData = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    alumniRegistrationData = dataContext.AlumniRegistrations.Where(db => db.AlumniId == AlumniId).ToList<AlumniRegistration>();
                    return alumniRegistrationData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public IList<AlumniRegistration> GetAlumniRegistrationList()
        {
            IList<AlumniRegistration> alumniRegistrationData = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    alumniRegistrationData = dataContext.AlumniRegistrations.ToList<AlumniRegistration>();
                    return alumniRegistrationData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

    }
}
