﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WadiaWebsiteDAL.Abstract;

namespace WadiaWebsiteDAL.Implementation
{
    public class CCMERegistrationMaster : CCMERegistrationMasterDB
    {
        private static CCMERegistrationMaster cCMERegistrationMaster = null;

        public static CCMERegistrationMaster Instance()
        {
            if (cCMERegistrationMaster == null)
            {
                cCMERegistrationMaster = new CCMERegistrationMaster();
            }
            return cCMERegistrationMaster;
        }

        public override IList<CMERegistrationMaster> GetCMERegistrationMasterData(string flag)
        {
            IList<CMERegistrationMaster> cmeRegistrationMasterdata = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    cmeRegistrationMasterdata = dataContext.CMERegistrationMasters.Where(db => db.IsActive == true && db.Flag == flag).OrderBy(db => db.Id).ToList<CMERegistrationMaster>();

                    return cmeRegistrationMasterdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public override IList<CMERegistrationMaster> GetCMERegistrationMasterDatabyid(int id)
        {
            IList<CMERegistrationMaster> cCMERegistrationMasterdata = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    cCMERegistrationMasterdata = dataContext.CMERegistrationMasters.Where(db => db.IsActive == true && db.Id == id).OrderBy(db => db.Id).ToList<CMERegistrationMaster>();

                    return cCMERegistrationMasterdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public override IList<CMERegistrationMaster> GetCMERegistrationMasterDataByFeesCategory(string flag, string Cat)
        {
            IList<CMERegistrationMaster> cmeRegistrationMasterdata = null;
            try
            {
                using (WadiaDBEntities dataContext = new WadiaDBEntities())
                {
                    cmeRegistrationMasterdata = dataContext.CMERegistrationMasters.Where(db => db.IsActive == true && db.Flag == flag && db.RegFeesCategory == Cat).OrderBy(db => db.Id).ToList<CMERegistrationMaster>();

                    return cmeRegistrationMasterdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

    }
}
