function getPageOffsetLeft() {
return arguments[0].offsetLeft + (arguments[0].offsetParent ? getPageOffsetLeft(arguments[0].offsetParent) : 0);
}

function getPageOffsetTop() {
return arguments[0].offsetTop + (arguments[0].offsetParent ? getPageOffsetTop(arguments[0].offsetParent) : 0);
}

function position(imgId,layerid,xdif,ydif) {
theImage = document.getElementById(imgId);
x = getPageOffsetLeft(theImage);
y = getPageOffsetTop(theImage);
//alert(x+" "+y);
document.getElementById(layerid).style.visibility="visible";
document.getElementById(layerid).style.left=x+xdif;
document.getElementById(layerid).style.top=y+ydif;

}

function hide(layerid){
document.getElementById(layerid).style.visibility="hidden";
document.getElementById(layerid).style.left=0;
document.getElementById(layerid).style.top=0;

}//function hide