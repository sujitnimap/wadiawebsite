﻿using OperationStatus;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web.UI;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class OneDayAlPayUResponce : System.Web.UI.Page
    {
        public string mihPayId = "";
        string mode = "";
        public string status = "";
        public string unMappedStatus = "";
        public String key = "";
        public String txnid = "";
        public string amount = "";
        public float discount = 0;
        float netAmountDebit = 0;
        public string productinfo = "";
        public string firstname = "";
        public string lastname = "";


        public string phone = "";
        public string email = "";
        string address1 = "";
        string address2 = "";
        string country = "";
        string city = "";
        DateTime addedDate = DateTime.Now;

        string state = "";
        string zipCode = "";

        public string udf1 = "";
        public string udf2 = "";
        public string udf3 = "";
        public string udf4 = "";
        public string udf5 = "";
        public string udf6 = "";
        public string udf7 = "";
        public string udf8 = "";
        public string udf9 = "";
        public string udf10 = "";
        public string salt = "";

        string paymentSource = "";
        string pgType = "";
        string bankRefNum = "";
        string bankCode = "";
        string field9 = "";

        string error = "";
        string errorMessage = "";

        string transactionNumber = "";
        public string hashC = "";
        public string responseHash = "";
        Boolean IsHashValid = false;

        string seprator = "|";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                try
                {
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    IEnumerator en = Request.Form.GetEnumerator();
                    while ((en.MoveNext()))
                    {
                        string xkey = Convert.ToString(en.Current);

                        string xval = Request.Form[xkey];
                        dict.Add(xkey, xval);
                    }                   

                    salt = ConfigurationManager.AppSettings["CSalt"];
                    //salt = "eCwWELxi"; //demo creds
                    status = dict["status"];
                    udf10 = dict["udf10"];
                    udf9 = dict["udf9"];
                    udf8 = dict["udf8"];
                    udf7 = dict["udf7"];
                    udf6 = dict["udf6"];
                    udf5 = dict["udf5"];
                    udf4 = dict["udf4"];
                    udf3 = dict["udf3"];
                    udf2 = dict["udf2"];
                    udf1 = dict["udf1"];
                    email = dict["email"];
                    phone = dict["phone"];
                    firstname = dict["firstname"];
                    productinfo = dict["productinfo"];
                    amount = dict["amount"];
                    txnid = dict["txnid"];
                    key = dict["key"];
                    transactionNumber = dict["txnid"];
                    mihPayId = dict["mihpayid"];

                    country = dict["country"];
                    city = dict["city"];
                    errorMessage = dict["error_Message"];
                    error = dict["error"];
                    mode = dict["mode"];

                    netAmountDebit = float.Parse(dict["net_amount_debit"]);
                    address1 = dict["address1"];
                    paymentSource = dict["payment_source"];
                    bankRefNum = dict["bank_ref_num"];
                    bankCode = dict["bankcode"];
                    pgType = dict["PG_TYPE"];

                    responseHash = dict["hash"];

                    String hashStr = salt + seprator + status + seprator + udf10 + seprator + udf9 + seprator + udf8 + seprator + udf7 + seprator + udf6 + seprator + udf5 + seprator + udf4 + seprator + udf3 + seprator + udf2 + seprator + udf1 + seprator + email + seprator + firstname + seprator + productinfo + seprator + amount + seprator + txnid + seprator + key;
                    hashC = CreateBase64SHA512Hash(hashStr, "ISO-8859-2").ToLower();

                    if (responseHash == hashC)
                    {
                        IsHashValid = true;
                    }
                    else
                    {
                        IsHashValid = false;
                    }

                    paymentDetail objpaymentDetail = new paymentDetail();
                    COperationStatus os = new COperationStatus();
                    objpaymentDetail.ChildDonateOnlineID = Convert.ToInt32(udf1);
                    objpaymentDetail.AddedDate = addedDate;
                    objpaymentDetail.ProdInfo = productinfo;
                    objpaymentDetail.MerchantKey = key;
                    objpaymentDetail.TransationID = txnid;
                    objpaymentDetail.MihPayId = mihPayId;
                    objpaymentDetail.IsHashValid = IsHashValid;

                    objpaymentDetail.FirstName = firstname;
                    objpaymentDetail.LastName = lastname;
                    objpaymentDetail.Amount = Convert.ToDouble(amount);
                    objpaymentDetail.Email = email;
                    objpaymentDetail.Phone = phone;
                    objpaymentDetail.hash = responseHash;
                    objpaymentDetail.Status = status;

                    objpaymentDetail.NetAmountDebit = netAmountDebit;
                    objpaymentDetail.Address1 = address1;
                    objpaymentDetail.PaymentSource = paymentSource;
                    objpaymentDetail.BankRefNum = bankRefNum;
                    objpaymentDetail.BankCode = bankCode;
                    objpaymentDetail.PGType = pgType;
                    objpaymentDetail.Mode = mode;
                    objpaymentDetail.UnMappedStatus = unMappedStatus;

                    objpaymentDetail.Country = "";
                    objpaymentDetail.City = city;
                    objpaymentDetail.State = state;
                    objpaymentDetail.ZipCode = zipCode;
                    objpaymentDetail.Address2 = address2;
                    objpaymentDetail.Error = error;
                    objpaymentDetail.ErrorMessage = errorMessage;

                    objpaymentDetail.UDF1 = udf1;
                    objpaymentDetail.UDF2 = udf2;
                    objpaymentDetail.UDF3 = udf3;
                    objpaymentDetail.UDF4 = udf4;
                    objpaymentDetail.UDF5 = udf5;
                    objpaymentDetail.UDF6 = udf6;
                    objpaymentDetail.UDF7 = udf7;
                    objpaymentDetail.UDF8 = udf8;
                    objpaymentDetail.UDF9 = udf9;
                    objpaymentDetail.UDF10 = udf10;

                    objpaymentDetail.Field1 = "";
                    objpaymentDetail.Field2 = "";
                    objpaymentDetail.Field3 = "";
                    objpaymentDetail.Field4 = "";
                    objpaymentDetail.Field5 = "";
                    objpaymentDetail.Field6 = "";
                    objpaymentDetail.Field7 = "";
                    objpaymentDetail.Field8 = "";
                    objpaymentDetail.Field9 = field9;

                    os = CPaymentDetail.Instance().InsertPaymentDetail(objpaymentDetail);
                    lblAddedSuccessfully.Text = os.Success.ToString();


                    if (IsHashValid && status == "success")
                    {
                        lblPaymentSuccessMessage.Text = "Transaction Successfull";

                        lblTransactionNumber.Text = transactionNumber;

                        #region BodyHeader
                        string body = "";                


                        StringBuilder strBody = new StringBuilder();

                        strBody.Append("<table width='900px' border='0' align='center' cellpadding='0' cellspacing='0'>");
                        strBody.Append("<tr>");
                        strBody.Append("<td>");
                        strBody.Append("<strong>Greetings " + firstname + ",</strong>");
                        strBody.Append("</td>");
                        strBody.Append("</tr>");

                        strBody.Append("<tr>");
                        strBody.Append("<td>");
                        strBody.Append("You Have been successfully registered for Wadia Alumni Event on <strong>14th Dec 2019</strong> at <strong>Dr. S M Merchant Auditorium</strong> and at the <strong>ITC Grand Central Banquet, Parel, Mumbai</strong>");
                        strBody.Append("</td>");
                        strBody.Append("</tr>");
                        strBody.Append("</table>");                       

                        body = strBody.ToString();
                        #endregion

                        CollectionHelper.AlumniSendMail(email, "Registration For One Day Alumni Event", "", body);

                        dataDiv.InnerHtml = body;

                        hdnPayID.Value = udf1; 
                    }
                    else
                    {
                        lblPaymentSuccessMessage.Text = "Fail";
                        lblTransactionNumber.Text = transactionNumber;
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.StackTrace + "<br/> " + ex.Message);
                }
            }
        }

        public static string CreateBase64SHA512Hash(string hashTarget, string encoding)
        {
            System.Security.Cryptography.SHA512 sha = new System.Security.Cryptography.SHA512CryptoServiceProvider();
            byte[] targetBytes = System.Text.Encoding.GetEncoding(encoding).GetBytes(hashTarget);
            byte[] hashBytes = sha.ComputeHash(targetBytes);

            StringBuilder sb = new StringBuilder(hashBytes.Length * 2);
            foreach (byte b in hashBytes)
            {
                sb.AppendFormat("{0:x2}", b);
            }
            return sb.ToString();
        }
    }
}