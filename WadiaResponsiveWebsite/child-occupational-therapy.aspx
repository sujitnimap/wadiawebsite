﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-occupational-therapy.aspx.cs" Inherits="WadiaResponsiveWebsite.child_occupational_therapy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2> Occupational therapy</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         

Occupational Therapy is the use of treatments to develop, recover, or maintain the daily living and work skills of people with a physical, mental or developmental condition. Occupational therapy interventions focus on adapting the environment, modifying the task, teaching the skill, and educating the client/family in order to increase participation in and performance of daily activities, particularly those that are meaningful to the client.
Patients are often referred for Occupational Therapy from the Paediatric Orthopaedic, Paediatric Medicine, Paediatric Neurology and Paediatric Surgery Departments. 


 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               

Occupational Therapists use various procedures such as:
                            <br /><br />
                <div style="padding-left:10px">

-	Neuromuscular Development
<br />-	Sensory Integrative
<br />-	Priomuscular techniques
    <br />
</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

         








		</div>




</asp:Content>
