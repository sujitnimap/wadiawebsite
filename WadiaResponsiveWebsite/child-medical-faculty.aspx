﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-medical-faculty.aspx.cs" Inherits="WadiaResponsiveWebsite.child_medical_faculty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="row block05">

        <div class="col-2-3">
            <div class="wrap-col">
                <div class="heading">
                    <h2>FACULTY</h2>
                </div>

                <br />

                <!-- ABOUT US CONTENT -->

                <div class="devider_20px"></div>
                <div style="line-height: 20px; text-align: center; width: 100%">


                    <table align="center" width="83%" cellspacing="0" cellpadding="0" style="border: 1px solid #666">
                        <tr>
                            <td colspan="4" height="40" style="border: 1px solid #666; padding: 5px; background-color: #1ACCFC; text-align: center"><b>FACULTY AVAILABILITY AT BAI JERBAI WADIA HOSPITAL FOR CHILDREN</b></td>                        
                        </tr>
                        <tr>
                            <td height="40" style="border: 1px solid #666; padding: 5px; background-color: #1ACCFC; text-align: center"><b>Sr. No.</b></td>
                            <td style="border: 1px solid #666; background-color: #1ACCFC; text-align: center; padding-top: 15PX; width: 148px; padding-left: 5px; padding-right: 5px; padding-bottom: 5px;"><b>Speciality</b> </td>
                            <td style="border: 1px solid #666; background-color: #1ACCFC; text-align: center; padding-top: 15PX; width: 163px; padding-left: 5px; padding-right: 5px; padding-bottom: 5px;"><b>Doctor Name </b></td>
                            <td style="border: 1px solid #666; padding: 5px; background-color: #1ACCFC; text-align: center"><b>Timing</b></td>
                        </tr>
                       
                        <tr>
                            <td rowspan="3" style="border: 1px solid #666; padding: 5px; text-align: center">1</td>
                            <td rowspan="3" style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Neonatology</td>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr Alpana Utture</td>
                            <td rowspan="3" style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>
                        </tr>
                        <tr>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: center">2</td>--%>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Neonatology</td>--%>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr Praful Shahbhag</td>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>--%>
                        </tr>
                         <tr>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: center">3</td>--%>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Neonatology</td>--%>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr Suma.S</td>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>--%>
                        </tr>


                        <tr>
                            <td rowspan="2" style="border: 1px solid #666; padding: 5px; text-align: center">2</td>
                            <td rowspan="2" style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Paediatric Intensive Care</td>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr Rekha Solomon</td>
                            <td rowspan="2" style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>
                        </tr>
                        <tr>
                           <%-- <td style="border: 1px solid #666; padding: 5px; text-align: center">2</td>
                            <td style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Paediatric Intensive Care</td>--%>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr Isha Bhagat</td>
                           <%-- <td style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>--%>
                        </tr>


                         <tr>
                            <td rowspan="2" style="border: 1px solid #666; padding: 5px; text-align: center">3</td>
                            <td rowspan="2" style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Paediatric Cardiology</td>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr Shreepal Jain</td>
                            <td rowspan="2" style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>
                        </tr>
                        <tr>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: center">2</td>
                            <td style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Dr Jayashree Mishra</td>--%>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr Jayashree Mishra</td>
                           <%-- <td style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>--%>
                        </tr>


                        <tr>
                            <td rowspan="3" style="border: 1px solid #666; padding: 5px; text-align: center">4</td>
                            <td rowspan="3" style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Paediatric Gastroenterology</td>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr  Ira Shah</td>
                            <td rowspan="3" style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>
                        </tr>
                        <tr>
                           <%-- <td style="border: 1px solid #666; padding: 5px; text-align: center">2</td>
                            <td style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Paediatric Gastroenterology</td>--%>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr Sanjay.B. Prabhu</td>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>--%>
                        </tr>
                        <tr>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: center">3</td>
                            <td style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Paediatric Gastroenterology</td>--%>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr Sagar J. Mehta</td>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>--%>
                        </tr>

                         <tr>
                            <td rowspan="2" style="border: 1px solid #666; padding: 5px; text-align: center">5</td>
                            <td rowspan="2" style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Paediatric Heamato-Oncology</td>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr Sangeeta Mudaliar</td>
                            <td rowspan="2" style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>
                        </tr>
                        <tr>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: center">2</td>
                            <td style="border: 1px solid #666; padding: 5px; text-align: left; width: 188px;">Paediatric Heamato-Oncology</td>--%>
                            <td style="border: 1px solid #666; padding: 5px; width: 123px; text-align: center">Dr Prashant R. Hiwarkar</td>
                            <%--<td style="border: 1px solid #666; padding: 5px; text-align: center">9am -5pm, all working days</td>--%>
                        </tr>


                    </table>
                    <div class="devider_20px"></div>





                </div>
                <!-- ABOUT US CONTENT -->





            </div>
        </div>



        <div class="col-1-3">
            <div class="wrap-col">

                <div class="box">
                    <div class="heading">
                        <h2>MEDICAL</h2>
                    </div>
                    <div class="content">
                        <div class="list">
                            <ul>


                                <li><a href="child-medical-overview.aspx">Overview  </a></li>
                                <li><a href="child-medical-directormsg.aspx">Directors message </a></li>
                                <li><a href="child-medical-courses.aspx">Courses available</a></li>
                                <li><a href="child-medical-faculty.aspx">Faculty </a></li>
                                <li><a href="child-contact-us.aspx">Contact Us  </a></li>


                                <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
									
                            </ul>
                        </div>
                    </div>
                </div>
                <%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
            </div>
        </div>



    </div>



</asp:Content>
