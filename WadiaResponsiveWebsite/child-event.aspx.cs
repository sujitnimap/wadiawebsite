﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class child_event : System.Web.UI.Page
    {

        DataTable eventdata;

        static string flag = "c";

        int ID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                ViewState["id"] = Convert.ToInt32(Request.QueryString["id"]);
                ID = Convert.ToInt32(ViewState["id"]);
            }


            if (!IsPostBack)
            {
                geteventdata();

            }
        }


        public void geteventdata()
        {
            eventdata = CEvent.Instance().GetEventDatabyEventId(ID);

            //var list = (from t in eventdata orderby t.createdate descending select t).Take(1);


            dlisteventdata.DataSource = eventdata;
            dlisteventdata.DataBind();

        }
    }
}