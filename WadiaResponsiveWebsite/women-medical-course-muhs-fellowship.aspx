﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-medical-course-muhs-fellowship.aspx.cs" Inherits="WadiaResponsiveWebsite.women_medical_course_muhs_fellowship" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <style>
        pdf-btn-text {
            color: #fff;
            text-decoration: none;
        }

        pdf-btn-text:hover,
        pdf-btn-text:active {
            color: #fff;
            text-decoration: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
        <div class="col-2-3">
            <div class="wrap-col">
                <div class="heading"><h2>MUHS Fellowship (Mandate)</h2></div>

                <div class="devider_20px"></div>

                <%--Accordion-Start--%>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            1 MUHS Fellowship in Fetal Medicine
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Fetal%20Medicine/Annexure%20A%20FC%20in%20Fetal%20Medicine.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Fetal%20Medicine/Annexure%20B%20FC%20in%20Fetal%20Medicine.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Fetal%20Medicine/Annexure%20C%20FC%20in%20Fetal%20Medicine.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Fetal%20Medicine/Annexure%20D%20FC%20in%20Fetal%20Medicine.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Fetal%20Medicine/Annexure%20E%20FC%20in%20Fetal%20Medicine.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Fetal%20Medicine/Annexure%20F%20FC%20in%20Fetal%20Medicine.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Fetal%20Medicine/Annexure%20G%20FC%20in%20Fetal%20Medicine.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Fetal%20Medicine/Annexure%20H%20FC%20in%20Fetal%20Medicine.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                            2 MUHS Fellowship in Minimal Access Surgery in Gynaecology
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology/Annexure%20A%20FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology/Annexure%20B%20FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology/Annexure%20C%20FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology/Annexure%20D%20FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology/Annexure%20E%20FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology/Annexure%20F%20FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology/Annexure%20G%20FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology/Annexure%20H%20FC%20in%20Minimal%20Access%20Surgery%20in%20Gynecology.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            3 MUHS Fellowship in Assisted Reproduction
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a class="pdf-btn-text" target="_blank" href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Assisted%20Reproduction/Annexure%20A%20FC%20in%20Assisted%20ReproductionTechniques.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a class="pdf-btn-text" target="_blank" href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Assisted%20Reproduction/Annexure%20B%20FC%20in%20Assisted%20ReproductionTechniques.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a class="pdf-btn-text" target="_blank" href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Assisted%20Reproduction/Annexure%20C%20FC%20in%20Assisted%20ReproductionTechniques.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a class="pdf-btn-text" target="_blank" href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Assisted%20Reproduction/Annexure%20D%20FC%20in%20Assisted%20ReproductionTechniques.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a class="pdf-btn-text" target="_blank" href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Assisted%20Reproduction/Annexure%20E%20FC%20in%20Assisted%20ReproductionTechniques.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a class="pdf-btn-text" target="_blank" href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Assisted%20Reproduction/Annexure%20F%20FC%20in%20Assisted%20ReproductionTechniques.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a class="pdf-btn-text" target="_blank" href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Assisted%20Reproduction/Annexure%20G%20FC%20in%20Assisted%20ReproductionTechniques.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a class="pdf-btn-text" target="_blank" href="LIC%20MUHS/nwmh%20-%20MUHS%20Fellowship%20(Mandate)/FC%20in%20Assisted%20Reproduction/Annexure%20H%20FC%20in%20Assisted%20ReproductionTechniques.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <%--Accordion-End--%>

            </div>
        </div>

        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>MEDICAL</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						 <li><a href="women-medical-overview.aspx">	Overview  </a></li>
                         <li><a href="women-medical-directormsg.aspx"> Directors message </a></li>
                         <li><a href="women-medical-courses.aspx"> 	Courses available</a></li>
                         <li><a href="women-contact-us.aspx">	Contact Us  </a></li>


                                     <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									<%--</li>--%>
									
								</ul>
							</div>
						</div>
					</div>
					


				</div>
			</div>

    </div>

    
</asp:Content>
