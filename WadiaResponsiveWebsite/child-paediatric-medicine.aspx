﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-medicine.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_medicine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>GENERAL PAEDIATRIC MEDICINE</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Outpatient Department</a></li>
             <li><a href="#view4">Inpatient  Department</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
                The Paediatric Medicine Department at BJWHC provides medical care for children from infancy to 18 years of age. 
                The medical team is experienced in diagnosing and treating childhood illnesses, chronic conditions, and medical emergencies. Working closely with families and the referring physicians, we strive to provide the most effective care for our patients. From health promotion to emergency care, the Department of Paediatric Medicine offers a wide range of non-surgical services for infants, children, teenagers and their parents.

 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
              The Paediatric OPD provides comprehensive, high-quality healthcare from birth to the age of 18. It sees over 100,000 children each year. Our dedicated staff includes highly qualified specialists and nurses who work towards ensuring that each child has a comfortable, warm and caring environment, along with empowering them as well as their families. 
The Department runs a General OPD everyday from Monday to Saturday between 1 PM and 3 PM.and Specialty OPD between 9 am and 1 pm.  The General OPD provides general health care to children with common ailments, and those requiring routine medical examinations. 

<br />
<br />
<b>Services Provided in general OPD :</b>
<br />
<br />
•	Complete physical examinations 

<br />
•	Vision and hearing screening 
<br />•	Immunizations 
<br />•	Growth and developmental assessment and referral of children with developmental delays 
<br />•	Nutritional evaluation 
<br />•	Parenting education
<br />•	Diagnosis and treatment of – all types of childhood ailments, including upper respiratory infections, <div style="padding-left:10px">ear infections, bronchiolitis, gastrointestinal infections, urinary tract infections and more. </div>
• On-going care of children with acute or chronic conditions, such as asthma, diabetes, genetic <div style="padding-left:10px">disorders, neurological conditions, behavioural issues and more. </div>

<br /></br>

<b>Services provided in specialty OPD :</b>
                <br />
<br />
On-going ambulatory care of children with - chronic conditions, such as asthma, diabetes, genetic disorders, neurological conditions, behavioural issues, 
                
                heart diseases etc. is provided in specialty OPDs 
<br /><br />

               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
               
            The Hospital has well equipped wards with facilities for routine and advance care of children with well-trained staff nurses round the clock. We offer a comprehensive array of health services to promote healthy growth and development as well as medical management of acute and chronic conditions. During the child’s stay at the hospital, a paediatric healthcare physician will visit the patient everyday led by a team of paediatric residents, subspecialists, nurses and social workers. The hospital also has Neonatal and Paediatric Intensive Care Units to cater to critically ill children.


           <br /><br />
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

    

		</div>
	




</asp:Content>
