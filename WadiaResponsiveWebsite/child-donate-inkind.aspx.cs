﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WadiaResponsiveWebsite
{
    public partial class child_donate_inkind : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void imgbtncontact_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
              
                    string strMailUserName = ConfigurationManager.AppSettings["SEND_TO_ADDR"];

                    CollectionHelper.sendMail(strMailUserName, "Child-donate-inkind", "Dear Sir/Madam ,", txtname.Text + "<br>Email-Id : " + txtemail.Text.Trim() + "<br>Pancard no. :" + txtpancardno.Text.Trim() + "<br>Address :" + txtadderess.Text.Trim(), txtmessage.Text, txtconatctno.Text);

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Thank you.');");
                    sb.Append("$('#editModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
                
                    txtname.Text = string.Empty;
                    txtemail.Text = string.Empty;
                    txtconatctno.Text = string.Empty;
                    txtmessage.Text = string.Empty;
                    txtpancardno.Text = string.Empty;
                    txtadderess.Text = string.Empty;
            }
            catch (Exception ex)
            {

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + ex.Message + "');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);

            }
        }
    }
}