﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AlumniPayURequest.aspx.cs" Inherits="WadiaResponsiveWebsite.AlumniPayURequest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form method="post" action="https://secure.payu.in/_payment" id="form2" name="form1">
        <%--https://test.payu.in/_payment--%>
    <script src="themes/jquery-1.9.0.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#form2").submit();
            });
        </script>

        <input type="hidden" name="firstname" value="<%=firstname%>" />
        <input type="hidden" name="lastname" value="<%=lastname%>" />
        <input type="hidden" name="phone" value="<%=phone%>" />
        <input type="hidden" name="key" value="<%=key%>" />
        <input type="hidden" name="txnid" value="<%=txnid%>" />
        <input type="hidden" name="amount" value="<%=amount%>" />
        <input type="hidden" name="productinfo" value="<%=productinfo%>" />
        <input type="hidden" name="email" value="<%=email%>" />
        <input type="hidden" name="surl" value="<%=acceptURL%>" />
        <input type="hidden" name="furl" value="<%=declineURL%>" />
        <input type="hidden" name="curl" value="<%=cancelURL%>" />

        <input type="hidden" name="udf1" value="<%=udf1%>" />
        <input type="hidden" name="udf2" value="<%=udf2%>" />
        <input type="hidden" name="udf3" value="<%=udf3%>" />
        <input type="hidden" name="udf4" value="<%=udf4%>" />
        <input type="hidden" name="udf5" value="<%=udf5%>" />

        <input type="hidden" name="hash" value="<%=hash%>" />
        <input type="submit" value="Submit" style="display: none;"/>

         <table>
			<tr>
				<td>
					<asp:Image ID="Image1" runat="server" ImageUrl="~/img/loading-thickbox.gif" />
				</td>
			</tr>
			<tr>
				<td style="border:1px solid #dddddd; height:25px; color:#646464">Please wait while we redirect you to payment gateway...
					<br />
					<span style="color:#ff0000">Do not close this window...</span>
				</td>
			</tr>
		</table>
    </form>
</body>
</html>
