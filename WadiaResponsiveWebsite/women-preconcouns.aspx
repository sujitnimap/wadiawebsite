﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-preconcouns.aspx.cs" Inherits="WadiaResponsiveWebsite.women_preconcouns" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Preconception and Premarital Counselling </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
Premarital counselling helps couples prepare for marriage. Our endeavour is to help build a strong, healthy relationship between the couple – giving them a better chance at a stable and fulfilling marriage. The therapy can also help identify weaknesses or differences that could later be amplified during the course of their marriage.
<br />
 <br />Women are encouraged to seek preconception counselling as well, which helps identify risk factors for any potential complications at the time of pregnancy.  At the hospital we can help women make adjustments to certain aspects of their life, which are under their control such as lifestyle, exercise, rest and stress reduction. 
<br />
 <br />
                <b>Our counselling focuses on the following: </b>
 <br />
 <br />
<div style="padding-left:25px">
       
-	Communication Skills
 <br />-	Conflict Resolution
 <br />-	Building Love and Respect 
 <br />-	Commitment
 <br />-	Anxiety and Stress Management 

</div>


 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>



		</div>



</asp:Content>
