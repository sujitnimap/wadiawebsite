﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-donate-online.aspx.cs" Inherits="WadiaResponsiveWebsite.child_donate_online" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        var specialKeys = new Array();

        specialKeys.push(8); //Backspace

        function IsNumeric(e) {

            var keyCode = e.which ? e.which : e.keyCode

            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);

            return ret;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-2-3">
            <div class="wrap-col">

                <div class="heading">
                    <h2>Donate Online </h2>
                </div>
                <br />
                <!-- ABOUT US CONTENT -->
                <div style="line-height: 20px">
                    As a hospital working primarily towards bridging the gaps and providing quality healthcare to every section of the society, we offer subsidized treatments to our patients without ever compromising on quality. To ensure we continue to do so, we rely on aid from you. Regardless of the size of your contribution, your support makes a huge difference to our work and mission. 
                    <br />
                    <br />
                    All donations made to Wadia Hospitals by individuals and organisations are acknowledged in the Annual report and are eligible for a 50% tax exemption under section 80G.
                    <br />
                    <br />
                    Donating online is quick and easy. Fill in the form below to send us your contribution:
                    <br />
                    <br />
                    <div style="border: 1px solid #ddd; padding: 10px 20px 20px 10px; border-radius: 4px;">

                        <div style="width: 99%; background-color: #999; height: 20px; color: #FFF; vertical-align: middle; padding: 8px 6px 10px 10px;">
                            General Online Donation Form 

                        </div>
                        <%--  <div class="devider_20px"></div>--%>

                        <div class="devider_20px"></div>
                        <div>
                            <div style="display: inline; padding-right: 35px">

                                <strong>Donation From</strong>
                            </div>

                            <div style="display: inline">
                                <asp:DropDownList ID="ddlDonationFrom" runat="server" OnSelectedIndexChanged="ddlDonationFrom_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text="Corporation" Selected="True" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Individual" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Others" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <%--<div style="display: inline">
                                Individual  &nbsp;&nbsp;<asp:CheckBox ID="chkIndividual" runat="server" />
                            </div>
                            <div style="display: inline">
                                Corporation  &nbsp;&nbsp;<asp:CheckBox ID="chkCorporation" runat="server" />
                            </div>
                            <div style="display: inline">
                                Others  &nbsp;&nbsp;<asp:CheckBox ID="chkDonationFromOthers" runat="server" />
                            </div>--%>
                        </div>

                        <asp:Panel ID="pnlNameOfOrganisation" runat="server">
                            <div class="devider_20px"></div>

                            <div style="width: 99%; background-color: #999; height: 20px; color: #FFF; vertical-align: middle; padding: 8px 6px 10px 10px;">
                                For donation by Company and others 
                            </div>

                            <div class="devider_20px"></div>

                            <asp:TextBox ID="txtNameOfOragnisation" CssClass="textboxcontact" runat="server" placeholder="Name Of Organisation*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name Of Organisation'"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reftxtNameOfOragnisation" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtNameOfOragnisation" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </asp:Panel>

                         <div class="devider_10px"></div>

                        <asp:TextBox ID="txtFirstNameOfContactPerson" CssClass="textboxcontact" runat="server" placeholder="First Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtFirstNameOfContactPerson" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtFirstNameOfContactPerson" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                         <div class="devider_10px"></div>
                        <asp:TextBox ID="txtLastNameOfContactPerson" CssClass="textboxcontact" runat="server" placeholder="Last Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtLastNameOfContactPerson" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtLastNameOfContactPerson" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                       <div class="devider_10px"></div>
                        <asp:TextBox ID="txtPostalAddress" CssClass="textboxcontact" runat="server" placeholder="Postal Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Postal Address'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtPostalAddress" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtPostalAddress" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                         <div class="devider_10px"></div>
                        <asp:TextBox ID="txtPostalCode" CssClass="textboxcontact" runat="server" placeholder="Postal Code*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Postal Code'"></asp:TextBox>
                        <div class="devider_10px"></div>

                        <asp:TextBox ID="txtCity" CssClass="textboxcontact" runat="server" placeholder="City*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'City' "></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtCity" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtCity" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <div style="height: 8px;"></div>

                       <asp:DropDownList CssClass="textboxcontact_list" ID="ddlCountrys" runat="server">
                            <asp:ListItem>Afghanistan</asp:ListItem>
                            <asp:ListItem>Aland Islands</asp:ListItem>
                            <asp:ListItem>Albania</asp:ListItem>
                            <asp:ListItem>Algeria</asp:ListItem>
                            <asp:ListItem>Andorra</asp:ListItem>
                            <asp:ListItem>Angola</asp:ListItem>
                            <asp:ListItem>Anguilla</asp:ListItem>
                            <asp:ListItem>Antartica</asp:ListItem>
                            <asp:ListItem>Antigua And Barbuda</asp:ListItem>
                            <asp:ListItem>Argentina</asp:ListItem>
                            <asp:ListItem>Armenia</asp:ListItem>
                            <asp:ListItem>Aruba</asp:ListItem>
                            <asp:ListItem>Ascension Island/St. Helena</asp:ListItem>
                            <asp:ListItem>Australia</asp:ListItem>
                            <asp:ListItem>Austria</asp:ListItem>
                            <asp:ListItem>Azerbaijan</asp:ListItem>
                            <asp:ListItem>Bahamas</asp:ListItem>
                            <asp:ListItem>Bahrain</asp:ListItem>
                            <asp:ListItem>Bangladesh</asp:ListItem>
                            <asp:ListItem>Barbados</asp:ListItem>
                            <asp:ListItem>Belarus</asp:ListItem>
                            <asp:ListItem>Belgium</asp:ListItem>
                            <asp:ListItem>Belize</asp:ListItem>
                            <asp:ListItem>Benin</asp:ListItem>
                            <asp:ListItem>Bermuda</asp:ListItem>
                            <asp:ListItem>Bhutan</asp:ListItem>
                            <asp:ListItem>Bolivia</asp:ListItem>
                            <asp:ListItem>Bosnia Herzegovina</asp:ListItem>
                            <asp:ListItem>Botswana</asp:ListItem>
                            <asp:ListItem>Bouvet Island</asp:ListItem>
                            <asp:ListItem>Brazil</asp:ListItem>
                            <asp:ListItem>British Virgin Islands</asp:ListItem>
                            <asp:ListItem>Brunei</asp:ListItem>
                            <asp:ListItem>Bulgaria</asp:ListItem>
                            <asp:ListItem>Burkina Faso</asp:ListItem>
                            <asp:ListItem>Burundi</asp:ListItem>
                            <asp:ListItem>Cambodia</asp:ListItem>
                            <asp:ListItem>Cameroon, United Republic Of</asp:ListItem>
                            <asp:ListItem>Canada</asp:ListItem>
                            <asp:ListItem>Cape Verde, Republic Of</asp:ListItem>
                            <asp:ListItem>Cayman Islands</asp:ListItem>
                            <asp:ListItem>Central African Republic</asp:ListItem>
                            <asp:ListItem>Chad</asp:ListItem>
                            <asp:ListItem>Chile</asp:ListItem>
                            <asp:ListItem>China</asp:ListItem>
                            <asp:ListItem>Christmas Island</asp:ListItem>
                            <asp:ListItem>Cocos Islands</asp:ListItem>
                            <asp:ListItem>Colombia</asp:ListItem>
                            <asp:ListItem>Comoros</asp:ListItem>
                            <asp:ListItem>Congo</asp:ListItem>
                            <asp:ListItem>Congo Democratic Republic Of</asp:ListItem>
                            <asp:ListItem>Cook Islands</asp:ListItem>
                            <asp:ListItem>Costa Rica</asp:ListItem>
                            <asp:ListItem>Croatia</asp:ListItem>
                            <asp:ListItem>Cuba</asp:ListItem>
                            <asp:ListItem>Cyprus</asp:ListItem>
                            <asp:ListItem>Czech Republic</asp:ListItem>
                            <asp:ListItem>Denmark</asp:ListItem>
                            <asp:ListItem>Djibouti</asp:ListItem>
                            <asp:ListItem>Dominica</asp:ListItem>
                            <asp:ListItem>Dominican Republic</asp:ListItem>
                            <asp:ListItem>Ecuador</asp:ListItem>
                            <asp:ListItem>Egypt</asp:ListItem>
                            <asp:ListItem>El Salvador</asp:ListItem>
                            <asp:ListItem>Equatorial Guinea</asp:ListItem>
                            <asp:ListItem>Eritrea</asp:ListItem>
                            <asp:ListItem>Estonia</asp:ListItem>
                            <asp:ListItem>Ethiopia</asp:ListItem>
                            <asp:ListItem>Faeroe Islands</asp:ListItem>
                            <asp:ListItem>Falkland Islands</asp:ListItem>
                            <asp:ListItem>Fiji Islands</asp:ListItem>
                            <asp:ListItem>Finland</asp:ListItem>
                            <asp:ListItem>France</asp:ListItem>
                            <asp:ListItem>French Guiana</asp:ListItem>
                            <asp:ListItem>French Polynesia</asp:ListItem>
                            <asp:ListItem>Gabon</asp:ListItem>
                            <asp:ListItem>Gambia</asp:ListItem>
                            <asp:ListItem>Georgia</asp:ListItem>
                            <asp:ListItem>Germany</asp:ListItem>
                            <asp:ListItem>Ghana</asp:ListItem>
                            <asp:ListItem>Gibraltar</asp:ListItem>
                            <asp:ListItem>Greece</asp:ListItem>
                            <asp:ListItem>Greenland</asp:ListItem>
                            <asp:ListItem>Grenada</asp:ListItem>
                            <asp:ListItem>Guadeloupe</asp:ListItem>
                            <asp:ListItem>Guam</asp:ListItem>
                            <asp:ListItem>Guatemala</asp:ListItem>
                            <asp:ListItem>Guernsey</asp:ListItem>
                            <asp:ListItem>Guinea</asp:ListItem>
                            <asp:ListItem>Guinea Bissau</asp:ListItem>
                            <asp:ListItem>Guyana</asp:ListItem>
                            <asp:ListItem>Haiti</asp:ListItem>
                            <asp:ListItem>Heard Island And McDonald Islands</asp:ListItem>
                            <asp:ListItem>Honduras</asp:ListItem>
                            <asp:ListItem>Hong Kong</asp:ListItem>
                            <asp:ListItem>Hungary</asp:ListItem>
                            <asp:ListItem>Iceland</asp:ListItem>
                            <asp:ListItem Selected="True">India</asp:ListItem>
                            <asp:ListItem>Indonesia</asp:ListItem>
                            <asp:ListItem>Iran</asp:ListItem>
                            <asp:ListItem>Iraq</asp:ListItem>
                            <asp:ListItem>Ireland, Republic Of</asp:ListItem>
                            <asp:ListItem>Isle Of Man</asp:ListItem>
                            <asp:ListItem>Israel</asp:ListItem>
                            <asp:ListItem>Italy</asp:ListItem>
                            <asp:ListItem>Ivory Coast</asp:ListItem>
                            <asp:ListItem>Jamaica</asp:ListItem>
                            <asp:ListItem>Japan</asp:ListItem>
                            <asp:ListItem>Jersey Island</asp:ListItem>
                            <asp:ListItem>Jordan</asp:ListItem>
                            <asp:ListItem>Kazakstan</asp:ListItem>
                            <asp:ListItem>Kenya</asp:ListItem>
                            <asp:ListItem>Kiribati</asp:ListItem>
                            <asp:ListItem>Korea, Democratic Peoples Republic</asp:ListItem>
                            <asp:ListItem>Korea, Republic Of</asp:ListItem>
                            <asp:ListItem>Kuwait</asp:ListItem>
                            <asp:ListItem>Kyrgyzstan</asp:ListItem>
                            <asp:ListItem>Lao, People&#39;s Dem. Rep.</asp:ListItem>
                            <asp:ListItem>Latvia</asp:ListItem>
                            <asp:ListItem>Lebanon</asp:ListItem>
                            <asp:ListItem>Lesotho</asp:ListItem>
                            <asp:ListItem>Liberia</asp:ListItem>
                            <asp:ListItem>Libyan Arab Jamahiriya</asp:ListItem>
                            <asp:ListItem>Liechtenstein</asp:ListItem>
                            <asp:ListItem>Lithuania</asp:ListItem>
                            <asp:ListItem>Luxembourg</asp:ListItem>
                            <asp:ListItem>Macau</asp:ListItem>
                            <asp:ListItem>Macedonia</asp:ListItem>
                            <asp:ListItem>Madagascar (Malagasy)</asp:ListItem>
                            <asp:ListItem>Malawi</asp:ListItem>
                            <asp:ListItem>Malaysia</asp:ListItem>
                            <asp:ListItem>Maldives</asp:ListItem>
                            <asp:ListItem>Mali</asp:ListItem>
                            <asp:ListItem>Malta</asp:ListItem>
                            <asp:ListItem>Marianna Islands</asp:ListItem>
                            <asp:ListItem>Marshall Islands</asp:ListItem>
                            <asp:ListItem>Martinique</asp:ListItem>
                            <asp:ListItem>Mauritania</asp:ListItem>
                            <asp:ListItem>Mauritius</asp:ListItem>
                            <asp:ListItem>Mayotte</asp:ListItem>
                            <asp:ListItem>Mexico</asp:ListItem>
                            <asp:ListItem>Micronesia</asp:ListItem>
                            <asp:ListItem>Moldova</asp:ListItem>
                            <asp:ListItem>Monaco</asp:ListItem>
                            <asp:ListItem>Mongolia</asp:ListItem>
                            <asp:ListItem>Montserrat</asp:ListItem>
                            <asp:ListItem>Morocco</asp:ListItem>
                            <asp:ListItem>Mozambique</asp:ListItem>
                            <asp:ListItem>Myanmar</asp:ListItem>
                            <asp:ListItem>Namibia</asp:ListItem>
                            <asp:ListItem>Nauru</asp:ListItem>
                            <asp:ListItem>Nepal</asp:ListItem>
                            <asp:ListItem>Netherland Antilles</asp:ListItem>
                            <asp:ListItem>Netherlands</asp:ListItem>
                            <asp:ListItem>New Caledonia</asp:ListItem>
                            <asp:ListItem>New Zealand</asp:ListItem>
                            <asp:ListItem>Nicaragua</asp:ListItem>
                            <asp:ListItem>Niger</asp:ListItem>
                            <asp:ListItem>Nigeria</asp:ListItem>
                            <asp:ListItem>Niue</asp:ListItem>
                            <asp:ListItem>Norfolk Island</asp:ListItem>
                            <asp:ListItem>Norway</asp:ListItem>
                            <asp:ListItem>Occupied Palestinian Territory</asp:ListItem>
                            <asp:ListItem>Oman, Sultanate Of</asp:ListItem>
                            <asp:ListItem>Pakistan</asp:ListItem>
                            <asp:ListItem>Palau</asp:ListItem>
                            <asp:ListItem>Panama</asp:ListItem>
                            <asp:ListItem>Papua New Guinea (Niugini)</asp:ListItem>
                            <asp:ListItem>Paraguay</asp:ListItem>
                            <asp:ListItem>Peru</asp:ListItem>
                            <asp:ListItem>Philippines</asp:ListItem>
                            <asp:ListItem>Pitcairn</asp:ListItem>
                            <asp:ListItem>Poland</asp:ListItem>
                            <asp:ListItem>Portugal</asp:ListItem>
                            <asp:ListItem>Qatar</asp:ListItem>
                            <asp:ListItem>Reunion</asp:ListItem>
                            <asp:ListItem>Romania</asp:ListItem>
                            <asp:ListItem>Russian Federation</asp:ListItem>
                            <asp:ListItem>Rwanda</asp:ListItem>
                            <asp:ListItem>Saint Lucia</asp:ListItem>
                            <asp:ListItem>Saint Vincent And The Grenadines</asp:ListItem>
                            <asp:ListItem>Samoa, American</asp:ListItem>
                            <asp:ListItem>Samoa, Independent State Of</asp:ListItem>
                            <asp:ListItem>San Marino</asp:ListItem>
                            <asp:ListItem>Sao Tome &amp; Principe</asp:ListItem>
                            <asp:ListItem>Saudi Arabia</asp:ListItem>
                            <asp:ListItem>Senegal</asp:ListItem>
                            <asp:ListItem>Serbia</asp:ListItem>
                            <asp:ListItem>Seychelles Islands</asp:ListItem>
                            <asp:ListItem>Sierra Leone</asp:ListItem>
                            <asp:ListItem>Singapore</asp:ListItem>
                            <asp:ListItem>Slovakia</asp:ListItem>
                            <asp:ListItem>Slovenia</asp:ListItem>
                            <asp:ListItem>Solomon Islands</asp:ListItem>
                            <asp:ListItem>Somalia</asp:ListItem>
                            <asp:ListItem>South Africa</asp:ListItem>
                            <asp:ListItem>Spain</asp:ListItem>
                            <asp:ListItem>Sri Lanka</asp:ListItem>
                            <asp:ListItem>St. Kitts - Nevis</asp:ListItem>
                            <asp:ListItem>St. Pierre &amp; Miquelon</asp:ListItem>
                            <asp:ListItem>Sudan</asp:ListItem>
                            <asp:ListItem>Suriname</asp:ListItem>
                            <asp:ListItem>Svalbard And Jan Mayen Is</asp:ListItem>
                            <asp:ListItem>Swaziland</asp:ListItem>
                            <asp:ListItem>Sweden</asp:ListItem>
                            <asp:ListItem>Switzerland</asp:ListItem>
                            <asp:ListItem>Syrian Arab Rep.</asp:ListItem>
                            <asp:ListItem>Taiwan, Republic of China</asp:ListItem>
                            <asp:ListItem>Tajikistan</asp:ListItem>
                            <asp:ListItem>Tanzania</asp:ListItem>
                            <asp:ListItem>Thailand</asp:ListItem>
                            <asp:ListItem>Timor Leste</asp:ListItem>
                            <asp:ListItem>Togo</asp:ListItem>
                            <asp:ListItem>Tonga</asp:ListItem>
                            <asp:ListItem>Trinidad &amp; Tobago</asp:ListItem>
                            <asp:ListItem>Tunisia</asp:ListItem>
                            <asp:ListItem>Turkey</asp:ListItem>
                            <asp:ListItem>Turkmenistan</asp:ListItem>
                            <asp:ListItem>Turks And Caicos Islands</asp:ListItem>
                            <asp:ListItem>Tuvalu</asp:ListItem>
                            <asp:ListItem>Uganda</asp:ListItem>
                            <asp:ListItem>Ukraine</asp:ListItem>
                            <asp:ListItem>United Arab Emirates</asp:ListItem>
                            <asp:ListItem>United Kingdom</asp:ListItem>
                            <asp:ListItem>United States</asp:ListItem>
                            <asp:ListItem>United States Minor Outlying Islnds</asp:ListItem>
                            <asp:ListItem>Uruguay</asp:ListItem>
                            <asp:ListItem>Uzbekistan Sum</asp:ListItem>
                            <asp:ListItem>Vanuatu</asp:ListItem>
                            <asp:ListItem>Vatican City State</asp:ListItem>
                            <asp:ListItem>Venezuela</asp:ListItem>
                            <asp:ListItem>Vietnam</asp:ListItem>
                            <asp:ListItem>Wallis &amp; Futuna Islands</asp:ListItem>
                            <asp:ListItem>Western Sahara</asp:ListItem>
                            <asp:ListItem>Yemen, Republic Of</asp:ListItem>
                            <asp:ListItem>Yugoslavia</asp:ListItem>
                            <asp:ListItem>Zambia</asp:ListItem>
                            <asp:ListItem>Zimbabwe</asp:ListItem>
                        </asp:DropDownList>


                        <div class="devider_10px"></div>

                        <asp:TextBox ID="txtEmail" CssClass="textboxcontact" runat="server" placeholder="Email*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtEmail" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtEmail" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <div class="devider_10px"></div>

                        <asp:TextBox ID="txtPanNumbers" CssClass="textboxcontact" runat="server" placeholder="Pan Number*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Pan Number'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtPanNumbers" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtPanNumbers" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <div class="devider_10px"></div>

                        <asp:TextBox ID="txtTelephone" CssClass="textboxcontact" runat="server"  onkeypress="return IsNumeric(event);" MaxLength="15" placeholder="Telephone*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Telephone'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtTelephone" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtTelephone" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                       <div class="devider_10px"></div>

                        <asp:TextBox ID="txtMobile" CssClass="textboxcontact" runat="server" onkeypress="return IsNumeric(event);" MaxLength="15" placeholder="Mobile*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtMobile" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtMobile" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <div class="devider_10px"></div>

                      


                        <div style="width: 99%; background-color: #999; height: 20px; color: #FFF; vertical-align: middle; padding: 8px 6px 10px 10px;">
                            Payment Details 
                        </div>

                        <div class="devider_20px"></div>

                        <asp:TextBox ID="txtAmount" CssClass="textboxcontact" runat="server" placeholder="Amount*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Amount'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rerftxtAmount" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtAmount" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <div class="devider_10px"></div>

                        <div>
                            <div style="display: inline; padding-right: 60px">

                                <strong>Donation Towards</strong>
                            </div>

                            <div style="display: inline; padding-right: 20px">
                                <%-- <asp:CheckBox ID="chkHospital" Text="Hospital" runat="server" />--%>
                             <div class="devider_10px"></div>
                                
                                   <asp:RadioButton ID="rbHospital" GroupName="donationTowards" Checked="true" Text="Hospital" OnCheckedChanged="donationTowards_CheckedChanged" AutoPostBack="true" runat="server" />
                            </div>

                            <div class="devider_10px"></div>

                            <div style="display: inline; padding-right: 20px">
                                <%--Patient Welfare Fund  &nbsp;&nbsp;<asp:CheckBox ID="chkPatientWelfareFund" runat="server" />--%>
                                <asp:RadioButton ID="rbPatientWelfareFund" GroupName="donationTowards" Text="Patient Welfare Fund" OnCheckedChanged="donationTowards_CheckedChanged" AutoPostBack="true" runat="server" />
                                <%--<asp:CheckBox ID="chkPatientWelfareFund" Text="Patient Welfare Fund" runat="server" />--%>
                            </div>



                            <div class="devider_10px"></div>

                            <div style="display: inline; padding-right: 20px">
                                <%--Cancer Research   &nbsp;&nbsp;<asp:CheckBox ID="chkCancerResearch" runat="server" />--%>
                                <asp:RadioButton ID="rbEquipmentOrInfrastucture" GroupName="donationTowards" Text="Equipment/infrastucture" OnCheckedChanged="donationTowards_CheckedChanged" AutoPostBack="true" runat="server" />
                                <%--<asp:CheckBox ID="chkEquipmentOrInfrastucture"  Text="Equipment/infrastucture" runat="server" />--%>
                            </div>
                            <div class="devider_10px"></div>
                            <div style="display: inline; padding-right: 20px">
                                <%-- Others  &nbsp;&nbsp;<asp:CheckBox ID="chkDonationTowardsOthers" runat="server" />--%>
                                <asp:RadioButton ID="rbDonationTowardsOthers" GroupName="donationTowards" Text="Others" runat="server" OnCheckedChanged="donationTowards_CheckedChanged" AutoPostBack="true" />
                                <%--<asp:CheckBox ID="chkOthers"  Text="Others" runat="server" />--%>
                            </div>

                        </div>

                        <asp:Panel ID="pnlDonationTowardsOther" Visible="false" runat="server">
                            <div class="devider_20px"></div>



                            <asp:TextBox ID="txtDonationTowardsOther" CssClass="textboxcontact" runat="server" placeholder="If others(specify)" onfocus="this.placeholder = ''" onblur="this.placeholder = 'if others(specify)'"></asp:TextBox>
                        </asp:Panel>

                        <div class="devider_20px"></div>

                          <asp:TextBox ID="txtMessageToBePrintedOnReceipt" CssClass="textboxcontact" runat="server" placeholder="Message to be printed on Receipt" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Message to be printed on Receipt'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtMessageToBePrintedOnReceipt" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtMessageToBePrintedOnReceipt" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <div class="devider_20px"></div>


                        <div>
                            <div style="display: inline; padding-right: 30px">

                                <strong>*Do you require Income-tax exemption Certificate under section 80G? </strong>
                            </div>
                            <div class="devider_20px"></div>
                            <%--<div style="display: inline; padding-right: 20px">
                                 Yes   &nbsp;&nbsp;<asp:CheckBox ID="chkIsIncomTaxCertificateRequiredYes" runat="server" />
                            </div>--%>
                            <asp:RadioButton ID="rbIsIncomTaxCertificateRequiredYes" GroupName="incomeTaxExemptionCertificate" Checked="true" Text="Yes" runat="server" OnCheckedChanged="rbIsIncomTaxCertificateRequiredNo_CheckedChanged" AutoPostBack="true" />


                            <div style="display: inline">
                                <%-- No   &nbsp;&nbsp;<asp:CheckBox ID="chkIsIncomTaxCertificateRequiredNo" runat="server" />--%>
                                <asp:RadioButton ID="rbIsIncomTaxCertificateRequiredNo" GroupName="incomeTaxExemptionCertificate" Text="No" runat="server" OnCheckedChanged="rbIsIncomTaxCertificateRequiredNo_CheckedChanged" AutoPostBack="true"  />

                            </div>
                        </div>
                   

                    <%-- <div class="devider_20px"></div>--%>
                    <div>
                        <%-- <div style="display: inline; padding-right: 30px">

                                <strong>If yes tax exemption required U/S 80G  </strong>
                            </div>--%>

                        <div style="display: inline; padding-right: 20px">

                            <asp:RadioButton ID="rbIncomTaxExemptionOption80" GroupName="taxExemption" Visible="false" Checked="true" Text="80G" runat="server" />
                        </div>

                        <div style="display: inline; padding-right: 20px">

                            <asp:RadioButton ID="rbIncomTaxExemptionOption35" GroupName="taxExemption" Visible="false" Text="35 (1) (ii) " runat="server" />

                        </div>

                    </div>

                    <asp:Panel ID="pnlDonationReceipt" runat="server">
                        <div class="devider_10px"></div>

                        <asp:TextBox ID="txtDonationReceipt" CssClass="textboxcontact" runat="server" placeholder="Donation receipt in favour of*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Donation receipt in favour of'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtDonationReceipt" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtDonationReceipt" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </asp:Panel>
                    <div class="devider_20px"></div>


                    <div>

                        <strong>Note:</strong>
                        <div class="devider_10px"></div>
                        The receipt and exemption income tax certificate if any will be sent by post to the address specified.
                            <div class="devider_20px"></div>
                        Donation once made will not be refunded.

                    </div>
                 



                    <%-- <div class="devider_20px"></div>
                        <div>
                            <div style="display: inline; padding-right: 30px">

                                <strong>Mode of Payment </strong>
                            </div>
                            <div style="display: inline">
                                Credit Card   &nbsp;&nbsp;<asp:CheckBox ID="chkCreditCard" Checked="true" runat="server" Visible="false" />

                            </div>

                        </div>
                        <div class="devider_20px"></div>--%>
                    <div>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </div>
                    <div class="devider_20px"></div>
                   
                        <%--<img src="images/SubmitButton.png" 

                            <div class="more"><a href="#" class="make_payment">Save and Make Payment</a></div>/>--%>

             <asp:Button ID="btnSaveAndMakePayment" runat="server" Text="Save and Make Payment" ValidationGroup="vChildDonationOnlineAdd" CssClass="btn" OnClick="btnSaveAndMakePayment_Click" />

                  

                    </asp:Panel>
                </div>

                        </div>
                <!-- ABOUT US CONTENT -->

            </div>
        </div>

        <div class="col-1-3">
            <div class="wrap-col">

                <div class="box">
                    <div class="heading">
                        <h2>CONTRIBUTE</h2>
                    </div>
                    <div class="content">
                        <div class="list">
                            <ul>


                                <li><a href="child-donate-online.aspx">Donate Online</a></li>
                                <li><a href="child-sponsor-patient.aspx">Sponsor a Patient</a></li>
                                <li><a href="child-donate-equipment.aspx">Donate Equipment</a></li>

                                <li><a href="child-donate-inkind.aspx">Donate in-kind </a></li>


                                <li><a href="child-corporate-giving.aspx">Corporate Giving</a></li>
                                <li><a href="child-testimonials.aspx?catnm=Contributors">Testimonials </a></li>



                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
    </div>
</asp:Content>
