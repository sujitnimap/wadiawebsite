﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class cme_list : System.Web.UI.Page
    {
        IList<CMERegistration> cmedata;
        string flag = "";
        string formCat = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
                if (flag == "c")
                {
                    formCat = "child care";
                }
                if (flag == "w")
                {
                    formCat = "Maternity care";
                }
            }           

            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                cmedata = CCMERegistration.Instance().GetCMERegistrationList(formCat);

                //Bind the fetched data to gridview
                if (cmedata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(cmedata);

                    grdCMERegMaster.DataSource = detailTable;
                    grdCMERegMaster.DataBind();
                }
                else
                {
                    grdCMERegMaster.DataSource = null;
                    grdCMERegMaster.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=CMERegistrationsDetail.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                grdCMERegMaster.AllowPaging = false;
                this.BindGrid();

                grdCMERegMaster.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in grdCMERegMaster.HeaderRow.Cells)
                {
                    cell.BackColor = grdCMERegMaster.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in grdCMERegMaster.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = grdCMERegMaster.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = grdCMERegMaster.RowStyle.BackColor;
                        }
                        //cell.CssClass = "textmode";
                    }
                }

                grdCMERegMaster.RenderControl(hw);

                //style to format numbers to string
                //string style = @"<style> .textmode { } </style>";
                // Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void grdCMERegMaster_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdCMERegMaster.PageIndex = e.NewPageIndex;
            grdCMERegMaster.DataBind();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void grdCMERegMaster_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //int index = Convert.ToInt32(e.CommandArgument);
            //if (e.CommandName.Equals("editRecord"))
            //{
            //    cmedata = CCME.Instance().GetcmeData(flag);

            //    GridViewRow gvrow = grdCMERegMaster.Rows[index];

            //    Int32 id = Convert.ToInt32(grdCMERegMaster.DataKeys[index].Value.ToString());

            //    IList<cme> data = cmedata.Where(p => p.Id.Equals(id)).ToList();

            //    DataTable detailTable = ToDataTable(data);

            //    lblid.Text = id.ToString();
            //    ddlRegFeesCatEdit.SelectedValue = (detailTable.Rows[0]["RegFeesCategory"]).ToString();
            //    txtLastAddateEdit.Text = (detailTable.Rows[0]["LastRegDate"]).ToString();
            //    ddlRegCatEdit.SelectedValue = (detailTable.Rows[0]["RegCategory"]).ToString();
            //    txtFeesEdit.Text = (detailTable.Rows[0]["Fees"]).ToString();
            //    lblResult.Visible = false;

            //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //    sb.Append(@"<script type='text/javascript'>");
            //    sb.Append("$('#editModal').modal('show');");
            //    sb.Append(@"</script>");
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            //}
            //else if (e.CommandName.Equals("deleteRecord"))
            //{
            //    string id = grdCMERegMaster.DataKeys[index].Value.ToString();
            //    hfCode.Value = id;
            //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //    sb.Append(@"<script type='text/javascript'>");
            //    sb.Append("$('#deleteModal').modal('show');");
            //    sb.Append(@"</script>");
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            //}
        }        
    }
}