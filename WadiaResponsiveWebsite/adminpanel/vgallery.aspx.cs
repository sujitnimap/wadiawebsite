﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class vgallary : System.Web.UI.Page
    {
        DataTable vgallarydata;

        string flag = "";

        static string type = "Video";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }


            if (!IsPostBack)
            {
                BindGrid();
                bindcategory();
            }

        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                vgallarydata = CVideoGallary.Instance().GetVideogallaryData(flag, type);

                //Bind the fetched data to gridview
                if (vgallarydata.Rows.Count > 0)
                {
                    //DataTable detailTable = ToDataTable(artgallarydata);

                    grdvwgallary.DataSource = vgallarydata;
                    grdvwgallary.DataBind();
                }
                else
                {
                    grdvwgallary.DataSource = null;
                    grdvwgallary.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public void bindcategory()
        {
            category c = new category();

            ddlcategory.DataSource = CCategory.Instance().GetCategoryData(flag, type);
            ddlcategory.DataBind();
            ddlcategory.Items.Insert(0, new ListItem("Select", "0"));

            ddleditcategory.DataSource = CCategory.Instance().GetCategoryData(flag, type);
            ddleditcategory.DataBind();
            ddleditcategory.Items.Insert(0, new ListItem("Select", "0"));

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwgallary_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                Int32 id = Convert.ToInt32(grdvwgallary.DataKeys[index].Value.ToString());
                vgallarydata = CVideoGallary.Instance().GetVideogallaryData(id);
                
                DetailsView1.DataSource = vgallarydata;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))
            {
                GridViewRow gvrow = grdvwgallary.Rows[index];

                Int32 id = Convert.ToInt32(grdvwgallary.DataKeys[index].Value.ToString());

                vgallarydata = CVideoGallary.Instance().GetVideogallaryData(id);


                lblid.Text = id.ToString();
                txtalbumName.Text = (vgallarydata.Rows[0]["name"]).ToString();
                ddleditcategory.SelectedValue = (vgallarydata.Rows[0]["categoryid"]).ToString();
                lblResult.Visible = false;

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwgallary.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {

            int id = Convert.ToInt32(lblid.Text.ToString());
            string name = txtalbumName.Text.Trim();

            videogallary objvideogallary = new videogallary();


            vgallarydata = CArtgallary.Instance().GetArtgallaryData(id);

            COperationStatus os = new COperationStatus();

            objvideogallary.id = id;
            objvideogallary.name = name;
            objvideogallary.categoryid = Convert.ToInt32(ddleditcategory.SelectedValue);
            objvideogallary.categorynm = ddleditcategory.SelectedItem.ToString();
            objvideogallary.videolink = "";

            os = CVideoGallary.Instance().UpdateVideogallary(objvideogallary);

            if (os.Success == true)
            {
                txtalbumName.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }
        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {

            string name = txtalbumnm.Text.Trim();

            videogallary objvideogallary = new videogallary();
            COperationStatus os = new COperationStatus();

            objvideogallary.name = name;
            objvideogallary.categoryid = Convert.ToInt32(ddlcategory.SelectedValue);
            objvideogallary.categorynm = ddlcategory.SelectedItem.ToString();
            objvideogallary.videolink = "";
            objvideogallary.createdate = DateTime.Now;
            objvideogallary.isactive = true;
            objvideogallary.flag = flag;

            os = CVideoGallary.Instance().InsertVideogallary(objvideogallary);

            if (os.Success == true)
            {
                txtalbumnm.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + os.Message + "');");
                sb.Append("$('#addModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;

            category objevent = new category();
            COperationStatus os = new COperationStatus();

            os = CVideoGallary.Instance().DeleteVideogallary(Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        protected void grdvwgallary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwgallary.PageIndex = e.NewPageIndex;
            grdvwgallary.DataBind();
        }

    }
}