﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class tenders : System.Web.UI.Page
    {
        IList<tender> tenderdata;
        string flag = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }


            if (!IsPostBack)
            {
                BindGrid();
            }

        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                tenderdata = CTender.Instance().GetTenderData(flag);

                //Bind the fetched data to gridview
                if (tenderdata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(tenderdata);

                    grdvwtender.DataSource = detailTable;
                    grdvwtender.DataBind();
                }
                else
                {
                    grdvwtender.DataSource = null;
                    grdvwtender.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwtender_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                Int32 id = Convert.ToInt32(grdvwtender.DataKeys[index].Value.ToString());

                tenderdata = CTender.Instance().GetTenderData(flag);
                IList<tender> data = tenderdata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))
            {
                GridViewRow gvrow = grdvwtender.Rows[index];

                Int32 id = Convert.ToInt32(grdvwtender.DataKeys[index].Value.ToString());

                tenderdata = CTender.Instance().GetTenderData(flag);
                IList<tender> data = tenderdata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                lblid.Text = id.ToString();
                txtName.Text = (detailTable.Rows[0]["name"]).ToString();
                FreeTextBox1.Text = (detailTable.Rows[0]["description"]).ToString();
                txtexpirydate.Text = (detailTable.Rows[0]["expirydate"]).ToString();
                txtOpeningDate.Text = (detailTable.Rows[0]["openingdate"]).ToString();
                lblResult.Visible = false;

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwtender.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            string downloadlink = "";
            if (fldtndrfile.HasFile)
            {
                string filename = Path.GetFileName(fldtndrfile.PostedFile.FileName);

                string file = "tenderfiles/" + filename;

                fldtndrfile.SaveAs(Server.MapPath("~/adminpanel/tenderfiles/" + filename));

                downloadlink = file;
            }
            else
            {
                downloadlink = "n/a";
            }

            int id = Convert.ToInt32(lblid.Text.ToString());
            string name = txtName.Text.Trim();
            string description = FreeTextBox1.Text.Trim();
            string openingdate = txtOpeningDate.Text.Trim();
            string expirydate = txtexpirydate.Text.Trim();


            tender objtender = new tender();
            COperationStatus os = new COperationStatus();

            objtender.id = id;
            objtender.name = name;
            objtender.description = description;
            objtender.openingdate = Convert.ToDateTime(openingdate);
            objtender.expirydate = Convert.ToDateTime(expirydate);
            objtender.downloadlink = downloadlink;

            os = CTender.Instance().UpdateTender(objtender);

            if (os.Success == true)
            {
                txtName.Text = string.Empty;
                FreeTextBox1.Text = string.Empty;
                txtOpeningDate.Text = string.Empty;
                txtexpirydate.Text = string.Empty;


                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }
        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {
            string downloadlink = "";

            if (fldtndrfile.HasFile)
            {
                string filename = Path.GetFileName(fldtndrfile.PostedFile.FileName);

                string file = "tenderfiles/" + filename;

                fldtndrfile.SaveAs(Server.MapPath("~/adminpanel/tenderfiles/" + filename));

                downloadlink = file;
            }
            else
            {
                downloadlink = "n/a";
            }

            string name = txteventnm.Text.Trim();
            string description = txtdescrption.Text.Trim();
            string openingdate = txtopening.Text;
            string expirydate = txtexpiry.Text;


            tender objtest = new tender();
            COperationStatus os = new COperationStatus();

            objtest.name = name;
            objtest.description = description;
            objtest.openingdate = Convert.ToDateTime(openingdate);
            objtest.expirydate = Convert.ToDateTime(expirydate);
            objtest.downloadlink = downloadlink;
            objtest.createdate = DateTime.Now;
            objtest.isactive = true;
            objtest.flag = flag;

            os = CTender.Instance().InsertTender(objtest);

            if (os.Success == true)
            {
                txteventnm.Text = string.Empty;
                txtdescrption.Text = string.Empty;
                txtopening.Text = string.Empty;
                txtexpiry.Text = string.Empty;


                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + os.Message + "');");
                sb.Append("$('#addModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }

        }

        //protected void lnkDownload_Click(object sender, EventArgs e)
        //{
        //    LinkButton lnkbtn = sender as LinkButton;
        //    GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
        //    string id = grdvwtender.DataKeys[gvrow.RowIndex].Value.ToString();

        //    IList<tender> databyid = null;           

        //    databyid = CTender.Instance().GetTenderDatabyid(Convert.ToInt32(id));

        //    DataTable detailTable = ToDataTable(databyid);

        //    string filePath = detailTable.Rows[0]["downloadlink"].ToString();

        //    Response.ContentType = "image/jpg";
        //    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filePath + "\"");
        //    Response.TransmitFile(Server.MapPath(filePath));
        //    Response.End();
        //}

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;

            tender objevent = new tender();
            COperationStatus os = new COperationStatus();

            os = CTender.Instance().DeleteTender(Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        protected void grdvwtender_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwtender.PageIndex = e.NewPageIndex;
            grdvwtender.DataBind();
        }
    }
}