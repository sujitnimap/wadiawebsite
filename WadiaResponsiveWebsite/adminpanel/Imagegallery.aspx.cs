﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class gallaryImages : System.Web.UI.Page
    {
        IList<gallaryimg> gallaryimgdata;


        public int intCatId
        {
            get
            {
                if (ViewState["catid"].Equals(null))
                    return 0;
                else
                    return (int)ViewState["catid"];
            }
            set
            {
                ViewState["catid"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(Request.QueryString["catid"]))
            {
                ViewState["catid"] = Convert.ToInt32(Request.QueryString["catid"]);
            }

            if (!IsPostBack)
            {
                BindGrid();
                binddtl();
            }
        }


        public void binddtl()
        {

            DataTable dt = CArtgallary.Instance().GetArtgallaryData(intCatId);

            lblalbumnm.Text = dt.Rows[0]["albumname"].ToString();
            lblcatnm.Text = dt.Rows[0]["categorynm"].ToString();
        }


        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                gallaryimgdata = CGallary.Instance().GetgallaryimgData(intCatId);

                //Bind the fetched data to gridview
                if (gallaryimgdata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(gallaryimgdata);

                    grdvwgallaryview.DataSource = detailTable;
                    grdvwgallaryview.DataBind();
                }
                else
                {
                    grdvwgallaryview.DataSource = null;
                    grdvwgallaryview.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwgallaryview_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwgallaryview.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {
            //---------------Image Upload Start----------//
            string bigfilename = "n/a";
            string thumfilename = "n/a";

            if (fldmainimg.HasFile && fldthumbnail.HasFile)
            {
                //bigfilename = Path.GetFileName(fldmainimg.FileName);
                //fldmainimg.SaveAs(Server.MapPath("gallary/") + bigfilename);
                //thumfilename = Path.GetFileName(fldthumbnail.FileName);
                //fldthumbnail.SaveAs(Server.MapPath("gallary/") + thumfilename);

                if (fldmainimg.HasFile)
                {
                    string extension = Path.GetExtension(fldmainimg.FileName);
                    string filename = Path.GetFileNameWithoutExtension(fldmainimg.FileName);
                    string id = Guid.NewGuid().ToString();

                    string fileLocation = string.Format("{0}/{1}{2}", Server.MapPath("gallary/mainimage/"), filename + id, extension);

                    fldmainimg.SaveAs(fileLocation);

                    bigfilename = "gallary/mainimage/" + filename + id + extension;
                }

                if (fldthumbnail.HasFile)
                {
                    string extension = Path.GetExtension(fldthumbnail.FileName);
                    string filename = Path.GetFileName(fldthumbnail.FileName);
                    string id = Guid.NewGuid().ToString();

                    string fileLocation = string.Format("{0}/{1}{2}", Server.MapPath("gallary/thumbnail/"), filename + id, extension);

                    fldthumbnail.SaveAs(fileLocation);

                    thumfilename = "gallary/thumbnail/" + filename + id + extension;
                }

            }
            //-------------Image Upload End----------//


            gallaryimg objgallaryimg = new gallaryimg();
            COperationStatus os = new COperationStatus();
            objgallaryimg.categoryid = intCatId;
            objgallaryimg.imageurl = bigfilename;
            objgallaryimg.thumbnail = thumfilename;
            objgallaryimg.createdate = DateTime.Now;
            objgallaryimg.isactive = true;

            os = CGallary.Instance().Insertgallaryimg(objgallaryimg);

            if (os.Success == true)
            {

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + os.Message + "');");
                sb.Append("$('#addModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;

            category objevent = new category();
            COperationStatus os = new COperationStatus();

            os = CGallary.Instance().Deletegallaryimg(Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        protected void grdvwgallaryview_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwgallaryview.PageIndex = e.NewPageIndex;
            grdvwgallaryview.DataBind();
        }

    }
}