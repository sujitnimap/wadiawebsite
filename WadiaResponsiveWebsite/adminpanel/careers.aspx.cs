﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class careers : System.Web.UI.Page
    {
        IList<career> careerdata;

        string flag = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }

            if (!IsPostBack)
            {
                BindGrid();
                getjob();
            }


        }


        public void getjob()
        {
            //job c = new job();

            //ddljobtype.DataSource = CJob.Instance().GetJobData(flag);
            //ddljobtype.DataBind();
            //ddljobtype.Items.Insert(0, new ListItem("Select", "0"));

        }


        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                careerdata = CCareer.Instance().GetCareerData(flag);

                //Bind the fetched data to gridview
                if (careerdata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(careerdata);

                    grdvwcareer.DataSource = detailTable;
                    grdvwcareer.DataBind();
                }
                else
                {
                    grdvwcareer.DataSource = null;
                    grdvwcareer.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }
        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwcareer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                Int32 id = Convert.ToInt32(grdvwcareer.DataKeys[index].Value.ToString());

                careerdata = CCareer.Instance().GetCareerData(flag);
                IList<career> data = careerdata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwcareer.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;
            //executeDelete(id);
            ourevent objevent = new ourevent();
            COperationStatus os = new COperationStatus();

            os = CCareer.Instance().Deletecareer(Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }


        protected void grdvwcareer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwcareer.PageIndex = e.NewPageIndex;
            grdvwcareer.DataBind();
        }


    }
}