﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminpanel/admin.Master" EnableEventValidation = "false" AutoEventWireup="true" CodeBehind="ChildParticipantOfMarathon.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel.ChildParticipantOfMarathon" %>


<%@ Register TagName="Error" Src="~/usercontrols/Error.ascx" TagPrefix="userControlError" %>
<%@ Register TagName="Info" Src="~/usercontrols/Info.ascx" TagPrefix="userControlInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large;">Child Participant Of Marathon
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <userControlInfo:Info ID="info" runat="server" Visible="false" />
                    <userControlError:Error ID="error" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" Height="30px" OnClick="btnExport_Click" />

                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>
                            <div style="height: 10px; width: 100%">
                            </div>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdvwChildOfMarathonInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-hover table-striped"  HorizontalAlign="Center" OnPageIndexChanging="grdvwChildOfMarathonInfo_PageIndexChanging" PageSize="15" Width="100%">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr No.">
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Transation-ID">
                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstName" runat="server" Text='<%#Bind("TransationID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Child's First Name">
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstName" runat="server" Text='<%#Bind("ChildFirstName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Child's Middle Name">
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstName" runat="server" Text='<%#Bind("ChildMiddleName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Child's Last Name">
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstName" runat="server" Text='<%#Bind("ChildLastName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Age">
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="server" Text='<%#Bind("Age") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Gender">
                                            <ItemStyle HorizontalAlign="Center" Width="25%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmail" runat="server" Text='<%#Bind("Gender") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TShirtSize">
                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblMobileNumber" runat="server" Text='<%#Bind("TShirtSize") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="How did come to know about the Marathon">
                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblKnowAboutMarathon" runat="server" Text='<%#Bind("KnowAboutMarathonBy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>


                </td>
            </tr>
        </table>

        
    </div>

</asp:Content>
