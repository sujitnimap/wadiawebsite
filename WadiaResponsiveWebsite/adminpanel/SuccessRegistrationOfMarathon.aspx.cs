﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class SuccessRegistrationOfMarathon : System.Web.UI.Page
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ToString());
        string flag = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }

            if (!IsPostBack)
            {
                BindGrid();
            }

        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database

                DataTable childdata = new DataTable();

                SqlCommand cmd = new SqlCommand("GetSuccessMarathonRegistration", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Open();
                da.Fill(childdata);
                conn.Close();

                //Bind the fetched data to gridview

                if (childdata.Rows.Count > 0)
                {
                    grdvwSuccessRegstOfMarathonInfo.DataSource = childdata;
                    grdvwSuccessRegstOfMarathonInfo.DataBind();
                }
                else
                {
                    grdvwSuccessRegstOfMarathonInfo.DataSource = null;
                    grdvwSuccessRegstOfMarathonInfo.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=SuccessRegistrationOfMarathon.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                grdvwSuccessRegstOfMarathonInfo.AllowPaging = false;
                this.BindGrid();

                grdvwSuccessRegstOfMarathonInfo.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in grdvwSuccessRegstOfMarathonInfo.HeaderRow.Cells)
                {
                    cell.BackColor = grdvwSuccessRegstOfMarathonInfo.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in grdvwSuccessRegstOfMarathonInfo.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = grdvwSuccessRegstOfMarathonInfo.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = grdvwSuccessRegstOfMarathonInfo.RowStyle.BackColor;
                        }
                        //cell.CssClass = "textmode";
                    }
                }

                grdvwSuccessRegstOfMarathonInfo.RenderControl(hw);

                //style to format numbers to string
                //string style = @"<style> .textmode { } </style>";
                // Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void grdvwSuccessRegstOfMarathonInfo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwSuccessRegstOfMarathonInfo.PageIndex = e.NewPageIndex;
            grdvwSuccessRegstOfMarathonInfo.DataBind();
        }

    }
}