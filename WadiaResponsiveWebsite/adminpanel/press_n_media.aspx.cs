﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;
namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class press_n_media : System.Web.UI.Page
    {
        IList<pressmedia> pressmediadata;

        string flag = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }

            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                pressmediadata = CPressmedia.Instance().GetMediaData(flag);

                //Bind the fetched data to gridview
                if (pressmediadata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(pressmediadata);

                    grdvwpresmedia.DataSource = detailTable;
                    grdvwpresmedia.DataBind();
                }
                else
                {
                    grdvwpresmedia.DataSource = null;
                    grdvwpresmedia.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwpresmedia_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                Int32 id = Convert.ToInt32(grdvwpresmedia.DataKeys[index].Value.ToString());

                pressmediadata = CPressmedia.Instance().GetMediaData(flag);
                IList<pressmedia> data = pressmediadata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))
            {
                GridViewRow gvrow = grdvwpresmedia.Rows[index];

                Int32 id = Convert.ToInt32(grdvwpresmedia.DataKeys[index].Value.ToString());

                pressmediadata = CPressmedia.Instance().GetMediaData(flag);
                IList<pressmedia> data = pressmediadata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                lblid.Text = id.ToString();
                txtupheadline.Text = (detailTable.Rows[0]["headline"]).ToString();
                txtupsource.Text = (detailTable.Rows[0]["source"]).ToString();
                txtupdatedate.Text = (detailTable.Rows[0]["date"]).ToString();
                txtupdatelink.Text = (detailTable.Rows[0]["link"]).ToString();
                lblResult.Visible = false;

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwpresmedia.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            

            int id = Convert.ToInt32(lblid.Text.ToString());
            string headline = txtupheadline.Text.Trim();
            string source = txtupsource.Text.Trim();
            string date = txtupdatedate.Text.Trim();
            string link = txtupdatelink.Text.Trim();

            pressmedia objpressmedia = new pressmedia();
            COperationStatus os = new COperationStatus();

            objpressmedia.id = id;
            objpressmedia.headline = headline;
            objpressmedia.source = source;
            objpressmedia.link = link;
            objpressmedia.date = Convert.ToDateTime(date);

            os = CPressmedia.Instance().UpdatePressmedia(objpressmedia);

            if (os.Success == true)
            {
                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }
        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {
            
            string headline = txtaddheadline.Text.Trim();
            string link = txtaddlink.Text.Trim();
            string date = txtadddate.Text;
            string source = txtaddsource.Text;

            pressmedia objpressmedia = new pressmedia();
            COperationStatus os = new COperationStatus();

            objpressmedia.headline = headline;
            objpressmedia.link = link;
            objpressmedia.source = source;
            objpressmedia.date = Convert.ToDateTime(date);
            objpressmedia.createdate = DateTime.Now;
            objpressmedia.isactive = true;
            objpressmedia.flag = flag;

            os = CPressmedia.Instance().InsertPressmedia(objpressmedia);

            if (os.Success == true)
            {

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + os.Message + "');");
                sb.Append("$('#addModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;

            pressmedia objpressmedia = new pressmedia();
            COperationStatus os = new COperationStatus();

            os = CPressmedia.Instance().DeletePressmedia(Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        protected void grdvwpresmedia_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwpresmedia.PageIndex = e.NewPageIndex;
            grdvwpresmedia.DataBind();
        }
    }
}