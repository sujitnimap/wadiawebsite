﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class awardsnachivements : System.Web.UI.Page
    {
        IList<awardnachievement> awarddata;

        string flag = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }

            if (!IsPostBack)
            {
                BindGrid();
            }

        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                awarddata = CAwardsnachive.Instance().GetAwardData(flag);

                //Bind the fetched data to gridview
                if (awarddata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(awarddata);

                    grdvwaward.DataSource = detailTable;
                    grdvwaward.DataBind();
                }
                else
                {
                    grdvwaward.DataSource = null;
                    grdvwaward.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwaward_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                awarddata = CAwardsnachive.Instance().GetAwardData(flag);

                Int32 id = Convert.ToInt32(grdvwaward.DataKeys[index].Value.ToString());

                IList<awardnachievement> data = awarddata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))
            {
                awarddata = CAwardsnachive.Instance().GetAwardData(flag);

                GridViewRow gvrow = grdvwaward.Rows[index];

                Int32 id = Convert.ToInt32(grdvwaward.DataKeys[index].Value.ToString());

                IList<awardnachievement> data = awarddata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                lblid.Text = id.ToString();
                FreeTextBox1.Text = (detailTable.Rows[0]["description"]).ToString();
                txteditdoctornm.Text = (detailTable.Rows[0]["doctornm"]).ToString();
                //ddleditcategory.SelectedItem.Text = (detailTable.Rows[0]["categorynm"]).ToString();
                ddleditcategory.SelectedIndex = Convert.ToInt32((detailTable.Rows[0]["categoryid"]));
                txtedityear.Text = (detailTable.Rows[0]["year"]).ToString();

                if ((detailTable.Rows[0]["categorynm"]).ToString() != "")
                {
                    ddleditcategory.SelectedItem.Text = (detailTable.Rows[0]["categorynm"]).ToString();
                }

                txtedityear.Text = (detailTable.Rows[0]["year"]).ToString();
                lblResult.Visible = false;

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwaward.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(lblid.Text.ToString());

            string description = FreeTextBox1.Text.Trim();
            string year = txtedityear.Text.Trim();
            string catnm = ddleditcategory.SelectedItem.Text.Trim();
            string doctornm = txteditdoctornm.Text.Trim();
            int categoryid = ddleditcategory.SelectedIndex;

            awardnachievement objaward = new awardnachievement();
            COperationStatus os = new COperationStatus();

            objaward.id = id;
            objaward.description = description;
            objaward.year = year;
            objaward.categorynm = catnm;
            objaward.categoryid = categoryid;
            objaward.doctornm = doctornm;

            os = CAwardsnachive.Instance().Updateawardnachievement(objaward);

            if (os.Success == true)
            {
                txtedityear.Text = string.Empty;
                txteditdoctornm.Text = string.Empty;
                FreeTextBox1.Text = string.Empty;
                ddleditcategory.SelectedIndex = 0;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }
        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {
            string doctonm = txtadddoctornm.Text.Trim();
            string description = txtdescrption.Text.Trim();
            string catorynm = ddladdcategory.SelectedItem.Text.Trim();
            string year = txtaddYear.Text.Trim();

            awardnachievement objtest = new awardnachievement();

            COperationStatus os = new COperationStatus();

            objtest.doctornm = doctonm;
            objtest.description = description;
            objtest.categorynm = catorynm;
            objtest.year = year;
            objtest.createdate = DateTime.Now;
            objtest.isactive = true;
            objtest.flag = flag;
            objtest.categoryid = ddladdcategory.SelectedIndex;

            os = CAwardsnachive.Instance().Insertawardnachievement(objtest);

            if (os.Success == true)
            {

                txtadddoctornm.Text = string.Empty;
                txtdescrption.Text = string.Empty;
                txtaddYear.Text = string.Empty;
                ddladdcategory.SelectedIndex = 0;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + os.Message + "');");
                sb.Append("$('#addModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;

            category objevent = new category();
            COperationStatus os = new COperationStatus();

            os = CAwardsnachive.Instance().Deleteawards(Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }


        protected void grdvwaward_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwaward.PageIndex = e.NewPageIndex;
            grdvwaward.DataBind();
        }

    }
}