﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class case_study : System.Web.UI.Page
    {
        IList<casestudy> casestudydata;

        string flag = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }

            if (!IsPostBack)
            {
                BindGrid();
            }

        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                casestudydata = CCasestudy.Instance().GetCasestudyData(flag);

                //Bind the fetched data to gridview
                if (casestudydata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(casestudydata);

                    grdvwcasestudy.DataSource = detailTable;
                    grdvwcasestudy.DataBind();
                }
                else
                {
                    grdvwcasestudy.DataSource = null;
                    grdvwcasestudy.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwcasestudy_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                Int32 id = Convert.ToInt32(grdvwcasestudy.DataKeys[index].Value.ToString());

                casestudydata = CCasestudy.Instance().GetCasestudyData(flag);
                IList<casestudy> data = casestudydata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))
            {
                GridViewRow gvrow = grdvwcasestudy.Rows[index];

                Int32 id = Convert.ToInt32(grdvwcasestudy.DataKeys[index].Value.ToString());

                casestudydata = CCasestudy.Instance().GetCasestudyData(flag);
                IList<casestudy> data = casestudydata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                lblid.Text = id.ToString();
                txtName.Text = (detailTable.Rows[0]["title"]).ToString();
                FreeTextBox1.Text = (detailTable.Rows[0]["description"]).ToString();
                lblResult.Visible = false;

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwcasestudy.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            string filepath = "";

            Boolean isfile = false;

            if (fldcasestudyfile.HasFile)
            {
                string filename = Path.GetFileName(fldcasestudyfile.PostedFile.FileName);

                string file = "casestudies/" + filename;

                fldcasestudyfile.SaveAs(Server.MapPath("~/adminpanel/casestudies/" + filename));

                filepath = file;

                isfile = true;
            }
            else
            {
                filepath = "#";

                isfile = false;
            }


            int id = Convert.ToInt32(lblid.Text.ToString());
            string title = txtName.Text.Trim();
            string description = FreeTextBox1.Text.Trim();

            casestudy objcasestudy = new casestudy();
            COperationStatus os = new COperationStatus();

            objcasestudy.id = id;
            objcasestudy.title = title;
            objcasestudy.description = description;
            objcasestudy.filepath = filepath;
            objcasestudy.isfileactive = isfile;

            os = CCasestudy.Instance().UpdateCasestudy(objcasestudy);

            if (os.Success == true)
            {
                txtName.Text = string.Empty;
                FreeTextBox1.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }
        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {
            string filepath = "";

            Boolean isfile =false;

            if (fldtndrfile.HasFile)
            {
                string filename = Path.GetFileName(fldtndrfile.PostedFile.FileName);

                string file = "casestudies/" + filename;

                fldtndrfile.SaveAs(Server.MapPath("~/adminpanel/casestudies/" + filename));

                filepath = file;

                isfile = true;
            }
            else
            {
                filepath = "n/a";

                isfile = false;
            }

            string name = txtHeadline.Text.Trim();
            string description = txtdescrption.Text.Trim();


            casestudy objcasestudy = new casestudy();
            COperationStatus os = new COperationStatus();

            objcasestudy.title = name;
            objcasestudy.description = description;
            objcasestudy.filepath = filepath;
            objcasestudy.createdate = DateTime.Now;
            objcasestudy.isactive = true;
            objcasestudy.flag = flag;
            objcasestudy.isfileactive = isfile;

            os = CCasestudy.Instance().InsertCasestudy(objcasestudy);

            if (os.Success == true)
            {
                txtHeadline.Text = string.Empty;
                txtdescrption.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + os.Message + "');");
                sb.Append("$('#addModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }

        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;

            casestudy objcasestudy = new casestudy();
            COperationStatus os = new COperationStatus();

            os = CCasestudy.Instance().DeleteCasestudy(Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        protected void grdvwcasestudy_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwcasestudy.PageIndex = e.NewPageIndex;
            grdvwcasestudy.DataBind();
        }
    }
}