﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class subscribedusers : System.Web.UI.Page
    {

        IList<subscribe> subscribedata;

        string flag = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }

            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                subscribedata = CEvent.Instance().GetSubscribeData();

                //Bind the fetched data to gridview
                if (subscribedata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(subscribedata);

                    grdvwcorporate.DataSource = detailTable;
                    grdvwcorporate.DataBind();
                }
                else
                {
                    grdvwcorporate.DataSource = null;
                    grdvwcorporate.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        protected void grdvwcorporate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwcorporate.PageIndex = e.NewPageIndex;
            grdvwcorporate.DataBind();
        }


        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

    }
}