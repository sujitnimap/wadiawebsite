﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class CMERegMasterList : System.Web.UI.Page
    {
        IList<CMERegistrationMaster> cmeregistrationmasterdata;
        string flag = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                cmeregistrationmasterdata = CCMERegistrationMaster.Instance().GetCMERegistrationMasterData(flag);

                //Bind the fetched data to gridview
                if (cmeregistrationmasterdata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(cmeregistrationmasterdata);

                    grdCMERegMaster.DataSource = detailTable;
                    grdCMERegMaster.DataBind();
                }
                else
                {
                    grdCMERegMaster.DataSource = null;
                    grdCMERegMaster.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdCMERegMaster_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("editRecord"))
            {
                cmeregistrationmasterdata = CCMERegistrationMaster.Instance().GetCMERegistrationMasterData(flag);

                GridViewRow gvrow = grdCMERegMaster.Rows[index];

                Int32 id = Convert.ToInt32(grdCMERegMaster.DataKeys[index].Value.ToString());

                IList<CMERegistrationMaster> data = cmeregistrationmasterdata.Where(p => p.Id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                lblid.Text = id.ToString();
                ddlRegFeesCatEdit.SelectedValue = (detailTable.Rows[0]["RegFeesCategory"]).ToString();
                txtLastAddateEdit.Text = (detailTable.Rows[0]["LastRegDate"]).ToString();
                txtCounsultantFeesEdit.Text = (detailTable.Rows[0]["ConsultantFees"]).ToString();
                txtPGStudentFeesEdit.Text = (detailTable.Rows[0]["PgStudentFees"]).ToString();
                lblResult.Visible = false;


                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdCMERegMaster.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(lblid.Text.ToString());
            CMERegistrationMaster objCMERegistrationMaster = new CMERegistrationMaster();
            COperationStatus os = new COperationStatus();

            objCMERegistrationMaster.Id = id;
            objCMERegistrationMaster.RegFeesCategory = ddlRegFeesCatEdit.SelectedValue.ToString();
            objCMERegistrationMaster.LastRegDate = Convert.ToDateTime(txtLastAddateEdit.Text.ToString());
            objCMERegistrationMaster.ConsultantFees = txtCounsultantFeesEdit.Text.ToString();
            objCMERegistrationMaster.PgStudentFees = txtPGStudentFeesEdit.Text.ToString();
            objCMERegistrationMaster.IsActive = true;
            objCMERegistrationMaster.Flag = flag;
            objCMERegistrationMaster.LastModify = DateTime.Now;

            os = CCMERegistrationMaster.Instance().UpdateCMERegistrationMaster(objCMERegistrationMaster);

            if (os.Success == true)
            {
                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnCMERegUser_Click(object sender, EventArgs e)
        {
            Response.Redirect("cme-list.aspx");
        }

        protected void clear()
        {
            ddlRegFeesCat.SelectedIndex = 0;
            txtLastAddate.Text = "";
            txtCounsultantFees.Text = "";
            txtPGStudentFees.Text = "";
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {
            CMERegistrationMaster objCMERegistrationMaster = new CMERegistrationMaster();
            COperationStatus os = new COperationStatus();

            objCMERegistrationMaster.RegFeesCategory = ddlRegFeesCat.SelectedValue.ToString();
            objCMERegistrationMaster.LastRegDate = Convert.ToDateTime(txtLastAddate.Text.ToString());
            objCMERegistrationMaster.ConsultantFees = txtCounsultantFees.Text.ToString();
            objCMERegistrationMaster.PgStudentFees = txtPGStudentFees.Text.ToString();
            objCMERegistrationMaster.IsActive = true;
            objCMERegistrationMaster.Flag = flag;
            objCMERegistrationMaster.LastModify = DateTime.Now;

            os = CCMERegistrationMaster.Instance().InsertCMERegistrationMaster(objCMERegistrationMaster);

            if (os.Success == true)
            {
                clear();
                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + os.Message + "');");
                sb.Append("$('#addModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;
            CMERegistrationMaster objevent = new CMERegistrationMaster();
            COperationStatus os = new COperationStatus();
            os = CCMERegistrationMaster.Instance().DeleteCMERegistrationMaster(Convert.ToInt32(id));
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        protected void grdCMERegMaster_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdCMERegMaster.PageIndex = e.NewPageIndex;
            grdCMERegMaster.DataBind();
        }
    }
}