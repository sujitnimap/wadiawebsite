﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Abstract;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblerror.Visible = false;
            lblinfo.Visible = false;

            if (Session["adminid"] != null)
            {
                Response.Redirect("event.aspx");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            adminuser objCM = CAdminUser.Instance().fn_checkAdminLogin(txtname.Text.Trim(), txtpassword.Text.Trim());

            if (txtname.Text != "" && txtpassword.Text != "")
            {
                if (objCM != null)
                {
                    Session.Add("admin", txtname.Text);
                    Session.Add("adminid", objCM.adminuser_id);
                    if (rbtnchild.Checked == true)
                    {
                        Session.Add("flag", "c");
                    }
                    else
                    {
                        Session.Add("flag", "w");
                    }
                    Response.Redirect("event.aspx");
                }
                else
                {
                    lblerror.Text = "Login Incorrect, please enter valid login information";
                    lblerror.Visible = true;
                }
            }
            else
            {
                lblerror.Text = "Login Incorrect, please enter valid login information";
                lblerror.Visible = true;
            }
        }
    }
}