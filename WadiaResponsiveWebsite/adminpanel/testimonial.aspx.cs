﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class testimonialnew : System.Web.UI.Page
    {
        IList<testimonial> testimonialdata;
        string flag = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }


            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                testimonialdata = CTestimonial.Instance().GetTestimonialData(flag);

                //Bind the fetched data to gridview
                if (testimonialdata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(testimonialdata);

                    grdvwtestimonial.DataSource = detailTable;
                    grdvwtestimonial.DataBind();
                }
                else
                {
                    grdvwtestimonial.DataSource = null;
                    grdvwtestimonial.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwtestimonial_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                Int32 id = Convert.ToInt32(grdvwtestimonial.DataKeys[index].Value.ToString());

                testimonialdata = CTestimonial.Instance().GetTestimonialData(flag);
                IList<testimonial> data = testimonialdata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))
            {
                GridViewRow gvrow = grdvwtestimonial.Rows[index];

                Int32 id = Convert.ToInt32(grdvwtestimonial.DataKeys[index].Value.ToString());

                testimonialdata = CTestimonial.Instance().GetTestimonialData(flag);
                IList<testimonial> data = testimonialdata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                lblid.Text = id.ToString();
                txttestimlnm.Text = (detailTable.Rows[0]["testimonialnm"]).ToString();
                FreeTextBox1.Text = (detailTable.Rows[0]["testimonialdescrp"]).ToString();
                txtdesignt.Text = (detailTable.Rows[0]["designation"]).ToString();
                txtcreate.Text = (detailTable.Rows[0]["createby"]).ToString();
                ddleditcategory.SelectedIndex = Convert.ToInt32(detailTable.Rows[0]["categoryid"]);
                txteditdate.Text = (detailTable.Rows[0]["createdate"]).ToString();

                lblResult.Visible = false;

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwtestimonial.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(lblid.Text.ToString());
            string name = txttestimlnm.Text.Trim();
            string date =txteditdate.Text.Trim();
            string description = FreeTextBox1.Text.Trim();
            string designation = txtdesignt.Text.Trim();
            string createby = txtcreate.Text.Trim();
            string categorynm = ddleditcategory.SelectedItem.Text;
            testimonial objtestimonial = new testimonial();
            COperationStatus os = new COperationStatus();


            if (flduploadupdateimg.HasFile == true)
            {

                Random rd = new Random();
                int no = rd.Next(1, 99999);

                //Get Filename from fileupload control
                string filename = Path.GetFileName(flduploadupdateimg.PostedFile.FileName);
                //string file = string.Concat(filename, Convert.ToString(no));

                //Save images into Images folder
                flduploadupdateimg.SaveAs(Server.MapPath("testimonialsimg/" + filename));

                objtestimonial.imageurl = "testimonialsimg/" + filename;

            }
            else
            {
                objtestimonial.imageurl = "n/a";
            }

            objtestimonial.id = id;
            objtestimonial.testimonialnm = name;
            objtestimonial.testimonialdescrp = description;
            objtestimonial.designation = designation;
            objtestimonial.createby = createby;
            objtestimonial.categorynm = categorynm;
            objtestimonial.categoryid = ddleditcategory.SelectedIndex;
            if (date == "")
            {
                objtestimonial.createdate = DateTime.Now;
            }
            else
            {
                objtestimonial.createdate = Convert.ToDateTime(date);
            }
            

            os = CTestimonial.Instance().UpdateTestimonial(objtestimonial);

            if (os.Success == true)
            {
                txttestimlnm.Text = string.Empty;
                FreeTextBox1.Text = string.Empty;
                txtcreate.Text = string.Empty;
                txtdesignt.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }
        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {
            string name = txttstmnlnm.Text.Trim();
            string description = txtdescrption.Text.Trim();
            string createby = txtcreateby.Text.Trim();
            string designation = txtdesignation.Text.Trim();
            string date = txtadddate.Text.Trim();

            testimonial objtest = new testimonial();
            COperationStatus os = new COperationStatus();

            if (flduploadaddimg.HasFile == true)
            {

                Random rd = new Random();
                int no = rd.Next(1, 99999);

                //Get Filename from fileupload control
                string filename = Path.GetFileName(flduploadaddimg.PostedFile.FileName);

                //Save images into Images folder
                flduploadaddimg.SaveAs(Server.MapPath("testimonialsimg/" + filename));

                objtest.imageurl = "testimonialsimg/" + filename;

            }
            else
            {
                objtest.imageurl = "n/a";
            }


            objtest.testimonialnm = name;
            objtest.testimonialdescrp = description;
            objtest.createby = createby;
            objtest.designation = designation;
            if (date == "")
            {
                objtest.createdate = DateTime.Now;
            }
            else
            {
                objtest.createdate = Convert.ToDateTime(date);
            }            
            objtest.isactive = true;
            objtest.viewflag = false;
            objtest.flag = flag;
            objtest.categorynm = ddladdcategory.SelectedItem.Text;
            objtest.categoryid = ddladdcategory.SelectedIndex;

            os = CTestimonial.Instance().InsertTestimonial(objtest);

            if (os.Success == true)
            {
                txttstmnlnm.Text = string.Empty;
                txtdescrption.Text = string.Empty;
                txtcreateby.Text = string.Empty;
                txtdesignation.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + os.Message + "');");
                sb.Append("$('#addModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;

            testimonial objevent = new testimonial();
            COperationStatus os = new COperationStatus();

            os = CTestimonial.Instance().DeleteTestimonial(Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        protected void grdvwtestimonial_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwtestimonial.PageIndex = e.NewPageIndex;
            grdvwtestimonial.DataBind();
        }

    }
}