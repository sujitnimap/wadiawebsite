﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminpanel/admin.Master" AutoEventWireup="true" CodeBehind="cme-list.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel.cme_list" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register TagName="Error" Src="~/usercontrols/Error.ascx" TagPrefix="userControlError" %>
<%@ Register TagName="Info" Src="~/usercontrols/Info.ascx" TagPrefix="userControlInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large;">CME Registered User
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <userControlInfo:Info ID="info" runat="server" Visible="false" />
                    <userControlError:Error ID="error" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>

                    <div style="padding-left: 20px">
                        <%--<asp:Button ID="btnAdd" runat="server" Text="Add New" CssClass="btn btn-info" OnClick="btnAdd_Click" />--%>
                        <asp:Button ID="btnExport" runat="server" Text="Export To Excel" CssClass="btn btn-info" OnClick="btnExport_Click" />
                    </div>

                    <div align="left" style="width: 100%; padding-left: 20px">
                    </div>
                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdCMERegMaster" runat="server" Width="100%" HorizontalAlign="Center"
                                    OnRowCommand="grdCMERegMaster_RowCommand" PageSize="20" AutoGenerateColumns="False" AllowPaging="True"
                                    DataKeyNames="CMEId" CssClass="table table-hover table-striped" OnPageIndexChanging="grdCMERegMaster_PageIndexChanging">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr No.">
                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reg Category">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblRegCat" runat="server" Text='<%#Bind("RegCategory") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblRegFeesCategory" runat="server" Text='<%#Bind("Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Designation">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblDesignation" runat="server" Text='<%#Bind("Designation") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Speciality">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblSpeciality" runat="server" Text='<%#Bind("Speciality") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Institution">
                                            <ItemStyle Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstitution" runat="server" Text='<%#Bind("Institution") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                            <ItemStyle Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%#Bind("Address") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email ID">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmailID" runat="server" Text='<%#Bind("EmailID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile No">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblMobNo" runat="server" Text='<%#Bind("MobNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MCIRegNo">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblMCIRegNo" runat="server" Text='<%#Bind("MCIRegNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="StateCouncilNo">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblStateCouncilNo" runat="server" Text='<%#Bind("StateCouncilNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="YearOfTraining">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblYearOfTraining" runat="server" Text='<%#Bind("YearOfTraining") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
