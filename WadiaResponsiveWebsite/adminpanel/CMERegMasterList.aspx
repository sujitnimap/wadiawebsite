﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminpanel/admin.Master" AutoEventWireup="true" CodeBehind="CMERegMasterList.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel.CMERegMasterList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register TagName="Error" Src="~/usercontrols/Error.ascx" TagPrefix="userControlError" %>
<%@ Register TagName="Info" Src="~/usercontrols/Info.ascx" TagPrefix="userControlInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>

        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large;">CME Registration Master
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <userControlInfo:Info ID="info" runat="server" Visible="false" />
                    <userControlError:Error ID="error" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>
                            <div style="padding-left: 20px">
                                <asp:Button ID="btnAdd" runat="server" Text="Add New" CssClass="btn btn-info" OnClick="btnAdd_Click" />
                                <asp:Button ID="btnCMERegUser" runat="server" Text="CME Registered User" CssClass="btn btn-info" OnClick="btnCMERegUser_Click" />
                            </div>

                            <div align="left" style="width: 100%; padding-left: 20px">
                            </div>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdCMERegMaster" runat="server" Width="100%" HorizontalAlign="Center"
                                    OnRowCommand="grdCMERegMaster_RowCommand" PageSize="5" AutoGenerateColumns="False" AllowPaging="True"
                                    DataKeyNames="Id" CssClass="table table-hover table-striped" OnPageIndexChanging="grdCMERegMaster_PageIndexChanging">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr No.">
                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fees Category">
                                            <ItemStyle Width="20%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblRegFeesCategory" runat="server" Text='<%#Bind("RegFeesCategory") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Date">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastRegDate" runat="server" Text='<%#Bind("LastRegDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Consultant Fees">
                                            <ItemStyle Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbltype" runat="server" Text='<%#Bind("ConsultantFees") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pg Student Fees">
                                            <ItemStyle Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbltype" runat="server" Text='<%#Bind("PgStudentFees") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Edit" HeaderText="Edit Record">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                        <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Delete" HeaderText="Delete Record">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    <!-- Edit Modal Starts here -->
    <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="editModalLabel">Edit Record</h3>
        </div>
        <asp:UpdatePanel ID="upEdit" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <td>Id : </td>
                            <td>
                                <asp:Label ID="lblid" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Reg Fees Category : </td>
                            <td>
                                <asp:DropDownList ID="ddlRegFeesCatEdit" runat="server" Width="150px" Enabled="false">
                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                    <asp:ListItem Value="Early Bird">Early Bird</asp:ListItem>
                                    <asp:ListItem Value="Regular">Regular</asp:ListItem>
                                    <asp:ListItem Value="Late">Late</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" ID="rfvRegFeesCatEdit" ValidationGroup="vgCMERegEdit" runat="server" ControlToValidate="ddlRegFeesCatEdit"
                                    ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Last Date : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtLastAddateEdit" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txtLastAddate_calenderEdit" Format="MM/dd/yyyy" runat="server" TargetControlID="txtLastAddateEdit">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="refLastAddateEdit" ValidationGroup="vgCMERegEdit" ControlToValidate="txtLastAddateEdit" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Counsultant Fees : </td>
                            <td>
                                <asp:TextBox ID="txtCounsultantFeesEdit" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvConsultantFeesEdit" ValidationGroup="vgCMERegEdit" ControlToValidate="txtCounsultantFeesEdit" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td>PG Student Fees : </td>
                            <td>
                                <asp:TextBox ID="txtPGStudentFeesEdit" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPGStudentFeesEdit" ValidationGroup="vgCMERegEdit" ControlToValidate="txtPGStudentFeesEdit" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <asp:Label ID="lblResult" Visible="false" runat="server"></asp:Label>
                    <asp:Button ID="btnSave" runat="server" Text="Update" ValidationGroup="vgCMERegEdit" CssClass="btn btn-info" OnClick="btnSave_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="grdCMERegMaster" EventName="RowCommand" />
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!-- Edit Modal Ends here -->
    <!-- Add Record Modal Starts here-->
    <div id="addModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="addModalLabel">Add New Record</h3>
        </div>
        <asp:UpdatePanel ID="upAdd" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <td>Reg Fees Category : </td>
                            <td>
                                <asp:DropDownList ID="ddlRegFeesCat" runat="server" Width="150px">
                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                    <asp:ListItem Value="Early Bird">Early Bird</asp:ListItem>
                                    <asp:ListItem Value="Regular">Regular</asp:ListItem>
                                    <asp:ListItem Value="Late">Late</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" ID="rfvRegFeesCat" ValidationGroup="vgCMEReg" runat="server" ControlToValidate="ddlRegFeesCat"
                                    ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Last Date : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtLastAddate" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txtLastAddate_calender" Format="MM/dd/yyyy" runat="server" TargetControlID="txtLastAddate">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="refLastAddate" ValidationGroup="vgCMEReg" ControlToValidate="txtLastAddate" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Counsultant Fees : </td>
                            <td>
                                <asp:TextBox ID="txtCounsultantFees" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvConsultantFees" ValidationGroup="vgCMEReg" ControlToValidate="txtCounsultantFees" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td>PG Student Fees : </td>
                            <td>
                                <asp:TextBox ID="txtPGStudentFees" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPGStudentFees" ValidationGroup="vgCMEReg" ControlToValidate="txtPGStudentFees" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnAddRecord" runat="server" Text="Add" ValidationGroup="vgCMEReg" CssClass="btn btn-info" OnClick="btnAddRecord_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAddRecord" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!--Add Record Modal Ends here-->
    <!-- Delete Record Modal Starts here-->
    <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="delModalLabel">Delete Record</h3>
        </div>
        <asp:UpdatePanel ID="upDel" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    Are you sure you want to delete the record?
                            <asp:HiddenField ID="hfCode" runat="server" />
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelete_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!--Delete Record Modal Ends here -->

</asp:Content>
