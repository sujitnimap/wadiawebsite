﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class categry : System.Web.UI.Page
    {
        IList<category> categorydata;

        string flag = "";
        string type = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }

           

            if (!IsPostBack)
            {
                BindGrid();
            }


        }

        public void BindGrid()
        {
            try
            {

                if (Convert.ToString(Session["type"]) == "Video")
                {
                    ddltype.SelectedIndex = 1;
                }
                else if (Convert.ToString(Session["type"]) == "Image")
                {
                    ddltype.SelectedIndex = 0;
                }
                else
                {
                    ddltype.SelectedIndex = 0;
                }


                type = ddltype.SelectedItem.Text;
                //Fetch data from mysql database
                categorydata = CCategory.Instance().GetCategoryData(flag, type);

                //Bind the fetched data to gridview
                if (categorydata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(categorydata);

                    grdvwcategory.DataSource = detailTable;
                    grdvwcategory.DataBind();
                }
                else
                {
                    grdvwcategory.DataSource = null;
                    grdvwcategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwcategory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                type = ddltype.SelectedItem.Text;

                categorydata = CCategory.Instance().GetCategoryData(flag, type);

                Int32 id = Convert.ToInt32(grdvwcategory.DataKeys[index].Value.ToString());

                IList<category> data = categorydata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))
            {
                type = ddltype.SelectedItem.Text;

                categorydata = CCategory.Instance().GetCategoryData(flag,type);

                GridViewRow gvrow = grdvwcategory.Rows[index];

                Int32 id = Convert.ToInt32(grdvwcategory.DataKeys[index].Value.ToString());

                IList<category> data = categorydata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                lblid.Text = id.ToString();
                txtName.Text = (detailTable.Rows[0]["categorynm"]).ToString();
                //FreeTextBox1.Text = (detailTable.Rows[0]["description"]).ToString();
                lblResult.Visible = false;
                txteditdate.Text = (detailTable.Rows[0]["createdate"]).ToString();
                ddlupdatecattype.SelectedValue = (detailTable.Rows[0]["type"]).ToString();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwcategory.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {


            if (Convert.ToString(Session["type"]) == "Video")
            {
                ddladdcattype.SelectedIndex = 1;
            }
            else if (Convert.ToString(Session["type"]) == "Image")
            {
                ddladdcattype.SelectedIndex = 0;
            }
            else
            {
                ddladdcattype.SelectedIndex = 0;
            }


            int id = Convert.ToInt32(lblid.Text.ToString());
            string name = txtName.Text.Trim();
            string date = txteditdate.Text.Trim();
            //string description = FreeTextBox1.Text.Trim();

            category objcategory = new category();
            COperationStatus os = new COperationStatus();

            if (AsyncFldupdate.HasFile == true)
            {

                ////Get Filename from fileupload control
                //string filename = Path.GetFileName(AsyncFldupdate.PostedFile.FileName);
                ////string file = string.Concat(filename, Convert.ToString(no));
                ////Save images into Images folder
                //AsyncFldupdate.SaveAs(Server.MapPath("thumbnail/" + filename));


                string extension = Path.GetExtension(AsyncFldupdate.FileName);
                string filename = Path.GetFileNameWithoutExtension(AsyncFldupdate.FileName);
                string uid = Guid.NewGuid().ToString();

                string fileLocation = string.Format("{0}\\{1}{2}", Server.MapPath("categorythubmnail/"), filename + uid, extension);

                AsyncFldupdate.SaveAs(fileLocation);

                objcategory.thumbnail = "categorythubmnail/" + filename + uid + extension;

            }
            else
            {
                objcategory.thumbnail = "n/a";
            }

            objcategory.id = id;
            objcategory.categorynm = name;
            objcategory.description = "";
            objcategory.type = ddladdcattype.SelectedItem.Text;
            objcategory.createdate = Convert.ToDateTime(date);

            os = CCategory.Instance().Updatecategory(objcategory);

            if (os.Success == true)
            {
                txtName.Text = string.Empty;
                //FreeTextBox1.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }
        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {


            if (Convert.ToString(Session["type"]) == "Video")
            {
                ddladdcattype.SelectedIndex = 1;
            }
            else if (Convert.ToString(Session["type"]) == "Image")
            {
                ddladdcattype.SelectedIndex = 0;
            }
            else
            {
                ddladdcattype.SelectedIndex = 0;
            }


            string name = txttitle.Text.Trim();
            //string description = txtdescrption.Text.Trim();

            category objtest = new category();
            COperationStatus os = new COperationStatus();

            if (AsyncFldadd.HasFile == true)
            {
                ////Get Filename from fileupload control
                //string filename = Path.GetFileName(AsyncFldadd.PostedFile.FileName);
                ////string file = string.Concat(filename, Convert.ToString(no));
                ////Save images into Images folder
                //AsyncFldadd.SaveAs(Server.MapPath("gallary/thumbnail/" + filename));
                //objtest.thumbnail = "gallary/thumbnail/" + filename;

                string extension = Path.GetExtension(AsyncFldadd.FileName);
                string filename = Path.GetFileNameWithoutExtension(AsyncFldadd.FileName);
                string id = Guid.NewGuid().ToString();

                string fileLocation = string.Format("{0}/{1}{2}", Server.MapPath("categorythubmnail/"), filename + id, extension);

                AsyncFldadd.SaveAs(fileLocation);

                objtest.thumbnail = "categorythubmnail/" + filename + id + extension;

            }
            else
            {
                objtest.thumbnail = "n/a";
            }

            objtest.categorynm = name;
            objtest.description = "";
            objtest.createdate = DateTime.Now;
            objtest.isactive = true;
            objtest.type = ddladdcattype.SelectedItem.Text;
            objtest.flag = flag;

            os = CCategory.Instance().Insertcategory(objtest);

            if (os.Success == true)
            {
                txttitle.Text = string.Empty;
                //txtdescrption.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + os.Message + "');");
                sb.Append("$('#addModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;

            category objevent = new category();
            COperationStatus os = new COperationStatus();

            os = CCategory.Instance().Deletecategory(Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        protected void grdvwcategory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwcategory.PageIndex = e.NewPageIndex;
            grdvwcategory.DataBind();
        }

        protected void ddltype_SelectedIndexChanged(object sender, EventArgs e)
        {

            Session["type"] = ddltype.SelectedItem.Text;

            BindGrid();

        }

        protected void ddladdcattype_SelectedIndexChanged(object sender, EventArgs e)
        {

            Session["type"] = ddladdcattype.SelectedItem.Text;

        }

        protected void ddlupdatecattype_SelectedIndexChanged(object sender, EventArgs e)
        {

            Session["type"] = ddlupdatecattype.SelectedItem.Text;

        }


    }
} 