﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class corporate : System.Web.UI.Page
    {
        IList<corporategiving> corporatedata;

        string flag = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }

            if (!IsPostBack)
            {
                BindGrid();
            }

        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                corporatedata = CCorporate.Instance().GetCorporateData(flag);

                //Bind the fetched data to gridview
                if (corporatedata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(corporatedata);

                    grdvwcorporate.DataSource = detailTable;
                    grdvwcorporate.DataBind();
                }
                else
                {
                    grdvwcorporate.DataSource = null;
                    grdvwcorporate.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwcorporate_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                corporatedata = CCorporate.Instance().GetCorporateData(flag);

                Int32 id = Convert.ToInt32(grdvwcorporate.DataKeys[index].Value.ToString());

                IList<corporategiving> data = corporatedata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))
            {
                corporatedata = CCorporate.Instance().GetCorporateData(flag);

                GridViewRow gvrow = grdvwcorporate.Rows[index];

                Int32 id = Convert.ToInt32(grdvwcorporate.DataKeys[index].Value.ToString());

                IList<corporategiving> data = corporatedata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                lblid.Text = id.ToString();
                FreeTextBox1.Text = (detailTable.Rows[0]["description"]).ToString();
                lblResult.Visible = false;

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwcorporate.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            string logofile = "n/a";

            if (AsyncFldEdit.HasFile == true)
            {
                //logofile = Path.GetFileName(AsyncFldEdit.FileName);
                //AsyncFldEdit.SaveAs(Server.MapPath("corporatelogo/") + logofile);


                string extension = Path.GetExtension(AsyncFldEdit.FileName);
                string filename = Path.GetFileNameWithoutExtension(AsyncFldEdit.FileName);
                string uid = Guid.NewGuid().ToString();

                string fileLocation = string.Format("{0}\\{1}{2}", Server.MapPath("corporatelogo/"), filename + uid, extension);

                AsyncFldEdit.SaveAs(fileLocation);

                logofile = "corporatelogo/" + filename + uid + extension;
            }
            


            int id = Convert.ToInt32(lblid.Text.ToString());

            string description = FreeTextBox1.Text.Trim();

            corporategiving objcorporate = new corporategiving();
            COperationStatus os = new COperationStatus();

            objcorporate.id = id;
            objcorporate.description = description;

            if (logofile == "n/a")
            {
                objcorporate.logoimg = "n/a";
            }
            else
            {
                objcorporate.logoimg = logofile;
            }

            os = CCorporate.Instance().Updatecorporate(objcorporate);

            if (os.Success == true)
            {

                FreeTextBox1.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }
        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {

            string logofile = "n/a";

            if (AsyncFldadd.HasFile == true)
            {
                //logofile = Path.GetFileName(AsyncFldadd.FileName);
                //AsyncFldadd.SaveAs(Server.MapPath("corporatelogo/") + logofile);


                string extension = Path.GetExtension(AsyncFldadd.FileName);
                string filename = Path.GetFileNameWithoutExtension(AsyncFldadd.FileName);
                string uid = Guid.NewGuid().ToString();

                string fileLocation = string.Format("{0}\\{1}{2}", Server.MapPath("corporatelogo/"), filename + uid, extension);

                AsyncFldadd.SaveAs(fileLocation);

                logofile = "corporatelogo/" + filename + uid + extension;
            }

            string description = txtdescrption.Text.Trim();

            corporategiving objcorporate = new corporategiving();

            COperationStatus os = new COperationStatus();

            objcorporate.description = description;
            objcorporate.createdate = DateTime.Now;
            objcorporate.isactive = true;
            objcorporate.flag = flag;
            if (logofile == "n/a")
            {
                objcorporate.logoimg = logofile;
            }
            else
            {
                objcorporate.logoimg = logofile;
            }

            os = CCorporate.Instance().Insertcorporate(objcorporate);

            if (os.Success == true)
            {

                txtdescrption.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + os.Message + "');");
                sb.Append("$('#addModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;

            category objevent = new category();
            COperationStatus os = new COperationStatus();

            os = CCorporate.Instance().Deletecorporate(Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }


        protected void grdvwcorporate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwcorporate.PageIndex = e.NewPageIndex;
            grdvwcorporate.DataBind();
        }


    }
}