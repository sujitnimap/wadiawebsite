﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WadiaResponsiveWebsite.adminpanel
{
		public partial class AdultParticipantOfMarathon : System.Web.UI.Page
		{


				SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ToString());
				string flag = "";

				protected void Page_Load(object sender, EventArgs e)
				{

						if (Convert.ToString(Session["flag"]) == "")
						{
								Response.Redirect("login.aspx");
						}
						{
								flag = Session["flag"].ToString();
						}

						if (!IsPostBack)
						{
								BindGrid();
						}

				}

				public void BindGrid()
				{
						try
						{
								//Fetch data from mysql database
							DataTable adultdata = new DataTable();

							SqlCommand cmd = new SqlCommand("GetSuccessMarathonAdultRegistration", conn);

								//DataTable childdata = new DataTable();

								//SqlCommand cmd = new SqlCommand("GetSuccessMarathonChildRegistration", conn);
								cmd.CommandType = CommandType.StoredProcedure;
								SqlDataAdapter da = new SqlDataAdapter(cmd);
								conn.Open();
								da.Fill(adultdata);
								conn.Close();

								//Bind the fetched data to gridview

								if (adultdata.Rows.Count > 0)
								{
									grdvwAdultOfMarathonInfo.DataSource = adultdata;                   
										grdvwAdultOfMarathonInfo.DataBind();
								}
								else
								{
										grdvwAdultOfMarathonInfo.DataSource = null;
										grdvwAdultOfMarathonInfo.DataBind();
								}
						}
						catch (Exception ex)
						{
								((Label)error.FindControl("lblError")).Text = ex.Message;
								error.Visible = true;
						}

				}

				protected void grdvwAdultOfMarathonInfo_PageIndexChanging(object sender, GridViewPageEventArgs e)
				{
						BindGrid();

						grdvwAdultOfMarathonInfo.PageIndex = e.NewPageIndex;
						grdvwAdultOfMarathonInfo.DataBind();
				}

				protected void btnExport_Click(object sender, EventArgs e)
				{
						Response.Clear();
						Response.Buffer = true;
						Response.AddHeader("content-disposition", "attachment;filename=AdultParticipantOfMarathon.xls");
						Response.Charset = "";
						Response.ContentType = "application/vnd.ms-excel";
						using (StringWriter sw = new StringWriter())
						{
								HtmlTextWriter hw = new HtmlTextWriter(sw);

								//To Export all pages
								grdvwAdultOfMarathonInfo.AllowPaging = false;
								this.BindGrid();

								grdvwAdultOfMarathonInfo.HeaderRow.BackColor = Color.White;
								foreach (TableCell cell in grdvwAdultOfMarathonInfo.HeaderRow.Cells)
								{
										cell.BackColor = grdvwAdultOfMarathonInfo.HeaderStyle.BackColor;
								}
								foreach (GridViewRow row in grdvwAdultOfMarathonInfo.Rows)
								{
										row.BackColor = Color.White;
										foreach (TableCell cell in row.Cells)
										{
												if (row.RowIndex % 2 == 0)
												{
														cell.BackColor = grdvwAdultOfMarathonInfo.AlternatingRowStyle.BackColor;
												}
												else
												{
														cell.BackColor = grdvwAdultOfMarathonInfo.RowStyle.BackColor;
												}
												//cell.CssClass = "textmode";
										}
								}

								grdvwAdultOfMarathonInfo.RenderControl(hw);

								//style to format numbers to string
								//string style = @"<style> .textmode { } </style>";
								// Response.Write(style);
								Response.Output.Write(sw.ToString());
								Response.Flush();
								Response.End();
						}
				}

				public override void VerifyRenderingInServerForm(Control control)
				{
						/* Verifies that the control is rendered */
				}

				protected void grdvwAdultOfMarathonInfo_DataBound(object sender, EventArgs e)
				{
						for (int i = grdvwAdultOfMarathonInfo.Rows.Count - 1; i > 0; i--)
						{
								GridViewRow row = grdvwAdultOfMarathonInfo.Rows[i];
								GridViewRow previousRow = grdvwAdultOfMarathonInfo.Rows[i - 1];
								for (int j = 0; j < row.Cells.Count; j++)
								{
										if (row.Cells[j].Text == previousRow.Cells[j].Text)
										{
												if (previousRow.Cells[j].RowSpan == 0)
												{
														if (row.Cells[j].RowSpan == 0)
														{
																previousRow.Cells[j].RowSpan += 2;
														}
														else
														{
																previousRow.Cells[j].RowSpan = row.Cells[j].RowSpan + 1;
														}
														row.Cells[j].Visible = false;
												}
										}
								}
						}
				}

        protected void grdvwAdultOfMarathonInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[5].Text !="")
                {
                    if (Convert.ToInt32(e.Row.Cells[5].Text) == 1)
                    {
                        string a = DataBinder.Eval(e.Row.DataItem, "TShirtSize1").ToString();
                        e.Row.Cells[6].Text = a;
                    }
                    else if (Convert.ToInt32(e.Row.Cells[5].Text) == 2)
                    {
                        string a = DataBinder.Eval(e.Row.DataItem, "TShirtSize1").ToString();
                        string b = DataBinder.Eval(e.Row.DataItem, "TShirtSize2").ToString();

                    }
                    else if (Convert.ToInt32(e.Row.Cells[5].Text) == 3)
                    {
                        string a = DataBinder.Eval(e.Row.DataItem, "TShirtSize1").ToString();
                        string b = DataBinder.Eval(e.Row.DataItem, "TShirtSize2").ToString();
                        string c = DataBinder.Eval(e.Row.DataItem, "TShirtSize3").ToString();
                    }
                    else if (Convert.ToInt32(e.Row.Cells[5].Text) == 4)
                    {
                        string a = DataBinder.Eval(e.Row.DataItem, "TShirtSize1").ToString();
                        string b = DataBinder.Eval(e.Row.DataItem, "TShirtSize2").ToString();
                        string c = DataBinder.Eval(e.Row.DataItem, "TShirtSize3").ToString();
                        string d = DataBinder.Eval(e.Row.DataItem, "TShirtSize4").ToString();
                    }
                    else if (Convert.ToInt32(e.Row.Cells[5].Text) == 5)
                    {
                        string a = DataBinder.Eval(e.Row.DataItem, "TShirtSize1").ToString();
                        string b = DataBinder.Eval(e.Row.DataItem, "TShirtSize2").ToString();
                        string c = DataBinder.Eval(e.Row.DataItem, "TShirtSize3").ToString();
                        string d = DataBinder.Eval(e.Row.DataItem, "TShirtSize4").ToString();
                        string g = DataBinder.Eval(e.Row.DataItem, "TShirtSize5").ToString();

                    }
                }
            }
        }
    }
}