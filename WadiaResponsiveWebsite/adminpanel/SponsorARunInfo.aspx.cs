﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL.Implementation;
using WadiaWebsiteDAL.Abstract;
using WadiaWebsiteDAL;
using System.Data;
using System.Reflection;
using System.IO;
using System.Drawing;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class SponsorARunInfo : System.Web.UI.Page
    {
        IList<sponsorARunForMarathon> sponsorARunForMarathonData;
        string flag = "";
        int conl = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }


            if (!IsPostBack)
            {
                BindGrid();
            }
        }


        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                sponsorARunForMarathonData = CSponsorARunForMarathon .Instance().GetSponsorARunForMarathonInfos();

                //Bind the fetched data to gridview
                if (sponsorARunForMarathonData.Count > 0)
                {
                    DataTable detailTable = ToDataTable(sponsorARunForMarathonData);

                    grdvwSponsorARunInfo .DataSource = detailTable;
                    grdvwSponsorARunInfo.DataBind();
                }
                else
                {
                    grdvwSponsorARunInfo.DataSource = null;
                    grdvwSponsorARunInfo.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;

            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }


        protected void grdvwSponsorARunInfo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);

            if (e.CommandName.Equals("detail"))
            {
                Int32 id = Convert.ToInt32(grdvwSponsorARunInfo.DataKeys[index].Value.ToString());

                sponsorARunForMarathonData = CSponsorARunForMarathon .Instance().GetSponsorARunForMarathonInfos ();


                IList<sponsorARunForMarathon> data = sponsorARunForMarathonData.Where(p => p.SponsorARunForMarathonID .Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                dlvSponsorARunInfo.DataSource = detailTable;
                dlvSponsorARunInfo.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }

        }

        protected void grdvwSponsorARunInfo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwSponsorARunInfo.PageIndex = e.NewPageIndex;
            grdvwSponsorARunInfo.DataBind();
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=SponsorInfo.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                grdvwSponsorARunInfo.AllowPaging = false;
                this.BindGrid();

                grdvwSponsorARunInfo.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in grdvwSponsorARunInfo.HeaderRow.Cells)
                {
                    cell.BackColor = grdvwSponsorARunInfo.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in grdvwSponsorARunInfo.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = grdvwSponsorARunInfo.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = grdvwSponsorARunInfo.RowStyle.BackColor;
                        }
                        //cell.CssClass = "textmode";
                    }
                }

                grdvwSponsorARunInfo.RenderControl(hw);

                //style to format numbers to string
                //string style = @"<style> .textmode { } </style>";
                // Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

    }
}