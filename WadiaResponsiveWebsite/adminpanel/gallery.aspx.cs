﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class gallary : System.Web.UI.Page
    {

        DataTable artgallarydata;

        string flag = "";

        static string type = "Image";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }

            if (!IsPostBack)
            {
                BindGrid();
                bindcategory();
            }

        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                artgallarydata = CArtgallary.Instance().GetArtgallarygridData(flag);

                //Bind the fetched data to gridview
               if (artgallarydata.Rows.Count > 0)
                {
                    //DataTable detailTable = ToDataTable(artgallarydata);

                    grdvwgallary.DataSource = artgallarydata;
                    grdvwgallary.DataBind();
                }
                else
                {
                    grdvwgallary.DataSource = null;
                    grdvwgallary.DataBind();
                }
            }
            catch (Exception ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public void bindcategory()
        {
            category c = new category();

            ddlcategory.DataSource = CCategory.Instance().GetCategoryData(flag, type);
            ddlcategory.DataBind();
            ddlcategory.Items.Insert(0, new ListItem("Select", "0"));

            ddleditcategory.DataSource = CCategory.Instance().GetCategoryData(flag, type);
            ddleditcategory.DataBind();
            ddleditcategory.Items.Insert(0, new ListItem("Select", "0"));

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwgallary_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                Int32 id = Convert.ToInt32(grdvwgallary.DataKeys[index].Value.ToString());

                artgallarydata = CArtgallary.Instance().GetArtgallaryData(id);


                DetailsView1.DataSource = artgallarydata;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))
            {
                GridViewRow gvrow = grdvwgallary.Rows[index];

                Int32 id = Convert.ToInt32(grdvwgallary.DataKeys[index].Value.ToString());

                artgallarydata = CArtgallary.Instance().GetArtgallaryData(id);


                lblid.Text = id.ToString();
                txtalbumName.Text = (artgallarydata.Rows[0]["albumname"]).ToString();
                ddleditcategory.SelectedValue = (artgallarydata.Rows[0]["catname"]).ToString();
                txtupdatedate.Text = (artgallarydata.Rows[0]["cratedate"]).ToString();
                FreeTextBox1.Text = (artgallarydata.Rows[0]["description"]).ToString();
                lblResult.Visible = false;

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwgallary.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {

            //---------------Image Upload Start----------//

            string thumfilename = "n/a";

            if (AsyncFileUpload1.HasFile == true )
            {
                //bigfilename = Path.GetFileName(AsyncFileUpload1.FileName);
                //AsyncFileUpload1.SaveAs(Server.MapPath("gallary/") + bigfilename);
                //thumfilename = Path.GetFileName(AsyncFileUpload2.FileName);
                //AsyncFileUpload2.SaveAs(Server.MapPath("gallary/") + thumfilename);

                string extension = Path.GetExtension(AsyncFileUpload1.FileName);
                string filename = Path.GetFileNameWithoutExtension(AsyncFileUpload1.FileName);
                string uid = Guid.NewGuid().ToString();

                string fileLocation = string.Format("{0}/{1}{2}", Server.MapPath("gallary/thumbnail/"), filename + uid, extension);

                AsyncFileUpload1.SaveAs(fileLocation);

                thumfilename = "gallary/thumbnail/" + filename + uid + extension;
            }

            //-------------Image Upload End----------//

            int id = Convert.ToInt32(lblid.Text.ToString());
            string name = txtalbumName.Text.Trim();
            string description = FreeTextBox1.Text.Trim();

            artgallary objartgallary = new artgallary();

            COperationStatus os = new COperationStatus();

            objartgallary.id = id;
            objartgallary.albumname = name;
            objartgallary.categorynm = Convert.ToInt32(ddleditcategory.SelectedValue);
            objartgallary.description = description;
            objartgallary.thumbnail = thumfilename;
            objartgallary.cratedate = Convert.ToDateTime(txtupdatedate.Text);
            

            os = CArtgallary.Instance().UpdateArtgallary(objartgallary);

            if (os.Success == true)
            {
                txtalbumName.Text = string.Empty;
                FreeTextBox1.Text = string.Empty;
                txtupdatedate.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }
        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {
            //---------------Image Upload Start----------//
            string bigfilename = "n/a";
            string thumfilename = "n/a";

            if (fldthumbnail.HasFile == true)
            {
                //bigfilename = Path.GetFileName(fldmainimg.FileName);
                //fldmainimg.SaveAs(Server.MapPath("gallary/") + bigfilename);                
                //thumfilename = Path.GetFileName(fldthumbnail.FileName);
                //fldthumbnail.SaveAs(Server.MapPath("gallary/") + thumfilename);

                string extension = Path.GetExtension(fldthumbnail.FileName);
                string filename = Path.GetFileNameWithoutExtension(fldthumbnail.FileName);
                string uid = Guid.NewGuid().ToString();

                string fileLocation = string.Format("{0}/{1}{2}", Server.MapPath("gallary/thumbnail/"), filename + uid, extension);

                fldthumbnail.SaveAs(fileLocation);

                thumfilename = "gallary/thumbnail/" + filename + uid + extension;
            }

            //-------------Image Upload End----------//

            string name = txtalbumnm.Text.Trim();
            string description = txtdescrption.Text.Trim();

            artgallary objartgallary = new artgallary();
            COperationStatus os = new COperationStatus();

            objartgallary.albumname = name;
            objartgallary.categorynm = Convert.ToInt32(ddlcategory.SelectedValue);
            objartgallary.description = description;
            objartgallary.imagenm = "";
            objartgallary.thumbnail = thumfilename;
            objartgallary.cratedate = Convert.ToDateTime(txtadddate.Text);
            objartgallary.isactive = true;
            objartgallary.flag = flag;

            os = CArtgallary.Instance().InsertArtgallary(objartgallary);

            if (os.Success == true)
            {
                txtalbumnm.Text = string.Empty;
                txtdescrption.Text = string.Empty;
                txtadddate.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + os.Message + "');");
                sb.Append("$('#addModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;

            category objevent = new category();
            COperationStatus os = new COperationStatus();

            os = CArtgallary.Instance().DeleteArtgallary (Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        protected void grdvwgallary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwgallary.PageIndex = e.NewPageIndex;
            grdvwgallary.DataBind();
        }

    }
}