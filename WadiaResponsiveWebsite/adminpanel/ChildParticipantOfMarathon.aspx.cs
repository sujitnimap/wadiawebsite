﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WadiaResponsiveWebsite.adminpanel
{
		public partial class ChildParticipantOfMarathon : System.Web.UI.Page
		{

				SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ToString());
				string flag = "";

				protected void Page_Load(object sender, EventArgs e)
				{
						 if (Convert.ToString(Session["flag"]) == "")
						{
								Response.Redirect("login.aspx");
						}
						{
								flag = Session["flag"].ToString();
						}

						if (!IsPostBack)
						{
								BindGrid();
						}
				}

				public void BindGrid()
				{
						try
						{
								//Fetch data from mysql database

								//DataTable adultdata = new DataTable();

								//SqlCommand cmd = new SqlCommand("GetSuccessMarathonAdultRegistration", conn);
							DataTable childdata = new DataTable();

							SqlCommand cmd = new SqlCommand("GetSuccessMarathonChildRegistration", conn);

								cmd.CommandType = CommandType.StoredProcedure;
								SqlDataAdapter da = new SqlDataAdapter(cmd);
								conn.Open();
								da.Fill(childdata);
								conn.Close();

								//Bind the fetched data to gridview
								if (childdata.Rows.Count > 0)
								{
									grdvwChildOfMarathonInfo.DataSource = childdata;
										grdvwChildOfMarathonInfo.DataBind();
								}
								else
								{
										grdvwChildOfMarathonInfo.DataSource = null;
										grdvwChildOfMarathonInfo.DataBind();
								}
						}
						catch (Exception ex)
						{
								((Label)error.FindControl("lblError")).Text = ex.Message;
								error.Visible = true;
						}
				}

				protected void grdvwChildOfMarathonInfo_PageIndexChanging(object sender, GridViewPageEventArgs e)
				{
						BindGrid();

						grdvwChildOfMarathonInfo.PageIndex = e.NewPageIndex;
						grdvwChildOfMarathonInfo.DataBind();
				}


				protected void btnExport_Click(object sender, EventArgs e)
				{
						Response.Clear();
						Response.Buffer = true;
						Response.AddHeader("content-disposition", "attachment;filename=ChildParticipantOfMarathon.xls");
						Response.Charset = "";
						Response.ContentType = "application/vnd.ms-excel";
						using (StringWriter sw = new StringWriter())
						{
								HtmlTextWriter hw = new HtmlTextWriter(sw);

								//To Export all pages
								grdvwChildOfMarathonInfo.AllowPaging = false;
								this.BindGrid();

								grdvwChildOfMarathonInfo.HeaderRow.BackColor = Color.White;
								foreach (TableCell cell in grdvwChildOfMarathonInfo.HeaderRow.Cells)
								{
										cell.BackColor = grdvwChildOfMarathonInfo.HeaderStyle.BackColor;
								}
								foreach (GridViewRow row in grdvwChildOfMarathonInfo.Rows)
								{
										row.BackColor = Color.White;
										foreach (TableCell cell in row.Cells)
										{
												if (row.RowIndex % 2 == 0)
												{
														cell.BackColor = grdvwChildOfMarathonInfo.AlternatingRowStyle.BackColor;
												}
												else
												{
														cell.BackColor = grdvwChildOfMarathonInfo.RowStyle.BackColor;
												}
												//cell.CssClass = "textmode";
										}
								}

								grdvwChildOfMarathonInfo.RenderControl(hw);

								//style to format numbers to string
								//string style = @"<style> .textmode { } </style>";
								// Response.Write(style);
								Response.Output.Write(sw.ToString());
								Response.Flush();
								Response.End();
						}
				}

				public override void VerifyRenderingInServerForm(Control control)
				{
						/* Verifies that the control is rendered */
				}

				protected void grdvwChildOfMarathonInfo_DataBound(object sender, EventArgs e)
				{
						for (int i = grdvwChildOfMarathonInfo.Rows.Count - 1; i > 0; i--)
						{
								GridViewRow row = grdvwChildOfMarathonInfo.Rows[i];
								GridViewRow previousRow = grdvwChildOfMarathonInfo.Rows[i - 1];
								for (int j = 0; j < row.Cells.Count; j++)
								{
										if (row.Cells[j].Text == previousRow.Cells[j].Text)
										{
												if (previousRow.Cells[j].RowSpan == 0)
												{
														if (row.Cells[j].RowSpan == 0)
														{
																previousRow.Cells[j].RowSpan += 2;
														}
														else
														{
																previousRow.Cells[j].RowSpan = row.Cells[j].RowSpan + 1;
														}
														row.Cells[j].Visible = false;
												}
										}
								}
						}
				}


		}
}