﻿<%@ Page Title="Reproductive Infertility Payment Info" EnableEventValidation="false" Language="C#" MasterPageFile="~/adminpanel/admin.Master" AutoEventWireup="true" CodeBehind="ReproductiveInfertilityPaymentInfo.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel.ReproductiveInfertilityPaymentInfo" %>

<%@ Register TagName="Error" Src="~/usercontrols/Error.ascx" TagPrefix="userControlError" %>
<%@ Register TagName="Info" Src="~/usercontrols/Info.ascx" TagPrefix="userControlInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large;">Reproductive Infertility Payment Info
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <userControlInfo:Info ID="info" runat="server" Visible="false" />
                    <userControlError:Error ID="error" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" Height="30px" OnClick="btnExport_Click" />

                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>
                            <div style="height: 10px; width: 100%">
                            </div>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdvwRegistrationOfMarathonInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-hover table-striped" DataKeyNames="RICId" HorizontalAlign="Center" OnPageIndexChanging="grdvwRegistrationOfMarathonInfo_PageIndexChanging" OnRowCommand="grdvwRegistrationOfMarathonInfo_RowCommand" PageSize="15" Width="100%">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr No." >
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Patient Name">
                                            <ItemStyle HorizontalAlign="Center" Width="12%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstName" runat="server" Text='<%#Bind("FirstName") %>'></asp:Label>
                                                <asp:Label ID="lblLastName" runat="server" Text='<%#Bind("LastName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OPDNumber">
                                            <ItemStyle HorizontalAlign="Center" Width="8%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblOPDNumber" runat="server" Text='<%#Bind("OPDNumber") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Age">
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAge" runat="server" Text='<%#Bind("Age") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MobileNumber">
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblMobileNumber" runat="server" Text='<%#Bind("MobileNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                            <ItemStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%#Bind("Address") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%#Bind("Amount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TransactionId">
                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTransactionId" runat="server" Text='<%#Bind("TransactionId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Payment Status">
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentStatus" runat="server" Text='<%#Bind("PaymentStatus") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:ButtonField ButtonType="Button" CommandName="detail" ControlStyle-CssClass="btn btn-info" HeaderText="Detailed View" Text="Detail">
                                            <ControlStyle CssClass="btn btn-info" />
                                        </asp:ButtonField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>

        <%--View start--%>
        <div id="detailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <h3 id="myModalLabel">Detailed View</h3>
            </div>
            <div class="modal-body">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:DetailsView ID="dlvRegistrationOfMarathonInfo" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                            <Fields>
                                <asp:BoundField DataField="RICId" HeaderText="Id" />
                                <asp:TemplateField HeaderText="Patient Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFirstName" runat="server" Text='<%#Bind("FirstName") %>'></asp:Label>
                                        <asp:Label ID="lblLastName" runat="server" Text='<%#Bind("LastName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="OPDNumber">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOPDNumber" runat="server" Text='<%#Bind("OPDNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Age" HeaderText="Age" />
                                <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                <asp:BoundField DataField="TransactionId" HeaderText="TransactionId" />
                                <asp:BoundField DataField="Amount" HeaderText="Total Amount" />
                                <asp:BoundField DataField="PaymentStatus" HeaderText="Payment Status" />
                                <asp:TemplateField HeaderText="Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Bind("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Fields>
                        </asp:DetailsView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="grdvwRegistrationOfMarathonInfo" EventName="RowCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

        </div>
    </div>

</asp:Content>
