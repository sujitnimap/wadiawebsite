﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.adminpanel
{
    public partial class _event : System.Web.UI.Page
    {
        IList<ourevent> evetdata;

        string flag;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Convert.ToString(Session["flag"]) == "")
            {
                Response.Redirect("login.aspx");
            }
            {
                flag = Session["flag"].ToString();
            }

            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                evetdata = CEvent.Instance().GetEventData(flag);

                //Bind the fetched data to gridview
                if (evetdata.Count > 0)
                {
                    DataTable detailTable = ToDataTable(evetdata);

                    grdvwevent.DataSource = detailTable;
                    grdvwevent.DataBind();
                }
                else
                {
                    grdvwevent.DataSource = null;
                    grdvwevent.DataBind();
                }

            }
            catch (SqlException ex)
            {
                ((Label)error.FindControl("lblError")).Text = ex.Message;
                error.Visible = true;
            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwevent_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                Int32 id = Convert.ToInt32(grdvwevent.DataKeys[index].Value.ToString());

                evetdata = CEvent.Instance().GetEventData(flag);
                IList<ourevent> data = evetdata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))
            {
                GridViewRow gvrow = grdvwevent.Rows[index];

                Int32 id = Convert.ToInt32(grdvwevent.DataKeys[index].Value.ToString());

                evetdata = CEvent.Instance().GetEventData(flag);
                IList<ourevent> data = evetdata.Where(p => p.id.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                lblid.Text = id.ToString();
                txtName.Text = (detailTable.Rows[0]["eventname"]).ToString();
                FreeTextBox1.Text = (detailTable.Rows[0]["description"]).ToString();
                txtlocatn.Text = (detailTable.Rows[0]["location"]).ToString();
                txtstartdt.Text = (detailTable.Rows[0]["startdate"]).ToString();
                txtenddt.Text = (detailTable.Rows[0]["enddate"]).ToString();
                txtstime.Text = (detailTable.Rows[0]["starttime"]).ToString();
                txtetime.Text = (detailTable.Rows[0]["endtime"]).ToString();

                lblResult.Visible = false;

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string id = grdvwevent.DataKeys[index].Value.ToString();
                hfCode.Value = id;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(lblid.Text.ToString());
            string name = txtName.Text;
            string description = FreeTextBox1.Text;
            string location = txtlocatn.Text;
            string startdate = txtstartdt.Text;
            string enddate = txtenddt.Text;
            string starttm = txtstime.Text;
            string endtm = txtetime.Text;

            ourevent objevent = new ourevent();
            COperationStatus os = new COperationStatus();

            objevent.id = id;
            objevent.eventname = name;
            objevent.description = description;
            objevent.location = location;
            objevent.startdate = Convert.ToDateTime(startdate.ToString());
            objevent.enddate = Convert.ToDateTime(enddate.ToString());
            objevent.starttime = starttm;
            objevent.endtime = endtm;
            objevent.createdate = DateTime.Now;
            objevent.isactive = true;

            os = CEvent.Instance().UpdateEvent(objevent);


            if (os.Success == true)
            {
                txtName.Text = string.Empty;
                FreeTextBox1.Text = string.Empty;
                txtlocatn.Text = string.Empty;
                txtstartdt.Text = string.Empty;
                txtenddt.Text = string.Empty;
                txtstime.Text = string.Empty;
                txtetime.Text = string.Empty;

                BindGrid();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
            else
            {
                ((Label)error.FindControl("lblError")).Text = os.Message;
                error.Visible = true;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddRecord_Click(object sender, EventArgs e)
        {
            try
            {
                int flagProceed = 0;
                string name = txteventnm.Text;
                string description = txtdescrption.Text;
                string location = txtlocation.Text;
                string startdate = txtstartdate.Text;
                string enddate = txtenddate.Text;
                string starttm = txtstarttm.Text;
                string endtm = txtendtm.Text;

                ourevent objevent = new ourevent();
                COperationStatus os = new COperationStatus();

                //DateTime sdate = DateTime.ParseExact(startdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                //DateTime edate = DateTime.ParseExact(enddate, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                objevent.eventname = name;
                objevent.description = description;
                objevent.location = location;
                objevent.startdate = Convert.ToDateTime(startdate.ToString());
                objevent.enddate = Convert.ToDateTime(startdate.ToString());
                objevent.starttime = starttm;
                objevent.endtime = endtm;
                objevent.createdate = DateTime.Now;
                objevent.isactive = true;
                objevent.flag = flag;
                //write file Upload logic
                if (eventFileUpload.HasFile)
                {
                    string filename = Path.GetFileName(eventFileUpload.PostedFile.FileName);
                    string str = DateTime.Now.ToString("hhmmssffffff") + filename;
                    string fileExtension = Path.GetExtension(eventFileUpload.PostedFile.FileName);
                    if (fileExtension == ".doc" || fileExtension == ".docx" || fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".pdf")
                    {
                        string UploadFilePath = "~/Uploads/" + str;
                        objevent.EventFilePath = UploadFilePath;//assigning path
                        eventFileUpload.SaveAs(Server.MapPath(UploadFilePath));
                    }
                    else
                    {
                        Label5.Text = "Only word,excel or pdf files can be uploaded";
                        Label5.ForeColor = System.Drawing.Color.Red;
                        flagProceed++;
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", "<script>alert('Only word, excel or pdf files can be uploaded')</script>", false);
                       
                    }
                }
                if (flagProceed == 0)
                {
                    os = CEvent.Instance().InsertEvent(objevent);
                }
                if (os.Success == true)
                {
                    txteventnm.Text = string.Empty;
                    txtdescrption.Text = string.Empty;
                    txtlocation.Text = string.Empty;
                    txtstartdate.Text = string.Empty;
                    txtenddate.Text = string.Empty;
                    txtstarttm.Text = string.Empty;
                    txtendtm.Text = string.Empty;

                    BindGrid();

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('" + os.Message + "');");
                    //sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                else
                {
                    ((Label)error.FindControl("lblError")).Text = os.Message;
                    error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Label5.Text = "Message:" + ex.Message + "\n Inner Exception:" + ex.InnerException + "\n StackTrace" + ex.StackTrace;
                Label5.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string id = hfCode.Value;

            ourevent objevent = new ourevent();
            COperationStatus os = new COperationStatus();

            os = CEvent.Instance().DeleteEvent(Convert.ToInt32(id));

            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('" + os.Message + "');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        protected void grdvwevent_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwevent.PageIndex = e.NewPageIndex;
            grdvwevent.DataBind();
        }

    }
}