﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminpanel/admin.Master" EnableEventValidation = "false" AutoEventWireup="true" CodeBehind="AdultParticipantOfMarathon.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel.AdultParticipantOfMarathon" %>


<%@ Register TagName="Error" Src="~/usercontrols/Error.ascx" TagPrefix="userControlError" %>
<%@ Register TagName="Info" Src="~/usercontrols/Info.ascx" TagPrefix="userControlInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large;">Adult Participant Of Marathon
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <userControlInfo:Info ID="info" runat="server" Visible="false" />
                    <userControlError:Error ID="error" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" Height="30px" OnClick="btnExport_Click" />

                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>
                            <div style="height: 10px; width: 100%">
                            </div>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdvwAdultOfMarathonInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-hover table-striped" HorizontalAlign="Center" OnPageIndexChanging="grdvwAdultOfMarathonInfo_PageIndexChanging" PageSize="15" Width="100%" OnRowDataBound="grdvwAdultOfMarathonInfo_RowDataBound">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr No.">
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Transation-ID">
                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstName" runat="server" Text='<%#Bind("TransationID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adult Name">
                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastName" runat="server" Text='<%#Bind("AdultName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Age">
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="server" Text='<%#Bind("Age") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Gender">
                                            <ItemStyle HorizontalAlign="Center" Width="25%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmail" runat="server" Text='<%#Bind("Gender") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tshirt Quantity">
                                            <ItemStyle HorizontalAlign="Center" Width="25%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblQuantityTshirt" runat="server" Text='<%#Bind("QuantityTshirt") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TShirtSize for Parent">
                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTShirtSize1" runat="server" Text='<%#Bind("TShirtSize1") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                         <%--<asp:TemplateField HeaderText="TShirtSize for Parent 2">
                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTShirtSize2" runat="server" Text='<%#Bind("TShirtSize2") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="TShirtSize for Parent 3">
                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTShirtSize3" runat="server" Text='<%#Bind("TShirtSize3") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="TShirtSize for Parent 4">
                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTShirtSize4" runat="server" Text='<%#Bind("TShirtSize4") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="TShirtSize for Parent 5">
                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTShirtSize5" runat="server" Text='<%#Bind("TShirtSize5") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>


                </td>
            </tr>
        </table>
        
    </div>

</asp:Content>
