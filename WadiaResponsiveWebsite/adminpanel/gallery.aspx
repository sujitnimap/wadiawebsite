﻿<%@ Page Title="Gallery" Language="C#" MasterPageFile="~/adminpanel/admin.Master" AutoEventWireup="true" CodeBehind="gallery.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel.gallary" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register TagName="Error" Src="~/usercontrols/Error.ascx" TagPrefix="userControlError" %>
<%@ Register TagName="Info" Src="~/usercontrols/Info.ascx" TagPrefix="userControlInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large;">Image Gallery Master
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <userControlInfo:Info ID="info" runat="server" Visible="false" />
                    <userControlError:Error ID="error" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>
                            <div style="padding-left: 20px">
                                <asp:Button ID="btnAdd" runat="server" Text="Add New Image Gallery" CssClass="btn btn-info" OnClick="btnAdd_Click" />
                            </div>

                            <div style="height: 10px; width: 100%"></div>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdvwgallary" runat="server" Width="100%" HorizontalAlign="Center"
                                    OnRowCommand="grdvwgallary_RowCommand" PageSize="5" AutoGenerateColumns="False" AllowPaging="True"
                                    DataKeyNames="id" CssClass="table table-hover table-striped" OnPageIndexChanging="grdvwgallary_PageIndexChanging">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr No.">
                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblnm" runat="server" Text='<%#Bind("albumname") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Category Name">
                                            <ItemStyle Width="20%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblcategorynm" runat="server" Text='<%#Bind("categorynm") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Main Image">
                                            <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Image ID="imgmain" Width="100px" Height="100px" ImageUrl='<%#(Eval("thumbnail")) %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblcreatedate" runat="server" Text='<%#(Eval("cratedate")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Add More Images Here">
                                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibtngallaryImg" runat="server" CausesValidation="false" ImageUrl="../adminpanel/images/layout_add.png"
                                                    PostBackUrl='<%# "Imagegallery.aspx?catid="+ Convert.ToString(Eval("Id"))  %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Detail" HeaderText="Detailed View">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                        <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Edit" HeaderText="Edit Record">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                        <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Delete" HeaderText="Delete Record">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                    </Columns>
                                    <%--<PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <PagerStyle BackColor="#7779AF" Font-Bold="true" ForeColor="White" />--%>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>

    <!-- Detail Modal Starts here-->
    <div id="detailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Detailed View</h3>
        </div>
        <div class="modal-body">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:DetailsView ID="DetailsView1" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                        <Fields>
                            <asp:BoundField DataField="id" HeaderText="Id" />
                            <asp:BoundField DataField="albumname" HeaderText="Album Name" />
                            <asp:BoundField DataField="categorynm" HeaderText="Category Name" />
                            <asp:BoundField DataField="cratedate" HeaderText="Date" />
                            <asp:TemplateField HeaderText="Description">
                                <ItemTemplate>
                                    <asp:Label ID="lbldescription" runat="server" Text='<%#Bind("description") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Thumbnail Image">
                                <ItemTemplate>
                                    <asp:Image ID="mimg" Width="200px" Height="150px" ImageUrl='<%#(Eval("imagenm")) %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Thumbnail Image">
                                <ItemTemplate>
                                    <asp:Image ID="timg" Width="200px" Height="150px"  ImageUrl='<%#(Eval("Thumbnail")) %>'  runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        </Fields>
                    </asp:DetailsView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdvwgallary" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <div class="modal-footer">
                <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
    <!-- Detail Modal Ends here -->
    <!-- Edit Modal Starts here -->
    <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="editModalLabel">Edit Record</h3>
        </div>
        <asp:UpdatePanel ID="upEdit" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <td>Id : </td>
                            <td colspan="2">
                                <asp:Label ID="lblid" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Album Name&nbsp;: </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtalbumName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtalbumName" ValidationGroup="galleryadd" ControlToValidate="txtalbumName" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Date&nbsp;: </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtupdatedate" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txtupdatedate_CalendarExtender" Format="MM/dd/yyyy" runat="server" TargetControlID="txtupdatedate">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reftxtupdatedate" ValidationGroup="galleryadd" ControlToValidate="txtupdatedate" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Description&nbsp;:</td>
                            <td>
                                <FTB:FreeTextBox ID="FreeTextBox1" runat="server">
                                </FTB:FreeTextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="refFreeTextBox1" ValidationGroup="galleryadd" ControlToValidate="FreeTextBox1" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td>Category : </td>
                            <td colspan="2">
                                <asp:DropDownList ID="ddleditcategory" DataValueField="id" DataTextField="categorynm" Width="150px" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>Thumbnail&nbsp;: </td>
                            <td colspan="2">
                                <asp:AsyncFileUpload ID="AsyncFileUpload1" runat="server" />
                                Size :(300 X 300)
                            </td>
                        </tr>
                        <%--<tr id="Tr1" runat="server" visible="false">
                            <td>Thumbnail Image : </td>
                            <td colspan="2">
                                <asp:AsyncFileUpload ID="AsyncFileUpload2" runat="server" />
                                Size :(300 X 300)                                
                            </td>
                        </tr>--%>
                    </table>
                </div>
                <div class="modal-footer">
                    <asp:Label ID="lblResult" Visible="false" runat="server"></asp:Label>
                    <asp:Button ID="btnSave" runat="server" Text="Update" CssClass="btn btn-info" OnClick="btnSave_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="grdvwgallary" EventName="RowCommand" />
                <asp:PostBackTrigger ControlID="btnSave" />
                <%--<asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!-- Edit Modal Ends here -->

    <!-- Add Record Modal Starts here-->
    <div id="addModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="addModalLabel">Add New Record</h3>
        </div>
        <asp:UpdatePanel ID="upAdd" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <td>Album Name&nbsp;: </td>
                            <td>
                                <asp:TextBox ID="txtalbumnm" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtalbumnm" ValidationGroup="galleryadd" ControlToValidate="txtalbumnm" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <td>Date&nbsp;: </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtadddate" runat="server"></asp:TextBox>
                            <asp:CalendarExtender ID="txtadddate_CalendarExtender" Format="MM/dd/yyyy" runat="server" TargetControlID="txtadddate">
                                </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="reftxtadddate" ValidationGroup="galleryadd" ControlToValidate="txtadddate" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        </tr>
                        <tr>
                            <td>Description&nbsp;: </td>
                            <td>
                                <FTB:FreeTextBox ID="txtdescrption" runat="server">
                                </FTB:FreeTextBox>
                                <%--<asp:TextBox ID="txtdescrption" runat="server"></asp:TextBox>--%>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="reftxtdescrption" ValidationGroup="galleryadd" ControlToValidate="txtdescrption" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td>Category : </td>
                            <td>
                                <asp:DropDownList ID="ddlcategory" DataValueField="id" DataTextField="categorynm" Width="150px" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>Thumbnail&nbsp;: </td>
                            <td>
                                <asp:AsyncFileUpload ID="fldthumbnail" runat="server" />
                                Size :(300 X 300)
                                
                            </td>
                        </tr>
                        <%--<tr id="tr2" runat="server" visible="false">
                            <td>Thumbnail Image : </td>
                            <td>
                                <asp:AsyncFileUpload ID="fldthumbnail" runat="server" />
                                Size :(300 X 300)
                            </td>
                        </tr>--%>
                    </table>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnAddRecord" runat="server" Text="Add" CssClass="btn btn-info" OnClick="btnAddRecord_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAddRecord" />
                <%--<asp:AsyncPostBackTrigger ControlID="btnAddRecord" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!--Add Record Modal Ends here-->
    <!-- Delete Record Modal Starts here-->
    <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="delModalLabel">Delete Record</h3>
        </div>
        <asp:UpdatePanel ID="upDel" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    Are you sure you want to delete the record?
                            <asp:HiddenField ID="hfCode" runat="server" />
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelete_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!--Delete Record Modal Ends here -->

</asp:Content>
