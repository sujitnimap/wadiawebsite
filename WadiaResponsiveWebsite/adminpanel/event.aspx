﻿<%@ Page Title="Event" Language="C#" MasterPageFile="~/adminpanel/admin.Master" AutoEventWireup="true" CodeBehind="event.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel._event" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register TagName="Error" Src="~/usercontrols/Error.ascx" TagPrefix="userControlError" %>
<%@ Register TagName="Info" Src="~/usercontrols/Info.ascx" TagPrefix="userControlInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large;">Event Master
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <userControlInfo:Info ID="info" runat="server" Visible="false" />
                    <userControlError:Error ID="error" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>

                            <div style="padding-left: 20px">
                                <asp:Button ID="btnAdd" runat="server" Text="Add New Event" CssClass="btn btn-info" OnClick="btnAdd_Click" />
                            </div>

                            <div style="height: 10px; width: 100%"></div>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdvwevent" runat="server" Width="100%" HorizontalAlign="Center"
                                    OnRowCommand="grdvwevent_RowCommand" PageSize="5" AutoGenerateColumns="False" AllowPaging="True"
                                    DataKeyNames="id" CssClass="table table-hover table-striped" OnPageIndexChanging="grdvwevent_PageIndexChanging">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                         <asp:TemplateField HeaderText="Sr No.">
                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Event Name">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbleventname" runat="server" Text='<%#Bind("eventname") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemStyle Width="35%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbldescription" runat="server" Text='<%#Bind("description") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbllocation" runat="server" Text='<%#Bind("location") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstartdate" runat="server" Text='<%# Eval("startdate", "{0:D}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstartdate" runat="server" Text='<%# Eval("enddate", "{0:D}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstarttime" runat="server" Text='<%#Bind("starttime") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblendtime" runat="server" Text='<%#Bind("endtime") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Detail" HeaderText="Detailed View">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                        <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Edit" HeaderText="Edit Record">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                        <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Delete" HeaderText="Delete Record">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                    </Columns>
                                    <%--<PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <PagerStyle BackColor="#7779AF" Font-Bold="true" ForeColor="White" />--%>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    <!-- Detail Modal Starts here-->
    <div id="detailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Detailed View</h3>
        </div>
        <div class="modal-body">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:DetailsView ID="DetailsView1" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                        <Fields>
                            <asp:BoundField DataField="id" HeaderText="Id" />
                            <asp:BoundField DataField="eventname" HeaderText="Name" />
                            <asp:TemplateField HeaderText="Description">
                                <ItemTemplate>
                                    <asp:Label ID="lbldescription" runat="server" Text='<%#Bind("description") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="description" HeaderText="Description" />--%>
                            <asp:BoundField DataField="location" HeaderText="Location" />
                            <asp:BoundField DataField="startdate" DataFormatString="{0:D}" HeaderText="Startdate" />
                            <asp:BoundField DataField="enddate" DataFormatString="{0:d}" HeaderText="Enddate" />
                            <asp:BoundField DataField="starttime" HeaderText="Start Time" />
                            <asp:BoundField DataField="endtime" HeaderText="End Time" />
                        </Fields>
                    </asp:DetailsView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdvwevent" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <div class="modal-footer">
                <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
    <!-- Detail Modal Ends here -->
    <!-- Edit Modal Starts here -->
    <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="editModalLabel">Edit Record</h3>
        </div>
        <asp:UpdatePanel ID="upEdit" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <td>Id : </td>
                            <td colspan="2">
                                <asp:Label ID="lblid" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Name : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtName" ValidationGroup="eventupdate" ControlToValidate="txtName" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Description&nbsp;:</td>
                            <td>
                                <FTB:FreeTextBox ID="FreeTextBox1" runat="server">
                                </FTB:FreeTextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="refFreeTextBox1" ValidationGroup="eventupdate" ControlToValidate="FreeTextBox1" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Location:</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtlocatn" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtlocatn" ValidationGroup="eventupdate" ControlToValidate="txtlocatn" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Start Date:</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtstartdt" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txtstartdt_CalendarExtender" TargetControlID="txtstartdt" runat="server"></asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reftxtstartdt" ValidationGroup="eventupdate" ControlToValidate="txtstartdt" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>End Date:</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtenddt" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txtenddt_CalendarExtender" TargetControlID="txtenddt" runat="server"></asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reftxtenddt" ValidationGroup="eventupdate" ControlToValidate="txtenddt" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Start Time:</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtstime" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtstime" ValidationGroup="eventupdate" ControlToValidate="txtstime" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>End Time:</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtetime" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtetime" ValidationGroup="eventupdate" ControlToValidate="txtetime" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <asp:Label ID="lblResult" Visible="false" runat="server"></asp:Label>
                    <asp:Button ID="btnSave" runat="server" Text="Update" ValidationGroup="eventupdate" CssClass="btn btn-info" OnClick="btnSave_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="grdvwevent" EventName="RowCommand" />
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!-- Edit Modal Ends here -->
    <!-- Add Record Modal Starts here-->
    <div id="addModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="addModalLabel">Add New Record</h3>
        </div>
        <asp:UpdatePanel ID="upAdd" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <td>Event Name: </td>
                            <td colspan="2">
                                <asp:TextBox ID="txteventnm" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxteventnm" ValidationGroup="eventadd" ControlToValidate="txteventnm" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Description&nbsp;: </td>
                            <td>
                                <FTB:FreeTextBox ID="txtdescrption" runat="server">
                                </FTB:FreeTextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="reftxtdescrption" ValidationGroup="eventadd" ControlToValidate="txtdescrption" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Location : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtlocation" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtlocation" ValidationGroup="eventadd" ControlToValidate="txtlocation" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Start Date : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtstartdate" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txtstartdate_CalendarExtender" runat="server" TargetControlID="txtstartdate">
                                </asp:CalendarExtender>

                                <asp:Label ID="lblstartdate" runat="server" Font-Size="Smaller" Text="Type Date Format!" />
                                <asp:RequiredFieldValidator ID="reftxtstartdate" ValidationGroup="eventadd" ControlToValidate="txtstartdate" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>End Date : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtenddate" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txtenddate_CalendarExtender" runat="server" TargetControlID="txtenddate">
                                </asp:CalendarExtender>

                                <asp:Label ID="lblenddate" runat="server" Font-Size="Smaller" Text="Type Date Format!" />
                                <asp:RequiredFieldValidator ID="reftxtenddate" ValidationGroup="eventadd" ControlToValidate="txtenddate" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Start Time :</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtstarttm" runat="server"></asp:TextBox>
                                <asp:Label ID="Label3" runat="server" Font-Size="Smaller" Text="Type Integer Value!" />
                                <asp:RequiredFieldValidator ID="reftxtstarttm" ValidationGroup="eventadd" ControlToValidate="txtstarttm" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>End Time :</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtendtm" runat="server"></asp:TextBox>
                                <asp:Label ID="Label4" runat="server" Font-Size="Smaller" Text="Type Integer Value!" />
                                <asp:RequiredFieldValidator ID="reftxtendtm" ValidationGroup="eventadd" ControlToValidate="txtendtm" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Upload a file</td>
                            <td colspan="2">
                                <asp:FileUpload ID="eventFileUpload" runat="server" />
                                <asp:Label ID="Label5" runat="server" Font-Size="Smaller" Text="Please upload file" />
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="eventadd" ControlToValidate="eventFileUpload" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnAddRecord" runat="server" Text="Add" ValidationGroup="eventadd" CssClass="btn btn-info" OnClick="btnAddRecord_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAddRecord" />
               <%-- <asp:AsyncPostBackTrigger ControlID="btnAddRecord" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!--Add Record Modal Ends here-->
    <!-- Delete Record Modal Starts here-->
    <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="delModalLabel">Delete Record</h3>
        </div>
        <asp:UpdatePanel ID="upDel" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    Are you sure you want to delete the record?
                            <asp:HiddenField ID="hfCode" runat="server" />
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelete_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!--Delete Record Modal Ends here -->

</asp:Content>
