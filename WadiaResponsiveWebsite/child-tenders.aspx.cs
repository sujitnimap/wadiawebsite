﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class child_tenders1 : System.Web.UI.Page
    {
        static string flag = "c";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                upcomingtendersdata();
                passtendersdata();
            }

        }


        public void upcomingtendersdata()
        {

            DataTable dt = CTender.Instance().GetUcomingTenderData(flag);


            dtlistupcmingtenders.DataSource = dt;
            dtlistupcmingtenders.DataBind();

        }

        public void passtendersdata()
        {


            DataTable dt = CTender.Instance().GetpsTenderData(flag);


            dtlistpasttenders.DataSource = dt;
            dtlistpasttenders.DataBind();

        }
    }
}