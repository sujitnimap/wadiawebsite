﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-about-doctdetails-drtrupti.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_doctdetails_drtrupti" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



<div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr Trupti Nadkarni.jpg" />
<div class="docdetails">
Dr. Trupti K Nadkarni



<div class="docdetailsdesg">
Associate Professor    <br />
MD, DNB, FCPS, DGO


</div>

</div>
</div>

     <div class="areaofinterest">

Areas of interest : Menopause and endoscopy.
</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
    •	2nd prize at the Shirin Mehtaji Competition in November 2001 for the paper ‘Perinatal outcome of Tuberculosis in pregnancy”.
<br />•	 1st prize at the annual MOGS Conference 2002 for the paper “Role of Misoprostol and manual vacuum aspiration in first trimester MTP” 
<br />•	The Dr. D.K.Tank prize for an interesting case presentation on “Bladder Endometriosis” in January 2008 annual MOGS conference.
<br />•	Paper presentation “Tuberculosis in pregnancy” at the Association of Medical Women of India Conference in December 2001.
<br />•	Interesting case presentation “Gonadoblastoma in a case of Turners Mosaic” at the annual conference MOGS 2010.
<br />•	“Endometriosis: Dilemmas in management” at the annual IAGE conference Pune 2011.

<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
     Member of Mumbai Obstetrics and Gynaecology Society
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	“Caesarean Hysterectomy”. Caesarean Birth. FOGSI publication 2003.
<br />•	“Post partum Haemorrhage”. A practical approach to high risk Pregnancy and Delivery.5th edition.
<br />•	Delayed menopause due to granulosa cell tumour of the ovary. J Midlife Health. 2011 Jul;2(2):86-8
<br />•	Amniotic band syndrome- a case report. ISFN 2012 Jun ;2(3), 2249-9784. 
<br />•	‘Bilateral Gonadoblastoma in a case of Turners mosaic- a case report’( under review in Bombay Hospital Journal)
<br />•	‘Laparoscopic management of ovarian torsion in third trimester of pregnancy’.( under review in Bombay Hospital Journal) 

<br /><br />
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			






				</div>
			
</asp:Content>
