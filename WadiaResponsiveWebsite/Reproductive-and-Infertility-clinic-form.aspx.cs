﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class Reproductive_and_Infertility_clinic_form : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {

            Reproductive_Infertility_Clinic_Detail objReproductiveInfertilityClinicDetail = new Reproductive_Infertility_Clinic_Detail();
            COperationStatus os = new COperationStatus();

            objReproductiveInfertilityClinicDetail.FirstName = txtFirstName.Text.Trim().ToString();
            objReproductiveInfertilityClinicDetail.LastName = txtLastName.Text.Trim().ToString();
            objReproductiveInfertilityClinicDetail.OPDNumber = txtOPDNo.Text.Trim().ToString();
            objReproductiveInfertilityClinicDetail.Address = txtAddress.Text.Trim().ToString();
            objReproductiveInfertilityClinicDetail.MobileNo = txtMobileNo.Text.Trim().ToString();

            objReproductiveInfertilityClinicDetail.Age = Convert.ToByte(txtAge.Text);
            objReproductiveInfertilityClinicDetail.Amount = txtAmount.Text.Trim().ToString();


            objReproductiveInfertilityClinicDetail.TransactionId = txtFirstName.Text.Trim().ToString().Substring(0, 1) + txtLastName.Text.Trim().ToString().Substring(0, 1) + txtMobileNo.Text.Trim().ToString() + DateTime.Now.ToString("hhmmssfff");

            objReproductiveInfertilityClinicDetail.PaymentStatus = "P";

            objReproductiveInfertilityClinicDetail.CreateDate = DateTime.Now;
            objReproductiveInfertilityClinicDetail.ModifiedDate = DateTime.Now;


            os = CCMERegistration.Instance().InsertReproductiveInfertilityClinicDetail(objReproductiveInfertilityClinicDetail);

            if (os.Success == true)
            {
                Response.Redirect("ReproductiveInfertilityClinicPayURequest.aspx?Id=" + objReproductiveInfertilityClinicDetail.RICId);
            }

        }
    }
}