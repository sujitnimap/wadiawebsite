﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drvivek.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drvivek" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
		<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dr vivek.jpg" />
<div class="docdetails">
Dr. Vivek Rege
<div class="docdetailsdesg">
Asst. Hon. Paediatric Surgery
    <br />
MD, DCH, FRCPCHMS, Mch, DNB (Ped Sur)


</div>

</div>
</div>

                    <div class="areaofinterest">


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
  •	U.C. Chakraworty Award - Best    Paper: Original Research, 
Paediatric Surgery: 1987.
<br /><br />
•	Hargobind Foundation International Medical
Fellowship: for specialised training in Paediatric  Surgery abroad.
<br /><br />
•	Madgavkar  Trust  Travelling  Fellowship: for specialised training in Paediatric Surgery abroad 					                                         
<br /><br />
•	Godrej Foundation Fellowship: for specialised training in Paediatric Surgery abroad.




<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	Member: Indian Association of  Paediatric Surgeons.                                
<br /><br />
•	Member: Indian Academy of Paediatrics.
<br /><br />
•	Member : Association of Surgery of India.
<br /><br />
•	Member:  Indian  Society   of   Prenatal Diagnosis & Therapy.
<br /><br />
•	Member:  Association for Trauma  Care of India
<br /><br />
•	Member: Indian Society for Paediatric Urology




    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Gandhi   R.K.,   Rege   V.M.,  Deshmukh   S.S.,   Sane   S.Y.,   : Bronchopulmonary foregut Malformation : Indian Journal of Surgery, Vol : 47, JUNE : 1985.
	<br /><br />
•	Rege  V.M., Gandhi R.K., Deshmukh S.S., Farooq M.S., Sane S.Y.,  : Epithelial  Cyst of the Spleen : Indian Journal of Surgery, Vol  :  48 MAY : 1986.
	<br /><br />
•	Rege  V.M.,  Deshmukh S.S., Borwankar S.S., Gandhi  R.K.  :  Blind Ending Bifid Ureter : Journal of Postgraduate Medicine Vol : 32  :  APRIL : 1986.
	<br /><br />
•	Rege  V.M.,  Deshmukh  S.S.  :  Megacystis  Microcolon  Intestinal Hypoperistalsis  Syndrome : Indian Journal of Surgery Vol :  50  :  FEB. 1988.
	<br /><br />
•	Rege  V.M.,  Deshmukh S.S. : Major thoracic trauma in  Children  : Journal of Postgraduate Medicine (Accepted for Publication).  Vol:  34 : APRIL : 1988.
	<br /><br />
•	Rege V.M., Deshmukh S.S. : Accessory Perineal Scrotum : Journal of   Postgraduate Medicine Vol : 34, JULY : 1988.
	<br /><br />
•	Rege  V.M., Deshmukh S.S., Borwankar S.S., Kulkarni B.K.  :  Blunt Renal Trauma in Children : Indian Journal of Surgery
                    <br /><br /> 
•	Rege   V.M.,   Deshmukh  S.S.,  Borwankar  S.S.,   Kulkarni   B.K. :Intussusception  in  infancy  and childhood  :  Evaluation  of  a prognostic  Scoring pattern  : Journal of Postgraduate Medicine 
	<br /><br />
•	Sheth N.P., Navinkumar P, Rege V.M. : Sacro-Perineal Pull Thru for Supralevator  Ano-Rectal  Anomalies  :  Journal  of   Postgraduate  Medicine.
	<br /><br />
•	Sheth  N.P., Navinkumar P, Rege V.M. : Suggested  Modification  of  Kasai's hepatic Porto-Enterostomy : Indian Journal of Surgery.
	<br /><br />
•	Sheth  N.P.,  Navinkumar P, Rege V.M. :  Further  experience  with     Sacro-Perineal  procedure for Supralevator Ano-Rectal Anomalies  : Indian Journal of Surgery.
	<br /><br />
•	Telander R.L., Rege V.M., Sims : Modification of Endo-Rectal  Pull Thru for Hirschsprungs Disease : Journal of Paediatric Surgery.
	<br /><br />
	
	
•	Dalal  Subhash & Rege Vivek : Cryptorchidism : Recent  Advances  :  Indian Academy of Paediatrics.
	<br /><br />
•	Dalal  Subhash  & Rege Vivek : Intersex in Adolescent  Girls  :  A   Surgeon's   Approach   Chapter   II  in   the   Adolescent   Girl, Ed.  Dr.  U.  Krishna,  Dr.  S. Parulekar  &  Dr.  V.  Salvi  Pub.  Federation of Obstetric and Gynaecological Societies of India  Pg. 100-105. 
	<br /><br />
•	Rege Vivek : Fetal Surgery : Fetal Medicine & Surgery - Quarterly: Indian Society of Prenatal Diagnosis & Therapy.
	<br /><br />
•	Rege  Vivek  :  Gastroesophageal Reflux :    Recent  Advances  in  Paediatrics  Volume 4, Ed. Dr. S. Gupte, Pub. Jaypee Brothers  1994 Chapter 6, Pg. 88-110.
	<br /><br />
•	Rege  Vivek  :  Urinary  Incontinence :Recent  Advances  in  Paediatrics  Volume 4, Ed. Dr. S. Gupte. Pub Jaypee  Brothers  1994 Chapter 7, Pg 111-139.
	<br /><br />
•	Rege Vivek : Neonatal Surgery : Publication of Annual Convention :  National Neonatology Forum Kerala September 1994.
	<br /><br />
•	Rege  Vivek : Esophageal Atresia and Tracheo Esophageal Fistula  :   Chapter  Recent Advances in Paediatrics, Volume 5, Ed. Suraj  Gupte Pub Jaypee Brothers 1995 Pp : 106-132.
	<br /><br />
•	Rege  Vivek : Vesicoureteral Reflux : Chapter Recent  Advances  in   Paediatrics,  Volume  5 Ed. Suraj Gupte Pub  Jaypee  Brothers  1995  Pp : 133-153.
	<br /><br />
•	Rege  Vivek  :  Paediatric Surgical Conditions -  In.Pediatrics  :  Priorities   in   Office   Practice.  Ed  Dr.   Y.K.   Amdekar   &  Dr. R.P. Khubchandani Pub. Paediatric Priorities 1995.
	<br /><br />
•	Rege  Vivek  :  Neonatal Surgical  Conditions  -  In.Pediatrics  : Priorities   in   Office  Practice.  Ed.  Dr.   Y.K.   Amdekar   & Dr. R.P. Khubchandani. Pub. Paediatric Priorities 1995.
	<br /><br />
•	Rege Vivek : Congenital Diaphragmatic Hernia : In Recent  Advances in  Paediatrics Volume 6, Ed. Dr. S. Gupte. Pub. Jaypee  Brothers,  pg 379-397, 1996. .
	<br /><br />
•	Rege  Vivek  : Urinary Tract Infections :  Recent  Advances  in  Paediatrics  Volume  6, Ed. Dr. S. Gupte.  Pub.  Jaypee  Brothers,  pg 149-163, 1996.
	<br /><br />
	
•	Rege  Vivek & Vaidya A : Pneumothorax : Intercostal Drain : Bombay   Hospital Journal, Vol 37, No 4, October 1995, pg 758-759. 
	<br /><br />
•	Vaidya  A  &  Rege Vivek : Vascular Access in  Neonates  :  Bombay  Hospital Journal, Volume 37, No 4, October 1995, pg 760-761.
	 <br /><br />
	
•	Rege Vivek : Book Review of Practical Paediatric Surgery : Dr. R.K. Gandhi,  Dr. (Mrs) S. S. Deshmukh in Bombay Hospital Journal,  Vol  37, No 4, October 1995.
<br /><br />
	
•	Shah  Mahendra  & Rege Vivek : Parenteral Nutrition in  infants  &                  children   Ed. Dr. Suraj Gupte Recent Advances in  Paediatrics  : Special issue on Nutrition, Growth & Development.
	<br /><br />
•	Rege  Vivek: Bilious Vomiting in the Neonate : In Recent  Advances  in  Paediatrics. Volume 7 Ed. Dr Suraj Gupte. Pub Jaypee  Brothers, 1997, pg 110-113.
	<br /><br />
•	Rege  Vivek  :  Congenital Pulmonary Cystic Lesions : In  Recent                  Advances  in Paediatrics. Volume 7, Ed. Dr Suraj Gupte. Pub  Jaypee Brothers, pg 279-293
	<br /><br />
•	Rege  Vivek  :  Cryptorchidism : In  Indian  Journal  of  Clinical Practice, 1997.

<br /><br />


                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
         </div>




</asp:Content>
