﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-Epilepsy-clinic.aspx.cs" Inherits="WadiaResponsiveWebsite.child_epilepsy_clinic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Epilepsy Clinic  </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview </a></li>
            <li><a href="#view3">Treatments </a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
One of the most common neurological disorders among children, epilepsy is a chronic condition that causes recurrent seizures.  We understand the unique needs of children with epilepsy and offer not only treatment to affected patients but also provide counselling to parents about symptoms, diagnosis and treatment measures.  
<br />
                Our collaborative approach towards epilepsy ensures that we give the child holistic treatment through neuropsychology and neuroradiology as well. 
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
<b>Depending on the severity of epilepsy in the child, we provide the following: 
</b>
                <br /><br />
<div style="padding-left:30px">
-	Epilepsy Medication 
<br />-	Extratemporal Epilepsy Surgery 
<br />-	Hemispherectomy
<br />-	Vagus Nerve Stimulation
<br />-	Ketogenic Diet

</div>


                
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            
	</div>
</asp:Content>
