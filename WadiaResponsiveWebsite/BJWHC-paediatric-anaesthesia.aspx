﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-paediatric-anaesthesia.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_anaesthesia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PAEDIATRIC ANAESTHESIA</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 
    <strong>Overview </strong>
    <br />    <br />


The hospital boasts of one of the best Paediatric Anaesthesia Department in the country that offers routine and emergency services to surgical 
    departments within the hospital such as General surgery, Neurosurgery, Orthopaedic surgery, Plastic surgery, ENT, Burns and Radiology. With more than 6000 surgeries per year and some of them very complicated even in premature babies, anaesthetists play a vital role in success of surgical procedures. Fellows come from distant parts of India and other countries to learn paediatric anaesthesia techniques.  


    <div style="height:160px"></div>

</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            


		</div>

</asp:Content>
