﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite.webservices
{
    /// <summary>
    /// Summary description for wadiaservices
    /// </summary>
    [WebService(Namespace = "http://nimapinfotech.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class wadiaservices : System.Web.Services.WebService
    {

        //public void UserAuthenticationService
        //{

        //}

        [WebMethod]
        public XmlDocument geteventdata(string flag)
        {
             XmlDocument xDoc = new XmlDocument();

             try
             {
                 IList<ourevent> eventdata;

                 eventdata = CEvent.Instance().GetEventData(flag);

                 if (eventdata.Count > 0)
                 {
                     var xEle = new XElement("wadia",
                                     new XAttribute("status", "0"),
                             new XElement("eventinfo",
                             from data in eventdata
                             select new XElement("event",
                                    new XAttribute("id", data.id),
                                    new XElement("startdate", data.startdate),
                                    new XElement("enddate", data.enddate),
                                    new XElement("starttime", data.starttime),
                                    new XElement("endtime", data.endtime),
                                    new XElement("description", data.description),
                                    new XElement("location", data.location),
                                    new XElement("createdate", data.createdate)
                          )));

                     XmlReader xRead = xEle.CreateReader();
                     xDoc.Load(xRead);

                     return xDoc;
                 }
                 else
                 {
                      var xEle = new XElement("wadia",
                                     new XAttribute("status", "1"));

                     XmlReader xRead = xEle.CreateReader();
                     xDoc.Load(xRead);

                     return xDoc;
                 }
             }
             catch (Exception)
             {
                 throw new SoapException("Error while getting data, Please try again !!", SoapException.ServerFaultCode);
             }
        }

        [WebMethod]
        public XmlDocument geteventdatabyeventid(string eventid)
        {
            XmlDocument xDoc = new XmlDocument();

            try
            {
                IList<ourevent> eventdata;

                eventdata = CEvent.Instance().GetEventlistDatabyEventId(Convert.ToInt32(eventid));

                if (eventdata.Count > 0)
                {
                    var xEle = new XElement("wadia",
                                    new XAttribute("status", "0"),
                            new XElement("eventinfo",
                            from data in eventdata
                            select new XElement("event",
                                   new XAttribute("id", data.id),
                                   new XElement("startdate", data.startdate),
                                   new XElement("enddate", data.enddate),
                                   new XElement("starttime", data.starttime),
                                   new XElement("endtime", data.endtime),
                                   new XElement("description", data.description),
                                   new XElement("location", data.location),
                                   new XElement("createdate", data.createdate)
                         )));

                    XmlReader xRead = xEle.CreateReader();
                    xDoc.Load(xRead);

                    return xDoc;
                }
                else
                {
                    var xEle = new XElement("wadia",
                                   new XAttribute("status", "1"));

                    XmlReader xRead = xEle.CreateReader();
                    xDoc.Load(xRead);

                    return xDoc;
                }
            }
            catch (Exception)
            {
                throw new SoapException("Error while getting data, Please try again !!", SoapException.ServerFaultCode);
            }
        }


        [WebMethod]
        public XmlDocument gettestimonialdata(string flag)
        {
            XmlDocument xDoc = new XmlDocument();

            try
            {
                IList<testimonial> testimonialdata;

                testimonialdata = CTestimonial.Instance().GetTestimonialData(flag);

                if (testimonialdata.Count > 0)
                {
                    var xEle = new XElement("wadia",
                                    new XAttribute("status", "0"),
                            new XElement("testimonialsinfo",
                            from data in testimonialdata
                            select new XElement("testimonial",
                                   new XAttribute("id", data.id),
                                   new XElement("testimonialnm", data.testimonialnm),
                                   new XElement("testimonialdescrp", data.testimonialdescrp),
                                   new XElement("designation", data.designation),
                                   new XElement("createby", data.createby),
                                   new XElement("createdate", data.createdate)
                         )));

                    XmlReader xRead = xEle.CreateReader();
                    xDoc.Load(xRead);

                    return xDoc;
                }
                else
                {
                    var xEle = new XElement("wadia",
                                   new XAttribute("status", "1"));

                    XmlReader xRead = xEle.CreateReader();
                    xDoc.Load(xRead);

                    return xDoc;
                }
            }
            catch (Exception)
            {
                throw new SoapException("Error while getting data, Please try again !!", SoapException.ServerFaultCode);
            }
        }


        [WebMethod]
        public XmlDocument getartgallarydata()
        {
            XmlDocument xDoc = new XmlDocument();

            try
            {
                IList<artgallary> artgallarydata;

                //artgallarydata = CArtgallary.Instance().GetArtgallarygridData();

                //if (artgallarydata.Count > 0)
                //{
                //    var xEle = new XElement("wadia",
                //                    new XAttribute("status", "0"),
                //            new XElement("gallaryinfo",
                //            from data in artgallarydata
                //            select new XElement("album",
                //                   new XAttribute("id", data.id),
                //                   new XElement("albumname", data.albumname),
                //                   new XElement("categorynm", data.categorynm),
                //                   new XElement("subcategorynm", data.subcategorynm),
                //                   new XElement("imagenm", data.imagenm),
                //                   new XElement("thumbnail", data.thumbnail),
                //                    new XElement("cratedate", data.cratedate)
                //         )));

                //    XmlReader xRead = xEle.CreateReader();
                //    xDoc.Load(xRead);

                //    return xDoc;
                //}
                //else
                //{
                //    var xEle = new XElement("wadia",
                //                   new XAttribute("status", "1"));

                //    XmlReader xRead = xEle.CreateReader();
                //    xDoc.Load(xRead);

                //    return xDoc;
                //}

                var xEle = new XElement("wadia",
                                   new XAttribute("status", "1"));

                XmlReader xRead = xEle.CreateReader();
                xDoc.Load(xRead);

                return xDoc;

            }
            catch (Exception)
            {
                throw new SoapException("Error while getting data, Please try again !!", SoapException.ServerFaultCode);
            }
        }


        [WebMethod]
        public XmlDocument getarttenderdata(string flag)
        {
            XmlDocument xDoc = new XmlDocument();

            try
            {
                IList<tender> tenderdata;

                tenderdata = CTender.Instance().GetTenderData(flag);

                if (tenderdata.Count > 0)
                {
                    var xEle = new XElement("wadia",
                                    new XAttribute("status", "0"),
                            new XElement("tenderinfo",
                            from data in tenderdata
                            select new XElement("tender",
                                   new XAttribute("id", data.id),
                                   new XElement("name", data.name),
                                   new XElement("description", data.description),
                                   new XElement("downloadlink", data.downloadlink),
                                   new XElement("expirydate", data.expirydate),
                                    new XElement("createdate", data.createdate)
                         )));

                    XmlReader xRead = xEle.CreateReader();
                    xDoc.Load(xRead);

                    return xDoc;
                }
                else
                {
                    var xEle = new XElement("wadia",
                                   new XAttribute("status", "1"));

                    XmlReader xRead = xEle.CreateReader();
                    xDoc.Load(xRead);

                    return xDoc;
                }
            }
            catch (Exception)
            {
                throw new SoapException("Error while getting data, Please try again !!", SoapException.ServerFaultCode);
            }
        }

    }
}
