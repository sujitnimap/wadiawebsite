﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OperationStatus;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class women_donate_online : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnSaveAndMakePayment_Click(object sender, EventArgs e)
        {

            //Boolean isTHM = chkTMH.Checked;
            //Boolean isACTREC = chkACTREC.Checked;
            string nameOfOragnisation = txtNameOfOragnisation.Text.Trim();
            //string firstName = txtF

            //int donationTo = 1;
            //if (rdbACTREC.Checked == true)
            //{
            //    donationTo = 2;
            //}
            //if (chkTMH.Checked == true)
            //{
            //    donationTo = 1;
            //}
            //else
            //{
            //    donationTo = 2;
            //}

            int donationFrom = 1;
            //if (chkIndividual.Checked)
            //{
            //    donationTo = 1;
            //    chkCorporation.Checked = false;
            //    chkDonationFromOthers.Checked = false;
            //}
            //else if (chkCorporation.Checked)
            //{
            //    donationTo = 2;

            //    chkIndividual.Checked = false;
            //    chkDonationFromOthers.Checked = false;
            //}
            //else if (chkDonationFromOthers.Checked)
            //{
            //    donationTo = 3;

            //    chkCorporation.Checked = false;
            //    chkIndividual.Checked = false;
            //}

            donationFrom = Convert.ToInt32(ddlDonationFrom.SelectedValue);


            string nameOfOraganisation = txtNameOfOragnisation.Text.Trim();


            string firstName = txtFirstNameOfContactPerson.Text.Trim();
            string lastName = txtLastNameOfContactPerson.Text.Trim();

            string postalAddress = txtPostalAddress.Text.Trim();
            string postalCode = txtPostalCode.Text.Trim();
            string city = txtCity.Text.Trim();
            string country = ddlCountrys.SelectedValue;
            string email = txtEmail.Text.Trim();

            string pANNumber = txtPanNumbers.Text.Trim();

            string telePhoneNumber = txtTelephone.Text.Trim();
            string mobileNumber = txtMobile.Text.Trim();
            string messageToPrintedOnReceipt = txtMessageToBePrintedOnReceipt.Text.Trim();

            long amount = Convert.ToInt32(txtAmount.Text.Trim());

            int donationTowards = 0;

            bool isDonationTowardHospital = rbHospital.Checked;
            bool isDonationTowardPatientWelfareFund = rbPatientWelfareFund.Checked;
            bool isDonationTowardEquipmentOrInfrastucture = rbEquipmentOrInfrastucture.Checked;
            bool isDonationTowardOthers = rbDonationTowardsOthers .Checked;
            

            //if (rbPatientWelfareFund.Checked)
            //{
            //    donationTowards = 1;
            //}
            //else if (rbCancerResearch.Checked)
            //{
            //    donationTowards = 2;
            //}
            //else if (rbDonationTowardsOthers.Checked)
            //{
            //    donationTowards = 3;
            //}
            string donationTowardsOther = txtDonationTowardsOther.Text.Trim();

            bool isIncomTaxCertificateRequired = false;
            if (rbIsIncomTaxCertificateRequiredYes.Checked)
            {
                isIncomTaxCertificateRequired = true;
            }
            else if (rbIsIncomTaxCertificateRequiredNo.Checked)
            {
                isIncomTaxCertificateRequired = false;
            }

            int incomTaxExemptionOption = 0;
            if (rbIncomTaxExemptionOption80.Checked)
            {
                incomTaxExemptionOption = 1;
            }
            else if (rbIncomTaxExemptionOption35.Checked)
            {
                incomTaxExemptionOption = 2;
            }
            int modeOfPayment = 1; //1 --Credit Card

            //if (nameOfOraganisation == "")
            //{
            //    lblMessage.Text = "Please enter Organiser name";

            //}

            childDonateOnline objchildDonateOnline = new childDonateOnline();
            COperationStatus os = new COperationStatus();

            objchildDonateOnline.DonationFrom = donationFrom ;
            objchildDonateOnline.NameOfOraganisation = nameOfOragnisation;
            objchildDonateOnline.ContactPerson = firstName;
            objchildDonateOnline.PostalAddress = postalAddress;
            objchildDonateOnline.PostalCode = postalCode;
            objchildDonateOnline.City = city;
            objchildDonateOnline.Country = country;
            objchildDonateOnline.Email = email;
            objchildDonateOnline.PANNumber = pANNumber;
            objchildDonateOnline.TelePhoneNumber = telePhoneNumber;
            objchildDonateOnline.MobileNumber = mobileNumber;
            objchildDonateOnline.MessageToPrintedOnReceipt = messageToPrintedOnReceipt;
            objchildDonateOnline.Amount = amount;

            objchildDonateOnline.IsDonationTowardHospital = isDonationTowardHospital;
            objchildDonateOnline.IsDonationTowardPatientWelfareFund = isDonationTowardPatientWelfareFund;
            objchildDonateOnline.IsDonationTowardEquipmentOrInfrastucture = isDonationTowardEquipmentOrInfrastucture;
            objchildDonateOnline.IsDonationTowardOthers = isDonationTowardOthers;

            objchildDonateOnline.DonationTowardsOther = donationTowardsOther;
            objchildDonateOnline.IsIncomTaxCertificateRequired = isIncomTaxCertificateRequired;
            //objchildDonateOnline.isIncomTaxExemptionOption = ;
            objchildDonateOnline.ModeOfPayment = modeOfPayment;
            objchildDonateOnline.LastName = lastName;

            os = CChildDonateOnline.Instance().InsertDonationDetail(objchildDonateOnline);


            //os = CTestimonial.Instance().UpdateTestimonial(objchildDonateOnline);

            if (os.Success == true)
            {
                Response.Redirect("PayUWRequest.aspx?Id=" + objchildDonateOnline.Id);

            }
        }

        protected void ddlDonationFrom_SelectedIndexChanged(object sender, EventArgs e)
        {

            DonationFrom empType = (DonationFrom)Enum.Parse(typeof(DonationFrom), ddlDonationFrom.SelectedValue);
            if (empType.Equals(DonationFrom.Individuals))
            {
                pnlNameOfOrganisation.Visible = false;
            }
            else
            {
                pnlNameOfOrganisation.Visible = true;
            }
        }


        protected void donationTowards_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDonationTowardsOthers.Checked == true)
            {
                pnlDonationTowardsOther.Visible = true;
            }
            else
            {
                pnlDonationTowardsOther.Visible = false;
            }
        }

        protected void rbIsIncomTaxCertificateRequiredNo_CheckedChanged(object sender, EventArgs e)
        {
            if (rbIsIncomTaxCertificateRequiredNo.Checked == true)
            {
                pnlDonationReceipt.Visible = false;
            }
            else
            {
                pnlDonationReceipt.Visible = true;
            }
        }

        enum DonationFrom
        {
            Corporation = 1,
            Individuals = 2,
            Others = 3
        };

    }
}