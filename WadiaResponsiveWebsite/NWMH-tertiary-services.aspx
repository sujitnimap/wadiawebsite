﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-tertiary-services.aspx.cs" Inherits="WadiaResponsiveWebsite.women_tertiaryservices" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Tertiary Level Neonatal Intensive care Services </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
The Neonatology Department of NWMH provides level-III care for neonates born in the hospital through the highly specialized NICU. 
                The NICU conducts ROP screening every week, and collaborates with 
                the Paediatric Surgery, Paediatric Cardiology, Paediatric Endocrinology, Paediatric Haemato-oncology, Paediatric Neurosurgery and Paediatric Orthopaedic departments in the hospital as and when needed. 
                 
  <br />
 <br />
The Neonatology department adopts all the principles of Developmentally Supportive Care. It is baby friendly and strongly advocates breastfeeding to all newborn babies.
  <br />
 <br />
              <b>Services Provided:</b>   
 <br />
 <br />
<div style="padding-left:25px">
       
-	Salvaging very low birth weight neonates
<br />-	Treating neonates with severe medical conditions
<br />-	Treating neonates with birth defects
<br />-Testing for hearing of all newborn babies

</div>


 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

        



		</div>
</asp:Content>
