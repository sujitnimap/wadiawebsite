﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-cardiology.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_cardiology" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PAEDIATRIC CARDIOLOGY </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Service</a></li>
         <%--   <li><a href="#view3">Treatments</a></li>--%>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
      The Paediatric Cardiology Department caters to children with hereditary and acquired heart disease and offers non-invasive procedures of diagnosis and 
                therapy. It is equipped with state-of-art 2D echocardiographic machine that helps detect cardiac defect accurately. 
                Congenital heart defects are likely to occur in one out of 100 children born. Such newborns are detected immediately at birth and 
                managed appropriately by our trained specialists. The department treats around 1000 children a year and help many of them survive with early diagnosis and prompt management. Our specialists also provide backup to critically ill children in PICU and NICU with portable echocardiograph facilities to monitor heart functions.

 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               




                <br />


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

           


		</div>





</asp:Content>
