﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-paediatric-nephrology.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_nephrology" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Paediatric Nephrology</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
         <%--    <li><a href="#view4">Achievements</a></li>--%>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
The Paediatric Nephrology Department at BJWHC offers complete evaluation of children referred with renal disorders. The hospital also has the most sophisticated facilities for renal biopsy and histopathology.
                 <br />
 <br />
The BJWHC is the only centre in Western India that offers dialysis (including peritoneal, haemodialysis and plasmapheresis) services for all children including new-born and premature babies. The department has established itself as a centre of excellence for paediatric nephrology, providing a whole range of outdoor and indoor services to more than 1000 children in a year. Department also organises meetings of parents of children suffering from renal diseases that spreads awareness and understanding of renal diseases amongst the community with sharing mutual experiences. Such meetings have helped parents comply with medications and other advice. Experts during such meetings answer questions from parents and guide them to treat their children effectively. 
                The department is also recognised for training fellowship in renal diseases by International Paediatric Nephrology Association.
                <br /><br />
At the OPD, each patient is evaluated and clinical and lab findings are documented in detail. Basic laboratory investigations are performed and thereafter depending on the diagnosis, the medical team discusses and counsels family members about treatment, medication and care required.

 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
 The Paediatric Nephrology Department offers care and treatment of renal diseases and other kidney conditions such as : 
                <br />
<br />
<br />

                <div style="padding-left:10px">


                    -	Congenital anomalies of the urinary tract including vesicoureteral reflux and obstructive uropathy
<br />-	 Electrolyte imbalance
<br />-	 Renal parenchyma diseases including glomerulonephritis
<br />-	 Tubular diseases
<br />-	 Collagen vascular diseases, including systemic lupus erythematous




                </div>




        </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
     <%--  <div id="view4">
               
         - The department has been recognized by the International Paediatric Nephrology Association as a Training Centre for Paediatric Nephrology.


           <br /><br />
               
            </div>
            --%>
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

           

		</div>






</asp:Content>
