﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-paediatric-tuberculosis.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_tuberculosis" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Paediatric Tuberculosis Clinic</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 
    <strong>Overview </strong>
    <br />    <br />


At the BJWHC we run a Paediatric Tuberculosis Clinic once a week for children who not only have but also have been in close contact with Tuberculosis. 
    For over 1000 children, every year, comprehensive, multidisciplinary care is delivered with an emphasis on the whole family unit. 
    These children are monitored and treated through drug therapy. 
    <br />
    
    This unit has assumed great importance as multi-drug resistant Tuberculosis is on the 
    increase mainly due to improper treatment and non-compliance of patients. Special emphasis is given to avoid these problems in this unit. 
    Early diagnosis of drug resistant Tuberculosis and proper treatment with second line drugs is another challenge that this unit shoulders. 

    <div style="height:160px"></div>

</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

          

		</div>



</asp:Content>
