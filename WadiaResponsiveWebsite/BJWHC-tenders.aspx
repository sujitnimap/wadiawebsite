﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-tenders.aspx.cs" Inherits="WadiaResponsiveWebsite.child_tenders1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>TENDERS</h2>
                </div>

                <br />

                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px">

                    <div><b>Upcoming Tenders</b></div>

                    <%--<div class="devider_20px"></div>--%>

                    <asp:DataList ID="dtlistupcmingtenders" runat="server" Width="100%" RepeatColumns="1">
                       
                        <ItemTemplate>
                            <div class="devider_20px"></div>
                            <table width="100%" style="border: 1px solid #666" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftpad15" style="width: 10%">Sr No</td>
                                    <td class="leftpad15">Particular</td>
                                    <td class="leftpad15" style="width: 15%">Opening Date</td>
                                    <td class="leftpad15" style="width: 15%">Closing Date</td>
                                    <td class="leftpad15" style="text-align: center; width: 15%">Download Details</td>
                                </tr>
                                <tr>
                                    <td class="leftpad15">
                                        <asp:Label ID="lblsrno" runat="server" Text='<%#Container.ItemIndex + 1 %>'>'></asp:Label>
                                    </td>
                                    <td class="leftpad15">
                                        <asp:Label ID="lblname" runat="server" Text='<%#Eval("name") %>'></asp:Label>
                                    </td>
                                    <td class="leftpad15">
                                        <asp:Label ID="lblopenigdate" runat="server" Text='<%#Eval("openingdate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </td>
                                    <td class="leftpad15">
                                        <asp:Label ID="lblexpirydate" runat="server" Text='<%#Eval("expirydate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </td>

                                    <td class="leftpad15" style="text-align: center"><a target="_blank" href='/adminpanel/<%#Eval("downloadlink") %>'>
                                        <img src="images/pdf.jpg" /></a></td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:DataList>

                    <div class="devider_30px"></div>

                    <div><b>Past Tenders</b></div>


                    <asp:DataList ID="dtlistpasttenders" runat="server" Width="100%" RepeatColumns="1">
                        <ItemTemplate>
                            <div class="devider_20px"></div>
                            <table width="100%" style="border: 1px solid #666" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="leftpad15" style="width: 10%">Sr No</td>
                                    <td class="leftpad15">Particular</td>
                                    <td class="leftpad15" style="width: 15%">Opening Date</td>
                                    <td class="leftpad15" style="width: 15%">Closing Date</td>

                                    <td class="leftpad15" style="text-align: center; width: 15%">Download Details</td>
                                </tr>
                                <tr>
                                    <td class="leftpad15">
                                        <asp:Label ID="lblsrnopass" runat="server" Text='<%#Container.ItemIndex + 1 %>'>'></asp:Label>
                                    </td>
                                    <td class="leftpad15">
                                        <asp:Label ID="lblnamepass" runat="server" Text='<%#Eval("name") %>'></asp:Label>
                                    </td>
                                    <td class="leftpad15">
                                        <asp:Label ID="lblopenigdatepass" runat="server" Text='<%#Eval("openingdate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </td>
                                    <td class="leftpad15">
                                        <asp:Label ID="lblexpirydatepass" runat="server" Text='<%#Eval("expirydate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </td>

                                    <td class="leftpad15" style="text-align: center"><a target="_blank" href='/adminpanel/<%#Eval("downloadlink") %>'>
                                        <img src="images/pdf.jpg" /></a></td>
                                </tr>
                            </table>

                        </ItemTemplate>
                    </asp:DataList>
                </div>
                <!-- ABOUT US CONTENT -->





            </div>
        </div>







    </div>


</asp:Content>
