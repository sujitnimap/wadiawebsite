﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drsudha.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drsudha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Sudha Rao.jpg" />
<div class="docdetails">
Professor
<div class="docdetailsdesg">
MD, Fellowship in Paediatric Endocrinology

</div>

</div>
</div>

                    <div class="areaofinterest">

Areas of interest : Paediatric Endocrinology, Neonatology


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <%--<li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research or publications</a></li>--%>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
     •	Secretary- Treasurer, Indian Society of Paediatric and Adolescent Endocrinology.
<br />•	Authored Textbook on Paediatric Endocrinology
<br />•	Executive member , IAP Neochap on Specialty .
<br />•	National Trainer for NRP, PALS
<br />•	Gold medallist MD Paediatrics

<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			








		</div>
</asp:Content>
