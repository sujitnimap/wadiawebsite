﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-about-doctdetails-drFaram.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_doctdetails_drFaram" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
					<div class="col-full">

				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dr.jpg" />
<div class="docdetails">
Faram E. Irani

<div class="docdetailsdesg">
Hon. Consultant
   <br />
MD, FCPS, FCCP, DGO, DFP

</div>

</div>
</div>

     <%--<div class="areaofinterest">


</div>--%>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <%--<li><a href="#view4">Research and publications</a></li>--%>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
    •	Gold Medal for standing first in FCPS Examination in Bombay.
<br />•	Certificate of Merit from Bombay University for securing second rank in the M.D. Examination in Bombay.
<br />•	Award from the Centre of Applied Medicine in recognition of services.
<br />•	M. Shah prize for best paper presentation at the Annual Conference of the Bombay Obstetric & Gynaecology society.
<br />•	Indumati Jhaveri prize for best paper presentation at All India Obstetric & Gynaecology Conference.
<br />•	National Scholarship awarded by Ministry of Education of India.
<br />•	Certificate of Merit awarded by Rotary club.
<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
   •	Fellow of the College of Physicians and Surgeons.
<br />•	Founder Fellow of the Indian Academy of Juvenile and Adolescent Gynaecology and Obstetrics. .
<br />•	Life Member of the Bombay Obstetrics and Gynaecological Society.
<br />•	Life member of the Indian Association of Fertility and Sterility..
<br />•	Life member of the Indian Association of Gynaecological Endoscopists.
<br />•	Life member of the Indian Medical Association.
<br /><br />
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
				





				</div>
	


</asp:Content>
