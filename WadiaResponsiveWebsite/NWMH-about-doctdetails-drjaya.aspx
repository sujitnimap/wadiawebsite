﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-about-doctdetails-drjaya.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_doctdetails_drjaya" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Jaya Deshpande.jpg" />
<div class="docdetails">
Dr. Jaya Deshpande


<div class="docdetailsdesg">
Pathologist
   <br />
MD, PGDHMM

</div>

</div>
</div>

     <div class="areaofinterest">

Areas of Interest : Cardiovascular & Pulmonary pathology, Autopsy, Paediatric pathology


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
   •	Prize for best research paper
<br />•	Best teacher award.
<br />•	Certificate of merit in GS medical college.
<br />•	Organised 50th annual conference of IAPM, Mumbai in 2001  
<br />•	Member board of studies Mumbai University.
<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
    •	Member of Indian association of pathologists & Microbiologist (IAPM)
<br />•	Member Indian association of cytology
<br />•	Member Indian Cardiological Society of India.
<br /><br />
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
Over 70 research articles have been published in various Academic and Industry journals.

<br /><br />
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			


	</div>





</asp:Content>
