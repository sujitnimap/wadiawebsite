﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mainmaster.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WadiaResponsiveWebsite.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <!--------------Content--------------->


    <section id="content">
	<div class="wrap-content zerogrid">
		<div class="row block05">
			<!--<div class="title"><span>BLOG</span></div>-->
           
            <div style="padding:10px 10px 0px 10px;"> 
            <div class="col-full">
  
  
  
  
  
   <div id="wrapper">
     
        <div class="slider-wrapper theme-default">
            <div id="slider" class="nivoSlider">
                <img src="images/Index1.png" data-thumb="images/Index1.png" alt="" />
                <img src="images/Index2.png" data-thumb="images/Index2.png" alt="" />
                <img src="images/Index3.png" data-thumb="images/Index3.png" alt="" />
                <img src="images/Index4.png" data-thumb="images/Index4.png" alt="" />

            </div>
           
        </div>

    </div>
    <script type="text/javascript" src="themes/jquery-1.9.0.min.js"></script>
                <script type="text/javascript" src="themes/jquery.nivo.slider.js"></script>
                

    <script type="text/javascript">
        $(window).load(function () {
            $('#slider').nivoSlider();
        });
    </script>
   
   </div>
   </div>
   
   
  
   <div class="home_padding50">

       


  <div class="row block02">

      <div class="col-full">
<div class="titlewadiacontentgray">

Carrying forward the philanthropic tradition of the Wadia Family, The Wadia Hospitals provide world-class 
services for Paediatrics, Obstetrics & Gynaecology, making affordable healthcare accessible to people 
from every section of society, because we believe that each person is entitled to a happy and healthy life.
</div>
</div>  



<%--<div class="devider_10px"></div>--%>


<div class="col-1-2">
<div class="wrap-col">
					
		<div style="vertical-align:top">
<div class="titlewadia25gray"><a target="_blank" href="index-child.aspx">Child Care</a></div>

<div style="border:1px solid #999;"></div>
 <div class="devider_5px"></div>
 
  
 <div class="titlewadiacontentgray">
<a target="_blank" href="index-child.aspx"><img src="images/HomeChildrenCare.png" /></a>
 <div class="devider_5px"></div>

It is our aim at The Bai Jerbai Wadia Hospital for Children to ensure that each child is given the 
health-care, attention and love they deserve, regardless of the socio-economic background they are born into. 
We believe that every child should have the power to design their own destiny and until they can, 
we’re here to help. 

<div style="height:10px;"></div>

<div class="readmore"><a target="_blank" href="BJWHC.aspx">Enter <img src="images/IconReadMore.png" /></a> </div>


</div>
</div>              
   
                    
</div>
</div>


<div class="col-1-2">
<div class="wrap-col">






<div class="titlewadia25gray"><a target="_blank" href="index-women.aspx">Women Care</a></div>

<div style="border:1px solid #999;"></div>
 <div class="devider_5px"></div>
 
 <div class="titlewadiacontentgray">
<a target="_blank" href="index-women.aspx"><img src="images/HomeWomenCare.png" /></a>
 <div class="devider_5px"></div>

At The Nowrosjee Wadia Maternity Hospital we understand the constantly changing medical needs of a 
woman through the different stages of her life. It is our tireless endeavour, through our expert obstetric, 
neonatal and gynaecological care, to make sure that these needs are met through the comprehensive services 
that we provide.
<div style="height:9px;"></div>

<div class="readmore"><a target="_blank" href="index-women.aspx">Enter <img src="images/IconReadMore.png" /></a></div>
</div>

</div>



</div>
			
</div>
  
  
  </div><!-- Padding DIV end -->
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
            </div>
                            
			</div>
	
</section>

    <%--Section End--%>


</asp:Content>
