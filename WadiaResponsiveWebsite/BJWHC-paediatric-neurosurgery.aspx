﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-paediatric-neurosurgery.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_neurosurgery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Paediatric Neurosurgery </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         

The Division of Neurosurgery offers state of the art surgical management of disorders of the nervous system  and work in collaboration with paediatric neurologists. Complicated surgeries are carried out by highly experienced neurosurgeons. They are ably backed up by our expert anaesthetists.

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
The department of Paediatric Neurosurgery offers care and treatment to patients suffering from the following conditions :


                                <br /><br />
                <div style="padding-left:10px">

-	Surgical treatment of Epilepsy
<br />-	Surgical treatment of Spasticity
<br />-	Brain Tumours
<br />-	Spinal Cord Tumours
<br />-	Hydrocephalus (including minimally invasive endoscopic techniques)
<br />-	Head and Spine Trauma
<br />-	Myelomeningocele and other congenital spinal anomalies
<br />-	Chiari Malformations
<br />-	Craniosynostosis and Craniofacial Syndromes
<br />-	Arachnoid Cysts
<br />-	Vascular Malformations of the brain (Aneurysms and Arteriovenous Malformations) and spinal cord
<br />-	Peripheral nerve surgery (e.g. brachial plexus repair)
<br />-	Inflammatory/Infectious processes of the brain and spine (e.g. brain abscess)







</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>




		</div>





</asp:Content>
