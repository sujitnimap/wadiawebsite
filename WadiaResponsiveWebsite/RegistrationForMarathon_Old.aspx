﻿<%@ Page Title="Registration For Marathon" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="RegistrationForMarathon.aspx.cs" Inherits="WadiaResponsiveWebsite.RegistrationForMarathon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
        function validateChildCnt() {
            var chilMinorCnt = 0;
            var lblMessage = $("span[id$='lblMessage']");
            var isValid = true;
            $("#<%=gvwChild.ClientID%> tr:has(td)").each(function () {

                var childNameVal = $(this).find("td").get(0).childNodes[1].value;
                var childAgeVal = $(this).find("td").get(1).childNodes[1].value;
                var childGenderVal = $(this).find("td").get(2).childNodes[1].value;
                var childTShirtSizeVal = $(this).find("td").get(3).childNodes[1].value;
                if (childNameVal == "") {
                    isValid = false;
                }

                if (childAgeVal <= 10) {
                    chilMinorCnt = chilMinorCnt + 1;
                }

            });

            if (isValid == false) {
                lblMessage.html('Please enter child Name.')
                return false;
            }

            var isChecked = $('#chkTearmAndCondition').is(':checked');

            if (!isChecked) {
                lblMessage.html('Please check Terms And Conditions')
                return false;
            }

            var noOfParent = $("select[id$='ddlNoOfParents'").val();

            if (chilMinorCnt > noOfParent) {
                lblMessage.html('Please select the parents for child whose age between 5 to 10.')
                return false;
            }
            else {
                lblMessage.val();
                return true;
            }

            //var emailAddress = $("select[id$='txtEmailAddress'").va();
            //alert(emailAddress);
            //var confirmEmailAddress = $("select[id$='txtConfirmEmailAddress'").va();
            //alert(confirmEmailAddress);

            //if (emailAddress != confirmEmailAddress)
            //{
            //    lblMessage.html('Please enter correct email address')
            //    return false;
            //}

        }

        function ValidateEmptyVal(sender, args) {
            $("#<%=gvwChild.ClientID%> tr:has(td)").each(function () {

            var childAgeVal = $(this).find("td").get(0).childNodes[1].value;
            if (childAgeVal == "") {
                //alert('asdasdas');
                args.IsValid = false;
                sender.innerHTML = "asdasd";
            }

        });

    }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row block05">





        <div class="col-full">
            <div class="wrap-col">

                <div style="border: 1px solid #ddd; padding: 10px 20px 20px 10px; border-radius: 4px;">

                    <div class="heading">
                        <h2>Registration Form - 2015 Marathon</h2>
                    </div>


                    <br />


                    <div style="display: inline; padding-right: 35px">

                        <b style="font-style: normal; font-variant: normal; letter-spacing: normal; orphans: auto; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(102, 102, 102); font-family: Arial; font-size: 15px; line-height: 25px; background-color: rgb(255, 255, 255);">Total number of child participants </b><strong>:</strong>
                    </div>

                    <div style="display: inline">

                        <asp:DropDownList ID="ddlNoOfChildrens" CssClass="textboxcontact_drpdwn" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlNoOfChildrens_SelectedIndexChanged">
                            <asp:ListItem Selected="True" Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="3">5</asp:ListItem>
                            <asp:ListItem Value="4">6</asp:ListItem>
                        </asp:DropDownList>

                    </div>


                    <div class="devider_20px"></div>

                    <div style="display: inline; padding-right: 35px">

                        <b style="font-style: normal; font-variant: normal; letter-spacing: normal; orphans: auto; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(102, 102, 102); font-family: Arial; font-size: 15px; line-height: 25px; background-color: rgb(255, 255, 255);">Total number of adult participants </b><strong>: </strong>
                    </div>

                    <div style="display: inline">

                        <asp:DropDownList ID="ddlNoOfParents" CssClass="textboxcontact_drpdwn" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlNoOfParents_SelectedIndexChanged">
                            <asp:ListItem Selected="True" Value="0">0</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    
                    <div class="devider_20px"></div>

                    <asp:GridView ID="gvwChild" runat="server" OnRowDataBound="gvwChild_RowDataBound" Width="100%" BorderWidth="0" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField HeaderText="Child Participants name">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtChildName" CssClass="textboxcontact_grid" Text='<%# Eval("ChildName") %>' runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Age">
                                <ItemTemplate>
                                    <asp:DropDownList CssClass="textboxcontact_list" ID="ddlChildAge" runat="server">
                                        <asp:ListItem Selected="True" Text="3" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gender">
                                <ItemTemplate>
                                    <asp:DropDownList CssClass="textboxcontact_list" ID="ddlChildGender" runat="server">
                                        <asp:ListItem Selected="True" Text="M"></asp:ListItem>
                                        <asp:ListItem Text="F"></asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="T-Shirt Size(S/M/L)">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtChildTShirtSize" CssClass="textboxcontact_grid" Text='<%# Eval("TShirtSize") %>' MaxLength="2" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="School">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtChildSchool" CssClass="textboxcontact_grid" Text='<%# Eval("School") %>' runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField Visible="false" >
                                    <ItemTemplate>
                                        <asp:CustomValidator ID="custval1" runat="server" ValidationGroup="validateGrid" ClientValidationFunction="ValidateEmptyVal"></asp:CustomValidator>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                        </Columns>
                    </asp:GridView>



                    <div class="devider_20px"></div>

                    <asp:GridView ID="gvwParents" runat="server" Width="100%" BorderWidth="0" OnRowDataBound="gvwParents_RowDataBound" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField HeaderText="Adult Participants name">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtParentOrGuardianName" CssClass="textboxcontact_grid" Text='<%# Eval("AdultName") %>' runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Age">
                                <ItemTemplate>
                                    <asp:DropDownList CssClass="textboxcontact_list" ID="ddlParentAge" runat="server">
                                        <asp:ListItem Text="18" Selected="True" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                        <asp:ListItem Text="24" Value="24"></asp:ListItem>
                                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                        <asp:ListItem Text="26" Value="26"></asp:ListItem>
                                        <asp:ListItem Text="27" Value="27"></asp:ListItem>
                                        <asp:ListItem Text="28" Value="28"></asp:ListItem>
                                        <asp:ListItem Text="29" Value="29"></asp:ListItem>
                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                        <asp:ListItem Text="31" Value="31"></asp:ListItem>
                                        <asp:ListItem Text="32" Value="32"></asp:ListItem>
                                        <asp:ListItem Text="33" Value="33"></asp:ListItem>
                                        <asp:ListItem Text="34" Value="34"></asp:ListItem>
                                        <asp:ListItem Text="35" Value="35"></asp:ListItem>
                                        <asp:ListItem Text="36" Value="36"></asp:ListItem>
                                        <asp:ListItem Text="37" Value="37"></asp:ListItem>
                                        <asp:ListItem Text="38" Value="38"></asp:ListItem>
                                        <asp:ListItem Text="39" Value="39"></asp:ListItem>
                                        <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                        <asp:ListItem Text="41" Value="41"></asp:ListItem>
                                        <asp:ListItem Text="42" Value="42"></asp:ListItem>
                                        <asp:ListItem Text="43" Value="43"></asp:ListItem>
                                        <asp:ListItem Text="44" Value="44"></asp:ListItem>
                                        <asp:ListItem Text="45" Value="45"></asp:ListItem>
                                        <asp:ListItem Text="46" Value="46"></asp:ListItem>
                                        <asp:ListItem Text="47" Value="47"></asp:ListItem>
                                        <asp:ListItem Text="48" Value="48"></asp:ListItem>
                                        <asp:ListItem Text="49" Value="49"></asp:ListItem>
                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gender">
                                <ItemTemplate>
                                    <asp:DropDownList CssClass="textboxcontact_list" ID="ddlParentGender" runat="server">
                                        <asp:ListItem Selected="True" Text="M"></asp:ListItem>
                                        <asp:ListItem Text="F"></asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="T-shirt Size(S/M/L/XL)">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtParentTShirtSize" CssClass="textboxcontact_grid" Text='<%# Eval("TShirtSize") %>' MaxLength="2" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Organization">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtParentsOrganization" CssClass="textboxcontact_grid" Text='<%# Eval("Organization") %>' runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>



                    <div class="devider_20px"></div>


                    <asp:TextBox ID="txtEmailAddress" CssClass="textboxcontact_list" runat="server" placeholder="Email Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'"></asp:TextBox>


                    <div class="devider_20px"></div>



                    <asp:TextBox ID="txtConfirmEmailAddress" CssClass="textboxcontact_list" runat="server" placeholder="Confirm Email Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Confirm Email Address'"></asp:TextBox>



                    <div class="devider_20px"></div>



                    <asp:TextBox ID="txtMobileNumber1" CssClass="textboxcontact_list" runat="server" placeholder="Mobile Number(1)*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile Number(1)'"></asp:TextBox>


                    <div class="devider_20px"></div>



                    <asp:TextBox ID="txtMobileNumber2" CssClass="textboxcontact_list" runat="server" placeholder="Mobile Number(2)" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile Number(2)'"></asp:TextBox>


                    <div class="devider_20px"></div>



                    <asp:TextBox ID="txtAddress" TextMode="MultiLine" CssClass="textboxcontact_list" runat="server" placeholder="Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address'"></asp:TextBox>


                    <div class="devider_20px"></div>



                    <asp:TextBox ID="txtCity" CssClass="textboxcontact_list" runat="server" placeholder="City*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'City'"></asp:TextBox>


                    <div class="devider_20px"></div>



                    <asp:TextBox ID="txtState" CssClass="textboxcontact_list" runat="server" placeholder="State*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'State'"></asp:TextBox>


                    <div class="devider_20px"></div>

                    <div style="display: inline; padding-right: 35px">

                        <strong>Country * : </strong>
                    </div>


                    <div style="display: inline">
                        <asp:DropDownList CssClass="textboxcontact_drpdwn" ID="ddlCountrys" runat="server">
                            <asp:ListItem>Afghanistan</asp:ListItem>
                            <asp:ListItem>Aland Islands</asp:ListItem>
                            <asp:ListItem>Albania</asp:ListItem>
                            <asp:ListItem>Algeria</asp:ListItem>
                            <asp:ListItem>Andorra</asp:ListItem>
                            <asp:ListItem>Angola</asp:ListItem>
                            <asp:ListItem>Anguilla</asp:ListItem>
                            <asp:ListItem>Antartica</asp:ListItem>
                            <asp:ListItem>Antigua And Barbuda</asp:ListItem>
                            <asp:ListItem>Argentina</asp:ListItem>
                            <asp:ListItem>Armenia</asp:ListItem>
                            <asp:ListItem>Aruba</asp:ListItem>
                            <asp:ListItem>Ascension Island/St. Helena</asp:ListItem>
                            <asp:ListItem>Australia</asp:ListItem>
                            <asp:ListItem>Austria</asp:ListItem>
                            <asp:ListItem>Azerbaijan</asp:ListItem>
                            <asp:ListItem>Bahamas</asp:ListItem>
                            <asp:ListItem>Bahrain</asp:ListItem>
                            <asp:ListItem>Bangladesh</asp:ListItem>
                            <asp:ListItem>Barbados</asp:ListItem>
                            <asp:ListItem>Belarus</asp:ListItem>
                            <asp:ListItem>Belgium</asp:ListItem>
                            <asp:ListItem>Belize</asp:ListItem>
                            <asp:ListItem>Benin</asp:ListItem>
                            <asp:ListItem>Bermuda</asp:ListItem>
                            <asp:ListItem>Bhutan</asp:ListItem>
                            <asp:ListItem>Bolivia</asp:ListItem>
                            <asp:ListItem>Bosnia Herzegovina</asp:ListItem>
                            <asp:ListItem>Botswana</asp:ListItem>
                            <asp:ListItem>Bouvet Island</asp:ListItem>
                            <asp:ListItem>Brazil</asp:ListItem>
                            <asp:ListItem>British Virgin Islands</asp:ListItem>
                            <asp:ListItem>Brunei</asp:ListItem>
                            <asp:ListItem>Bulgaria</asp:ListItem>
                            <asp:ListItem>Burkina Faso</asp:ListItem>
                            <asp:ListItem>Burundi</asp:ListItem>
                            <asp:ListItem>Cambodia</asp:ListItem>
                            <asp:ListItem>Cameroon, United Republic Of</asp:ListItem>
                            <asp:ListItem>Canada</asp:ListItem>
                            <asp:ListItem>Cape Verde, Republic Of</asp:ListItem>
                            <asp:ListItem>Cayman Islands</asp:ListItem>
                            <asp:ListItem>Central African Republic</asp:ListItem>
                            <asp:ListItem>Chad</asp:ListItem>
                            <asp:ListItem>Chile</asp:ListItem>
                            <asp:ListItem>China</asp:ListItem>
                            <asp:ListItem>Christmas Island</asp:ListItem>
                            <asp:ListItem>Cocos Islands</asp:ListItem>
                            <asp:ListItem>Colombia</asp:ListItem>
                            <asp:ListItem>Comoros</asp:ListItem>
                            <asp:ListItem>Congo</asp:ListItem>
                            <asp:ListItem>Congo Democratic Republic Of</asp:ListItem>
                            <asp:ListItem>Cook Islands</asp:ListItem>
                            <asp:ListItem>Costa Rica</asp:ListItem>
                            <asp:ListItem>Croatia</asp:ListItem>
                            <asp:ListItem>Cuba</asp:ListItem>
                            <asp:ListItem>Cyprus</asp:ListItem>
                            <asp:ListItem>Czech Republic</asp:ListItem>
                            <asp:ListItem>Denmark</asp:ListItem>
                            <asp:ListItem>Djibouti</asp:ListItem>
                            <asp:ListItem>Dominica</asp:ListItem>
                            <asp:ListItem>Dominican Republic</asp:ListItem>
                            <asp:ListItem>Ecuador</asp:ListItem>
                            <asp:ListItem>Egypt</asp:ListItem>
                            <asp:ListItem>El Salvador</asp:ListItem>
                            <asp:ListItem>Equatorial Guinea</asp:ListItem>
                            <asp:ListItem>Eritrea</asp:ListItem>
                            <asp:ListItem>Estonia</asp:ListItem>
                            <asp:ListItem>Ethiopia</asp:ListItem>
                            <asp:ListItem>Faeroe Islands</asp:ListItem>
                            <asp:ListItem>Falkland Islands</asp:ListItem>
                            <asp:ListItem>Fiji Islands</asp:ListItem>
                            <asp:ListItem>Finland</asp:ListItem>
                            <asp:ListItem>France</asp:ListItem>
                            <asp:ListItem>French Guiana</asp:ListItem>
                            <asp:ListItem>French Polynesia</asp:ListItem>
                            <asp:ListItem>Gabon</asp:ListItem>
                            <asp:ListItem>Gambia</asp:ListItem>
                            <asp:ListItem>Georgia</asp:ListItem>
                            <asp:ListItem>Germany</asp:ListItem>
                            <asp:ListItem>Ghana</asp:ListItem>
                            <asp:ListItem>Gibraltar</asp:ListItem>
                            <asp:ListItem>Greece</asp:ListItem>
                            <asp:ListItem>Greenland</asp:ListItem>
                            <asp:ListItem>Grenada</asp:ListItem>
                            <asp:ListItem>Guadeloupe</asp:ListItem>
                            <asp:ListItem>Guam</asp:ListItem>
                            <asp:ListItem>Guatemala</asp:ListItem>
                            <asp:ListItem>Guernsey</asp:ListItem>
                            <asp:ListItem>Guinea</asp:ListItem>
                            <asp:ListItem>Guinea Bissau</asp:ListItem>
                            <asp:ListItem>Guyana</asp:ListItem>
                            <asp:ListItem>Haiti</asp:ListItem>
                            <asp:ListItem>Heard Island And McDonald Islands</asp:ListItem>
                            <asp:ListItem>Honduras</asp:ListItem>
                            <asp:ListItem>Hong Kong</asp:ListItem>
                            <asp:ListItem>Hungary</asp:ListItem>
                            <asp:ListItem>Iceland</asp:ListItem>
                            <asp:ListItem Selected="True">India</asp:ListItem>
                            <asp:ListItem>Indonesia</asp:ListItem>
                            <asp:ListItem>Iran</asp:ListItem>
                            <asp:ListItem>Iraq</asp:ListItem>
                            <asp:ListItem>Ireland, Republic Of</asp:ListItem>
                            <asp:ListItem>Isle Of Man</asp:ListItem>
                            <asp:ListItem>Israel</asp:ListItem>
                            <asp:ListItem>Italy</asp:ListItem>
                            <asp:ListItem>Ivory Coast</asp:ListItem>
                            <asp:ListItem>Jamaica</asp:ListItem>
                            <asp:ListItem>Japan</asp:ListItem>
                            <asp:ListItem>Jersey Island</asp:ListItem>
                            <asp:ListItem>Jordan</asp:ListItem>
                            <asp:ListItem>Kazakstan</asp:ListItem>
                            <asp:ListItem>Kenya</asp:ListItem>
                            <asp:ListItem>Kiribati</asp:ListItem>
                            <asp:ListItem>Korea, Democratic Peoples Republic</asp:ListItem>
                            <asp:ListItem>Korea, Republic Of</asp:ListItem>
                            <asp:ListItem>Kuwait</asp:ListItem>
                            <asp:ListItem>Kyrgyzstan</asp:ListItem>
                            <asp:ListItem>Lao, People&#39;s Dem. Rep.</asp:ListItem>
                            <asp:ListItem>Latvia</asp:ListItem>
                            <asp:ListItem>Lebanon</asp:ListItem>
                            <asp:ListItem>Lesotho</asp:ListItem>
                            <asp:ListItem>Liberia</asp:ListItem>
                            <asp:ListItem>Libyan Arab Jamahiriya</asp:ListItem>
                            <asp:ListItem>Liechtenstein</asp:ListItem>
                            <asp:ListItem>Lithuania</asp:ListItem>
                            <asp:ListItem>Luxembourg</asp:ListItem>
                            <asp:ListItem>Macau</asp:ListItem>
                            <asp:ListItem>Macedonia</asp:ListItem>
                            <asp:ListItem>Madagascar (Malagasy)</asp:ListItem>
                            <asp:ListItem>Malawi</asp:ListItem>
                            <asp:ListItem>Malaysia</asp:ListItem>
                            <asp:ListItem>Maldives</asp:ListItem>
                            <asp:ListItem>Mali</asp:ListItem>
                            <asp:ListItem>Malta</asp:ListItem>
                            <asp:ListItem>Marianna Islands</asp:ListItem>
                            <asp:ListItem>Marshall Islands</asp:ListItem>
                            <asp:ListItem>Martinique</asp:ListItem>
                            <asp:ListItem>Mauritania</asp:ListItem>
                            <asp:ListItem>Mauritius</asp:ListItem>
                            <asp:ListItem>Mayotte</asp:ListItem>
                            <asp:ListItem>Mexico</asp:ListItem>
                            <asp:ListItem>Micronesia</asp:ListItem>
                            <asp:ListItem>Moldova</asp:ListItem>
                            <asp:ListItem>Monaco</asp:ListItem>
                            <asp:ListItem>Mongolia</asp:ListItem>
                            <asp:ListItem>Montserrat</asp:ListItem>
                            <asp:ListItem>Morocco</asp:ListItem>
                            <asp:ListItem>Mozambique</asp:ListItem>
                            <asp:ListItem>Myanmar</asp:ListItem>
                            <asp:ListItem>Namibia</asp:ListItem>
                            <asp:ListItem>Nauru</asp:ListItem>
                            <asp:ListItem>Nepal</asp:ListItem>
                            <asp:ListItem>Netherland Antilles</asp:ListItem>
                            <asp:ListItem>Netherlands</asp:ListItem>
                            <asp:ListItem>New Caledonia</asp:ListItem>
                            <asp:ListItem>New Zealand</asp:ListItem>
                            <asp:ListItem>Nicaragua</asp:ListItem>
                            <asp:ListItem>Niger</asp:ListItem>
                            <asp:ListItem>Nigeria</asp:ListItem>
                            <asp:ListItem>Niue</asp:ListItem>
                            <asp:ListItem>Norfolk Island</asp:ListItem>
                            <asp:ListItem>Norway</asp:ListItem>
                            <asp:ListItem>Occupied Palestinian Territory</asp:ListItem>
                            <asp:ListItem>Oman, Sultanate Of</asp:ListItem>
                            <asp:ListItem>Pakistan</asp:ListItem>
                            <asp:ListItem>Palau</asp:ListItem>
                            <asp:ListItem>Panama</asp:ListItem>
                            <asp:ListItem>Papua New Guinea (Niugini)</asp:ListItem>
                            <asp:ListItem>Paraguay</asp:ListItem>
                            <asp:ListItem>Peru</asp:ListItem>
                            <asp:ListItem>Philippines</asp:ListItem>
                            <asp:ListItem>Pitcairn</asp:ListItem>
                            <asp:ListItem>Poland</asp:ListItem>
                            <asp:ListItem>Portugal</asp:ListItem>
                            <asp:ListItem>Qatar</asp:ListItem>
                            <asp:ListItem>Reunion</asp:ListItem>
                            <asp:ListItem>Romania</asp:ListItem>
                            <asp:ListItem>Russian Federation</asp:ListItem>
                            <asp:ListItem>Rwanda</asp:ListItem>
                            <asp:ListItem>Saint Lucia</asp:ListItem>
                            <asp:ListItem>Saint Vincent And The Grenadines</asp:ListItem>
                            <asp:ListItem>Samoa, American</asp:ListItem>
                            <asp:ListItem>Samoa, Independent State Of</asp:ListItem>
                            <asp:ListItem>San Marino</asp:ListItem>
                            <asp:ListItem>Sao Tome &amp; Principe</asp:ListItem>
                            <asp:ListItem>Saudi Arabia</asp:ListItem>
                            <asp:ListItem>Senegal</asp:ListItem>
                            <asp:ListItem>Serbia</asp:ListItem>
                            <asp:ListItem>Seychelles Islands</asp:ListItem>
                            <asp:ListItem>Sierra Leone</asp:ListItem>
                            <asp:ListItem>Singapore</asp:ListItem>
                            <asp:ListItem>Slovakia</asp:ListItem>
                            <asp:ListItem>Slovenia</asp:ListItem>
                            <asp:ListItem>Solomon Islands</asp:ListItem>
                            <asp:ListItem>Somalia</asp:ListItem>
                            <asp:ListItem>South Africa</asp:ListItem>
                            <asp:ListItem>Spain</asp:ListItem>
                            <asp:ListItem>Sri Lanka</asp:ListItem>
                            <asp:ListItem>St. Kitts - Nevis</asp:ListItem>
                            <asp:ListItem>St. Pierre &amp; Miquelon</asp:ListItem>
                            <asp:ListItem>Sudan</asp:ListItem>
                            <asp:ListItem>Suriname</asp:ListItem>
                            <asp:ListItem>Svalbard And Jan Mayen Is</asp:ListItem>
                            <asp:ListItem>Swaziland</asp:ListItem>
                            <asp:ListItem>Sweden</asp:ListItem>
                            <asp:ListItem>Switzerland</asp:ListItem>
                            <asp:ListItem>Syrian Arab Rep.</asp:ListItem>
                            <asp:ListItem>Taiwan, Republic of China</asp:ListItem>
                            <asp:ListItem>Tajikistan</asp:ListItem>
                            <asp:ListItem>Tanzania</asp:ListItem>
                            <asp:ListItem>Thailand</asp:ListItem>
                            <asp:ListItem>Timor Leste</asp:ListItem>
                            <asp:ListItem>Togo</asp:ListItem>
                            <asp:ListItem>Tonga</asp:ListItem>
                            <asp:ListItem>Trinidad &amp; Tobago</asp:ListItem>
                            <asp:ListItem>Tunisia</asp:ListItem>
                            <asp:ListItem>Turkey</asp:ListItem>
                            <asp:ListItem>Turkmenistan</asp:ListItem>
                            <asp:ListItem>Turks And Caicos Islands</asp:ListItem>
                            <asp:ListItem>Tuvalu</asp:ListItem>
                            <asp:ListItem>Uganda</asp:ListItem>
                            <asp:ListItem>Ukraine</asp:ListItem>
                            <asp:ListItem>United Arab Emirates</asp:ListItem>
                            <asp:ListItem>United Kingdom</asp:ListItem>
                            <asp:ListItem>United States</asp:ListItem>
                            <asp:ListItem>United States Minor Outlying Islnds</asp:ListItem>
                            <asp:ListItem>Uruguay</asp:ListItem>
                            <asp:ListItem>Uzbekistan Sum</asp:ListItem>
                            <asp:ListItem>Vanuatu</asp:ListItem>
                            <asp:ListItem>Vatican City State</asp:ListItem>
                            <asp:ListItem>Venezuela</asp:ListItem>
                            <asp:ListItem>Vietnam</asp:ListItem>
                            <asp:ListItem>Wallis &amp; Futuna Islands</asp:ListItem>
                            <asp:ListItem>Western Sahara</asp:ListItem>
                            <asp:ListItem>Yemen, Republic Of</asp:ListItem>
                            <asp:ListItem>Yugoslavia</asp:ListItem>
                            <asp:ListItem>Zambia</asp:ListItem>
                            <asp:ListItem>Zimbabwe</asp:ListItem>
                        </asp:DropDownList>

                    </div>


                    <div class="devider_20px"></div>

                    <div style="display: inline; padding-right: 35px">

                        <strong>Nationality *&nbsp; :</strong>
                    </div>

                    <div style="display: inline">

                        <asp:DropDownList ID="ddlNationalitys" CssClass="textboxcontact_drpdwn" runat="server">
                            <asp:ListItem>Afghanistan</asp:ListItem>
                            <asp:ListItem>Aland Islands</asp:ListItem>
                            <asp:ListItem>Albania</asp:ListItem>
                            <asp:ListItem>Algeria</asp:ListItem>
                            <asp:ListItem>Andorra</asp:ListItem>
                            <asp:ListItem>Angola</asp:ListItem>
                            <asp:ListItem>Anguilla</asp:ListItem>
                            <asp:ListItem>Antartica</asp:ListItem>
                            <asp:ListItem>Antigua And Barbuda</asp:ListItem>
                            <asp:ListItem>Argentina</asp:ListItem>
                            <asp:ListItem>Armenia</asp:ListItem>
                            <asp:ListItem>Aruba</asp:ListItem>
                            <asp:ListItem>Ascension Island/St. Helena</asp:ListItem>
                            <asp:ListItem>Australia</asp:ListItem>
                            <asp:ListItem>Austria</asp:ListItem>
                            <asp:ListItem>Azerbaijan</asp:ListItem>
                            <asp:ListItem>Bahamas</asp:ListItem>
                            <asp:ListItem>Bahrain</asp:ListItem>
                            <asp:ListItem>Bangladesh</asp:ListItem>
                            <asp:ListItem>Barbados</asp:ListItem>
                            <asp:ListItem>Belarus</asp:ListItem>
                            <asp:ListItem>Belgium</asp:ListItem>
                            <asp:ListItem>Belize</asp:ListItem>
                            <asp:ListItem>Benin</asp:ListItem>
                            <asp:ListItem>Bermuda</asp:ListItem>
                            <asp:ListItem>Bhutan</asp:ListItem>
                            <asp:ListItem>Bolivia</asp:ListItem>
                            <asp:ListItem>Bosnia Herzegovina</asp:ListItem>
                            <asp:ListItem>Botswana</asp:ListItem>
                            <asp:ListItem>Bouvet Island</asp:ListItem>
                            <asp:ListItem>Brazil</asp:ListItem>
                            <asp:ListItem>British Virgin Islands</asp:ListItem>
                            <asp:ListItem>Brunei</asp:ListItem>
                            <asp:ListItem>Bulgaria</asp:ListItem>
                            <asp:ListItem>Burkina Faso</asp:ListItem>
                            <asp:ListItem>Burundi</asp:ListItem>
                            <asp:ListItem>Cambodia</asp:ListItem>
                            <asp:ListItem>Cameroon, United Republic Of</asp:ListItem>
                            <asp:ListItem>Canada</asp:ListItem>
                            <asp:ListItem>Cape Verde, Republic Of</asp:ListItem>
                            <asp:ListItem>Cayman Islands</asp:ListItem>
                            <asp:ListItem>Central African Republic</asp:ListItem>
                            <asp:ListItem>Chad</asp:ListItem>
                            <asp:ListItem>Chile</asp:ListItem>
                            <asp:ListItem>China</asp:ListItem>
                            <asp:ListItem>Christmas Island</asp:ListItem>
                            <asp:ListItem>Cocos Islands</asp:ListItem>
                            <asp:ListItem>Colombia</asp:ListItem>
                            <asp:ListItem>Comoros</asp:ListItem>
                            <asp:ListItem>Congo</asp:ListItem>
                            <asp:ListItem>Congo Democratic Republic Of</asp:ListItem>
                            <asp:ListItem>Cook Islands</asp:ListItem>
                            <asp:ListItem>Costa Rica</asp:ListItem>
                            <asp:ListItem>Croatia</asp:ListItem>
                            <asp:ListItem>Cuba</asp:ListItem>
                            <asp:ListItem>Cyprus</asp:ListItem>
                            <asp:ListItem>Czech Republic</asp:ListItem>
                            <asp:ListItem>Denmark</asp:ListItem>
                            <asp:ListItem>Djibouti</asp:ListItem>
                            <asp:ListItem>Dominica</asp:ListItem>
                            <asp:ListItem>Dominican Republic</asp:ListItem>
                            <asp:ListItem>Ecuador</asp:ListItem>
                            <asp:ListItem>Egypt</asp:ListItem>
                            <asp:ListItem>El Salvador</asp:ListItem>
                            <asp:ListItem>Equatorial Guinea</asp:ListItem>
                            <asp:ListItem>Eritrea</asp:ListItem>
                            <asp:ListItem>Estonia</asp:ListItem>
                            <asp:ListItem>Ethiopia</asp:ListItem>
                            <asp:ListItem>Faeroe Islands</asp:ListItem>
                            <asp:ListItem>Falkland Islands</asp:ListItem>
                            <asp:ListItem>Fiji Islands</asp:ListItem>
                            <asp:ListItem>Finland</asp:ListItem>
                            <asp:ListItem>France</asp:ListItem>
                            <asp:ListItem>French Guiana</asp:ListItem>
                            <asp:ListItem>French Polynesia</asp:ListItem>
                            <asp:ListItem>Gabon</asp:ListItem>
                            <asp:ListItem>Gambia</asp:ListItem>
                            <asp:ListItem>Georgia</asp:ListItem>
                            <asp:ListItem>Germany</asp:ListItem>
                            <asp:ListItem>Ghana</asp:ListItem>
                            <asp:ListItem>Gibraltar</asp:ListItem>
                            <asp:ListItem>Greece</asp:ListItem>
                            <asp:ListItem>Greenland</asp:ListItem>
                            <asp:ListItem>Grenada</asp:ListItem>
                            <asp:ListItem>Guadeloupe</asp:ListItem>
                            <asp:ListItem>Guam</asp:ListItem>
                            <asp:ListItem>Guatemala</asp:ListItem>
                            <asp:ListItem>Guernsey</asp:ListItem>
                            <asp:ListItem>Guinea</asp:ListItem>
                            <asp:ListItem>Guinea Bissau</asp:ListItem>
                            <asp:ListItem>Guyana</asp:ListItem>
                            <asp:ListItem>Haiti</asp:ListItem>
                            <asp:ListItem>Heard Island And McDonald Islands</asp:ListItem>
                            <asp:ListItem>Honduras</asp:ListItem>
                            <asp:ListItem>Hong Kong</asp:ListItem>
                            <asp:ListItem>Hungary</asp:ListItem>
                            <asp:ListItem>Iceland</asp:ListItem>
                            <asp:ListItem Selected="True">India</asp:ListItem>
                            <asp:ListItem>Indonesia</asp:ListItem>
                            <asp:ListItem>Iran</asp:ListItem>
                            <asp:ListItem>Iraq</asp:ListItem>
                            <asp:ListItem>Ireland, Republic Of</asp:ListItem>
                            <asp:ListItem>Isle Of Man</asp:ListItem>
                            <asp:ListItem>Israel</asp:ListItem>
                            <asp:ListItem>Italy</asp:ListItem>
                            <asp:ListItem>Ivory Coast</asp:ListItem>
                            <asp:ListItem>Jamaica</asp:ListItem>
                            <asp:ListItem>Japan</asp:ListItem>
                            <asp:ListItem>Jersey Island</asp:ListItem>
                            <asp:ListItem>Jordan</asp:ListItem>
                            <asp:ListItem>Kazakstan</asp:ListItem>
                            <asp:ListItem>Kenya</asp:ListItem>
                            <asp:ListItem>Kiribati</asp:ListItem>
                            <asp:ListItem>Korea, Democratic Peoples Republic</asp:ListItem>
                            <asp:ListItem>Korea, Republic Of</asp:ListItem>
                            <asp:ListItem>Kuwait</asp:ListItem>
                            <asp:ListItem>Kyrgyzstan</asp:ListItem>
                            <asp:ListItem>Lao, People&#39;s Dem. Rep.</asp:ListItem>
                            <asp:ListItem>Latvia</asp:ListItem>
                            <asp:ListItem>Lebanon</asp:ListItem>
                            <asp:ListItem>Lesotho</asp:ListItem>
                            <asp:ListItem>Liberia</asp:ListItem>
                            <asp:ListItem>Libyan Arab Jamahiriya</asp:ListItem>
                            <asp:ListItem>Liechtenstein</asp:ListItem>
                            <asp:ListItem>Lithuania</asp:ListItem>
                            <asp:ListItem>Luxembourg</asp:ListItem>
                            <asp:ListItem>Macau</asp:ListItem>
                            <asp:ListItem>Macedonia</asp:ListItem>
                            <asp:ListItem>Madagascar (Malagasy)</asp:ListItem>
                            <asp:ListItem>Malawi</asp:ListItem>
                            <asp:ListItem>Malaysia</asp:ListItem>
                            <asp:ListItem>Maldives</asp:ListItem>
                            <asp:ListItem>Mali</asp:ListItem>
                            <asp:ListItem>Malta</asp:ListItem>
                            <asp:ListItem>Marianna Islands</asp:ListItem>
                            <asp:ListItem>Marshall Islands</asp:ListItem>
                            <asp:ListItem>Martinique</asp:ListItem>
                            <asp:ListItem>Mauritania</asp:ListItem>
                            <asp:ListItem>Mauritius</asp:ListItem>
                            <asp:ListItem>Mayotte</asp:ListItem>
                            <asp:ListItem>Mexico</asp:ListItem>
                            <asp:ListItem>Micronesia</asp:ListItem>
                            <asp:ListItem>Moldova</asp:ListItem>
                            <asp:ListItem>Monaco</asp:ListItem>
                            <asp:ListItem>Mongolia</asp:ListItem>
                            <asp:ListItem>Montserrat</asp:ListItem>
                            <asp:ListItem>Morocco</asp:ListItem>
                            <asp:ListItem>Mozambique</asp:ListItem>
                            <asp:ListItem>Myanmar</asp:ListItem>
                            <asp:ListItem>Namibia</asp:ListItem>
                            <asp:ListItem>Nauru</asp:ListItem>
                            <asp:ListItem>Nepal</asp:ListItem>
                            <asp:ListItem>Netherland Antilles</asp:ListItem>
                            <asp:ListItem>Netherlands</asp:ListItem>
                            <asp:ListItem>New Caledonia</asp:ListItem>
                            <asp:ListItem>New Zealand</asp:ListItem>
                            <asp:ListItem>Nicaragua</asp:ListItem>
                            <asp:ListItem>Niger</asp:ListItem>
                            <asp:ListItem>Nigeria</asp:ListItem>
                            <asp:ListItem>Niue</asp:ListItem>
                            <asp:ListItem>Norfolk Island</asp:ListItem>
                            <asp:ListItem>Norway</asp:ListItem>
                            <asp:ListItem>Occupied Palestinian Territory</asp:ListItem>
                            <asp:ListItem>Oman, Sultanate Of</asp:ListItem>
                            <asp:ListItem>Pakistan</asp:ListItem>
                            <asp:ListItem>Palau</asp:ListItem>
                            <asp:ListItem>Panama</asp:ListItem>
                            <asp:ListItem>Papua New Guinea (Niugini)</asp:ListItem>
                            <asp:ListItem>Paraguay</asp:ListItem>
                            <asp:ListItem>Peru</asp:ListItem>
                            <asp:ListItem>Philippines</asp:ListItem>
                            <asp:ListItem>Pitcairn</asp:ListItem>
                            <asp:ListItem>Poland</asp:ListItem>
                            <asp:ListItem>Portugal</asp:ListItem>
                            <asp:ListItem>Qatar</asp:ListItem>
                            <asp:ListItem>Reunion</asp:ListItem>
                            <asp:ListItem>Romania</asp:ListItem>
                            <asp:ListItem>Russian Federation</asp:ListItem>
                            <asp:ListItem>Rwanda</asp:ListItem>
                            <asp:ListItem>Saint Lucia</asp:ListItem>
                            <asp:ListItem>Saint Vincent And The Grenadines</asp:ListItem>
                            <asp:ListItem>Samoa, American</asp:ListItem>
                            <asp:ListItem>Samoa, Independent State Of</asp:ListItem>
                            <asp:ListItem>San Marino</asp:ListItem>
                            <asp:ListItem>Sao Tome &amp; Principe</asp:ListItem>
                            <asp:ListItem>Saudi Arabia</asp:ListItem>
                            <asp:ListItem>Senegal</asp:ListItem>
                            <asp:ListItem>Serbia</asp:ListItem>
                            <asp:ListItem>Seychelles Islands</asp:ListItem>
                            <asp:ListItem>Sierra Leone</asp:ListItem>
                            <asp:ListItem>Singapore</asp:ListItem>
                            <asp:ListItem>Slovakia</asp:ListItem>
                            <asp:ListItem>Slovenia</asp:ListItem>
                            <asp:ListItem>Solomon Islands</asp:ListItem>
                            <asp:ListItem>Somalia</asp:ListItem>
                            <asp:ListItem>South Africa</asp:ListItem>
                            <asp:ListItem>Spain</asp:ListItem>
                            <asp:ListItem>Sri Lanka</asp:ListItem>
                            <asp:ListItem>St. Kitts - Nevis</asp:ListItem>
                            <asp:ListItem>St. Pierre &amp; Miquelon</asp:ListItem>
                            <asp:ListItem>Sudan</asp:ListItem>
                            <asp:ListItem>Suriname</asp:ListItem>
                            <asp:ListItem>Svalbard And Jan Mayen Is</asp:ListItem>
                            <asp:ListItem>Swaziland</asp:ListItem>
                            <asp:ListItem>Sweden</asp:ListItem>
                            <asp:ListItem>Switzerland</asp:ListItem>
                            <asp:ListItem>Syrian Arab Rep.</asp:ListItem>
                            <asp:ListItem>Taiwan, Republic of China</asp:ListItem>
                            <asp:ListItem>Tajikistan</asp:ListItem>
                            <asp:ListItem>Tanzania</asp:ListItem>
                            <asp:ListItem>Thailand</asp:ListItem>
                            <asp:ListItem>Timor Leste</asp:ListItem>
                            <asp:ListItem>Togo</asp:ListItem>
                            <asp:ListItem>Tonga</asp:ListItem>
                            <asp:ListItem>Trinidad &amp; Tobago</asp:ListItem>
                            <asp:ListItem>Tunisia</asp:ListItem>
                            <asp:ListItem>Turkey</asp:ListItem>
                            <asp:ListItem>Turkmenistan</asp:ListItem>
                            <asp:ListItem>Turks And Caicos Islands</asp:ListItem>
                            <asp:ListItem>Tuvalu</asp:ListItem>
                            <asp:ListItem>Uganda</asp:ListItem>
                            <asp:ListItem>Ukraine</asp:ListItem>
                            <asp:ListItem>United Arab Emirates</asp:ListItem>
                            <asp:ListItem>United Kingdom</asp:ListItem>
                            <asp:ListItem>United States</asp:ListItem>
                            <asp:ListItem>United States Minor Outlying Islnds</asp:ListItem>
                            <asp:ListItem>Uruguay</asp:ListItem>
                            <asp:ListItem>Uzbekistan Sum</asp:ListItem>
                            <asp:ListItem>Vanuatu</asp:ListItem>
                            <asp:ListItem>Vatican City State</asp:ListItem>
                            <asp:ListItem>Venezuela</asp:ListItem>
                            <asp:ListItem>Vietnam</asp:ListItem>
                            <asp:ListItem>Wallis &amp; Futuna Islands</asp:ListItem>
                            <asp:ListItem>Western Sahara</asp:ListItem>
                            <asp:ListItem>Yemen, Republic Of</asp:ListItem>
                            <asp:ListItem>Yugoslavia</asp:ListItem>
                            <asp:ListItem>Zambia</asp:ListItem>
                            <asp:ListItem>Zimbabwe</asp:ListItem>
                        </asp:DropDownList>


                    </div>


                    <div class="devider_20px"></div>



                    <asp:TextBox ID="txtAnyOtherRelevantInformation" TextMode="MultiLine" Rows="5" CssClass="textboxcontact_list" runat="server" placeholder="Any other additional information" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Any other additional information'"></asp:TextBox>


                    <div class="devider_20px"></div>

                    <asp:CheckBox ID="chkIsSponsorARun" runat="server" Text="Sponsor a run" AutoPostBack="true" OnCheckedChanged="chkIsSponsorARun_CheckedChanged" />


                    <div class="devider_20px"></div>

                    <asp:Panel ID="pnlNoOfSponsorRun" runat="server" Visible="false">
                        How many
                            <asp:DropDownList ID="ddlNoOfSponsorRun" CssClass="textboxcontact_drpdwn" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlNoOfSponsorRun_SelectedIndexChanged">
                                <asp:ListItem Value="0" Text="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                            </asp:DropDownList>
                    </asp:Panel>



                    <div class="devider_10px"></div>


                    <div style="display: inline; padding-right: 35px">

                        <strong>Total Amount in INR : </strong>
                    </div>

                    <div style="display: inline">


                        <asp:Label ID="lblTotalAmountInINR" runat="server" Text="400"></asp:Label>


                    </div>


                    <div class="devider_10px"></div>


                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>


                    <div class="devider_20px"></div>



                    <input type="checkbox" id="chkTearmAndCondition" title="Terms And Conditions" />
                    Terms And Conditions


                 <div class="devider_20px"></div>


                    <asp:Button ID="btnPayment" Text="Pay now" CssClass="btn" runat="server" OnClientClick="return validateChildCnt();" OnClick="btnPayment_Click" />






                    <%--<table align="center" border="1" style="width:100%" >
                <tr>
                    <td>Child :
                    </td>
                    <td>
                       
                    </td>

                </tr>
                <tr>
                    <td>Adults :
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="width:100%">
                        

                    </td>

                </tr>
                <tr>
                    <td colspan="2" style="width:100%">
                        

                    </td>
                </tr>
                <tr>
                    <td>Email Address :</td>
                    <td>
                        </td>
                </tr>
                <tr>
                    <td>Confirm Email Address :</td>
                    <td>
                       </td>
                </tr>

                <tr>
                    <td>Mobile Number(1) :</td>
                    <td>
                      </td>

                </tr>
                <tr>
                    <td>Mobile Number(2) :</td>
                    <td>
                        </td>
                </tr>
                <tr>
                    <td>Address:
                    </td>
                    <td>
                        </td>
                </tr>
                <tr>
                    <td>City *</td>
                    <td>
                       </td>

                </tr>
                <tr>
                    <td>State *</td>
                    <td>
                       
                    </td>
                </tr>
                <tr>
                    <td>Country *</td>
                    <td>
                       
                    </td>
                </tr>
                <tr>
                    <td>Nationality *</td>
                    <td>
                        

                    </td>
                </tr>
              
                <tr>
                    <td>Any other additional information :
                    </td>
                    <td>
                       
                    </td>
                </tr>
                <tr>
                    <td>
                       
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>Total Amount in INR  :
                    </td>
                    <td>
                       
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                       
                    </td>
                </tr>
                <tr align="center">
                    <td align="right">
                        
                    </td>
                    <td align="left">
                        
                    </td>
                </tr>

            </table>--%>
                </div>

            </div>

        </div>



    </div>




</asp:Content>
