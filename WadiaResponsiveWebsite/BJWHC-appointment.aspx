﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-appointment.aspx.cs" Inherits="WadiaResponsiveWebsite.child_appointment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>APPOINTMENT</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">

Please note requesting an appointment through our website does not indicate confirmation of appointment. 
You will receive confirmation of appointment either through email or phone number mentioned in the form. 
For any queries please email us at <b><i>appointments@wadiahospitals.org</i></b> 
We look forward to serving you. Thank you.
<br />
<br />



</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
									<li><a href="child-about-overview.aspx">Overview</a></li>
									<li><a href="child-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="child-about-chairman-msg.aspx">Chairman's message</a></li>
									<li><a href="child-about-awards.aspx">Awards and Achievements</a></li>
									<li><a href="child-about-history.aspx">History </a>
                                        <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>







		</div>
</asp:Content>
