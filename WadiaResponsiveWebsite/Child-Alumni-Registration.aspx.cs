﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class Child_Alumni_Registration : System.Web.UI.Page
    {
        string flag = "c";

        SqlConnection con;
        SqlCommand cmd = new SqlCommand();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {
            AlumniRegistration objAlumniRegistration = new AlumniRegistration();
            COperationStatus os = new COperationStatus();

            if (rdbtnLife.Checked)
            {
                objAlumniRegistration.RegistrationCategory = "Lifetime";
            }
            if (rdbtnAnnual.Checked)
            {
                objAlumniRegistration.RegistrationCategory = "Annual";
            }
            if (rdbtnNRI.Checked)
            {
                objAlumniRegistration.RegistrationCategory = "NRI";
            }            
            objAlumniRegistration.Name = txtName.Text.ToString();
            if (rdbtnMale.Checked)
            {
                objAlumniRegistration.Gender = "Male";
            }
            if (rdbtnFemale.Checked)
            {
                objAlumniRegistration.Gender = "Female";
            }
            objAlumniRegistration.DOB = Convert.ToDateTime(txtDOB.Text.ToString());
            objAlumniRegistration.PermanentAdd = txtPermanentAddress.Text.ToString();
            objAlumniRegistration.Mobile = txtMobileNo.Text.ToString();
            objAlumniRegistration.TelNo = txtTelNo.Text.ToString();
            objAlumniRegistration.Email = txtEmailId.Text.ToString();
            objAlumniRegistration.ProfAddress = txtProfessionalAddress.Text.ToString();
            objAlumniRegistration.ProfTelNo = txtProfTelNo.Text.ToString();            
            objAlumniRegistration.LastModify = DateTime.Now;
            objAlumniRegistration.TotalAmount = int.Parse(hdfTotalPayableAmount.Value.ToString());

            os = CAlumniRegistration.Instance().InsertAlumniRegistration(objAlumniRegistration);

            if (os.Success == true)
            {
                AlumniEducation objAlumniEducation1 = new AlumniEducation();
                COperationStatus objAEStatus1 = new COperationStatus();

                objAlumniEducation1.AlumniId = objAlumniRegistration.AlumniId;
                objAlumniEducation1.College = txtCollege.Text.ToString();
                objAlumniEducation1.Degree = txtDegree.Text.ToString();
                objAlumniEducation1.JoinYear = txtYearOfJoining.Text.ToString();
                objAlumniEducation1.CompleteYear = txtYearOfCompletion.Text.ToString();
                objAlumniEducation1.LastModify = DateTime.Now;

                objAEStatus1 = CAlumniEducation.Instance().InsertAlumniEducation(objAlumniEducation1);

                AlumniEducation objAlumniEducation2 = new AlumniEducation();
                COperationStatus objAEStatus2 = new COperationStatus();

                objAlumniEducation2.AlumniId = objAlumniRegistration.AlumniId;
                objAlumniEducation2.College = txtCollege1.Text.ToString();
                objAlumniEducation2.Degree = txtDegree1.Text.ToString();
                objAlumniEducation2.JoinYear = txtYearOfJoining1.Text.ToString();
                objAlumniEducation2.CompleteYear = txtYearOfCompletion1.Text.ToString();
                objAlumniEducation2.LastModify = DateTime.Now;

                objAEStatus2 = CAlumniEducation.Instance().InsertAlumniEducation(objAlumniEducation2);


                Response.Redirect("AlumniPayURequest.aspx?Id=" + objAlumniRegistration.AlumniId);
            }
        }

        protected void cv_Gender(object source, ServerValidateEventArgs args)
        {
            args.IsValid = rdbtnMale.Checked || rdbtnFemale.Checked;
        }

        protected void cv_RegistrationCategory(object source, ServerValidateEventArgs args)
        {
            args.IsValid = rdbtnLife.Checked || rdbtnAnnual.Checked || rdbtnNRI.Checked;
        }
    }
}