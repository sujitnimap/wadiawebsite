﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drrujuta.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drrujuta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
		<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dr rujuta mehta.jpg" />
<div class="docdetails">
Dr. Rujuta Mehta 
<div class="docdetailsdesg">
Hon Ped. Orthopaedics
    <br />
M S (Ortho.), DNB (Ortho)



</div>

</div>
</div>

                    <div class="areaofinterest">
 Areas of Interest : Paediatric Hand Surgery, Paediatric Hand and Upper Limb Disorders
	


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
  •	Functional Analysis of Results of Early Pollicisation in Radial Club Hands: with Anomalous fingers. -awarded prestigious K.S.Masalawala Best Paper WIROC-2001. 

<br /><br />
•	Long Term results of single Stage Correction of Congenital Vertical Talus.-. Retrospective Study of 10 yrs. Awarded R.J.S.Panday best paper Award Fancon 2002.(Foot and Ankle Society of India).

<br /><br />
•	Natural History and Outcome Analysis in Varied Presentations of Obstetric Palsy-POSI CON BEST PAPER AWARD. JAN 2005.

<br /><br />
•	Marcela Uribe Zamudio Award for outstanding Lady orthopaedic Surgeon-to be presented at SICOT (Societe Internationale De Chirurgie Orthoapedeqe Et De Traumatologie ) Triennial World Congress Hong Kong August 2008.  

<br /><br />
•	Featured In 20-Women Achievers IN Modern Medicine –March 2009 issue.

<br /><br />
•	Veteran Surgeons Forum- WIROC  2010- Looking Beyond The shoulders of Giants.




<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	Case report: Hyperparathyroidism due to parathyroid adenoma Rujuta Mehta , Pispati Amit, J C Tarporvala. Bombay Hospital Journal: July 1995 Vol 37, No.3 pg.561-564
    <br /><br />
•	Case report: Adamantinoma Of the tibia Rujuta Mehta, Abhay Nene, JCTaraporvala. Bombay Hospital Journal: July 1995 Vol 37, No.3 pg.561-564
    <br /><br />
•	Review article: Current concepts in Obstetric palsy.Rujuta Mehta, Anirban Chaterjee,Kanti Shetty, Mukund Thatte. Bombay Hospital Journal .April 2003.Vol 45 No.2 Pg 320-324.   
    <br /><br />
•	Benign Juvenile Hemangioma: Case Report of an Unusual Presentation. Bombay Hospital Journal .July 2003..Vol 45 No 3.Pg 466-469.       
    <br /><br />
•	Long Term results of single Stage Correction of Congenital Vertical Talus. - Retrospective Study. Rujuta Mehta ,Kumar Dussa .M G Yagnik. Journal of Foot and Ankle Society of India Vol 3 Dec 2002 pg 16-171.



    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Case report: Hyperparathyroidism due to parathyroid adenoma Rujuta Mehta , Pispati Amit, J C Tarporvala. Bombay Hospital Journal: July 1995 Vol 37, No.3 pg.561-564
<br /><br />
•	Case report: Adamantinoma Of the tibia Rujuta Mehta, Abhay Nene, JCTaraporvala. Bombay Hospital Journal: July 1995 Vol 37, No.3 pg.561-564
<br /><br />
•	Review article: Current concepts in Obstetric palsy.Rujuta Mehta, Anirban Chaterjee,Kanti Shetty, Mukund Thatte. Bombay Hospital Journal .April 2003.Vol 45 No.2 Pg 320-324.   
<br /><br />
•	Benign Juvenile Hemangioma: Case Report of an Unusual Presentation. Bombay Hospital Journal .July 2003..Vol 45 No 3.Pg 466-469.       
<br /><br />
•	Long Term results of single Stage Correction of Congenital Vertical Talus. - Retrospective Study. Rujuta Mehta ,Kumar Dussa .M G Yagnik. Journal of Foot and Ankle Society of India Vol 3 Dec 2002 pg 16-171.
<br /><br />


<b>International Publications</b>
<br /><br />
•	Innovations article: New method of graft fixation for T.M. jt ankylosis
Journal of Plastic and Reconstructive Surgery Jan 2001 
<br /><br />
•	Use of UMEX fixator cum Distractor for primary and secondary treatment of Radial Club Hand with Ulnar Based Bilobed Flap; M R Thatte, Rujuta Mehta, Nitin Mokal, M G Yagnik. Scientific Transactions of Combined Congress APFFSH August 2000.Pg.136-138.
<br /><br />
•	Analysis of over 200 consecutive cases of adult posttraumatic Brachial Plexus Injury treated by one surgeon in one hospital from 1994 to 2004  
Scientific transactions IFFSH Budapest  June 2004.
<br /><br />
•	A 5 year follow-up of the modified Treatment of Radial Club Hand–with External Fixator cum Distractor and a Bilobed Flap  Dr Rujuta Mehta ,Dr M R Thatte.
Journal of Japanese Paediatric Orthopaedic Association November 2004
<br /><br />
•	Benign juvenile hemangioma—a case report . Acta Orthopaedica, Volume 77, Number 1, February 2006, pp. 171-173(3)Publisher: Taylor and Francis Ltd
<br /><br />
•	Gap Non Union In Osteogenesis Imperfecta : Role Of Maternal Fibular Graft’ : A Case Report. Journal Of Injury. Vol 4 Jan 2005 pg 231-236.
<br /><br />
•	Ultrasound Evaluation of Clubfoot Correction during Ponseti Technique-:Original Article :J Pediatr Ortho(A).Vol 27,No8 Dec 2007.
<br /><br />
•	Treatment of radial dysplasia by a combination of distraction, radialisation and a bilobed flap – the results at long term follow up-  Journal of Hand Surgery (European Volume) 2008; 33; 616.
<br /><br />
•	A new approach to Brachial plexus exposure- Accepted for Publication in “Techniques of Hand surgery.
<br /><br />

<b>Book Chapters</b>

                    <br /><br />
•	Ulnar hemimelia: pathogenesis, clinical features, treatment and rehabilitation-
<r></r>B.B. Joshi’s Text Book of Congenital Hand Anomalies.

•	Congenital Anomalies –Dr G S Kulkarni’s Text Book .Text Book of Orthoapedics and Trauma – jaypee borthers ,2nd  edition 2008. ISBN 978-81-8448-242-3.
<br /><br />
•	Genetics In Paediatric Orthopaedics. Dr G S Kulkarni’s Textbook. Chap 354. Text Book of Orthoapedics and Trauma – jaypee borthers ,2nd edition 2008. ISBN 978-81-8448-242-3.
<br /><br />
•	Streeter’s Dysplasia.-Dr G S Kulkarni’s Text Book. ISBN 978-81-8448-242-3.
<br /><br />

<b>Invited article</b>

                    <br /><br />
•	Management of Obstetric Brachial Plexus Injuries –What’s out? What’s in?
POSITIVE –quarterly News Letter of The Paediatric Orthopaedic Society of India. April2003
<br /><br />
•	Current concepts : fracture neck femur in children-MOA Journal 2009 oct issue.
<br /><br />
•	Osteotomies around the knee in children- Publication of Deformity correction conference for osteotomies of the Knee –update Jaslok  April 2010.

<br /><br />
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
         </div>



</asp:Content>
