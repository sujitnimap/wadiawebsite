﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-doctdetails-drmanish.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drmanish" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
		<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Manish Agarwal.jpg" />
<div class="docdetails">
 Dr. Manish Agarwal 
<div class="docdetailsdesg">
Asst. Honarary Ortho. Oncologist
    <br />
M.S. (Ortho) ; D (Ortho); D.N.B(Ortho)



</div>

</div>
</div>

                    <div class="areaofinterest">


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
   •	Seth Jairamdas Berry Gold Medal for the highest marks in Pharmacology at Bombay University in 1986.
<br /><br />•	Smt. Manorama Vijayraj Hazrat prize for highest aggregate marks at the 2nd MBBS examination of Bombay University in 1986.
<br /><br />•	Gold Medal for Orthopaedic Surgery 2004 conferred by the Special Executive Magistrates Society, Dec 2004
<br /><br />•	AA Mehta Gold Medal for the best paper at the 49th National conference of the Indian Orthopaedic Association, Dec 2004.
<br /><br />•	Best Paper Award : WIROC (Annual meeting of the Bombay Orthopaedic society) 98  :  For “Rotationplasty as a limb salvage procedure for malignant bone tumours” presented at Western India Regional Orthopaedic Conference which is the annual conference of the Bombay Orthopaedic Society.
<br /><br />•	Best Paper Award : WIROC 2002 for paper on “ The role of  freeze dried irradiated allografts in tumor reconstruction.
<br /><br />•	Best Paper Award : WIROC 2004 for paper on “Limb salvage surgery in very large tumors-is it worth the effort?”
<br /><br />•	Best Clinical meeting for 2004 and 2007 Shield conferred by Bombay Orthopaedic Society (BOS). 5 meetings are held in a year rotating through teaching medical colleges and institutes. Only two meeting were held at Tata Memorial and both times it was adjudged as the best clinical meeting of the year.
<br /><br />•	S.S Yadav Gold Medal for best Paper in Oncology in 2009 for paper on “ Joint-sparing or physis sparing diaphyseal resections” at IOACON 2009, The annual Conference of the Indian Orthopaedic Association
<br /><br />•	K.S Masalawala Best Paper Award 2010 for paper on “implant for non joint bone defect” presented at WIROC 2010, Annual Conference of the Bombay Orthopaedic Society
<br /><br />•	P.K Patwardhan Technology Development award for OrthoCAD Project from IIT Mumbai



<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	Member of Musculokeletal Tumor Society (North America)
  <br /><br />•	Member of Indian Orthopaedic Association, Indian Society of Oncology
  <br /><br />•	Reviewer for Clinical Orthopaedics and Related Research (CORR), Reviewer for Journal of Bone and Joint Surgery (JBJS Br)
  <br /><br />•	Appointed on board as Independent Non Executive Director for Zee Learn, an education company of the reputed Zee Group which deals with education at various levels and has brands like KIDZEE, ZIMA, ZICA, SMOKE, KIDZCARE, and KIDZEE HIGH. 


    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Karade V, Ravi B, Agarwal M. Extramedullary versus intramedullary tibial cutting guides in megaprosthetic total knee replacement. J Orthop Surg Res. 2012 Oct 2;7:33. doi: 10.1186/1749-799X-7-33.
<br /><br />
•	Jambhekar NA, Agarwal M, Suryawanshi P, Desai S, Rekhi B, Gulia A, Puri A. Osteosarcoma of the femur mimicking Ewing sarcoma/primitive neuroectodermal tumour on biopsy and metastatic carcinoma on resection. Skeletal Radiol. 2012 Sep;41(9):1163-8.
<br /><br />
•	Rekhi, Bharat; Ingle, Abhijeet; Agarwal, Manish; Puri, Ajay; Laskar, Siddharth; Jambhekar, Nirmala A. Alveolar soft part sarcoma 'revisited': clinicopathological review of 47 cases from a tertiary cancer referral centre, including immunohistochemical expression of TFE3 in 22 cases and 21 other tumours. Pathology. 2012 Jan;44(1):11-7.
<br /><br />
•	Suryawanshi P, Agarwal M, Dhake R, Desai S, Rekhi B, Reddy KB, Jambhekar NA. Phosphaturic mesenchymal tumor with chondromyxoid fibroma-like feature: an unusual morphological appearance. Skeletal Radiol. 2011 Nov;40(11):1481-5.
<br /><br />
•	Agarwal M, Puri A, Gulia A, Reddy K. Joint-sparing or Physeal-sparing Diaphyseal Resections: The Challenge of Holding Small Fragments. Clin Orthop Relat Res. 2010 Nov;468(11):2924-32.
<br /><br />
•	Agarwal MG, Gulia A, Ravi B, Ghayyar R, Puri A.: Revision of Broken Knee Megaprostheses: New Solution to Old Problems. Clin Orthop Relat Res. 2010 Nov;468(11):2904-13.
<br /><br />
•	Malhotra G, Agrawal A, Jambhekar NA, Sarathi V, Jagtap V, Agarwal MG, Kulkarni M, Asopa RV, Shah NS. Interesting image. The search for primary tumor in a patient with oncogenic osteomalacia: F-18 FDG PET resolves the conundrum. Clin Nucl Med. 2010 Nov;35(11):896-8.
<br /><br />
•	Puri A, Gulia A, Agarwal M, Jambhekar N, Laskar S. Extracorporeal irradiated tumor bone: A reconstruction option in diaphyseal Ewing's sarcomas. Indian J Orthop. 2010 Oct;44(4):390-6.
<br /><br />
•	Puri A, Gulia A, Agarwal M : Ulna translocation: an effective reconstruction modality after excision of Campanacci grade 3 giant cell tumours of distal radiusJ Bone Joint Surg Br. 2010 Jun;92(6):875-9.
<br /><br />
•	Gota VS, Maru GB, Soni TG, Gandhi TR, Kochar N, Agarwal MG. Safety and pharmacokinetics of a solid lipid curcumin particle formulation in osteosarcoma patients and healthy volunteers. J Agric Food Chem. 2010 Feb 24;58(4):2095-9.
<br /><br />
•	Uppin SG, Jambhekar N, Puri A, Kumar R, Agarwal M, Sanghvi D. Bone metastasis of glandular cardiac myxoma mimicking a metastatic carcinoma. Skeletal Radiol. 2010 Jul 1. 
<br /><br />
•	Jambhekar NA, Rekhi B, Thorat K, Dikshit R, Agrawal M, Puri A. Revisiting chordoma with brachyury, a "new age" marker: analysis of a validation study on 51 cases. Arch Pathol Lab Med. 2010 Aug;134(8):1181-7.
<br /><br />
•	Purandare NC, Rangarajan V, Agarwal M, Sharma AR, Shah S, Arora A, Parasar DS. Integrated PET/CT in evaluating sarcomatous transformation in osteochondromas. Clin Nucl Med. 2009 Jun;34(6):350-4.
<br /><br />
•	 K. Subburaj, B. Ravi, and M.G. Agarwal Automated identification of anatomical landmarks on 3D bone models reconstructed from CT scan images. Computerised Medical Imaging and Graphics. Volume 33, Issue 5, July 2009, Pages 359-368 
<br /><br />
•	Puri A, Shah M, Agarwal MG, Jambhekar NA, Basappa P. Chondrosarcoma of bone: does the size of the tumor, the presence of a pathologic fracture, or prior intervention have an impact on local control and survival? J Cancer Res Ther.2009 Jan-Mar;5(1):14-9.
<br /><br />
•	K. Subburaj, B. Ravi, and M.G. Agarwal, (2008) "3D Shape Reasoning for Identifying Anatomical Landmarks," Computer-Aided Design & Applications, 5(1-4), 2008, 153-160 
<br /><br />
•	Puri A, Subin BS, Agarwal MG. Fibular centralisation for the reconstruction of defects of the tibial diaphysis and distal metaphysis after excision of bone tumours. J Bone Joint Surg Br. 2009 Feb;91(2):234-9.
<br /><br />
•	Puri A, Agarwal MG, Shah M, Srinivas CH, Shukla PJ, Shrikhande SV, Jambhekar NA. Decision making in primary sacral tumors. Spine J. 2008 Dec 5.
<br /><br />
•	Gupta R, Seethalakshmi V, Jambhekar NA, Prabhudesai S, Merchant N, Puri A, Agarwal M. Clinicopathologic profile of 470 giant cell tumors of bone from a cancer hospital in western India. Ann Diagn Pathol. 2008 Aug;12(4):239-48. Epub 2008 Jan 11. 
<br /><br />
•	Rekhi B, Jambhekar NA, Puri A, Agrawal M, Chinoy RF: Clinicomorphologic features of a series of 10 cases of malignant triton tumors diagnosed over 10 years at a tertiary cancer hospital in Mumbai, India.  Ann Diagn Pathol. 2008 Apr;12(2):90-7. Epub 2007 Oct 24. 
<br /><br />•	Rekhi B, Jambhekar NA, Desai SB, Basak R, Puri A, Agrawal M. A t(X; 18) SYT-SSX2 positive synovial sarcoma in the pelvis of a young adult male: A rare case report with review of literature. Indian J Cancer. 2008 Apr-Jun;45(2):67-71. 
<br /><br />•	Agarwal M.G : Low cost Limb Reconstruction Surgery. Curr Opin Orthop 2007 18:561–571.
<br /><br />•	Agarwal M,  Anchan C, Shah M, Puri A, Pai S. Limb salvage surgery for osteosarcoma: effective low-cost treatment. Clin Orthop Relat Res. 2007 Jun;459:82-91.
<br /><br />•	Agarwal M, Puri A, Anchan C, Shah M, Jambhekar N. Hemicortical excision for low-grade selected surface sarcomas of bone. Clin Orthop Relat Res. 2007 Jun;459:161-6.
<br /><br />•	Agarwal M, Puri A, Anchan C, Shah M, Jambhekar N. Rotationplasty for bone tumors: is there still a role? Clin Orthop Relat Res. 2007 Jun;459:76-81.

<br /><br />•	Hingmire S. S., Pai S. K., Bakshi A. V., R. Bharath, Agarwal M., Puri A., Dangi U., Mandhaniya S., Maksud T. and Parikh P. M : Non-methotrexate based triple drug combination chemotherapy for untreated osteosarcoma. Journal of Clinical Oncology, 2007 ASCO Annual Meeting Proceedings (Post-Meeting Edition).
Vol 25, No 18S (June 20 Supplement), 2007: 20512

<br /><br />•	Puri A, Agarwal MG, Shah M, Jambhekar NA, Anchan C, Behle S.,: Giant cell tumor of bone in children and adolescents. J Pediatr Orthop. 2007 Sep;27(6):635-9.
 <br /><br />
•	Laskar S, Bahl G, Ann Muckaden M, Puri A, Agarwal MG, Patil N, Shrivastava SK, Dinshaw KA. Interstitial brachytherapy for childhood soft tissue sarcoma.Pediatr Blood Cancer. 2007 Oct 15;49(5):649-55.
<br /><br />
•	Laskar S, Bahl G, Puri A, Agarwal MG, Muckaden M, Patil N, Jambhekar N, Gupta S, Deshpande DD, Shrivastava SK, Dinshaw KA. Perioperative interstitial brachytherapy for soft tissue sarcomas: prognostic factors and long-term results of 155 patients. Ann Surg Oncol. 2007 Feb;14(2):560-7.
<br /><br />
•	Puri Ajay, Agarwal M.G : Facilitating Rotationplasty. J Surg Oncol. 2007 Mar 15;95(4):351-4.
<br /><br />
•	Puri Ajay, Agarwal Manish : Use of Polypropylene Mesh to Stabilize Skeletal Reconstructions After Resection for Bone Tumors. Surg Oncol. 2007 Feb 1;95(2):158-60.
<br /><br />
•	Puri A, Anchan C, Jambhekar NA, Agarwal MG, Badwe RA. Recurrent gossypiboma in the thigh. Skeletal Radiol. 2007 Jun;36 Suppl 1:S95-100.
<br /><br />
•	Khapake DP, Jambhekar NA, Anchan C, Madur BP, Chinoy RF, Agarwal M, Puri A. Epithelioid sarcoma of the foot with subsequent lesion in hand: metastatic lesion or second primary? Indian J Pathol Microbiol. 2007 Jul;50(3):563-5. 
<br /><br />
•	Pant V, Jambhekar NA, Madur B, Shet TM, Agarwal M, Puri A, Gujral S, Banavali M, Arora B. Anaplastic large cell lymphoma (ALCL) presenting as primary bone and soft tissue sarcoma--a study of 12 cases. Indian J Pathol Microbiol. 2007 Apr;50(2):303-7. 
<br /><br />
•	Sanghvi DA, Purandare NC, Jambhekar NA, Agarwal MG, Agarwal A. Diffuse-type giant cell tumor of the subcutaneous thigh. Skeletal Radiol. 2007 Apr;36(4):327-30. Epub 2006 Mar 25. 
<br /><br />
•	Ghule P, Kadam PA, Jambhekar N, Bamne M, Pai S, Nair C, Banavali S, Puri A, Agarwal M.p53 gene gets altered by various mechanisms: studies in childhood sarcomas and retinoblastoma. Med Sci Monit. 2006 Dec;12(12):BR385-396. Epub 2006 Nov 23.
<br /><br />
•	Agarwal M.G, Puri A. Management of Sarcomas in a developing country. Focus on large tumors. Orthopaedic Knowledge Online (OKO), Publication of the American Orthopaedic Association. Nov 2006.
<br /><br />
•	Desai SS, Jambhekar N, Agarwal M, Puri A, Merchant N.Adamantinoma of tibia: a study of 12 cases.J Surg Oncol. 2006 Apr 1;93(5):429-33.
<br /><br />•	Panchwagh Yogesh, Puri A, Agarwal MG, Chinoy R, Jambhekar N.A, : metastatic adamantinoma of the Tibia”an unusual presentation: a case report : Skeletal Radiol. 2006 Mar;35(3):190-3. 
<br /><br />•	Puri A, Shingade V.U, Agarwal MG, Juvekar S, Desai S and  Jambhekar N.A: CT guided percutaneous core needle biopsy in deep seated musculoskeletal lesions: a prospective study of 128 cases. Skeletal Skeletal Radiol. 2006 Mar;35(3):138-43
<br /><br />•	Qureshi SS, Puri A, Agarwal M, Desai S, Jambhekar N.: Recurrent giant cell tumor of bone with simultaneous regional lymph node and pulmonary metastases. Skeletal Radiol. 2004 Sep 10
<br /><br />•	Chaturvedi P, Puri A, Agarwal MG et al.: Pathological fracture in a pregnant woman. Postgraduate Medical Journal, 2004 Dec;80(950):740
<br /><br />•	Jambhekar NA, Desai SS, Puri A, Agarwal M: Florid reactive periostitis of the hands. Skeletal Radiol. 2004 Nov; 33(11):663-5
<br /><br />•	Qureshi SS, Puri A, Agarwal M, Merchant NH, Sheth T, Jambhekar N.: Unusual late sequel of ruptured distal tendon of biceps brachii mimicking a soft-tissue tumor. Skeletal Radiol. 2004 Jul;33(7):417-20.
<br /><br />•	Gajiwala Lobo A, Agarwal M, Puri A, D'Lima C, Duggal A : The use of irradiated allografts in reconstruction of tumor defects - the tata memorial hospital experience. Cell Tissue Bank.2003;4(2-4):125-32.
<br /><br />•	Gajiwala Lobo A, Agarwal M, Puri A, D'Lima C, Duggal A.: Reconstructing tumour defects: lyophilised, irradiated bone allografts. Cell Tissue Bank. 2003;4(2-4):109-18
<br /><br />•	Pramesh CS, Deshpande MS, Pardiwala DN, Agarwal MG, Puri A.: Core needle biopsy for bone tumours. Eur J Surg Oncol. 2001 Nov;27(7):668-71
<br /><br />•	Agarwal MG, Puri A. Limb salvage for malignant primary bone tumours: current status with a review of the literature. Indian J Surg 2003;65:354-60.
<br /><br />•	Pardiwala D, Agarwal M, Puri A, Vyas S.:  Coexisting chondroblastoma and osteochondroma: a case report. J Postgrad Med. 2002 Apr-Jun;48(2):127-8.
<br /><br />•	Agarwal MG, Prabhudesai SG, Jambhekar NA, Duggal A and Puri A : "Bizarre parosteal osteochondromatous proliferation of the phalanx" - SICOT Online Report E020 Accepted January 21st, 2003 
<br /><br />•	Agarwal MG. Puri A: Giant Cell Tumor of Bone : Reinforcing old principles and unraveling controversies. Journal of Delhi Orthopaedic Association, Jan 2005
<br /><br />•	Agarwal M.G, Puri A: Biopsy for bone and soft tissue tumors.  Journal of Delhi Orthopaedic Association, Jan 2007
<br /><br />•	Agarwal MG, Puri Ajay, Jambhekar N.A et al. Limb Salvage for malignant primary bone tumors : Current status with a review of literature. Journal of Bone and Joint diseases, Vol 20, Dec 2004, pp9-25.
<br /><br />•	Puri A, Agarwal MG : Giant Cell Tumor of Bone: North East ORTHOSCAN, Official Journal of NEROSA - Vol 3 Jan 2004
<br /><br />•	Duggal A, Gupta S, Puri A, Agarwal MG:  Large Chondrosarcomas of the shoulder girdle - a report of 3 cases in the Bombay Hospital Journal – Vol 45, No: 2, 2003.
<br /><br />•	Agarwal MG, Puri A: Limb Salvage for osteosarcoma around the knee. Journal of Kolhapur Orthopaedic Association- Year I,Viii, Aug 2002.
<br /><br />•	Puri A,  Agarwal M, Mohan A, and Dinshaw KA:  Limb Salvage with an Indian Prosthesis TMH-NICE; Nuclear India, Vol 36, Nov Dec 2002.
<br /><br />•	Parasnis A.S, Duggal A , Navadgi S, Puri A, Agarwal M.G ,Desai S.B: Spinal metastasis of intermediate grade chondrosarcoma without pulmonary involvement : Sicot Online Reports: July 2002
<br /><br />•	Pardiwala DN, Vyas S, Puri A, Agarwal MG:  Giant Cell Tumor – A Pictorial Essay; Ind J Radiol Imag 2001 11:3:119-126
<br /><br />•	Gajjar SM, Agarwal MG, Aroojis A, Saraf ML: Abduction osteotomy for nonunion fracture neck femur – a new look : Indian Journal of Orthopaedics : Vol34 : No4 : Jan 2000 : pp30-34
<br /><br />•	Badhwar R, Agarwal M:  Rotationplasty as a limb salvage procedure for malignant bone tumours. Int Orthop. 1998;22(2):122-5.
<br /><br />
<b>Books </b>
<br /><br />
•	Osteosarcoma Intech.com, open access. Edited by Manish Agarwal, ISBN 978-953-51-0506-0, Hard cover, 174 pages, Publisher: InTech, Published: April 11, 2012 under CC BY 3.0 license, in subject Oncology. DOI: 10.5772/1266
<br /><br />This book is aimed at quickly updating the reader on osteosarcoma, a dreaded primary bone cancer. Progress in management of osteosarcoma has been slow after the evolution of chemotherapy and limb salvage surgery. Research is now directed towards identifying molecular targets for systemic therapy. Availability of chemotherapy drugs and low cost implants in developing world have allowed limb salvage surgery to develop. This book looks at current basic knowledge on osteosarcoma and some of the developments in research which have the potential to change the prognosis.
•	Puri Ajay, Agarwal M.G : “Current Concepts in Bone and Soft tissue Tumors”. Textbook editors. 1st edition,2006,  Paras Medical Books. 
<br /><br />


<b>Book Chapters : </b>
<br /><br />
•	Tumors of the Foot and ankle: In Foot and ankle surgery, Edited by Selene Parekh, Jaypee publications, February 2012
<br /><br />•	Agarwal Manish & Nayak Prakash :Limb Salvage for Osteosarcoma: Current Status with a Review of Literature. In ‘Osteosarcoma’, : InTech, Published: April 11, 2012
<br /><br />•	Ravi B, Agarwal MG: Chapter titled "Computer-Aided Development of Mega Endo-Prostheses" for the book titled "Bio Materials & Prototyping Materials in Medicine”. Published by Springer. Edited By Bopaya Bidanda & Paulo Bartolo, Department of Industrial Engineering, University of Pittsburgh, Pittsburgh PA 15261, Published by Springer Nov 2007


                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			



         </div>
</asp:Content>
