﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-reprodclinic.aspx.cs" Inherits="WadiaResponsiveWebsite.women_reprodclinic" %>


<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .button {
            border: 0;
            border-radius: 0.25rem;
            background: #2186de;
            color: white;
            font-family: -system-ui, sans-serif;
            font-size: 1rem;
            line-height: 1.2;
            white-space: nowrap;
            text-decoration: none;
            padding: 0.25rem 0.5rem;
            margin: 0.25rem;
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%--<asp:ToolkitScriptManager runat="server"></asp:ToolkitScriptManager>
                            <asp:ValidationSummary ID="vsCMEReg" runat="server" DisplayMode="BulletList"
        ShowSummary="false" ShowMessageBox="true" ValidationGroup="vgCME" />--%>

    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>Reproductive and infertility clinic </h2>
                </div>

                <br />

                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px">



                    <div class="scrollcontent">
                        <div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">


                            <ul class="tabs" data-persist="true">
                                <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
                                <li><a href="#view2">Overview</a></li>
                            </ul>

                            <div class="scrollbar" id="ex3">
                                <div class="tabcontents">
                                    <!---- 1st view end -->
                                    <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>


                                    <!---- 1st view end ---->

                                    <!---- 2nd view start ---->

                                    <div id="view2">
                                        At the reproductive and infertility clinics, our trained staff offers confidential advice and assistance to individuals or couples who wish to become parents but have been unable to, due to medical reasons. At the clinic, our doctors discuss the needs and problems of our patients and conduct diagnostic tests. Once they have achieved a complete understanding of the patient’s condition, they offer advance medical treatments to obtain the desired conceptions and pregnancies. 
  <br />
                                        <br />
                                        <b>Services Provided:</b>
                                        <br />
                                        <br />
                                        <div style="padding-left: 25px">
                                            -	In Vitro Fertilization (IVF)
 <br />
                                            -	Embryo and oocyte cryopreservation
 <br />
                                            -	Assisted hatching
 <br />
                                            -	Intracytoplasmic sperm injection (ICSI)
 <br />
                                            -	Preimplantation genetic diagnosis (PGD)
 <br />
                                            -	Basic work up of infertile couple.
 <br />
                                            -	Male Infertile Management.
 <br />
                                            -	Super ovulation protocols.
 <br />
                                            -	Fertility Enhancing Surgeries.
 <br />
                                            -	Artificial Reproductive Technique.
 <br />
                                            -	Folliculometry.

                                        </div>


                                        <br />
                                        <br />


                                    </div>

                                    <!---- 2nd view end ---->


                                </div>
                            </div>

                            <br />

                            <div style="text-align: center;"><a class="button" href="Reproductive-and-Infertility-clinic-form.aspx">Click To Pay </a></div>

                            <%-- <table>
                                <tr>
                                    <td>Patient Name </td>
                                    <td>
                                        <asp:TextBox ID="txtFirstName" CssClass="textboxcontactcme" MaxLength="30" required="" runat="server" placeholder="First Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvFirstName" ValidationGroup="vgCME" ControlToValidate="txtFirstName" runat="server" ForeColor="Red" ErrorMessage="Please Enter First Name">*</asp:RequiredFieldValidator>

                                        &nbsp;&nbsp;
                           
                                <asp:TextBox ID="txtLastName" CssClass="textboxcontactcme" MaxLength="30" required="" runat="server" placeholder="Last Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvLastName" ValidationGroup="vgCME" ControlToValidate="txtLastName" runat="server" ForeColor="Red" ErrorMessage="Please Enter Last Name">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>OPD Number</td>
                                    <td>
                                        <asp:TextBox ID="txtOPDNo" CssClass="textboxcontactcme" MaxLength="20" required="" runat="server" placeholder="OPD Number*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'OPD Number'">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvOPDNo" ValidationGroup="vgCME" ControlToValidate="txtOPDNo" runat="server" ForeColor="Red" ErrorMessage="Please Enter OPD Number">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>
                                        <asp:TextBox ID="txtAddress" TextMode="MultiLine" MaxLength="500" CssClass="textboxcontactcme" runat="server" placeholder="Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address'">
                                        </asp:TextBox>

                                        <asp:RequiredFieldValidator ID="rfvAddress" ValidationGroup="vgCME" ControlToValidate="txtAddress" runat="server" ForeColor="Red" ErrorMessage="Please Enter Address">*</asp:RequiredFieldValidator>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Mobile Number</td>
                                    <td>
                                        <asp:TextBox ID="txtMobileNo" CssClass="textboxcontactcme" MaxLength="10" required=""
                                            runat="server" placeholder="Mobile Number*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile Number'"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvMobileNo" ValidationGroup="vgCME"
                                            ControlToValidate="txtMobileNo" ForeColor="Red" runat="server" ErrorMessage="Please enter your Mobile Number">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revMobileNo" Display="None" ValidationGroup="vgCME" ValidationExpression="^[0-9]{10}$"
                                            ControlToValidate="txtMobileNo" SetFocusOnError="true" runat="server" ErrorMessage="Enter 10 digit numeric value in Mobile Number"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Age</td>
                                    <td>
                                        <asp:TextBox ID="txtAge" CssClass="textboxcontactcme" required="" MaxLength="2" runat="server" placeholder="Age*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Age'">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvAge" ValidationGroup="vgCME"
                                            ControlToValidate="txtAge" ForeColor="Red" runat="server" ErrorMessage="Please enter your Age">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revAge" Display="None" ValidationGroup="vgCME" ValidationExpression="^[0-9]{2}$"
                                            ControlToValidate="txtAge" SetFocusOnError="true" runat="server" ErrorMessage="Enter numeric value in Age"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Amount</td>
                                    <td>
                                        <asp:TextBox ID="txtAmount" CssClass="textboxcontactcme" required="" MaxLength="10" runat="server" placeholder="Amount*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Amount'">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvAmount" ValidationGroup="vgCME"
                                            ControlToValidate="txtAmount" ForeColor="Red" runat="server" ErrorMessage="Please enter Amount">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revAmount" Display="None" ValidationGroup="vgCME" ValidationExpression="^[0-9]*$"
                                            ControlToValidate="txtAmount" SetFocusOnError="true" runat="server" ErrorMessage="Enter numeric value in Amount"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>

                            <br />
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:Button ID="btnPayment" Text="Pay now" ValidationGroup="vgCME" CssClass="btn" runat="server" OnClick="btnPayment_Click" />--%>
                        </div>

                    </div>










                </div>
                <!-- ABOUT US CONTENT -->



            </div>
        </div>




    </div>




</asp:Content>
