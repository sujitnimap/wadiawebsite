﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster1.Master" AutoEventWireup="true" CodeBehind="index-child1.aspx.cs" Inherits="WadiaResponsiveWebsite.index_child1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			<!--<div class="title"><span>BLOG</span></div>-->
            
           
           <div style="padding:20px 20px 0px 20px;">
            <div class="col-full">
            
            
        <div id="wrapper">
     
        <div class="slider-wrapper theme-default">
            <div id="slider" class="nivoSlider">
                <img src="images/child-banner1.png" data-thumb="images/child-banner1.png" alt="" />
                <img src="images/child-banner2.png" data-thumb="images/child-banner2.png" alt="" />
                <img src="images/child-banner3.png" data-thumb="images/child-banner3.png" alt="" />
               <%-- <img src="images/BJWHC1.4.png" data-thumb="images/HomeBanner3.3.png" alt="" />--%>
            </div>
           
        </div>

    </div>
    <script type="text/javascript" src="themes/jquery-1.9.0.min.js"></script>
                <script type="text/javascript" src="themes/jquery.nivo.slider.js"></script>
                

    <script type="text/javascript">
        $(window).load(function () {
            $('#slider').nivoSlider();
        });
    </script>
   
   </div>
   </div>
   
   




    <div class="row block02">

<div class="col-1-3">
<div class="wrap-col">
					
<div class="boxhome" style="background:#bfcd06;">
<div class="titlewadia">  <img src="images/IconTestimonial.png" width="40px" height="40px" /> <b>Testimonials</b></div>
<div class="devider_5px"></div>
Here are a few things some of our patients and partners have to say, about their journey with BJWHC
        <br>
<div align="right"><img src="images/IconGoRightArrow.png"></div>
 </div>

</div>
</div>


<div class="col-1-3">
<div class="wrap-col">
					
		 <div class="boxhome" style="background:#7ad8fb">
		 <div class="titlewadia">   <img src="images/IconDonations.png" width="30px" height="30px" /> <b>Contribute </b></div>
<%--<div class="devider_5px"></div>--%>

Help us fulfil our promise to the children. A promise of quality healthcare for a healthy and, more importantly, happy life

         <div align="right"><img src="images/IconGoRightArrow.png"></div>
         
		 </div>               
                    
</div>
</div>


<div class="col-1-3">
<div class="wrap-col">

<div class="boxhome" style="background-image:url(images/gallery@.png); background-repeat:no-repeat;">
<div class="titlewadia" style="padding-left:70px; padding-top:8px;"> <b> Gallery</b></div>

<div style="height:109px"></div>

</div>
</div>				
                
                
</div>
</div>
			



      
<div class="row block02">

<div class="col-1-3">
<div class="wrap-col">
					

<div class="colorcontent">
<div class="titlewadia25">About Us</div>

<div style="border:1px solid #999; width:100%;"></div>
 <div class="devider_10px"></div>
 
Before the onset of Paediatrics as an independent discipline, childcare was limited to interpolating adult medication on to children, who were seen merely as mini versions of adults. In 1929, Sir Ness Wadia and Sir Cusrow Wadia built The Bai Jerbai Wadia Hospital for Children, in memory of their mother Bai Jerbai Wadia. Emerging as India’s first specialized Paediatrics hospital, it was dedicated exclusively to healthcare for children. 

<div class="devider_10px"></div>

Located in the heart of the city, the hospital pledges to extend its services to people, regardless of their socio-economic status, believing strongly in the fact that quality healthcare should not be restricted to only certain sections of society.
<div class="devider_20px"></div>
<div style="text-align:right">

    <a href="child-about-overview.aspx" class="readmorebtngrns"><img src="images/readmore1.png" /></a>


                                              </div>



</div>


</div>
</div>


<div class="col-1-3">
<div class="wrap-col">
					
<div class="colorcontent"> 
<div class="titlewadia25">Events</div>

<div style="border:1px solid #999; "></div>
 <div class="devider_10px"></div>
 

We organize several fundraising events, medical conferences, Continuing Medical Education Programs and other events throughout the year, in different parts of the city, to raise awareness and funds for causes that are close to our hearts. 
  <div class="devider_10px"></div>


    <%-- <div class="readmorecase" style="display:inline-block; width:100%;"><a href="child-events.aspx">Read More <img src="images/IconReadMore.png" width="25px" height="25px" /></a> </div>--%>

   <div style="text-align:right"> <a href=child-events.aspx" class="readmorebtngrns"><img src="images/readmore2.png" /></a></div>

  <div class="devider_10px"></div>



</div>             
                    
</div>
</div>


<div class="col-1-3">
<div class="wrap-col">


<div class="colorcontent">
<div class="titlewadia25"> Newsletter</div>
<div style="border:1px solid #999;"></div>
 <div class="devider_10px"></div>
 
 <asp:TextBox ID="TextBox1" runat="server" CssClass="textboxnews" placeholder="Enter Email Address..." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email Address...'" ></asp:TextBox><img src="images/subscribe.png" />
 
 
 <!--<div>  

<div style="padding-top:2px; height:30px; display:inline-block"> <input placeholder="Enter your email address" name="" type="text" style="background-color:#F2F2F2; height:30px; width:210px" /></div>
<div style="display:inline-block">
<img src="images/SubmitButton.png" /></div>
</div>-->
 
 <div class="devider_10px"></div>
 <div style="border:1px solid #999;"></div>
  <div class="devider_10px"></div>
 <div class="titlewadia25">Case Studies </div>


At the Bai Jerbai Wadia Hospital for Children we are constantly dealing with a wide range of rare and complex conditions. Here is an insight into the work we do. 
 <div class="titlewadiacontent"> Download Now <img src="images/pdf.jpg" /> &nbsp;
<div class="readmorecase" style="display:inline-block"><a href="child-case-studies.aspx"><img src="images/readmore3@.png" /></a> </div>

    



  <div class="devider_10px"></div>
 </div>

 <div style="border:1px solid #999;"></div>
  <div class="devider_10px"></div>
   
   <img src="images/vedio.png" /> 
   </div>


</div>
</div>				
                
            
</div>








     

    </div>




</asp:Content>
