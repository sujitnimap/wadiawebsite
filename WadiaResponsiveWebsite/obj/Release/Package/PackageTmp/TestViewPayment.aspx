﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mainmaster.Master" AutoEventWireup="true" CodeBehind="TestViewPayment.aspx.cs" Inherits="WadiaResponsiveWebsite.TestViewPayment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
         <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large;">Online Donation Form
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>

                            <div style="height: 10px; width: 100%"></div>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdvwpaymentDetails" runat="server" Width="100%" HorizontalAlign="Center"
                                    OnRowCommand="grdvwpaymentDetails_RowCommand" PageSize="5" AutoGenerateColumns="False" AllowPaging="True"
                                    DataKeyNames="id" CssClass="table table-hover table-striped" OnPageIndexChanging="grdvwpaymentDetails_PageIndexChanging">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr No.">
                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mih Pay Id">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblMihPayId" runat="server" Text='<%#Bind("MihPayId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="ChildDonateOnlineID">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblChildDonateOnlineID" runat="server" Text='<%#Bind("ChildDonateOnlineID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%#Bind("Status") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Merchant Key">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblMerchantKey" runat="server" Text='<%#Bind("MerchantKey") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TransationID">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTransationID" runat="server" Text='<%#Bind("TransationID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%#Bind("Amount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AddedDate">
                                            <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddedDate" runat="server" Text='<%#Bind("AddedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%#Bind("Amount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Detail" HeaderText="Detailed View">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                    </Columns>
                                    <%--<PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <PagerStyle BackColor="#7779AF" Font-Bold="true" ForeColor="White" />--%>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>

    <%--View start--%>
    <div id="detailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--%>
            <h3 id="myModalLabel">Detailed View</h3>
        </div>
        <div class="modal-body">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:DetailsView ID="dlvpaymentDetails" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                        <Fields>
                            <asp:BoundField DataField="PaymentDetailID" HeaderText="PaymentDetail ID" />
                            <asp:BoundField DataField="ChildDonateOnlineID" HeaderText="ChildDonateOnline ID" />
                            <asp:BoundField DataField="MihPayId" HeaderText="MihPayID" />
                            <asp:BoundField DataField="Mode" HeaderText="Mode" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                            <asp:BoundField DataField="MerchantKey" HeaderText="MerchantKey" />
                             <asp:BoundField DataField="UnMappedStatus" HeaderText="UnMapped Status" />
                            <asp:BoundField DataField="TransationID" HeaderText="Transation ID" />
                            <asp:BoundField DataField="ProdInfo" HeaderText="Prod Info" />
                            <asp:BoundField DataField="Mode" HeaderText="Mode" />
                            <asp:BoundField DataField="FirstName" HeaderText="FirstName" />
                            <asp:BoundField DataField="LastName" HeaderText="LastName" />

                             <asp:BoundField DataField="Email" HeaderText="Email" />
                            <asp:BoundField DataField="Phone" HeaderText="Phone" />
                            <asp:BoundField DataField="Address1" HeaderText="Address1" />
                            <asp:BoundField DataField="IsHashValid" HeaderText="IsHashValid" />
                            <asp:BoundField DataField="BankRefNum" HeaderText="Bank Ref Num" />
                            <asp:BoundField DataField="Error" HeaderText="Error" />
                            <asp:BoundField DataField="ErrorMessage" HeaderText="Error Message" />
                            <asp:BoundField DataField="UDF1" HeaderText="UDF1" />
                            <asp:BoundField DataField="Field9" HeaderText="Field9" />                            
                        </Fields>
                    </asp:DetailsView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdvwpaymentDetails" EventName="RowCommand" />
                    <%--<asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />--%>
                </Triggers>
            </asp:UpdatePanel>
            <%--<div class="modal-footer">
                <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>--%>
        </div>
    </div>

</asp:Content>
