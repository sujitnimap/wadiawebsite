﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-contact-us.aspx.cs" Inherits="WadiaResponsiveWebsite.women_contact_us" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="box">
                    <div class="heading">
                        <h2>CONTACT US</h2>
                    </div>
                </div>

                <div style="padding-left: 12px">
                    <h2>Thank you for making the time to visit our website. Please feel free to get in touch with us for comments, suggestions, enrolments or queries and we will respond to you at the earliest. </h2>
                </div>


            </div>
        </div>


        <div class="col-1-2">
            <div class="wrap-col">

                <div style="line-height: 20px; padding-left: 10px;">
                    <asp:DropDownList CssClass="textboxcontact" ID="ddlquerytype" runat="server">
                        <asp:ListItem Text="General Information/Query" Value="1" />
                        <asp:ListItem Text="Contribute" Value="2" />
                        <%--<asp:ListItem Text="Book an appointment " Value="3" />--%>
                        <asp:ListItem Text="Medical Education" Value="4" />
                        <asp:ListItem Text="Nursing Education" Value="5" />
                        <asp:ListItem Text="Tender" Value="6" />
                        <asp:ListItem Text="Careers" Value="7" />
                        <asp:ListItem Text="Feedback/Testimonials" Value="8" />
                        <asp:ListItem Text="Other" Value="9" />
                    </asp:DropDownList>
                    <div class="devider_10px"></div>

                    <asp:DropDownList CssClass="textboxcontact" ID="ddlnameprefix" runat="server">
                        <asp:ListItem Text="Title" Value="1" />
                        <asp:ListItem Text="Dr." Value="2" />
                        <asp:ListItem Text="Mr." Value="3" />
                        <asp:ListItem Text="Mrs." Value="4" />
                        <asp:ListItem Text="Ms." Value="5" />
                    </asp:DropDownList>
                    <div class="devider_10px"></div>



                    <asp:TextBox ID="txtfname" CssClass="textboxcontact" required runat="server" placeholder="Enter your first name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your first name'"></asp:TextBox>
                    <div class="devider_10px"></div>
                    <asp:TextBox ID="txtlname" CssClass="textboxcontact" required runat="server" placeholder="Enter your last name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your last name'"></asp:TextBox>
                    <div class="devider_10px"></div>

                    <asp:TextBox ID="txtemailid" CssClass="textboxcontact" required runat="server" placeholder="Enter your email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email address'"></asp:TextBox>
                    <div class="devider_10px"></div>
                    <asp:TextBox ID="txtcontactno" CssClass="textboxcontact" required runat="server" placeholder="Contact no" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Contact no'"></asp:TextBox>
                    <div class="devider_10px"></div>
                    <asp:TextBox ID="txtcomments" CssClass="textboxcontactdes" required runat="server" placeholder="Comments" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Message'"></asp:TextBox>
                    <div class="devider_10px"></div>
                    <div style="vertical-align: central; display: inline-block; padding-right: 15px; padding-top: 6px">Enter Security Code </div>
                    <div style="display: inline-block">
                        <%-- <img src="images/captcha.jpg" />--%>
                        <asp:UpdatePanel ID="uptpnlform" runat="server">
                            <ContentTemplate>
                                <asp:Image ID="imgVerificationCode" runat="server" ImageUrl="capcha.aspx" Width="100"
                                    Height="30" EnableViewState="false" />
                                <br />
                                Can't see?
                                                <asp:LinkButton ID="lbRefresh" CssClass="bluecolor_link" runat="server" CausesValidation="False"
                                                    OnClick="lbRefresh_Click">Refresh!</asp:LinkButton>
                                <br />

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="devider_10px"></div>
                    <asp:TextBox ID="tb_Code" CssClass="textboxcontact" required runat="server" placeholder="Enter captcha" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter captcha'"></asp:TextBox>
                    <div class="devider_10px"></div>
                    <div style="text-align: left">
                        <%--<img src="images/SubmitButton.png" />--%>
                        <asp:ImageButton ID="imgbtncontact" src="images/SubmitButton.png" runat="server" OnClick="imgbtncontact_Click" />
                    </div>

                    <br />
                    <br />



                </div>
                <!-- ABOUT US CONTENT -->





            </div>
        </div>


        <div class="col-1-2">
            <div class="wrap-col">

                <div class="box">
                    <%--<div class="heading"><h2>About Us</h2></div>--%>



                    <img src="images/address.jpg" />&nbsp;<i><b>Address</b></i>
                    :  Nowrosjee Wadia Maternity Hospital, 
                    
                    <div style="padding-left: 92px">Acharya Donde Marg, Parel, </div>
                    <div style="padding-left: 92px">Mumbai, Maharashtra,India, 400 012</div>

                    <div class="devider_10px"></div>
                    <div style="padding-left: 5px">
                        <img src="images/phone.jpg" />
                        <b>&nbsp;<i>Tel</i> : +91 - 22 - 24146964 / 65 / 66 / 67</b>
                    </div>


                    <div class="devider_10px"></div>
                    <div style="padding-left: 5px">
                        <img src="images/email.jpg" />
                        <b>&nbsp;<i>Email</i> : info@wadiahospitals.org</b>
                    </div>


                    <div class="devider_10px"></div>


                    <iframe width="100%" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Nowrosjee+Wadia+Maternity+Hospital,+Acharya+Donde+Marg,+Parel,+Mumbai,+Maharashtra,+India&amp;aq=0&amp;oq=Nowrosjee+wadia+maternity+hospital&amp;sll=37.0625,-95.677068&amp;sspn=36.915634,79.013672&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=19.003224,72.842836&amp;spn=0.006295,0.006295&amp;t=m&amp;iwloc=A&amp;output=embed"></iframe>
                    <br />

                    <br />
                    <small>View <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Nowrosjee+Wadia+Maternity+Hospital,+Acharya+Donde+Marg,+Parel,+Mumbai,+Maharashtra,+India&amp;aq=0&amp;oq=Nowrosjee+wadia+maternity+hospital&amp;sll=37.0625,-95.677068&amp;sspn=36.915634,79.013672&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=19.003224,72.842836&amp;spn=0.006295,0.006295&amp;t=m&amp;iwloc=A" style="color: #0000FF; text-align: left">Nowrosjee Wadia Maternity Hospital</a> in Larger Map</small>

                    <br></br>


                </div>
                <%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
            </div>
        </div>


    </div>





</asp:Content>
