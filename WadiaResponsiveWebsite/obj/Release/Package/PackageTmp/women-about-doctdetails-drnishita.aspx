﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-about-doctdetails-drnishita.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_doctdetails_drnishita" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
		<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dr.jpg" />
<div class="docdetails">
Dr. Nishita Ashok Parekh


<div class="docdetailsdesg">
Assistant Professor
    <br />
MBBS, DNB


</div>

</div>
</div>

     <div class="areaofinterest">

Areas of Interest : Reproductive Medicine, Foetal Medicine and Laparoscopy
</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
        <%--    <li><a href="#view2">Achievements</a></li>--%>
            <li><a href="#view3">Achievements</a></li>
            <li><a href="#view4">Research or publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	Best Paper at the FOGSI-FIGO International Congress in April 2011
<br />•	2nd Prize for Best Interesting Case Presentation during National Conference	 2012 AFG jointly with ISOPARB in March 2012
<br />•	MOGS- Dr. L. M. Shah Prize for Best Paper presentation during 41st Annual Conference of Mumbai Obstetrics & Gynaecological Society in March 2013
</p>

<br /><br />               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Author in an article titled ‘An Interesting Case of Invasive Mole’ published in AFG Times Issue- 04/ Year 2012-13
<br />•	Co-author in two chapters in “Manual on Vaginal Surgery ” – AICOG 2013- a FOGSI Publication
<br />•	Author and co-author in four chapters in “Understanding Female Urinary Incontinence & Master Management” - a FOGSI Publication 2013       
<br /><br />
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			






</asp:Content>
