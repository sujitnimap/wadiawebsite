﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-nursing-courses.aspx.cs" Inherits="WadiaResponsiveWebsite.women_nursing_courses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                     <div class="heading">
                  <h2>COURSES AVAILABLE</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->


<div style="line-height:20px">


<table width="70%" cellspacing="0" cellpadding="0" style="border: 1px solid #666;">
  <tr>
    <td height="40" style="border: 1px solid #666; padding:5px"><b>SR. NO.</b></td>
    <td style="border: 1px solid #666; padding:5px"><b>COURSE</b> </td>
    <td style="border: 1px solid #666; padding:5px"><b>CATEGORY</b></td>
    <td style="border: 1px solid #666; padding:5px"><b>DURATION</b></td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">1</td>
    <td style="border: 1px solid #666; padding:5px">General Nursing Midwifery Course </td>
    <td style="border: 1px solid #666; padding:5px">Residential </td>
    <td style="border: 1px solid #666; padding:5px">4 – 5 months </td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">2</td>
    <td style="border: 1px solid #666; padding:5px">Auxiliary Nurse Midwifery Course </td>
    <td style="border: 1px solid #666; padding:5px">Non- Residential </td>
    <td style="border: 1px solid #666; padding:5px">1 – 2 months </td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">3</td>
    <td style="border: 1px solid #666; padding:5px">B.Sc Nursing </td>
    <td style="border: 1px solid #666; padding:5px">Non- Residential </td>
    <td style="border: 1px solid #666; padding:5px">1 month</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">4</td>
    <td style="border: 1px solid #666; padding:5px">M.Sc Nursing </td>
    <td style="border: 1px solid #666; padding:5px">Residential </td>
    <td style="border: 1px solid #666; padding:5px">1 month</td>
  </tr>
</table>





</div>

<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>Nursing</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						  <li><a href="women-nursing-overview.aspx">	Overview  </a></li>
                        <li><a href="women-nursing-school.aspx"> 	Nursing Department</a></li>
                        <li><a href="women-nursing-courses.aspx"> 	Courses available</a></li>
                         <li><a href="women-contact-us.aspx">	Contact Us  </a></li>


                                     <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
			



				</div>
			</div>



		</div>




</asp:Content>
