﻿<%@ Page Title="Sponsor A Run For Marathon" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="littleheartsmarathon2015-Sponsorarun.aspx.cs" Inherits="WadiaResponsiveWebsite.SponsorARunForMarathon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <p>
        &nbsp;&nbsp;
    </p>
    <script src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
        function validateChildCnt() {

            var chilMinorCnt = 0;
            var lblMessage = $("span[id$='lblMessage']");
            var emailAddress = $("#<%=txtEmailAddress.ClientID %>").val();
            var confirmEmailAddress = $("#<%=txtConfirmEmailAddress.ClientID %>").val();

            var lblTotalAmountInINR = $("#<%=lblTotalAmountInINR.ClientID %>").val();

            //if (lblTotalAmountInINR == 0) {
            //    lblMessage.html('Amount should be greater than zero')
            //    return false;
            //}


            var isChecked = $('#chkTearmAndCondition').is(':checked');

            if (!isChecked) {
                lblMessage.html('Please check Terms And Conditions')
                return false;
            }

            //if ($('#chkIsSponsorARun').not(':checked')) {
            //     lblMessage.html('Please check Terms And Conditions')
            //     return false;
            // }

            if (emailAddress != confirmEmailAddress) {
                lblMessage.html('Please enter correct email address')
                return false;
            }

        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row block05">



        <div class="col-full">
            <div class="wrap-col">

                <div style="border: 1px solid #ddd; padding: 10px 20px 20px 10px; border-radius: 4px;">

                    <div class="heading">
                        <h2>Sponsor A Run - 2016 Marathon</h2>
                    </div>
                    <br />
                    <strong>Aid young hearts, aid young India!</strong><br />
                    We invite you to look into the event and connect with one, two, or even more children’s run!
Your involvement with us by sponsoring a run allows you with a unique opportunity to connect with the community on a more personal level. Your contribution will help a child run on your behalf.
So if you cannot make it to the marathon for some reason or the other, make sure your presence is felt by contributing a run!

                    <br />
                    <div class="devider_20px"></div>

                    <asp:TextBox ID="txtNameOfOragnisation" CssClass="textboxcontact_list" required runat="server" placeholder="Name of School/College or Organisation" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name of School/College or Organisation'"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reftxtNameOfOragnisation" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtNameOfOragnisation" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                    <div class="devider_10px"></div>

                    <asp:TextBox ID="txtFirstNameOfContactPerson" CssClass="textboxcontact_list" required runat="server" placeholder="First Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reftxtFirstNameOfContactPerson" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtFirstNameOfContactPerson" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                    <div class="devider_10px"></div>
                    <asp:TextBox ID="txtLastNameOfContactPerson" CssClass="textboxcontact_list" required runat="server" placeholder="Last Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reftxtLastNameOfContactPerson" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtLastNameOfContactPerson" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                    <div class="devider_10px"></div>
                    <asp:Panel ID="pnlNoOfSponsorRun" runat="server">
                        
                    How many runs will you like to sponsor
                        <asp:DropDownList ID="ddlNoOfSponsorRun" CssClass="textboxcontact_drpdwn" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlNoOfSponsorRun_SelectedIndexChanged">
                            <asp:ListItem Value="0" Text="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                            <asp:ListItem Value="6" Text="6"></asp:ListItem>
                            <asp:ListItem Value="7" Text="7"></asp:ListItem>
                            <asp:ListItem Value="8" Text="8"></asp:ListItem>
                            <asp:ListItem Value="9" Text="9"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                            <asp:ListItem Value="11" Text="11"></asp:ListItem>
                            <asp:ListItem Value="12" Text="12"></asp:ListItem>
                            <asp:ListItem Value="13" Text="13"></asp:ListItem>
                            <asp:ListItem Value="14" Text="14"></asp:ListItem>
                            <asp:ListItem Value="15" Text="15"></asp:ListItem>
                            <asp:ListItem Value="16" Text="16"></asp:ListItem>
                            <asp:ListItem Value="17" Text="17"></asp:ListItem>
                            <asp:ListItem Value="18" Text="18"></asp:ListItem>
                            <asp:ListItem Value="19" Text="19"></asp:ListItem>
                            <asp:ListItem Value="20" Text="20"></asp:ListItem>
                            <asp:ListItem Value="21" Text="21"></asp:ListItem>
                            <asp:ListItem Value="22" Text="22"></asp:ListItem>
                            <asp:ListItem Value="23" Text="23"></asp:ListItem>
                            <asp:ListItem Value="24" Text="24"></asp:ListItem>
                            <asp:ListItem Value="25" Text="25"></asp:ListItem>
                            <asp:ListItem Value="26" Text="26"></asp:ListItem>
                            <asp:ListItem Value="27" Text="27"></asp:ListItem>
                            <asp:ListItem Value="28" Text="28"></asp:ListItem>
                            <asp:ListItem Value="29" Text="29"></asp:ListItem>
                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                            <asp:ListItem Value="31" Text="31"></asp:ListItem>
                            <asp:ListItem Value="32" Text="32"></asp:ListItem>
                            <asp:ListItem Value="33" Text="33"></asp:ListItem>
                            <asp:ListItem Value="34" Text="34"></asp:ListItem>
                            <asp:ListItem Value="35" Text="35"></asp:ListItem>
                            <asp:ListItem Value="36" Text="36"></asp:ListItem>
                            <asp:ListItem Value="37" Text="37"></asp:ListItem>
                            <asp:ListItem Value="38" Text="38"></asp:ListItem>
                            <asp:ListItem Value="39" Text="39"></asp:ListItem>
                            <asp:ListItem Value="40" Text="40"></asp:ListItem>
                            <asp:ListItem Value="41" Text="41"></asp:ListItem>
                            <asp:ListItem Value="42" Text="42"></asp:ListItem>
                            <asp:ListItem Value="43" Text="43"></asp:ListItem>
                            <asp:ListItem Value="44" Text="44"></asp:ListItem>
                            <asp:ListItem Value="45" Text="45"></asp:ListItem>
                            <asp:ListItem Value="46" Text="46"></asp:ListItem>
                            <asp:ListItem Value="47" Text="47"></asp:ListItem>
                            <asp:ListItem Value="48" Text="48"></asp:ListItem>
                            <asp:ListItem Value="49" Text="49"></asp:ListItem>
                            <asp:ListItem Value="50" Text="50"></asp:ListItem>
                            <asp:ListItem Value="51" Text="51"></asp:ListItem>
                            <asp:ListItem Value="52" Text="52"></asp:ListItem>
                            <asp:ListItem Value="53" Text="53"></asp:ListItem>
                            <asp:ListItem Value="54" Text="54"></asp:ListItem>
                            <asp:ListItem Value="55" Text="55"></asp:ListItem>
                            <asp:ListItem Value="56" Text="56"></asp:ListItem>
                            <asp:ListItem Value="57" Text="57"></asp:ListItem>
                            <asp:ListItem Value="58" Text="58"></asp:ListItem>
                            <asp:ListItem Value="59" Text="59"></asp:ListItem>
                            <asp:ListItem Value="60" Text="60"></asp:ListItem>
                            <asp:ListItem Value="61" Text="61"></asp:ListItem>
                            <asp:ListItem Value="62" Text="62"></asp:ListItem>
                            <asp:ListItem Value="63" Text="63"></asp:ListItem>
                            <asp:ListItem Value="64" Text="64"></asp:ListItem>
                            <asp:ListItem Value="65" Text="65"></asp:ListItem>
                            <asp:ListItem Value="66" Text="66"></asp:ListItem>
                            <asp:ListItem Value="67" Text="67"></asp:ListItem>
                            <asp:ListItem Value="68" Text="68"></asp:ListItem>
                            <asp:ListItem Value="69" Text="69"></asp:ListItem>
                            <asp:ListItem Value="70" Text="70"></asp:ListItem>
                            <asp:ListItem Value="71" Text="71"></asp:ListItem>
                            <asp:ListItem Value="72" Text="72"></asp:ListItem>
                            <asp:ListItem Value="73" Text="73"></asp:ListItem>
                            <asp:ListItem Value="74" Text="74"></asp:ListItem>
                            <asp:ListItem Value="75" Text="75"></asp:ListItem>
                            <asp:ListItem Value="76" Text="76"></asp:ListItem>
                            <asp:ListItem Value="77" Text="77"></asp:ListItem>
                            <asp:ListItem Value="78" Text="78"></asp:ListItem>
                            <asp:ListItem Value="79" Text="79"></asp:ListItem>
                            <asp:ListItem Value="80" Text="80"></asp:ListItem>
                            <asp:ListItem Value="81" Text="81"></asp:ListItem>
                            <asp:ListItem Value="82" Text="82"></asp:ListItem>
                            <asp:ListItem Value="83" Text="83"></asp:ListItem>
                            <asp:ListItem Value="84" Text="84"></asp:ListItem>
                            <asp:ListItem Value="85" Text="85"></asp:ListItem>
                            <asp:ListItem Value="86" Text="86"></asp:ListItem>
                            <asp:ListItem Value="87" Text="87"></asp:ListItem>
                            <asp:ListItem Value="88" Text="88"></asp:ListItem>
                            <asp:ListItem Value="89" Text="89"></asp:ListItem>
                            <asp:ListItem Value="90" Text="90"></asp:ListItem>
                            <asp:ListItem Value="91" Text="91"></asp:ListItem>
                            <asp:ListItem Value="92" Text="92"></asp:ListItem>
                            <asp:ListItem Value="93" Text="93"></asp:ListItem>
                            <asp:ListItem Value="94" Text="94"></asp:ListItem>
                            <asp:ListItem Value="95" Text="95"></asp:ListItem>
                            <asp:ListItem Value="96" Text="96"></asp:ListItem>
                            <asp:ListItem Value="97" Text="97"></asp:ListItem>
                            <asp:ListItem Value="98" Text="98"></asp:ListItem>
                            <asp:ListItem Value="99" Text="99"></asp:ListItem>
                            <asp:ListItem Value="100" Text="100"></asp:ListItem>
                        </asp:DropDownList>
                    </asp:Panel>
                    <div class="devider_10px"></div>

                    <asp:TextBox ID="txtEmailAddress" CssClass="textboxcontact_list" required runat="server" placeholder="Email Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'"></asp:TextBox>

                    <div class="devider_10px"></div>



                    <asp:TextBox ID="txtConfirmEmailAddress" CssClass="textboxcontact_list" required runat="server" placeholder="Confirm Email Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Confirm Email Address'"></asp:TextBox>

                    <div class="devider_10px"></div>



                    <asp:TextBox ID="txtMobileNumber1" CssClass="textboxcontact_list" required runat="server" placeholder="Mobile Number(1)*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile Number(1)'"></asp:TextBox>

                    <div class="devider_10px"></div>

                    <asp:TextBox ID="txtMobileNumber2" CssClass="textboxcontact_list" runat="server" placeholder="Mobile Number(2)" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile Number(2)'"></asp:TextBox>

                    <div class="devider_10px"></div>



                    <asp:TextBox ID="txtAddress" TextMode="MultiLine" CssClass="textboxcontact_list" required runat="server" placeholder="Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address'"></asp:TextBox>

                    <div class="devider_10px"></div>



                    <asp:TextBox ID="txtCity" CssClass="textboxcontact_list" runat="server" placeholder="City*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'City'"></asp:TextBox>

                    <div class="devider_10px"></div>



                    <asp:TextBox ID="txtState" CssClass="textboxcontact_list" runat="server" placeholder="State*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'State'"></asp:TextBox>

                    <div class="devider_10px"></div>

                    <div style="display: inline; padding-right: 35px">

                        <strong>Country * : </strong>
                    </div>
                    <div style="display: inline">
                        <asp:DropDownList CssClass="textboxcontact_drpdwn" ID="ddlCountrys" runat="server">
                            <asp:ListItem>Afghanistan</asp:ListItem>
                            <asp:ListItem>Aland Islands</asp:ListItem>
                            <asp:ListItem>Albania</asp:ListItem>
                            <asp:ListItem>Algeria</asp:ListItem>
                            <asp:ListItem>Andorra</asp:ListItem>
                            <asp:ListItem>Angola</asp:ListItem>
                            <asp:ListItem>Anguilla</asp:ListItem>
                            <asp:ListItem>Antartica</asp:ListItem>
                            <asp:ListItem>Antigua And Barbuda</asp:ListItem>
                            <asp:ListItem>Argentina</asp:ListItem>
                            <asp:ListItem>Armenia</asp:ListItem>
                            <asp:ListItem>Aruba</asp:ListItem>
                            <asp:ListItem>Ascension Island/St. Helena</asp:ListItem>
                            <asp:ListItem>Australia</asp:ListItem>
                            <asp:ListItem>Austria</asp:ListItem>
                            <asp:ListItem>Azerbaijan</asp:ListItem>
                            <asp:ListItem>Bahamas</asp:ListItem>
                            <asp:ListItem>Bahrain</asp:ListItem>
                            <asp:ListItem>Bangladesh</asp:ListItem>
                            <asp:ListItem>Barbados</asp:ListItem>
                            <asp:ListItem>Belarus</asp:ListItem>
                            <asp:ListItem>Belgium</asp:ListItem>
                            <asp:ListItem>Belize</asp:ListItem>
                            <asp:ListItem>Benin</asp:ListItem>
                            <asp:ListItem>Bermuda</asp:ListItem>
                            <asp:ListItem>Bhutan</asp:ListItem>
                            <asp:ListItem>Bolivia</asp:ListItem>
                            <asp:ListItem>Bosnia Herzegovina</asp:ListItem>
                            <asp:ListItem>Botswana</asp:ListItem>
                            <asp:ListItem>Bouvet Island</asp:ListItem>
                            <asp:ListItem>Brazil</asp:ListItem>
                            <asp:ListItem>British Virgin Islands</asp:ListItem>
                            <asp:ListItem>Brunei</asp:ListItem>
                            <asp:ListItem>Bulgaria</asp:ListItem>
                            <asp:ListItem>Burkina Faso</asp:ListItem>
                            <asp:ListItem>Burundi</asp:ListItem>
                            <asp:ListItem>Cambodia</asp:ListItem>
                            <asp:ListItem>Cameroon, United Republic Of</asp:ListItem>
                            <asp:ListItem>Canada</asp:ListItem>
                            <asp:ListItem>Cape Verde, Republic Of</asp:ListItem>
                            <asp:ListItem>Cayman Islands</asp:ListItem>
                            <asp:ListItem>Central African Republic</asp:ListItem>
                            <asp:ListItem>Chad</asp:ListItem>
                            <asp:ListItem>Chile</asp:ListItem>
                            <asp:ListItem>China</asp:ListItem>
                            <asp:ListItem>Christmas Island</asp:ListItem>
                            <asp:ListItem>Cocos Islands</asp:ListItem>
                            <asp:ListItem>Colombia</asp:ListItem>
                            <asp:ListItem>Comoros</asp:ListItem>
                            <asp:ListItem>Congo</asp:ListItem>
                            <asp:ListItem>Congo Democratic Republic Of</asp:ListItem>
                            <asp:ListItem>Cook Islands</asp:ListItem>
                            <asp:ListItem>Costa Rica</asp:ListItem>
                            <asp:ListItem>Croatia</asp:ListItem>
                            <asp:ListItem>Cuba</asp:ListItem>
                            <asp:ListItem>Cyprus</asp:ListItem>
                            <asp:ListItem>Czech Republic</asp:ListItem>
                            <asp:ListItem>Denmark</asp:ListItem>
                            <asp:ListItem>Djibouti</asp:ListItem>
                            <asp:ListItem>Dominica</asp:ListItem>
                            <asp:ListItem>Dominican Republic</asp:ListItem>
                            <asp:ListItem>Ecuador</asp:ListItem>
                            <asp:ListItem>Egypt</asp:ListItem>
                            <asp:ListItem>El Salvador</asp:ListItem>
                            <asp:ListItem>Equatorial Guinea</asp:ListItem>
                            <asp:ListItem>Eritrea</asp:ListItem>
                            <asp:ListItem>Estonia</asp:ListItem>
                            <asp:ListItem>Ethiopia</asp:ListItem>
                            <asp:ListItem>Faeroe Islands</asp:ListItem>
                            <asp:ListItem>Falkland Islands</asp:ListItem>
                            <asp:ListItem>Fiji Islands</asp:ListItem>
                            <asp:ListItem>Finland</asp:ListItem>
                            <asp:ListItem>France</asp:ListItem>
                            <asp:ListItem>French Guiana</asp:ListItem>
                            <asp:ListItem>French Polynesia</asp:ListItem>
                            <asp:ListItem>Gabon</asp:ListItem>
                            <asp:ListItem>Gambia</asp:ListItem>
                            <asp:ListItem>Georgia</asp:ListItem>
                            <asp:ListItem>Germany</asp:ListItem>
                            <asp:ListItem>Ghana</asp:ListItem>
                            <asp:ListItem>Gibraltar</asp:ListItem>
                            <asp:ListItem>Greece</asp:ListItem>
                            <asp:ListItem>Greenland</asp:ListItem>
                            <asp:ListItem>Grenada</asp:ListItem>
                            <asp:ListItem>Guadeloupe</asp:ListItem>
                            <asp:ListItem>Guam</asp:ListItem>
                            <asp:ListItem>Guatemala</asp:ListItem>
                            <asp:ListItem>Guernsey</asp:ListItem>
                            <asp:ListItem>Guinea</asp:ListItem>
                            <asp:ListItem>Guinea Bissau</asp:ListItem>
                            <asp:ListItem>Guyana</asp:ListItem>
                            <asp:ListItem>Haiti</asp:ListItem>
                            <asp:ListItem>Heard Island And McDonald Islands</asp:ListItem>
                            <asp:ListItem>Honduras</asp:ListItem>
                            <asp:ListItem>Hong Kong</asp:ListItem>
                            <asp:ListItem>Hungary</asp:ListItem>
                            <asp:ListItem>Iceland</asp:ListItem>
                            <asp:ListItem Selected="True">India</asp:ListItem>
                            <asp:ListItem>Indonesia</asp:ListItem>
                            <asp:ListItem>Iran</asp:ListItem>
                            <asp:ListItem>Iraq</asp:ListItem>
                            <asp:ListItem>Ireland, Republic Of</asp:ListItem>
                            <asp:ListItem>Isle Of Man</asp:ListItem>
                            <asp:ListItem>Israel</asp:ListItem>
                            <asp:ListItem>Italy</asp:ListItem>
                            <asp:ListItem>Ivory Coast</asp:ListItem>
                            <asp:ListItem>Jamaica</asp:ListItem>
                            <asp:ListItem>Japan</asp:ListItem>
                            <asp:ListItem>Jersey Island</asp:ListItem>
                            <asp:ListItem>Jordan</asp:ListItem>
                            <asp:ListItem>Kazakstan</asp:ListItem>
                            <asp:ListItem>Kenya</asp:ListItem>
                            <asp:ListItem>Kiribati</asp:ListItem>
                            <asp:ListItem>Korea, Democratic Peoples Republic</asp:ListItem>
                            <asp:ListItem>Korea, Republic Of</asp:ListItem>
                            <asp:ListItem>Kuwait</asp:ListItem>
                            <asp:ListItem>Kyrgyzstan</asp:ListItem>
                            <asp:ListItem>Lao, People&#39;s Dem. Rep.</asp:ListItem>
                            <asp:ListItem>Latvia</asp:ListItem>
                            <asp:ListItem>Lebanon</asp:ListItem>
                            <asp:ListItem>Lesotho</asp:ListItem>
                            <asp:ListItem>Liberia</asp:ListItem>
                            <asp:ListItem>Libyan Arab Jamahiriya</asp:ListItem>
                            <asp:ListItem>Liechtenstein</asp:ListItem>
                            <asp:ListItem>Lithuania</asp:ListItem>
                            <asp:ListItem>Luxembourg</asp:ListItem>
                            <asp:ListItem>Macau</asp:ListItem>
                            <asp:ListItem>Macedonia</asp:ListItem>
                            <asp:ListItem>Madagascar (Malagasy)</asp:ListItem>
                            <asp:ListItem>Malawi</asp:ListItem>
                            <asp:ListItem>Malaysia</asp:ListItem>
                            <asp:ListItem>Maldives</asp:ListItem>
                            <asp:ListItem>Mali</asp:ListItem>
                            <asp:ListItem>Malta</asp:ListItem>
                            <asp:ListItem>Marianna Islands</asp:ListItem>
                            <asp:ListItem>Marshall Islands</asp:ListItem>
                            <asp:ListItem>Martinique</asp:ListItem>
                            <asp:ListItem>Mauritania</asp:ListItem>
                            <asp:ListItem>Mauritius</asp:ListItem>
                            <asp:ListItem>Mayotte</asp:ListItem>
                            <asp:ListItem>Mexico</asp:ListItem>
                            <asp:ListItem>Micronesia</asp:ListItem>
                            <asp:ListItem>Moldova</asp:ListItem>
                            <asp:ListItem>Monaco</asp:ListItem>
                            <asp:ListItem>Mongolia</asp:ListItem>
                            <asp:ListItem>Montserrat</asp:ListItem>
                            <asp:ListItem>Morocco</asp:ListItem>
                            <asp:ListItem>Mozambique</asp:ListItem>
                            <asp:ListItem>Myanmar</asp:ListItem>
                            <asp:ListItem>Namibia</asp:ListItem>
                            <asp:ListItem>Nauru</asp:ListItem>
                            <asp:ListItem>Nepal</asp:ListItem>
                            <asp:ListItem>Netherland Antilles</asp:ListItem>
                            <asp:ListItem>Netherlands</asp:ListItem>
                            <asp:ListItem>New Caledonia</asp:ListItem>
                            <asp:ListItem>New Zealand</asp:ListItem>
                            <asp:ListItem>Nicaragua</asp:ListItem>
                            <asp:ListItem>Niger</asp:ListItem>
                            <asp:ListItem>Nigeria</asp:ListItem>
                            <asp:ListItem>Niue</asp:ListItem>
                            <asp:ListItem>Norfolk Island</asp:ListItem>
                            <asp:ListItem>Norway</asp:ListItem>
                            <asp:ListItem>Occupied Palestinian Territory</asp:ListItem>
                            <asp:ListItem>Oman, Sultanate Of</asp:ListItem>
                            <asp:ListItem>Pakistan</asp:ListItem>
                            <asp:ListItem>Palau</asp:ListItem>
                            <asp:ListItem>Panama</asp:ListItem>
                            <asp:ListItem>Papua New Guinea (Niugini)</asp:ListItem>
                            <asp:ListItem>Paraguay</asp:ListItem>
                            <asp:ListItem>Peru</asp:ListItem>
                            <asp:ListItem>Philippines</asp:ListItem>
                            <asp:ListItem>Pitcairn</asp:ListItem>
                            <asp:ListItem>Poland</asp:ListItem>
                            <asp:ListItem>Portugal</asp:ListItem>
                            <asp:ListItem>Qatar</asp:ListItem>
                            <asp:ListItem>Reunion</asp:ListItem>
                            <asp:ListItem>Romania</asp:ListItem>
                            <asp:ListItem>Russian Federation</asp:ListItem>
                            <asp:ListItem>Rwanda</asp:ListItem>
                            <asp:ListItem>Saint Lucia</asp:ListItem>
                            <asp:ListItem>Saint Vincent And The Grenadines</asp:ListItem>
                            <asp:ListItem>Samoa, American</asp:ListItem>
                            <asp:ListItem>Samoa, Independent State Of</asp:ListItem>
                            <asp:ListItem>San Marino</asp:ListItem>
                            <asp:ListItem>Sao Tome &amp; Principe</asp:ListItem>
                            <asp:ListItem>Saudi Arabia</asp:ListItem>
                            <asp:ListItem>Senegal</asp:ListItem>
                            <asp:ListItem>Serbia</asp:ListItem>
                            <asp:ListItem>Seychelles Islands</asp:ListItem>
                            <asp:ListItem>Sierra Leone</asp:ListItem>
                            <asp:ListItem>Singapore</asp:ListItem>
                            <asp:ListItem>Slovakia</asp:ListItem>
                            <asp:ListItem>Slovenia</asp:ListItem>
                            <asp:ListItem>Solomon Islands</asp:ListItem>
                            <asp:ListItem>Somalia</asp:ListItem>
                            <asp:ListItem>South Africa</asp:ListItem>
                            <asp:ListItem>Spain</asp:ListItem>
                            <asp:ListItem>Sri Lanka</asp:ListItem>
                            <asp:ListItem>St. Kitts - Nevis</asp:ListItem>
                            <asp:ListItem>St. Pierre &amp; Miquelon</asp:ListItem>
                            <asp:ListItem>Sudan</asp:ListItem>
                            <asp:ListItem>Suriname</asp:ListItem>
                            <asp:ListItem>Svalbard And Jan Mayen Is</asp:ListItem>
                            <asp:ListItem>Swaziland</asp:ListItem>
                            <asp:ListItem>Sweden</asp:ListItem>
                            <asp:ListItem>Switzerland</asp:ListItem>
                            <asp:ListItem>Syrian Arab Rep.</asp:ListItem>
                            <asp:ListItem>Taiwan, Republic of China</asp:ListItem>
                            <asp:ListItem>Tajikistan</asp:ListItem>
                            <asp:ListItem>Tanzania</asp:ListItem>
                            <asp:ListItem>Thailand</asp:ListItem>
                            <asp:ListItem>Timor Leste</asp:ListItem>
                            <asp:ListItem>Togo</asp:ListItem>
                            <asp:ListItem>Tonga</asp:ListItem>
                            <asp:ListItem>Trinidad &amp; Tobago</asp:ListItem>
                            <asp:ListItem>Tunisia</asp:ListItem>
                            <asp:ListItem>Turkey</asp:ListItem>
                            <asp:ListItem>Turkmenistan</asp:ListItem>
                            <asp:ListItem>Turks And Caicos Islands</asp:ListItem>
                            <asp:ListItem>Tuvalu</asp:ListItem>
                            <asp:ListItem>Uganda</asp:ListItem>
                            <asp:ListItem>Ukraine</asp:ListItem>
                            <asp:ListItem>United Arab Emirates</asp:ListItem>
                            <asp:ListItem>United Kingdom</asp:ListItem>
                            <asp:ListItem>United States</asp:ListItem>
                            <asp:ListItem>United States Minor Outlying Islnds</asp:ListItem>
                            <asp:ListItem>Uruguay</asp:ListItem>
                            <asp:ListItem>Uzbekistan Sum</asp:ListItem>
                            <asp:ListItem>Vanuatu</asp:ListItem>
                            <asp:ListItem>Vatican City State</asp:ListItem>
                            <asp:ListItem>Venezuela</asp:ListItem>
                            <asp:ListItem>Vietnam</asp:ListItem>
                            <asp:ListItem>Wallis &amp; Futuna Islands</asp:ListItem>
                            <asp:ListItem>Western Sahara</asp:ListItem>
                            <asp:ListItem>Yemen, Republic Of</asp:ListItem>
                            <asp:ListItem>Yugoslavia</asp:ListItem>
                            <asp:ListItem>Zambia</asp:ListItem>
                            <asp:ListItem>Zimbabwe</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="devider_20px"></div>

                    <div style="display: inline; padding-right: 35px">

                        <strong>Nationality *&nbsp; :</strong>
                    </div>
                    <div style="display: inline">

                        <asp:DropDownList ID="ddlNationalitys" CssClass="textboxcontact_drpdwn" runat="server">
                            <asp:ListItem>Afghanistan</asp:ListItem>
                            <asp:ListItem>Aland Islands</asp:ListItem>
                            <asp:ListItem>Albania</asp:ListItem>
                            <asp:ListItem>Algeria</asp:ListItem>
                            <asp:ListItem>Andorra</asp:ListItem>
                            <asp:ListItem>Angola</asp:ListItem>
                            <asp:ListItem>Anguilla</asp:ListItem>
                            <asp:ListItem>Antartica</asp:ListItem>
                            <asp:ListItem>Antigua And Barbuda</asp:ListItem>
                            <asp:ListItem>Argentina</asp:ListItem>
                            <asp:ListItem>Armenia</asp:ListItem>
                            <asp:ListItem>Aruba</asp:ListItem>
                            <asp:ListItem>Ascension Island/St. Helena</asp:ListItem>
                            <asp:ListItem>Australia</asp:ListItem>
                            <asp:ListItem>Austria</asp:ListItem>
                            <asp:ListItem>Azerbaijan</asp:ListItem>
                            <asp:ListItem>Bahamas</asp:ListItem>
                            <asp:ListItem>Bahrain</asp:ListItem>
                            <asp:ListItem>Bangladesh</asp:ListItem>
                            <asp:ListItem>Barbados</asp:ListItem>
                            <asp:ListItem>Belarus</asp:ListItem>
                            <asp:ListItem>Belgium</asp:ListItem>
                            <asp:ListItem>Belize</asp:ListItem>
                            <asp:ListItem>Benin</asp:ListItem>
                            <asp:ListItem>Bermuda</asp:ListItem>
                            <asp:ListItem>Bhutan</asp:ListItem>
                            <asp:ListItem>Bolivia</asp:ListItem>
                            <asp:ListItem>Bosnia Herzegovina</asp:ListItem>
                            <asp:ListItem>Botswana</asp:ListItem>
                            <asp:ListItem>Bouvet Island</asp:ListItem>
                            <asp:ListItem>Brazil</asp:ListItem>
                            <asp:ListItem>British Virgin Islands</asp:ListItem>
                            <asp:ListItem>Brunei</asp:ListItem>
                            <asp:ListItem>Bulgaria</asp:ListItem>
                            <asp:ListItem>Burkina Faso</asp:ListItem>
                            <asp:ListItem>Burundi</asp:ListItem>
                            <asp:ListItem>Cambodia</asp:ListItem>
                            <asp:ListItem>Cameroon, United Republic Of</asp:ListItem>
                            <asp:ListItem>Canada</asp:ListItem>
                            <asp:ListItem>Cape Verde, Republic Of</asp:ListItem>
                            <asp:ListItem>Cayman Islands</asp:ListItem>
                            <asp:ListItem>Central African Republic</asp:ListItem>
                            <asp:ListItem>Chad</asp:ListItem>
                            <asp:ListItem>Chile</asp:ListItem>
                            <asp:ListItem>China</asp:ListItem>
                            <asp:ListItem>Christmas Island</asp:ListItem>
                            <asp:ListItem>Cocos Islands</asp:ListItem>
                            <asp:ListItem>Colombia</asp:ListItem>
                            <asp:ListItem>Comoros</asp:ListItem>
                            <asp:ListItem>Congo</asp:ListItem>
                            <asp:ListItem>Congo Democratic Republic Of</asp:ListItem>
                            <asp:ListItem>Cook Islands</asp:ListItem>
                            <asp:ListItem>Costa Rica</asp:ListItem>
                            <asp:ListItem>Croatia</asp:ListItem>
                            <asp:ListItem>Cuba</asp:ListItem>
                            <asp:ListItem>Cyprus</asp:ListItem>
                            <asp:ListItem>Czech Republic</asp:ListItem>
                            <asp:ListItem>Denmark</asp:ListItem>
                            <asp:ListItem>Djibouti</asp:ListItem>
                            <asp:ListItem>Dominica</asp:ListItem>
                            <asp:ListItem>Dominican Republic</asp:ListItem>
                            <asp:ListItem>Ecuador</asp:ListItem>
                            <asp:ListItem>Egypt</asp:ListItem>
                            <asp:ListItem>El Salvador</asp:ListItem>
                            <asp:ListItem>Equatorial Guinea</asp:ListItem>
                            <asp:ListItem>Eritrea</asp:ListItem>
                            <asp:ListItem>Estonia</asp:ListItem>
                            <asp:ListItem>Ethiopia</asp:ListItem>
                            <asp:ListItem>Faeroe Islands</asp:ListItem>
                            <asp:ListItem>Falkland Islands</asp:ListItem>
                            <asp:ListItem>Fiji Islands</asp:ListItem>
                            <asp:ListItem>Finland</asp:ListItem>
                            <asp:ListItem>France</asp:ListItem>
                            <asp:ListItem>French Guiana</asp:ListItem>
                            <asp:ListItem>French Polynesia</asp:ListItem>
                            <asp:ListItem>Gabon</asp:ListItem>
                            <asp:ListItem>Gambia</asp:ListItem>
                            <asp:ListItem>Georgia</asp:ListItem>
                            <asp:ListItem>Germany</asp:ListItem>
                            <asp:ListItem>Ghana</asp:ListItem>
                            <asp:ListItem>Gibraltar</asp:ListItem>
                            <asp:ListItem>Greece</asp:ListItem>
                            <asp:ListItem>Greenland</asp:ListItem>
                            <asp:ListItem>Grenada</asp:ListItem>
                            <asp:ListItem>Guadeloupe</asp:ListItem>
                            <asp:ListItem>Guam</asp:ListItem>
                            <asp:ListItem>Guatemala</asp:ListItem>
                            <asp:ListItem>Guernsey</asp:ListItem>
                            <asp:ListItem>Guinea</asp:ListItem>
                            <asp:ListItem>Guinea Bissau</asp:ListItem>
                            <asp:ListItem>Guyana</asp:ListItem>
                            <asp:ListItem>Haiti</asp:ListItem>
                            <asp:ListItem>Heard Island And McDonald Islands</asp:ListItem>
                            <asp:ListItem>Honduras</asp:ListItem>
                            <asp:ListItem>Hong Kong</asp:ListItem>
                            <asp:ListItem>Hungary</asp:ListItem>
                            <asp:ListItem>Iceland</asp:ListItem>
                            <asp:ListItem Selected="True">India</asp:ListItem>
                            <asp:ListItem>Indonesia</asp:ListItem>
                            <asp:ListItem>Iran</asp:ListItem>
                            <asp:ListItem>Iraq</asp:ListItem>
                            <asp:ListItem>Ireland, Republic Of</asp:ListItem>
                            <asp:ListItem>Isle Of Man</asp:ListItem>
                            <asp:ListItem>Israel</asp:ListItem>
                            <asp:ListItem>Italy</asp:ListItem>
                            <asp:ListItem>Ivory Coast</asp:ListItem>
                            <asp:ListItem>Jamaica</asp:ListItem>
                            <asp:ListItem>Japan</asp:ListItem>
                            <asp:ListItem>Jersey Island</asp:ListItem>
                            <asp:ListItem>Jordan</asp:ListItem>
                            <asp:ListItem>Kazakstan</asp:ListItem>
                            <asp:ListItem>Kenya</asp:ListItem>
                            <asp:ListItem>Kiribati</asp:ListItem>
                            <asp:ListItem>Korea, Democratic Peoples Republic</asp:ListItem>
                            <asp:ListItem>Korea, Republic Of</asp:ListItem>
                            <asp:ListItem>Kuwait</asp:ListItem>
                            <asp:ListItem>Kyrgyzstan</asp:ListItem>
                            <asp:ListItem>Lao, People&#39;s Dem. Rep.</asp:ListItem>
                            <asp:ListItem>Latvia</asp:ListItem>
                            <asp:ListItem>Lebanon</asp:ListItem>
                            <asp:ListItem>Lesotho</asp:ListItem>
                            <asp:ListItem>Liberia</asp:ListItem>
                            <asp:ListItem>Libyan Arab Jamahiriya</asp:ListItem>
                            <asp:ListItem>Liechtenstein</asp:ListItem>
                            <asp:ListItem>Lithuania</asp:ListItem>
                            <asp:ListItem>Luxembourg</asp:ListItem>
                            <asp:ListItem>Macau</asp:ListItem>
                            <asp:ListItem>Macedonia</asp:ListItem>
                            <asp:ListItem>Madagascar (Malagasy)</asp:ListItem>
                            <asp:ListItem>Malawi</asp:ListItem>
                            <asp:ListItem>Malaysia</asp:ListItem>
                            <asp:ListItem>Maldives</asp:ListItem>
                            <asp:ListItem>Mali</asp:ListItem>
                            <asp:ListItem>Malta</asp:ListItem>
                            <asp:ListItem>Marianna Islands</asp:ListItem>
                            <asp:ListItem>Marshall Islands</asp:ListItem>
                            <asp:ListItem>Martinique</asp:ListItem>
                            <asp:ListItem>Mauritania</asp:ListItem>
                            <asp:ListItem>Mauritius</asp:ListItem>
                            <asp:ListItem>Mayotte</asp:ListItem>
                            <asp:ListItem>Mexico</asp:ListItem>
                            <asp:ListItem>Micronesia</asp:ListItem>
                            <asp:ListItem>Moldova</asp:ListItem>
                            <asp:ListItem>Monaco</asp:ListItem>
                            <asp:ListItem>Mongolia</asp:ListItem>
                            <asp:ListItem>Montserrat</asp:ListItem>
                            <asp:ListItem>Morocco</asp:ListItem>
                            <asp:ListItem>Mozambique</asp:ListItem>
                            <asp:ListItem>Myanmar</asp:ListItem>
                            <asp:ListItem>Namibia</asp:ListItem>
                            <asp:ListItem>Nauru</asp:ListItem>
                            <asp:ListItem>Nepal</asp:ListItem>
                            <asp:ListItem>Netherland Antilles</asp:ListItem>
                            <asp:ListItem>Netherlands</asp:ListItem>
                            <asp:ListItem>New Caledonia</asp:ListItem>
                            <asp:ListItem>New Zealand</asp:ListItem>
                            <asp:ListItem>Nicaragua</asp:ListItem>
                            <asp:ListItem>Niger</asp:ListItem>
                            <asp:ListItem>Nigeria</asp:ListItem>
                            <asp:ListItem>Niue</asp:ListItem>
                            <asp:ListItem>Norfolk Island</asp:ListItem>
                            <asp:ListItem>Norway</asp:ListItem>
                            <asp:ListItem>Occupied Palestinian Territory</asp:ListItem>
                            <asp:ListItem>Oman, Sultanate Of</asp:ListItem>
                            <asp:ListItem>Pakistan</asp:ListItem>
                            <asp:ListItem>Palau</asp:ListItem>
                            <asp:ListItem>Panama</asp:ListItem>
                            <asp:ListItem>Papua New Guinea (Niugini)</asp:ListItem>
                            <asp:ListItem>Paraguay</asp:ListItem>
                            <asp:ListItem>Peru</asp:ListItem>
                            <asp:ListItem>Philippines</asp:ListItem>
                            <asp:ListItem>Pitcairn</asp:ListItem>
                            <asp:ListItem>Poland</asp:ListItem>
                            <asp:ListItem>Portugal</asp:ListItem>
                            <asp:ListItem>Qatar</asp:ListItem>
                            <asp:ListItem>Reunion</asp:ListItem>
                            <asp:ListItem>Romania</asp:ListItem>
                            <asp:ListItem>Russian Federation</asp:ListItem>
                            <asp:ListItem>Rwanda</asp:ListItem>
                            <asp:ListItem>Saint Lucia</asp:ListItem>
                            <asp:ListItem>Saint Vincent And The Grenadines</asp:ListItem>
                            <asp:ListItem>Samoa, American</asp:ListItem>
                            <asp:ListItem>Samoa, Independent State Of</asp:ListItem>
                            <asp:ListItem>San Marino</asp:ListItem>
                            <asp:ListItem>Sao Tome &amp; Principe</asp:ListItem>
                            <asp:ListItem>Saudi Arabia</asp:ListItem>
                            <asp:ListItem>Senegal</asp:ListItem>
                            <asp:ListItem>Serbia</asp:ListItem>
                            <asp:ListItem>Seychelles Islands</asp:ListItem>
                            <asp:ListItem>Sierra Leone</asp:ListItem>
                            <asp:ListItem>Singapore</asp:ListItem>
                            <asp:ListItem>Slovakia</asp:ListItem>
                            <asp:ListItem>Slovenia</asp:ListItem>
                            <asp:ListItem>Solomon Islands</asp:ListItem>
                            <asp:ListItem>Somalia</asp:ListItem>
                            <asp:ListItem>South Africa</asp:ListItem>
                            <asp:ListItem>Spain</asp:ListItem>
                            <asp:ListItem>Sri Lanka</asp:ListItem>
                            <asp:ListItem>St. Kitts - Nevis</asp:ListItem>
                            <asp:ListItem>St. Pierre &amp; Miquelon</asp:ListItem>
                            <asp:ListItem>Sudan</asp:ListItem>
                            <asp:ListItem>Suriname</asp:ListItem>
                            <asp:ListItem>Svalbard And Jan Mayen Is</asp:ListItem>
                            <asp:ListItem>Swaziland</asp:ListItem>
                            <asp:ListItem>Sweden</asp:ListItem>
                            <asp:ListItem>Switzerland</asp:ListItem>
                            <asp:ListItem>Syrian Arab Rep.</asp:ListItem>
                            <asp:ListItem>Taiwan, Republic of China</asp:ListItem>
                            <asp:ListItem>Tajikistan</asp:ListItem>
                            <asp:ListItem>Tanzania</asp:ListItem>
                            <asp:ListItem>Thailand</asp:ListItem>
                            <asp:ListItem>Timor Leste</asp:ListItem>
                            <asp:ListItem>Togo</asp:ListItem>
                            <asp:ListItem>Tonga</asp:ListItem>
                            <asp:ListItem>Trinidad &amp; Tobago</asp:ListItem>
                            <asp:ListItem>Tunisia</asp:ListItem>
                            <asp:ListItem>Turkey</asp:ListItem>
                            <asp:ListItem>Turkmenistan</asp:ListItem>
                            <asp:ListItem>Turks And Caicos Islands</asp:ListItem>
                            <asp:ListItem>Tuvalu</asp:ListItem>
                            <asp:ListItem>Uganda</asp:ListItem>
                            <asp:ListItem>Ukraine</asp:ListItem>
                            <asp:ListItem>United Arab Emirates</asp:ListItem>
                            <asp:ListItem>United Kingdom</asp:ListItem>
                            <asp:ListItem>United States</asp:ListItem>
                            <asp:ListItem>United States Minor Outlying Islnds</asp:ListItem>
                            <asp:ListItem>Uruguay</asp:ListItem>
                            <asp:ListItem>Uzbekistan Sum</asp:ListItem>
                            <asp:ListItem>Vanuatu</asp:ListItem>
                            <asp:ListItem>Vatican City State</asp:ListItem>
                            <asp:ListItem>Venezuela</asp:ListItem>
                            <asp:ListItem>Vietnam</asp:ListItem>
                            <asp:ListItem>Wallis &amp; Futuna Islands</asp:ListItem>
                            <asp:ListItem>Western Sahara</asp:ListItem>
                            <asp:ListItem>Yemen, Republic Of</asp:ListItem>
                            <asp:ListItem>Yugoslavia</asp:ListItem>
                            <asp:ListItem>Zambia</asp:ListItem>
                            <asp:ListItem>Zimbabwe</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="devider_20px"></div>



                    <asp:TextBox ID="txtAnyOtherRelevantInformation" TextMode="MultiLine" Rows="5" CssClass="textboxcontact_list" runat="server" placeholder="Any other additional information" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Any other additional information'"></asp:TextBox><div class="devider_10px"></div>


                    <div style="display: inline; padding-right: 35px">

                        <strong>Total Amount in INR : </strong>
                    </div>
                    <div style="display: inline">


                        <asp:Label ID="lblTotalAmountInINR" runat="server" Text="0"></asp:Label>
                    </div>
                    <div class="devider_10px"></div>


                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label><div class="devider_20px"></div>

                    

                    <input type="checkbox" id="chkTearmAndCondition" title="Terms And Conditions" />
                    I agree to all the <a href="Little_Hearts_Marathon-terms.aspx">terms, conditions and guidelines.</a>


                    <div class="devider_20px"></div>


                    <asp:Button ID="btnPayment" Text="Pay now" ValidationGroup="vChildDonationOnlineAdd" CssClass="btn" runat="server" OnClientClick="return validateChildCnt();" OnClick="btnPayment_Click" />
                </div>

            </div>
        </div>
    </div>
</asp:Content>

