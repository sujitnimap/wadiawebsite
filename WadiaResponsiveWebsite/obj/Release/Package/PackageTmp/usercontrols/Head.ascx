﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Head.ascx.cs" Inherits="eBookReader.admin.usercontrols.Head" %>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="50" valign="middle" class="logobg">
            <div style="height: 5px; background-color: #FFF;"></div>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="headertxt">
                <tr>
                    <td class="heading" align="center" width="100%">
                        <div>
                            <%---------header----------%>
                            <ul id="menuhorz">
                                <li id="home"><a href="../index.aspx" class="homeIcon" target="_blank">Home</a></li>
                                <li id="news"><a href="event.aspx">Event</a></li>
                                <li id="about"><a href="careers.aspx">Career</a></li>
                                <li id="services"><a href="jobs.aspx">Job</a></li>
                                <li id="contact"><a href="tenders.aspx">Tender</a></li>
                                <li id="testimonial"><a href="testimonial.aspx">Testimonial</a></li>
                                <li id="category"><a href="category.aspx">Category</a></li>
                                <li id="image"><a href="gallery.aspx">Image Gallery</a></li>
                                <li id="video"><a href="vgallery.aspx">Video Gallery</a></li>
                                <li id="case"><a href="casestudy.aspx">Latest News</a></li>
                                <li id="media"><a href="press_n_media.aspx">Press-Media</a></li>
                                <li id="corporate"><a href="corporate.aspx">Corporate</a></li>
                                <li id="awards"><a href="awardsnachivements.aspx">Awards-Achievements</a></li>
                                <%--<li id="subscribe"><a href="subscribe-users.aspx">Subscribe Users</a></li>--%>
                                <li id="onlineinfo"><a href="onlineDonationInformation.aspx">online Donation Info</a></li>
                                <li id="paydtl"><a href="payUpaymentDetails.aspx">Payment Details</a></li>
                                <li id="cmel"><a href="CMERegMasterList.aspx">CME Details</a></li>
                                <li id="reginfo"><a href="RegistrationOfMarathonInfo.aspx">Registrtaion Info</a></li>
                                <li id="sponsorinfo"><a href="SponsorARunInfo.aspx">Sponsor Info</a></li>
                                <li id="adultparticipant"><a href="AdultParticipantOfMarathon.aspx">Adult Participant Info</a></li>
                                <li id="childparticipant"><a href="ChildParticipantOfMarathon.aspx">Child Participant Info</a></li>
                                <li id="successRegstOfMarathon"><a href="SuccessRegistrationOfMarathon.aspx">SuccessfulRegistration</a></li>
                            </ul>

                            <%-----------End header-----------%>
                        </div>
                    </td>
                </tr>
            </table>
            <div align="right">
                <table>
                    <tr>
                        <td width="120" align="right">
                            <asp:Label ID="lblName" Font-Bold="true" Font-Size="13px" runat="server" Visible="true"
                                Text="Welcome Admin" ForeColor="Black"></asp:Label>
                        </td>
                        <td width="20" align="center">
                            <asp:Image ID="imgarrow" runat="server" ImageUrl="~/adminpanel/images/Admarrow.png" />
                        </td>
                        <td width="65" align="left">

                            <asp:ImageButton ID="ImgLogout" runat="server" CausesValidation="false" ImageUrl="~/adminpanel/images/Admlogout.jpg"
                                OnClick="ImgLogout_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="height: 5px; background-color: #FFF;"></div>
        </td>
    </tr>
</table>
