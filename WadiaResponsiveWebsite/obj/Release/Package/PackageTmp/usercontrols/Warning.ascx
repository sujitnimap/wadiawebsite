﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Warning.ascx.cs" Inherits="eBookReader.usercontrols.Warning" %>

<table width="420" class="warning" border="0" cellspacing="0" cellpadding="0"
    style="border: 1px solid #FF0000; margin-bottom: 20px; background-color: #FFCC99; height: 35px;">
    <tr>
        <td>
            <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="45" align="center">
                        <img src="../images/warning_user.jpg" width="26" height="26" alt="warning" /></td>
                    <td align="left">
                        <asp:Label ID="lblwarning" runat="server" ForeColor="LightYellow"></asp:Label></td>
                </tr>
            </table>
        </td>
    </tr>
</table>


