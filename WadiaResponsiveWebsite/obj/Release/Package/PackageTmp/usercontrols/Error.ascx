﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Error.ascx.cs" Inherits="eBookReader.usercontrols.Error" %>

<table width="449" class="error" border="0" cellspacing="0" cellpadding="0" style="border: 1px solid #FF0000; margin-bottom: 20px; background-color: #FFCCCC; height: 35px;">
    <tr>
        <td>
            <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="45" align="center">
                        <img src="../images/img4.png" width="26" height="26" alt="Error" />
                    </td>
                    <td align="left">
                        <strong>Error:</strong>&nbsp;<asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
