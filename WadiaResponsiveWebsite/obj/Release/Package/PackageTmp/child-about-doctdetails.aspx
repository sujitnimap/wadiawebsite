﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-doctdetails.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


		<div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Arvind  Madhusudan Vartak.jpg" />
<div class="docdetails">

Dr. Arvind  Madhusudan Vartak

<div class="docdetailsdesg">
Hon. Ped. Burns & Plastic
    <br />
M.S. General Surgery, M.S. Plastic Surgery

</div>

</div>
</div>

<div class="areaofinterest">

    Areas of Interest : Management Of Acute Burn, Post Burn Deformities and Disabilities

</div>


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
            <%--<li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Research/Publications</a></li>
            <li><a href="#view4">Contribution To Books</a></li>
            <%--<li><a href="#view5">Contribution To Books  </a></li>--%>
        </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
           <%-- <div id="view1">
                
                <p>
                
              Management Of Acute Burn, Post Burn Deformities and Disabilities
                    
                    
                    <br />
              
                </p>
                
            </div>--%>
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
         President Of National Academy Of Burns India 2006-2007    
                
                
                </P>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
                
                

Papers  Published  :
                     <div class="leftpaddocdetail">
                    <br />
                  

•	Proceedings Of The National Multidisciplinary Symposium On Burns (1974) 122 – 124 The Tata Dept. Of Plastic Surgery Sir J. J. Group Of Hospitals, Bombay.
“Role Of Mesh Grafting In The Treatment Of Burns”.  Dr. A. M. Vartak. 
<br />
                    <br />
•	Indian Journal Of Occupational Health, June 1981 “Burns At Port Trust Hospital Bombay --An Epidemiological Study 1970-1979”. A. M. Vartak, A. B. Bhajekar.
<br />
                    <br />
•	 Burns (1990) 16, (2), 137 –143. “Histological And Bacteriological Studies Of Burn   Wound Treated With Boiled Potato Peel Dressings.”  M. H. Keswani,  A. M. Vartak, A. Patil, J.W.L.Davies.
<br />
                    <br />
•	Indian Journal Of Surgery (1991), 53 (10), 399 – 403 “The Boiled Potato Peel Dressing (Its Use In The Management Of The Burn Wound.)
<br />
                    <br />
•	Dr. A. M. Vartak, Dr. M. H. Keswani, Sr. M. Savitri, Ms. A. R. Patil.
<br />
                    <br />
•	Burns (1991) 17, (3), 239 –242.
“Cellophane – A Dressing For Split – Thickness Skin – Graft Donor Sites.”
M. Vartak, M. H. Keswani, A. R. Patil, Sr. Savitri, Sr. B. Fernandes.
<br />
                    <br />
•	Burns (1992) 18. (2) 157 –158.
“X-Plasty For Repair Of Burn Contractures.”
M. Vartak,  M. H. Keswani.
<br />
                    <br />
•	Burns (1992) 18, (2), 157 – 158.
“New Economical Skin Graft Expansion Wheel”.
M. Vartak
<br />
<br />
•	Indian Journal Of Plastic Surgery (2009) 42,(2), 213 -- 218.   
“Bacteriology Of The Burn Wound At The Bai Jerbai Wadia Hospital For Children, Mumbai, India----A 13-Years Study, Part(I)-Bacteriological Profile”
M. Vartak ,Shankar Srinivasan, Aakanksha Patil, Jovita Saldhana
<br />
                    <br />












       </p>
                    </div>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
           <div class="leftpaddocdetail">
                <p>
                
           
•	“Emergency Management In Burns” Ch.  21, Pg. No. 414 – 424,  A Hand Book Of Emergencies,  5th Edition (2005),  Editor – Dr. A. F. Golwalla,   A. M. Vartak. 
<br />
                    <br />

•	Pathophysiology Of Burn Shock, Ch.4,  Pg.No.37-41.  “Principles And Practice Burn Care”, 1st Edition (2010), Editor-Sujata Sarabahi, V K Tiwari, Arun Goel, Lc Gupta, 
Publisher – Jaypee Brothers Medical Publishers (P) Ltd.
©2010 Jaypee Brothers Medical Publishers (P) Ltd. 
<br />
                    <br />


•	Pathophysiology Of Burn Wound, Ch. 5, Pg. No. 42-50, “Principles And Practice Burn Care”, 1st Edition (2010), Editor-Sujata Sarabahi, V K Tiwari, Arun Goel, Lc Gupta, Publisher – Jaypee Brothers Medical Publishers (P) Ltd.
©2010 Jaypee Brothers Medical Publishers (P) Ltd.
<br />
                    <br />

•	Fluid Resuscitation--Current Concepts And Special Needs In Paediatric Burns, Ch. 6 Pg. 57-77, “Paediatric Burns Total Management Of The Burned Child”, 1st Edition (2011), Editor – Marella L Hanumadass, K. Mathangi Ramakrishnan, Co-Editor – Pralhad  K Bilwani, Arvind Vartak, Foreword By – Yk Amdekar,   Publisher – Paras Medical Publisher. 
©2011 Hanumadass, Ramkrishnan
<br />
                    <br />

•	Metabolism And Nutritional Support In Children With Major Burn Injury, Ch.7 Pg. 78 – 98, “Paediatric Burns Total Management Of The Burned Child”, 1st Adition (2011), Editr – Marella L Hanumadass, K. Mathangi Ramakrishnan, Co-Aditor – Prhalad K Bilwani, Arvind Vartak, Foreword By – Yk Amdekar,   Publisher – Paras Medical Publisher.  
©2011 Hanumadass, Ramkrishnan
<br />
                    <br />

•	 “Burns”, Quarterly Medical Review Vol.62, No. 3, July-September 2011. Publisher – Raptakos, Brett & Co. Ltd., 

                </p>
                </div>
            </div>
            
            <!---- 4th view end ---->     
            
            
            
            
            
            
            
            
            
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			

    <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
									<li><a href="child-about-overview.aspx">Overview</a></li>
									<li><a href="child-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="child-about-chairman-msg.aspx">Chairman's message</a></li>
									<li><a href="child-about-awards.aspx">Awards and Achievements</a></li>
									<li><a href="child-about-history.aspx">History </a>
                                        <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>



		</div>
	











</asp:Content>
