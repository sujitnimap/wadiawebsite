﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-neurology.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_neurology" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Paediatric Neurology </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
The department of Paediatric Neurology was started at the BJWHC in 1993. Over years it has developed into a tertiary referral centre for entire Western India. 
                It runs 4 specialised clinics every week that run from 9 am to 4 pm, well beyond their routine time of 1 pm. Each clinic takes care of 
                subsets of patients suffering from developmental delay, autism spectrum disorders, epilepsy and general neurology, The department also has facilities for video EEG and EMG  and work closely with imaging specialists and neurosurgeons.


 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
The Neurology department offers care and treatment to patients suffering from the following neurological conditions :

                                <br /><br />
                <div style="padding-left:10px">

-	Neurological Diseases & Disorders
<br />-	 Arachnoid Cysts
<br />-	 Bone Dysplasia/Achondroplasia
<br />-	 Brachial Plexus Injury
<br />-	 Brain Tumour
<br />-	 Chiari/Syringomyelia
<br />-	 Craniofacial Abnormalities
<br />-	 Craniosynostosis
<br />-	 Epilepsy 
<br />-	 Foetal  CNS Abnormalities
<br />-	 Fibrous Dysplasia
<br />-	 Hydrocephalus
<br />-	 Ventriculoperitoneal shunting
<br />-	 Macrocephaly
<br />-	 Neural Tube Defects
<br />-	 Meningocele repair   
<br />-	 Myelomeningocele
<br />-	 Neurofibromatosis
<br />-	 Plagiocephaly
<br />-	 Pseudotumor Cerebri
<br />-	 Skull Fracture
<br />-	 Spasticity/Cerebral Palsy
<br />-	 Spinal Cord Injury/Disc Herniation/Back Pain
<br />-	 Stroke/Cerebrovascular Aneurysm/Moya Moya
<br />-	 Tethered Spinal Cord
<br />-	 Traumatic Brain Injury
<br />-	 Tuberous Sclerosis
<br />-	 Vascular Malformations








</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

           




		</div>




</asp:Content>
