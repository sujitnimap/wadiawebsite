﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-events.aspx.cs" Inherits="WadiaResponsiveWebsite.women_events" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>EVENTS</h2>
                </div>

                <br />

                <!-- EVENT CONTENT -->

                <div style="line-height: 20px">
                    <div id="container">
                        <div class="pagination">
                            <asp:DataList class="page gradient" RepeatDirection="Horizontal" runat="server" ID="dlPagerup"
                                OnItemCommand="dlPagerup_ItemCommand">
                                <ItemTemplate>

                                    <a id="pageno" runat="server">
                                        <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>
                                    </a>

                                </ItemTemplate>
                            </asp:DataList>

                        </div>

                        <div align="left"><b>Events Archives</b></div>
                        <br />
                        <asp:DataList ID="dlisteventdata" Width="100%" runat="server" BorderColor="#666666"
                            BorderStyle="None" BorderWidth="2px" CellPadding="3" CellSpacing="2" Font-Names="Verdana" Font-Size="Small" GridLines="Both" RepeatColumns="1">
                            <ItemTemplate>
                                <div class="devider_10px"></div>
                                <table style="width: 100%">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lbleventname" Font-Bold="true" Font-Size="12px" Font-Names="Arial" runat="server" Text='<%#Eval("eventname") %>'></asp:Label></td>
                                    </tr>
                                    <%--<tr>
                                        <td colspan="2">
                                            <asp:Label ID="lbldescription" runat="server" Text='<%#Eval("description") %>'></asp:Label></td>
                                    </tr>--%>
                                    <tr>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%"> Date </td>
                                        <td>:&nbsp;<asp:Label ID="lblstartdate" runat="server" Text='<%#Eval("startdate") %>'></asp:Label></td>
                                    </tr>
                                    <%--<tr>
                                        <td style="width: 20%">End Date </td>
                                        <td>:&nbsp;<asp:Label ID="lblenddate" runat="server" Text='<%#Eval("enddate") %>'></asp:Label></td>
                                    </tr>--%>
                                    <tr>
                                        <td style="width: 20%">Start Time </td>
                                        <td>:&nbsp;<asp:Label ID="lblstarttime" runat="server" Text='<%#Eval("starttime") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%">End Time </td>
                                        <td>:&nbsp;<asp:Label ID="lblendtime" runat="server" Text='<%#Eval("endtime") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%">Location </td>
                                        <td>:&nbsp;<asp:Label ID="lbllocation" runat="server" Text='<%#Eval("location") %>'></asp:Label></td>
                                    </tr>

                                </table>
                                <div align="right">
                                    <a id="A1" href='<%# "women-event.aspx?id="+ Convert.ToString(Eval("id"))  %>' runat="server" class="readmorebtngrns">
                                        <img src="images/readmore2.png" /></a>
                                </div>
                                <%--<div class="devider_10px" style="border-bottom: 2px solid"></div>--%>
                                <div class="devider_10px"></div>
                            </ItemTemplate>
                        </asp:DataList>

                        <div class="devider_15px"></div>

                        <div class="pagination">
                            <asp:DataList class="page gradient" RepeatDirection="Horizontal" runat="server" ID="dlPagerdown"
                                OnItemCommand="dlPagerdown_ItemCommand">
                                <ItemTemplate>

                                    <a id="pageno" runat="server">
                                        <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>
                                    </a>

                                </ItemTemplate>
                            </asp:DataList>
                        </div>

                    </div>

                </div>
                <!-- EVENT CONTENT -->

            </div>
        </div>


    </div>


</asp:Content>
