﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="Little_Hearts_Marathon-terms.aspx.cs" Inherits="WadiaResponsiveWebsite.Little_Hearts_Marathon_terms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>Terms & Conditions</h2>
                </div>
                                
                <br />
                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px;">
                   
• Please choose the event category carefully, confirmed registrations are non-refundable, non-transferable and cannot be modified. Provide us with a secure email ID/mobile number that you can access regularly, since this will be our primary means of communication during the run up to the event.

<div class="devider_10px"></div>

• Users of email services that offer filtering/blocking of messages from unknown email address should add this email id <em><strong>marathon@wadiahospitals.org</strong></em> to their address list.
<div class="devider_10px"></div>

• We will be sending regular updates to the Mobile number you have provided in the registration form this should be not treated as spam and you shall not take any action against our bulk sms service provider.

<div class="devider_10px"></div>

• Any notice sent to the email address registered with the organizers shall be deemed as received by the runners.

<div class="devider_10px"></div>

• Please fill out only those fields that are necessary for mailing purposes. Do not provide redundant data in multiple fields. (i.e., do not list the same data for city, province and country), as this will only complicate our ability to contact you, if necessary.

<div class="devider_10px"></div>

• You are aware that running / long distance running is an extreme sport and can be injurious to body and health. You take full responsibility for participating in the Little Hearts Marathon 2018 event and do not hold the organizing committee or any of its members or entities responsible for any injury or accident.

<div class="devider_10px"></div>

• You shall consult your physician and undergo complete medical examination to assess your suitability to participate in the event.

<div class="devider_10px"></div>

• You also assume all risks associated with participating in this event including, but not limited to, falls, contact with other participants, the effects of the weather, including high heat or humidity, traffic and the condition of the road, arson or terrorist threats and all other risks associated with a public event.

<div class="devider_10px"></div>

• You agree that Little Hearts Marathon 2018 shall not be liable for any loss, damage, illness or injury that might occur as a result of your participation in the event.

<div class="devider_10px"></div>

• You agree to abide by the instructions provided by the organizers from time to time in the best interest of your health and event safety.

<div class="devider_10px"></div>

• You also agree to stop running if instructed by the Race Director or the Medical Staff or by the Aid Station Volunteers.

<div class="devider_10px"></div>

• You confirm that your name and media recordings taken during your participation may be used to publicize the event.

<div class="devider_10px"></div>

• You may acknowledge and agree that your personal information can be stored and used by Little Hearts Marathon 2018 or any other company in connection with the organization, promotion and administration of the event and for the compilation of statistical information.
• You confirm that, in the event of adverse weather conditions, major incidents or threats on the day, any of the force majeure or restriction by authority, the organizers reserve the right to stop/cancel/postpone the event. You understand that confirmed registrations and merchandise orders are non-refundable, non-transferable and cannot be modified. The organizers reserve the right to reject any application without providing reasons. Any amount collected from rejected applications alone will be refunded in full (excluding bank charges wherever applicable)

<div class="devider_10px"></div>

• For any reason you cannot turn up on race day, no refund of any form will be given.

<div class="devider_10px"></div>

• Wadia Hospitals is only a service provider and is not in any way responsible for the delivery of the event.

<div class="devider_10px"></div>

• If this registration is being made on behalf of a minor, I confirm that I am the parent / guardian of the child and that he/She has my permission to take part in the event. I further concur that all the above rules shall apply to him/her as if he were a major.

<div class="devider_10px"></div>

• Participants must personally appear for collecting the running Bibs. Bibs can be distributed in-absentee with written consent of the registered runners.

<div class="devider_10px"></div>

• Runners will not be allowed to stay on the course beyond the cut-off time considering the safety and health issues. We request total co-operation from runners in this regards.

<div class="devider_10px"></div>

 
 



                </div>

            </div>




            <%--    SUPPORTIVE END--%>
        </div>

    </div>

</asp:Content>
