﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-supportive-services.aspx.cs" Inherits="WadiaResponsiveWebsite.women_supportive_services" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>services</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

                     <div class="serviceheading">SUPPORTIVE SERVICES</div>

    <div style="padding-left:30px">

     <div class="devider_20px"></div>
    
 


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13%" height="55" valign="middle"><img src="images/services/Audiology & Speech therapy.png" width="46" height="52" /></td>
    <td class="servicename" width="87%"><a href="child-audiology-speech.aspx">	Audiology & Speech therapy  </a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/services/Blood bank.png" width="52" height="42" /></td>
    <td class="servicename"><a href="child-blood-bank.aspx"> 	Blood bank </a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/services/Nutrition- Dietition.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-nutrition.aspx"> 	Nutrition- Dietition </a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/services/Physiotherapy  &  Occupational Therapy.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-physiotherapy.aspx"> 	Physiotherapy  &  Occupational Therapy  </a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/services/Pathology.png" width="52" height="44" /></td>
    <td class="servicename"><a href="child-pathology.aspx"> 	Pathology</a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/services/Radiology.png" width="47" height="47" /></td>
    <td class="servicename"><a href="child-radiology.aspx">     Radiology  </a></td>
  </tr>

    <tr>
    <td valign="middle" height="55"><img src="images/services/Social Service Department.png" width="47" height="47" /></td>
    <td class="servicename"><a href="child-socialserv-dept.aspx">     Social Service Department  </a></td>
  </tr>



</table>






</div>

</div>




                
                    
                    
				</div>
			</div>



</asp:Content>
