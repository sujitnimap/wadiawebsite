﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-doctdetails-drdivya.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drdivya" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
		<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Divya Prabhat.png" />
<div class="docdetails">
Dr. Divya Prabhat
<div class="docdetailsdesg">
Hon. ENT Surgeon
    <br />
MS, DORL, DNB, FICS



</div>

</div>
</div>

                    <div class="areaofinterest">


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
  •	Ethicon travel fellowship award of AOI in 1992 
<br /><br />•	Best Consultant paper award at AOI conference 2001
<br /><br />•	Nominated as Top Doctors in ENT by India today for  2012 /13.



<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	President, Association of Otolaryngologists of India (Mumbai, 2000)
<br /><br />•	Advisor, Clinical Research Institute
<br /><br />•	Advisor, Indian Practitioner Journal
<br /><br />•	Member of www.indiandoctorsguide.com and www.pediatriconcall.com


    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	36 Published  research articles in national and international journals
<br /><br />•	Contributed to the ENT chapters in
                    
                    <div style="padding-left:30px"> <br />
o	Text book of Adolescent Medicine
<br /><br />o	100 Questions in Paediatrics
<br /><br />o	Principles of Asthma and Allergy 
<br /><br />o	History taking and examination in the Adolescent, Undergraduate textbook of Paediatrics.
                        </div>
   </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
         </div>




</asp:Content>
