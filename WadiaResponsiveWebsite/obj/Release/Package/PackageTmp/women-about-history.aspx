﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-about-history.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_history" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading"><h2>HISTORY</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">

   In a country where Women’s Healthcare was only just being established and at a time when focus was shifting to women’s requirements and their empowerment, Sir Ness Wadia understood the importance of catering to the medical needs of women across the board, regardless of their economic status. 
<br />
<br />
True to the philanthropic tradition of the Wadia family, he set up the Nowrosjee Wadia Maternity Hospital in fond memory of his father Nowrosjee Wadia. It was designed by Architect George Wittet, who also designed the Gateway of India, the Prince of Wales museum, and many other renowned landmarks in the city. The foundation stone was laid on 9th June 1925 and the hospital was declared open on 13th December 1926.
<br />
<br />
This joint venture between the State Government and the Bombay Municipality at the time, was set up to provide affordable comprehensive healthcare and super speciality services for women and to ensure that their changing needs are met through the comprehensive services that we provide.


<br />
<br />
</div>
<!-- ABOUT US CONTENT -->

           
                    
                    
				</div>
			</div>
			

        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="women-about-overview.aspx">Overview</a></li>
									<li><a href="women-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="women-about-chairman-msg.aspx">Chairman's message</a></li>
									<%--<li><a href="women-about-awards.aspx">Awards and Achievements</a></li>--%>
									<li><a href="women-about-history.aspx">History </a>

                                         <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-nusserwanji-wadia.aspx">Nowrosjee Nusserwanji Wadia</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-ness-wadia.aspx">Sir Ness Wadia</a>

                                        </div>


									</li>
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>



		</div>





</asp:Content>
