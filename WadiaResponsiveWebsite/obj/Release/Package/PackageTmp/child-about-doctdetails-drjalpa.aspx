﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-doctdetails-drjalpa.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drjalpa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


  <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dr.jpg" />
<div class="docdetails">
Dr. Jalpa Parikh

<div class="docdetailsdesg">
BPT, MPT    <br />
Physiotherapist


</div>

</div>
</div>

     <%--<div class="areaofinterest">

    Areas of Interest : Paediatric Infectious Diseases, Paediatric Hepatology

</div>--%>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
             
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
     SEMINARS ATTENDED:
                    <br />
                   <div style="padding-left:25px;">
•	Introduction and update on Evidence Based Physiotherapy and Clinical Practice
<br />•	Guidelines held at Govt. Physiotherapy College, Ahmedabad.
<br />•	Ergonomics held at S.B.B. College of Physiotherapy, Ahmedabad.
<br />•	Pathophysiology of pain and pain management at Pramukhswami Medical College, Karamsad.
<br />•	Yoga and Rehabilitation held at S.B.B. College of Physiotherapy, Ahmedabad.
<br />•	Biomechanics and Applied Biomechanics in Sports Medicine held at 
<br />•	Govt. Physiotherapy College, Ahmedabad.
<br />•	Hemiplegia – Assessment and management Govt. Physiotherapy College, Ahmedabad.
<br />•	Voluntary Control of Movement held at Pramukhswami Medical College, Karamsad.

                       </div>

WORKSHOPS ATTENDED:
<br />
                      <div style="padding-left:25px;">
•	Hands on Workshop on Manual Therapy Concept (Maitland, Mckanzie, Cyriax) for vertebral column and pelvic complex by Prof. Umashankar Mohanti.
<br />•	Workshop of EMG and NCV.
<br />•	Training Workshop on Emergency Medicine.

</div>

CONFERENCES ATTENDED: 
                    <br />

  <div style="padding-left:25px;">

•	47th Annual Conference of IAP (WCPT- AWP and IAP congress 2009), Mumbai.
<br />•	45th Annual Conference of IAP at Kolkata.
<br />•	Gujarat Conphycs-08 at Shree Swaminarayan Physiotherapy College, Surat.
<br />•	Conphycs-07 at K.M. Patel Institute of Physiotherapy, Karamsad.

             
            </div>
                    </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			<div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
									<li><a href="child-about-overview.aspx">Overview</a></li>
									<li><a href="child-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="child-about-chairman-msg.aspx">Chairman's message</a></li>
									<li><a href="child-about-awards.aspx">Awards and Achievements</a></li>
									<li><a href="child-about-history.aspx">History </a>


                                        <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>


									</li>
									
								</ul>
							</div>
						</div>
					</div>
				








				</div>
			</div>









		</div>



</asp:Content>
