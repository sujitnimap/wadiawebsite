﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="SponsorARunPayUResponse.aspx.cs" Inherits="WadiaResponsiveWebsite.SponsorARunPayUResponse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function printDiv(divID) {

            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head><title></title></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div id="fb-root"></div>
    <script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <br />

                <!-- EVENT CONTENT -->

                <div style="text-align: center">
                    <%--  Message :--%>
                    <asp:Label ID="lblAddedSuccessfully" Visible="false" runat="server" Font-Size="20px" Font-Bold="true" ForeColor="#000066"></asp:Label>
                    <br />
                    <%--<strong>Transaction Number :</strong>--%>
                    <asp:Label ID="lblTransactionNumber" Text="" Visible="false" runat="server" ForeColor="#000066"></asp:Label>

                    <%--<strong>Added Successfully :</strong>--%>
                    <asp:Label ID="lblPaymentSuccessMessage" runat="server" ForeColor="#000066"></asp:Label>

                    <br />
                </div>


                <asp:Button ID="btnprint" runat="server" Text="Print"  Font-Bold="true"  Width="70px" Height="30px" OnClientClick="printDiv('dataDiv')" />

                <br />
                    <div class="fb-share-button" data-href="https://www.facebook.com/littleheartsmarathon?ref=stream" data-layout="box_count"></div>
                

                <div runat="server" id="dataDiv">
                </div>



                <!-- EVENT CONTENT -->

            </div>
        </div>


    </div>

</asp:Content>
