﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="gallery_responsive.aspx.cs" Inherits="WadiaResponsiveWebsite.gallery_responsive" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   

    	<link href="css/anoflow.css" rel="stylesheet" type="text/css" />
    <link href="http://www.anowave.com/factory/cdn/import.css" rel="stylesheet" type="text/css" />
		<link href="http://www.anowave.com/factory/cdn/styles/rainbow/themes/github.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
		<!-- /.CDN -->
		<script type="text/javascript" src="js/jquery.anoflow.js"></script>
		<!--[if lt IE 9]>
		   <script>
		      document.createElement('header');
		      document.createElement('nav');
		      document.createElement('section');
		      document.createElement('article');
		      document.createElement('aside');
		      document.createElement('footer');
		      document.createElement('figure');
		   </script>
		<![endif]-->
		<style type="text/css">
			ul.images { margin-top:50px; } 
			ul.images, 
			ul.images li { display:block; }
			ul.images li { width:100px; text-align:center; float:left; border:3px solid #515151; padding:1px; margin:0px 5px 5px 0px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; }
			ul.images li img { margin:0px; width:100%; cursor:pointer; float:left; }
			
			.logo { height:100px; padding:0px 0px 0px 110px; background:url(img/logo.png) 0 0 no-repeat; }
		</style>





<div class="row block05">		
<div class="row block05">
<div class="col-full">
		
    
    <div style="line-height:20px; padding:0px 20px 20px 20px" class="colorcontent">
        
        		
 <div class="heading">
                  <h2>Well Baby Clinic</h2></div>
                    
               
<!-- ABOUT US CONTENT -->


<div class="wrapper">
			
    			<ul class="images fix">
				<ul class="images fix">
					<li><img class="lightbox" data-caption="anoFlow is fully responsive lightbox gallery built on top of jQuery" data-src="slides/1.jpg" src="slides/thumbnails/1.jpg" /></li>
					<li><img class="lightbox" data-caption="<div class='logo'><strong>Captions can include HTML</strong><br/><br />It really depends on your imagination and thinking.</div>" data-src="slides/2.jpg" src="slides/thumbnails/2.jpg" /></li>
					<li><img class="lightbox" data-caption="You can use your keyboard left and right arrows to <br/>navigate the entire gallery quickly." data-src="slides/3.jpg" src="slides/thumbnails/3.jpg" /></li>
					<li><img class="lightbox" data-src="slides/4.jpg" src="slides/thumbnails/4.jpg" /></li>
					<li><img class="lightbox" data-src="slides/5.jpg" src="slides/thumbnails/5.jpg" /></li>
					<li><img class="lightbox" data-src="slides/6.jpg" src="slides/thumbnails/6.jpg" /></li>
					<li><img class="lightbox" data-src="slides/7.jpg" src="slides/thumbnails/7.jpg" /></li>
					<li><img class="lightbox" data-src="slides/8.jpg" src="slides/thumbnails/8.jpg" /></li>
					<li><img class="lightbox" data-src="slides/9.jpg" src="slides/thumbnails/9.jpg" /></li>
				</ul>
			</ul>
		</div>
		<script type="text/javascript">
		    var flow = $('img.lightbox').anoFlow(
			{
			    distance: 100,
			    easing: 'linear',
			    defaultSize:
				{
				    width: 420,
				    height: 200
				},
			    timeout: 300,
			    blockUI:
				{
				    speed: 300,
				    opacity: 0.4,
				    zIndex: 9998,
				    background: 'rgb(0,0,0)'
				},
			    resize:
				{
				    speed: 400,
				    optimize: 100
				},
			    image:
				{
				    fade: 300,
				    draggable: false
				},
			    onEnd: function (index, content) {
			        /* Add extra logic once animation ends */
			    }
			}).data('anoFlow');
		</script>



<!-- ABOUT US CONTENT -->


</div>
</div>
</div>
</div>

</asp:Content>
