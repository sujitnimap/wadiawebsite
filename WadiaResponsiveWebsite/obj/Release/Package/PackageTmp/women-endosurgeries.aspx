﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-endosurgeries.aspx.cs" Inherits="WadiaResponsiveWebsite.women_endosurgeries" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Endoscopic Surgeries</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
        <%--    <li><a href="#view3">Achievements</a></li>
            
           --%>         </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
               Endoscopic surgeries involve using scopes that are inserted through small incisions or natural body openings in order to diagnose and treat disease. The hospital, a pioneer in the field of endoscopic surgeries, has a fully equipped endoscopic theatre and all laparoscopic surgeries like hysterectomy and myomectomy are performed by our specialists.
<br /><br />
                
                <b>Services Provided:</b>
 <br /><br />               
                 
-	Laparoscopy
<br />-	Cystoscopy
<br />-	Endosurgery
<br />-	Minimal access surgery
<br />-	Endoscopic microsurgery
<br />-	Videoendoscopic surgery
<br />-	Endoscopic fertility enhancing surgeries
<br />-	Laparoscopic Hysterectomies
<br />-	Endoscopic Myomectomies/Fibroid Surgery
<br />-	Endoscopic Uterine displacement corrective surgeries
<br />-	Endoscopic Neo-Vagina Creation Surgeries

 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <%--<div id="view3">
               
             -	We are recognized by the Govt. of India, Ministry of Health and Family Welfare, for Laparoscopic Training 
<br />
 <br />
-	Our institution is recognized by the Govt. of Maharashtra as a training center for Laparoscopic Training in Maharashtra


               
            </div>--%>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

		</div>
	








</asp:Content>
