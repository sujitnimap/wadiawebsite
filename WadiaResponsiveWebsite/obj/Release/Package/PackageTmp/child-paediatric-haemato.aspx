﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-haemato.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_haemato" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PAEDIATRIC HAEMATO-ONCOLOGY AND IMMUNOLOGY</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
The Paediatric Haematology-Oncology department has been rendering its services to children suffering from blood disorders and cancer since 1985. Paediatric 
                Haemato-Oncologists are experts who treat children with blood disease or cancer. Children with these conditions have special physical, 
                emotional and psychological needs. They require the highest level of medical expertise and highly specialized medical treatment combined 
                with very personalized support from their physicians and care staff. Our doctors of the children's cancer centre are trained and experienced 
                in supporting children during treatment for serious illnesses. Their skill and compassion can make all the difference for a child and family 
                during a difficult time and help in renewing health and restoring hope. 
                <br />
                The hospital is also a University recognized centre, which offers 
                a Post Doctoral Fellowship in Paediatric Hemato-Oncology. The department provides a regular blood transfusion service to children born with 
                Thalassemi Major. Immunology is a subset of haemato-oncology a newer specialty that offers diagnosis and management of children born 
                with defective immunity resulting in recurrent serious infections. Such services are extremely scarce in India and ours is one of the few 
                hospitals that offer such specialised services dedicated to children. These immune disorders are far more difficult to manage and our experts 
                have large experience in managing them.   <br />
Besides our expert doctors, the department boasts of excellent services of two social workers who support patients’ monitory and psychosocial needs  

 <br />
 <br />

     

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
                
This specialty covers the diagnosis, treatment, prognosis and prevention of blood diseases and cancer of the blood and lymphatic system.



 <br />
 <br />


Conditions we treat include:
                                                                                <br /><br />
                <div style="padding-left:10px">
-	AIHA
<br />-	ALL (Acute Lymphoblastic Leukaemia)
<br />-	AML (Acute Myeloid Leukaemia)
<br />-	Aplastic Anaemia
<br />-	Burkits’ Lymphoma
<br />-	CML
<br />-	CNS Tumour
<br />-	Ewing’s Sarcoma
<br />-	Germ Cell Tumour
<br />-	Haemophilia
<br />-	Haepatoblastoma
<br />-	Hereditary Spherocytosis
<br />-	Hodgkin’s Lyphoma
<br />-	ITP (Idiopathic Thrombocytopenic Purpura)
<br />-	JMML
<br />-	LCH
<br />-	Lipoblastoma
<br />-	MDS
<br />-	Megaloblastic Anaemia
<br />-	Neuroblastoma
<br />-	NHL
<br />-	Osteosarcoma and Ewing's sarcoma
<br />-	PNET
<br />-	Retinoblastima
<br />-	Sickle Cell Anaemia
<br />-	Thalassemia Major




</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

           


		</div>




</asp:Content>
