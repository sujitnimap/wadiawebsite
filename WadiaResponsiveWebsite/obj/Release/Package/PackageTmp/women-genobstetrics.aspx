﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-genobstetrics.aspx.cs" Inherits="WadiaResponsiveWebsite.women_genobstetrics" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Obstetrics</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
              •	<b>OPD</b> : The Obstetrics OPD (Out-Patient Department) provides comprehensive evaluation, diagnosis, management and counselling to women with complications during their pregnancies. Prenatal diagnosis and management is available for conditions such as Rh isoimmunisation and thalassemia.

                <br /><br />


•	<b>IPD</b> : The IPD (In-Patient Department) for Obstetrics caters to women who need to be admitted to the hospital overnight or for an undetermined period of time for treatment, observation or surgery during the course of their pregnancy. We have top of the line equipment for intensive monitoring of our patients and also have efficient processes in place for emergency deliveries or caesarean section surgeries. 

                <br /><br />
<b>Services Provided : </b>
                                <br /><br />


-	Management of normal pregnancies
<br />-	Total preconception and pregnancy care
<br />-	Ultrasonography
<br />-	Antenatal care (before birth)
<br />-	Intrapartum care (during labour and delivery)
<br />-	Postnatal care (after birth)

 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>SPECIALISTS</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
									<li><a href="women-about-doctdetails-drjassawalla.aspx">Dr. M. J. Jassawalla</a></li>
									<li><a href="women-about-doctdetails-drmhatre.aspx">Dr. P. N. Mhatre</a></li>
									<li><a href="women-about-doctdetails-drnarvekar.aspx">Dr. N. M. Narvekar</a></li>
									<li><a href="women-about-doctdetails-drdamania.aspx">Dr. K. R. Damania</a></li>
									<li><a href="women-about-doctdetails-drsatoskar.aspx">Dr. P. R. Satoskar </a>

                                        <li><a href="women-about-doctdetails-drbalsarkar.aspx">Dr. G. D. Balsarkar </a>
                                        <li><a href="women-about-doctdetails-drbhalerao.aspx">Dr. S. A. Bhalerao </a>
                                        <li><a href="women-about-doctdetails-drmadhuri.aspx">Dr. Madhuri Patel </a>
                                        <li><a href="women-about-doctdetails-drsujata.aspx">Dr. Sujata Dalvi </a>
                                        <li><a href="women-about-doctdetails-drtank.aspx">Dr. P. D. Tank </a>
                                        <li><a href="women-about-doctdetails-drswati.aspx">Dr. Swati Alhabadia </a>
                                        <li><a href="women-about-doctdetails-drtrupti.aspx">Dr. Trupti K.Nadkarni </a>

                                              <li><a href="women-about-doctdetails-dramol.aspx">Dr. Amol Pawar </a>
                                              <li><a href="women-about-doctdetails-drpayal.aspx">Dr. Payal D. Lakhani </a>
                                              <li><a href="women-about-doctdetails-drvandana.aspx">Dr. Vandana Bansal</a>
                                              <li><a href="women-about-doctdetails-drrachana.aspx">Dr. Rachana Dalmia </a>
                                              <li><a href="women-about-doctdetails-drpooja.aspx">Dr. Pooja K. Bandekar </a>
                                       
									</li>
									
								</ul>
							</div>
						</div>
					</div>
				




				</div>
			</div>







		</div>
</asp:Content>
