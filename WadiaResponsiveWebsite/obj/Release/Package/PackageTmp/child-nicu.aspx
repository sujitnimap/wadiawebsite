﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-nicu.aspx.cs" Inherits="WadiaResponsiveWebsite.child_nicu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2> 	Neonatal Intensive Care Unit (NICU)  </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
            The hospital is equipped with a Level III Neonatal Intensive Care Unit (NICU), which offers state of the art facilities as well as qualified paediatricians to look after newborn babies round the clock. Annually, the hospital offers Neonatal Intensive Care to more than 7000 critically ill newborns. The NICU is equipped with ventilators, multipara monitors, pulse oximeters, apnoea monitors, Phototherapy units, Infusion pumps, syringe drivers and portable X ray and Echo facilities. Very low birth weight premature newborns are cared for in this unit which attracts newborns from all over the city and beyond. All types of medical and surgical diseases and defects can be treated in this unit. 


 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
              <b>The NICU offers facilities such as:</b>
                <br /><br />
                <div style="padding-left:10px">
-	Breathing and other respiratory disorders
<br />-	Congenital malformations
<br />-	Gastrointestinal disorders
<br />-	Heart diseases
<br />-	Hematologic disorders
<br />-	Infections
<br />-	Inherited metabolic disorders
<br />-	Medical Genetics
<br />-	Neurologic problems
<br />-	Pulmonology
</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

   </div>




</asp:Content>
