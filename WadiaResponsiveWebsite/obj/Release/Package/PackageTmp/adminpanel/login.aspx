﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!-- Basics -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Login</title>

    <!-- CSS -->
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/reset.css" rel="stylesheet" />
    <link href="css/styles.css" rel="stylesheet" />
</head>

<!-- Main HTML -->

<body style="background-image:url(images/bg.png)">

    <!-- Begin Page Content -->
    <div style="height: 20px"></div>
    <div align="center">
        <asp:Label ID="lblerror" runat="server" Visible="False" Font-Bold="True" ForeColor="Red" Font-Size="Small"></asp:Label>
        <asp:Label ID="lblinfo" runat="server" Visible="False" Font-Bold="True" ForeColor="Black" Font-Size="Small"></asp:Label>
    </div>

    <div id="container">
        <form id="Form1" runat="server">

        <div align="center">
            <asp:Literal ID="ltlhead" runat="server" Text="Wadia Admin Panel"></asp:Literal>
            <br />
            <br />
            <asp:RadioButton ID="rbtnchild" Checked="true" GroupName="login" Text="Child" runat="server" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:RadioButton ID="rbtnwoman" Text="Woman" GroupName="login" runat="server" />
        </div>

            <label for="name">Username:</label>

            <asp:TextBox ID="txtname" CssClass="name" runat="server" ></asp:TextBox>
           
            <label for="username">Password:</label>

            <p>
                <%--<a href="#">Forgot your password?</a>--%>

                <asp:TextBox ID="txtpassword" type="password" runat="server" TextMode="Password"></asp:TextBox>
               
                <div id="lower">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                </div>
        </form>

    </div>


    <!-- End Page Content -->

</body>

</html>