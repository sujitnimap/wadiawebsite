﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminpanel/admin.Master" AutoEventWireup="true" CodeBehind="subscribe-users.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel.subscribedusers" %>



<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register TagName="Error" Src="~/usercontrols/Error.ascx" TagPrefix="userControlError" %>
<%@ Register TagName="Info" Src="~/usercontrols/Info.ascx" TagPrefix="userControlInfo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>

        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large; padding-top:0px;">Subscribed User Master
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <userControlInfo:info ID="info" runat="server" Visible="false" />
                    <userControlError:error ID="error" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>


                            <div style="height: 10px; width: 100%"></div>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdvwcorporate" runat="server" Width="50%" HorizontalAlign="Center"
                                    PageSize="5" AutoGenerateColumns="False" AllowPaging="True"
                                    DataKeyNames="id" CssClass="table table-hover table-striped" OnPageIndexChanging="grdvwcorporate_PageIndexChanging">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr No.">
                                            <ItemStyle Width="15%" />
                                            <ItemTemplate>
                                                <center> <%# Container.DataItemIndex + 1 %></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email-Id">
                                            <ItemTemplate>
                                                <asp:Label ID="lblemailid" runat="server" Text='<%#Bind("emailid") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <%--<PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <PagerStyle BackColor="#7779AF" Font-Bold="true" ForeColor="White" />--%>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
