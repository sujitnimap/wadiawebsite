﻿<%@ Page Title="Awards & Achivements" Language="C#" MasterPageFile="~/adminpanel/admin.Master" AutoEventWireup="true" CodeBehind="awardsnachivements.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel.awardsnachivements" %>



<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register TagName="Error" Src="~/usercontrols/Error.ascx" TagPrefix="userControlError" %>
<%@ Register TagName="Info" Src="~/usercontrols/Info.ascx" TagPrefix="userControlInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>

        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large;">Awards-Achievements Master
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <userControlInfo:Info ID="info" runat="server" Visible="false" />
                    <userControlError:Error ID="error" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>
                            <div style="padding-left: 20px">
                                <asp:Button ID="btnAdd" runat="server" Text="Add New Award-Achievement" CssClass="btn btn-info" OnClick="btnAdd_Click" />
                            </div>

                            <div style="height: 10px; width: 100%"></div>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdvwaward" runat="server" Width="100%" HorizontalAlign="Center"
                                    OnRowCommand="grdvwaward_RowCommand" PageSize="5" AutoGenerateColumns="False" AllowPaging="True"
                                    DataKeyNames="id" CssClass="table table-hover table-striped" OnPageIndexChanging="grdvwaward_PageIndexChanging">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr No.">
                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Doctor Name">
                                            <ItemStyle Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbldoctornm" runat="server" Text='<%#Bind("doctornm") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Category Name">
                                            <ItemStyle Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblcategorynm" runat="server" Text='<%#Bind("categorynm") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemStyle Width="40%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbltestimonialdescrp" runat="server" Text='<%#Bind("description") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Year">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblyear" runat="server" Text='<%#Bind("year") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Detail" HeaderText="Detailed View">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                        <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Edit" HeaderText="Edit Record">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                        <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Delete" HeaderText="Delete Record">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                    </Columns>
                                    <%--<PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <PagerStyle BackColor="#7779AF" Font-Bold="true" ForeColor="White" />--%>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    <!-- Detail Modal Starts here-->
    <div id="detailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Detailed View</h3>
        </div>
        <div class="modal-body">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:DetailsView ID="DetailsView1" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                        <Fields>
                            <asp:BoundField DataField="id" HeaderText="Id" />
                            <asp:TemplateField HeaderText="Doctor Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbldtldoctorname" runat="server" Text='<%#Bind("doctornm") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Category Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbldtlcategorynm" runat="server" Text='<%#Bind("categorynm") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                <ItemTemplate>
                                    <asp:Label ID="lbldtldescription" runat="server" Text='<%#Bind("description") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Year">
                                <ItemTemplate>
                                    <asp:Label ID="lbldtlyear" runat="server" Text='<%#Bind("year") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="testimonialdescrp" HeaderText="Description" />--%>
                        </Fields>
                    </asp:DetailsView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdvwaward" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <div class="modal-footer">
                <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
    <!-- Detail Modal Ends here -->
    <!-- Edit Modal Starts here -->
    <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="editModalLabel">Edit Record</h3>
        </div>
        <asp:UpdatePanel ID="upEdit" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <td>Id : </td>
                            <td colspan="2">
                                <asp:Label ID="lblid" runat="server"></asp:Label>
                            </td>
                        </tr>
                         <tr>
                            <td>Doctor Name:</td>
                            <td  colspan="2">
                                <asp:TextBox ID="txteditdoctornm"  runat="server"></asp:TextBox> 
                                <%--<asp:RequiredFieldValidator ID="reftxteditdoctornm" ValidationGroup="awardupdate" ControlToValidate="txteditdoctornm" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>Category:</td>
                            <td  colspan="2">
                                <asp:DropDownList ID="ddleditcategory" runat="server">
                                    <asp:ListItem>Doctor</asp:ListItem>
                                    <asp:ListItem>Management</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                         <tr>
                            <td>Year:</td>
                            <td  colspan="2">
                                <asp:TextBox ID="txtedityear"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtedityear" ValidationGroup="awardupdate" ControlToValidate="txtedityear" runat="server"  ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Description&nbsp;:</td>
                            <td>
                                <FTB:FreeTextBox ID="FreeTextBox1" runat="server">
                                </FTB:FreeTextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="refreetxtbox" ValidationGroup="awardupdate" ControlToValidate="FreeTextBox1" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <%--<asp:TextBox ID="txtdescription" runat="server"></asp:TextBox>--%>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <asp:Label ID="lblResult" Visible="false" runat="server"></asp:Label>
                    <asp:Button ID="btnSave" runat="server" Text="Update" ValidationGroup="awardupdate" CssClass="btn btn-info" OnClick="btnSave_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="grdvwaward" EventName="RowCommand" />
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!-- Edit Modal Ends here -->
    <!-- Add Record Modal Starts here-->
    <div id="addModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="addModalLabel">Add New Record</h3>
        </div>
        <asp:UpdatePanel ID="upAdd" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <td>Doctor Name:</td>
                            <td  colspan="2">
                                <asp:TextBox ID="txtadddoctornm" Width="180px" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="reftxtadddoctornm" ValidationGroup="awardadd" ControlToValidate="txtadddoctornm" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>Category:</td>
                            <td  colspan="2">
                                <asp:DropDownList ID="ddladdcategory" runat="server">
                                    <asp:ListItem>Doctor</asp:ListItem>
                                    <asp:ListItem>Management</asp:ListItem>
                                </asp:DropDownList>

                            </td>
                        </tr>
                         <tr>
                            <td>Year:</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtaddYear"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtaddYear" ValidationGroup="awardadd"  ForeColor="Red" ControlToValidate="txtaddYear" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Description&nbsp;:</td>
                            <td>
                                <FTB:FreeTextBox ID="txtdescrption" runat="server">
                                </FTB:FreeTextBox>
                            </td>
                            <td>

                                <asp:RequiredFieldValidator ID="reftxtdescrption" ValidationGroup="awardadd" ControlToValidate="txtdescrption" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnAddRecord" runat="server" Text="Add" ValidationGroup="awardadd" CssClass="btn btn-info" OnClick="btnAddRecord_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnAddRecord" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!--Add Record Modal Ends here-->
    <!-- Delete Record Modal Starts here-->
    <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="delModalLabel">Delete Record</h3>
        </div>
        <asp:UpdatePanel ID="upDel" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    Are you sure you want to delete the record?
                            <asp:HiddenField ID="hfCode" runat="server" />
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelete_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!--Delete Record Modal Ends here -->

</asp:Content>
