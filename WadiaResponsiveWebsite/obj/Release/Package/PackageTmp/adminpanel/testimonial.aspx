﻿<%@ Page Title="Testimonial" Language="C#" MasterPageFile="~/adminpanel/admin.Master" AutoEventWireup="true" CodeBehind="testimonial.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel.testimonialnew" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register TagName="Error" Src="~/usercontrols/Error.ascx" TagPrefix="userControlError" %>
<%@ Register TagName="Info" Src="~/usercontrols/Info.ascx" TagPrefix="userControlInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large;">Testimonial Master
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <userControlInfo:Info ID="info" runat="server" Visible="false" />
                    <userControlError:Error ID="error" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>
                            <div style="padding-left: 20px">
                                <asp:Button ID="btnAdd" runat="server" Text="Add New Testimonial" CssClass="btn btn-info" OnClick="btnAdd_Click" />
                            </div>

                            <div style="height: 10px; width: 100%"></div>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdvwtestimonial" runat="server" Width="100%" HorizontalAlign="Center"
                                    OnRowCommand="grdvwtestimonial_RowCommand" PageSize="5" AutoGenerateColumns="False" AllowPaging="True"
                                    DataKeyNames="id" CssClass="table table-hover table-striped" OnPageIndexChanging="grdvwtestimonial_PageIndexChanging">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                         <asp:TemplateField HeaderText="Sr No.">
                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblnm" runat="server" Text='<%#Bind("testimonialnm") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Image">
                                            <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Image ID="imgmain" Width="100px" Height="100px" ImageUrl='<%#(Eval("imageurl")) %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Category Name">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblcategorynm" runat="server" Text='<%#Bind("categorynm") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblcategorynm" runat="server" Text='<%#Bind("createdate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbldescription" runat="server" Text='<%#Bind("testimonialdescrp") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Createby">
                                            <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblcreateby" runat="server" Text='<%#Bind("createby") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Designation">
                                            <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbldesignation" runat="server" Text='<%#Bind("designation") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Detail" HeaderText="Detailed View">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                        <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Edit" HeaderText="Edit Record">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                        <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Delete" HeaderText="Delete Record">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                    </Columns>
                                    <%--<PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <PagerStyle BackColor="#7779AF" Font-Bold="true" ForeColor="White" />--%>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>

    <!-- Detail Modal Starts here-->
    <div id="detailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Detailed View</h3>
        </div>
        <div class="modal-body">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:DetailsView ID="DetailsView1" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                        <Fields>
                            <asp:BoundField DataField="id" HeaderText="Id" />
                            <asp:BoundField DataField="testimonialnm" HeaderText="Name" />
                            <asp:TemplateField HeaderText="Description">
                                <ItemTemplate>
                                    <asp:Label ID="lbldescription" runat="server" Text='<%#Bind("testimonialdescrp") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Image">
                                <ItemTemplate>
                                    <asp:Image ID="mimg" Width="200px" Height="150px" ImageUrl='<%#(Eval("imageurl")) %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdvwtestimonial" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <div class="modal-footer">
                <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
    <!-- Detail Modal Ends here -->
    <!-- Edit Modal Starts here -->
    <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="editModalLabel">Edit Record</h3>
        </div>
        <asp:UpdatePanel ID="upEdit" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <td>Id : </td>
                            <td colspan="2">
                                <asp:Label ID="lblid" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Name : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txttestimlnm" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxttestimlnm" ValidationGroup="testimnlupdate" ControlToValidate="txttestimlnm" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Category : </td>
                            <td colspan="2">

                                <asp:DropDownList ID="ddleditcategory" runat="server">
                                    <asp:ListItem>Patients</asp:ListItem>
                                    <asp:ListItem>Students</asp:ListItem>
                                    <asp:ListItem>Archive</asp:ListItem>
                                    <asp:ListItem>Contributors</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                         <tr>
                            <td>Date : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txteditdate" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txteditdate_CalendarExtender" Format="MM/dd/yyyy" runat="server" TargetControlID="txteditdate">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reftxteditdate" ValidationGroup="testimnlupdate" ControlToValidate="txteditdate" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Description&nbsp;: </td>
                            <td>
                                <FTB:FreeTextBox ID="FreeTextBox1" runat="server">
                                </FTB:FreeTextBox>
                                <%--<asp:TextBox ID="txtdescrption" runat="server"></asp:TextBox>--%>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="refFreeTextBox1" ValidationGroup="testimnlupdate" ControlToValidate="FreeTextBox1" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td>User Image&nbsp;: </td>
                            <td colspan="2">
                                <asp:AsyncFileUpload ID="flduploadupdateimg" runat="server" />
                                (100 X 100)                              
                            </td>
                        </tr>
                        <tr>
                            <td>Create by : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtcreate" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtcreate" ValidationGroup="testimnlupdate" ControlToValidate="txtcreate" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Designation&nbsp;: </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtdesignt" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtdesignt" ValidationGroup="testimnlupdate" ControlToValidate="txtdesignt" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <asp:Label ID="lblResult" Visible="false" runat="server"></asp:Label>
                    <asp:Button ID="btnSave" runat="server" Text="Update" CssClass="btn btn-info" OnClick="btnSave_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="grdvwtestimonial" EventName="RowCommand" />
                <asp:PostBackTrigger ControlID="btnSave" />
                <%--<asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!-- Edit Modal Ends here -->

    <!-- Add Record Modal Starts here-->
    <div id="addModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="addModalLabel">Add New Record</h3>
        </div>
        <asp:UpdatePanel ID="upAdd" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <td>Name : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txttstmnlnm" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxttstmnlnm" ValidationGroup="testimnladd" ControlToValidate="txttstmnlnm" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Category : </td>
                            <td colspan="2">

                                <asp:DropDownList ID="ddladdcategory" runat="server">
                                    <asp:ListItem>Archive</asp:ListItem>
                                    <asp:ListItem>Contributors</asp:ListItem>
                                    <asp:ListItem>Patients</asp:ListItem>
                                    <asp:ListItem>Students</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>Date : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtadddate" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txtadddate_CalendarExtender" Format="MM/dd/yyyy" runat="server" TargetControlID="txtadddate">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reftxtadddate" ValidationGroup="testimnladd" ControlToValidate="txtadddate" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Description&nbsp;: </td>
                            <td>
                                <FTB:FreeTextBox ID="txtdescrption" runat="server">
                                </FTB:FreeTextBox>
                                <%--<asp:TextBox ID="txtdescrption" runat="server"></asp:TextBox>--%>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="reftxtdescrption" ValidationGroup="testimnladd" ControlToValidate="txtdescrption" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td>User Image : </td>
                            <td colspan="2">
                                <asp:AsyncFileUpload ID="flduploadaddimg" runat="server" />
                                (100 X 100)
                                <%--<asp:RequiredFieldValidator ID="refflduploadaddimg" ValidationGroup="testimnladd" ControlToValidate="flduploadaddimg" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>Create by : </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtcreateby" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtcreateby" ValidationGroup="testimnladd" ControlToValidate="txtcreateby" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Designation&nbsp;: </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtdesignation" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtdesignation" ValidationGroup="testimnladd" ControlToValidate="txtdesignation" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnAddRecord" runat="server" Text="Add" ValidationGroup="testimnladd" CssClass="btn btn-info" OnClick="btnAddRecord_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAddRecord" />
                <%--<asp:AsyncPostBackTrigger ControlID="btnAddRecord" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!--Add Record Modal Ends here-->
    <!-- Delete Record Modal Starts here-->
    <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="delModalLabel">Delete Record</h3>
        </div>
        <asp:UpdatePanel ID="upDel" runat="server">
            <ContentTemplate>
                <div class="modal-body">
                    Are you sure you want to delete the record?
                            <asp:HiddenField ID="hfCode" runat="server" />
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelete_Click" />
                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <!--Delete Record Modal Ends here -->

</asp:Content>
