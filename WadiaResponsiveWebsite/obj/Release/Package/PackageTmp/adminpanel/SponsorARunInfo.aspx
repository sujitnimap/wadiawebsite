﻿<%@ Page Title="SponsorARun Info" EnableEventValidation = "false" Language="C#" MasterPageFile="~/adminpanel/admin.Master" AutoEventWireup="true" CodeBehind="SponsorARunInfo.aspx.cs" Inherits="WadiaResponsiveWebsite.adminpanel.SponsorARunInfo" %>

<%@ Register TagName="Error" Src="~/usercontrols/Error.ascx" TagPrefix="userControlError" %>
<%@ Register TagName="Info" Src="~/usercontrols/Info.ascx" TagPrefix="userControlInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div style="height: 20px; width: 100%"></div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table width="100%" cellspacing="8" cellpadding="0" border="0">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: large;">SponsorARun Info
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <userControlInfo:Info ID="info" runat="server" Visible="false" />
                    <userControlError:Error ID="error" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" Height="30px" OnClick="btnExport_Click" />

                    <asp:UpdatePanel ID="upCrudGrid" runat="server">
                        <ContentTemplate>

                            <div style="height: 10px; width: 100%"></div>
                            <div style="padding: 10px 20px 0px 20px">
                                <asp:GridView ID="grdvwSponsorARunInfo" runat="server" Width="100%" HorizontalAlign="Center"
                                    OnRowCommand="grdvwSponsorARunInfo_RowCommand" PageSize="15" AutoGenerateColumns="False" AllowPaging="True"
                                    DataKeyNames="SponsorARunForMarathonID" CssClass="table table-hover table-striped" OnPageIndexChanging="grdvwSponsorARunInfo_PageIndexChanging">
                                    <PagerStyle CssClass="cssPager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr No.">
                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name Of Oraganisation">
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblNameOfOraganisation" runat="server" Text='<%#Bind("NameOfOrganisation") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="First Name">
                                            <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstName" runat="server" Text='<%#Bind("FirstName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastName" runat="server" Text='<%#Bind("LastName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="server" Text='<%#Bind("City") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email">
                                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmail" runat="server" Text='<%#Bind("EmailAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MobileNumber">
                                            <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblMobileNumber" runat="server" Text='<%#Bind("MobileNumber1") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%#Bind("AmountInINR") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                            ButtonType="Button" Text="Detail" HeaderText="Detailed View">
                                            <ControlStyle CssClass="btn btn-info"></ControlStyle>
                                        </asp:ButtonField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>

        <%--View start--%>
        <div id="detailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <h3 id="myModalLabel">Detailed View</h3>
            </div>
            <div class="modal-body">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:DetailsView ID="dlvSponsorARunInfo" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                            <Fields>
                                <asp:BoundField DataField="SponsorARunForMarathonID" HeaderText="Id" />
                                <asp:TemplateField HeaderText="Name Of Oraganisation">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNameOfOraganisation" runat="server" Text='<%#Bind("NameOfOrganisation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="EmailAddress" HeaderText="Email" />
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFirstName" runat="server" Text='<%#Bind("FirstName") %>'></asp:Label>
                                        &nbsp;&nbsp;<asp:Label ID="lblLastName" runat="server" Text='<%#Bind("LastName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Country" HeaderText="Country" />
                                <asp:BoundField DataField="MobileNumber1" HeaderText="MobileNumber" />
                                <asp:BoundField DataField="AmountInINR" HeaderText="Amount" />
                            </Fields>
                        </asp:DetailsView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="grdvwSponsorARunInfo" EventName="RowCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

        </div>
    </div>
</asp:Content>
