﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-press-media2.aspx.cs" Inherits="WadiaResponsiveWebsite.child_press_media2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PRESS & MEDIA</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">

<div class="devider_10px"></div>
This page is a collection of news, updates and articles related to the activities at Wadia Hospitals.
<div class="devider_20px"></div>

    <div>


<div id="container">
	

	<div class="pagination"> Page : &nbsp;&nbsp;
		 <a href="child-press-media.aspx" class="page gradient">1</a>
        <a href="child-press-media1.aspx" class="page gradient">2</a>
        <a href="child-press-media2.aspx" class="page active">3</a>
        <a href="child-press-media3.aspx" class="page gradient">4</a>
	</div>

	

	
</div>







    </div>

<b>News Archives</b>
<div class="devider_20px"></div>

 <div>
<div class="newsheading"><a target="_blank" href="http://daily.bhaskar.com/article/SPO-OFF-master-blaster-sachin-tendulkar-donates-rs-10-lakh-to-ai-jerbai-wadia-hospital-f-4499377-NOR.html">SACH A BIG HEART: Sachin Tendulkar donates R 10 lakh to Mumbai hospital</a></div>
<div class="newsdate">22.01.2014 - Daily Bhaskar</div>
    </div> 

    <div class="devider_20px"></div>

    <div>
<div class="newsheading"><a target="_blank" href="http://www.dnaindia.com/mumbai/report-sachin-tendulkar-donates-rs-10-lakh-for-raising-cancer-awareness-1955257">Sachin Tendulkar donates Rs 10 lakh for raising cancer awareness</a></div>
<div class="newsdate">22.01.2014 - DNA India</div>
    </div>

    <div class="devider_20px"></div>

    <div>
<div class="newsheading"><a target="_blank" href="http://jaipur.patrika.com/experience/health-experience/tendulkar-donated-rs-10-lakh-to-bai-jerbai-wadia-hospital/">Tendulkar donated Rs 10 lakh to Bai Jerbai Wadia Hospital</a></div>
<div class="newsdate">22.01.2014 - Jaipur Patrika</div>
    </div>

    <div class="devider_20px"></div>

    <div>
<div class="newsheading"> <a target="_blank" href="http://epaper.timesofindia.com/Repository/ml.asp?Ref=VE9JTS8yMDE0LzAxLzIyI0FyMDA0MDc=&Mode=Gif&Locale=english-skin-custom">Twins will need more operation, says docs</a></div>
<div class="newsdate">22.01.2014 - Times of India</div>
    </div>

    <div class="devider_20px"></div>

    <div>
<div class="newsheading"><a href="http://www.deccanchronicle.com/140121/sports-cricket/article/tendulkar-donates-rs-10-lakh-bai-jerbai-wadia-hospital" target="_blank">Tendulkar donates Rs 10 lakh to Bai Jerbai Wadia Hospital</a></div>
<div class="newsdate">21.01.2014 - Deccan Chronicle</div>
    </div>


    <div class="devider_20px"></div>
    
    <div>
<div class="newsheading"><a target="_blank" href="http://www.business-standard.com/article/pti-stories/tendulkar-donates-rs-10-lakh-to-bai-jerbai-wadia-hospital-114012101002_1.html">Tendulkar donates Rs 10 lakh to Bai Jerbai Wadia Hospital</a></div>
<div class="newsdate">21.01.2014 - Business Standard</div>
    </div>


    <div class="devider_20px"></div>

    <div>
<div class="newsheading"><a target="_blank" href="http://www.cricketcountry.com/news/sachin-tendulkar-donates-rs-10-lakh-to-hospital-for-raising-cancer-awareness-87802">Sachin Tendulkar donates Rs 10 lakh to hospital for raising cancer awareness</a></div>
<div class="newsdate">21.01.2014 - The Cricket Country</div>
    </div>


    <div class="devider_20px"></div>

    <div>
<div class="newsheading"><a target="_blank" href="http://www.financialexpress.com/news/sachin-tendulkar-donates-rs-10-lakh-to-bai-jerbai-wadia-hospital/1219660">Sachin Tendulkar donates Rs 10 lakh to Bai Jerbai Wadia Hospital</a></div>
<div class="newsdate">21.01.2014 - Financial Express</div>
    </div>


    <div class="devider_20px"></div>

     <div>
<div class="newsheading"><a target="_blank" href="http://www.rediff.com/cricket/report/sachin-tendulkar-donates-10-lakh-to-children-hospital-for-ventilator-mumbai/20140121.htm">Tendulkar donates 10 lakh to children's hospital for ventilator</a></div>
<div class="newsdate">21.01.2014 - Rediff.com</div>
    </div>


    <div class="devider_20px"></div>

   <div>
<div class="newsheading"><a target="_blank" href="http://zeenews.india.com/sports/cricket/sachin-tendulkar-donates-rs-10-lakh-to-bai-jerbai-wadia-hospital_778835.html">Sachin Tendulkar donates Rs 10 lakh to Bai Jerbai Wadia Hospital</a></div>
<div class="newsdate">21.01.2014 - Zee News</div>
    </div>




<div class="devider_20px"></div>


<div id="container">
	
    <div class="pagination"> Page : &nbsp;&nbsp;
		 <a href="child-press-media.aspx" class="page gradient">1</a>
        <a href="child-press-media1.aspx" class="page gradient">2</a>
        <a href="child-press-media2.aspx" class="page active">3</a>
        <a href="child-press-media3.aspx" class="page gradient">4</a>
	</div>

	
</div>




<b>Media Enquiries</b>

<div class="devider_10px"></div>


We welcome the opportunity to assist the media with health information requests, schedule interviews with our medical team and offer expert opinions. 
<div class="devider_10px"></div>


For more information, please get in touch with our team at 
    <div class="devider_10px"></div>

    
<a href="#">media@wadiahospitals.org</a>


<div class="devider_20px"></div>
    
    
            
</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        



		</div>




</asp:Content>
