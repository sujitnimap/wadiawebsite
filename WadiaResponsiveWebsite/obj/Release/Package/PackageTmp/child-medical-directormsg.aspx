﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-medical-directormsg.aspx.cs" Inherits="WadiaResponsiveWebsite.child_medical_directormsg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                               <div class="heading">
                  <h2>Medical Director’s Message</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">


     <div class="post-container"><img class="avatar" src="images/dr_y_ambdekar-2.jpg"><span class="post">
Despite India’s fast paced development in the field of medicine, problems of access, quality and affordability of healthcare across sections of society are still very real.  Statistics show that health complications are responsible for over 90% of all infant deaths. In this scenario it is our primary responsibility as paediatric specialists, to cater to the medical needs of every child regardless of the background they come from. The first of its kind, The Bai Jerbai Wadia Hospital for Children is known to be one of the country’s best paediatric hospitals, specializing in Paediatric as well as Neonatal care. We take it upon ourselves to not only provide affordable world class medical services to children across sections of society, but also to provide our society with highly skilled and reliable doctors, trained by us, who can carry forward our mission and philosophy, and provide the best possible treatment to the little ones who need it the most. 
<br /><br />
    In this effort, we run several full time as well as part-time training courses for aspiring medical professionals. Our courses cater to students for both medical and nursing fields. Our faculty comprises of highly experienced, specialized and renowned doctors who are constantly working towards updating their knowledge and skill sets to impart the highest quality of education by international standards.
<br /><br />
    Having developed our super-specialities at the hospital for the last 25-30 years our vision is to now expand further into the treatment of chronic diseases for children that affect the liver, kidney etc. At this point, we can only diagnose and give supportive treatment to these conditions, not cure them permanently. Therefore, we are looking to expand in the field of Paediatric organ transplantation and improve our knowledge base.

    <br /><br />
<b>Dr. Y. K. Amdekar</b>
<br />
Medical Director
<br />
Bai Jerbai Wadia Hospital for Children
</p> </span>

       </div>

</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>MEDICAL</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						 <li><a href="child-medical-overview.aspx">	Overview  </a></li>
                         <li><a href="child-medical-directormsg.aspx"> Directors message </a></li>
                         <li><a href="child-medical-courses.aspx"> 	Courses available</a></li>
                         <li><a href="child-contact-us.aspx">	Contact Us  </a></li>


                                     <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>



		</div>













</asp:Content>
