﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-gallery-category.aspx.cs" Inherits="WadiaResponsiveWebsite.child_gallary_category" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>GALLERY</h2>
                </div>
                <br />
                <asp:DataList ID="dlistchildcategory" Width="100%" runat="server" RepeatColumns="3">
                    <ItemTemplate> 
                        <br />                       
                        <div align="center">
                            <table>
                                <tr>
                                    <td>
                                        <div align="center">
                                            <asp:ImageButton ID="imgbtnthumnail" CommandArgument='<%#Eval("id") %>' Width="200px" ImageUrl='<%#("/adminpanel/" + Eval("thumbnail")) %>' OnClick="imgbtnthumnail_Click" Height="180px" runat="server" />

                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="center">
                                            <asp:Label ID="lblcat" runat="server" Font-Bold="true" Text='<%#Eval("categorynm") %>'></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ItemTemplate>
                </asp:DataList>



            </div>
        </div>
    </div>
</asp:Content>
