﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-doctdetails-drabhay.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drabhay" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
		<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dr.jpg" />
<div class="docdetails">
Dr. Abhay Nene
<div class="docdetailsdesg">
Hon. Spine Surgeon

    <br />
MS (Orth.)



</div>

</div>
</div>

                    <div class="areaofinterest">

                        Areas of Interest : Spinal deformity, spinal infections, Spinal Tumours


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
 •	ASSI-Dartmouth Spine Fellowship, September 2006, Dartmouth-Hitchcock Medical Center, New Hampshire, USA
<br /><br />
•	Scoliosis Research Society Scholarship, September 2004, Buenos Aires, Argentina
<br /><br />
•	International ISSLS Spine Fellow award, International Society for the Study of Lumbar Spine (ISSLS) May 2002 , Cleveland, Ohio, USA




<br /><br />
                    <b>International conferences participated in as a scientific presenter :                     </b>
<br /><br />

•	Asia-Pacific Osteoporosis Foundation certification course, Singapore, March 2007

    <br /><br />•	Scoliosis Research Society 41st Annual Meet, Monterey, California, September 2006

    <br /><br />•	AO Advanced Spine Course, Davos, December 2005

    <br /><br />•	Scoliosis Research Society 39th Annual Meet, Buenos Aires, Argentina, September 2004

    <br /><br />•	European Spine week, Porto, Portugal, June 2004

    <br /><br />•	APOA, October 2002, Singapore

    <br /><br />•	ISSLS, Cleveland, Ohio, May 2002

    <br /><br />•	Euro Traveling Spine Fellows Meet, May 2001, Nottingham, UK

    <br /><br />•	Organizing secretary for WIROC, the annual Western India Orthopedic Association meeting, December 2011, Bombay

    <br /><br />•	Organizing secretary for the Silver Jubilee Assicon meeting in Bombay 2011. This 400 delegate international meeting of the association of spine surgeons of India (ASSI) commemorated the 25 years of the existence on the ASSI

    <br /><br />•	Organizing secretary of the mid annual meeting of the ASSI (held as a joint meeting with the Spine Society of Europe) in the years 2005 and 2007 

    <br /><br />•	We organize 2 to 4 Spine Update CMEs / Surgical demos every year at our Institute

    <br /><br />•	Organizing the Spine Foundation Masterclass, the annual lecture series in spine surgery for post graduate students, since 2004

    <br /><br />•	Over 50 podium presentations and a dozen poster presentations at national and international spine meetings.

    <br /><br />•	Six to 8 Academic Spine conferences / workshops attended every year since 2001, as faculty, instructor or delegate. 


    <br /><br />









                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	Member, AO Spine India research Council (Research)
<br /><br />•	Member, Scientific committee, Association of Spine Surgeons of India
<br /><br />•	Secretary, Bombay Spine Club


    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Posterior approach in thoracolumbar tuberculosis : a clinical and radiological review of 67 operated cases. S Rawall, K Mohan, A Nene. Journal of Musculoskeletal Surgery, Springer. Dec 2012 
<br /><br />
                    •	Surgical Patterns in Osteoporotic Vertebral Compression Fractures
Patil, S., Pawar, U., Nene A
European Spine Journal, Sept 2012
<br /><br />
                    •	Postvertebroplasty instability Report of 5 cases .                    
                    <br />
                                             Premik Nagad, Saurabh Rawall, Vishal Kundnani, Kapil Mohan, Sanganagouda Patil, Abhay Nene. J Neurosurg Spine 16:387–393, April 2012 

<br /><br />•	Drug Resistance Patterns in 111 cases of drug resistant Tuberculosis-Spine. 
Kapil Mohan, Saurabh Rawall, Uday Pawar, Meeta Sadani, Premik Nagad, Amita Nene, Abhay Nene
European Spine Journal : January 2012

<br /><br />•	Post operative visual loss.
Indian Journal Of Orthopedics, Volume 46, January 2012
SS Patil, Kapil Mohan, U Pawar, A Nene

<br /><br />•	Role of ‘low cost Indian implants’ in our practice: our experience with      
1,572 pedicle screws
Saurabh Rawall, Kapil Mohan, Premik Nagad, Ashutosh Sabnis , Uday Pawar, Abhay Nene
European Spine J DOI 10.1007/s00586-011-1914-3, July 2011

<br /><br />•	Tuberculosis of the spine: 2011 update. Nene, U. Pawar
Argospine. Springer-Verlag France 2011 - DOI 10.1007/s12240-011-0021-y - ArgoSpine NEWS&JOURNAL - quarterly September 2011 - Vol. 23 - N°3

<br /><br />•	Outcome of single level instrumented posterior lumbar interbody fusion using corticocancellous laminectomy bone chips
Sanganagouda Patil, Saurabh Rawall, Premik Nagad, Bhavin Shial, Uday Pawar, Abhay Nene
Indian Journal of Orthopaedics | November 2011 | Vol. 45 | Issue 6

<br /><br />•	Multidrug-Resistant Tuberculosis of the Spine—Is it the Beginning of the End? A Study of Twenty-Five Culture Proven Multidrug-Resistant Tuberculosis Spine Patients 
•	Uday M. Pawar, D’ortho, DNB Ortho,* Vishal Kundnani, MS Ortho,* Vikas Agashe, MS Ortho,* Amita Nene, MD, DETRD, FCCP,† and Abhay Nene, MS Ortho* 
SPINE Volume 34, Number 22, pp E806 –E810, ©2009, Lippincott Williams & Wilkins 

<br /><br />•	Occipito-Atlanto-Axial Osteoarthritis 
A Cross Sectional Clinico-Radiological Prevalence Study in High Risk and General Population 
<br /><br />•	Siddharth A. Badve, MS,*† Shekhar Bhojraj, MS,† Abhay Nene, MS,‡ Abhijit Raut, MD,§ and Ravi Ramakanthan, MD§ 
SPINE Volume 35, Number 4, pp 434 – 438, ©2010, Lippincott Williams & Wilkins 


<br /><br />•	Major Vessel Injury With Cage Migration 
Surgical Complication in a Case of Spondylodiscitis 
Uday M. Pawar, D’Ortho, DNB Ortho, Vishal Kundnani, MS Ortho, and Abhay Nene, MS Ortho 
SPINE Volume 35, Number 14, pp E663–E666, ©2010, Lippincott Williams & Wilkins 

<br /><br />•	Results of Non Surgical Management of Thoracic Spinal Tuberculosis in Adults- a study of 50 cases 
A Nene, S Y Bhojraj, The Spine Journal, Jan-Feb 2005

<br /><br />•	Lumbosacral Tuberculosis in Adults
S Bhojraj, A Nene. British Journal of Bone and Joint Surgery, May 2002
<br /><br />
                    <b>Other Publications 
</b>
<br /><br />
•	ASSI textbook of Spinal Infections and Trauma<br />
i) Radiology in spinal trauma – A Sabnis, A Nene
<br />ii) Bacteriology of Spinal Tuberculosis – P Nagad, A Nene
<br />iii) Multi drug resistant Spinal TB- U Pawar, A Nene

<br /><br />•	Spinal  loop  rectangle  and  sub  laminar  wiring  as  a  
technique for scoliosis correction 
Shekhar Y Bhojraj, Raghuprasad G Varma, Abhay M Nene, Sheetal Mohite 
IJO - January - March 2010 / Volume 44 / Issue 1
<br /><br />
•	GIANT CELL TUMOR: Giant cell tumor of the spine: A review of 9 surgical interventions in 6 cases
Shekhar Y Bhojraj, Abhay Nene, Sheetal Mohite, Raghuprasad Varma
Indian Journal of Orthopaedics, Year 2007, Volume 41, Issue 2

<br /><br />
•	Spinal tumors and spinal instability
S Bhojraj, A Nene, S Mohite
Indian Journal of Orthopaedics, Year 2003, Volume 37, Issue 1
<br /><br />
•	Spinal instability in ankylosing spondylitis
Siddharth A Badve, Shekhar Y Bhojraj, Abhay M Nene, Raghuprasad Varma, Sheetal Mohite, Sameer Kalkotwar, Ankur Gupta
Indian J Orthop. 2010 Jul-Sep; 44(3): 270–276. doi: 10.4103/0019-5413.65151
<br /><br />
•	Textbook of Orthopedics and Traumatology. 2008, Jaypee
142.1 Metastatic Disease of the Spine
142.2 Primary Tumors of the Spine
S Bhojraj, A Nene

                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
         </div>





</asp:Content>
