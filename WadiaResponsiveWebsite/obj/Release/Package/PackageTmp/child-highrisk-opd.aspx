﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-highrisk-opd.aspx.cs" Inherits="WadiaResponsiveWebsite.child_highrisk_opd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>HIGH RISK OPD</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 
    <strong>Overview </strong>
    <br />    <br />


The High Risk OPD is run by the Neonatology Department. The department caters to low birth weight and very sick newborn babies after they are discharged 
    from our NICU. These infants are referred to as graduates from NICU and need close periodic follow-up to ensure normal growth and development, as they 
    are at high risk of developing complications in initial years of life. A neonatologist, developmental paediatrician and physiotherapist offer follow up 
    treatments to infants till two years of age. At the clinic, experienced doctors assess the neurodevelopmental outcome of all infants discharged
     from the NICU. 

    <div style="height:160px"></div>

</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

        





</asp:Content>
