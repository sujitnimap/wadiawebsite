﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-press-media1.aspx.cs" Inherits="WadiaResponsiveWebsite.child_press_media1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PRESS & MEDIA</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">

<div class="devider_10px"></div>
This page is a collection of news, updates and articles related to the activities at Wadia Hospitals.
<div class="devider_20px"></div>

    <div>


<div id="container">
	

	<div class="pagination"> Page : &nbsp;&nbsp;
		<a href="child-press-media.aspx" class="page gradient">1</a>
        <a href="child-press-media1.aspx" class="page active">2</a>
        <a href="child-press-media2.aspx" class="page gradient">3</a>
        <a href="child-press-media3.aspx" class="page gradient">4</a>
	</div>

	

	
</div>







    </div>

<b>News Archives</b>
<div class="devider_20px"></div>

    <div>
<div class="newsheading"><a target="_blank" href="http://photogallery.navbharattimes.indiatimes.com/events/mumbai/salman-flags-off-little-hearts-marathon/articleshow/30106615.cms">Salman Flags Off Little Hearts Marathon</a></div>
<div class="newsdate">10.02.2014 - NavBharat Times</div>
    </div> 

    <div class="devider_20px"></div>

    <div>
<div class="newsheading"><a target="_blank" href="http://ishare.rediff.com/video/entertainment/salman-khan-attends-little-hearts-marathon/9331193">Salman Khan Attends Little Hearts Marathon</a></div>
<div class="newsdate">10.02.2014 - Rediff.com</div>
    </div>

    <div class="devider_20px"></div>

    <div>
<div class="newsheading"><a target="_blank" href="http://www.indiainfoline.com/Markets/News/First-edition-of-Little-Hearts-Marathon-concludes-in-Mumbai/5864441275">First edition of Little Hearts Marathon concludes in Mumbai</a></div>
<div class="newsdate">10.02.2014 - India Infoline</div>
    </div>

    <div class="devider_20px"></div>

    <div>
<div class="newsheading"> <a target="_blank" href="http://www.bollywoodhungama.com/more/photos/view/stills/parties-and-events/id/2398871">Salman Khan flags off the Little Hearts Marathon</a></div>
<div class="newsdate">10.02.2014 - Bollywood Hungama</div>
    </div>

    <div class="devider_20px"></div>

    <div>
<div class="newsheading"><a href="http://www.fashioncirqle.com/2014/02/09/little-hearts-marathon-flagged-salman-khan/" target="_blank">Little Hearts Marathon Flagged By Salman Khan</a></div>
<div class="newsdate">09.02.2014 - Fashion Cirqle</div>
    </div>


    <div class="devider_20px"></div>
    
    <div>
<div class="newsheading"><a target="_blank" href="http://www.filmicafe.co/event_photos/Glitz_&_Glamour/Bollywood/3936_First_Edition_of_Little_Hearts_Marathon_flagged_by_Salman_Khan">Little Hearts Marathon Flagged By Salman Khan</a></div>
<div class="newsdate">09.02.2014 - Filmi Café</div>
    </div>


    <div class="devider_20px"></div>

    <div>
<div class="newsheading"><a target="_blank" href="http://www.firstpost.com/mumbai/photos-salman-khan-flags-off-little-hearts-marathon-in-mumbai-1381499.html?photoid=1">Salman Flags Off Little Hearts Marathon in Mumbai</a></div>
<div class="newsdate">09.02.2014 - First Post</div>
    </div>


    <div class="devider_20px"></div>

    <div>
<div class="newsheading"><a target="_blank" href="http://epaper.timesofindia.com/Repository/ml.asp?Ref=VE9JTS8yMDE0LzAxLzI5I0FyMDAyMTE=&Mode=Gif&Locale=english-skin-custom">Twins Moved out of ICU</a></div>
<div class="newsdate">29.01.2014 - Times of India</div>
    </div>


    <div class="devider_20px"></div>

     <div>
<div class="newsheading"><a target="_blank" href="http://www.outlookindia.com/article.aspx?206883">Siamese Wonder</a></div>
<div class="newsdate">25.01.2014 - Outlook India</div>
    </div>


    <div class="devider_20px"></div>

   <div>
<div class="newsheading"><a target="_blank" href="http://freepressjournal.in/sachin-donates-rs-10-lakh-to-wadia-hospital/">Sachin donates Rs 10 lakh to Wadia Hospital</a></div>
<div class="newsdate">22.01.2014 - Free Press Journal</div>
    </div>

    <div class="devider_20px"></div>


<div id="container">
	<%--<div class="pagination">
		<p>without gradient</p>
		<a href="#" class="page">first</a><a href="#" class=
		"page">2</a><a href="#" class="page">3</a><span
		class="page active">4</span><a href="#" class=
		"page">5</a><a href="#" class="page">6</a><a href="#"
		class="page">last</a>
	</div>--%>

	<div class="pagination"> Page : &nbsp;&nbsp;
		<a href="child-press-media.aspx" class="page gradient">1</a>
        <a href="child-press-media1.aspx" class="page active">2</a>
        <a href="child-press-media2.aspx" class="page gradient">3</a>
        <a href="child-press-media3.aspx" class="page gradient">4</a>
	</div>
	

	
</div>




<b>Media Enquiries</b>

<div class="devider_10px"></div>


We welcome the opportunity to assist the media with health information requests, schedule interviews with our medical team and offer expert opinions. 
<div class="devider_10px"></div>


For more information, please get in touch with our team at 
    <div class="devider_10px"></div>

    
<a href="#">media@wadiahospitals.org</a>


<div class="devider_20px"></div>
    
    
            
</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        



		</div>




</asp:Content>
