﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-donate-equipment.aspx.cs" Inherits="WadiaResponsiveWebsite.child_donate_equipment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Donate Equipment </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 

Please get in touch with our team if you have access to equipment that you would like to offer us to help us ensure universal healthcare to our patients across the board. Our team can also put you in touch with our authorised vendors and suppliers to ensure that the equipment purchased is suitable to the hospital's needs.  Alternately if you wish, you could donate money to help us procure new and improved medical equipment.
    <br /><br />
To contact us, please fill the form below and we will get in touch with you shortly.
<br /><br />

<asp:TextBox ID="txtname" CssClass="textboxcontact" required runat="server" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'"></asp:TextBox>
<div class="devider_10px"></div>

    <asp:TextBox ID="txtemail" CssClass="textboxcontact" required runat="server" placeholder="Enter your email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email address'"></asp:TextBox>
<div class="devider_10px"></div>
    <asp:TextBox ID="txtcontactno" CssClass="textboxcontact" required runat="server" placeholder="Phone Number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone Number'"></asp:TextBox>
<div class="devider_10px"></div>
    <asp:TextBox ID="txtmessage" CssClass="textboxcontactdes" required runat="server" placeholder="Message" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Message'"></asp:TextBox>
<div class="devider_10px"></div>

<div style="text-align:left"><asp:ImageButton ID="imgbtncontact" src="images/SubmitButton.png" runat="server" OnClick="imgbtncontact_Click"  /></div>









</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>CONTRIBUTE</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
									    <li><a href="child-donate-online.aspx">Donate Online</a></li>
									<li><a href="child-sponsor-patient.aspx">Sponsor a Patient</a></li>
									<li><a href="child-donate-equipment.aspx">Donate Equipment</a></li>
                                    <li><a href="child-donate-inkind.aspx">Donate in-kind </a></li>
									<li><a href="child-corporate-giving.aspx">Corporate Giving</a></li>
									<li><a href="child-testimonials.aspx?catnm=Contributors">Testimonials </a></li>
									
								</ul>
							</div>
						</div>
					</div>
					
				</div>
			</div>







		</div>





</asp:Content>
