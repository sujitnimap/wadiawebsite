/**
* 	anoFlow v1.0 (Optimized gallery for large image previews)
* 
*	@author: 	Angel Kostadinov
* 	@copyright: 2013, Anowave
*/
(function () 
{
    var anoFlow = function(element, options) 
    {
    	this.ui 	 = null;
		this.view 	 = null;
		this.images  = [];
		this.options = $.extend(
		{
			selector: '.lightbox',
			distance: 100,
			easing:  'linear',
			defaultSize:
			{
				width: 420,
				height:200
			},
			timeout: 50,
			blockUI:
			{
				speed: 		300,
				opacity: 	0.4,
				zIndex: 	9998,
				background: 'rgb(0,0,0)'
			},
			resize: 
			{
				speed: 400
			},
			image: 
			{
				fade: 300,
				draggable: false
			},
			onConstruct: function(){},
			onStart: function(index){},
			onEnd: function(index){}
		}, options);
		
		this.slide = 
		{
			next: 	 0,
			prev: 	 0,
			current: 0
		}
		
		this.element = $(element);
    };
 
    anoFlow.prototype = 
    {
    	getView: function()
    	{
    		if (!this.view)
			{	
				this.view = new View(this.options);
				
				var box = $('<div/>').addClass('flow');
				var top = $('<div/>').addClass('top');
				var mid = $('<div/>').addClass('mid');
				var bot = $('<div/>').addClass('bot');
				var tlc = $('<div/>').addClass('tlc'); 
				var trc = $('<div/>').addClass('trc'); 
				var blc = $('<div/>').addClass('blc'); 
				var brc = $('<div/>').addClass('brc');
				
				var wrapper = $('<div/>');
				var content = $('<div/>').css(
				{
					height: this.options.defaultSize.height
				})
				
				var controls = $('<div/>').addClass('controls').appendTo(content);
				var ribbon   = $('<div/>').addClass('ribbon').appendTo(box);
				
				var fn = 
				{
					next: delegate(this, this.next),
					prev: delegate(this, this.prev)
				}
				
				var prev = $('<a/>').addClass('ui-flow-prev').click(delegate(this, this.prev)), next = $('<a/>').addClass('ui-flow-next').click(delegate(this, this.next));
				
				$(document).unbind('keydown').on(
				{
					keydown: function(e)
					{
						switch(e.which)
						{
							case 37:
								fn.prev();
								break;
							case 39:
								fn.next();
								break;
						}
					}
				});
				
				$.each(
				[
					prev,
					next,
					$('<a/>').addClass('ui-flow-close').click(delegate(this, this.close))
				], function()
				{
					$(this).appendTo(content);
				})
				

				/* Append parts */
				$.each([top,mid,bot], function()
				{
					$(this).appendTo(box);
				})
			
				/* Top corner(s) */
				$.each([tlc,trc], function()
				{
					$(this).addClass('corner').appendTo(top);
				})
				
				/* Bottom corner(s) */
				$.each([brc,blc], function()
				{
					$(this).addClass('corner').appendTo(bot);
				})
				
				wrapper.append(content).appendTo(mid);
				
				/* Append box to body */
				box.css(
				{
					width:		 this.options.defaultSize.width,
					marginTop:	-this.options.defaultSize.height/2,
					marginLeft: -this.options.defaultSize.width/2
				}).appendTo('body');
				
				/* Set element */
				this.view.element = box;			
			}
			
			return this.view;
    	},
    	play: function(index)
		{
			
			this.options.onStart.apply(this,[index]);
			
			this.getView().blockUI().open().preload(this.images[index], delegate(this, function(ui)
			{
				this.open(ui, index);	
			}));
		},
		open: function(ui, index)
		{
			this.ui = ui;
			
			this.equal(index);
		},
		close: function()
		{
			this.getView().empty().close().unblockUI();
		},
		equal: function(index)
		{
			var image = this.ui.images[0], caption = this.images[index].image.data('caption');
				
			caption = caption || null;

			this.slide =
			{
				next:  index + 1 > this.images.length - 1  ? false : index + 1,
				prev:  index - 1 < 0 ? false : index - 1
			}

			this.getView().empty().reposition('50%', '50%', function()
			{
				this.setSize(
				{
					w: image.size.width,
					h: image.size.height
				}).resize(
				{
					w: image.size.width,
					h: image.size.height
				}, function()
				{
					this.setContent(image.image, caption);
					
					/* End Callback */
					this.options.onEnd.apply(this,[index, this.content()]);
				});
			});
			
			/* Hide controls */
			if (false === this.slide.next)
			{
				this.getView().disableNext();
			}
			else 
			{
				this.getView().enableNext();
			}
			
			if (false === this.slide.prev)
			{
				this.getView().disablePrev();
			}
			else 
			{
				this.getView().enablePrev();
			}
		},
		next: function()
		{
			if (false !== this.slide.next)
			{
				this.play(this.slide.next);
			}
		},
		prev: function()
		{
			if (false !== this.slide.prev)
			{
				this.play(this.slide.prev);
			}
		},
		construct: function()
		{
			this.images = [];
			
			/* Find other lightbox enabled images */
			$(this.options.selector).each(delegate(this, function(index, element)
			{
				var i = $(element);
				
				this.images.push(
				{
					image:  $(element),
					source: $(element).data('src')
				});
				
				if (!i.data('bound'))
				{
					i.on(
					{
						click: delegate(this, function()
						{
							this.play(index);
						})
					});
					
					i.data('bound', true);
				}
			}));
			
			this.options.onConstruct.apply(this,[]);
			
			return this;
		}
    }
    
    var View = function(options)
    {
    	this.options = options;
		
    	this.isUIBlocked = false;
    	
		this.size =
		{
			w:0,
			h:0
		};
		
		this.timer = 
		{
			scroll: null,
			resize: null
		};
		
		this.preloader = new anoPreload();
    }
    
    View.prototype =
    {
    	caption: null,
    	overlay: null,
		element: null,
		resize: function(size, callback)
		{	
			/* Responsive */
			var ratio = size.w/size.h;
			
			var diff = 
			{
				x: $(window).width() - this.options.distance,
				y: $(window).height() - this.options.distance
			}

			if (size.w > diff.x)
			{
				size.w = diff.x;
				size.h = Math.round(size.w/ratio);
			}
			
			if (size.h > diff.y)
			{
				size.h = diff.y;
				size.w = Math.round(size.h * ratio);
			}
				
			/* Resize box */
			this.element.stop().animate(
			{
				width:		 size.w + 20, 
				marginLeft:	-Math.round((size.w + 20)/2),
				marginTop:	-size.h/2 + $(window).scrollTop()
			}, this.options.speed, this.options.easing).find('.mid > div > div').stop().animate(
			{
				height: size.h
			}, this.options.speed, this.options.easing, delegate(this, callback));
			
			$(window).unbind('scroll').on(
			{
				scroll: delegate(this, this.updateScroll)
			})
			
			return this;
		},
		reposition: function(top, left, callback)
		{
			this.element.stop().animate(
			{
				top:  top,
				left: left
			}, 0, this.options.easing, delegate(this, callback));
		},
		content: function()
		{
			return this.element.find('.mid > div > div');
		},
		setSize: function(size)
		{
			this.size = size;
			
			return this;
		},
		setContent:function(image, caption)
		{
			this.empty();
			
			var append = function(index, element)
			{
				this.content().append(element);
			}
			
			var i = $(image).attr(
			{
				title: 'drag with mouse'
			}).hide(), wrapper = $('<div/>').append(i);
			
			$.each(
			[
				wrapper
			], delegate(this, append));
			
			i.fadeIn(300);
			
			/* Add caption */
			if (caption)
			{
				this.caption = $('<div/>').css(
				{
					position:'absolute',
					top: -100,
					left: 30,
					opacity: 0.9,
					padding: 10,
					background:'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjE0MTIyQkI0MDRBNTExRTNBM0Q5OEEwNzk2OTE4OTY5IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjE0MTIyQkI1MDRBNTExRTNBM0Q5OEEwNzk2OTE4OTY5Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MTQxMjJCQjIwNEE1MTFFM0EzRDk4QTA3OTY5MTg5NjkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MTQxMjJCQjMwNEE1MTFFM0EzRDk4QTA3OTY5MTg5NjkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6Td3NIAAAAD0lEQVR42mJiYGB4BhBgAAD1AOkiZsKUAAAAAElFTkSuQmCC) repeat'
				}).html(caption);

				this.caption.appendTo(this.content()).animate(
				{
					top: -5
				});				
			}
			
			return this;
		},
		empty: function()
		{
			this.content().find('>div>img').parent().remove();
			
			if (this.caption)
			{
				this.caption.remove();
				
				/* Set caption to null */
				this.caption = null;
			}
			
			return this;
		},
		open: function()
		{
			this.element.show();
			
			return this;
		},
		close: function()
		{
			this.element.hide();
			
			return this;	
		},
		blockUI: function()
		{
			if (!this.isUIBlocked)
			{
				this.isUIBlocked = true;
				
				this.overlay = $('<div/>').css(
				{
					position:			'absolute',
					top:				0,
					left: 				0,
					opacity: 			this.options.blockUI.opacity,
					width:  			$.boxModel && document.documentElement.clientWidth || document.body.clientWidth,
					height: 			$(document).height(),
					zIndex: 			this.options.blockUI.zIndex,
					background: 		this.options.blockUI.background
				}).hide().appendTo('body').fadeIn(this.options.blockUI.speed);

				$(window).unbind('resize').on(
				{
					resize: delegate(this, this.updateUI,[this.overlay])
				})
				
				this.overlay.on(
				{
					click: delegate(this, this.unblockUI)
				})
			}
			return this;
		},
		unblockUI: function()
		{
			this.isUIBlocked = false;
			
			/* Unbind window resize */
			$(window).unbind('resize');
			
			this.overlay.stop(true, true).fadeOut(this.options.blockUI.speed, function()
			{
				$(this).remove();
			})
			
			this.close();
			
			return this;
		},
		updateUI: function(event, overlay)
		{
			overlay.css(
			{
				width:  $.boxModel && document.documentElement.clientWidth || document.body.clientWidth,
				height: $(document).height()
			});
			
			if(this.timer.resize !== null) 
		    {
		        clearTimeout(this.timer.resize);        
		    }
		    
		    this.timer.resize = setTimeout(delegate(this, this.onResizeStop), this.options.timeout)
			
			return this;
		},
		updateScroll: function()
		{
		    if(this.timer.scroll !== null) 
		    {
		        clearTimeout(this.timer.scroll);        
		    }
		    
		    this.timer.scroll = setTimeout(delegate(this, this.onScrollStop), this.options.timeout)
		},
		onScrollStop: function()
		{
			this.element.stop(true, true).animate(
			{
				marginTop:	-this.size.h/2 + $(window).scrollTop()
			})
		},
		onResizeStop: function()
		{
			var size = 
			{
				w: this.size.w,
				h: this.size.h
			}
			
			this.resize(size, function()
			{
				/* TODO: Resize visible caption(s) */
			});
		},
		preload: function(image, fn)
		{		
			/* Start image preloading */
			this.preloader.reset().append([image]).preload(fn);
		},
		disableNext: function()
		{
			this.element.find('.ui-flow-next').hide();
		},
		disablePrev: function()
		{
			this.element.find('.ui-flow-prev').hide();
		},
		enableNext: function()
		{
			this.element.find('.ui-flow-next').show();
		},
		enablePrev: function()
		{
			this.element.find('.ui-flow-prev').show();
		}
    }
    
    var anoPreload = function()
    {
    	this.queue  = [];
    	this.images = [];
    	this.total = 0;
    	this.config = 
		{
			cache: true
		};
			
		this.time = 
		{
			start: 0,
			end: 0   
		}
    }
    
    anoPreload.prototype = 
    {
    	onComplete: function(ui){},
		reset: function()
		{
			this.queue 	= [];
			this.images = [];
			this.total 	= 0;
			
			return this;
		},
		append: function(element)
		{
			var queue = this.queue;
			
			$.each(element, function(index, element)
			{
				queue.push(element);
			});
			
			return this;
		},
		finish: function(event, index, image)
		{
			/* Decrease number of finished items */
			this.total--;
			
			$.each(this.images, function(x,i)
			{
				if (i.index == index)
				{
					i.size = {
						width: 	image.width,
						height: image.height
					}
				}
			})

			/* Check if no more items to preload */
			if (0 == this.total)
			{
				this.time.end = new Date().getTime();
				
				this.onComplete.apply(this,[
				{
					time: 	((this.time.end - this.time.start)/1000).toPrecision(2),
					images: this.images.reverse()
				}])
			}
		},
		preload: function(callback)
		{
			/* Set callback function */
			this.onComplete = callback || this.onComplete;
			
			this.time.start = new Date().getTime();
			
			/* Get queue length */
			this.total = i = this.queue.length;
			
			while(i--)
			{
				var image = new Image();

				/* Push image */
				this.images.push(
				{
					index: i,
					image: image,
					size: 
					{
						width:	0,
						height: 0
					}
				});
				
				image.onload  = image.onerror = image.onabort = delegate(this, this.finish, (-1 !== navigator.userAgent.indexOf('MSIE 8.0') ? [event, i,image] : [i,image]));

				/* Set image source */
				image.src = this.config.cache ? this.queue[i].source : (this.queue[i].source + '?u=' + (new Date().getTime()));
			}
		}
    }

	$.fn.anoFlow = function (options) 
	{
		return this.each(function () 
		{
			if (undefined == $(this).data('anoFlow')) 
			{
				var a = new anoFlow(this, options).construct();

				$(this).data('anoFlow', a);
			}
		});
	};
	
	var delegate = function(target, method, args)
	{
		return (typeof method === "function") ? function() 
		{ 
			/* Override prototype */
			arguments.push = Array.prototype.push;
			
			/* Push additional arguments */
			for (var arg in args)
			{
				arguments.push(args[arg]);
			}
			return method.apply(target, arguments); 
		} : function()
		{
			return false;
		};
	}
})();