﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-medical-courses.aspx.cs" Inherits="WadiaResponsiveWebsite.child_medical_courses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



<div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					       <div class="heading">
                  <h2>COURSES AVAILABLE</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<h1>1. Full time Courses</h1>
<div class="devider_20px"></div>
<div style="line-height:20px; text-align:center; width:100%">

    

  <table align="center" width="83%"cellspacing="0" cellpadding="0" style="border: 1px solid #666" >
  <tr>
    <td height="40" style="border: 1px solid #666; padding:5px; background-color:#1ACCFC; text-align:center"><b>Sr. No.</b></td>
    <td style="border: 1px solid #666; background-color:#1ACCFC; text-align:center; padding-top:15PX; width: 188px; padding-left: 5px; padding-right: 5px; padding-bottom: 5px;" ><b>Name of the Course</b> </td>
    <td style="border: 1px solid #666; background-color:#1ACCFC; text-align:center; padding-top:15PX; width: 123px; padding-left: 5px; padding-right: 5px; padding-bottom: 5px;"><b>Duration</b></td>
    <td style="border: 1px solid #666; padding:5px; background-color:#1ACCFC; text-align:center"><b>No. of seats</b></td>
    <td style="border: 1px solid #666; padding:5px; background-color:#1ACCFC; text-align:center"><b>Eligibility</b> </td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; text-align:center">1</td>
    <td style="border: 1px solid #666; padding:5px; text-align:left; width: 188px;">MD</td>
    <td style="border: 1px solid #666; padding:5px; width: 123px; text-align:center">3 Years</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center" >7</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">MBBS</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; text-align:center">2</td>
    <td style="border: 1px solid #666; padding:5px; text-align:left; width: 188px;">DCH</td>
    <td style="border: 1px solid #666; padding:5px; width: 123px; text-align:center">2 Years</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">6</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">MBBS</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; text-align:center">3</td>
    <td style="border: 1px solid #666; padding:5px; text-align:left; width: 188px;">CPS</td>
    <td style="border: 1px solid #666; padding:5px; width: 123px; text-align:center">2 Years</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">6</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">MBBS</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; text-align:center">4</td>
    <td style="border: 1px solid #666; padding:5px; text-align:left; width: 188px;">Post Doctoral Super Specialty Haemat Onco</td>
    <td style="border: 1px solid #666; padding:5px; width: 123px; text-align:center">2 Years</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">2</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">MD/DNB</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; text-align:center">5</td>
    <td style="border: 1px solid #666; padding:5px; text-align:left; width: 188px;">Post Doctoral Fellowship-NICU</td>
    <td style="border: 1px solid #666; padding:5px; width: 123px; text-align:center">1 Year Post MD / 
        <br />
        2 Years Post DCH</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center ">1 MD & <br />
        1 DCH</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">MD/DNB/DCH</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; text-align:center">6</td>
    <td style="border: 1px solid #666; padding:5px; text-align:left; width: 188px;">NNF / IAP</td>
    <td style="border: 1px solid #666; padding:5px; width: 123px; text-align:center">1 Year</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">1 MD & 
        <br />
        1 DCH</td>
    <td style="border: 1px solid #666; padding:5px">MD/DNB/DCH</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; text-align:center">7</td>
    <td style="border: 1px solid #666; padding:5px; text-align:left; width: 188px;"> PICU (IAP)</td>
    <td style="border: 1px solid #666; padding:5px; width: 123px; text-align:center">1 Year</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">2</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">MD/DNB</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; text-align:center">8</td>
    <td style="border: 1px solid #666; padding:5px; text-align:left; width: 188px;">Paediatric Nephrology (IPNA)</td>
    <td style="border: 1px solid #666; padding:5px; width: 123px; text-align:center">1 Year</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">1</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">MD/DNB</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; text-align:center">9</td>
    <td style="border: 1px solid #666; padding:5px; text-align:left; width: 188px;">Paediatric Endocrinology (MIAP)</td>
    <td style="border: 1px solid #666; padding:5px; width: 123px; text-align:center">1 Year</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">1</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">MD/DNB</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; text-align:center">10</td>
    <td style="border: 1px solid #666; padding:5px; text-align:left; width: 188px;">Institutional Fellowship in Paediatric Neurology</td>
    <td style="border: 1px solid #666; padding:5px; width: 123px; text-align:center">1 Year</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">1</td>
    <td style="border: 1px solid #666; padding:5px; text-align:center">MD/DNB</td>
  </tr>
</table>
<div class="devider_20px"></div>



<div style="text-align:left"><h1>2. Part time Courses</h1></div>

  <div class="devider_20px"></div>
  


<table width="83%" cellspacing="0" cellpadding="0" style="border: 1px solid #666;">
  <tr>
    <td height="40" style="border: 1px solid #666; padding:5px; width: 51px;"><b>Sr. No.</b></td>
    <td style="border: 1px solid #666; padding:5px"><b>Name of the Course</b> </td>
    <td style="border: 1px solid #666; padding:5px"><b>Duration</b></td>
    <td style="border: 1px solid #666; padding:5px"><b>Eligibility</b></td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 51px;">1</td>
    <td style="border: 1px solid #666; padding:5px; text-align:left">Child Care Health (CCH)</td>
    <td style="border: 1px solid #666; padding:5px">1 Month</td>
    <td style="border: 1px solid #666; padding:5px">BHMS, BAMS, Unani, Homeopath, MBBS
</td>
  </tr>
</table>



</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>MEDICAL</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						 <li><a href="child-medical-overview.aspx">	Overview  </a></li>
                         <li><a href="child-medical-directormsg.aspx"> Directors message </a></li>
                         <li><a href="child-medical-courses.aspx"> 	Courses available</a></li>
                         <li><a href="child-contact-us.aspx">	Contact Us  </a></li>


                                     <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>



		</div>



</asp:Content>
