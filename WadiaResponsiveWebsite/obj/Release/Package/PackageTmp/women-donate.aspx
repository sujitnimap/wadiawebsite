﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-donate.aspx.cs" Inherits="WadiaResponsiveWebsite.women_donate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


 


    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Contribute </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 

<%--<strong>Introduction paragraph</strong>--%>

Here at the Nowrosjee Wadia Maternity Hospital, we understand the need for quality healthcare and its intrinsic, universal nature. We have always aimed to provide medical services at affordable costs to every woman who needs them, irrespective of the background they come from. 
 <br /><br />
As a charity hospital, we offer subsidized costs and in some cases even a complete cost waiver to women who are unable to bear the fees, because women’s healthcare needs are constantly changing and a lack of resources should never prevent them from the healthy body their deserve. 
 <br /><br />
At the NWMH, we have always relied on the constant encouragement and generosity of our corporate affiliates and individual donors. Your support continues to be crucial for the smooth functioning of the hospital as well as consistent research and regular upgrades to ensure that our services are always at par with any other national or international medical institution. 
 <br /><br />
All donations made to Wadia Hospitals by individuals and organisations are acknowledged in the Annual report and are eligible for a 50% tax exemption under section 80G.
 <br /><br />


<b>How you can help</b>
 <br /><br />
Offering the very best medical services to the vulnerable patients in our care would not be affordable to us given the nominal fee we charge them. Therefore your support is essential to us.
     <br /><br />
•	Donate online
     <div style="padding-left:25px">

We provide quality healthcare to over 100,000 women every year. You can help us make a difference to these lives by contributing to our financial resources so that we can reach out to more and more women every year. 

<br />
         <a href="women-donate-online.aspx">Donate Online</a>



     </div>

     <br />

•	Sponsor a patient
     <div style="padding-left:25px">
To sponsor a patient means taking care of their expenses and giving them a fighting chance, seeing them through their sickness all the way to their smiles. 
<br />
         
         <a href="women-sponsor-patient.aspx">Sponsor a Patient</a>
     </div><br />

•	Equipment 
    <div style="padding-left:25px">
The world of medical sciences is constantly changing and it is imperative for us to keep up. We are constantly upgrading our equipment to ensure that all our treatment is always state of the art, to help provide the best care we can. 
<br />
        
        
        <a href="women-donate-equipment.aspx">Equipment</a>

</div><br />

•	Donate in-kind 
    <div style="padding-left:25px">
o	We gratefully accept contributions in the form of toys, books, craft stationary and other such child-friendly materials, which could comfort and entertain the children through their hospital experience. 
<br />
        
        
  <a href="women-donate-inkind.aspx"> Donate in-kind</a>      </b>
 <%--<- Link to a pop-up box--%>
</div><br />




<%--Pop up box text
In-kind donations can be sent or dropped off at:

Bai Jerbai Wadia Hospital and Institute of Child Health, 
Acharya Donde Marg, 
Lower Parel, 
Mumbai, 
Maharashtra,
India

Tel: +91 - 22 - 24146964 / 65 / 66 / 67
Email: contribute@wadiahospitals.org

 (Add Google Maps box)--%>






</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>CONTRIBUTE</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						    
										<li><a href="women-donate-online.aspx">Donate Online</a></li>
									<li><a href="women-sponsor-patient.aspx">Sponsor a Patient</a></li>
									<li><a href="women-donate-equipment.aspx">Donate Equipment</a></li>
                                     <li><a href="women-donate-inkind.aspx">Donate in-kind</a></li>
									<li><a href="women-corporate-giving.aspx">Corporate Giving</a></li>
									<li><a href="women-testimonials.aspx">Testimonials </a>

                                        <!--<li><a href="women-gallery.aspx">Gallery</a>
                                      
                                       
									</li>-->
									
								</ul>
							</div>
						</div>
					</div>
					
				</div>
			</div>


		</div>







</asp:Content>
