﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-donate-inkind.aspx.cs" Inherits="WadiaResponsiveWebsite.child_donate_inkind" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Donate in-kind </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 
Our aim is to ensure that a child’s experience throughout the period of treatment is as comfortable and pleasant as possible. In this endeavour, toys, books and other such child friendly materials are of immense help to calm down and comfort the children, as well as keep them occupied. 

    <br /><br />


To donate to us in-kind, please fill in the form below and we will be in touch with you shortly.

<br /><br />


 <asp:TextBox ID="txtname" CssClass="textboxcontact" runat="server" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'"></asp:TextBox>
<div class="devider_10px"></div>

    <asp:TextBox ID="txtemail" CssClass="textboxcontact" runat="server" placeholder="Enter your email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email address'"></asp:TextBox>
<div class="devider_10px"></div>
    <asp:TextBox ID="txtconatctno" CssClass="textboxcontact" MaxLength="10" runat="server" placeholder="Phone Number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone Number'"></asp:TextBox>
<div class="devider_10px"></div>
        <asp:TextBox ID="txtpancardno" CssClass="textboxcontact" MaxLength="20" runat="server" placeholder="PAN card number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'PAN card number'"></asp:TextBox>
<div class="devider_10px"></div>

      <asp:TextBox ID="txtadderess" CssClass="textboxcontactdes"  runat="server" placeholder="Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address'"></asp:TextBox>
<div class="devider_10px"></div>

    <asp:TextBox ID="txtmessage" CssClass="textboxcontactdes" runat="server" placeholder="Message" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Message'"></asp:TextBox>
<div class="devider_10px"></div>

<div style="text-align:left"><asp:ImageButton ID="imgbtncontact" src="images/SubmitButton.png" runat="server" OnClick="imgbtncontact_Click" /></div>









</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>CONTRIBUTE</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
								    <li><a href="child-donate-online.aspx">Donate Online</a></li>
									<li><a href="child-sponsor-patient.aspx">Sponsor a Patient</a></li>
									<li><a href="child-donate-equipment.aspx">Donate Equipment</a></li>

                                    <li><a href="child-donate-inkind.aspx">Donate in-kind </a></li>


									<li><a href="child-corporate-giving.aspx">Corporate Giving</a></li>
									<li><a href="child-testimonials.aspx?catnm=Contributors">Testimonials </a></li>
									
								</ul>
							</div>
						</div>
					</div>
					
				</div>
			</div>







		</div>



</asp:Content>
