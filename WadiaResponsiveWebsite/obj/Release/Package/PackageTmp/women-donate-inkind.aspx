﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-donate-inkind.aspx.cs" Inherits="WadiaResponsiveWebsite.women_donate_inkind" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   
    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Donate in-kind </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 

The hospital experience can often bring with it a certain level of stress or anxiety for patients. Here at the hospital, we do everything in our power to make the experience as comfortable as possible. To ensure this, we aim to provide women with all the necessities they require.
<br /><br />
To donate to us in-kind and help us keep our patients happy and comfortable, please fill in the form below and we will be in touch with you shortly.<br /><br />

<asp:TextBox ID="txtname" CssClass="textboxcontact" runat="server" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'"></asp:TextBox>
<div class="devider_10px"></div>

    <asp:TextBox ID="txtemail" CssClass="textboxcontact" runat="server" placeholder="Enter your email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email address'"></asp:TextBox>
<div class="devider_10px"></div>
    <asp:TextBox ID="txtconatctno" CssClass="textboxcontact" MaxLength="10" runat="server" placeholder="Phone Number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone Number'"></asp:TextBox>
<div class="devider_10px"></div>
        <asp:TextBox ID="txtpancardno" CssClass="textboxcontact" MaxLength="20" runat="server" placeholder="PAN card number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'PAN card number'"></asp:TextBox>
<div class="devider_10px"></div>

      <asp:TextBox ID="txtadderess" CssClass="textboxcontactdes"  runat="server" placeholder="Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address'"></asp:TextBox>
<div class="devider_10px"></div>

    <asp:TextBox ID="txtmessage" CssClass="textboxcontactdes" runat="server" placeholder="Message" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Message'"></asp:TextBox>
<div class="devider_10px"></div>

<div style="text-align:left"><asp:ImageButton ID="imgbtncontact" src="images/SubmitButton.png" runat="server" OnClick="imgbtncontact_Click" /></div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>CONTRIBUTE</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                 
									<li><a href="women-donate-online.aspx">Donate Online</a></li>
									<li><a href="women-sponsor-patient.aspx">Sponsor a Patient</a></li>
									<li><a href="women-donate-equipment.aspx">Donate Equipment</a></li>
                                    <li><a href="women-donate-inkind.aspx">Donate in-kind</a></li>
									<li><a href="women-corporate-giving.aspx">Corporate Giving</a></li>
                                    

									<li><a href="women-testimonials.aspx">Testimonials </a></li>
									
								</ul>
							</div>
						</div>
					</div>
					
				</div>
			</div>









    </div>
</asp:Content>
