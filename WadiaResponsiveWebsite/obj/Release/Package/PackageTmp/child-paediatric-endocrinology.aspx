﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-endocrinology.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_endocrinology" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PAEDIATRIC ENDOCRINOLOGY </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
         Paediatric Endocrinology is a speciality that deals with variations of physical growth and sexual development in childhood, as well as diabetes and other disorders of the endocrine glands. BJWHC offers a complete array of diagnostic, treatment and consulting services for infants, children and adolescents with endocrine disorders.
A full-service in-house laboratory supports the department and Endocrine services are provided through 2 OPD clinics held on Monday and Thursday where new and follow-up patients are attended.
<br /><br />

                The department also organises meetings of parents of children who suffer from diabetes and other endocrine disorders to empower them to treat such illnesses with proper knowledge and required care. Besides the specialists who guide parents, parents themselves narrate their own experiences of successful management that goes a long way in making other parents confident about managing such children. Children also are invited during these meetings and magic shows and quizzes are arranged that offer them basic knowledge about their illness in an entertaining way.  


 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
Following conditions are treated at the Paediatric Endocrinology Department :
                                                <br /><br />
                <div style="padding-left:10px">

-	Thyroid disorders
<br />-	Short stature
<br />-	Disorders of Sex Development 
<br />-	Adrenal disorders
<br />-	Bone and mineral metabolism
<br />-	Growth disorders
<br />-	Obesity
<br />-	Juvenile diabetes
<br />-	Menstrual disorders
<br />-	Pubertal disorders
<br />-	Puberty disorders
<br />-	Cancer survivors






</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            

		</div>



</asp:Content>
