﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-urogynaecology.aspx.cs" Inherits="WadiaResponsiveWebsite.women_urogynaecology" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Urogynaecology </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
This super-speciality is a surgical sub-speciality combining urology and gynaecology. It involves the diagnosis and treatment of female pelvic floor disorders such as urinary incontinence. Many women are reluctant to receive help for these problems, despite the widespread prevalence, due to the stigma associated with these conditions. Our trained medical professionals adhere to the highest standards of maintaining confidentiality and treat our patients with the love and respect they deserve, giving them the confidence to get themselves treated.  
                  <br />
 <br />
              <b>Services Provided:</b>   
 <br />
 <br />
<div style="padding-left:25px">
       
-	Urogynaecology Consultation buy a Specialist Physiotherapist
 <br />-	Treatment of prolapse both with and without surgery
 <br />-	Treatment of incontinence both with and without surgery
 <br />-	Management of female urological Disorders
 <br />-	Correction Surgeries- Open and Endoscopic
 <br />-	Pelvic floor Reconstructive Surgeries

</div>


 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

        



		</div>
</asp:Content>
