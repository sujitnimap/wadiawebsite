﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-reprodclinic.aspx.cs" Inherits="WadiaResponsiveWebsite.women_reprodclinic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Reproductive and infertility clinic </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
At the reproductive and infertility clinics, our trained staff offers confidential advice and assistance to individuals or couples who wish to become parents but have been unable to, due to medical reasons. At the clinic, our doctors discuss the needs and problems of our patients and conduct diagnostic tests. Once they have achieved a complete understanding of the patient’s condition, they offer advance medical treatments to obtain the desired conceptions and pregnancies. 
  <br />
 <br />
              <b>Services Provided:</b>   
 <br />
 <br />
<div style="padding-left:25px">
       
-	In Vitro Fertilization (IVF)
 <br />-	Embryo and oocyte cryopreservation
 <br />-	Assisted hatching
 <br />-	Intracytoplasmic sperm injection (ICSI)
 <br />-	Preimplantation genetic diagnosis (PGD)
 <br />-	Basic work up of infertile couple.
 <br />-	Male Infertile Management.
 <br />-	Super ovulation protocols.
 <br />-	Fertility Enhancing Surgeries.
 <br />-	Artificial Reproductive Technique.
 <br />-	Folliculometry.

</div>


 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

           


		</div>




</asp:Content>
