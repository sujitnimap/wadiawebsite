﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-surgery.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_surgery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>GENERAL PAEDIATRIC SURGERY</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Outpatient Department</a></li>
             <li><a href="#view4">Inpatient  Department</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
The hospital offers state of the art facilities for children requiring surgeries. It has well-equipped Operation Theatres that are capable of handling 
                complex paediatric surgical cases of all types. Routine surgeries are planned throughout the day between 8 am and 5 pm on all working days,  
                
                often extended beyond this time as per the need. Besides emergency surgeries are undertaken at any time of the day and night including 
                holidays backed up by availability of emergency staff at any given time. Portable x-ray and ultra sonograpy facilities are available.
                                
 <br />
 <br />
                Our dedicated staff includes highly qualified specialists and nurses who work towards ensuring that each child has a comfortable, 
                warm and caring environment, along with empowering them as well as their families. 
                <br /><br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
OPD runs general service every day between 1 and 3 pm wherein all types of surgical problems are attended. Besides specialty OPD
                 services are also available in morning hours caring for specialised problems such as neurosurgical, orthopaedic problems etc. 
                <br />
<br />
<b>Services Provided :</b>
<br />
<br />
•	Well-child checks/ physical check-ups 

<br />
•	Treatment and management of surgical childhood illnesses
<br />•	Incision and drainage of small abscesses
<br />•	Dressing changes for care of first-degree and limited second-degree burns 
<br />•	Urinary bladder and catheterization 

               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
               
Hospital has dedicated surgical wards for indoor patients. Patients are admitted for pre-operative evaluation and post operative care. Intensive care facilities are provided during immediate post-operative period in surgical intensive care unit as well in neonatal intensive care unit under supervision of intensivists. 

 <br /><br />

Paediatric Surgeons treat conditions related to the anatomic area of the body (thorax and abdomen) as well as diseases unique to infants, children and adolescents. The department offers tertiary level care for children with surgical diseases. 
           
           <br />
           <br />
                      
           <b>Some of the conditions treated by the department are :</b>


<br /><br />

<div style="padding-left:10px">

-	Abdominal masses and tumours including Wilm’s tumour and Neuroblastoma
    <br />
-	Appendicitis
    <br />
-	Biliary atresia
    <br />
-	Chest wall deformities
    <br />
-	Congenital anomalies of the intestine
    <br />
-	Congenital malformations
    <br />

-	Cystic hygromas
<br />-	Oesophageal atresia
<br />-	Head and neck tumours
<br />-	Hepatobiliary cysts and tumours
<br />-	Hernia
<br />-	Inflammatory conditions
<br />-	Lung and mediastinum malformations and tumours
<br />-	Minimally invasive techniques
<br />-	Neoplasms
<br />-	Paediatric colorectal diseases including, imperforate anus, Hirschsprung’s disease and inflammatory bowel disease
<br />-	Pectus Excavatum
<br />-	Undecided testes

    </div>
           <br />
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            


		</div>





</asp:Content>
