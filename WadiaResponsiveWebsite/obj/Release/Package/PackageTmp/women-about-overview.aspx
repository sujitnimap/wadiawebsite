﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-about-overview.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_overview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



		<div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading"><h2>OVERVIEW</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">

The Nowrosjee Wadia Maternity Hospital specializes in offering affordable obstetric and gynaecological services to women across all sections of society, catering to their changing needs through different stages of their lives.<br />
<br />
The hospital does not limit its services to its patients alone. It acts as a tertiary level referral centre as well as helps rehabilitate these women and their families by showing them methods of improving the health 
    and sanitation around their environment and thus ensuring a healthy life for their whole family. <br />
<br />
With a dedicated team of over 20 specialists this 305-bed hospital, declared as a heritage structure the hospital sees more than 10,000 inpatients and over 100,000 outpatients annually. In addition, as a teaching hospital, it also has a constant influx of aspiring doctors who train under some of the best specialists in not only the city but also the country. 


<br />
<br />
</div>
<!-- ABOUT US CONTENT -->

           
                    
                    
				</div>
			</div>
			


            <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="women-about-overview.aspx">Overview</a></li>
									<li><a href="women-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="women-about-chairman-msg.aspx">Chairman's message</a></li>
									<%--<li><a href="women-about-awards.aspx">Awards and Achievements</a></li>--%>
									<li><a href="women-about-history.aspx">History </a>

                                      <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-nusserwanji-wadia.aspx">Nowrosjee Nusserwanji Wadia (1849 – 99)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>


		</div>
	


</asp:Content>
