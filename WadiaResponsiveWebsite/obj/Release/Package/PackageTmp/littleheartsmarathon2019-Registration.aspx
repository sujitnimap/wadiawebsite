﻿<%@ Page Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="littleheartsmarathon2019-Registration.aspx.cs" Inherits="WadiaResponsiveWebsite.littleheartsmarathon2019_Registration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <script src="js/jquery-1.9.1.min.js"></script>

    <script type="text/javascript">
        
        $(document).ready(function () {

            var a = $('#ctl00_ContentPlaceHolder1_ddlTshirtforParentQuantity').val();
            if (a == 0) {
                $('#tr_tshirtforparent1').hide();
                $('#tr_tshirtforparent2').hide();
                $('#tr_tshirtforparent3').hide();
                $('#tr_tshirtforparent4').hide();
                $('#tr_tshirtforparent5').hide();

            }
        });
        function OnTshirtforParentQuantityChange() {
            {
                var a = $('#ctl00_ContentPlaceHolder1_ddlTshirtforParentQuantity').val();
                if (a == 0) {
                    $('#tr_tshirtforparent1').hide();
                    $('#tr_tshirtforparent2').hide();
                    $('#tr_tshirtforparent3').hide();
                    $('#tr_tshirtforparent4').hide();
                    $('#tr_tshirtforparent5').hide();

                }
                if (a == 1) {
                    $('#tr_tshirtforparent1').show();
                    $('#tr_tshirtforparent2').hide();
                    $('#tr_tshirtforparent3').hide();
                    $('#tr_tshirtforparent4').hide();
                    $('#tr_tshirtforparent5').hide();
                    CalculateTotalAmount();
                }
                if (a == 2) {
                    $('#tr_tshirtforparent1').show();
                    $('#tr_tshirtforparent2').show();
                    $('#tr_tshirtforparent3').hide();
                    $('#tr_tshirtforparent4').hide();
                    $('#tr_tshirtforparent5').hide();
                    CalculateTotalAmount();
                }
                if (a == 3) {
                    $('#tr_tshirtforparent1').show();
                    $('#tr_tshirtforparent2').show();
                    $('#tr_tshirtforparent3').show();
                    $('#tr_tshirtforparent4').hide();
                    $('#tr_tshirtforparent5').hide();
                    CalculateTotalAmount();
                }
                if (a == 4) {
                    $('#tr_tshirtforparent1').show();
                    $('#tr_tshirtforparent2').show();
                    $('#tr_tshirtforparent3').show();
                    $('#tr_tshirtforparent4').show();
                    $('#tr_tshirtforparent5').hide();
                    CalculateTotalAmount();
                }
                if (a == 5) {
                    $('#tr_tshirtforparent1').show();
                    $('#tr_tshirtforparent2').show();
                    $('#tr_tshirtforparent3').show();
                    $('#tr_tshirtforparent4').show();
                    $('#tr_tshirtforparent5').show();
                    CalculateTotalAmount();
                }

                
            }
        }

        function displayFirstParentSection() {
            category = $('#<%=ddlCategory.ClientID%>').val();      
            CalculateTotalAmount();
        }

        function OnCategoryChange() {
            category = $('#<%=ddlCategory.ClientID%>').val();
          
            if (category == "Wish Run 1.0 km 7 - 10 yr") {            
                CalculateTotalAmount();
                return false;
            }

            if (category == "Dream Run 2.5 km 11 - 14 yr" || category == "Delight Run 5 km 15 - 18 yr") {
                  CalculateTotalAmount();

                return false;
            }
        }
 
        function validateChildCnt() {

            var lblMessage = $("span[id$='lblMessage']");
            var isValid = true;
            lblMessage.html("");
            age = parseInt($('#<%=ddlChildAge.ClientID%>').val());//added by prashant
            var dob = $('#ctl00_ContentPlaceHolder1_txtDOB').val();

            //The following code is written by prashant for calculation of age in no. of months          
            var DateOfBirth = new Date(dob);
                  
            var finalDate = new Date(2019, 00, 27);            
                    
            var ActualAgeInYrs;
           
            ActualAgeInYrs = Math.ceil((finalDate-DateOfBirth) / (365.25 * 24 * 60 * 60 * 1000));
            
			//alert(ActualAgeInYrs);
            //alert(age);
			
            if (ActualAgeInYrs < age) {
                lblMessage.html('Please select proper date of birth as date selected by you does not match with the age selected by you');
                    return false;
            }

            category = $('#<%=ddlCategory.ClientID%>').val();


            if (category == "Dream Run 2.5 km 11 - 14 yr" ) {
                if (age < 11 || age > 14) {
                    lblMessage.html('Child\'s age  must be between 11 and 14 yrs');
                    return false;
                }
            }   
            
            if (category == "Delight Run 5 km 15 - 18 yr") {
                if (age < 15) {
                    lblMessage.html('Child\'s age  must be between 15 and 18 yrs');
                    return false;
                }
            }
            if (category == "Wish Run 1 km 7 - 10 yr") {
                if ($('#<%=txtFirstParentName.ClientID%>').val() == "") {
                    lblMessage.html('Please enter First Parent Name.')
                    return false;
                }            
            }

            if (category == "Wish Run 1.0 km 7 - 10 yr")
                if (age < 7 || age > 10) {
                    lblMessage.html('Age must be between 7 and 10 yrs');
                    return false;
                }
            
            var isChecked = $('#chkTearmAndCondition').is(':checked');

            if (!isChecked) {
                lblMessage.html('Please check Terms And Conditions')
                return false;
            }

            var email = $('#ctl00_ContentPlaceHolder1_txtParentEmailId').val();
            var atpos = email.indexOf("@");
            var dotpos = email.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                alert("Parent Email ID is Not a valid e-mail address");
                return false;
            }

           
            CalculateTotalAmount();
         }

       
</script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ToolkitScriptManager runat="server"></asp:ToolkitScriptManager>
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <asp:HiddenField ID="hdn_totalAmount" runat="server"  />
    <div class="row block05">
        <div class="col-full">
            <div class="wrap-col">

                <div style="border: 1px solid #ddd; padding: 10px 20px 20px 10px; border-radius: 4px;">

                    <div class="heading">
                        <h2>Registration Form - 2019 Marathon</h2>
                    </div>
                    <br />
                    Whether you walk, run, jog or stroll everyone is welcome! The important thing is to get involved and show your support and appreciation for the community. You can register as an individual, group of friends or family.
You may also choose to sponsor the run for the disadvantaged children in our society.<br />
                    <br />
                    Regestration Fees – RS. 500/-
                    <br />
T-SHIRT for parent if required – RS. 200/- each
                    <br />
                    <br />
                    <table>
                        <tr>
                            <td>Choose a Category
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCategory" CssClass="textboxcontact_list"
                                    required="" runat="server" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Category'" onchange="OnCategoryChange();">
                                    <%--<asp:ListItem Text="Fun Run 0.5 km 4 - 7 yr" Value="Fun Run 0.5 km 4 - 7 yr"></asp:ListItem>--%>
                                    <asp:ListItem Text="Wish Run 1.0 km 7 - 10 yr" Value="Wish Run 1.0 km 7 - 10 yr"></asp:ListItem>
                                    <asp:ListItem Text="Dream Run 2.5 km 11 - 14 yr" Value="Dream Run 2.5 km 11 - 14 yr"></asp:ListItem>
                                    <asp:ListItem Text="Delight Run 5 km 15 - 18 yr" Value="Delight Run 5 km 15 - 18 yr"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="marathonregAdd" ControlToValidate="ddlCategory" runat="server" ForeColor="Red" ErrorMessage="*" />
                            </td>
                        </tr>
                        <tr>
                            <td>Child First Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtChildFirstName" CssClass="textboxcontact_list" required="" runat="server" placeholder="First Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtChildFirstNames" ValidationGroup="marathonregAdd" ControlToValidate="txtChildFirstName" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Child Middle Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtChildMiddleName" CssClass="textboxcontact_list" required="" runat="server" placeholder="Middle Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Middle Name'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="marathonregAdd" ControlToValidate="txtChildFirstName" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Child Last Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtChildLastName" CssClass="textboxcontact_list" required="" runat="server" placeholder="Last Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtChildLastName" ValidationGroup="marathonregAdd" ControlToValidate="txtChildLastName" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Your Parent's Email ID
                            </td>
                            <td>
                                <asp:TextBox ID="txtParentEmailId" CssClass="textboxcontact_list"
                                    required="" runat="server" placeholder="Parent Email ID*" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Parent Email ID'">
                                </asp:TextBox>                                
                                <asp:RequiredFieldValidator ID="reftxtParentEmailId" ValidationGroup="marathonregAdd"
                                    ControlToValidate="txtParentEmailId" runat="server" 
                                    ErrorMessage="*">
                                </asp:RequiredFieldValidator>      
                            </td>
                        </tr>
                        <tr>
                            <td>Contact Number</td>
                            <td> <asp:TextBox ID="txtContactNo" CssClass="textboxcontact_list" MaxLength="10"
                                    required="" runat="server" placeholder="Contact Number*" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Contact Number'">
                                </asp:TextBox>                                
                                <asp:RequiredFieldValidator ID="reftxtContactNo" ValidationGroup="marathonregAdd"
                                    ControlToValidate="txtContactNo" runat="server" 
                                    ErrorMessage="*">
                                </asp:RequiredFieldValidator>  </td>
                        </tr>
                        <tr>
                            <td>Gender (Kids)
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbGenderKids" runat="server" >
                                    <asp:ListItem Text="Boy" Value="Male" Selected="True" />
                                    <asp:ListItem Text="Girl" Value="Female" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>

                        <tr>
                            <td>Age
                            </td>
                            <td>
                                <asp:DropDownList CssClass="textboxcontact_list" ID="ddlChildAge" runat="server">  
                                    <asp:ListItem Selected="True"  Text="7" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                    <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                    <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                    <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                    <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="refddlChildAge" ValidationGroup="marathonregAdd" ControlToValidate="ddlChildAge" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td>
                                  <asp:TextBox ID="txtDOB" CssClass="textboxcontact_list"
                                    required="" runat="server" placeholder="Date of Birth*" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Date of Birth'">
                                </asp:TextBox>
                                 <asp:CalendarExtender ID="txtDOB_calender" runat="server" TargetControlID="txtDOB">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reftxtDOB" ValidationGroup="marathonregAdd"
                                    ControlToValidate="txtDOB" runat="server" ForeColor="Red"
                                    ErrorMessage="*">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>T-Shirt Size
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTShirtSizeChild" CssClass="textboxcontact_list"
                                    required="" runat="server" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'T-Shirt Size'" >
                                    <asp:ListItem Text="28 Inches" Value="28"></asp:ListItem>                                    
                                    <asp:ListItem Text="32 Inches" Value="32"></asp:ListItem>                                    
                                    <asp:ListItem Text="36 Inches" Value="36"></asp:ListItem>
                                    <asp:ListItem Text="38 Inches" Value="38"></asp:ListItem>
                                    <asp:ListItem Text="40 Inches" Value="40"></asp:ListItem>
                                    <asp:ListItem Text="42 Inches" Value="42"></asp:ListItem>
                                    <asp:ListItem Text="44 Inches" Value="44"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="refddlTShirtSizeChild" ValidationGroup="marathonregAdd" ControlToValidate="ddlTShirtSizeChild" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td>School Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtSchoolName" CssClass="textboxcontact_list" required="" runat="server"
                                    placeholder="School Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="reftxtSchoolName" ValidationGroup="marathonregAdd" ControlToValidate="txtSchoolName" runat="server" ForeColor="Red" ErrorMessage="*" />
                            </td>
                        </tr>

                        <tr>
                            <td>Parent Full Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtFirstParentName" CssClass="textboxcontact_list"  runat="server"
                                    placeholder="Parent Full Name" onfocus="this.placeholder = ''" onblur="this.placeholder = '1st Parent Name'">
                                </asp:TextBox>
                            </td>
                        </tr>
                       <tr>
                           <td>
                               T-shirt quantity for Parent
                           </td>
                            <td>
                                <asp:DropDownList ID="ddlTshirtforParentQuantity" CssClass="textboxcontact_list"
                                    required="" runat="server" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'T-Shirt Quantity'" onchange="OnTshirtforParentQuantityChange();">
                                     <asp:ListItem Text="Select Quantity of T-shirt" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>                                   
                                </asp:DropDownList>                                
                            </td>
                       </tr>
                    
                          <tr id="tr_tshirtforparent1">
                           <td>
                               T-shirt size for Parent 1
                           </td>
                            <td>
                                <asp:DropDownList ID="ddlTshirtSizeParent1" CssClass="textboxcontact_list"
                                    required="" runat="server" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'T-Shirt Size'">    
                                    <asp:ListItem Text="Select T-shirt Size" Value="0"></asp:ListItem>                                                                   
                                    <asp:ListItem Text="36 Inches" Value="36 Inches"></asp:ListItem>
                                    <asp:ListItem Text="38 Inches" Value="38 Inches"></asp:ListItem>
                                    <asp:ListItem Text="40 Inches" Value="40 Inches"></asp:ListItem>
                                    <asp:ListItem Text="42 Inches" Value="42 Inches"></asp:ListItem>
                                    <asp:ListItem Text="44 Inches" Value="44 Inches"></asp:ListItem> 
                                    <asp:ListItem Text="46 Inches" Value="46 Inches"></asp:ListItem>                                  
                                </asp:DropDownList>                                
                            </td>
                       </tr>
                         <tr id="tr_tshirtforparent2">
                           <td>
                               T-shirt size for Parent 2
                           </td>
                            <td>
                                <asp:DropDownList ID="ddlTshirtSizeParent2" CssClass="textboxcontact_list"
                                    required="" runat="server" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'T-Shirt Size'">   
                                    <asp:ListItem Text="Select T-shirt Size" Value="0"></asp:ListItem>                                                                    
                                    <asp:ListItem Text="36 Inches" Value="36 Inches"></asp:ListItem>
                                    <asp:ListItem Text="38 Inches" Value="38 Inches"></asp:ListItem>
                                    <asp:ListItem Text="40 Inches" Value="40 Inches"></asp:ListItem>
                                    <asp:ListItem Text="42 Inches" Value="42 Inches"></asp:ListItem>
                                    <asp:ListItem Text="44 Inches" Value="44 Inches"></asp:ListItem> 
                                    <asp:ListItem Text="46 Inches" Value="46 Inches"></asp:ListItem>                                  
                                </asp:DropDownList>                                
                            </td>
                       </tr>
                         <tr id="tr_tshirtforparent3">
                           <td>
                               T-shirt size for Parent 3
                           </td>
                            <td>
                                <asp:DropDownList ID="ddlTshirtSizeParent3" CssClass="textboxcontact_list"
                                    required="" runat="server" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'T-Shirt Size'">   
                                    <asp:ListItem Text="Select T-shirt Size" Value="0"></asp:ListItem>                                                                    
                                    <asp:ListItem Text="36 Inches" Value="36 Inches"></asp:ListItem>
                                    <asp:ListItem Text="38 Inches" Value="38 Inches"></asp:ListItem>
                                    <asp:ListItem Text="40 Inches" Value="40 Inches"></asp:ListItem>
                                    <asp:ListItem Text="42 Inches" Value="42 Inches"></asp:ListItem>
                                    <asp:ListItem Text="44 Inches" Value="44 Inches"></asp:ListItem> 
                                    <asp:ListItem Text="46 Inches" Value="46 Inches"></asp:ListItem>                                  
                                </asp:DropDownList>                                
                            </td>
                       </tr>
                         <tr id="tr_tshirtforparent4">
                           <td>
                               T-shirt size for Parent 4
                           </td>
                            <td>
                                <asp:DropDownList ID="ddlTshirtSizeParent4" CssClass="textboxcontact_list"
                                    required="" runat="server" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'T-Shirt Size'">  
                                    <asp:ListItem Text="Select T-shirt Size" Value="0"></asp:ListItem>                                                                     
                                    <asp:ListItem Text="36 Inches" Value="36 Inches"></asp:ListItem>
                                    <asp:ListItem Text="38 Inches" Value="38 Inches"></asp:ListItem>
                                    <asp:ListItem Text="40 Inches" Value="40 Inches"></asp:ListItem>
                                    <asp:ListItem Text="42 Inches" Value="42 Inches"></asp:ListItem>
                                    <asp:ListItem Text="44 Inches" Value="44 Inches"></asp:ListItem> 
                                    <asp:ListItem Text="46 Inches" Value="46 Inches"></asp:ListItem>                                  
                                </asp:DropDownList>                                
                            </td>
                       </tr>
                         <tr id="tr_tshirtforparent5">
                           <td>
                               T-shirt size for Parent 5
                           </td>
                            <td>
                                <asp:DropDownList ID="ddlTshirtSizeParent5" CssClass="textboxcontact_list"
                                    required="" runat="server" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'T-Shirt Size'">   
                                    <asp:ListItem Text="Select T-shirt Size" Value="0"></asp:ListItem>                                                                    
                                    <asp:ListItem Text="36 Inches" Value="36 Inches"></asp:ListItem>
                                    <asp:ListItem Text="38 Inches" Value="38 Inches"></asp:ListItem>
                                    <asp:ListItem Text="40 Inches" Value="40 Inches"></asp:ListItem>
                                    <asp:ListItem Text="42 Inches" Value="42 Inches"></asp:ListItem>
                                    <asp:ListItem Text="44 Inches" Value="44 Inches"></asp:ListItem> 
                                    <asp:ListItem Text="46 Inches" Value="46 Inches"></asp:ListItem>                                  
                                </asp:DropDownList>                                
                            </td>
                       </tr>
                    <tr>
                        <td>
                            Address
                        </td>
                        <td>
                    <asp:TextBox ID="txtAddress" TextMode="MultiLine" CssClass="textboxcontact_list" runat="server" placeholder="Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address'">
                    </asp:TextBox>
                    
                        </td>
                    </tr>

                    <tr>
                        <td>
                            City
                        </td>
                        <td>
                            <asp:TextBox ID="txtCity" CssClass="textboxcontact_list" runat="server" placeholder="City" onfocus="this.placeholder = ''" onblur="this.placeholder = 'City'"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            State
                        </td>
                        <td>
                            <asp:TextBox ID="txtState" CssClass="textboxcontact_list" runat="server" placeholder="State" onfocus="this.placeholder = ''" onblur="this.placeholder = 'State'"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Country 
                        </td>
                        <td>
                        <asp:DropDownList CssClass="textboxcontact_drpdwn" ID="ddlCountrys" runat="server">
                            <asp:ListItem>Afghanistan</asp:ListItem>
                            <asp:ListItem>Aland Islands</asp:ListItem>
                            <asp:ListItem>Albania</asp:ListItem>
                            <asp:ListItem>Algeria</asp:ListItem>
                            <asp:ListItem>Andorra</asp:ListItem>
                            <asp:ListItem>Angola</asp:ListItem>
                            <asp:ListItem>Anguilla</asp:ListItem>
                            <asp:ListItem>Antartica</asp:ListItem>
                            <asp:ListItem>Antigua And Barbuda</asp:ListItem>
                            <asp:ListItem>Argentina</asp:ListItem>
                            <asp:ListItem>Armenia</asp:ListItem>
                            <asp:ListItem>Aruba</asp:ListItem>
                            <asp:ListItem>Ascension Island/St. Helena</asp:ListItem>
                            <asp:ListItem>Australia</asp:ListItem>
                            <asp:ListItem>Austria</asp:ListItem>
                            <asp:ListItem>Azerbaijan</asp:ListItem>
                            <asp:ListItem>Bahamas</asp:ListItem>
                            <asp:ListItem>Bahrain</asp:ListItem>
                            <asp:ListItem>Bangladesh</asp:ListItem>
                            <asp:ListItem>Barbados</asp:ListItem>
                            <asp:ListItem>Belarus</asp:ListItem>
                            <asp:ListItem>Belgium</asp:ListItem>
                            <asp:ListItem>Belize</asp:ListItem>
                            <asp:ListItem>Benin</asp:ListItem>
                            <asp:ListItem>Bermuda</asp:ListItem>
                            <asp:ListItem>Bhutan</asp:ListItem>
                            <asp:ListItem>Bolivia</asp:ListItem>
                            <asp:ListItem>Bosnia Herzegovina</asp:ListItem>
                            <asp:ListItem>Botswana</asp:ListItem>
                            <asp:ListItem>Bouvet Island</asp:ListItem>
                            <asp:ListItem>Brazil</asp:ListItem>
                            <asp:ListItem>British Virgin Islands</asp:ListItem>
                            <asp:ListItem>Brunei</asp:ListItem>
                            <asp:ListItem>Bulgaria</asp:ListItem>
                            <asp:ListItem>Burkina Faso</asp:ListItem>
                            <asp:ListItem>Burundi</asp:ListItem>
                            <asp:ListItem>Cambodia</asp:ListItem>
                            <asp:ListItem>Cameroon, United Republic Of</asp:ListItem>
                            <asp:ListItem>Canada</asp:ListItem>
                            <asp:ListItem>Cape Verde, Republic Of</asp:ListItem>
                            <asp:ListItem>Cayman Islands</asp:ListItem>
                            <asp:ListItem>Central African Republic</asp:ListItem>
                            <asp:ListItem>Chad</asp:ListItem>
                            <asp:ListItem>Chile</asp:ListItem>
                            <asp:ListItem>China</asp:ListItem>
                            <asp:ListItem>Christmas Island</asp:ListItem>
                            <asp:ListItem>Cocos Islands</asp:ListItem>
                            <asp:ListItem>Colombia</asp:ListItem>
                            <asp:ListItem>Comoros</asp:ListItem>
                            <asp:ListItem>Congo</asp:ListItem>
                            <asp:ListItem>Congo Democratic Republic Of</asp:ListItem>
                            <asp:ListItem>Cook Islands</asp:ListItem>
                            <asp:ListItem>Costa Rica</asp:ListItem>
                            <asp:ListItem>Croatia</asp:ListItem>
                            <asp:ListItem>Cuba</asp:ListItem>
                            <asp:ListItem>Cyprus</asp:ListItem>
                            <asp:ListItem>Czech Republic</asp:ListItem>
                            <asp:ListItem>Denmark</asp:ListItem>
                            <asp:ListItem>Djibouti</asp:ListItem>
                            <asp:ListItem>Dominica</asp:ListItem>
                            <asp:ListItem>Dominican Republic</asp:ListItem>
                            <asp:ListItem>Ecuador</asp:ListItem>
                            <asp:ListItem>Egypt</asp:ListItem>
                            <asp:ListItem>El Salvador</asp:ListItem>
                            <asp:ListItem>Equatorial Guinea</asp:ListItem>
                            <asp:ListItem>Eritrea</asp:ListItem>
                            <asp:ListItem>Estonia</asp:ListItem>
                            <asp:ListItem>Ethiopia</asp:ListItem>
                            <asp:ListItem>Faeroe Islands</asp:ListItem>
                            <asp:ListItem>Falkland Islands</asp:ListItem>
                            <asp:ListItem>Fiji Islands</asp:ListItem>
                            <asp:ListItem>Finland</asp:ListItem>
                            <asp:ListItem>France</asp:ListItem>
                            <asp:ListItem>French Guiana</asp:ListItem>
                            <asp:ListItem>French Polynesia</asp:ListItem>
                            <asp:ListItem>Gabon</asp:ListItem>
                            <asp:ListItem>Gambia</asp:ListItem>
                            <asp:ListItem>Georgia</asp:ListItem>
                            <asp:ListItem>Germany</asp:ListItem>
                            <asp:ListItem>Ghana</asp:ListItem>
                            <asp:ListItem>Gibraltar</asp:ListItem>
                            <asp:ListItem>Greece</asp:ListItem>
                            <asp:ListItem>Greenland</asp:ListItem>
                            <asp:ListItem>Grenada</asp:ListItem>
                            <asp:ListItem>Guadeloupe</asp:ListItem>
                            <asp:ListItem>Guam</asp:ListItem>
                            <asp:ListItem>Guatemala</asp:ListItem>
                            <asp:ListItem>Guernsey</asp:ListItem>
                            <asp:ListItem>Guinea</asp:ListItem>
                            <asp:ListItem>Guinea Bissau</asp:ListItem>
                            <asp:ListItem>Guyana</asp:ListItem>
                            <asp:ListItem>Haiti</asp:ListItem>
                            <asp:ListItem>Heard Island And McDonald Islands</asp:ListItem>
                            <asp:ListItem>Honduras</asp:ListItem>
                            <asp:ListItem>Hong Kong</asp:ListItem>
                            <asp:ListItem>Hungary</asp:ListItem>
                            <asp:ListItem>Iceland</asp:ListItem>
                            <asp:ListItem Selected="True">India</asp:ListItem>
                            <asp:ListItem>Indonesia</asp:ListItem>
                            <asp:ListItem>Iran</asp:ListItem>
                            <asp:ListItem>Iraq</asp:ListItem>
                            <asp:ListItem>Ireland, Republic Of</asp:ListItem>
                            <asp:ListItem>Isle Of Man</asp:ListItem>
                            <asp:ListItem>Israel</asp:ListItem>
                            <asp:ListItem>Italy</asp:ListItem>
                            <asp:ListItem>Ivory Coast</asp:ListItem>
                            <asp:ListItem>Jamaica</asp:ListItem>
                            <asp:ListItem>Japan</asp:ListItem>
                            <asp:ListItem>Jersey Island</asp:ListItem>
                            <asp:ListItem>Jordan</asp:ListItem>
                            <asp:ListItem>Kazakstan</asp:ListItem>
                            <asp:ListItem>Kenya</asp:ListItem>
                            <asp:ListItem>Kiribati</asp:ListItem>
                            <asp:ListItem>Korea, Democratic Peoples Republic</asp:ListItem>
                            <asp:ListItem>Korea, Republic Of</asp:ListItem>
                            <asp:ListItem>Kuwait</asp:ListItem>
                            <asp:ListItem>Kyrgyzstan</asp:ListItem>
                            <asp:ListItem>Lao, People&#39;s Dem. Rep.</asp:ListItem>
                            <asp:ListItem>Latvia</asp:ListItem>
                            <asp:ListItem>Lebanon</asp:ListItem>
                            <asp:ListItem>Lesotho</asp:ListItem>
                            <asp:ListItem>Liberia</asp:ListItem>
                            <asp:ListItem>Libyan Arab Jamahiriya</asp:ListItem>
                            <asp:ListItem>Liechtenstein</asp:ListItem>
                            <asp:ListItem>Lithuania</asp:ListItem>
                            <asp:ListItem>Luxembourg</asp:ListItem>
                            <asp:ListItem>Macau</asp:ListItem>
                            <asp:ListItem>Macedonia</asp:ListItem>
                            <asp:ListItem>Madagascar (Malagasy)</asp:ListItem>
                            <asp:ListItem>Malawi</asp:ListItem>
                            <asp:ListItem>Malaysia</asp:ListItem>
                            <asp:ListItem>Maldives</asp:ListItem>
                            <asp:ListItem>Mali</asp:ListItem>
                            <asp:ListItem>Malta</asp:ListItem>
                            <asp:ListItem>Marianna Islands</asp:ListItem>
                            <asp:ListItem>Marshall Islands</asp:ListItem>
                            <asp:ListItem>Martinique</asp:ListItem>
                            <asp:ListItem>Mauritania</asp:ListItem>
                            <asp:ListItem>Mauritius</asp:ListItem>
                            <asp:ListItem>Mayotte</asp:ListItem>
                            <asp:ListItem>Mexico</asp:ListItem>
                            <asp:ListItem>Micronesia</asp:ListItem>
                            <asp:ListItem>Moldova</asp:ListItem>
                            <asp:ListItem>Monaco</asp:ListItem>
                            <asp:ListItem>Mongolia</asp:ListItem>
                            <asp:ListItem>Montserrat</asp:ListItem>
                            <asp:ListItem>Morocco</asp:ListItem>
                            <asp:ListItem>Mozambique</asp:ListItem>
                            <asp:ListItem>Myanmar</asp:ListItem>
                            <asp:ListItem>Namibia</asp:ListItem>
                            <asp:ListItem>Nauru</asp:ListItem>
                            <asp:ListItem>Nepal</asp:ListItem>
                            <asp:ListItem>Netherland Antilles</asp:ListItem>
                            <asp:ListItem>Netherlands</asp:ListItem>
                            <asp:ListItem>New Caledonia</asp:ListItem>
                            <asp:ListItem>New Zealand</asp:ListItem>
                            <asp:ListItem>Nicaragua</asp:ListItem>
                            <asp:ListItem>Niger</asp:ListItem>
                            <asp:ListItem>Nigeria</asp:ListItem>
                            <asp:ListItem>Niue</asp:ListItem>
                            <asp:ListItem>Norfolk Island</asp:ListItem>
                            <asp:ListItem>Norway</asp:ListItem>
                            <asp:ListItem>Occupied Palestinian Territory</asp:ListItem>
                            <asp:ListItem>Oman, Sultanate Of</asp:ListItem>
                            <asp:ListItem>Pakistan</asp:ListItem>
                            <asp:ListItem>Palau</asp:ListItem>
                            <asp:ListItem>Panama</asp:ListItem>
                            <asp:ListItem>Papua New Guinea (Niugini)</asp:ListItem>
                            <asp:ListItem>Paraguay</asp:ListItem>
                            <asp:ListItem>Peru</asp:ListItem>
                            <asp:ListItem>Philippines</asp:ListItem>
                            <asp:ListItem>Pitcairn</asp:ListItem>
                            <asp:ListItem>Poland</asp:ListItem>
                            <asp:ListItem>Portugal</asp:ListItem>
                            <asp:ListItem>Qatar</asp:ListItem>
                            <asp:ListItem>Reunion</asp:ListItem>
                            <asp:ListItem>Romania</asp:ListItem>
                            <asp:ListItem>Russian Federation</asp:ListItem>
                            <asp:ListItem>Rwanda</asp:ListItem>
                            <asp:ListItem>Saint Lucia</asp:ListItem>
                            <asp:ListItem>Saint Vincent And The Grenadines</asp:ListItem>
                            <asp:ListItem>Samoa, American</asp:ListItem>
                            <asp:ListItem>Samoa, Independent State Of</asp:ListItem>
                            <asp:ListItem>San Marino</asp:ListItem>
                            <asp:ListItem>Sao Tome &amp; Principe</asp:ListItem>
                            <asp:ListItem>Saudi Arabia</asp:ListItem>
                            <asp:ListItem>Senegal</asp:ListItem>
                            <asp:ListItem>Serbia</asp:ListItem>
                            <asp:ListItem>Seychelles Islands</asp:ListItem>
                            <asp:ListItem>Sierra Leone</asp:ListItem>
                            <asp:ListItem>Singapore</asp:ListItem>
                            <asp:ListItem>Slovakia</asp:ListItem>
                            <asp:ListItem>Slovenia</asp:ListItem>
                            <asp:ListItem>Solomon Islands</asp:ListItem>
                            <asp:ListItem>Somalia</asp:ListItem>
                            <asp:ListItem>South Africa</asp:ListItem>
                            <asp:ListItem>Spain</asp:ListItem>
                            <asp:ListItem>Sri Lanka</asp:ListItem>
                            <asp:ListItem>St. Kitts - Nevis</asp:ListItem>
                            <asp:ListItem>St. Pierre &amp; Miquelon</asp:ListItem>
                            <asp:ListItem>Sudan</asp:ListItem>
                            <asp:ListItem>Suriname</asp:ListItem>
                            <asp:ListItem>Svalbard And Jan Mayen Is</asp:ListItem>
                            <asp:ListItem>Swaziland</asp:ListItem>
                            <asp:ListItem>Sweden</asp:ListItem>
                            <asp:ListItem>Switzerland</asp:ListItem>
                            <asp:ListItem>Syrian Arab Rep.</asp:ListItem>
                            <asp:ListItem>Taiwan, Republic of China</asp:ListItem>
                            <asp:ListItem>Tajikistan</asp:ListItem>
                            <asp:ListItem>Tanzania</asp:ListItem>
                            <asp:ListItem>Thailand</asp:ListItem>
                            <asp:ListItem>Timor Leste</asp:ListItem>
                            <asp:ListItem>Togo</asp:ListItem>
                            <asp:ListItem>Tonga</asp:ListItem>
                            <asp:ListItem>Trinidad &amp; Tobago</asp:ListItem>
                            <asp:ListItem>Tunisia</asp:ListItem>
                            <asp:ListItem>Turkey</asp:ListItem>
                            <asp:ListItem>Turkmenistan</asp:ListItem>
                            <asp:ListItem>Turks And Caicos Islands</asp:ListItem>
                            <asp:ListItem>Tuvalu</asp:ListItem>
                            <asp:ListItem>Uganda</asp:ListItem>
                            <asp:ListItem>Ukraine</asp:ListItem>
                            <asp:ListItem>United Arab Emirates</asp:ListItem>
                            <asp:ListItem>United Kingdom</asp:ListItem>
                            <asp:ListItem>United States</asp:ListItem>
                            <asp:ListItem>United States Minor Outlying Islnds</asp:ListItem>
                            <asp:ListItem>Uruguay</asp:ListItem>
                            <asp:ListItem>Uzbekistan Sum</asp:ListItem>
                            <asp:ListItem>Vanuatu</asp:ListItem>
                            <asp:ListItem>Vatican City State</asp:ListItem>
                            <asp:ListItem>Venezuela</asp:ListItem>
                            <asp:ListItem>Vietnam</asp:ListItem>
                            <asp:ListItem>Wallis &amp; Futuna Islands</asp:ListItem>
                            <asp:ListItem>Western Sahara</asp:ListItem>
                            <asp:ListItem>Yemen, Republic Of</asp:ListItem>
                            <asp:ListItem>Yugoslavia</asp:ListItem>
                            <asp:ListItem>Zambia</asp:ListItem>
                            <asp:ListItem>Zimbabwe</asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td>
                        Nationality
                            </td>
                        <td>
                        <asp:DropDownList ID="ddlNationalitys" CssClass="textboxcontact_drpdwn" runat="server">
                            <asp:ListItem>Afghanistan</asp:ListItem>
                            <asp:ListItem>Aland Islands</asp:ListItem>
                            <asp:ListItem>Albania</asp:ListItem>
                            <asp:ListItem>Algeria</asp:ListItem>
                            <asp:ListItem>Andorra</asp:ListItem>
                            <asp:ListItem>Angola</asp:ListItem>
                            <asp:ListItem>Anguilla</asp:ListItem>
                            <asp:ListItem>Antartica</asp:ListItem>
                            <asp:ListItem>Antigua And Barbuda</asp:ListItem>
                            <asp:ListItem>Argentina</asp:ListItem>
                            <asp:ListItem>Armenia</asp:ListItem>
                            <asp:ListItem>Aruba</asp:ListItem>
                            <asp:ListItem>Ascension Island/St. Helena</asp:ListItem>
                            <asp:ListItem>Australia</asp:ListItem>
                            <asp:ListItem>Austria</asp:ListItem>
                            <asp:ListItem>Azerbaijan</asp:ListItem>
                            <asp:ListItem>Bahamas</asp:ListItem>
                            <asp:ListItem>Bahrain</asp:ListItem>
                            <asp:ListItem>Bangladesh</asp:ListItem>
                            <asp:ListItem>Barbados</asp:ListItem>
                            <asp:ListItem>Belarus</asp:ListItem>
                            <asp:ListItem>Belgium</asp:ListItem>
                            <asp:ListItem>Belize</asp:ListItem>
                            <asp:ListItem>Benin</asp:ListItem>
                            <asp:ListItem>Bermuda</asp:ListItem>
                            <asp:ListItem>Bhutan</asp:ListItem>
                            <asp:ListItem>Bolivia</asp:ListItem>
                            <asp:ListItem>Bosnia Herzegovina</asp:ListItem>
                            <asp:ListItem>Botswana</asp:ListItem>
                            <asp:ListItem>Bouvet Island</asp:ListItem>
                            <asp:ListItem>Brazil</asp:ListItem>
                            <asp:ListItem>British Virgin Islands</asp:ListItem>
                            <asp:ListItem>Brunei</asp:ListItem>
                            <asp:ListItem>Bulgaria</asp:ListItem>
                            <asp:ListItem>Burkina Faso</asp:ListItem>
                            <asp:ListItem>Burundi</asp:ListItem>
                            <asp:ListItem>Cambodia</asp:ListItem>
                            <asp:ListItem>Cameroon, United Republic Of</asp:ListItem>
                            <asp:ListItem>Canada</asp:ListItem>
                            <asp:ListItem>Cape Verde, Republic Of</asp:ListItem>
                            <asp:ListItem>Cayman Islands</asp:ListItem>
                            <asp:ListItem>Central African Republic</asp:ListItem>
                            <asp:ListItem>Chad</asp:ListItem>
                            <asp:ListItem>Chile</asp:ListItem>
                            <asp:ListItem>China</asp:ListItem>
                            <asp:ListItem>Christmas Island</asp:ListItem>
                            <asp:ListItem>Cocos Islands</asp:ListItem>
                            <asp:ListItem>Colombia</asp:ListItem>
                            <asp:ListItem>Comoros</asp:ListItem>
                            <asp:ListItem>Congo</asp:ListItem>
                            <asp:ListItem>Congo Democratic Republic Of</asp:ListItem>
                            <asp:ListItem>Cook Islands</asp:ListItem>
                            <asp:ListItem>Costa Rica</asp:ListItem>
                            <asp:ListItem>Croatia</asp:ListItem>
                            <asp:ListItem>Cuba</asp:ListItem>
                            <asp:ListItem>Cyprus</asp:ListItem>
                            <asp:ListItem>Czech Republic</asp:ListItem>
                            <asp:ListItem>Denmark</asp:ListItem>
                            <asp:ListItem>Djibouti</asp:ListItem>
                            <asp:ListItem>Dominica</asp:ListItem>
                            <asp:ListItem>Dominican Republic</asp:ListItem>
                            <asp:ListItem>Ecuador</asp:ListItem>
                            <asp:ListItem>Egypt</asp:ListItem>
                            <asp:ListItem>El Salvador</asp:ListItem>
                            <asp:ListItem>Equatorial Guinea</asp:ListItem>
                            <asp:ListItem>Eritrea</asp:ListItem>
                            <asp:ListItem>Estonia</asp:ListItem>
                            <asp:ListItem>Ethiopia</asp:ListItem>
                            <asp:ListItem>Faeroe Islands</asp:ListItem>
                            <asp:ListItem>Falkland Islands</asp:ListItem>
                            <asp:ListItem>Fiji Islands</asp:ListItem>
                            <asp:ListItem>Finland</asp:ListItem>
                            <asp:ListItem>France</asp:ListItem>
                            <asp:ListItem>French Guiana</asp:ListItem>
                            <asp:ListItem>French Polynesia</asp:ListItem>
                            <asp:ListItem>Gabon</asp:ListItem>
                            <asp:ListItem>Gambia</asp:ListItem>
                            <asp:ListItem>Georgia</asp:ListItem>
                            <asp:ListItem>Germany</asp:ListItem>
                            <asp:ListItem>Ghana</asp:ListItem>
                            <asp:ListItem>Gibraltar</asp:ListItem>
                            <asp:ListItem>Greece</asp:ListItem>
                            <asp:ListItem>Greenland</asp:ListItem>
                            <asp:ListItem>Grenada</asp:ListItem>
                            <asp:ListItem>Guadeloupe</asp:ListItem>
                            <asp:ListItem>Guam</asp:ListItem>
                            <asp:ListItem>Guatemala</asp:ListItem>
                            <asp:ListItem>Guernsey</asp:ListItem>
                            <asp:ListItem>Guinea</asp:ListItem>
                            <asp:ListItem>Guinea Bissau</asp:ListItem>
                            <asp:ListItem>Guyana</asp:ListItem>
                            <asp:ListItem>Haiti</asp:ListItem>
                            <asp:ListItem>Heard Island And McDonald Islands</asp:ListItem>
                            <asp:ListItem>Honduras</asp:ListItem>
                            <asp:ListItem>Hong Kong</asp:ListItem>
                            <asp:ListItem>Hungary</asp:ListItem>
                            <asp:ListItem>Iceland</asp:ListItem>
                            <asp:ListItem Selected="True">India</asp:ListItem>
                            <asp:ListItem>Indonesia</asp:ListItem>
                            <asp:ListItem>Iran</asp:ListItem>
                            <asp:ListItem>Iraq</asp:ListItem>
                            <asp:ListItem>Ireland, Republic Of</asp:ListItem>
                            <asp:ListItem>Isle Of Man</asp:ListItem>
                            <asp:ListItem>Israel</asp:ListItem>
                            <asp:ListItem>Italy</asp:ListItem>
                            <asp:ListItem>Ivory Coast</asp:ListItem>
                            <asp:ListItem>Jamaica</asp:ListItem>
                            <asp:ListItem>Japan</asp:ListItem>
                            <asp:ListItem>Jersey Island</asp:ListItem>
                            <asp:ListItem>Jordan</asp:ListItem>
                            <asp:ListItem>Kazakstan</asp:ListItem>
                            <asp:ListItem>Kenya</asp:ListItem>
                            <asp:ListItem>Kiribati</asp:ListItem>
                            <asp:ListItem>Korea, Democratic Peoples Republic</asp:ListItem>
                            <asp:ListItem>Korea, Republic Of</asp:ListItem>
                            <asp:ListItem>Kuwait</asp:ListItem>
                            <asp:ListItem>Kyrgyzstan</asp:ListItem>
                            <asp:ListItem>Lao, People&#39;s Dem. Rep.</asp:ListItem>
                            <asp:ListItem>Latvia</asp:ListItem>
                            <asp:ListItem>Lebanon</asp:ListItem>
                            <asp:ListItem>Lesotho</asp:ListItem>
                            <asp:ListItem>Liberia</asp:ListItem>
                            <asp:ListItem>Libyan Arab Jamahiriya</asp:ListItem>
                            <asp:ListItem>Liechtenstein</asp:ListItem>
                            <asp:ListItem>Lithuania</asp:ListItem>
                            <asp:ListItem>Luxembourg</asp:ListItem>
                            <asp:ListItem>Macau</asp:ListItem>
                            <asp:ListItem>Macedonia</asp:ListItem>
                            <asp:ListItem>Madagascar (Malagasy)</asp:ListItem>
                            <asp:ListItem>Malawi</asp:ListItem>
                            <asp:ListItem>Malaysia</asp:ListItem>
                            <asp:ListItem>Maldives</asp:ListItem>
                            <asp:ListItem>Mali</asp:ListItem>
                            <asp:ListItem>Malta</asp:ListItem>
                            <asp:ListItem>Marianna Islands</asp:ListItem>
                            <asp:ListItem>Marshall Islands</asp:ListItem>
                            <asp:ListItem>Martinique</asp:ListItem>
                            <asp:ListItem>Mauritania</asp:ListItem>
                            <asp:ListItem>Mauritius</asp:ListItem>
                            <asp:ListItem>Mayotte</asp:ListItem>
                            <asp:ListItem>Mexico</asp:ListItem>
                            <asp:ListItem>Micronesia</asp:ListItem>
                            <asp:ListItem>Moldova</asp:ListItem>
                            <asp:ListItem>Monaco</asp:ListItem>
                            <asp:ListItem>Mongolia</asp:ListItem>
                            <asp:ListItem>Montserrat</asp:ListItem>
                            <asp:ListItem>Morocco</asp:ListItem>
                            <asp:ListItem>Mozambique</asp:ListItem>
                            <asp:ListItem>Myanmar</asp:ListItem>
                            <asp:ListItem>Namibia</asp:ListItem>
                            <asp:ListItem>Nauru</asp:ListItem>
                            <asp:ListItem>Nepal</asp:ListItem>
                            <asp:ListItem>Netherland Antilles</asp:ListItem>
                            <asp:ListItem>Netherlands</asp:ListItem>
                            <asp:ListItem>New Caledonia</asp:ListItem>
                            <asp:ListItem>New Zealand</asp:ListItem>
                            <asp:ListItem>Nicaragua</asp:ListItem>
                            <asp:ListItem>Niger</asp:ListItem>
                            <asp:ListItem>Nigeria</asp:ListItem>
                            <asp:ListItem>Niue</asp:ListItem>
                            <asp:ListItem>Norfolk Island</asp:ListItem>
                            <asp:ListItem>Norway</asp:ListItem>
                            <asp:ListItem>Occupied Palestinian Territory</asp:ListItem>
                            <asp:ListItem>Oman, Sultanate Of</asp:ListItem>
                            <asp:ListItem>Pakistan</asp:ListItem>
                            <asp:ListItem>Palau</asp:ListItem>
                            <asp:ListItem>Panama</asp:ListItem>
                            <asp:ListItem>Papua New Guinea (Niugini)</asp:ListItem>
                            <asp:ListItem>Paraguay</asp:ListItem>
                            <asp:ListItem>Peru</asp:ListItem>
                            <asp:ListItem>Philippines</asp:ListItem>
                            <asp:ListItem>Pitcairn</asp:ListItem>
                            <asp:ListItem>Poland</asp:ListItem>
                            <asp:ListItem>Portugal</asp:ListItem>
                            <asp:ListItem>Qatar</asp:ListItem>
                            <asp:ListItem>Reunion</asp:ListItem>
                            <asp:ListItem>Romania</asp:ListItem>
                            <asp:ListItem>Russian Federation</asp:ListItem>
                            <asp:ListItem>Rwanda</asp:ListItem>
                            <asp:ListItem>Saint Lucia</asp:ListItem>
                            <asp:ListItem>Saint Vincent And The Grenadines</asp:ListItem>
                            <asp:ListItem>Samoa, American</asp:ListItem>
                            <asp:ListItem>Samoa, Independent State Of</asp:ListItem>
                            <asp:ListItem>San Marino</asp:ListItem>
                            <asp:ListItem>Sao Tome &amp; Principe</asp:ListItem>
                            <asp:ListItem>Saudi Arabia</asp:ListItem>
                            <asp:ListItem>Senegal</asp:ListItem>
                            <asp:ListItem>Serbia</asp:ListItem>
                            <asp:ListItem>Seychelles Islands</asp:ListItem>
                            <asp:ListItem>Sierra Leone</asp:ListItem>
                            <asp:ListItem>Singapore</asp:ListItem>
                            <asp:ListItem>Slovakia</asp:ListItem>
                            <asp:ListItem>Slovenia</asp:ListItem>
                            <asp:ListItem>Solomon Islands</asp:ListItem>
                            <asp:ListItem>Somalia</asp:ListItem>
                            <asp:ListItem>South Africa</asp:ListItem>
                            <asp:ListItem>Spain</asp:ListItem>
                            <asp:ListItem>Sri Lanka</asp:ListItem>
                            <asp:ListItem>St. Kitts - Nevis</asp:ListItem>
                            <asp:ListItem>St. Pierre &amp; Miquelon</asp:ListItem>
                            <asp:ListItem>Sudan</asp:ListItem>
                            <asp:ListItem>Suriname</asp:ListItem>
                            <asp:ListItem>Svalbard And Jan Mayen Is</asp:ListItem>
                            <asp:ListItem>Swaziland</asp:ListItem>
                            <asp:ListItem>Sweden</asp:ListItem>
                            <asp:ListItem>Switzerland</asp:ListItem>
                            <asp:ListItem>Syrian Arab Rep.</asp:ListItem>
                            <asp:ListItem>Taiwan, Republic of China</asp:ListItem>
                            <asp:ListItem>Tajikistan</asp:ListItem>
                            <asp:ListItem>Tanzania</asp:ListItem>
                            <asp:ListItem>Thailand</asp:ListItem>
                            <asp:ListItem>Timor Leste</asp:ListItem>
                            <asp:ListItem>Togo</asp:ListItem>
                            <asp:ListItem>Tonga</asp:ListItem>
                            <asp:ListItem>Trinidad &amp; Tobago</asp:ListItem>
                            <asp:ListItem>Tunisia</asp:ListItem>
                            <asp:ListItem>Turkey</asp:ListItem>
                            <asp:ListItem>Turkmenistan</asp:ListItem>
                            <asp:ListItem>Turks And Caicos Islands</asp:ListItem>
                            <asp:ListItem>Tuvalu</asp:ListItem>
                            <asp:ListItem>Uganda</asp:ListItem>
                            <asp:ListItem>Ukraine</asp:ListItem>
                            <asp:ListItem>United Arab Emirates</asp:ListItem>
                            <asp:ListItem>United Kingdom</asp:ListItem>
                            <asp:ListItem>United States</asp:ListItem>
                            <asp:ListItem>United States Minor Outlying Islnds</asp:ListItem>
                            <asp:ListItem>Uruguay</asp:ListItem>
                            <asp:ListItem>Uzbekistan Sum</asp:ListItem>
                            <asp:ListItem>Vanuatu</asp:ListItem>
                            <asp:ListItem>Vatican City State</asp:ListItem>
                            <asp:ListItem>Venezuela</asp:ListItem>
                            <asp:ListItem>Vietnam</asp:ListItem>
                            <asp:ListItem>Wallis &amp; Futuna Islands</asp:ListItem>
                            <asp:ListItem>Western Sahara</asp:ListItem>
                            <asp:ListItem>Yemen, Republic Of</asp:ListItem>
                            <asp:ListItem>Yugoslavia</asp:ListItem>
                            <asp:ListItem>Zambia</asp:ListItem>
                            <asp:ListItem>Zimbabwe</asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    </tr>
<tr>
    <td>How did you come to know about the Marathon?</td>
    <td>
<asp:DropDownList ID="ddlKnowAboutMarathon" CssClass="textboxcontact_drpdwn" runat="server">
                            <asp:ListItem>Facebook</asp:ListItem>
                            <asp:ListItem>Twitter</asp:ListItem>
                            <asp:ListItem>Instagram</asp:ListItem>
                            <asp:ListItem>Website</asp:ListItem>
                            <asp:ListItem>Hoarding</asp:ListItem>
                            <asp:ListItem>WhatApp</asp:ListItem>
                            <asp:ListItem>Radio</asp:ListItem>
                            <asp:ListItem>Newspaper</asp:ListItem>
                            <asp:ListItem>Other Source</asp:ListItem>                            
    </asp:DropDownList>
    </td>
</tr>

                    </table>
                    <div class="devider_20px"></div>



                    <asp:TextBox ID="txtAnyOtherRelevantInformation" TextMode="MultiLine" Rows="5" CssClass="textboxcontact_list" runat="server" placeholder="Any other additional information" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Any other additional information'"></asp:TextBox>

                    <br />

                    We invite you to look into the event and connect with one, two, or even more children’s run!
Your involvement with us by sponsoring a run allows you with a unique opportunity to connect with the community on a more personal level. Your contribution will help a child run on your behalf.

                    <br />
                    <br />
                    <asp:CheckBox ID="chkIsSponsorARun" runat="server" Text="Sponsor a run" onclick="CalculateTotalAmount();"  />

                    <br />
                    <br />
                    <div style="display: inline; padding-right: 35px">
                        <strong>Total Amount in INR : </strong>
                    </div>

                    <div style="display: inline">
                        <asp:Label ID="lblTotalAmountInINR" runat="server" ></asp:Label>
                    </div>
                    <br />
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    <br />
                    <input type="checkbox" id="chkTearmAndCondition" title="Terms And Conditions" />
                    I agree to all the <a href="Little_Hearts_Marathon-terms.aspx">terms, conditions and guidelines.</a>


                    <div class="devider_20px"></div>


                    <asp:Button ID="btnPayment" OnClientClick="return validateChildCnt();" Text="Pay now" ValidationGroup="marathonregAdd" CssClass="btn" runat="server"  OnClick="btnPayment_Click"  />

                </div>
            </div>
        </div>
    </div>


<script type="text/javascript">
    function CalculateTotalAmount() {
        var totalAmount = 500;

        category = $('#<%=ddlCategory.ClientID%>').val();
        parentfortshirt = $('#<%=ddlTshirtforParentQuantity.ClientID%>').val();
        
        tshirtprice = parentfortshirt * 200;
        if ($('#<%=chkIsSponsorARun.ClientID%>').is(":checked"))
            totalAmount += 500;
        if (parentfortshirt>0) {
            totalAmount += tshirtprice
        }
        $('#<%=lblTotalAmountInINR.ClientID%>').text(totalAmount)
        $('#<%=hdn_totalAmount.ClientID%>').val(totalAmount);

        //alert(totalAmount);
    }


    $(document).ready(function () {
        displayFirstParentSection();
       // displaySecondParentSection();
        CalculateTotalAmount();
    });
</script>
</asp:Content>


