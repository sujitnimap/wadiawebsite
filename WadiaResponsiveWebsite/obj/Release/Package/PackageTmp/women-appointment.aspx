﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-appointment.aspx.cs" Inherits="WadiaResponsiveWebsite.women_appointment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading"><h2>CAREERS</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">

Please note requesting an appointment through our website does not indicate confirmation of appointment. 
You will receive confirmation of appointment either through email or phone number mentioned in the form. 
For any queries please email us at <b><i>appointments@wadiahospitals.org</i></b> 
We look forward to serving you. Thank you.
<br />
<br />

</div>
<!-- ABOUT US CONTENT -->

           
                    
                    
				</div>
			</div>
			

        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="women-about-overview.aspx">Overview</a></li>
									<li><a href="women-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="women-about-chairman-msg.aspx">Chairman's message</a></li>
									<li><a href="women-about-awards.aspx">Awards and Achievements</a></li>
									<li><a href="women-about-history.aspx">History </a>

                                         <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-nusserwanji-wadia.aspx">Nowrosjee Nusserwanji Wadia (1849 – 99)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>



		</div>


</asp:Content>
