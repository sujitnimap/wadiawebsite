﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH.aspx.cs" Inherits="WadiaResponsiveWebsite.NWMH" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    
 <script type="text/javascript">
        var shadow = $('<div id="shadowElem"></div>');
        var speed = 1000;
        $(document).ready(function () {
            $('body').prepend(shadow);
        });
        $(window).load(function () {
            screenHeight = $(window).height();
            screenWidth = $(window).width();
            elemWidth = $('#dropElem').outerWidth(true);
            elemHeight = $('#dropElem').outerHeight(true);

            leftPosition = (screenWidth / 2) - (elemWidth / 2);
            topPosition = (screenHeight / 2) - (elemHeight / 2);
            //console.log(screenHeight);

            $('#dropElem').css({
                'left': leftPosition + 'px',
                'top': -elemHeight + 'px'
            });
            $('#dropElem').show().animate({
                'top': topPosition
            }, speed);

            shadow.animate({
                'opacity': 0.7
            }, speed);

            $('#dropClose').click(function () {
                shadow.animate({
                    'opacity': 0
                }, speed);
                $('#dropElem').animate({
                    'top': -elemHeight + 'px'
                }, speed, function () {
                    shadow.remove();
                    $(this).remove();
                });
                //shadow.remove();
            });
        });


    </script>

    <style type="text/css">
        #dropElem {
            display: none;
            position: absolute;
            top: 0;
            border-radius: 10px 10px 10px 10px;
            box-shadow: 0 0 25px 5px #999;
            padding: 20px;
            background: #fff;
	    left:0px !important;
	    top:0% !important;
            z-index: 9999;
        }

        #shadowElem {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: none;
            opacity: 0.3;
        }

        #dropContent {
            position: relative;
            z-index: 99999;
        }

        #dropClose {
            position: absolute;
            z-index: 99999;
            cursor: pointer;
            top: -32px;
            right: -30px;
            padding: 5px;
            background-color: black;
            border-radius: 6px 6px 6px 6px;
            color: #fff;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <!--------------Content--------------->
    
    <%--<div class="splashPopup">
        <div class="splashPopupInside">
            <div class="splashPopupInsideClose">
                <img src="images/close_popup.png" onclick="hide()" />
            </div>
            <a href="http://littleheartsmarathon.com/" target="_blank">
                <img src="images/LHM_Banner.jpg" /></a>
        </div>
    </div>--%>

   <%--<div id="dropElem">
        <div id="dropContent">
            <div id="dropClose">X</div>
            <a href="http://littleheartsmarathon.com/" target="_blank">
                <img src="images/LHM_Banner.jpg" width="850" height="500" alt="briai" /></a>
        </div>
    </div>--%>

    <div class="row block05">
        <!--<div class="title"><span>BLOG</span></div>-->


        <div style="padding: 20px 20px 0px 20px;">
            <div class="col-full">


                <div id="wrapper">

                    <div class="slider-wrapper theme-default">
                        <div id="slider" class="nivoSlider">
                           <!-- <a href="littleheartsmarathon2017-Registration.aspx" target="_blank">
                                <img src="images/LHM1.jpg" data-thumb="images/LHM1.jpg" alt="" /></a>

                            <a href="littleheartsmarathon2017-Registration.aspx" target="_blank">
                                <img src="images/LHM2.jpg" data-thumb="images/LHM2.jpg" alt="" /></a> -->

                            <img src="images/NWMH1.png" data-thumb="images/NWMH1.png" alt="" />
                            <img src="images/NWMH2.png" data-thumb="images/NWMH2.png" alt="" />
                            <img src="images/NWMH3.png" data-thumb="images/NWMH3.png" alt="" />
                            <img src="images/NWMH4.png" data-thumb="images/NWMH4.png" alt="" />                            

                        </div>

                    </div>

                </div>
                <script type="text/javascript" src="themes/jquery-1.9.0.min.js"></script>
                <script type="text/javascript" src="themes/jquery.nivo.slider.js"></script>


                <script type="text/javascript">
                    $(window).load(function () {
                        $('#slider').nivoSlider();
                    });
                </script>

            </div>
        </div>


        <!-- ROW BLOCK END -->

        <div class="row block02">

            <div class="col-1-3">
                <div class="wrap-col">

                    <div class="boxhome" style="background: #3AB97A;">
                        <div class="titlewadia">
                            <img src="images/IconTestimonial.png" style="height: 30px; width: 25px;" />
                            <b>Testimonials</b>
                        </div>
                        <div style="height: 5px"></div>
                        Here are a few things some of our patients and partners have to say, about their journey with NWMH
                        
                         <div class="devider_20px"></div>
                        <%--<div style="height: 22px"></div>--%>
                        <div align="right">
                            <a href="women-testimonials.aspx" class="readmorebtngrns">
                                <img src="images/IconGoRightArrow.png"></a>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col-1-3">
                <div class="wrap-col">

                    <div class="boxhome" style="background: #FFBB0B; line-height: 22px;">
                        <div class="titlewadia">
                            <img src="images/IconDonations.png" style="width: 30px; height: 25px" />
                            <b>Contribute </b>
                        </div>
                        <%--<div class="devider_10px"></div>--%>


Help us fulfil our promise to the mothers of our country. A promise of quality healthcare for a healthy and, more importantly, happy life for them and their families

         <div align="right">
             <a href="women-donate.aspx">
                 <img src="images/IconGoRightArrow.png"></a>
         </div>

                    </div>

                </div>
            </div>


            <div class="col-1-3">
                <div class="wrap-col">
                    <a href="women-gallery-category.aspx">
                        <div class="boxhome" style="background-image: url(images/Childcare-Gallery.png); background-repeat: no-repeat;">
                            <div class="titlewadia" style="padding-left: 70px; padding-top: 8px;"><b>Gallery</b></div>

                            <div style="height: 132px"></div>
                        </div>
                    </a>
                </div>
            </div>

        </div>

        <div class="row block02">

            <div class="col-1-3">
                <div class="wrap-col">


                    <div class="colorcontent">
                        <div class="titlewadia25">
                            About Us
                        </div>

                        <div style="border: 1px solid #999; width: 100%;"></div>
                        <div class="devider_10px"></div>


                        In a country where Women’s Healthcare was still only being established and at a time when focus was shifting to women’s requirements 
                        and their empowerment, Sir Ness Wadia understood the importance of catering to the medical needs of women regardless of their economic 
                        status. 


                        <br />
                        <br />
                        In 1926, in the true philanthropic tradition of the Wadia family, Sir Ness set up the Nowrosjee Wadia Maternity Hospital in fond memory of his father Nowrosjee Wadia. This joint venture between the State Government and the Bombay Municipality at the time, was set up to provide affordable comprehensive healthcare for women catering to their changing needs through different stages of their lives.

                        <br />
                        <br />

                        <div class="readmore" style="text-align: right">
                            <a href="women-about-overview.aspx" class="readmorebtngrns">
                                <img src="images/readmore1.png" /></a>
                        </div>



                    </div>


                </div>
            </div>


            <div class="col-1-3">
                <div class="wrap-col">

                    <div class="colorcontent">
                        <div class="titlewadia25">Events</div>

                        <div style="border: 1px solid #999;"></div>
                        <div class="devider_10px"></div>


                        <%--<asp:Label ID="lblevent" runat="server" Visible="false" Font-Size="12px" Text=""></asp:Label>--%>
                        We organize several fundraising events, medical conferences, Continuing Medical Education Programs and other events throughout the year, in different parts of the city, to raise awareness and funds for causes that are close to our hearts. 
                        <div class="devider_10px"></div>
                        <asp:Panel ID="pnlforupcomingevent" runat="server">
                            <div align="center">
                                <asp:Label ID="lblUpcomingevent" runat="server" Font-Bold="true" Font-Size="14px" Text="Upcoming Events"></asp:Label>
                            </div>
                            <br />
                            <asp:DataList ID="dlistupcomingevent" RepeatColumns="1" runat="server">
                                <ItemTemplate>
                                    <a href='<%# "women-event.aspx?id="+ Convert.ToString(Eval("id"))  %>'>
                                        <asp:Label ID="lbleventname" Style="color: #666; font-size: 14px; font-family: Arial; line-height: 20px;" runat="server" Text='<%#Eval("eventname") %>'></asp:Label>
                                    </a>
                                    <br />
                                    <div class="devider_10px"></div>
                                </ItemTemplate>
                            </asp:DataList>
                            <div class="titlewadiacontent" align="right">
                                <%--<a target="_blank" href='/adminpanel/<%#Eval("filepath") %>'>Download Now &nbsp;&nbsp;<img src="images/pdf.jpg" /></a>--%>
                                <div class="devider_10px"></div>
                                <div style="text-align: right">
                                    <a href="women-events.aspx" class="readmorebtngrns">
                                        <img src="images/readmore2.png" /></a>
                                </div>

                            </div>
                        </asp:Panel>
                        <%-- <div class="readmorecase" style="display:inline-block; width:100%;"><a href="child-events.aspx">Read More <img src="images/IconReadMore.png" width="25px" height="25px" /></a> </div>--%>
                        <div class="devider_10px"></div>
                        <br />
                        <asp:Panel ID="pnlforpassevent" runat="server">
                            <div align="center">
                                <asp:Label ID="lblpastevent" runat="server" Font-Bold="true" Font-Size="17px" Text="Recent Events"></asp:Label>
                            </div>
                            <br />
                            <div class="devider_10px"></div>
                            <asp:DataList ID="dlistpastevent" RepeatColumns="1" runat="server">
                                <ItemTemplate>
                                    <a href='<%# "women-event.aspx?id="+ Convert.ToString(Eval("id"))  %>'>
                                        <asp:Label ID="lbleventname" Style="color: #666; font-size: 14px; font-family: Arial; line-height: 20px;" runat="server" Text='<%#Eval("eventname") %>'></asp:Label>
                                    </a>
                                    <br />
                                    <div class="devider_10px"></div>
                                </ItemTemplate>
                            </asp:DataList>
                            <div class="titlewadiacontent" align="right">
                                <%--<a target="_blank" href='/adminpanel/<%#Eval("filepath") %>'>Download Now &nbsp;&nbsp;<img src="images/pdf.jpg" /></a>--%>
                                <div class="devider_10px"></div>
                                <div style="text-align: right">
                                    <a href="women-events.aspx" class="readmorebtngrns">
                                        <img src="images/readmore2.png" /></a>
                                </div>

                            </div>
                        </asp:Panel>


                    </div>

                </div>
            </div>


            <div class="col-1-3">
                <div class="wrap-col">


                    <div class="colorcontent">
                        <div class="titlewadia25">Newsletter</div>
                        <div style="border: 1px solid #999;"></div>
                        <div class="devider_10px"></div>

                        <table style="width: 100%">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtsubscribe" runat="server" CssClass="textboxnews" placeholder="Enter Email Address..." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email Address...'"></asp:TextBox>
                                </td>

                                <td>
                                    <div style="padding-top: 10px; display: inline;">
                                        <asp:ImageButton ID="imgbtnsubscribe" runat="server" ValidationGroup="email" ImageUrl="./images/subscribe.png" OnClick="imgbtnsubscribe_Click" />
                                </td>

                                <td>
                                    <asp:RequiredFieldValidator ID="reqtxtsubscribe" runat="server" ControlToValidate="txtsubscribe" Font-Size="15px" ValidationGroup="email" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                                    <asp:RegularExpressionValidator ID="reftxtsubscribe" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="email" Font-Size="15px" ControlToValidate="txtsubscribe" ForeColor="Red" runat="server" ErrorMessage="*"></asp:RegularExpressionValidator>

                                </td>



                            </tr>


                        </table>
                        <!--<div>

<div style="padding-top:2px; height:30px; display:inline-block"> <input placeholder="Enter your email address" name="" type="text" style="background-color:#F2F2F2; height:30px; width:210px" /></div>
<div style="display:inline-block">
<img src="images/SubmitButton.png" /></div>
</div>-->

                        <div class="devider_10px"></div>
                        <div style="border: 1px solid #999;"></div>
                        <div class="devider_10px"></div>
                        <div class="titlewadia25">Latest News</div>
                        <asp:DataList ID="dlistcasestudy" RepeatColumns="1" runat="server">
                            <ItemTemplate>
                                <asp:Label ID="lbltitle" Font-Size="14px" Font-Names="Arial" runat="server" Text='<%#Eval("title") %>'></asp:Label>
                                <br />
                                <br />
                                <div class="devider_10px"></div>
                            </ItemTemplate>
                        </asp:DataList>
                        <div class="devider_10px"></div>
                        <div class="titlewadiacontent" align="right">
                            <%--<a target="_blank" href='/adminpanel/<%#Eval("filepath") %>'>Download Now &nbsp;&nbsp;<img src="images/pdf.jpg" /></a>--%>
                            <div class="readmorecase" style="display: inline-block">
                                <a href="women-case-studies.aspx">
                                    <img src="images/readmore3.png" /></a>
                            </div>
                            <div class="devider_10px"></div>
                        </div>
                    </div>

                    <div style="border: 1px solid #999;"></div>
                    <div class="devider_10px"></div>
                    <video width="240" height="200" controls>
                        <source src="video/WadiaHospitals.mp4" type="video/mp4">
                        <source src="video/WadiaHospitals.mp4" type="video/ogg">
                        Your browser does not support the video tag.
                    </video>


                </div>


            </div>
        </div>


    </div>



    <!------------- CONTENT END ------------>



</asp:Content>
