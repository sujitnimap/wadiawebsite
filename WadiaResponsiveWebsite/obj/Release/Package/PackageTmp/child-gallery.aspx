﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-gallery.aspx.cs" Inherits="WadiaResponsiveWebsite.child_gallary" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" type="text/css" href="../demo/jquery.lightbox.css">
    <style type="text/css">
        body {
            font-family: sans-serif;
            font-size: 1rem;
            color: #333;
        }

        .container {
            max-width: 800px;
            margin: 0 auto;
        }

        .gallery {
            list-style: none;
            overflow:visible;
            padding: 0;
            margin: 0;
        }

            .gallery li {
                float: left;
                margin: 4px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>GALLERY</h2>
                </div>
                <!-- ABOUT US CONTENT -->

                <div class="aboutus_app">

                    <div id="content">
                        <div class="post">

                            <div>
                                <asp:LinkButton ID="lbtnCategory" runat="server" OnClick="lbtnCategory_Click">Main Category</asp:LinkButton> 
                                >
                                <asp:LinkButton ID="lbtncategorynm" runat="server" Text="" OnClick="lbtncategorynm_Click"></asp:LinkButton>
                              
                            </div>
                            <div class="devider_10px"></div>

                            <div style="width: 100%" align="center">
                                <h2 style="text-transform: uppercase; font-size:18px;">
                                    <asp:Label ID="lblalbumname" runat="server" Text=""></asp:Label></h2>
                            </div>

                            <div>
                                <asp:Label ID="lbldescrp" runat="server" Text=""></asp:Label>
                            </div>

                            <div class="wrapper">
                                <ul class="gallery" style="padding-left:60px;">

                                    <asp:DataList ID="dlistalbumimages" Width="100%" RepeatDirection="Horizontal" runat="server" RepeatColumns="3">
                                        <ItemTemplate>
                                            <li>
                                                <a id="a" runat="server" href='<%# Bind("imageurl", "~/adminpanel/{0}") %>'>
                                                <img id="img" runat="server" style="width:200px; height:180px" src='<%# Bind("thumbnail", "~/adminpanel/{0}") %>' alt="Image"></a></li>
                                            <%--<li>

                                                <img id="Image1" runat="server" class="lightbox" src='<%# Bind("imageurl", "~/adminpanel/{0}")%>' />

                                            </li>--%>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </ul>
                            </div>
                            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                            <script src="../demo/jquery.lightbox.js"></script>
                            <script>
                                // Initiate Lightbox
                                $(function () {
                                    $('.gallery a').lightbox();
                                });
                            </script>
                        </div>

                    </div>
                </div>


            </div>
            <!-- END ABOUT US CONTENT -->


        </div>
    </div>





</asp:Content>
