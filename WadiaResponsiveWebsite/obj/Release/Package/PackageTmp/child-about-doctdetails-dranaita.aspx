﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-doctdetails-dranaita.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_dranaita" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Anaitha Hegde.jpg" />
<div class="docdetails">

Dr. Anaita Udwadia Hegde


<div class="docdetailsdesg">
Hon. Consultant In Epilepsy & Neurosurgery

<br />

    MD (Ped.), MRCPCH, Fellowship Ped. Neuroscience 

</div>

</div>
</div>



                    <div class="areaofinterest">

Areas of Interest : Epilepsy, Epilepsy Surgery, Childhood Disability
</div>

<%--<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
           <li><a href="#view1">Areas of Interest</a></li>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research or publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
            <div id="view1">
                
                <p>
   Epilepsy, Epilepsy Surgery, Childhood Disability


                 
                    <br />
              
                </p>
                
            </div>
            
        
               
            
        </div>
    </div>


</div>

</div>--%>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			



        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
									<li><a href="child-about-overview.aspx">Overview</a></li>
									<li><a href="child-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="child-about-chairman-msg.aspx">Chairman's message</a></li>
									<li><a href="child-about-awards.aspx">Awards and Achievements</a></li>
									<li><a href="child-about-history.aspx">History </a>

                                        <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>



									</li>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>





		</div>





</asp:Content>
