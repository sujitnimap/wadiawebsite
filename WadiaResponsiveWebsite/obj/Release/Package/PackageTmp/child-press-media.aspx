﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-press-media.aspx.cs" Inherits="WadiaResponsiveWebsite.child_press_media" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>PRESS & MEDIA</h2>
                </div>

                <br />

                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px">

                   <%-- <div class="devider_10px"></div>--%>
                   This page is a collection of news, updates and articles related to the activities at Wadia Hospitals.
                    <div class="devider_20px"></div>


                    <div id="container">
                        <div class="pagination">
                           
		<%-- <span class="page active">1</span>
                                <a href="child-press-media1.aspx" class="page gradient">2</a>
                                <a href="child-press-media2.aspx" class="page gradient">3</a>
                                <a href="child-press-media3.aspx" class="page gradient">4</a>--%>
                            
                                <asp:DataList class="page gradient" RepeatDirection="Horizontal" runat="server" ID="dlPagerup"
                                    OnItemCommand="dlPagerup_ItemCommand">
                                    <ItemTemplate>

                                        <a id="pageno" runat="server">
                                            <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>
                                            &nbsp;&nbsp;&nbsp;
                                        </a>

                                    </ItemTemplate>
                                </asp:DataList>
                            
                        </div>

                        <div align="left"><b>News Archives</b></div>
                        <asp:DataList ID="dlistpressdata" runat="server">
                            <ItemTemplate>

                                <div class="devider_20px"></div>

                                <div>
                                    <div class="newsheading">
                                        <a target="_blank" href='<%#Eval("link") %>'>
                                            <asp:Label ID="lblheadline" runat="server" Text='<%#Eval("headline") %>'>'></asp:Label></a>
                                    </div>
                                    <div class="newsdate">
                                        <asp:Label ID="lbldate" runat="server" Text='<%#Eval("date","{0:dd/MM/yyyy}") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblsource" runat="server" Text='<%#Eval("source") %>'></asp:Label>
                                    </div>
                                </div>

                                <div class="devider_20px"></div>

                            </ItemTemplate>
                        </asp:DataList>

                        <%--<div class="pagination">
		<p>without gradient</p>
		<a href="#" class="page">first</a><a href="#" class=
		"page">2</a><a href="#" class="page">3</a><span
		class="page active">4</span><a href="#" class=
		"page">5</a><a href="#" class="page">6</a><a href="#"
		class="page">last</a>
	</div>--%>

                        <div class="pagination">
                          
		<%-- <span class="page active">1</span>
                                <a href="child-press-media1.aspx" class="page gradient">2</a>
                                <a href="child-press-media2.aspx" class="page gradient">3</a>
                                <a href="child-press-media3.aspx" class="page gradient">4</a>--%>

                            <asp:DataList class="page gradient"  RepeatDirection="Horizontal" runat="server" ID="dlPagerdown"
                                OnItemCommand="dlPagerdown_ItemCommand">
                                <ItemTemplate>

                                    <a id="pageno" runat="server">
                                        <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>
                                        &nbsp;&nbsp;&nbsp;
                                    </a>

                                </ItemTemplate>
                            </asp:DataList>
                        </div>

                    </div>


                    <br /><br /><br />

                    <b>Media Enquiries</b>

                    <div class="devider_10px"></div>


                    We welcome the opportunity to assist the media with health information requests, schedule interviews with our medical team and offer expert opinions. 
                    <div class="devider_10px"></div>


                    For more information, please get in touch with our team at 


                    <a href="#">media@wadiahospitals.org</a>


                    <div class="devider_20px"></div>



                </div>
                <!-- ABOUT US CONTENT -->





            </div>
        </div>







    </div>



</asp:Content>
