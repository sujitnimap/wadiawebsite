﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-applicationform.aspx.cs" Inherits="WadiaResponsiveWebsite.applicationform" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <%-- <script type="text/javascript" language="javascript">
        function validate() {
            var uploadcontrol = document.getElementById('<%=fldresume.ClientID%>').value;
            //Regular Expression for fileupload control.
            var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.doc|.docx|.DOC|.DOCX.pdf|.PDF|)$/;
            if (uploadcontrol.length > 0) {
                //Checks with the control value.
                if (reg.test(uploadcontrol)) {
                    return true;
                }
                else {
                    //If the condition not satisfied shows error message.
                    alert("Only .doc, docx , .pdf files are allowed!");
                    return false;
                }
            }
        } //End of function validate.
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <%-- <h2>Application Form</h2>--%>
                </div>
                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px">
                    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
                    <asp:Panel ID="appform" runat="server">
                        <div align="center">
                            <div id="applyform" style="width: 70%">
                                <div>
                                    <asp:Label ID="lblstatus" Visible="false" runat="server" Text=""></asp:Label>
                                </div>
                                <table>
                                    <tr>
                                        <td colspan="3">
                                            <div align="center" style="height: 45px">
                                                <br />
                                                <asp:Label ID="lblappfrm" runat="server" ForeColor="Black" Font-Bold="true" Font-Size="18px" Text="Application Form"></asp:Label>

                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr id="app" runat="server">
                                        <td>Position Applied for:<font color="Red">*</font> :</td>
                                        <td>
                                            <asp:Label ID="lbltitle" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td>
                                            <div class="devider_30px"></div>
                                        </td>
                                    </tr>
                                    <tr id="app2" runat="server">
                                        <td>Position Applied for:<font color="Red">*</font> :</td>
                                        <td>
                                            <asp:DropDownList CssClass="textboxcontact" ID="ddltitle" runat="server">
                                                <asp:ListItem>Medical</asp:ListItem>
                                                <asp:ListItem>Nursing</asp:ListItem>
                                                <asp:ListItem>Para Medical</asp:ListItem>
                                                <asp:ListItem>Administration</asp:ListItem>
                                                <asp:ListItem>Others</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>First Name<font color="Red">*</font> :</td>
                                        <td>
                                            <asp:TextBox ID="txtfname" CssClass="textboxcontact" runat="server"></asp:TextBox>

                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="reftxtfname" runat="server" ValidationGroup="appform" ControlToValidate="txtfname" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td>Last Name<font color="Red">*</font> :</td>
                                        <td>
                                            <asp:TextBox ID="txtlname" CssClass="textboxcontact" runat="server"></asp:TextBox>

                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="reftxtlname" runat="server" ValidationGroup="appform" ControlToValidate="txtlname" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td>Date of Birth<font color="Red">*</font> :</td>
                                        <td>
                                            <asp:TextBox ID="txtbirthday" CssClass="textboxcontact" Width="80px" runat="server"></asp:TextBox>
                                            <asp:CalendarExtender ID="txtbirthday_CalendarExtender" runat="server" TargetControlID="txtbirthday">
                                            </asp:CalendarExtender>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Gender<font color="Red">*</font> :</td>
                                        <td>
                                            <%--<asp:RadioButtonList ID="rdbgender" TextAlign="Right" runat="server">
                                            <asp:ListItem>Male</asp:ListItem>
                                            <asp:ListItem>Female</asp:ListItem>
                                        </asp:RadioButtonList>--%>
                                            <asp:RadioButton ID="rdbmale" GroupName="gender" Checked="true" Text="Male" runat="server" />
                                            <asp:RadioButton ID="rdbfmale" GroupName="gender" Text="Female" runat="server" />
                                        </td>
                                        <td>
                                            <div class="devider_30px"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Phone No. :</td>
                                        <td>
                                            <asp:TextBox ID="txtmobile" CssClass="textboxcontact" runat="server"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Email ID<font color="Red">*</font> :</td>
                                        <td>
                                            <asp:TextBox ID="txtemailid" CssClass="textboxcontact" runat="server"></asp:TextBox>

                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="reftxtemailid" runat="server" ValidationGroup="appform" ControlToValidate="txtemailid" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <%--<tr>
                                    <td>Address :</td>
                                    <td>
                                        <asp:TextBox ID="txtaddress" TextMode="MultiLine" runat="server"></asp:TextBox></td>
                                </tr>--%>
                                    <tr>
                                        <td>Upload CV<font color="Red">*</font> :</td>
                                        <td>
                                            <asp:FileUpload ID="fldresume" runat="server" />

                                        </td>
                                        <td>
                                            <div class="devider_30px">
                                                <asp:RequiredFieldValidator ID="reffldresume" runat="server" ValidationGroup="appform" ControlToValidate="fldresume" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Upload Cover Letter<font color="Red">*</font> :</td>
                                        <td>
                                            <asp:FileUpload ID="fldcover" runat="server" />
                                        </td>
                                        <td>
                                            <div class="devider_30px"></div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Captcha Verification
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="uptpnlform" runat="server">
                                                <ContentTemplate>
                                                    <asp:Image ID="imgVerificationCode" runat="server" ImageUrl="capcha.aspx" Width="160"
                                                        Height="45" EnableViewState="false" />
                                                    <br />
                                                    Can't see?
                                                <asp:LinkButton ID="lbRefresh" CssClass="bluecolor_link" runat="server" CausesValidation="False"
                                                    OnClick="lbRefresh_Click">Refresh!</asp:LinkButton>
                                                    <br />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>

                                        <td>Enter the above code <font color="Red">*</font>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_Code" CssClass="textboxcontact" Width="150px" runat="server"></asp:TextBox>
                                            <br />
                                            security code is case - sensitive<br />
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvSecurityCode" ValidationGroup="appform" runat="server" ControlToValidate="tb_Code"
                                                ErrorMessage="*" ForeColor="Red" Display="Dynamic">* </asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <div style="height: 15px"></div>
                                            <div align="center" style="padding: 15px;">
                                                <asp:Button ID="btnsubmit" runat="server" Width="80px" Height="25px" Font-Bold="true" Text="Submit" ValidationGroup="appform" OnClick="btnsubmit_Click" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>

                </div>
                <!-- ABOUT US CONTENT -->





            </div>
        </div>







    </div>


</asp:Content>
