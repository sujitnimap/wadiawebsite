﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="Reproductive-and-Infertility-clinic-form.aspx.cs" Inherits="WadiaResponsiveWebsite.Reproductive_and_Infertility_clinic_form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ToolkitScriptManager runat="server"></asp:ToolkitScriptManager>
    <asp:ValidationSummary ID="vsCMEReg" runat="server" DisplayMode="BulletList"
        ShowSummary="false" ShowMessageBox="true" ValidationGroup="vgCME" />
    <div class="row block05">
        <div class="col-full">
            <div class="wrap-col">

                <div style="border: 1px solid #ddd; padding: 10px 20px 20px 10px; border-radius: 4px;">
                    <div class="heading">
                        <h2>Reproductive and Infertility Clinic Form</h2>
                    </div>
                    <br />

                    <table>                       
                        <tr>
                            <td> Patient Name </td>
                            <td>
                                <asp:TextBox ID="txtFirstName" CssClass="textboxcontactcme"  MaxLength="30" required="" runat="server" placeholder="First Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvFirstName" ValidationGroup="vgCME" ControlToValidate="txtFirstName" runat="server" ForeColor="Red" ErrorMessage="Please Enter First Name">*</asp:RequiredFieldValidator>
                           
                                &nbsp;&nbsp;
                           
                                <asp:TextBox ID="txtLastName" CssClass="textboxcontactcme"  MaxLength="30" required="" runat="server" placeholder="Last Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvLastName" ValidationGroup="vgCME" ControlToValidate="txtLastName" runat="server" ForeColor="Red" ErrorMessage="Please Enter Last Name">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>OPD Number</td>
                            <td>
                                <asp:TextBox ID="txtOPDNo" CssClass="textboxcontactcme" MaxLength="20" required="" runat="server" placeholder="OPD Number*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'OPD Number'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvOPDNo" ValidationGroup="vgCME" ControlToValidate="txtOPDNo" runat="server" ForeColor="Red" ErrorMessage="Please Enter OPD Number">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>
                                <asp:TextBox ID="txtAddress" TextMode="MultiLine" MaxLength="500" CssClass="textboxcontactcme" runat="server" placeholder="Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address'">
                                </asp:TextBox>

                                <asp:RequiredFieldValidator ID="rfvAddress" ValidationGroup="vgCME" ControlToValidate="txtAddress" runat="server" ForeColor="Red" ErrorMessage="Please Enter Address">*</asp:RequiredFieldValidator>
                            
                            </td>
                        </tr>
                        <tr>
                            <td>Mobile Number</td>
                            <td>
                                <asp:TextBox ID="txtMobileNo" CssClass="textboxcontactcme" MaxLength="10" required=""
                                    runat="server" placeholder="Mobile Number*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile Number'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvMobileNo" ValidationGroup="vgCME"
                                    ControlToValidate="txtMobileNo" ForeColor="Red" runat="server" ErrorMessage="Please enter your Mobile Number">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revMobileNo" Display="None" ValidationGroup="vgCME" ValidationExpression="^[0-9]{10}$"
                                    ControlToValidate="txtMobileNo" SetFocusOnError="true" runat="server" ErrorMessage="Enter 10 digit numeric value in Contact Number"></asp:RegularExpressionValidator>
                            </td>
                        </tr>

                        <tr>
                            <td>Age</td>
                            <td>
                                <asp:TextBox ID="txtAge" CssClass="textboxcontactcme" required="" MaxLength="2" runat="server" placeholder="Age*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Age'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAge" ValidationGroup="vgCME"
                                    ControlToValidate="txtAge" ForeColor="Red" runat="server" ErrorMessage="Please enter your Age">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revAge" Display="None" ValidationGroup="vgCME" ValidationExpression="^[0-9]{2}$"
                                    ControlToValidate="txtAge" SetFocusOnError="true" runat="server" ErrorMessage="Enter numeric value in Age"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Amount</td>
                            <td>
                                <asp:TextBox ID="txtAmount" CssClass="textboxcontactcme" required="" MaxLength="10" runat="server" placeholder="Amount*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Amount'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAmount" ValidationGroup="vgCME"
                                    ControlToValidate="txtAmount" ForeColor="Red" runat="server" ErrorMessage="Please enter Amount">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revAmount" Display="None" ValidationGroup="vgCME" ValidationExpression="^[0-9]*$"
                                    ControlToValidate="txtAmount" SetFocusOnError="true" runat="server" ErrorMessage="Enter numeric value in Amount"></asp:RegularExpressionValidator>
                           </td>
                        </tr>
                    </table>
                    
                    <br />
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    <br />
                    <%--<div class="devider_20px"></div>--%>

                    <asp:Button ID="btnPayment" Text="Pay now" ValidationGroup="vgCME" CssClass="btn" runat="server" OnClick="btnPayment_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>