﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Education-Medical-courses.aspx.cs" Inherits="WadiaResponsiveWebsite.women_medical_courses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					       <div class="heading">
                <h2>COURsES AVAILABLE</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<h1>1. Full time Courses</h1>
<div class="devider_20px"></div>
<div style="line-height:20px; text-align:center; width:100%">

<table align="center" width="80%"cellspacing="0" cellpadding="0" style="border: 1px solid #666;">
  <tr>
    <td height="40" style="border: 1px solid #666; padding:5px"><b>Sr. No.</b></td>
    <td style="border: 1px solid #666; padding:5px"><b>Name of the Course</b> </td>
    <td style="border: 1px solid #666; padding:5px"><b>Duration</b></td>
    <td style="border: 1px solid #666; padding:5px"><b>No. of seats</b></td>
    <td style="border: 1px solid #666; padding:5px"><b>Eligibility</b> </td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">1</td>
    <td style="border: 1px solid #666; padding:5px">MD</td>
    <td style="border: 1px solid #666; padding:5px">3 Years</td>
    <td style="border: 1px solid #666; padding:5px">12</td>
    <td style="border: 1px solid #666; padding:5px">M.B.B.S.</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">2</td>
    <td style="border: 1px solid #666; padding:5px">DGO</td>
    <td style="border: 1px solid #666; padding:5px">2 Years</td>
    <td style="border: 1px solid #666; padding:5px">12</td>
    <td style="border: 1px solid #666; padding:5px">M.B.B.S.</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">3</td>
    <td style="border: 1px solid #666; padding:5px">CPS DGO</td>
    <td style="border: 1px solid #666; padding:5px">2 Years</td>
    <td style="border: 1px solid #666; padding:5px">9</td>
    <td style="border: 1px solid #666; padding:5px">M.B.B.S.</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">4</td>
    <td style="border: 1px solid #666; padding:5px">CPS FCPS</td>
    <td style="border: 1px solid #666; padding:5px">1 Year</td>
    <td style="border: 1px solid #666; padding:5px">6</td>
    <td style="border: 1px solid #666; padding:5px">M.B.B.S.</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">5</td>
    <td style="border: 1px solid #666; padding:5px">CGO</td>
    <td style="border: 1px solid #666; padding:5px">1 Month</td>
    <td style="border: 1px solid #666; padding:5px">70</td>
    <td style="border: 1px solid #666; padding:5px">M.B.B.S.</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">6</td>
    <td style="border: 1px solid #666; padding:5px">Sonography & Foetal Monitoring</td>
    <td style="border: 1px solid #666; padding:5px">1 Month</td>
    <td style="border: 1px solid #666; padding:5px">30</td>
    <td style="border: 1px solid #666; padding:5px">M.B.B.S.</td>
  </tr>
</table>
<div class="devider_20px"></div>



<div style="text-align:left"><h1>2. Part time Courses</h1></div>

<div class="devider_20px"></div>
  


<table cellspacing="0" cellpadding="0" style="border: 1px solid #666;">
  <tr>
    <td height="43"  style="border: 1px solid #666; padding:5px; width:52px;"><b>Sr. No.</b></td>
    <td style="border: 1px solid #666; padding:5px; width: 145px"><b>Name of the Course</b> </td>
    <td style="border: 1px solid #666; padding:5px; width: 174px"><b>Duration</b></td>
    <td style="border: 1px solid #666; padding:5px"><b>Eligibility</b></td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 52px">1</td>
    <td style="border: 1px solid #666; padding:5px; width: 145px">USG</td>
    <td style="border: 1px solid #666; padding:5px; width: 174px">1&nbsp; month</td>
    <td style="border: 1px solid #666; padding:5px;">MD/DGO</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 52px">2</td>
    <td style="border: 1px solid #666; padding:5px; width: 145px">USG Observer</td>
    <td style="border: 1px solid #666; padding:5px; width: 174px">15 days </td>
    <td style="border: 1px solid #666; padding:5px;">MEDICAL POSTGRADUATES</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 52px">3</td>
    <td style="border: 1px solid #666; padding:5px; width: 145px">LAP TRG.</td>
    <td style="border: 1px solid #666; padding:5px; width: 174px">15 days </td>
    <td style="border: 1px solid #666; padding:5px;">MD/DGO</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 52px">4</td>
    <td style="border: 1px solid #666; padding:5px; width: 145px">MTP TRG.</td>
    <td style="border: 1px solid #666; padding:5px; width: 174px">1 Month</td>
    <td style="border: 1px solid #666; padding:5px;">MBBS</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 52px">5</td>
    <td style="border: 1px solid #666; padding:5px; width: 145px">CGO COURSE</td>
    <td style="border: 1px solid #666; padding:5px; width: 174px">1 Month</td>
    <td style="border: 1px solid #666; padding:5px;">BAMS/DAMS</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 52px">6</td>
    <td style="border: 1px solid #666; padding:5px; width: 145px">PG OBSERVER (Indian Student)</td>
    <td style="border: 1px solid #666; padding:5px; width: 174px">1 Month</td>
    <td style="border: 1px solid #666; padding:5px;">MBBS</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 52px">7</td>
    <td style="border: 1px solid #666; padding:5px; width: 145px">PG OBSERVER (International Student)</td>
    <td style="border: 1px solid #666; padding:5px; width: 174px">1 Month</td>
    <td style="border: 1px solid #666; padding:5px;">MBBS</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 52px">8</td>
    <td style="border: 1px solid #666; padding:5px; width: 145px">OT Technician</td>
    <td style="border: 1px solid #666; padding:5px; width: 174px">7 Months (once a week for 8 hours)</td>
    <td style="border: 1px solid #666; padding:5px;">12th pass with 2 years of work experience as OT Technician</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 52px">9</td>
    <td style="border: 1px solid #666; padding:5px; width: 145px">Medical Lab Technician</td>
    <td style="border: 1px solid #666; padding:5px; width: 174px">7 Months (once a week for 8 hours)</td>
    <td style="border: 1px solid #666; padding:5px;">12th pass with 2 years of work experience as Medical Lab Technician
	</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 52px">10</td>
    <td style="border: 1px solid #666; padding:5px; width: 145px">Dialysis Technician</td>
    <td style="border: 1px solid #666; padding:5px; width: 174px">7 Months (once a week for 8 hours)</td>
    <td style="border: 1px solid #666; padding:5px;">12th pass with 2 years of work experience as Dialysis Technician
</td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px; width: 52px">11</td>
    <td style="border: 1px solid #666; padding:5px; width: 145px">Radiology Technician </td>
    <td style="border: 1px solid #666; padding:5px; width: 174px">7 Months (once a week for 8 hours)</td>
    <td style="border: 1px solid #666; padding:5px;">12th pass with 2 years of work experience as Radiology Technician
</td>
  </tr>
 
</table>



</div>

<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>MEDICAL</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						 <li><a href="women-medical-overview.aspx">	Overview  </a></li>
                         <li><a href="women-medical-directormsg.aspx"> Directors message </a></li>
                         <li><a href="women-medical-courses.aspx"> 	Courses available</a></li>
                         <li><a href="women-contact-us.aspx">	Contact Us  </a></li>


                                     <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
					


				</div>
			</div>



		</div>





</asp:Content>
