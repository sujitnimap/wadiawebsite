﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class child_testimonials : System.Web.UI.Page
    {
       
        static string flag = "c";

        string Categorynm = "";
       

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(Request.QueryString["catnm"]))
            {
                ViewState["catnm"] = Convert.ToString(Request.QueryString["catnm"]);

                Categorynm = Convert.ToString(ViewState["catnm"]);
            }


            if (!IsPostBack)
            {
                if (Categorynm != "")
                {
                    if (Categorynm == "Video testimonials")
                    {
                        videotestimonialdata(1, "Video testimonials");
                    }
                    else
                    {
                        testimonialdata(1, Categorynm);
                    }
                }
                else
                {
                    testimonialdata(1, "Contributors");
                }
            }


            if (Categorynm == "Video testimonials")
            {

                pnlvideotestimonial.Visible = true;
                pnlalltestimonials.Visible = false;

            }
            else
            {
                pnlalltestimonials.Visible = true;
                pnlvideotestimonial.Visible = false;
            }


        }


        public void testimonialdata(int currentPage,string categorynm)
        {
            int pageSize = 5;
            int _TotalRowCount = 0;

            string _ConStr = ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(_ConStr))
            {

                SqlCommand cmd = new SqlCommand("sp_GetTestimonialData", con);
                cmd.CommandType = CommandType.StoredProcedure;

                int startRowNumber = ((currentPage - 1) * pageSize) + 1;

                cmd.Parameters.AddWithValue("@StartIndex", startRowNumber);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.Parameters.AddWithValue("@categorynm", categorynm);

                SqlParameter parTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                parTotalCount.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parTotalCount);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                _TotalRowCount = Convert.ToInt32(parTotalCount.Value);

                dtlisttestimonials.DataSource = ds;
                dtlisttestimonials.DataBind();


                generatePager(_TotalRowCount, pageSize, currentPage);

            }
        }

        public void generatePager(int totalRowCount, int pageSize, int currentPage)
        {
            int totalLinkInPage = 5;
            int totalPageCount = (int)Math.Ceiling((decimal)totalRowCount / pageSize);

            int startPageLink = Math.Max(currentPage - (int)Math.Floor((decimal)totalLinkInPage / 2), 1);
            int lastPageLink = Math.Min(startPageLink + totalLinkInPage - 1, totalPageCount);

            if ((startPageLink + totalLinkInPage - 1) > totalPageCount)
            {
                lastPageLink = Math.Min(currentPage + (int)Math.Floor((decimal)totalLinkInPage / 2), totalPageCount);
                startPageLink = Math.Max(lastPageLink - totalLinkInPage + 1, 1);
            }

            List<ListItem> pageLinkContainer = new List<ListItem>();

            if (startPageLink != 1)
                pageLinkContainer.Add(new ListItem("First", "1", currentPage != 1));
            for (int i = startPageLink; i <= lastPageLink; i++)
            {
                pageLinkContainer.Add(new ListItem(i.ToString(), i.ToString(), currentPage != i));
            }
            

            dlPagerup.DataSource = pageLinkContainer;
            dlPagerup.DataBind();

            dlPagerdown.DataSource = pageLinkContainer;
            dlPagerdown.DataBind();
        }


        protected void dlPagerup_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "PageNo")
            {
                testimonialdata(Convert.ToInt32(e.CommandArgument), Categorynm);
            }
        }

        protected void dlPagerdown_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "PageNo")
            {
                testimonialdata(Convert.ToInt32(e.CommandArgument), Categorynm);
            }
        }



        public void videotestimonialdata(int currentPage, string categorynm)
        {
            int pageSize = 5;
            int _TotalRowCount = 0;

            string _ConStr = ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(_ConStr))
            {

                SqlCommand cmd = new SqlCommand("sp_GetVideoTestimonialData", con);
                cmd.CommandType = CommandType.StoredProcedure;

                int startRowNumber = ((currentPage - 1) * pageSize) + 1;

                cmd.Parameters.AddWithValue("@StartIndex", startRowNumber);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.Parameters.AddWithValue("@categorynm", categorynm);

                SqlParameter parTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                parTotalCount.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parTotalCount);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                _TotalRowCount = Convert.ToInt32(parTotalCount.Value);

                dtlistvideotestimonials.DataSource = ds;
                dtlistvideotestimonials.DataBind();

                generatevideoPager(_TotalRowCount, pageSize, currentPage);

            }

        }

        public void generatevideoPager(int totalRowCount, int pageSize, int currentPage)
        {
            int totalLinkInPage = 5;
            int totalPageCount = (int)Math.Ceiling((decimal)totalRowCount / pageSize);

            int startPageLink = Math.Max(currentPage - (int)Math.Floor((decimal)totalLinkInPage / 2), 1);
            int lastPageLink = Math.Min(startPageLink + totalLinkInPage - 1, totalPageCount);

            if ((startPageLink + totalLinkInPage - 1) > totalPageCount)
            {
                lastPageLink = Math.Min(currentPage + (int)Math.Floor((decimal)totalLinkInPage / 2), totalPageCount);
                startPageLink = Math.Max(lastPageLink - totalLinkInPage + 1, 1);
            }

            List<ListItem> pageLinkContainer = new List<ListItem>();

            if (startPageLink != 1)
                pageLinkContainer.Add(new ListItem("First", "1", currentPage != 1));
            for (int i = startPageLink; i <= lastPageLink; i++)
            {
                pageLinkContainer.Add(new ListItem(i.ToString(), i.ToString(), currentPage != i));
            }


            ddlVideoPagerup.DataSource = pageLinkContainer;
            ddlVideoPagerup.DataBind();

            ddlVideoPagerdown.DataSource = pageLinkContainer;
            ddlVideoPagerdown.DataBind();
        }

        protected void ddlVideoPagerup_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "PageNo")
            {
                videotestimonialdata(Convert.ToInt32(e.CommandArgument), "Video testimonials");
            }
        }

        protected void ddlVideoPagerdown_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "PageNo")
            {
                videotestimonialdata(Convert.ToInt32(e.CommandArgument), "Video testimonials");
            }
        }

        protected void lnbtnArchives_Click(object sender, EventArgs e)
        {
            testimonialdata(1, "Archives"); ;
        }

        protected void lnbtnPatients_Click(object sender, EventArgs e)
        {
            testimonialdata(1, "Patients");
        }

        protected void lnbtnStudents_Click(object sender, EventArgs e)
        {
            testimonialdata(1, "Students");
        }

        protected void lnbtnContributors_Click(object sender, EventArgs e)
        {
            testimonialdata(1, "Contributors");
        }

        protected void lnbtnVideotestimonials_Click(object sender, EventArgs e)
        {
            videotestimonialdata(1, "Video testimonials");
        }
    }
}