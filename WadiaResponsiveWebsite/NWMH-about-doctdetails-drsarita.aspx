﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-about-doctdetails-drsarita.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_doctdetails_drsarita" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Sarita Bhalerao.jpg" />
<div class="docdetails">
Dr. Sarita Bhalerao



<div class="docdetailsdesg">
Hon. Clinical Assistant
   <br />
MD, DGO, FCPS, DNB, DFP, FRCOG, Diploma in Pelvic Endoscopy


</div>

</div>
</div>

     <div class="areaofinterest">

Areas of Interest : Ultrasonography in Obstetrics and Gynaecology, Gynaecologic Oncology, Gynaecologic Endoscopy
</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
    •	FOGSI Ethicon Travelling Fellowship (1997) for specialized training in Infertility & Cancer Detection
<br />•	Young Gynaecologists Award (2000) from The Asea – Oceana Federation of Obstetrician & Gynaecologist.
<br />•	Hargobind Foundation Fellowship (2003) for specialized training in Pelvic Endoscopy from Kiel, Germany.

<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
    •	Vice President of The Association of Medical Women in India, Mumbai Branch
<br />•	Joint Clinical Secretary the Mumbai Obstetric and Gynaecological Society
<br />•	Joint Secretary of Indian Menopause Society (Mumbai chapter)

<br /><br />
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Dealing with nausea and vomiting in pregnancy
<br />•	Diet In Pregnancy
<br />•	Cord blood banking Information for parents
<br />•	Yoga in Pregnancy
<br />•	Polycystic Ovarian Syndrome

<br /><br />
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			





				</div>
			

</asp:Content>
