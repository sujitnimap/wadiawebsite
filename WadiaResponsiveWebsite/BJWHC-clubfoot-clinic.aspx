﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-clubfoot-clinic.aspx.cs" Inherits="WadiaResponsiveWebsite.child_clubfoot_clinic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Clubfoot Clinic  </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
In July 2013, Bai Jerbai Hospital for Children joined hands with the Miracle Feet Foundation and CURE International India to introduce a Clubfoot 
                Clinic at the hospital. The main aim of this clinic is to treat clubfoot deformities in new born children so as to prevent further 
                
                disabilities during the course of their lives. The Ponseti method used to treat our patients involves gentle manipulation and serial 
                casting of the deformed foot for a period of four to six weeks to achieve a complete correction. This method has long-lasting effects and gives 
                the child the opportunity to have a healthy, able body. The hospital acts as a nodal centre for the treatment of this disease and offers 
                affordable healthcare to every child who needs it. 

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
         
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

      

		</div>







</asp:Content>
