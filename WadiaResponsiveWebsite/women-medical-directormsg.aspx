﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-medical-directormsg.aspx.cs" Inherits="WadiaResponsiveWebsite.women_medical_directormsg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					       <div class="heading">
                <h2>Medical Director’s Message</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">


     <div class="post-container"><img class="avatar" src="images/doc/Dr Jassawalla.jpg"><span class="post">

        In a developing country like India, where maternal mortality rates are still very high, the need of the hour is to have healthcare of international standards available to all, at inexpensive costs. Here at The Nowrosjee Wadia Maternity Hospital, known to be one of the country’s best maternity hospitals, it is our constant endeavour to meet this primary need for as many women as possible. We take it upon ourselves to not only provide affordable world class Obstetric as well as Gynaecological care to women across sections of society, but also to provide our society with highly skilled and reliable doctors, trained by us, who can carry forward our mission and philosophy, and provide the best possible treatment to the women who need it the most. 

<br /><br />

In this effort, we run several full time as well as part-time training courses for aspiring medical professionals. Our courses cater to students for both medical and nursing fields. 

<br /><br />

Our faculty comprises of highly specialized, renowned doctors with years of experience behind them. Having presented at seminars across the world and published several articles, journals and books, our faculty members are constantly working towards updating their knowledge and skill sets to impart the highest quality of education by international standards. 
<br /><br />


<b>Dr. M. J. Jassawala </b>
<br />
Medical Director
<br />
Nowrosjee Wadia Maternity Hospital

<br /> </span>

       </div>

</div>










<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>MEDICAL</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						 <li><a href="women-medical-overview.aspx">	Overview  </a></li>
                         <li><a href="women-medical-directormsg.aspx"> Directors message </a></li>
                         <li><a href="women-medical-courses.aspx"> 	Courses available</a></li>
                         <li><a href="women-contact-us.aspx">	Contact Us  </a></li>


                                     <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
					


				</div>
			</div>



		</div>


</asp:Content>
