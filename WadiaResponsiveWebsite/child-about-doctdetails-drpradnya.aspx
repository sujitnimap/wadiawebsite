﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-doctdetails-drpradnya.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drpradnya" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
		<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Pradnya Bendre.jpg" />
<div class="docdetails">
Dr. Pradnya Bendre
<div class="docdetailsdesg">
Professor and Unit Head
            <br />
MS, MCh, DNB

</div>

</div>
</div>

                    <div class="areaofinterest">

Areas of Interest : Paediatric Urology
</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Research and publications</a></li>
            <%--<li><a href="#view4">Research or publications</a></li>--%>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
 •	Dr. Bendre has successfully performed surgery on conjoint twins and has also successfully conducted surgeries involving complex anomalies
<br />•	Has received ‘Best Research Paper’ Award thrice
<br />•	Awarded National Merit scholarship during school, college and university super speciality training




<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
   •	Our Experience With Extravesical Reimplantation: Review Of 17 Cases Sandeep Hambarde*, Pradnya Bendre** Bombay Hospital Journal, Vol. 54, No. 1, 2012
<br /><br />
•	Antenatally Diagnosed Bigonal Torsion With Midgut 
 Volvulus Sandeep Hambarde*, Pradnya Bendre* Bombay Hospital Journal, Vol. 54,No. 1, 2012.
                    <br /><br />

•	<b>Diphallus With Anorectal Malformation-Case Report</b>  Original Research Article. Journal Of Paediatric Surgery, Volume 45, Issue 3, 
<br /><br />

•	Pancreatic Cystic Mass In Infancy-Rare Case Report Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje .Bombay Hospital Journal, 2012.
<br /><br />


•	<b>Cysticercosis Presenting As Rare Cause Of Midline Swelling</b> Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje Bombay Hospital Journal, 2012
<br /><br />

•	<b>Congenital Urethrocutaneous Fistula Rare Case Report</b> Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje Bombay Hospital Journal, 2012
<br /><br />


•	<b>Excellent Results In Intralesional Bleomycin In Cystic Hygroma</b> Appendix Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje Bombay Hospital Journal, 2012.
<br /><br />

•	Unusual Foreign Body In Vermiform Appendix Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje Bombay Hospital Journal, 2012.
<br /><br />

•	<b>Giant Hiatus Hernia With Intrathoracic Stomach</b> Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje Bombay Hospital Journal, 2012.
<br /><br />

•	Intralesional Bleomycin Injection For Cystic Hygroma In A Neonate Bombay Hospital Journal, Vol. 53, Special Issue, 2011 Sandeep Hambarde, Pradnya Bendre, Rajeev Redkar, Pankaj Mishra,Ramdas Nagargoje.
<br /><br />•	Recurrent Spontaneous Pneumothorax In A Child.Sri Lanka Journal Of Critical Care.Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje, Madhavi Vinayak Thakur
<br /><br />•	Anovestibular Fistula In Otherwise Normal Anorectum. African Journal Of Paediatric Surgery Sandeep Hambarde, Pradnya Bendre, Rajeev Redkar
Year: 2011   Volume : 8   Issue : 1   Page : 117-118.

<br /><br />•	Comparison Of Topical Application Of Fluticasone Alone And With Hyaluronic Acid For Phimosis.Sandeep Hambarde, Pradnya Bendre, Rajeev Redkar.Bombay Hospital Journal, Vol.53, No.2,2011

<br /><br />•	Anterior Urethral Diverticulum-Case Report- Accepted For Publication In African Journal Of  Paediatric Surgery

<br /><br />•	Isolated Anterior Urethral Diverticulum. Bombay Hospital Journal, Vol. 53, No. 2 , 2011 269.Sandeep Hambarde, Pradnya Bendre, Rajeev Redkar

<br /><br />•	Isolated Microgastria .Bombay Hospital Journal, Vol. 53, No. 4, 2011sandeep Hambarde, Pradnya Bendre, Ramdas Nagargoje

<br /><br />•	Foregut Duplication Cyst Presenting As Lingual Swelling: Case Report And Review Of Literature Natl J Maxillofac Surg. 2011 Jan-Jun; 2(1): 2–5 .Sandeep Hambarde, Pradnya Bendre

<br /><br />•	Jejunal Duplication Cyst
Sandeep Hambarde, Pradnya Bendre Annals Of Nigerian Medicine  Jul- Dec 2010 Volume: 4 Issue : 2 Page : 69-70.

<br /><br />•	Adrenal Masses Associated With Beckwith Wiedemann Syndrome In The New Born  Devendra V Taide, Pradnya S Bendre, Rajeev Redkar, Sandeep Hambarde. African Journal Of Paediatric Surgery Year : 2010 Volume : Issue : 3  7 Page : 209-210 , 10.

     
<br /><br />•	 Propranolol For Infantile Hemangioma
Sandeep Hambarde, Pradnya Bendre, Ramdas Nagargoje, Devendra Taide .Annals Of Nijerian Medicine July-Dec 2010 :4 :2 68.


<br /><br />•	<b>Ileal   Atresia With D</b> Asplenia Syndrome With Colonic Atresias And Hemifacial Hypoplasia Minakshi Sham*, Tejaswini Kamat***, Pradnya Bendre  
     Bombay Hospital Journal, Case Report. <b>Volume 48 No. 01, January 2006</b>
 
<br /><br />•	Isolated Prostatic Utricle.,Mukunda Ramachandra, Pradnya S Bendre, Rajeev G Redkar, Devendra V     Taide J Indian Assoc Pediatr Surg. 2009 Oct;14(4):228-9.


<br /><br />•	Intralesional Bleomycin Injection For Cystic Hygroma In A Neonate  Bombay Hospital Journal, Vol. 53, Special Issue, 2011 Sandeep Hambarde, Pradnya Bendre, Rajeev Redkar, Pankaj Mishra, Ramdas Nagargoje.

<br /><br />•	Anovestibular Fistula In Otherwise Normal Anorectum..
African Journal Of Paediatric Surgery 
Sandeep Hambarde, Pradnya Bendre, Rajeev Redkar
Year: 2011   Volume : 8   Issue : 1   Page : 117-118.

<br /><br />•	Comparison Of Topical Application Of Fluticasone Alone And With Hyaluronic Acid For Phimosis.
Sandeep Hambarde, Pradnya Bendre, Rajeev Redkar.
Bombay Hospital Journal, Vol.53, No.2,2011


<br /><br />•	Isolated Anterior Urethral Diverticulum.
Bombay Hospital Journal, Vol. 53, No. 2 , 2011 269.
Sandeep Hambarde, Pradnya Bendre, Rajeev Redkar

<br /><br />•	Isolated Microgastria 
Bombay Hospital Journal, Vol. 53, No. 4, 2011
Sandeep Hambarde, Pradnya Bendre, Ramdas Nagargoje

<br /><br />•	Foregut Duplication Cyst Presenting As Lingual Swelling: Case Report And Review Of Literature 
Natl J Maxillofac Surg. 2011 Jan-Jun; 2(1): 2–5 
Sandeep Hambarde, Pradnya Bendre


<br /><br />•	Recurrent Spontaneous Pneumothorax In A Child.
Sri Lanka Journal Of Critical Care, Vol.2(1) 2011 29-32
Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje, Madhavi Vinayak Thakur
  
<br /><br />•	Unusual Presentation Of Paediatric Perineal Trauma 
Sandeep Hambarde, Pradnya Suhas Bendre  
Sri Lanka Journal Of Child Health 
Volume 40 December 2011 No. 4 ,Pg 185 2012

     
<br /><br />•	Our Experience With  Extravesical  Reimplantation  : Review Of 17 Cases 
Sandeep Hambarde*, Pradnya Bendre**
Bombay Hospital Journal, Vol. 54, No. 1, 2012

<br /><br />•	Antenatally  Diagnosed  Bigonal  Torsion  With  Midgut Volvulus 
Sandeep Hambarde*, Pradnya Bendre*
Bombay Hospital  Journal, Vol. 54,No. 1 ,2012.


<br /><br />•	Isolated Meconium Ileus
Sandeep Hambarde, Pradnya Bendre
Bombay Hospital Journal, Volume 54 No. 01, January 2012


<br /><br />•	Unusual Site Of Foreign Body In Appendix- 
Sandeep Hambarde, Pradnya Bendre
Bombay Hospital Journal, Volume 55 No. 03,October. 2013

<br /><br />•	Giant Hiatus Hernia With Intrathoracic Stomach
Ramdas Nagargoje, Pradnya Bendre
Bombay Hospital Journal, Volume 55 No. 02,April. 2013


<br /><br />•	Congenital Urethrocutaneous Fistula Rare Case Report  
Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje 
Bombay Hospital Journal, 2012

<br /><br />•	Unusual Foreign Body In Vermiform Appendix
Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje 
Bombay Hospital Journal, 2012.

<br /><br />•	Giant Hiatus Hernia With Intrathoracic Stomach
Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje 
Bombay Hospital Journal, 2012.

<br /><br />•	<b>Pancreatic Cystic Mass In Infancy-Rare Case Report</b>
Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje .
Bombay Hospital Journal, 2012.

<br /><br />•	<b>Cysticercosis Presenting As Rare Cause Of Midline Swelling</b>
Pradnya Suhas Bendre, Ramdas Dagdu Nagargoje 
Bombay Hospital Journal, 2012

    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       

            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			



		</div>




</asp:Content>
