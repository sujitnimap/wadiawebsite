﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-event.aspx.cs" Inherits="WadiaResponsiveWebsite.child_event" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>EVENT</h2>
                </div>

                <asp:DataList ID="dlisteventdata" Width="100%" runat="server" BorderColor="#666666"
                    BorderStyle="None" BorderWidth="2px" CellPadding="3" CellSpacing="2" Font-Names="Verdana" Font-Size="Small" GridLines="Both" RepeatColumns="1">
                    <ItemTemplate>
                        <div class="devider_10px"></div>
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lbleventname" Font-Bold="true" Font-Size="14px" Font-Names="Arial" runat="server" Text='<%#Eval("eventname") %>'></asp:Label></td>
                            
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="devider_20px"></div>
                                    <asp:Label ID="lbldescription" runat="server" Text='<%#Eval("description") %>'></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td style="width: 20%">Date </td>
                                <td>:&nbsp;<asp:Label ID="lblstartdate" runat="server" Text='<%#Eval("startdate") %>'></asp:Label></td>
                            </tr>
                           <%-- <tr>
                                <td style="width: 20%">End Date </td>
                                <td>:&nbsp;<asp:Label ID="lblenddate" runat="server" Text='<%#Eval("enddate") %>'></asp:Label></td>
                            </tr>--%>
                            <tr>
                                <td style="width: 20%">Start Time </td>
                                <td>:&nbsp;<asp:Label ID="lblstarttime" runat="server" Text='<%#Eval("starttime") %>'></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 20%">End Time </td>
                                <td>:&nbsp;<asp:Label ID="lblendtime" runat="server" Text='<%#Eval("endtime") %>'></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 20%">Location </td>
                                <td>:&nbsp;<asp:Label ID="lbllocation" runat="server" Text='<%#Eval("location") %>'></asp:Label></td>
                            </tr>

                        </table>

                    </ItemTemplate>
                </asp:DataList>
            </div>
        </div>


    </div>

</asp:Content>
