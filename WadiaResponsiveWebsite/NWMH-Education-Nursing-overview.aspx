﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Education-Nursing-overview.aspx.cs" Inherits="WadiaResponsiveWebsite.women_nursing_overview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                     <div class="heading">
                  <h2>NURSING </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->


<div style="line-height:20px">

  The Department of Nursing at the Nowrosjee Wadia Maternity Hospital works on the principles of care, empathy and safety. To us, our patients come first and through a collaborative, multidisciplinary approach, we aim to provide the highest quality of holistic care to patients from diverse backgrounds. We understand that undergoing treatment can be traumatic for our patients and we do everything in our capacity to make the environment as warm and comfortable as we can for them. In addition, our services extend to not only educating our patients and their families, but also carry out professional training for students of nursing.
<br /><br />

<b>The professional nursing practice at the NWMH Hospital ensures that:</b>
    <br /><br />

    <div style="padding-left:25px">


•	The nurse assesses the patient and family for specific care needs to meet optimal outcomes.
<br /><br />
        •	The nurse identifies the amount, degree and level of nursing care needed to achieve those outcomes and manages the nursing resources to meet those needs.
<br /><br />
        •	The nurse recognizes the contributions of other disciplines as an integral part of patient care delivery.
<br /><br />
        •	The nurse provides every patient and family with complete and understandable information about care and aftercare through individual contracts, group programs and multimedia materials.
</div>
    <br />




</div>

<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>Nursing</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						  <li><a href="women-nursing-overview.aspx">	Overview  </a></li>
                        <li><a href="women-nursing-school.aspx"> 	Nursing Department</a></li>
                        <li><a href="women-nursing-courses.aspx"> 	Courses available</a></li>
                         <li><a href="women-contact-us.aspx">	Contact Us  </a></li>


                                     <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
			



				</div>
			</div>



		</div>







</asp:Content>
