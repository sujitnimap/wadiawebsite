﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-gallery-albums.aspx.cs" Inherits="WadiaResponsiveWebsite.child_gallery_album" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>GALLERY</h2>
                </div>
                <br />
                <div>
                    <asp:LinkButton ID="lbtnCategory" runat="server" OnClick="lbtnCategory_Click">Main Category</asp:LinkButton>
                    >
                    <asp:LinkButton ID="lbtncategorynm" runat="server"  Text="" OnClick="lbtncategorynm_Click"></asp:LinkButton>
                              
                </div>
                <asp:DataList ID="dlistalbumdtl" Width="100%" runat="server" RepeatColumns="3">
                    <ItemTemplate>
                        <div align="center">
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <div align="center">
                                            <asp:ImageButton ID="imgbalbumtnthumnail" CommandArgument='<%#Eval("id") %>' Width="200px" ImageUrl='<%#("/adminpanel/" + Eval("thumbnail")) %>' OnClick="imgbalbumtnthumnail_Click" Height="180px" runat="server" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div align="center">
                                            <asp:Label ID="lblcat" Font-Bold="true" runat="server" Text='<%#Eval("albumname") %>'></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ItemTemplate>
                </asp:DataList>



            </div>
        </div>
    </div>



</asp:Content>
