﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-about-chairman-msg.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_chairman_msg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading"><h2>CHAIRMAN'S MESSAGE</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">

  “Just like the absence of sadness is not joy, the absence of disease is not health,” states The World Health Organization. It defines Health as “a state of complete physical, mental and social well-being and not merely the absence of disease or infirmity”. Keeping this in mind, we practise preventive, curative, intensive and rehabilitative methods to provide a holistic approach to ‘Health’ along with spreading awareness through health education in order to enable our patients and their families to create a conducive, sanitary environment in their homes. Therefore, our efforts extend not only to the patient’s health, safety and wellbeing but also to their families. 
    <br />
<br />
Our primary focus has always been providing women with state of the art obstetric and gynaecological treatments through innovative techniques and highly skilled doctors. For this, we are constantly learning and upgrading our knowledge as well as equipment, to ensure that our services are at par with any other national or international medical institution. As a teaching hospital, we have high standards of education, training and clinical experience and we work endlessly towards strengthening these academic and research programmes while maintaining our Quality Management Systems.
<br />
<br />
Since the inception of the Nowrosjee Wadia Maternity Hospital, it has been our relentless effort to stay true to the philanthropic tradition set by our forefathers, and help carry their dream forward from generation to generation, widening our reach with every passing year. We continue to maintain the Indigenous Patients’ Fund to help provide affordable or even free healthcare to members on the margins of our Society.
<br />
<br />
I would like to express my most sincere gratitude to all the people who have been a part of the hospital’s journey towards excellence - to all the donors, patrons and well-wishers for the encouragement and support; to the State Government and Municipal Corporation of Greater Mumbai for standing by us throughout our journey and finally, to the Doctors, Nursing Staff and the Subordinate staff for their ‘one for all and all for one’ attitude, always looking at the bigger picture and working for a greater cause. 

<br />
<br />

<b>Mr. Nusli N Wadia</b>
    <br />
Chairman<br />
    Nowrosjee Wadia Maternity Hospital
   

<br />
<br />
</div>
<!-- ABOUT US CONTENT -->

           
                    
                    
				</div>
			</div>
			

        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="women-about-overview.aspx">Overview</a></li>
									<li><a href="women-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="women-about-chairman-msg.aspx">Chairman's message</a></li>
									<%--<li><a href="women-about-awards.aspx">Awards and Achievements</a></li>--%>
									<li><a href="women-about-history.aspx">History </a>


                                      <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-nusserwanji-wadia.aspx">Nowrosjee Nusserwanji Wadia (1849 – 99)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>



			</div>
		</div>


</asp:Content>
