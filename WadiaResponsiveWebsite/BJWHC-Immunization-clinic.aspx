﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-Immunization-clinic.aspx.cs" Inherits="WadiaResponsiveWebsite.child_immunization_clinic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


  <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>IMMUNIZATION CLINIC</h2></div>
                    
                    <br />

<!-- CONTENT -->

<div style="line-height:20px">

    <strong>Overview </strong>
    <br />    <br />


Immunization is the process whereby a person is made immune or resistant to an infectious disease, typically by the administration of a vaccine. The hospital offers free of cost immunization services as recommended by EPI – Expanded Program of Immunisation of Government of India through its Well Baby Clinics, which run thrice a week.
        <br /><br />
The Indian Academy of Paediatrics recommends an immunization schedule for children from birth through 18 years of age, which ensures immunity against a range of - infections in addition to those recommended by EPI of Government of India.
        <br /><br />
<a target="_blank" href="http://www.iapindia.org/IMM%20Schedule.pdf">Click here</a> to download the Immunization schedule (PDF)  | Source: Indian Academy of Paediatrics

    <div style="height:130px"></div>



</div>
<!--  CONTENT -->


                
                    
                    
				</div>
			</div>
			


        



		</div>

    
</asp:Content>
