﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drrajeev.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drrajeev" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Rajeev Redkar.png" />
<div class="docdetails">
Dr. Rajeev Redkar
<div class="docdetailsdesg">
Asst. Hon. Pediatric Surgery
    <br />
MS, Mch, DNB, FRCS, IAS



</div>

</div>
</div>


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
           <%-- <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research or publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
      
               •	SECOND in the merit list at the Master in Chirurgie M Ch  (Paediatric Surgery) Examination held by the University of Mumbai.
<br /><br />
•	Awarded Dr L R Patel Medal for standing first amongst all the candidates. Fellowship of the College of Physicians & Surgeons Examination of the College of Physicians & Surgeons of Bombay in March 1993
<br /><br />
•	Awarded the BAI NASSERWANJI COOPER PRIZE for the BEST THESIS ON SURGICAL PROBLEM for the year 1991.
<br /><br />
•	Awarded prestigious prize of the BEST INTERN (Dr NINA DAHEJA PREMCHAND PRIZE) of the KEM Hospital for the year 1987.
<br /><br />
•	Selected for the coveted ‘DR MENINO DE SOUZA PRIZE’ by the Bombay Medical Welfare Trust for the BEST ALL ROUND MEDICAL STUDENT for the year 1987
<br /><br />
•	Awarded the K.N. BAHADURJI SCHOLARSHIP for HIGHEST MARKS IN GENERAL SURGERY in the FINAL MBBS exam.
<br /><br />
•	Awarded the R.G. DHAYAGUDE MEMORIAL PRIZE for the BEST ESSAY on a SOCIO - MEDICAL SUBJECT
<br /><br />
•	Awarded the NATIONAL TALENT SEARCH SCHOLARSHIP by NCERT, Government of India in the year 1982 on the basis of a Nationwide Competitive Examination. 
<br /><br />
•	SELECTION FOR THE INDIAN ADMINISTRATIVE SERVICE By the Government of India in which 101 candidates were selected of the 400,000 that appeared all over India.
<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
         
•	Member of the Asian Association of Paediatric Surgeons.
    <br /><br />
•	Member of the Association of Surgeons of India.
    <br /><br />
•	Member of the Indian Association of Paediatric Surgeons
    <br /><br />
•	Associate member of the British Association of Paediatric Surgeons.
    <br /><br />
•	Fellow of the Royal College of Surgeons of Edinburgh.
    <br /><br />
•	Fellow of the Royal College of Surgeons of Glasgow.
    <br /><br />
•	Fellow of the College of Physicians and Surgeons of Bombay.
    <br /><br />
•	Patron of the Indian Education Society. Dadar, Mumbai

    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                


•	Recent Approach To Portal Hypertension In Children, 2006
<br /><br />
•	Cisapride reduces neonatal postoperative ileus: randomized placebo controlled trial – Trial Report, 1997
<br /><br />
•	Colloidal CA in 11 yr old child JPMG, 1993
<br /><br />
•	Posterior Cervical Cytic Hygroma JPMG, 1992
<br /><br />
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
        




		</div>




</asp:Content>
