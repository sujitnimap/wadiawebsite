﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-hepato.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_hepato" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PAEDIATRIC LIVER CLINIC </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
          
The Hospital offers specialised services for children suffering from liver diseases. 
                Though only few liver diseases can be cured as of now, many can be prevented with early diagnosis and proper management. 
                It is a multidisciplinary clinic run by liver specialists, hepatologists and hepatic surgeons with collaboration from genetists, 
                histopathologists, laboratory and imaging specialists.  <br />
 <br />


                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
The department treats diseases involving the liver and the biliary system such as :
                                                <br /><br />
                <div style="padding-left:10px">

-	Genetic/metabolic disease
<br />-	Biliary atresia
<br />-	Choledochal cysts
<br />-	Gallstones
<br />-	Sphincter of Oddi dysfunction
<br />-	Infectious hepatitis
<br />-	Autoimmune hepatitis
<br />-	Neonatal jaundice
<br />-	Primary sclerosing cholangitis
<br />-	Cirrhosis
<br />-	Hepatic masses
<br />-	Non-alcoholic fatty liver disease
<br />-	Storage diseases
<br />-	Cystic fibrosis
<br />-	Mitochondrial hepatopathy
<br />-	Liver failure
<br />-	Toxin induced hepatitis/failure
<br />-	Acute and chronic pancreatitis
<br />-	Pancreatic cysts and tumours
<br />-	Cystic fibrosis








</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            

		</div>




</asp:Content>
