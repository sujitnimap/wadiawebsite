﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="Child-OneDayAlumni-Registration.aspx.cs" Inherits="WadiaResponsiveWebsite.Children_OneDayAlumni_Registration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/jquery-1.9.1.min.js"></script>

    <script type="text/javascript">

        function cv_Gender(source, args) {
            if (document.getElementById("<%= rdbtnMale.ClientID %>").checked || document.getElementById("<%= rdbtnFemale.ClientID %>").checked) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }       
        
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ToolkitScriptManager runat="server"></asp:ToolkitScriptManager>
    <asp:ValidationSummary ID="vsCMEReg" runat="server" DisplayMode="BulletList"
        ShowSummary="false" ShowMessageBox="true" ValidationGroup="vgOneDayAlumni" />
    <div class="row block05">
        <div class="col-full">
            <div class="wrap-col">

                <div style="border: 1px solid #ddd; padding: 10px 20px 20px 10px; border-radius: 4px;">
                    <div class="heading">
                        <h2>Registration for One Day Alumni Event</h2>                        
                    </div>
                    <br />
                    Date: 14th December 2019, 3 pm onwards
                    <br />
                    (Registration is compulsory. Last date for registration is 15th Nov 2019)
                    <br />
                    <br />                    
                    <table>                       
                        <tr>
                            <td class="tdWidth">Name</td>
                            <td>
                                <asp:TextBox ID="txtName" CssClass="textboxcontact_list" required="" runat="server" placeholder="Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvName" ValidationGroup="vgOneDayAlumni" ControlToValidate="txtName" runat="server" ForeColor="Red" ErrorMessage="Please Enter Name">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="textboxcontact_list_rdb">Gender</td>
                            <td class="textboxcontact_list_rdb">
                                <asp:RadioButton ID="rdbtnMale" runat="server" Text="&nbsp;Male" GroupName="Gender" />&nbsp;&nbsp;&nbsp;&nbsp;                               
                            <asp:RadioButton ID="rdbtnFemale" runat="server" Text="&nbsp;Female" GroupName="Gender" />
                                <asp:CustomValidator ID="cvGender" runat="server" ErrorMessage="Please Select Gender"
                                    ClientValidationFunction="cv_Gender" OnServerValidate="cv_Gender"
                                    ForeColor="Red" ValidationGroup="vgOneDayAlumni">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td>
                                <asp:TextBox ID="txtDOB" CssClass="textboxcontact_list" required="" runat="server"
                                    placeholder="Date of Birth*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date of Birth'">
                                </asp:TextBox>
                                <asp:CalendarExtender ID="txtDOB_calender" runat="server" TargetControlID="txtDOB">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reftxtDOB" ValidationGroup="vgOneDayAlumni"
                                    ControlToValidate="txtDOB" runat="server" ForeColor="Red"
                                    ErrorMessage="Please Select Date Of Birth">*
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Mobile/Phone</td>
                            <td>
                                <asp:TextBox ID="txtMobileNo" CssClass="textboxcontact_list" MaxLength="10" required=""
                                    runat="server" placeholder="Mobile Number*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile Number'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvMobileNo" ValidationGroup="vgOneDayAlumni"
                                    ControlToValidate="txtMobileNo" ForeColor="Red" runat="server" ErrorMessage="Please enter your Mobile Number">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revMobileNo" Display="None" ValidationGroup="vgOneDayAlumni" ValidationExpression="^[0-9]{10}$"
                                    ControlToValidate="txtMobileNo" SetFocusOnError="true" runat="server" ErrorMessage="Enter 10 digit numeric value in Contact Number"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>
                                <asp:TextBox ID="txtEmailId" CssClass="textboxcontact_list" required="" runat="server"
                                    placeholder="Email ID*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email ID*'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmailId" ValidationGroup="vgOneDayAlumni" ControlToValidate="txtEmailId" ForeColor="Red"
                                    runat="server" ErrorMessage="Please enter your Email">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEmailId" Display="None" runat="server" ControlToValidate="txtEmailId" ErrorMessage="Enter a valid Email"
                                    ValidationExpression="^([a-zA-Z0-9_\-\.]+)@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$" ValidationGroup="vgOneDayAlumni"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>
                                <asp:TextBox ID="txtAddress" TextMode="MultiLine" CssClass="textboxcontact_list" runat="server" placeholder="Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address*'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvAddress" ValidationGroup="vgOneDayAlumni" ControlToValidate="txtAddress" runat="server" ForeColor="Red" ErrorMessage="Please Enter Address">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>City
                            </td>
                            <td>
                                <asp:TextBox ID="txtCity" CssClass="textboxcontact_list" runat="server" placeholder="City*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'City*'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvCity" ValidationGroup="vgOneDayAlumni" ControlToValidate="txtCity" runat="server" ForeColor="Red" ErrorMessage="Please Enter City">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td>State
                            </td>
                            <td>
                                <asp:TextBox ID="txtState" CssClass="textboxcontact_list" runat="server" placeholder="State*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'State*'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvState" ValidationGroup="vgOneDayAlumni" ControlToValidate="txtState" runat="server" ForeColor="Red" ErrorMessage="Please Enter State">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Country 
                            </td>
                            <td>
                                <asp:DropDownList CssClass="textboxcontact_drpdwn" ID="ddlCountrys" runat="server" style="width: 71%;">
                                    <asp:ListItem>Afghanistan</asp:ListItem>
                                    <asp:ListItem>Aland Islands</asp:ListItem>
                                    <asp:ListItem>Albania</asp:ListItem>
                                    <asp:ListItem>Algeria</asp:ListItem>
                                    <asp:ListItem>Andorra</asp:ListItem>
                                    <asp:ListItem>Angola</asp:ListItem>
                                    <asp:ListItem>Anguilla</asp:ListItem>
                                    <asp:ListItem>Antartica</asp:ListItem>
                                    <asp:ListItem>Antigua And Barbuda</asp:ListItem>
                                    <asp:ListItem>Argentina</asp:ListItem>
                                    <asp:ListItem>Armenia</asp:ListItem>
                                    <asp:ListItem>Aruba</asp:ListItem>
                                    <asp:ListItem>Ascension Island/St. Helena</asp:ListItem>
                                    <asp:ListItem>Australia</asp:ListItem>
                                    <asp:ListItem>Austria</asp:ListItem>
                                    <asp:ListItem>Azerbaijan</asp:ListItem>
                                    <asp:ListItem>Bahamas</asp:ListItem>
                                    <asp:ListItem>Bahrain</asp:ListItem>
                                    <asp:ListItem>Bangladesh</asp:ListItem>
                                    <asp:ListItem>Barbados</asp:ListItem>
                                    <asp:ListItem>Belarus</asp:ListItem>
                                    <asp:ListItem>Belgium</asp:ListItem>
                                    <asp:ListItem>Belize</asp:ListItem>
                                    <asp:ListItem>Benin</asp:ListItem>
                                    <asp:ListItem>Bermuda</asp:ListItem>
                                    <asp:ListItem>Bhutan</asp:ListItem>
                                    <asp:ListItem>Bolivia</asp:ListItem>
                                    <asp:ListItem>Bosnia Herzegovina</asp:ListItem>
                                    <asp:ListItem>Botswana</asp:ListItem>
                                    <asp:ListItem>Bouvet Island</asp:ListItem>
                                    <asp:ListItem>Brazil</asp:ListItem>
                                    <asp:ListItem>British Virgin Islands</asp:ListItem>
                                    <asp:ListItem>Brunei</asp:ListItem>
                                    <asp:ListItem>Bulgaria</asp:ListItem>
                                    <asp:ListItem>Burkina Faso</asp:ListItem>
                                    <asp:ListItem>Burundi</asp:ListItem>
                                    <asp:ListItem>Cambodia</asp:ListItem>
                                    <asp:ListItem>Cameroon, United Republic Of</asp:ListItem>
                                    <asp:ListItem>Canada</asp:ListItem>
                                    <asp:ListItem>Cape Verde, Republic Of</asp:ListItem>
                                    <asp:ListItem>Cayman Islands</asp:ListItem>
                                    <asp:ListItem>Central African Republic</asp:ListItem>
                                    <asp:ListItem>Chad</asp:ListItem>
                                    <asp:ListItem>Chile</asp:ListItem>
                                    <asp:ListItem>China</asp:ListItem>
                                    <asp:ListItem>Christmas Island</asp:ListItem>
                                    <asp:ListItem>Cocos Islands</asp:ListItem>
                                    <asp:ListItem>Colombia</asp:ListItem>
                                    <asp:ListItem>Comoros</asp:ListItem>
                                    <asp:ListItem>Congo</asp:ListItem>
                                    <asp:ListItem>Congo Democratic Republic Of</asp:ListItem>
                                    <asp:ListItem>Cook Islands</asp:ListItem>
                                    <asp:ListItem>Costa Rica</asp:ListItem>
                                    <asp:ListItem>Croatia</asp:ListItem>
                                    <asp:ListItem>Cuba</asp:ListItem>
                                    <asp:ListItem>Cyprus</asp:ListItem>
                                    <asp:ListItem>Czech Republic</asp:ListItem>
                                    <asp:ListItem>Denmark</asp:ListItem>
                                    <asp:ListItem>Djibouti</asp:ListItem>
                                    <asp:ListItem>Dominica</asp:ListItem>
                                    <asp:ListItem>Dominican Republic</asp:ListItem>
                                    <asp:ListItem>Ecuador</asp:ListItem>
                                    <asp:ListItem>Egypt</asp:ListItem>
                                    <asp:ListItem>El Salvador</asp:ListItem>
                                    <asp:ListItem>Equatorial Guinea</asp:ListItem>
                                    <asp:ListItem>Eritrea</asp:ListItem>
                                    <asp:ListItem>Estonia</asp:ListItem>
                                    <asp:ListItem>Ethiopia</asp:ListItem>
                                    <asp:ListItem>Faeroe Islands</asp:ListItem>
                                    <asp:ListItem>Falkland Islands</asp:ListItem>
                                    <asp:ListItem>Fiji Islands</asp:ListItem>
                                    <asp:ListItem>Finland</asp:ListItem>
                                    <asp:ListItem>France</asp:ListItem>
                                    <asp:ListItem>French Guiana</asp:ListItem>
                                    <asp:ListItem>French Polynesia</asp:ListItem>
                                    <asp:ListItem>Gabon</asp:ListItem>
                                    <asp:ListItem>Gambia</asp:ListItem>
                                    <asp:ListItem>Georgia</asp:ListItem>
                                    <asp:ListItem>Germany</asp:ListItem>
                                    <asp:ListItem>Ghana</asp:ListItem>
                                    <asp:ListItem>Gibraltar</asp:ListItem>
                                    <asp:ListItem>Greece</asp:ListItem>
                                    <asp:ListItem>Greenland</asp:ListItem>
                                    <asp:ListItem>Grenada</asp:ListItem>
                                    <asp:ListItem>Guadeloupe</asp:ListItem>
                                    <asp:ListItem>Guam</asp:ListItem>
                                    <asp:ListItem>Guatemala</asp:ListItem>
                                    <asp:ListItem>Guernsey</asp:ListItem>
                                    <asp:ListItem>Guinea</asp:ListItem>
                                    <asp:ListItem>Guinea Bissau</asp:ListItem>
                                    <asp:ListItem>Guyana</asp:ListItem>
                                    <asp:ListItem>Haiti</asp:ListItem>
                                    <asp:ListItem>Heard Island And McDonald Islands</asp:ListItem>
                                    <asp:ListItem>Honduras</asp:ListItem>
                                    <asp:ListItem>Hong Kong</asp:ListItem>
                                    <asp:ListItem>Hungary</asp:ListItem>
                                    <asp:ListItem>Iceland</asp:ListItem>
                                    <asp:ListItem Selected="True">India</asp:ListItem>
                                    <asp:ListItem>Indonesia</asp:ListItem>
                                    <asp:ListItem>Iran</asp:ListItem>
                                    <asp:ListItem>Iraq</asp:ListItem>
                                    <asp:ListItem>Ireland, Republic Of</asp:ListItem>
                                    <asp:ListItem>Isle Of Man</asp:ListItem>
                                    <asp:ListItem>Israel</asp:ListItem>
                                    <asp:ListItem>Italy</asp:ListItem>
                                    <asp:ListItem>Ivory Coast</asp:ListItem>
                                    <asp:ListItem>Jamaica</asp:ListItem>
                                    <asp:ListItem>Japan</asp:ListItem>
                                    <asp:ListItem>Jersey Island</asp:ListItem>
                                    <asp:ListItem>Jordan</asp:ListItem>
                                    <asp:ListItem>Kazakstan</asp:ListItem>
                                    <asp:ListItem>Kenya</asp:ListItem>
                                    <asp:ListItem>Kiribati</asp:ListItem>
                                    <asp:ListItem>Korea, Democratic Peoples Republic</asp:ListItem>
                                    <asp:ListItem>Korea, Republic Of</asp:ListItem>
                                    <asp:ListItem>Kuwait</asp:ListItem>
                                    <asp:ListItem>Kyrgyzstan</asp:ListItem>
                                    <asp:ListItem>Lao, People&#39;s Dem. Rep.</asp:ListItem>
                                    <asp:ListItem>Latvia</asp:ListItem>
                                    <asp:ListItem>Lebanon</asp:ListItem>
                                    <asp:ListItem>Lesotho</asp:ListItem>
                                    <asp:ListItem>Liberia</asp:ListItem>
                                    <asp:ListItem>Libyan Arab Jamahiriya</asp:ListItem>
                                    <asp:ListItem>Liechtenstein</asp:ListItem>
                                    <asp:ListItem>Lithuania</asp:ListItem>
                                    <asp:ListItem>Luxembourg</asp:ListItem>
                                    <asp:ListItem>Macau</asp:ListItem>
                                    <asp:ListItem>Macedonia</asp:ListItem>
                                    <asp:ListItem>Madagascar (Malagasy)</asp:ListItem>
                                    <asp:ListItem>Malawi</asp:ListItem>
                                    <asp:ListItem>Malaysia</asp:ListItem>
                                    <asp:ListItem>Maldives</asp:ListItem>
                                    <asp:ListItem>Mali</asp:ListItem>
                                    <asp:ListItem>Malta</asp:ListItem>
                                    <asp:ListItem>Marianna Islands</asp:ListItem>
                                    <asp:ListItem>Marshall Islands</asp:ListItem>
                                    <asp:ListItem>Martinique</asp:ListItem>
                                    <asp:ListItem>Mauritania</asp:ListItem>
                                    <asp:ListItem>Mauritius</asp:ListItem>
                                    <asp:ListItem>Mayotte</asp:ListItem>
                                    <asp:ListItem>Mexico</asp:ListItem>
                                    <asp:ListItem>Micronesia</asp:ListItem>
                                    <asp:ListItem>Moldova</asp:ListItem>
                                    <asp:ListItem>Monaco</asp:ListItem>
                                    <asp:ListItem>Mongolia</asp:ListItem>
                                    <asp:ListItem>Montserrat</asp:ListItem>
                                    <asp:ListItem>Morocco</asp:ListItem>
                                    <asp:ListItem>Mozambique</asp:ListItem>
                                    <asp:ListItem>Myanmar</asp:ListItem>
                                    <asp:ListItem>Namibia</asp:ListItem>
                                    <asp:ListItem>Nauru</asp:ListItem>
                                    <asp:ListItem>Nepal</asp:ListItem>
                                    <asp:ListItem>Netherland Antilles</asp:ListItem>
                                    <asp:ListItem>Netherlands</asp:ListItem>
                                    <asp:ListItem>New Caledonia</asp:ListItem>
                                    <asp:ListItem>New Zealand</asp:ListItem>
                                    <asp:ListItem>Nicaragua</asp:ListItem>
                                    <asp:ListItem>Niger</asp:ListItem>
                                    <asp:ListItem>Nigeria</asp:ListItem>
                                    <asp:ListItem>Niue</asp:ListItem>
                                    <asp:ListItem>Norfolk Island</asp:ListItem>
                                    <asp:ListItem>Norway</asp:ListItem>
                                    <asp:ListItem>Occupied Palestinian Territory</asp:ListItem>
                                    <asp:ListItem>Oman, Sultanate Of</asp:ListItem>
                                    <asp:ListItem>Pakistan</asp:ListItem>
                                    <asp:ListItem>Palau</asp:ListItem>
                                    <asp:ListItem>Panama</asp:ListItem>
                                    <asp:ListItem>Papua New Guinea (Niugini)</asp:ListItem>
                                    <asp:ListItem>Paraguay</asp:ListItem>
                                    <asp:ListItem>Peru</asp:ListItem>
                                    <asp:ListItem>Philippines</asp:ListItem>
                                    <asp:ListItem>Pitcairn</asp:ListItem>
                                    <asp:ListItem>Poland</asp:ListItem>
                                    <asp:ListItem>Portugal</asp:ListItem>
                                    <asp:ListItem>Qatar</asp:ListItem>
                                    <asp:ListItem>Reunion</asp:ListItem>
                                    <asp:ListItem>Romania</asp:ListItem>
                                    <asp:ListItem>Russian Federation</asp:ListItem>
                                    <asp:ListItem>Rwanda</asp:ListItem>
                                    <asp:ListItem>Saint Lucia</asp:ListItem>
                                    <asp:ListItem>Saint Vincent And The Grenadines</asp:ListItem>
                                    <asp:ListItem>Samoa, American</asp:ListItem>
                                    <asp:ListItem>Samoa, Independent State Of</asp:ListItem>
                                    <asp:ListItem>San Marino</asp:ListItem>
                                    <asp:ListItem>Sao Tome &amp; Principe</asp:ListItem>
                                    <asp:ListItem>Saudi Arabia</asp:ListItem>
                                    <asp:ListItem>Senegal</asp:ListItem>
                                    <asp:ListItem>Serbia</asp:ListItem>
                                    <asp:ListItem>Seychelles Islands</asp:ListItem>
                                    <asp:ListItem>Sierra Leone</asp:ListItem>
                                    <asp:ListItem>Singapore</asp:ListItem>
                                    <asp:ListItem>Slovakia</asp:ListItem>
                                    <asp:ListItem>Slovenia</asp:ListItem>
                                    <asp:ListItem>Solomon Islands</asp:ListItem>
                                    <asp:ListItem>Somalia</asp:ListItem>
                                    <asp:ListItem>South Africa</asp:ListItem>
                                    <asp:ListItem>Spain</asp:ListItem>
                                    <asp:ListItem>Sri Lanka</asp:ListItem>
                                    <asp:ListItem>St. Kitts - Nevis</asp:ListItem>
                                    <asp:ListItem>St. Pierre &amp; Miquelon</asp:ListItem>
                                    <asp:ListItem>Sudan</asp:ListItem>
                                    <asp:ListItem>Suriname</asp:ListItem>
                                    <asp:ListItem>Svalbard And Jan Mayen Is</asp:ListItem>
                                    <asp:ListItem>Swaziland</asp:ListItem>
                                    <asp:ListItem>Sweden</asp:ListItem>
                                    <asp:ListItem>Switzerland</asp:ListItem>
                                    <asp:ListItem>Syrian Arab Rep.</asp:ListItem>
                                    <asp:ListItem>Taiwan, Republic of China</asp:ListItem>
                                    <asp:ListItem>Tajikistan</asp:ListItem>
                                    <asp:ListItem>Tanzania</asp:ListItem>
                                    <asp:ListItem>Thailand</asp:ListItem>
                                    <asp:ListItem>Timor Leste</asp:ListItem>
                                    <asp:ListItem>Togo</asp:ListItem>
                                    <asp:ListItem>Tonga</asp:ListItem>
                                    <asp:ListItem>Trinidad &amp; Tobago</asp:ListItem>
                                    <asp:ListItem>Tunisia</asp:ListItem>
                                    <asp:ListItem>Turkey</asp:ListItem>
                                    <asp:ListItem>Turkmenistan</asp:ListItem>
                                    <asp:ListItem>Turks And Caicos Islands</asp:ListItem>
                                    <asp:ListItem>Tuvalu</asp:ListItem>
                                    <asp:ListItem>Uganda</asp:ListItem>
                                    <asp:ListItem>Ukraine</asp:ListItem>
                                    <asp:ListItem>United Arab Emirates</asp:ListItem>
                                    <asp:ListItem>United Kingdom</asp:ListItem>
                                    <asp:ListItem>United States</asp:ListItem>
                                    <asp:ListItem>United States Minor Outlying Islnds</asp:ListItem>
                                    <asp:ListItem>Uruguay</asp:ListItem>
                                    <asp:ListItem>Uzbekistan Sum</asp:ListItem>
                                    <asp:ListItem>Vanuatu</asp:ListItem>
                                    <asp:ListItem>Vatican City State</asp:ListItem>
                                    <asp:ListItem>Venezuela</asp:ListItem>
                                    <asp:ListItem>Vietnam</asp:ListItem>
                                    <asp:ListItem>Wallis &amp; Futuna Islands</asp:ListItem>
                                    <asp:ListItem>Western Sahara</asp:ListItem>
                                    <asp:ListItem>Yemen, Republic Of</asp:ListItem>
                                    <asp:ListItem>Yugoslavia</asp:ListItem>
                                    <asp:ListItem>Zambia</asp:ListItem>
                                    <asp:ListItem>Zimbabwe</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvCountry" ValidationGroup="vgOneDayAlumni" ControlToValidate="ddlCountrys" runat="server" ForeColor="Red" ErrorMessage="Please Select Country">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Period affiliated to BJWHC
                            </td>
                            <td>
                                <asp:TextBox ID="txtPeriodBJWHC" CssClass="textboxcontact_list" runat="server" placeholder="Period affiliated to BJWHC*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Period affiliated to BJWHC*'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPeriodBJWHC" ValidationGroup="vgOneDayAlumni" ControlToValidate="txtPeriodBJWHC" runat="server" ForeColor="Red" ErrorMessage="Please Enter Period affiliated to BJWHC">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td>Dept./Unit
                            </td>
                            <td>
                                <asp:TextBox ID="txtDeptUnit" CssClass="textboxcontact_list" runat="server" placeholder="Dept./Unit*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Dept./Unit*'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvDeptUnit" ValidationGroup="vgOneDayAlumni" ControlToValidate="txtDeptUnit" runat="server" ForeColor="Red" ErrorMessage="Please Enter Dept. or Unit">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Degree
                            </td>
                            <td>
                                <asp:TextBox ID="txtDegree" CssClass="textboxcontact_list" runat="server" placeholder="Degree*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Degree*'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvDegree" ValidationGroup="vgOneDayAlumni" ControlToValidate="txtDegree" runat="server" ForeColor="Red" ErrorMessage="Please Enter Degree">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Branch
                            </td>
                            <td>
                                <asp:TextBox ID="txtBranch" CssClass="textboxcontact_list" runat="server" placeholder="Branch*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Branch*'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvBranch" ValidationGroup="vgOneDayAlumni" ControlToValidate="txtBranch" runat="server" ForeColor="Red" ErrorMessage="Please Enter Branch">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>                        
                    </table>
                    <br />
                    <div style="display: inline; padding-right: 35px">
                        <strong>Registration Fee for event: </strong>
                    </div>
                    <div style="display: inline">
                        <asp:Label ID="lblTotalAmountInINR" runat="server" Text="Rs. 3500"></asp:Label>
                        <asp:HiddenField ID="hdfTotalPayableAmount" runat="server" Value="3500"/>

                    </div>

                    <br />
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    <br />
                    <asp:Button ID="btnPayment" Text="Pay now" ValidationGroup="vgOneDayAlumni" CssClass="btn" runat="server" OnClick="btnPayment_Click" />
                </div>
            </div>
        </div>
    </div>
   
</asp:Content>
