﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;
using System.Reflection;

namespace WadiaResponsiveWebsite
{
    public partial class ReproductiveInfertilityClinicPayURequest : System.Web.UI.Page
    {
        public String key = "";
        public String txnid = "";

        public string amount = "";
        public string productinfo = "";
        public string firstname = "";
        public string lastname = "";
        public string phone = "";
        public string email = "";
        public string udf1 = "";
        public string udf2 = "";
        public string udf3 = "";
        public string udf4 = "";
        public string udf5 = "";
        public string udf6 = "";
        public string udf7 = "";
        public string udf8 = "";
        public string udf9 = "";
        public string udf10 = "";
        public string salt = "";

        public string hash = "";
        public string acceptURL;
        public string declineURL;
        public string exceptionURL;
        public string cancelURL;

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int _RICId = 0;            
            IList<Reproductive_Infertility_Clinic_Detail> reproductiveInfertilityClinicData;

            if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
            {
                _RICId = Convert.ToInt32(Request.QueryString["Id"]);
            }            

            reproductiveInfertilityClinicData = CCMERegistration.Instance().GetReproductiveInfertilityClinic(_RICId);

            IList<Reproductive_Infertility_Clinic_Detail> data = reproductiveInfertilityClinicData.Where(p => p.RICId.Equals(_RICId)).ToList();

            DataTable detailTable = ToDataTable(data);

            if (detailTable.Rows.Count > 0)
            {
                firstname = (detailTable.Rows[0]["FirstName"]).ToString();
                lastname = (detailTable.Rows[0]["LastName"]).ToString();
            }
            amount = (detailTable.Rows[0]["Amount"]).ToString();
            phone = (detailTable.Rows[0]["MobileNo"]).ToString();
            email = "";  //(detailTable.Rows[0]["EmailID"]).ToString();            
            
            String seprator = "|";

            //Live
            //key = "3FJu2b";
            //salt = "ABYPOdc6";

            //key = "VPcm4L";
            //salt = "OmM6jqjz";

            //key = "gtKFFx";
            //salt = "eCwWELxi";

            key = ConfigurationManager.AppSettings["CKey"];
            salt = ConfigurationManager.AppSettings["CSalt"];

            productinfo = "Reproductive Infertility";

            udf1 = _RICId.ToString(); // donateOnlineID.ToString();
            udf4 = "";
            //txnid = Guid.NewGuid().ToString();
            //txnid = Now.ToString("yyyyMMdd_HHmmssffff");

            txnid = DateTime.Now.ToString("yyyyMMddHHmmssffff", CultureInfo.InvariantCulture);

            string strWebSiteURL = ConfigurationManager.AppSettings["LinkTosite"];

            //acceptURL = "http://localhost:1007/ReproductiveInfertilityClinicPayUResponse.aspx";
            //declineURL = "http://localhost:1007/ReproductiveInfertilityClinicPayUResponse.aspx";
            //cancelURL = "http://localhost:1007/ReproductiveInfertilityClinicPayUResponse.aspx";


            acceptURL = "http://wadiahospitals.org/ReproductiveInfertilityClinicPayUResponse.aspx";
            declineURL = "http://wadiahospitals.org/ReproductiveInfertilityClinicPayUResponse.aspx";
            cancelURL = "http://wadiahospitals.org/ReproductiveInfertilityClinicPayUResponse.aspx";

            String hashStr = key + seprator + txnid + seprator + amount + seprator + productinfo + seprator + firstname + seprator + email + seprator + udf1 + seprator + udf2 + seprator + udf3 + seprator + udf4 + seprator + udf5 + seprator + udf6 + seprator + udf7 + seprator + udf8 + seprator + udf9 + seprator + udf10 + seprator + salt;
            hash = CreateBase64SHA512Hash(hashStr, "ISO-8859-2").ToLower();

        }

        public static string CreateBase64SHA512Hash(string hashTarget, string encoding)
        {
            System.Security.Cryptography.SHA512 sha = new System.Security.Cryptography.SHA512CryptoServiceProvider();
            byte[] targetBytes = System.Text.Encoding.GetEncoding(encoding).GetBytes(hashTarget);
            byte[] hashBytes = sha.ComputeHash(targetBytes);

            StringBuilder sb = new StringBuilder(hashBytes.Length * 2);
            foreach (byte b in hashBytes)
            {
                sb.AppendFormat("{0:x2}", b);
            }
            return sb.ToString();
        }
    }
}