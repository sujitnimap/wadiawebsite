﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-obstetrics.aspx.cs" Inherits="WadiaResponsiveWebsite.women_obstetrics" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2> 	Obstetrics  </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
•	OPD: The Obstetrics OPD (Out-Patient Department) provides comprehensive evaluation, diagnosis, management and counselling to women with complications during their pregnancies. Prenatal diagnosis and management is available for conditions such as Rh isoimmunisation and thalassemia.

 <br />
 <br />
•	IPD: The IPD (In-Patient Department) for Obstetrics caters to women who need to be admitted to the hospital overnight or for an undetermined period of time for treatment, observation or surgery during the course of their pregnancy. We have top of the line equipment for intensive monitoring of our patients and also have efficient processes in place for emergency deliveries or caesarean section surgeries. 

 <br />
 <br />
       <b>Services Provided: </b>
 <br />
 <br />
<div style="padding-left:25px">
       
-	Management of normal pregnancies
<br />-	Total preconception and pregnancy care
<br />-	Ultrasonography
<br />-	Antenatal care (before birth)
<br />-	Intrapartum care (during labour and delivery)
<br />-	Postnatal care (after birth)

</div>


 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

        

</div>




</asp:Content>
