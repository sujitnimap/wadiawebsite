﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-about-doctdetails-drnitin.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_doctdetails_drnitin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row block05">
			
		<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Nitin Shah.jpg" />
<div class="docdetails">
Dr. Nitin Shah
<div class="docdetailsdesg">
Asst. Honorary Paediatric Haematologist Oncologist
        <br />
MD, DCH



</div>

</div>
</div>

                    <div class="areaofinterest">

                         Areas of interest : Paediatric Haematology Oncology, Immunology
</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
  •	President, Indian Academy of Paediatrics, 2006
<br /><br />•	IAP Trainee Fellowship in Paediatric Haematology
<br /><br />•	Editor, IAP Textbook of Paediatrics 
<br /><br />•	Editor, IAP Specialty Series Books 
<br /><br />•	Editor, Textbook of Paediatric Haematology Oncology
<br /><br />•	Editor, IAP Specialty Series book on Paediatric Haematology
<br /><br />•	Editor, IAP Book on Vaccines
<br /><br />•	Editor, Paediatrics Clinics of India
<br /><br />•	Editor, Paediatrics Today
<br /><br />•	Delivered more than 600 Guest Lectures and Orations at National and International forums




<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	Fellow of Indian Society of Haematology and Transfusion Medicine
<br /><br />•	Life member of Indian Academy of Paediatrics
<br /><br />•	Life member of Mumbai Branch of Indian Academy of Paediatrics
<br /><br />•	Life Member of Paediatric Haematology Oncology Chapter of Indian Academy of Paediatrics
<br /><br />•	Life member of Sion Medicos, Mumbai
<br /><br />•	Life member Mahim-Dharavi Medical Practitioners Association (MDMPA), Mumbai 
<br /><br />•	Active member of Lion’s Club of Sion, Mumbai
<br /><br />•	Life member of Bombay Haematology Group, Mumbai
<br /><br />•	Life member of Dr. Athavale Research Foundation, Mumbai



    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
Dr. Nitin Shah has published more than 500 scientific papers in leading journals of national and international repute.
                    <br />
He has been invited as a speaker for over 400 guest lectures, has organized more than 20 conferences and has been a chair at over 15 conferences in his career.




                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
         </div>





</asp:Content>
