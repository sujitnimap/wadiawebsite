﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Aboutus-NusserwanjiWadia.aspx.cs" Inherits="WadiaResponsiveWebsite.women_nusserwanji_wadia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>HISTORY</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Nowrosjee Wadia.png" />
<div class="docdetails">

Nowrosjee Nusserwanjee Wadia (1849 – 1899)

</div>
</div>

<div class="historydetails">

Following in the footsteps of his father Nusserwanjee Ardeshir Wadia, Nowrosjee Wadia went to the United Kingdom at the impressionable age of 14 and pursued engineering. On his return, he applied his exceptional engineering skills and went on to establish the renowned Bombay Dyeing and Manufacturing Co. in 1879 along with several other textile mills across India.
 <br />
<br />
Having achieved success early in his life, this philanthropist made use of his status to give back to his society, to those who needed it the most. He made benevolent efforts to provide help and care to the underprivileged.
<br />
<br />
As part of various government and educational bodies - a member of some and as Chairman in others, Nowrosjee worked from the inside to help evolve the condition of education. He brought about better schooling through more effective teaching methods, he introduced the kindergarten system of education as well as encouraged physical training for both, boys and girls. He also helped improve the administration of hospitals in the city.
<br />
<br />

</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>
                                   
									<li><a href="NWMH-Aboutus-Overview.aspx">Overview</a></li>
									<li><a href="NWMH-Aboutus-Mission-Vision-CoreValues.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="NWMH-Aboutus-Chairman'smessage.aspx">Chairman's message</a></li>
									<%--<li><a href="women-about-awards.aspx">Awards and Achievements</a></li>--%>
									<li><a href="NWMH-Aboutus-History.aspx">History </a>

                                         <div class="leftpadsubmenu">

                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="NWMH-Aboutus-NusserwanjiWadia.aspx">Nowrosjee Nusserwanji Wadia </a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="NWMH-Aboutus-SirNessWadia.aspx">Sir Ness Wadia </a>

                                        </div>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>







		</div>



</asp:Content>
