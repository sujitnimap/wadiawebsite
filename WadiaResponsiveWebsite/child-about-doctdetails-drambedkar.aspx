﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-doctdetails-drambedkar.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drambedkar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row block05">
			
			<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dr_y_ambdekar-2.jpg" />
<div class="docdetails">
Dr. Amdekar Yeshwant K.
<div class="docdetailsdesg">
FRCPH (UK), MD, DCH, MBBS
        <br />
Medical Director, BJWHC

</div>

</div>
</div>

                    <div class="areaofinterest">

Dr. Amdekar has over 40 years of clinical experience while working in various Paediatric hospitals across the city. He has also authored a number of books and contributed to research through his publications in research journals of national and international repute.

</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research or publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
     •	Research Fellow of Council of Scientific and Industrial Research (CSIR)
<br />•	Was awarded several orations including most prestigious oration of Indian Academy of Paediatrics – “Dr. Shantilal Sheth oration” 
<br />•	Has been invited as a faculty in City, State and National conferences of Indian Academy of Paediatrics and Indian Medical Association over several years and also in international conferences.
<br />•	Was invited as a speaker in USA, China, Bangkok, Kuala Lumpur, Hongkong, Philippines, Finland and Singapore. 
<br />•	Appointed by Royal College of Paediatrics and Child Health as Regional Advisor for Asia for 2012-2014
<br />•	Recently has been invited by Royal College of Paediatrics and Child Health London to be an examiner for MRCP.




<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
     •	Has held key positions of President, General Secretary and Treasurer of Indian Academy of Paediatrics
<br />•	Founder trustee of Heinz Nutrition Foundation of India since its inception 14 years ago and continue to serve on board of trustees till date
<br />•	Founder trustee of Rousel Foundation of India and continued to serve on board of trustees from 1982 to 1990
<br />•	Founder member of ACASH – non-governmental organization related to health issues
<br />•	Trustee of charitable organisation – “Foundation for mother and child health” that has adopted Ganesh Nagar low socioeconomic community.

    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Authored four books in Paediatrics – 
<div style="padding-left:30px">
o	Paediatric priorities in Office practice
<br />o	Lessons for Grand Rounds
<br />o	Lessons from Grand Rounds 1
<br />o	Lessons from Grand Rounds for office practice
</div> 
•	Contributed to publications and editorials in Indian Paediatrics – official journal of Indian Academy of Paediatrics
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
</asp:Content>
