﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Gynaecolgic-surgery.aspx.cs" Inherits="WadiaResponsiveWebsite.women_gynasurgery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Gynaecologic Surgery</h2></div>
                    
                    <br />

<!-- CONTENT -->

<div style="line-height:20px">


    <strong>Overview </strong>
    <br />    <br />



Gynaecologic Surgery refers to surgeries performed on the female reproductive system. These surgeries are performed by trained super-specialists and are conducted to treat benign conditions, infertility, hysterectomy and myomectomy among others. 

<br /><br />
<b>Services Provided :</b> 
<br /><br />
    <div style="padding-left:25px">
-	Laparoscopic myomectomy (removal of uterine fibroids) 
<br />-	Abdominal myomectomy
<br />-	Hysteroscopic myomectomy
<br />-	Vaginal hysterectomy
<br />-	Laparoscopic total hysterectomy (removal of uterus and cervix)
<br />-	Abdominal hysterectomy
<br />-	Laparoscopic supracervical hysterectomy (removal of uterus, preservation of cervix)
<br />-	Diagnostic laparoscopy
<br />-	Laparoscopic removal of endometriosis
<br />-	Laparoscopic removal of ovarian cysts
<br />-	Laparoscopic removal of adhesions (scar tissue)
<br />-	Laparoscopic removal of a tube and ovary
<br />-	Laparoscopic uterine suspension
<br />-	Hysteroscopic surgery (removal of polyps or fibroids from the inside of the uterus)
<br />-	Laparoscopic bladder support surgery
<br />-	Endometrial ablation (for heavy periods)
</div>

    <div style="height:90px"></div>



</div>
<!--  CONTENT -->


                
                    
                    
				</div>
			</div>
			


        



		</div>





</asp:Content>
