﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OperationStatus;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class littleheartsmarathon2017_Registration : System.Web.UI.Page
    {
        int totalAmount = 500;

        

        protected void Page_Load(object sender, EventArgs e)
        {
            //string category = ddlCategory.SelectedValue;

            if (cb_EnrollFirstParent.Checked)
                totalAmount += 200;

            if (cb_EnrollSecondParent.Checked)
                totalAmount += 300;

            if (chkIsSponsorARun.Checked)
                totalAmount += 500;

            lblTotalAmountInINR.Text = totalAmount.ToString();

            hdn_totalAmount.Value = totalAmount.ToString();
        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {

            int registrationForMarathonID;
            int noOfChildrens = 1;
            int noOfAdults;
            string emailAddress;
            string confirmEmailAddress;
            string mobileNumber1;
            string mobileNumber2;
            string address;
            string city;
            string state;
            string country;
            string nationality;
            string additionalInformation;
            bool isSponsorARun;
            int noOfSponsorARun;
            int totalAmount;


            noOfChildrens = 1;
            noOfAdults = cb_EnrollFirstParent.Checked ? 1 : 0;
            noOfAdults = cb_EnrollSecondParent.Checked ? 2 : noOfAdults;

            emailAddress = txtParentEmailId.Text.Trim();
            mobileNumber1 = txtFirstParentMobileNo.Text.Trim();
            mobileNumber2 = txtSecondParentMobileNo.Text.Trim();
            address = txtAddress.Text.Trim();
            city = txtCity.Text.Trim();
            state = txtState.Text.Trim();
            country = ddlCountrys.SelectedValue;
            nationality = ddlNationalitys.SelectedValue;
            additionalInformation = txtAnyOtherRelevantInformation.Text.Trim();
            isSponsorARun = chkIsSponsorARun.Checked;
            noOfSponsorARun = chkIsSponsorARun.Checked ? 1 : 0;
            totalAmount = Convert.ToInt32(hdn_totalAmount.Value);

            //if (emailAddress != confirmEmailAddress)
            //{
            //    lblMessage.Text = "Please check terms and conditions";
            //}

            registrationForMarathon objRegistrationForMarathon = new registrationForMarathon();
            COperationStatus os = new COperationStatus();

            objRegistrationForMarathon.Category = ddlCategory.SelectedValue;
            objRegistrationForMarathon.NoOfChildrens = noOfChildrens;
            objRegistrationForMarathon.NoOfAdults = noOfAdults;
            objRegistrationForMarathon.EmailAddress = emailAddress;
            objRegistrationForMarathon.ConfirmEmailAddress = emailAddress;
            objRegistrationForMarathon.MobileNumber1 = mobileNumber1;
            objRegistrationForMarathon.MobileNumber2 = mobileNumber2;
            objRegistrationForMarathon.Address = address;
            objRegistrationForMarathon.City = city;
            objRegistrationForMarathon.State = state;
            objRegistrationForMarathon.Country = country;
            objRegistrationForMarathon.Nationality = nationality;
            objRegistrationForMarathon.AdditionalInformation = additionalInformation;
            objRegistrationForMarathon.IsSponsorARun = isSponsorARun;
            objRegistrationForMarathon.NoOfSponsorARun = noOfSponsorARun;
            objRegistrationForMarathon.TotalAmount = totalAmount;

            os = CRegistrationForMarathon.Instance().InsertRegistrationForMarathonDetail(objRegistrationForMarathon);

            registeredChildForMarathon registeredChildForMarathon = new registeredChildForMarathon();
            registeredChildForMarathon.ChildFirstName = txtChildFirstName.Text + txtChildLastName.Text;
            registeredChildForMarathon.Age = Convert.ToInt32(ddlChildAge.SelectedValue);
            registeredChildForMarathon.Gender = rbGenderKids.SelectedValue;
            registeredChildForMarathon.TShirtSize = ddlTShirtSizeChild.SelectedValue;
            registeredChildForMarathon.School = txtSchoolName.Text;
            registeredChildForMarathon.RegistrationForMarathonID = objRegistrationForMarathon.RegistrationForMarathonID;


            os = CRegisteredChildForMarathon.Instance().InsertRegisteredChildForMarathonDetail(registeredChildForMarathon);


            List<registeredAdultForMarathon> selRegisteredAdultForMarathon = (List<registeredAdultForMarathon>)ViewState["registeredAdultForMarathons"];

            if (cb_EnrollFirstParent.Checked)
            {
                registeredAdultForMarathon registerFirstParent = new registeredAdultForMarathon();
                registerFirstParent.AdultName = txtFirstParentName.Text;
                registerFirstParent.Age = 0;
                registerFirstParent.Gender = rbFirstParentGender.SelectedValue;
                registerFirstParent.TShirtSize1 = ddlTShirtSizeFirstParent.SelectedValue;
                registerFirstParent.Organization = "";
                registerFirstParent.RegistrationForMarathonID = objRegistrationForMarathon.RegistrationForMarathonID;

                os = CRegisteredAdultForMarathon.Instance().InsertRegisteredAdultForMarathonDetail(registerFirstParent);
            }
            if (cb_EnrollSecondParent.Checked)
            {
                registeredAdultForMarathon registeredSecondParent = new registeredAdultForMarathon();
                registeredSecondParent.AdultName = txtSecondParentName.Text;
                registeredSecondParent.Age = 0;
                registeredSecondParent.Gender = rblSecondParentGender.SelectedValue;
                registeredSecondParent.TShirtSize1 = ddlSecondParentTShirtSize.SelectedValue;
                registeredSecondParent.Organization = "";
                registeredSecondParent.RegistrationForMarathonID = objRegistrationForMarathon.RegistrationForMarathonID;
                os = CRegisteredAdultForMarathon.Instance().InsertRegisteredAdultForMarathonDetail(registeredSecondParent);


            }

            if (os.Success == true)
            {

                Response.Redirect("MarathonPayURequest.aspx?Id=" + objRegistrationForMarathon.RegistrationForMarathonID);

            }
        }
    }
}