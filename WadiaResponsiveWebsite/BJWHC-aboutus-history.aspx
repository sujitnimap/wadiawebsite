﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-aboutus-history.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_history" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>HISTORY</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">

Before the onset of Paediatrics as an independent discipline, childcare was limited to using adult medication on to children, who were seen merely as mini versions of adults. It was in 1928 that Sir Ness Wadia and Sir Cusrow Wadia had a vision to provide specialized Paediatric and Neonatal care for children and as strong believers in the idea that the need for quality healthcare is universal, they vowed to provide affordable world-class medical services to children in every section of the society. <br />
<br />
They built The Bai Jerbai Wadia Hospital for Children, in memory of their mother Bai Jerbai Wadia. 
<br />
<br />
In the first board meeting held by the Trust in February 1928, Sir Cusrow Wadia was selected as the Chairman of the board consisting of his brother Sir Ness Wadia and six other members. Dr. RN Cooper was appointed as Hon. Principal Medical Officer.
<br />
<br />
The foundation stone for the hospital was laid on 7th March, 1928 and was declared open on the 12th of December, 1929 by the Governor of Bombay at the time, Sir Frederick Sykes. 
<br />
<br />
BJWHC emerged as India’s first independent specialized Paediatrics hospital, dedicated exclusively to healthcare for children, as recognized by the Indian Academy of Paediatrics. 
<br />
<br />


</div>
<!-- ABOUT US CONTENT -->
                    
				</div>
			</div>


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
									<li><a href="BJWHC-aboutus-overview.aspx">Overview</a></li>
									<li><a href="BJWHC-aboutus-mission-vision-corevalues.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="BJWHC-Aboutus-Chairman's-Message.aspx">Chairman's message</a></li>
									<%--<li><a href="child-about-awards.aspx">Awards and Achievements</a></li>--%>
										<li><a href="BJWHC-aboutus-history.aspx">History </a>

                                        <div class="leftpadsubmenu">

                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="BJWHC-Aboutus-Bai-jerbai-wadia.aspx">Bai Jerbai Wadia </a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cusrow Wadia </a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="BJWHC-Aboutus-Sir-ness-wadia.aspx">Sir Ness Wadia </a>

                                        </div>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>



		</div>




</asp:Content>
