﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Education-Medical-Overview.aspx.cs" Inherits="WadiaResponsiveWebsite.women_medical_overview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					       <div class="heading">
                  <h2>OVERVIEW</h2></div>
                

<!-- ABOUT US CONTENT -->

<div class="devider_20px"></div>
<div style="line-height:20px; text-align:justify; width:100%">

    
As a leading hospital for maternity and obstetric care,we further expanded our reach in medicine by opening up our world of knowledge to aspiring postgraduate professionals in the field of Obstetrics and Gynaecology. 
Through the efforts of our institution and our faculty of highly specialized, experienced and renowned doctors, we vow to provide our society with talented medical professionals.
<BR /><BR />The education imparted at our courses ensures that our students gain excellent practical experience. It is also our constant endeavour to 
    keep updating our knowledge and skill sets to impart the highest quality of education by highest quality standards. 



</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>MEDICAL</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						 <li><a href="women-medical-overview.aspx">	Overview  </a></li>
                         <li><a href="women-medical-directormsg.aspx"> Directors message </a></li>
                         <li><a href="women-medical-courses.aspx"> 	Courses available</a></li>
                         <li><a href="women-contact-us.aspx">	Contact Us  </a></li>


                                     <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
					


				</div>
			</div>



		</div>






    

</asp:Content>
