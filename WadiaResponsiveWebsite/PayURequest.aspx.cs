﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

public partial class PayURequest : System.Web.UI.Page
{
    public String key = "";
    public String txnid = "";

    public string amount = "";
    public string productinfo = "";
    public string firstname = "";
    public string lastname = "";
    public string phone = "";
    public string email = "";
    public string udf1 = "";
    public string udf2 = "";
    public string udf3 = "";
    public string udf4 = "";
    public string udf5 = "";
    public string udf6 = "";
    public string udf7 = "";
    public string udf8 = "";
    public string udf9 = "";
    public string udf10 = "";
    public string salt = "";

    public string hash = "";
    public string acceptURL;
    public string declineURL;
    public string exceptionURL;
    public string cancelURL;

    public static DataTable ToDataTable<T>(IList<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int donateOnlineID = 0;
        IList<childDonateOnline> childDonateOnlinedata;

        if (!string.IsNullOrEmpty(Request.QueryString["Id"]))
        {
            donateOnlineID = Convert.ToInt32(Request.QueryString["Id"]);
            
        }

        childDonateOnlinedata = CChildDonateOnline.Instance().GetOnlineDonationInfo(donateOnlineID);

        IList<childDonateOnline> data = childDonateOnlinedata.Where(p => p.Id .Equals(donateOnlineID)).ToList();

        DataTable detailTable = ToDataTable(data);

        amount = (detailTable.Rows[0]["Amount"]).ToString();
        firstname = (detailTable.Rows[0]["ContactPerson"]).ToString();
        phone = (detailTable.Rows[0]["MobileNumber"]).ToString();
        email = (detailTable.Rows[0]["Email"]).ToString();
        lastname = (detailTable.Rows[0]["LastName"]).ToString();
       
        String seprator = "|";
        
        //Live
        //key = "3FJu2b";
        //salt = "ABYPOdc6";

        //key = "VPcm4L";
        //salt = "OmM6jqjz";
        
        key = ConfigurationManager.AppSettings["CKey"];
        salt = ConfigurationManager.AppSettings["CSalt"];
        
        productinfo = "CHILD DONATION";

        udf1 = donateOnlineID.ToString(); // donateOnlineID.ToString();
        udf4 = "";
        //txnid = Guid.NewGuid().ToString();
        //txnid = Now.ToString("yyyyMMdd_HHmmssffff");

        txnid = DateTime.Now.ToString("yyyyMMddHHmmssffff", CultureInfo.InvariantCulture);
        
        string strWebSiteURL = ConfigurationManager.AppSettings["LinkTosite"];

        acceptURL = "http://wadiahospitals.org/PayUResponse.aspx";
        declineURL = "http://wadiahospitals.org/PayUResponse.aspx";
        cancelURL = "http://wadiahospitals.org/PayUResponse.aspx";

        //acceptURL = "http://localhost:1007/PayUResponse.aspx";
        //declineURL = "http://localhost:1007/PayUResponse.aspx";
        //cancelURL = "http://localhost:1007/PayUResponse.aspx";

        String hashStr = key + seprator + txnid + seprator + amount + seprator + productinfo + seprator + firstname + seprator + email + seprator + udf1 + seprator + udf2 + seprator + udf3 + seprator + udf4 + seprator + udf5 + seprator + udf6 + seprator + udf7 + seprator + udf8 + seprator + udf9 + seprator + udf10 + seprator + salt;
        hash = CreateBase64SHA512Hash(hashStr, "ISO-8859-2").ToLower();

    }

    public static string CreateBase64SHA512Hash(string hashTarget, string encoding)
    {
        System.Security.Cryptography.SHA512 sha = new System.Security.Cryptography.SHA512CryptoServiceProvider();
        byte[] targetBytes = System.Text.Encoding.GetEncoding(encoding).GetBytes(hashTarget);
        byte[] hashBytes = sha.ComputeHash(targetBytes);

        StringBuilder sb = new StringBuilder(hashBytes.Length * 2);
        foreach (byte b in hashBytes)
        {
            sb.AppendFormat("{0:x2}", b);
        }
        return sb.ToString();
    }

    //public void onlineDonationInfo(int onlineDonationInfoID)
    //{
    //    //int pageSize = 5;
    //    //int _TotalRowCount = 0;

    //    string _ConStr = ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ConnectionString;
    //    using (SqlConnection con = new SqlConnection(_ConStr))
    //    {

    //        SqlCommand cmd = new SqlCommand("GetOnlineDonationInfo", con);
    //        cmd.CommandType = CommandType.StoredProcedure;

    //        //int startRowNumber = ((onlineDonationInfoID - 1) * pageSize) + 1;
    //        cmd.Parameters.AddWithValue("@DonateOnlineID", onlineDonationInfoID);

    //        //SqlParameter parTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
    //        //parTotalCount.Direction = ParameterDirection.Output;
    //        //cmd.Parameters.Add(parTotalCount);

    //        SqlDataAdapter da = new SqlDataAdapter(cmd);
    //        DataSet ds = new DataSet();
    //        da.Fill(ds);

    //    }
    //}
}