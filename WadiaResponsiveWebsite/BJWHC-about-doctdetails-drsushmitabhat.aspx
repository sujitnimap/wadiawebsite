﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drsushmitabhat.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drsushmitabhat" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
		<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Sushmita Bhatnagar.png" />
<div class="docdetails">
Dr. Sushmita Bhatnagar
<div class="docdetailsdesg">
Professor and Head of Paediatric Surgery


  <br />
MS, M.ch, M.Phil




</div>

</div>
</div>

                    <div class="areaofinterest">


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
 •	<b>Ethicon traveling teacher’s fellowship</b> award in 2008
<br /><br />
•	<b>Lifetime achievement award</b> for work of excellence in Incontinence management in October 2008

<br /><br />•	<b>SIOP scholarship</b> – October 2008

<br /><br />•	<b>Visiting Professor</b> invitation to Nelson Mandela School of Medicine, Durban, South Africa in the Department of Pediatric Surgery and Pediatric Surgical Oncology, 2009.
<br /><br />•	<b>Visiting Professor to Red</b> Cross Hospital, Cape Town, South Africa in the Department of Pediatric Surgery, 2009.
<br /><br />•	<b>“AVANTIKA DHANVANTRI  SEVA SAMMAN”</b> for excellence in services- Ujjain, 2009
<br /><br />•	<b>Datar Award :</b> Best paper in Oncology : SVC Syndrome : January 2010
<br /><br />•	<b>Sushil Pradhan Trust Award :</b> Best paper in Solid tumors : Functional adrenocortical tumors : Sangli, 2011
<br /><br />•	<b>Best poster Award :</b> Functional adrenocortical tumors: SIOP 2011, Auckland, New Zealand. 
<br /><br />	<b>Best Poster Award :</b> CIPA: MCIAPSCON January 2012, Nagpur. 
<br /><br />•	<b>Young Achiever’s Award :</b> April 2012, Mumbai from CAXSA 
<br /><br />•	<b>Best Poster Award :</b> Diffuse infiltrative Lipoblastomatosis – Ashort case series. SIOP 2013 (International conference on Pediatric Oncology), Hong Kong 2013.
    
<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	IAPS – Indian Association of Pediatric Surgeons, Life member
    <br /><br />•	IAP -  Indian Association of Pediatrics, Life Member
    <br /><br />•	IPSO – International Pediatric Surgical Oncology.
    <br /><br />•	FAPSS – Federation of Association of Pediatric Surgeons of SAARC Countries, Life member.
    <br /><br />•	Surgical Oncology Section – IAPS – <b>Secretary</b> & Life Member
    <br /><br />•	Community Oriented Pediatric Surgery – IAPS – Life Member.
    <br /><br />•	Pediatric Urology Chapter of IAPS – Ex-Executive Committee Member(West Zone), Life Member. 
    <br /><br />•	Maharashtra chapter of IAPS – Ex-Secretary,  Life 	member
    <br /><br />•	MPSS – Mumbai Pediatric Surgical Society – Life member
    <br /><br />•	SACCSG – South African Childhood Cancer Study Group – Honorary Life
Member. 
    <br /><br />•	PHO (Pediatric Hematology Oncology) Chapter of IAP (Indian Association of Pediatrics), Life member
    <br /><br />•	AMC (Association of Medical Consultants) –Managing Committee Member,  Life member. 


    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
<b>Thesis supervisor/ reviewer :</b>
<br /><br />
                    <div style="padding-left:30px">
•	Study of short-term prognosis in patients with posterior urethral valves - 2006. 
<br /><br />•	To define the early prognostic factors determining mortality in surgical neonates - 2007
<br /><br />•	Intralesional injection bleomcyin for patients with hemangioma – 2008
</div>

<br />
<b>Clinical projects:
</b><br /><br />
           <div style="padding-left:30px">         
               •	Electromyography in anorectal malformations
<br /><br />•	Uses of electromyography of external anal sphincter in anorectal conditions
<br /><br />•	Anal re-education therapy for correction of fecal incontinence
<br /><br />•	Faradic stimulation in urinary incontinence
<br /><br />•	Low bacteria diet in children with malignant diseases
<br /><br />•	Effect of bleomycin injection sclerotherapy in haemangiomas versus lymphangiomas
</div>
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
         </div>



</asp:Content>
