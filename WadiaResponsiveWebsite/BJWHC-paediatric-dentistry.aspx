﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-paediatric-dentistry.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_dentistry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PAEDIATRIC DENTISTRY </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
Our Paediatric Dentistry department provides comprehensive dental health care for children from birth to adolescence. Children need dental care right from the time the teeth starts erupting, similar to well-baby clinics that ensure normal growth and development throughout growing period. It is a myth that temporary teeth do not require care as they are any way going to fall down and will be replaced by permanent teeth after the age of 6-7 years. In fact good care of temporary teeth ensures healthy permanent teeth. 
                <br />
 <br />

Children are not always able to be patient and cooperative during a dental exam. Our experts know how to examine and treat children in ways that make them comfortable. In addition, they use specially designed equipment in clinics that are arranged and decorated with children in mind.
<br />
 <br />
Our experts are specially trained to manage basic to complex treatments for children, with a focus on pain management and anxiety control. 

 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
We provide comprehensive oral health care that includes preventive as well as curative dental care as follows:
                                <br /><br />
                <div style="padding-left:10px">

•	Infant oral health exams, which include risk assessment for caries in mother and child
<br />
•	Preventive dental care including cleaning and fluoride treatments, as well as nutrition and diet recommendations
<br />
•	Habit counselling (for example, pacifier use and thumb sucking)
<br />
•	Early assessment and treatment for straightening teeth and correcting an improper bite (orthodontics)
<br />
•	Repair of tooth cavities or defects
<br />
•	Diagnosis of oral conditions associated with diseases such as diabetes, congenital heart defect, asthma, hay fever, and attention deficit/ hyperactivity disorder
<br />
•	Management of gum diseases and conditions including ulcers and paediatric periodontal disease
<br />
•	Care for dental injuries (for example, fractured, displaced, or knocked-out teeth








</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

       


		</div>




</asp:Content>
