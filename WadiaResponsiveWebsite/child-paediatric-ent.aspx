﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-ent.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_ent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PAEDIATRIC ENT </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
The hospital has excellent paediatric ENT surgeons who are trained in treatment of childhood disorders of the ears, nose, throat, face and neck. 
                These surgeons work closely with other paediatric specialities including: allergy, anaesthesia, audiology, paediatric medicine, 
                paediatric surgery, cleft palate and craniofacial surgery, and speech therapy. Through this multi-disciplinary approach, we strive to provide the most effective healthcare for our children.



 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
Services provided :
                                                                <br /><br />
                <div style="padding-left:10px">
-	Head and neck cancer and tumour surgery
<br />-	Diagnostic and therapeutic surgery for paediatric airway disorders
<br />-	Cleft lip and palate surgery
<br />-	Laryngeal and tracheal reconstruction
<br />-	Reconstructive surgery of the middle and outer ear
<br />-	Congenital atresia repair
<br />-	Video nasal endoscopy
<br />-	Fiberscopic laryngoscopy
<br />-	Myringotomy
<br />-	Speech fluoroscopy
<br />-	Removal of foreign bodies from the ears, nose and throat
<br />-	Laser laryngoscopy and bronchoscopy
<br />-	Treatment for complex head and neck tumours
<br />-	Treatment of symptoms resulting from cystic fibrosis 
<br />-	Tonsillectomy and adenoidectomy for children with bleeding disorders
<br />-	Routine procedures performed on children with high-risk medical problems
<br />-	Evaluation and treatment for facial trauma
<br />-	Routine and complex surgery for paediatric sinus disease
<br />-	Treatment for craniofacial disorders
<br />-	Treatment for complex airway diseases





</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

        



		</div>
</asp:Content>
