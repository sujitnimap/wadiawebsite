﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OperationStatus;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;
using System.Data;

namespace WadiaResponsiveWebsite
{
		public partial class SponsorARunPayUResponse : System.Web.UI.Page
		{
				public string mihPayId = "";
				string mode = "";
				public string status = "";
				public string unMappedStatus = "";
				public String key = "";
				public String txnid = "";
				public string amount = "";
				public float discount = 0;
				float netAmountDebit = 0;
				public string productinfo = "";
				public string firstname = "";
				public string lastname = "";


				public string phone = "";
				public string email = "";
				string address1 = "";
				string address2 = "";
				string country = "";
				string city = "";
				DateTime addedDate = DateTime.Now;

				string state = "";
				string zipCode = "";

				public string udf1 = "";
				public string udf2 = "";
				public string udf3 = "";
				public string udf4 = "";
				public string udf5 = "";
				public string udf6 = "";
				public string udf7 = "";
				public string udf8 = "";
				public string udf9 = "";
				public string udf10 = "";
				public string salt = "";

				string paymentSource = "";
				string pgType = "";
				string bankRefNum = "";
				string bankCode = "";
				string field9 = "";

				string error = "";
				string errorMessage = "";

				string transactionNumber = "";
				public string hashC = "";
				public string responseHash = "";
				Boolean IsHashValid = false;

				string seprator = "|";

				protected void Page_Load(object sender, EventArgs e)
				{
						Dictionary<string, string> dict = new Dictionary<string, string>();
						IEnumerator en = Request.Form.GetEnumerator();
						while ((en.MoveNext()))
						{
								string xkey = Convert.ToString(en.Current);

								string xval = Request.Form[xkey];
								dict.Add(xkey, xval);
						}

						//gvwAllResponseData.DataSource = dict;
						//gvwAllResponseData.DataBind();

						//Demo
						//salt = "eCwWELxi";

						salt = ConfigurationManager.AppSettings["CSalt"];
						status = dict["status"];
						udf10 = dict["udf10"];
						udf9 = dict["udf9"];
						udf8 = dict["udf8"];
						udf7 = dict["udf7"];
						udf6 = dict["udf6"];
						udf5 = dict["udf5"];
						udf4 = dict["udf4"];
						udf3 = dict["udf3"];
						udf2 = dict["udf2"];
						udf1 = dict["udf1"];
						email = dict["email"];
						phone = dict["phone"];
						firstname = dict["firstname"];
						productinfo = dict["productinfo"];
						amount = dict["amount"];
						txnid = dict["txnid"];
						key = dict["key"];
						transactionNumber = dict["txnid"];
						mihPayId = dict["mihpayid"];

						country = dict["country"];
						city = dict["city"];
						errorMessage = dict["error_Message"];
						error = dict["error"];
						mode = dict["mode"];

						netAmountDebit = float.Parse(dict["net_amount_debit"]);
						address1 = dict["address1"];
						paymentSource = dict["payment_source"];
						bankRefNum = dict["bank_ref_num"];
						bankCode = dict["bankcode"];
						pgType = dict["PG_TYPE"];

						responseHash = dict["hash"];

						String hashStr = salt + seprator + status + seprator + udf10 + seprator + udf9 + seprator + udf8 + seprator + udf7 + seprator + udf6 + seprator + udf5 + seprator + udf4 + seprator + udf3 + seprator + udf2 + seprator + udf1 + seprator + email + seprator + firstname + seprator + productinfo + seprator + amount + seprator + txnid + seprator + key;
						hashC = CreateBase64SHA512Hash(hashStr, "ISO-8859-2").ToLower();

						if (responseHash == hashC)
						{
								IsHashValid = true;
						}
						else
						{
								IsHashValid = false;
						}

						paymentDetail objpaymentDetail = new paymentDetail();
						COperationStatus os = new COperationStatus();
						objpaymentDetail.ChildDonateOnlineID = Convert.ToInt32(udf1);
						objpaymentDetail.AddedDate = addedDate;
						objpaymentDetail.ProdInfo = productinfo;
						objpaymentDetail.MerchantKey = key;
						objpaymentDetail.TransationID = txnid;
						objpaymentDetail.MihPayId = mihPayId;
						objpaymentDetail.IsHashValid = IsHashValid;

						objpaymentDetail.FirstName = firstname;
						objpaymentDetail.LastName = lastname;
						objpaymentDetail.Amount = Convert.ToDouble(amount);
						objpaymentDetail.Email = email;
						objpaymentDetail.Phone = phone;
						objpaymentDetail.hash = responseHash;
						objpaymentDetail.Status = status;

						objpaymentDetail.NetAmountDebit = netAmountDebit;
						objpaymentDetail.Address1 = address1;
						objpaymentDetail.PaymentSource = paymentSource;
						objpaymentDetail.BankRefNum = bankRefNum;
						objpaymentDetail.BankCode = bankCode;
						objpaymentDetail.PGType = pgType;
						objpaymentDetail.Mode = mode;
						objpaymentDetail.UnMappedStatus = unMappedStatus;


						objpaymentDetail.Country = "";
						objpaymentDetail.City = city;
						objpaymentDetail.State = state;
						objpaymentDetail.ZipCode = zipCode;
						objpaymentDetail.Address2 = address2;
						objpaymentDetail.Error = error;
						objpaymentDetail.ErrorMessage = errorMessage;

						objpaymentDetail.UDF1 = udf1;
						objpaymentDetail.UDF2 = udf2;
						objpaymentDetail.UDF3 = udf3;
						objpaymentDetail.UDF4 = udf4;
						objpaymentDetail.UDF5 = udf5;
						objpaymentDetail.UDF6 = udf6;
						objpaymentDetail.UDF7 = udf7;
						objpaymentDetail.UDF8 = udf8;
						objpaymentDetail.UDF9 = udf9;
						objpaymentDetail.UDF10 = udf10;


						objpaymentDetail.Field1 = "";
						objpaymentDetail.Field2 = "";
						objpaymentDetail.Field3 = "";
						objpaymentDetail.Field4 = "";
						objpaymentDetail.Field5 = "";
						objpaymentDetail.Field6 = "";
						objpaymentDetail.Field7 = "";
						objpaymentDetail.Field8 = "";
						objpaymentDetail.Field9 = field9;

						os = CPaymentDetail.Instance().InsertPaymentDetail(objpaymentDetail);
						lblAddedSuccessfully.Text = os.Success.ToString();

						if (IsHashValid && status == "success")
						{
								lblPaymentSuccessMessage.Text = "Transaction Successfull (Note : Copy of invoice to be printed)";

								lblTransactionNumber.Text = transactionNumber;


								IList<sponsorARunForMarathon> SponsARunForMarathon = CSponsorARunForMarathon.Instance().GetSponsorARunForMarathonInfo(Convert.ToInt32(udf1));
								DataTable dtsponsor = new DataTable();
								dtsponsor = CollectionHelper.GetDataTable(SponsARunForMarathon);


								#region BodyHeader

								//string body = "Hellow";

								string body = @"<table width='800px' border='1' align='center' cellpadding='0' cellspacing='0'>
												<tr>
														<td valign='top' class='contentbig' style='padding: 20px'>
																<table width='100%' border='0' cellspacing='0' cellpadding='0'>
																		<tr>
																				<td width='22%' valign='top'>
																						<img src='http://wadiahospitals.org/images/Logo.png' width='100' height='100' />
																				</td>
																				<td width='50%' valign='top' style='padding-left: 15px'>
																						<table width='99%' border='0' cellspacing='0' cellpadding='0'>
																								<tr>
																										<td height='39'>
																												<h2 class='contentheader'>
																														Wadia Hospital</h2>
																										</td>
																								</tr>
																								<tr>
																										<td height='78' class='content' valign='top'>
																										Bai Jerbai Wadia Hospital for Children, 
																										<br />
																										Acharya Donde Marg, Lower Parel,
																										<br />
																										Mumbai, Maharashtra,India, 400 012                                                     
																										</td>
																								</tr>
																						</table>
																				</td>
																				<td width='35%' style='padding-left: 15px' valign='top'>
																						<table width='100%' border='0' cellspacing='0' cellpadding='0'>                                                
																								<tr>
																										<td height='27' class='content'>
																												Date
																										</td>
																										<td>
																												<strong>: </strong>
																										</td>
																										<td>"
																													 + DateTime.Now +
																										@"</td>
																								</tr>
																						</table>
																				</td>
																		</tr>
																</table>

<table>
<tr>
<td>
<br />
Registration No. : <strong> " + udf1.ToString() + @"</strong>
<br />
Online Registration e-invoice <strong> " + transactionNumber + @"</strong> </br>

Thanks to received an amount of Rs." + dtsponsor.Rows[0]["AmountInINR"].ToString() + @" towards Participation/Donation/Sponsor a Run, for Little </br>

Hearts Marathon 2016 scheduled on 7th February 2016.

</td>
</tr>
</table>
			 <br />                 
<p>
																		<strong>USER INFORMATION : </strong>
																		<br />
																</p>
																<table width='100%' border='1' cellspacing='0' cellpadding='0'>
																		<tr>
																				<td height='24' colspan='2' class='contenthead' style='background-color: #CCC; padding-left: 5px;'>
																						Billing Address
																				</td>
																		</tr>
																		<tr>
																				<td class='content' height='30' style='padding-left: 5px'>
																						Name Of Organisation<strong> :</strong>
																				</td>
																				<td width='70%'>"
                                                                                                + dtsponsor.Rows[0]["NameOfOrganisation"].ToString() +
																					 @"</td>
																		</tr>
																		<tr>
																				<td class='content' height='30' style='padding-left: 5px'>
																						Name <strong>:</strong>
																				</td>
																				<td>"
																								+ dtsponsor.Rows[0]["FirstName"].ToString() + " " + dtsponsor.Rows[0]["LastName"].ToString() +
																					@"</td>
																		</tr>
																		<tr>
																				<td class='content' height='30' style='padding-left: 5px'>
																						Address <strong>:</strong>
																				</td>
																				<td>"
																								+ dtsponsor.Rows[0]["Address"].ToString() + 
																					@"</td>
																		</tr>
																		<tr>
																				<td class='content' height='30' style='padding-left: 5px'>
																						Total Amount <strong>:</strong>
																				</td>
																				<td>"
																								+ dtsponsor.Rows[0]["AmountInINR"].ToString() +
																					@"</td>
																		</tr>
																		<tr>
																				<td class='content' height='30' style='padding-left: 5px'>
																						Mobile Number <strong>:</strong>
																				</td>
																				<td>"
																								+ dtsponsor.Rows[0]["MobileNumber1"].ToString() +
																					@"</td>
																		</tr>
																</table>";

								body += @"<tr>
 </tr>
												</table>
													<br />       
<p>
																		<strong> DETAILS OF EVENT : </strong>
																		<br />
																</p>

<table>
<tr>
<td>
<strong>Date </strong> : 7th February 2016
</td>
</tr>

<tr>
<td>
<strong>Time</strong>: 8 AM
</td>
</tr>

<tr>
<td>
<strong>Venue</strong>: Marathon starts from Siddhivinayak Ganapati, Temple, SK Bole Marg, 
</br></br>
Prabhadevi, Mumbai, 400028
</br></br>
Marathon finishes at Bai Jerbai Wadia Hospital for Children, Acharya Donde Marg, Parel, 
</br></br>
Mumbai, 400012
</br>
</td>
</tr>
</table>

</br></br>
<table>  
<tr>
<td>  
<strong> Terms, conditions and guidelines : </strong>
</br>
</td>
</tr>

<tr>
<td>
• Please note Last Date of registration 31st January 2016 till midnight subject to availability. The organiser 
</br>
reserves the right to change the dates.

</br>
• <strong> Fitness: </strong>
<p>
 &nbsp;&nbsp;&nbsp; • In case of any illness or disability please mail Fitness Certificate from your treating certified Doctor 
to <a href='#'><u>marathon@wadiahospitals.org</u></a>
</br>
 &nbsp;&nbsp;&nbsp; • Participants must be fit to run and free from injury
</p>
</br>
• <strong> Participation: </strong>
<p>
 &nbsp;&nbsp;&nbsp; • The child's age should be from 3 to 17 years (For age group 3 to 12 it is mandatory to run with 
parent/guardian who will take the responsibility of the child throughout the Marathon to avoid any 
incident.)
</br>
 &nbsp;&nbsp;&nbsp; • For all participants need to confirm their names
<br>
The Little Hearts Marathon is an event to spread awareness on Cardiac diseases: Prevention of cardiac diseases
in children and also a fund raising event for the mentioned cause.
</p>
<br>

• Marathon will start from Nardulla Tank behind Siddhivinayak Temple Prabhadevi to Bai Jerbai Wadia Hospital
for Children and all children will get certificates; there will not be any competitive prizes.
</br>
• All registration fees are non-refundable. In the event you cannot participate, please consider your registration
fee as generous donation towards “Little Hearts Marathon” alternatively you may choose to sponsor a run by 
writing a mail to us before 2nd February 2016 on <u>marathon@wadiahospitals.org</u>
</br>
• It is mandatory for all confirmed participants to come to the Nowrosjee Wadia Maternity on 5th and 6th
February 2016 between 10 am to 4 pm to collect T-shirts along with valid photo ID and copy of invoice.
</br>
• Information against all mandatory fields prefixed with ‘*’ on the entry form must be filled. Incomplete forms
will be rejected.
</br>
• This Entry Form and the right to participate in the event and the rights and benefits available to the applicant
under this application form is at the sole discretion of Little Hearts Marathon and cannot be transferred to any 
other person under any circumstances, and the applicant alone shall be entitled to the rights and benefits 
arising hereunder.
</br>
• Please go through all information mentioned in this website prior to submitting your application. Confirmation
of application received will be subject to the applicable qualification criteria and entry rules & Guidelines given 
herein.
</br>
• You can collect Goody bags with valid coupons only; participants without coupons will not be entertained.
</br>

<strong>I declare, confirm and agree as follows and I/my word </strong>
</br>
(i) I agree to the entry terms and guidelines. 
</br>
(ii) The information given by me in this entry form is true and I am solely responsible for the accuracy of this information; 
</br>
(iii) Have fully understood the risk and responsibility of participating in the event and will be participating entirely at my own risk and responsibility; 
</br>
(iv) The security of my child is solely the responsibility of the accompanying parent/ guardian. The Organisers shall not be held responsible for any security related incident that may occur during the course of the marathon. 
</br>
(v) Any illness/ medical condition/ ailment have to be reported to the Organisers in detail. If the parents fail to intimate the exact medical condition of the child, orself; then the Organisers shall not be held responsible for any mishap that may occur or arise due to the such illness/ medical condition/ ailment. 
</br>
(vi) Understand the risk of participating on a course with vehicular traffic, even if the course may be regulated / policed; 
</br>
(vii) Understand that I must be of, and must train to, an appropriate level of fitness to participate in such a physically demanding event and I have obtained a medical clearance from a registered medical practitioner, allowing me to participate in the event/s; 
</br>
(viii) The parents/ guardians are required to strictly follow the instructions given by the Organisers and incase of the difficulty forthwith report to the Organisers. 
</br>
(ix) For myself/ourselves and our legal representatives, waive all claims of whatsoever nature against any and all Sponsors of the event, all political entities, authorities and officials, all contractors and construction firms working on or near the course, all Committee persons, officials and volunteers, Little Hearts Marathon, Wadia Hospitals, and all other persons and entities associated with the event and the directors, employees, agents and representatives of all or any of the aforementioned including, but not limited to, any claims that might result from me participating in the event and whether on account of illness, injury, death or otherwise; 
</br>
(x) Agree that if I am injured or taken ill or otherwise suffer any detriment whatsoever, I hereby irrevocably authorize the event officials and organizers to, at my own risk and cost, transport me to a medical facility and/or to administer emergency medical treatment and I waive all claims that might result from such transport and/or treatment or delay or deficiency therein. I shall pay or reimburse to you my medical and emergency expenses and I hereby authorize/s you to incur the same; 
</br>
(xi) Shall provide to race officials such medical data relating to me as they may request. I agree that nothing herein shall oblige the event officials or organizers or any other person to incur any expense or to provide any transport or treatment; 
</br>
(xii) In case of any illness or injury caused to me or death suffered by me or my due to any force majeure event including but not limited to fire, riots or other civil disturbances, earthquakes, storms, typhoons or any terrorist act, none of the sponsors of the event or any political entity or authorities and officials or any contractor or construction firms working on or near the course, or any of the Man and Mountain Meet persons, officials or volunteers, Wadia Hospitals, Little Hearts Marathon or any persons or entities associated with the event or the directors, employees, agents or representatives of all or any of the aforementioned shall be held liable by me or my representatives; 
</br>
(xiii) Understand, agree and irrevocably permit Wadia Hospitals and Little Hearts Marathon to share the information given by me in this application, with all/any entities associated with this event, at its own discretion
</br>
(xiv) Understand, agree and irrevocably permit Wadia Hospitals and Little Hearts Marathon to my photograph and record (on film, tape, hard drive, or any other method) my performance, appearance, name and/or voice and the results in perpetuity for the purpose of promoting the Event, at its own discretion.
</br>
(xv) Further authorize Wadia Hospitals and Little Hearts Marathon to edit the same at discretion and to include it with performance of other and with sound effects, special effects, music etc. in any manner or media whatsoever, including without limitation, unrestricted use for purpose of publicity, advertising and sales promotion; and to use my name. Wadia Hospitals and Little Hearts Marathon owns all rights to the results and proceeds of my services in connection herewith; 
</br>
(xvi) I understand that it is at the discretion of Wadia Hospitals and Little Hearts Marathon to make any changes in the terms conditions and guidelines for the event.
</br>

</td>
<tr>
												</table>";

                #endregion

                CollectionHelper.MarathonsendMail(email , "Marathon Sponsorarun", "Dear Sir/Madam ,", body);

								CollectionHelper.MarathonsendMail("lhm2015registrations@wadiahospitals.org", "Marathon Sponsorarun", "Dear Sir/Madam ,", body);

								dataDiv.InnerHtml = body;
						}
						else
						{
								lblPaymentSuccessMessage.Text = "Fail";
								lblTransactionNumber.Text = transactionNumber;

						}
				}

				public static string CreateBase64SHA512Hash(string hashTarget, string encoding)
				{
						System.Security.Cryptography.SHA512 sha = new System.Security.Cryptography.SHA512CryptoServiceProvider();
						byte[] targetBytes = System.Text.Encoding.GetEncoding(encoding).GetBytes(hashTarget);
						byte[] hashBytes = sha.ComputeHash(targetBytes);

						StringBuilder sb = new StringBuilder(hashBytes.Length * 2);
						foreach (byte b in hashBytes)
						{
								sb.AppendFormat("{0:x2}", b);
						}
						return sb.ToString();
				}
		}
}