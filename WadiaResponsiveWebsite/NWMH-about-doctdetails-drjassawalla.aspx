﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-about-doctdetails-drjassawalla.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_doctdetails_drjassawalla" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



<div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dr.jpg" />
<div class="docdetails">
Dr. Jassawalla Mehernosh Jamshed



<div class="docdetailsdesg">
Medical Director, NWMH    <br />
MBBS, MD, FCPS, DGO, DFP, FICOG, FIAJAGO, FICMCH, FRSH, FICA


</div>

</div>
</div>

     <div class="areaofinterest">

Dr. Jassawalla, has more than 25 years of undergraduate and postgraduate teaching experience.  He has been an examiner in Obstetrics and Gynaecology at Mumbai University, Gujarat University, SNDT and College of Physicians and Surgeons. Apart from authoring several award winning articles and books, he has also gained immense experience and training at institutes within India and abroad.

</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
    •	Awarded first prize for the paper entitled ‘Epidemiology of prematurity’ at a competition organized by the Association of the Medical Women in India.
<br />•	Dr. M.S. Shah Prize for a scientific paper entitled ‘Role of cervical mucus in the evaluation of ovarian factor in infertility’ presented at the XIIth annual conference of MOGGS
<br />•	Dr. K.M. Masani Prize for a scientific paper titled ‘Service at your doorstep’ presented at the XIth annual conference of MOGS
<br />•	Dr. K.M. Masani Prize for a scientific paper titled ‘Maternity case monitoring, a stepping stone to success by the year 2000 AD’ a prospective study of 1500 cases at NWMH’.
<br />•	Dr. K.M. Masani Prize for a scientific paper titled ‘Epidemiology of low birth weight babies’ a prospective study of 1440 cases at NWMH’.
<br />•	Dr. M.S Shah Travelling Fellowship, 1984
<br />•	The FOGSI Ethicon Travelling Fellowship, 1984
<br />•	Award of appreciation given by the Baroda Obstetric & Gynaecological Society for delivering a guest lecture on the Role of Intra-Amniotic saline in 2nd Trimester 
<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
     •	Life member of The Federation of Obstetrics and Gynaecological Societies of India
<br />•	Life member of The Indian Association of Gynaecological Endoscopy
<br />•	Life member of The Indian Association of Fertility and Sterility
<br />•	Life member of The Indian Society of Perinatology and Reproductive Biology
<br />•	Life member of The Indian Academy of Juvenile and Adolescent Obstetrics and Gynaecology
<br />•	Life member of The Indian Medical Association
<br />•	Life member of The National Association of Voluntary Sterlization in India
<br />•	Life member of The Assocation of Medical Consultants in India
<br />•	Life member of The ‘D’ Ward Medical Association
<br />•	Founder fellow of The Indian College of Maternal and Child Health
<br />•	Founder fellow of The Indian College of Obstetrics and Gynaecology
<br />•	On the Governing Council of the College of Physicians and Surgeons
<br />•	On the Editorial Board of the Journal of Obstetric and Gynaecology of India
<br />•	Chief Editor of the Indian Society of Perinatology and reproductive Biology (1988 – 1989)
<br /><br />
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	More than 85 scientific papers presented at various national and international conferences from 1978 till date
<br />•	More than 45 scientific publications in both national and international journals
<br />•	Numerous book chapters published in various textbooks and manuals till date.
<br /><br />
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			




	





</asp:Content>
