﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-PICU.aspx.cs" Inherits="WadiaResponsiveWebsite.child_picu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>THE PAEDIATRIC INTENSIVE CARE UNIT(PICU)</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
The Paediatric Intensive Care Unit (PICU) is a multidisciplinary unit that provides care for infants, children and adolescents who become critically ill or injured. The PICU is well equipped for managing critically ill children with medical and surgical emergencies. Children are often referred to the PICU from other hospitals in the city and beyond.
                <br />
PICU is able to handle all types of emergencies and critically ill children of all age groups. It has state-of-art facilities that include availability of high frequency ventilation, various invasive procedures, 24 hour laboratory and radiological facili  ties with instant reporting and the unit is manned by intensivist and fellows who are able to provide specialised care round the clock.   

                <br /><br />


                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
                The PICU offers facilities such as :
                <br /><br />
Use of multipara machines to monitor the following noninvasively :

                <div style="padding-left:20px">
-	Blood Pressure
<br />-	End Tidal Carbon-dioxide concentration
<br />-	Heart Rate
<br />-	Oxygenation
<br />-	Respiratory Rate
</div>
<br />
Invasive monitoring of arterial blood pressure and central venous pressure is also performed at the PICU.

 <br />
 <br />

              The PICU offers level-III care and monitoring to patients suffering from 

                <br /><br />
                <div style="padding-left:10px">
-	Breathing and other respiratory disorders
<br />-	Dengue Fevers
<br />-	Heart failure
<br />-	HIV
<br />-	Liver failure
<br />-	Malaria
<br />-	Pneumonia
<br />-	Renal failure
<br />-	Tuberculosis

</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

        


		</div>




</asp:Content>
