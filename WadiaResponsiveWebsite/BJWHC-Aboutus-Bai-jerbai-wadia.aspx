﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-Aboutus-Bai-jerbai-wadia.aspx.cs" Inherits="WadiaResponsiveWebsite.child_jerbai_wadia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-2-3">
            <div class="wrap-col">

                <div class="heading">
                    <h2>HISTORY</h2>
                </div>

                <br />

                <!-- ABOUT US CONTENT -->

                <div class="doctorbox">

                    <img src="images/doc/Bai Jerbai Wadia.png" />
                    <div class="docdetails">
                        Bai Jerbai Wadia (1852 – 1926)

                    </div>
                </div>

                <div class="historydetails">
                    Born to a traditional family in 1852, Bai Jerbai Wadia was raised in the true Zoroastrian way of life, to become a sophisticated member of society. However, despite her affluent background and her family’s aspirations, Jerbai’s heart was otherwise inclined. Following the tenet of Zoroastrian philosophy, she wanted to immerse herself in providing service to the underprivileged of the community and work towards empowering and uplifting them.<br />
                    <br />
                    Bai Jerbai Wadia dedicated her life to philanthropic activities. She supervised the building of low-cost housing colonies for Zoroastrian aspirants looking for opportunities in Mumbai. She established the Nowrosjee Nusserwanjee Wadia Trust and Rustomjee Nowrosjee Wadia Trust fund for the building and maintenance of more such colonies for the Parsees. Her philanthropic endeavours also ensured provision of adequate healthcare for women and helped build several clinics and hospitals around the city.

                </div>
                <!-- ABOUT US CONTENT -->





            </div>
        </div>



        <div class="col-1-3">
            <div class="wrap-col">

                <div class="box">
                    <div class="heading">
                        <h2>About Us</h2>
                    </div>
                    <div class="content">
                        <div class="list">
                            <ul>


                                <li><a href="BJWHC-aboutus-overview.aspx">Overview</a></li>
                                <li><a href="BJWHC-aboutus-mission-vision-corevalues.aspx">Mission, Vision, Core Values</a></li>
                                <li><a href="BJWHC-Aboutus-Chairman's-Message.aspx">Chairman's message</a></li>
                                <%--<li><a href="BJWHC-Aboutus-Awards-Achievements.aspx">Awards and Achievements</a></li>--%>
                                <li><a href="BJWHC-aboutus-history.aspx">History </a>

                                    <div class="leftpadsubmenu">

                                        <img style="padding-top: 5px" src="images/submenu.jpg" />
                                        <a href="BJWHC-Aboutus-Bai-jerbai-wadia.aspx">Bai Jerbai Wadia </a>
                                        <br />
                                        <img style="padding-top: 5px" src="images/submenu.jpg" />
                                        <a href="child-cursetjee-wadia.aspx">Sir Cusrow Wadia </a>
                                        <br />
                                        <img style="padding-top: 5px" src="images/submenu.jpg" />
                                        <a href="BJWHC-Aboutus-Sir-ness-wadia.aspx">Sir Ness Wadia </a>

                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
            </div>
        </div>



    </div>






</asp:Content>
