﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drfaram.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drfaram" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dr.jpg" />
<div class="docdetails">
Purnima Satoskar


<div class="docdetailsdesg">
Professor
    <br />
MD, DNB, MRCOG



</div>

</div>
</div>

     <div class="areaofinterest">

Areas of Interest : Foetal Medicine including interventional ultrasonography, High-risk obstetrics, Hysterolaparoscopic surgery

</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research or publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
    •	Hargobind Foundation Medical Fellowship for the year 2000 for studying Prenatal Diagnosis at Beth Israel Deaconess Medical Center, Harvard Medical College, Boston USA.
<br />•	FOGSI Ethicon Travelling Fellowship for the year 2000.
<br />•	FOGSI Imaging Science Award for the year 2002
<br />•	FOGSI Rhogam Grant 2002.
<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
    •	Member Representative, AICC RCOG Western Zone of India
<br />•	Member, Managing Committee, Mumbai Obstetric and Gynaecological Society,
<br />•	Member, Managing Committee, Indian Society of Prenatal Diagnosis and Therapy
<br />•	Member, Board of Trustees, Association of Medical Women in India, Mumbai Branch
<br />•	Consultant Obstetrician and Gynaecologist, Jaslok Hospital and Research Centre

<br /><br />
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Satoskar P.R. , Deshpande A.D.  The value of ultrasonography in the diagnosis of adnexal masses J. Postgraduate Med., 1990; 37: 35-9.
<br />•	Pradhan S.R., Salvi V.S., Satoskar P.R. Does Caesarean Section alter neonatal outcome in low birth weight babies? J of Obstet Gynaecol of India, 1998; 48: 2, 51 – 55.
<br />•	Gupta A, Lokhandwala Y.Y., Satoskar P.R., Salvi V.S. Balloon Mitral Valvotomy in Pregnancy Maternal and Fetal Outcome : J Am Col Surg 1998, 187 : 409 – 415.
<br />•	Satoskar PR, Salvi VS. Operative Intervention for delivery of the dead foetus: Bombay Hospital J 41, No. 2, 273 – 275: 1999.
<br />•	Sheikh S, Satoskar P, Bhartiya D Expression of Insulin-like growth factor-1 and placental growth hormone mRNA in placentae : a comparison between normal and intrauterine growth retardation pregnancies Mol Human Reprod Vol 7, No. 3, 287 – 292 , 2001.
<br />•	Colah R., Surve R., Nadkarni A., Phanasgaonkar S., Satoskar P, Mohanty D. Prenatal diagnosis of sickle syndromes in India: dilemmas in counseling. Prenat Diagn 25:345-349, 200
<br />•	Vora S, Shetty S, Salvi V, Satoskar P, Ghosh K A comprehensive screening analysis of antiphospholipid antibodies in Indian women with fetal loss.  Eur J Obstet Gynecol Reprod Biol 2007 Jul 16 (Epub ahead of print)
<br />•	Vora S, Ghosh K, Shetty S, Salvi V, Satoskar P Deep venous thrombosis in the antenatal period in a large cohort of pregnancies from western India. Thrombosis Journal, 5:9, 2007
<br />•	Satoskar P:Management of severe preeclampsia and eclampsia The Journal of General Medicine Vol 19; 4 16-18, 2007
<br />•	Satoskar P, Doshi K, Dalmia R,  Bansal V,  Dastur A: Changing trends in the management of Rh isoimmunization over a 14 year period in a tertiary institution ;1(1):23-25, 2010
<br />•	Rucha Patil, Kanjaksha Ghosh, Purnima Satoskar, Shrimati Shetty: Elevated procoagulant endothelial and tissue factor expressing microparticles in women with recurrent pregnancy loss.;PLoS One 2013 20;8(11):e81407. Epub 2013 Nov 20.
<br />•	Anirudh B Badade, Amar Bhide, Purnima Satoskar, Darshan Wadekar: Validation of the global reference for fetal weight and birth weight percentiles. Indian J Radiol Imaging 2013 Jul;23(3):266-8
<br /><br />





<b>Brief Reports </b>
<br /><br />
•	Satoskar PR, Patkar V.D., Badhwar V.R. Abdominal Colposacropexy as a primary operation for massive procidentia. Int. J. of Gynaecol and Obstet (Ireland), 1993, 43 (2) : 196 – 7.
<br />•	Satoskar P.R., Chavan N., Shah P.K., Foetal Interlocking complicating twin pregnancy accepted for publication in J. Postgraduate Medicine 1994.	
<br />•	Satoskar P.R., Badhwar V.R. Pregnancy with scleroderma. Bombay Hospital J., 1997, 39: 2, 372 – 3.
<br />•	Satoskar P.R., Salvi V.S., Bharucha B. Blepharophimosis, Ptosis, Epicanthus inversus and secondary amenorrhoea – a rare syndrome. Bombay Hospital J., 1997, 39: 4, 762 – 3.
<br />•	Pradhan S.R., Salvi V.S., Satoskar P.R., Fallopian tube carcinoma – a case report. Bombay Hospital J., 1998, 40: 2, 299 – 300.
<br />•	Satoskar P.R. fetus – in fetu: Ultrasonographic prenatal diagnosis Bombay Hosp J 45; 1, 170-171, 2003.
<br />•	Balsarkar G, Satoskar P: Leaking PV posthysterectomy – a clinical dilemma Bombay Hosp J 45: 1, 156-157, 2003.
<br />•	Balsarkar G.,Burde G, Jassawalla M J, Satoskar P : Unusual ultrasonographic appearance of a dermoid Journal of Obstetrics and Gynecology of India  52:5,73,2002
<br />•	Binti R Bhatiyani, Dhvani M Lalan, Purnima R Satoskar: Partial mole with live fetus Bombay Hospital Journal Vol 47.No.3, 2005 
<br />•	Doshi Kausha, Dalmia R, Satoskar P: Placenta accrete An obstetrician’s nightmare (abstract) The Journal of General Medicine Vol 19; 4 46, 2007
<br />•	Satoskar P. R. : Management of severe preeclampsia and eclampsia The Journal of General Medicine Vol 19; 4, 16-18 2007
<br />•	Y.Kazi & P. Satoskar: Meckel-Gruber Syndrome: A Case Report. The Internet Journal of Gynecology and Obstetrics. Vol 12 No 1, 2009

<br /><br />

<b>Chapters </b>
                    
                    <br /><br />	
•	Satoskar P.R. ‘Sudden Postpartum Collapse’ in Krishna, Tank, Daftary (Eds) 3rd Ed, Pregnancy at Risk, Jaypee, New Delhi, India, 1997, 449-455.
 <br />•	Satoskar P.R. ‘The Neuroendocrine System’ in Walvekar VR, Jassawalla MJ, Anjaria PH, Wani RJ (Ed), 2nd Ed, Reproductive Endocrinology, Jaypee New Delhi, India, 2001.
 <br />•	Satoskar P.R. “Acute Renal Failure in Obstetrics’ in ‘Medical and Surgical Disorders in Pregnancy’ Dr. Vinita Salvi (Ed), 1st Ed, Jaypee, New Delhi, India 2003.
 <br /> <br />


















                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			



</asp:Content>





</asp:Content>
