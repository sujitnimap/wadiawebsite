﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-Testimonials.aspx.cs" Inherits="WadiaResponsiveWebsite.child_testimonials" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>TESTIMONIALS</h2>
                </div>
                <br />
                <div align="center">
                    <table style="width: 50%">
                        <tr>
                            <td>
                                <div align="center">
                                    <asp:LinkButton ID="lnbtnContributors" PostBackUrl="~/child-testimonials.aspx?catnm=Contributors" runat="server" OnClick="lnbtnContributors_Click">Contributors</asp:LinkButton>
                                </div>
                            </td>
                            <td>|</td>
                            <td>
                                <div align="center">
                                    <asp:LinkButton ID="lnbtnPatients" PostBackUrl="~/child-testimonials.aspx?catnm=Patients" runat="server" OnClick="lnbtnPatients_Click"> Patients</asp:LinkButton>
                                </div>
                            </td>
                            <%--<td>|</td>
                            <td>
                                <div align="center">
                                    <asp:LinkButton ID="lnbtnStudents" PostBackUrl="~/child-testimonials.aspx?catnm=Students" runat="server" OnClick="lnbtnStudents_Click">Students</asp:LinkButton>
                                </div>
                            </td>--%>
                            <td>|</td>
                            <td>
                                <div align="center">
                                    <asp:LinkButton ID="lnbtnArchives" PostBackUrl="~/child-testimonials.aspx?catnm=Archives" runat="server" OnClick="lnbtnArchives_Click">Archives</asp:LinkButton>
                                </div>
                            </td>
                            <%--<td>|</td>
                            <td>
                                <div align="center">
                                    <asp:LinkButton ID="lnbtnVideotestimonials" PostBackUrl="~/child-testimonials.aspx?catnm=Video testimonials" runat="server" OnClick="lnbtnVideotestimonials_Click">Video testimonials</asp:LinkButton>
                                </div>
                            </td>--%>
                        </tr>
                    </table>
                </div>

                <div id="container">
                    <asp:Panel ID="pnlalltestimonials" runat="server">
                        <div class="pagination">
                            
                            <asp:DataList class="page gradient" RepeatDirection="Horizontal" runat="server" ID="dlPagerup"
                                OnItemCommand="dlPagerup_ItemCommand">
                                <ItemTemplate>

                                    <a id="pageno" runat="server" >
                                        <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>
                                    </a>

                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div class="devider_20px"></div>
                        <asp:DataList ID="dtlisttestimonials" runat="server" Width="100%" RepeatColumns="1">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 10%" valign="top">
                                            <div class="left">
                                                <asp:Image ID="imgtestimonial" Width="100px" ImageUrl='<%# "adminpanel/" + Eval("imageurl") %>' Height="100px" runat="server" />
                                            </div>
                                        </td>
                                        <td style="width: 5%; padding-right:7px;" valign="top">
                                            <div align="right">
                                                <img width="25px" height="25px" src="images/opening-coma.png">
                                            </div>
                                        </td>
                                        <td style="width: 85%; padding-top:7px;">
                                            <div class="left">
                                                <div>
                                                    <asp:Label ID="lbltestimonial" runat="server" Text='<%#Eval("testimonialdescrp") %>'></asp:Label>
                                                   <div class="devider_10px"></div>
                                                    <asp:Label ID="lblname" Font-Bold="true" Font-Size="12px" Font-Italic="true" runat="server" Text='<%#Eval("createby") %>'></asp:Label>
                                                    |
                                                    <asp:Label ID="lbldesignation" runat="server" Font-Size="12px" Font-Italic="true" Text='<%#Eval("designation") %>'></asp:Label>
                                                </div>
                                                <%--<img width="25px" height="25px" src="images/closing-coma.png">--%>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </ItemTemplate>
                        </asp:DataList>

                        <br />

                        <div class="pagination">

                            <asp:DataList class="page gradient" RepeatDirection="Horizontal" runat="server" ID="dlPagerdown"
                                OnItemCommand="dlPagerdown_ItemCommand">
                                <ItemTemplate>

                                    <a id="pageno" runat="server">

                                        <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>

                                    </a>

                                </ItemTemplate>
                            </asp:DataList>
                        </div>

                    </asp:Panel>


                    <asp:Panel ID="pnlvideotestimonial" Visible="false" runat="server">
                        <div class="pagination">
                            <asp:DataList class="page gradient" RepeatDirection="Horizontal" runat="server" ID="ddlVideoPagerup"
                                OnItemCommand="ddlVideoPagerup_ItemCommand">
                                <ItemTemplate>

                                    <a id="pageno" runat="server">
                                        <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkVideoPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>

                                    </a>

                                </ItemTemplate>
                            </asp:DataList>
                        </div>

                        <asp:DataList ID="dtlistvideotestimonials" runat="server" Width="100%" RepeatDirection="Horizontal" RepeatColumns="2">
                            <ItemTemplate>
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            <div class="left">

                                                <asp:Image ID="imgtestimonial" Width="200px" ImageUrl='<%# "adminpanel/" + Eval("imageurl") %>' Height="200px" runat="server" />

                                            </div>
                                        </td>
                                    </tr>

                                </table>
                            </ItemTemplate>
                        </asp:DataList>

                        <br />

                        <div class="pagination">
                            <asp:DataList class="page gradient" RepeatDirection="Horizontal" runat="server" ID="ddlVideoPagerdown"
                                OnItemCommand="ddlVideoPagerdown_ItemCommand">
                                <ItemTemplate>

                                    <a id="pageno" runat="server">
                                        <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkVideoPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>

                                    </a>

                                </ItemTemplate>
                            </asp:DataList>
                        </div>

                    </asp:Panel>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
