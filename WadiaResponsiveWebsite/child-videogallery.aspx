﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-videogallery.aspx.cs" Inherits="WadiaResponsiveWebsite.child_videogallary" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <br />
    <div style="visibility:hidden">
        <asp:LinkButton ID="lbtn1" Font-Bold="true" runat="server" Font-Underline="true" OnClick="lbtn1_Click">Category></asp:LinkButton>

        <asp:LinkButton ID="lbtn2" Font-Bold="true" Visible="false" Font-Underline="true" runat="server" OnClick="lbtn2_Click">Video Albums></asp:LinkButton>

        <asp:LinkButton ID="lbtn3" Font-Bold="true" Enabled="false" ForeColor="Black" Visible="false" runat="server">Album Videos</asp:LinkButton>
    </div>
    <br />
    <br />
    <asp:Panel ID="pnlcatdtl" runat="server">
        <div align="center" style="visibility:hidden">
            <asp:Label ID="lblcategory" runat="server" Font-Bold="true" Font-Size="18px" Text="Categories Detail"></asp:Label>
        </div>
        <asp:DataList ID="dlistchildvideocategory" Width="100%" runat="server" RepeatColumns="2">
            <ItemTemplate>
                <br />
                <div align="center">
                    <table>
                        <tr>
                            <td>
                                <div align="center">
                                    <asp:ImageButton ID="imgbtnthumnail" CommandArgument='<%#Eval("id") %>' ImageUrl='<%#("/adminpanel/" + Eval("thumbnail")) %>' Width="200px" OnClick="imgbtnthumnail_Click" Height="180px" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="center">
                                    <asp:Label ID="lblcat" runat="server" Text='<%#Eval("categorynm") %>'></asp:Label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </ItemTemplate>
        </asp:DataList>
    </asp:Panel>
    <asp:Panel ID="pnlalbumsdtl" Visible="false" runat="server">
        <div align="center" style="visibility:hidden">
            <asp:Label ID="lblalbum" runat="server" Font-Bold="true" Font-Size="18px" Text="Video Albums Detail"></asp:Label>
        </div>
        <asp:DataList ID="dlistvideoalbumdtl" Width="100%" runat="server" RepeatColumns="2">
            <ItemTemplate>
                <br />
                <div align="center">
                    <table>
                        <tr>
                            <td>
                                <div align="center">
                                    <asp:ImageButton ID="imgbalbumtnthumnail" ToolTip="click to view videos" AlternateText="View videos" CommandArgument='<%#Eval("id") %>' Width="200px" OnClick="imgbalbumtnthumnail_Click" Height="180px" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div align="center">
                                    <asp:Label ID="lblcat" runat="server" Text='<%#Eval("name") %>'></asp:Label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </ItemTemplate>
        </asp:DataList>
    </asp:Panel>

    <asp:Panel ID="pnlalbumdtl" Visible="false" runat="server">
        <div align="center" style="visibility:hidden">
            <asp:Label ID="lblalbumimgs" runat="server" Font-Bold="true" Font-Size="18px" Text="Album Videos"></asp:Label>
        </div>
        <br />
        <asp:DataList ID="dlistalbumvideos" Width="100%" runat="server" RepeatColumns="2">
            <ItemTemplate>
                <div align="center">

                    <object width="300" height="250">
                        <param name="movie" value="<%#Eval("videourl") %>">
                        <param name="allowFullScreen" value="true">
                        <param name="allowscriptaccess" value="always">
                        <embed src="<%#Eval("videourl") %>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="300" height="250"></object>

                </div>
                <br />
            </ItemTemplate>
        </asp:DataList>
    </asp:Panel>

</asp:Content>
