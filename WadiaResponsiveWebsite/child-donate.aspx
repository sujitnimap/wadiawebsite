﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-donate.aspx.cs" Inherits="WadiaResponsiveWebsite.child_donate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



 <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Contribute </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 

<%--<strong>Introduction paragraph</strong>--%>

Here at the Bai Jerbai Wadia Hospital for Children, we understand the need for quality healthcare and its intrinsic, universal nature. We have always aimed to provide medical services at affordable costs to every child who needs them, irrespective of the background they come from. 
     <br /><br />
As a charity hospital, we offer subsidized costs and in some cases even a complete cost waiver for treatments if their parents are unable to bear the fees, because we believe that a lack of resources should never prevent these young patients from the healthy body that they deserve. 
 <br /><br />
At the BJWHC, we have always relied on the constant encouragement and generosity of our corporate affiliates and individual donors. Your support continues to be crucial for the smooth functioning of the hospital as well as consistent research and regular upgrades to ensure that our services are always at par with any other national or international medical institution. 
 <br /><br />


<b>Ways to help us</b>
 <br /><br />
Offering the very best medical services to the delicate patients in our care would not be affordable to us given the nominal fee we charge them. Therefore your support is essential to us.
 <br /><br />
<b>•	Donate online</b>
     <div style="padding-left:25px">
We provide quality healthcare to over 100,000 children every year. You can help us make a difference to these young lives by contributing to our financial resources so that we can reach out to more and more children every year. 
<br />
         
         
         <a href="child-donate-online.aspx">Donate Online</a></div>

     <br />

<b>•	Sponsor a patient</b>
     <div style="padding-left:25px">
To sponsor a patient means taking care of their expenses and giving them a fighting chance, seeing them through their sickness all the way to their smiles. 
<br />
         
         <a href="child-sponsor-patient.aspx">Sponsor a Patient</a>
     </div><br />

<b>•	Donate equipment </b>
    <div style="padding-left:25px">
The world of medical sciences is constantly changing and it is imperative for us to keep up. We are constantly upgrading our equipment to ensure that all our treatment is always state of the art, to help provide the best care we can. 
<br />
        
        
        <a href="child-donate-equipment.aspx">Donate Equipment</a>
</div><br />

<b>•	Donate in-kind </b>
    <div style="padding-left:25px">
We gratefully accept contributions in the form of toys, books, craft stationary and other such child-friendly materials, which could comfort and entertain the children through their hospital experience. 
<br />
        
        
    <a href="child-donate-inkind.aspx">Donate in-kind</a> <%--<- Link to a pop-up box--%>
</div><br />

    <div align="center"><b>FCRA Registration Number - 083780590</b></div>


<%--Pop up box text
In-kind donations can be sent or dropped off at:

Bai Jerbai Wadia Hospital and Institute of Child Health, 
Acharya Donde Marg, 
Lower Parel, 
Mumbai, 
Maharashtra,
India

Tel: +91 - 22 - 24146964 / 65 / 66 / 67
Email: contribute@wadiahospitals.org

 (Add Google Maps box)--%>






</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>CONTRIBUTE</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						    
									<li><a href="child-donate-online.aspx">Donate Online</a></li>
									<li><a href="child-sponsor-patient.aspx">Sponsor a Patient</a></li>
									<li><a href="child-donate-equipment.aspx">Donate Equipment</a></li>
                                    <li><a href="child-donate-inkind.aspx">Donate in-kind </a></li>
									<li><a href="child-corporate-giving.aspx">Corporate Giving</a></li>
									<li><a href="child-testimonials.aspx">Testimonials </a></li>

                                        
									
								</ul>
							</div>
						</div>
					</div>
					
				</div>
			</div>


		</div>







</asp:Content>
