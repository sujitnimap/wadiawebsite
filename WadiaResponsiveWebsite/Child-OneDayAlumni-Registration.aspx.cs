﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class Children_OneDayAlumni_Registration : System.Web.UI.Page
    {
        
        string flag = "c";

        SqlConnection con;
        SqlCommand cmd = new SqlCommand();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
        }

        
        protected void btnPayment_Click(object sender, EventArgs e)
        {
            OneDayAlRegistration objOneDayAlRegistration = new OneDayAlRegistration();
            COperationStatus os = new COperationStatus();

            objOneDayAlRegistration.Name = txtName.Text.ToString();
            if (rdbtnMale.Checked)
            {
                objOneDayAlRegistration.Gender = "Male";
            }
            if (rdbtnFemale.Checked)
            {
                objOneDayAlRegistration.Gender = "Female";
            }
            objOneDayAlRegistration.DOB = Convert.ToDateTime(txtDOB.Text.ToString());
            objOneDayAlRegistration.Mobile = txtMobileNo.Text.ToString();
            objOneDayAlRegistration.Email = txtEmailId.Text.ToString();
            objOneDayAlRegistration.Address = txtAddress.Text.ToString();
            objOneDayAlRegistration.City = txtCity.Text.ToString();
            objOneDayAlRegistration.State = txtState.Text.ToString();
            objOneDayAlRegistration.Country = ddlCountrys.SelectedValue.ToString();
            objOneDayAlRegistration.PeriodToBJWHC = txtPeriodBJWHC.Text.ToString();
            objOneDayAlRegistration.DeptUnit = txtDeptUnit.Text.ToString();
            objOneDayAlRegistration.Degree = txtDegree.Text.ToString();
            objOneDayAlRegistration.Branch = txtBranch.Text.ToString();
            objOneDayAlRegistration.LastModify = DateTime.Now;
            objOneDayAlRegistration.TotalAmount = int.Parse(hdfTotalPayableAmount.Value.ToString());


            os = COneDayAlRegistration.Instance().InsertOneDayAlRegistration(objOneDayAlRegistration);

            if (os.Success == true)
            {
                Response.Redirect("OneDayAlPayURequest.aspx?Id=" + objOneDayAlRegistration.OneDayAlId);
            }
        }

        

        protected void cv_Gender(object source, ServerValidateEventArgs args)
        {
            args.IsValid = rdbtnMale.Checked || rdbtnFemale.Checked;
        }

        
    }
}