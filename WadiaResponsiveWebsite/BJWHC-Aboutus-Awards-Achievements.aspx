﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-Aboutus-Awards-Achievements.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_awards" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">

        <div class="col-2-3">
            <div class="wrap-col">

                <div class="heading">
                    <h2>AWARDS AND ACHIEVEMENTS</h2>
                </div>

                <br />

                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px">

                    <div class="devider_10px"></div>
                    This page is a collection of awards related to the activities at Wadia Hospitals.
                    <br />
                    <br />
                    <div align="center">
                        <table style="width: 50%">
                            <tr>

                                <td>
                                    <asp:LinkButton ID="lnbtnManagement" PostBackUrl="~/child-about-awards.aspx?catnm=Management" runat="server" OnClick="lnbtnManagement_Click">Management</asp:LinkButton>
                                </td>



                                <td>|</td>


                                <td style="padding-left: 50px">
                                    <asp:LinkButton ID="lnbtnDoctor" PostBackUrl="~/child-about-awards.aspx?catnm=Doctor" runat="server" OnClick="lnbtnDoctor_Click">Doctors</asp:LinkButton>
                                </td>


                            </tr>
                        </table>
                    </div>
                    <div class="devider_20px"></div>
                    <div align="center">
                        <asp:Label ID="lblmessage" runat="server" Visible="false" Text=""></asp:Label></div>
                    <div>

                        <div id="container">
                            <asp:Panel ID="pnlawrds" runat="server">
                                <div class="pagination">
                                    <asp:DataList class="page gradient" RepeatDirection="Horizontal" runat="server" ID="dlPagerup"
                                        OnItemCommand="dlPagerup_ItemCommand">
                                        <ItemTemplate>
                                            <a id="pageno" runat="server">
                                                <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>
                                            </a>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                                <div class="devider_20px"></div>
                                <asp:DataList ID="dlistawrdsdata" Width="100%" runat="server">
                                    <ItemTemplate>
                                        <asp:Label ID="lbldoctornm" runat="server" Font-Bold="true" Text='<%#Eval("doctornm") %>'></asp:Label>
                                        <div style="height: 5px"></div>
                                        <asp:Label ID="lbldescription" runat="server" Text='<%#Eval("description") %>'></asp:Label>
                                        <div style="height: 5px"></div>
                                        Year : &nbsp;<asp:Label ID="lblyear" runat="server" Text='<%#Eval("year") %>'></asp:Label>
                                        <div align="Center">
                                            <div style="border-bottom: 1px solid"></div>
                                        </div>
                                        <br />
                                    </ItemTemplate>
                                </asp:DataList>

                                <div class="pagination" align="left">
                                    <asp:DataList class="page gradient" RepeatDirection="Horizontal" runat="server" ID="dlPagerdown"
                                        OnItemCommand="dlPagerdown_ItemCommand">
                                        <ItemTemplate>
                                            <a id="pageno" runat="server">
                                                <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>
                                            </a>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>

                            </asp:Panel>
                        </div>
                    </div>
                </div>

                <!-- ABOUT US CONTENT -->




            </div>
        </div>
        <div class="col-1-3">
            <div class="wrap-col">

                <div class="box">
                    <div class="heading">
                        <h2>About Us</h2>
                    </div>
                    <div class="content">
                        <div class="list">
                            <ul>

                                <li><a href="BJWHC-aboutus-overview.aspx">Overview</a></li>
                                <li><a href="BJWHC-aboutus-mission-vision-corevalues.aspx">Mission, Vision, Core Values</a></li>
                                <li><a href="BJWHC-Aboutus-Chairman's-Message.aspx">Chairman's message</a></li>
                                <li><a href="BJWHC-Aboutus-Awards-Achievements.aspx">Awards and Achievements</a></li>
                                <li><a href="BJWHC-aboutus-history.aspx">History </a>

                                    <%--                                        <div class="leftpadsubmenu" id="foo" style="display:none">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>


                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
            </div>
        </div>





    </div>
</asp:Content>
