﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OperationStatus;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class SponsorARunForMarathon : System.Web.UI.Page
    {
        public int totalAmontOfChildAndAdult = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {
            //int registrationForMarathonID;
            try
            {
                string firstName;
                string lastName;
                string nameOfOrganisation;
                string emailAddress;
                string confirmEmailAddress;
                string mobileNumber1;
                string mobileNumber2;
                string address;
                string city;
                string state;
                string country;
                string nationality;
                string additionalInformation;
                int noOfSponsorARun;
                int totalAmount;

                firstName = txtFirstNameOfContactPerson.Text.Trim();
                lastName = txtLastNameOfContactPerson.Text.Trim();
                nameOfOrganisation = txtNameOfOragnisation.Text.Trim();
                emailAddress = txtEmailAddress.Text.Trim();
                confirmEmailAddress = txtConfirmEmailAddress.Text.Trim();
                mobileNumber1 = txtMobileNumber1.Text.Trim();
                mobileNumber2 = txtMobileNumber2.Text.Trim();
                address = txtAddress.Text.Trim();
                city = txtCity.Text.Trim();
                state = txtState.Text.Trim();
                country = ddlCountrys.SelectedValue;
                nationality = ddlNationalitys.SelectedValue;
                additionalInformation = txtAnyOtherRelevantInformation.Text.Trim();
                noOfSponsorARun = Convert.ToInt32(ddlNoOfSponsorRun.SelectedValue);
                totalAmount = Convert.ToInt32(lblTotalAmountInINR.Text.Trim());

                sponsorARunForMarathon oSponsorARunForMarathon = new sponsorARunForMarathon();
                COperationStatus os = new COperationStatus();

                oSponsorARunForMarathon.NameOfOrganisation = nameOfOrganisation;
                oSponsorARunForMarathon.FirstName = firstName;
                oSponsorARunForMarathon.LastName = lastName;

                oSponsorARunForMarathon.EmailAddress = emailAddress;
                oSponsorARunForMarathon.ConfirmEmailaddress = confirmEmailAddress;
                oSponsorARunForMarathon.MobileNumber1 = mobileNumber1;
                oSponsorARunForMarathon.MobileNumber2 = mobileNumber2;
                oSponsorARunForMarathon.Address = address;
                oSponsorARunForMarathon.City = city;
                oSponsorARunForMarathon.State = state;
                oSponsorARunForMarathon.Country = country;
                oSponsorARunForMarathon.Nationality = nationality;
                oSponsorARunForMarathon.AnyOtherRelevantInformation = additionalInformation;
                oSponsorARunForMarathon.NoOfSponsorRun = noOfSponsorARun;
                oSponsorARunForMarathon.AmountInINR = totalAmount;


                os = CSponsorARunForMarathon.Instance().InsertSponsorARunForMarathonDetail(oSponsorARunForMarathon);

                if (os.Success == true)
                {
                    //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    //sb.Append(@"<script type='text/javascript'>");
                    //sb.Append("alert('" + os.Message + "');");
                    //sb.Append("$('#editModal').modal('hide');");
                    //sb.Append(@"</script>");
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);

                    Response.Redirect("SponsorARunPayURequest.aspx?Id=" + oSponsorARunForMarathon.SponsorARunForMarathonID);
                }
                else
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('" + os.Message + "');");
                    sb.Append("$('#editModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
                }
            }
            catch (Exception ex)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + ex.Message + "');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
        }

        protected void ddlNoOfSponsorRun_SelectedIndexChanged(object sender, EventArgs e)
        {
            calculateAmount();
        }

        public void calculateAmount()
        {
            int noOfSponsorRun = Convert.ToInt32(ddlNoOfSponsorRun.SelectedValue);

            totalAmontOfChildAndAdult = (100 * noOfSponsorRun);
            lblTotalAmountInINR.Text = totalAmontOfChildAndAdult.ToString();
        }        

    }
}