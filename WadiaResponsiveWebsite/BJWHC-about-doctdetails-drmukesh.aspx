﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drmukesh.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drmukesh" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
		<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Mukesh Desai.jpg" />
<div class="docdetails">
Dr. Mukesh Desai 
<div class="docdetailsdesg">
Hon. In Hemato-Oncology & Immunology 
    <br />
MBBS, MD (Paediatrics); DCH 



</div>

</div>
</div>

                    <div class="areaofinterest">
Areas of interest : 
                        <br />
•	Pediatric Hematology Oncology & 
<br />•	Primary Immunodeficiency.
<br />•	Hemophagocytic Lymphohistiocytosis
<br />•	Mendelian Suceptibility to Mycobacterial Disease
<br />•	Leukocyte Adhesion Deficiency
<br />•	Phagocytic Defects

</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
 •	Along with Dr Zinet Currimbhoy established the Division of Immunology at B J Wadia Hospital for Children in association with NIIH KEM. Will soon get Centre of Excellence from ICMR.
<br /><br />•	Collaboration with Dr J L Casanova for work up of patients with genetic susceptibility to Tuberculosis
<br /><br />•	Written a Book on Primary immunodeficiency


<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	Member  :
                    <div style="padding-left:30px"> 
                        <br />

o	ASH (American Society of Hematology); 
<br /><br />o	ISHTM (Indian Society of Hematology & Transfusion Medicine); 
<br /><br />o	MHG (Mumbai Hematology Group); 
<br /><br />o	ESID (European Society of Primary Immunodeficiency)
<br /><br />o	Hematology Cell
<br /><br />o	Hematology & Immunology Cell

</div>

    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Written a Book on Primary Immunodeficiency Disorder based on experience at B J Wadia Hospital for Children
<br /><br />
•	Comprehensive Report of Primary Immunodeficiency Disorders from a Tertiary Care Center in India.
Madkaikar M, Mishra A, Desai M, Gupta M, Mhatre S, Ghosh K.
J Clin Immunol. 2012 Oct 31. [Epub ahead of print]
PMID: 23108471 [PubMed - as supplied by publisher]
<br /><br />
•	Autoimmune lymphoproliferative syndrome caused by a homozygous null FAS ligand (FASLG) mutation.
Magerus-Chatinet A, Stolzenberg MC, Lanzarotti N, Neven B, Daussy C, Picard C, Neveux N, Desai M, Rao M, Ghosh K, Madkaikar M, Fischer A, Rieux-Laucat F.
J Allergy Clin Immunol. 2013 Feb;131(2):486-90. doi: 10.1016/j.jaci.2012.06.011. Epub 2012 Jul 31.
PMID: 22857792 [PubMed - in process].
<br /><br />
•	Primary immune deficiency in the intensive care unit: It is never too late to diagnose and treat.
Dagaonkar RS, Sen T, Udwadia ZF, Desai MB.
Indian J Crit Care Med. 2011 Jul;15(3):179-81. doi: 10.4103/0972-5229.84903.
PMID: 22013312 [PubMed] Free PMC Article
<br /><br />
•	Purine nucleoside phosphorylase deficiency with a novel PNP gene mutation: a first case report from India.
Madkaikar MR, Kulkarni S, Utage P, Fairbanks L, Ghosh K, Marinaki A, Desai M.
BMJ Case Rep. 2011 Dec 8;2011. doi:pii: bcr0920114804. 10.1136/bcr.09.2011.4804.
PMID: 22669887 [PubMed - in process]
<br /><br />
•	Hemolytic anemia and distal renal tubular acidosis in two Indian patients homozygous for SLC4A1/AE1 mutation A858D.
Shmukler BE, Kedar PS, Warang P, Desai M, Madkaikar M, Ghosh K, Colah RB, Alper SL.
Am J Hematol. 2010 Oct;85(10):824-8. doi: 10.1002/ajh.21836.
PMID: 20799361 [PubMed - indexed for MEDLINE] Free PMC Article
<br /><br />
•	Bone marrow cells for myocardial repair-a new therapeutic concept.
Shah VK, Desai AJ, Vasvani JB, Desai MM, Shah BP, Lall TK, Mashru MR, Shalia KK, Tanavde V, Desai SS, Jankharia BJ.
Indian Heart J. 2007 Nov-Dec;59(6):482-90.
PMID: 19151463 [PubMed - indexed for MEDLINE]
<br /><br />
•	Haemophagocytic lymphohistiocytosis: a case series from Mumbai
Authors: Joshi, R.; Phatarpekar, A.; Currimbhoy, Z.; Desai, M.
Annals of Tropical Paediatrics: International Child Health, Volume 31, Number 2, May 2011 , pp. 135-140(6)

<br /><br />
•	Indian Pediatr. 2011 May 30. pii: S09747559INPE1000387-2. [Epub ahead of print]
Clinical Profile of Leukocyte Adhesion Deficiency Type 1.
Madkaikar M, Currimbhoy Z, Gupta M, Desai M, Rao M.
<br /><br />
•	Comprehensive Report of Primary Immunodeficiency Disorders from a Tertiary Care Center in India.
Manisha Madkaikar, Anju Mishra, Mukesh Desai, Maya Gupta, Snehal Mhatre, Kanjaksha Ghosh
Journal of Clinical Immunology 10/2012
<br /><br />
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
         </div>



</asp:Content>
