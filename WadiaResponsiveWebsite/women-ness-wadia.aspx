﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-ness-wadia.aspx.cs" Inherits="WadiaResponsiveWebsite.women_ness_wadia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>HISTORY</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Sir Ness Wadia.png" />
<div class="docdetails">

Sir Ness Wadia (1873 – 1952)

</div>
</div>

<div class="historydetails">
The second son of Bai Jerbai and Nowrosjee Wadia, Sir Ness Wadia, helped his brother Sir Cusrow carry their father’s businesses and legacy of philanthropy forward. 
<br />
<br />
He assisted his brother in the expansion of the Bombay Dyeing and Manufacturing Company and helped make it the largest textile operation in India. After the passing of Sir Cusrow Wadia, Sir Ness took charge of the expansion, growth and modernization of the company. His nature and upbringing made him perfectly suitable for the job. His success had earned him the unofficial title of ‘The Cotton King’. 
<br />
<br />
Sir Ness Wadia, the first Indian to be awarded Knighthood of the British Empire in 1919, was also instrumental in establishing a wireless service between India and Britain for the first time, in the 1920s. 
<br />
<br />

</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
									<li><a href="women-about-overview.aspx">Overview</a></li>
									<li><a href="women-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="women-about-chairman-msg.aspx">Chairman's message</a></li>
									<%--<li><a href="women-about-awards.aspx">Awards and Achievements</a></li>--%>
									<li><a href="women-about-history.aspx">History </a>

                                         <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-nusserwanji-wadia.aspx">Nowrosjee Nusserwanji Wadia </a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-ness-wadia.aspx">Sir Ness Wadia </a>

                                        </div>


									</li>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>







		</div>



</asp:Content>
