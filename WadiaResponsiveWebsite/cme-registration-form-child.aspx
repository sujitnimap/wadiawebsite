﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="cme-registration-form-child.aspx.cs" Inherits="WadiaResponsiveWebsite.cme_registration_form_child" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/jquery-1.9.1.min.js"></script>

    <script type="text/javascript">        
        function cv_RegistrationCategory(source, args) {
            if (document.getElementById("<%= rdbtnConsultant.ClientID %>").checked || document.getElementById("<%= rdbtnPGStudent.ClientID %>").checked) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }
        function OnRegCatChange() {
            var totalAmount = 0;
            var varEarlyBirdLastDate = $('#<%=hdfEarlyBirdLastDate.ClientID%>').val();
            var varRegularLastDate = $('#<%=hdfRegularLastDate.ClientID%>').val();
            var varLateLastDate = $('#<%=hdfLateLastDate.ClientID%>').val();

            var today = new Date();
            varEarlyBirdLastDate = Date.parse(varEarlyBirdLastDate);
            varRegularLastDate = Date.parse(varRegularLastDate);
            varLateLastDate = Date.parse(varLateLastDate);
            today = Date.parse(today);

            if (today <= varEarlyBirdLastDate) {
                if ($('#<%=rdbtnConsultant.ClientID%>').is(":checked")) {
                    totalAmount = $('#<%=hdfEarlyBirdConsulatant.ClientID%>').val();
                    $('#<%=lblTotalAmountInINR.ClientID%>').text(totalAmount)
                    $('#<%=hdfTotalPayableAmount.ClientID%>').val(totalAmount);
                    return false;
                }
                if ($('#<%=rdbtnPGStudent.ClientID%>').is(":checked")) {
                    totalAmount = $('#<%=hdfEarlyBirdPGStudent.ClientID%>').val();
                    $('#<%=lblTotalAmountInINR.ClientID%>').text(totalAmount)
                    $('#<%=hdfTotalPayableAmount.ClientID%>').val(totalAmount);
                    return false;
                }
            }

            if (today <= varRegularLastDate) {
                if ($('#<%=rdbtnConsultant.ClientID%>').is(":checked")) {
                    totalAmount = $('#<%=hdfRegularConsulatant.ClientID%>').val();
                    $('#<%=lblTotalAmountInINR.ClientID%>').text(totalAmount)
                    $('#<%=hdfTotalPayableAmount.ClientID%>').val(totalAmount);
                    return false;
                }
                if ($('#<%=rdbtnPGStudent.ClientID%>').is(":checked")) {
                    totalAmount = $('#<%=hdfRegularPGStudent.ClientID%>').val();
                    $('#<%=lblTotalAmountInINR.ClientID%>').text(totalAmount)
                    $('#<%=hdfTotalPayableAmount.ClientID%>').val(totalAmount);
                    return false;
                }
                //return false;
            }

            if (today > varLateLastDate) {
                if ($('#<%=rdbtnConsultant.ClientID%>').is(":checked")) {
                    totalAmount = $('#<%=hdfLateConsulatant.ClientID%>').val();
                    $('#<%=lblTotalAmountInINR.ClientID%>').text(totalAmount)
                    $('#<%=hdfTotalPayableAmount.ClientID%>').val(totalAmount);
                    return false;
                }
                if ($('#<%=rdbtnPGStudent.ClientID%>').is(":checked")) {
                    totalAmount = $('#<%=hdfLatePGStudent.ClientID%>').val();
                    $('#<%=lblTotalAmountInINR.ClientID%>').text(totalAmount)
                    $('#<%=hdfTotalPayableAmount.ClientID%>').val(totalAmount);
                    return false;
                }
            }

            $('#<%=lblTotalAmountInINR.ClientID%>').text(totalAmount)
            $('#<%=hdfTotalPayableAmount.ClientID%>').val(totalAmount);
            return false;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ToolkitScriptManager runat="server"></asp:ToolkitScriptManager>
    <asp:ValidationSummary ID="vsCMEReg" runat="server" DisplayMode="BulletList"
        ShowSummary="false" ShowMessageBox="true" ValidationGroup="vgCME" />
    <div class="row block05">
        <div class="col-full">
            <div class="wrap-col">

                <div style="border: 1px solid #ddd; padding: 10px 20px 20px 10px; border-radius: 4px;">
                    <div class="heading">
                        <h2>CME Registration Form</h2>
                        <asp:HiddenField ID="hdfTotalPayableAmount" runat="server" />
                    </div>
                    <br />

                    <table>
                        <tr>
                            <td class="textboxcontact_list_rdb">Registration Category</td>
                            <td class="textboxcontact_list_rdb"> 
                            <asp:RadioButton ID="rdbtnConsultant" Enabled="true" runat="server" Text="&nbsp;Wadia Doctors" GroupName="RegCategory" onclick="OnRegCatChange();" />&nbsp;&nbsp;&nbsp;&nbsp;                               
                                <asp:RadioButton  ID="rdbtnPGStudent" Checked="true" runat="server" Text="&nbsp;Non-Wadia Doctors" GroupName="RegCategory" onclick="OnRegCatChange();" />                                
                                <asp:CustomValidator ID="cvRegCat" runat="server" ErrorMessage="Please Select Registration Category"
                                    ClientValidationFunction="cv_RegistrationCategory" OnServerValidate="cv_RegistrationCategory"
                                    ForeColor="Red" ValidationGroup="vgCME">*</asp:CustomValidator>
                            </td>
                        </tr>                         
                        <tr>
                            <td>Name</td>
                            <td>
                                <asp:TextBox ID="txtName" CssClass="textboxcontact_list" required="" runat="server" placeholder="Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvName" ValidationGroup="vgCME" ControlToValidate="txtName" runat="server" ForeColor="Red" ErrorMessage="Please Enter Name">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>                       
                        <tr>
                            <td>Designation</td>
                            <td>
                                <asp:TextBox ID="txtDesignation" Enabled="true" CssClass="textboxcontact_list" required="" runat="server" placeholder="Designation" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Designation'">
                                </asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="rfvDesignation" ValidationGroup="vgCME" ControlToValidate="txtDesignation" runat="server" ForeColor="Red" ErrorMessage="Please Enter Designation">*</asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>Speciality</td>
                            <td>
                                <asp:TextBox ID="txtSpeciality" Enabled="true" CssClass="textboxcontact_list" required="" runat="server" placeholder="Speciality" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Speciality'">
                                </asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="rfvSpeciality" ValidationGroup="vgCME" ControlToValidate="txtSpeciality" runat="server" ForeColor="Red" ErrorMessage="Please Enter Speciality">*</asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>Institution</td>
                            <td>
                                <asp:TextBox ID="txtInstitution" CssClass="textboxcontact_list" required="" runat="server"
                                    placeholder="Institution*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Institution'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvInstitution" ValidationGroup="vgCME" ControlToValidate="txtInstitution" runat="server" ForeColor="Red" ErrorMessage="Please Enter Institution">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>
                                <asp:TextBox ID="txtAddress" TextMode="MultiLine" CssClass="textboxcontact_list" runat="server" placeholder="Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address'">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Email ID</td>
                            <td>
                                <asp:TextBox ID="txtEmailId" CssClass="textboxcontact_list" required="" runat="server"
                                    placeholder="Email ID*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email ID*'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmailId" ValidationGroup="vgCME" ControlToValidate="txtEmailId" ForeColor="Red"
                                    runat="server" ErrorMessage="Please enter your Email">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEmailId" Display="None" runat="server" ControlToValidate="txtEmailId" ErrorMessage="Enter a valid Email"
                                    ValidationExpression="^([a-zA-Z0-9_\-\.]+)@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$" ValidationGroup="vgCME"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Mobile Number</td>
                            <td>
                                <asp:TextBox ID="txtMobileNo" CssClass="textboxcontact_list" MaxLength="10" required=""
                                    runat="server" placeholder="Mobile Number*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile Number'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvMobileNo" ValidationGroup="vgCME"
                                    ControlToValidate="txtMobileNo" ForeColor="Red" runat="server" ErrorMessage="Please enter your Mobile Number">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revMobileNo" Display="None" ValidationGroup="vgCME" ValidationExpression="^[0-9]{10}$"
                                    ControlToValidate="txtMobileNo" SetFocusOnError="true" runat="server" ErrorMessage="Enter 10 digit numeric value in Contact Number"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>MCI Reg. No.</td>
                            <td>
                                <asp:TextBox ID="txtMCIRegNo" CssClass="textboxcontact_list" runat="server"
                                    required="" placeholder="MCI Reg. No.*" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'MCI Reg. No.'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvMCIRegNo" ValidationGroup="vgCME" ForeColor="Red"
                                    ControlToValidate="txtMCIRegNo" runat="server" ErrorMessage="Please enter MCI Reg. No.">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>State Council Reg. No.</td>
                            <td>
                                <asp:TextBox ID="txtStateCouncilRegNo" CssClass="textboxcontact_list"
                                    runat="server" required="" placeholder="State Council Reg. No.*"
                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'State Council Reg. No.'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvStateCouncilRegNo" ValidationGroup="vgCME" ForeColor="Red"
                                    ControlToValidate="txtStateCouncilRegNo" runat="server" ErrorMessage="Please enter State Council Reg. No.">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Year of Training
                            </td>
                            <td>
                                <asp:TextBox ID="txtYearOfTraining" CssClass="textboxcontact_list"
                                    runat="server" required="" placeholder="Year of Training.*"
                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Year of Training.'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvYearOfTraining" ValidationGroup="vgCME" ForeColor="Red"
                                    ControlToValidate="txtYearOfTraining" runat="server" ErrorMessage="Year of Training.">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revYearOfTraining" ValidationGroup="vgCME" runat="server" ValidationExpression="^[0-9]{4}$" ControlToValidate="txtYearOfTraining" SetFocusOnError="true" ErrorMessage="Year of training should be 4 digit number only"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <%--<tr>
                            <td>Choose Payment Category</td>
                            <td>
                                <asp:DropDownList ID="ddlPaymentCategory" CssClass="textboxcontact_list"
                                    required="" runat="server" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Payment Category'">
                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                    <asp:ListItem Text="Early Bird" Value="Early Bird"></asp:ListItem>
                                    <asp:ListItem Text="Regular" Value="Regular"></asp:ListItem>
                                    <asp:ListItem Text="Late" Value="Late"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" ID="rfvPaymentCat" ValidationGroup="vgCME"
                                    ControlToValidate="ddlPaymentCategory" runat="server" ForeColor="Red"
                                    ErrorMessage="Select Payment Category">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>--%>
                    </table>                    
                    <br />
                    <div style="display: inline; padding-right: 35px">
                        <strong>Total Amount in INR : </strong>
                    </div>
                    <div style="display: inline">
                        <asp:Label ID="lblTotalAmountInINR" runat="server"></asp:Label>

                        <asp:HiddenField ID="hdfEarlyBirdLastDate" runat="server" />
                        <asp:HiddenField ID="hdfEarlyBirdConsulatant" runat="server" />
                        <asp:HiddenField ID="hdfEarlyBirdPGStudent" runat="server" />

                        <asp:HiddenField ID="hdfRegularLastDate" runat="server" />
                        <asp:HiddenField ID="hdfRegularConsulatant" runat="server" />
                        <asp:HiddenField ID="hdfRegularPGStudent" runat="server" />

                        <asp:HiddenField ID="hdfLateLastDate" runat="server" />
                        <asp:HiddenField ID="hdfLateConsulatant" runat="server" />
                        <asp:HiddenField ID="hdfLatePGStudent" runat="server" />
                    </div>

                    <br />
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    <br />
                    <%--<div class="devider_20px"></div>--%>

                    <asp:Button ID="btnPayment" Text="Pay now" ValidationGroup="vgCME" CssClass="btn" runat="server" OnClick="btnPayment_Click" />
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">       
        $(document).ready(function () {
            OnRegCatChange();
        });
    </script>
</asp:Content>
