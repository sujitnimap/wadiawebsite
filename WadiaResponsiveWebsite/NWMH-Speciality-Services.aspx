﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Speciality-Services.aspx.cs" Inherits="WadiaResponsiveWebsite.women_speciality_services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>services</h2>
                </div>

                <br />

                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px;">

                    <div class="serviceheading" style="text-align: LEFT; padding-left: 30px">SPECIALITY SERVICES</div>

                    <div style="padding-left: 30px">

                        <div class="devider_20px"></div>


                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="12%" height="55" valign="middle">
                                    <img src="images/serviceswomen/Obstetrics.png" width="46" height="52" /></td>
                                <td class="servicename" width="46%"><a href="NWMH-Obstetrics.aspx">Obstetrics  </a></td>
                                <td class="servicename" width="10%">
                                    <img src="images/serviceswomen/Tertiary Level Neonatal Intensive care Services.png" width="52" height="42" /></td>
                                <td class="servicename" width="32%"><a href="NWMH-Gynacologic-oncology.aspx">Gynaecology </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="55">
                                    <img src="images/serviceswomen/Family Planning Clinic.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="NWMH-Family-Planning-Clinic.aspx">Family Planning Clinic</a></td>
                                <td class="servicename">
                                    <img src="images/serviceswomen/Well Women Clinic.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="NWMH-Well-women-clinic.aspx">Well Women Clinic</a></td>
                            </tr>
                        </table>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="12%" height="66px" valign="middle">
                                    <img src="images/serviceswomen/Dental Clinic.png" width="46" height="46" /></td>
                                <td class="servicename" width="46%"><a href="NWMH-Dentistry.aspx">Dental Clinic  </a></td>
                                <td class="servicename" width="10%">
                                    <img src="images/serviceswomen/Endocrinology.png" width="46" height="42" /></td>
                                <td class="servicename" width="32%"><a href="NWMH-Endocrinology.aspx">Endocrinology </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/serviceswomen/Endoscopic Surgeries.png" width="52" height="42" /></td>
                                <td class="servicename"><a href="NWMH-EndoscopicSurgeries.aspx">Endoscopic Surgeries</a></td>
                                <td class="servicename">
                                    <img src="images/serviceswomen/Foetal Medicine.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="NWMH-Foetal-Medicine.aspx">Foetal Medicine </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/serviceswomen/Well Women Clinic.png" width="46" height="41" /></td>
                                <td align="left" class="servicename"><a href="NWMH-Gynacologic-oncology.aspx">Gynaecologic Oncology</a></td>
                                <td align="left" class="servicename">
                                    <img src="images/serviceswomen/Urogynaecology.png" width="46" height="42" /></td>
                                <td align="left" class="servicename"><a href="NWMH-Gynaecolgic-surgery.aspx">Gynaecologic Surgery </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/serviceswomen/High Risk Pregnancy Clinic.png" width="38" height="38" /></td>
                                <td class="servicename"><a href="NWMH-High-risk-pregnancy-clinic.aspx">High Risk Pregnancy Clinic </a></td>
                                <td class="servicename">
                                    <img src="images/serviceswomen/Immunology.png" width="46" height="43" /></td>
                                <td class="servicename"><a href="NWMH-Immunology.aspx">Immunology </a></td>
                            </tr>

                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/serviceswomen/Nutrition and Exercise Clinic.png" width="48" height="48" /></td>
                                <td class="servicename"><a href="NWMH-Nutrition-Exercise-Clinic.aspx">Nutrition and Exercise Clinic </a></td>
                                <td class="servicename">
                                    <img src="images/serviceswomen/Perinatal Counselling for High Risk Foetuses and Neonates.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="NWMH-Perinatal-Counseling.aspx">Perinatal Counselling for High Risk Foetuses and Neonates </a></td>
                            </tr>

                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/serviceswomen/Preconception and Premarital Counselling.png" width="47" height="47" /></td>
                                <td class="servicename"><a href="NWMH-Preconception-counseling.aspx">Preconception and Premarital Counselling  </a></td>
                                <td class="servicename">
                                    <img src="images/serviceswomen/Reproductive and infertility clinic.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="NWMH-Reproductive-Clinic.aspx">Reproductive and infertility clinic  </a></td>
                            </tr>

                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/serviceswomen/Gynaecology.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="NWMH-tertiary-services.aspx">Tertiary Level Neonatal Intensive care Services </a></td>
                                <td class="servicename">
                                    <img src="images/serviceswomen/Urogynaecology.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="NWMH-Urogynaecology.aspx">Urogynaecology </a></td>
                            </tr>


                        </table>
                        <div class="devider_20px"></div>
                        <div class="serviceheading" style="text-align: LEFT;">SUPPORTIVE SERVICES</div>
                        <div class="devider_20px"></div>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="12%" height="55" valign="middle">
                                    <img src="images/serviceswomen/Anaesthesiology.png" width="46" height="52" /></td>
                                <td class="servicename" width="46%"><a href="NWMH-Anaesthesiology.aspx">Anaesthesiology  </a></td>
                                <td class="servicename" width="10%">
                                    <img src="images/serviceswomen/Blood Bank.png" width="52" height="42" /></td>
                                <td class="servicename" width="32%"><a href="NWMH-Blood-bank.aspx">Blood bank </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="55">
                                    <img src="images/serviceswomen/Pathology Lab.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="NWMH-Pathology-Lab.aspx">Pathology Lab </a></td>
                                <td class="servicename">
                                    <img src="images/serviceswomen/Physiotherapy.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="NWMH-Physiotherapy.aspx">Physiotherapy </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="55">
                                    <img src="images/serviceswomen/Social Service Department.png" width="52" height="44" /></td>
                                <td class="servicename"><a href="NWMH-Social-Service-Department.aspx">Social Service Department</a></td>
                                <td class="servicename">
                                    <img src="images/serviceswomen/Ultrasonography.png" width="47" height="47" /></td>
                                <td class="servicename"><a href="NWMH-Ultrasonography.aspx">Ultrasonography and Radiology</a></td>
                            </tr>


                        </table>


                    </div>
                </div>
                <%--SUPER SPECIALITY--%>


                <%--SUPPORTIVE START--%>


                <%--SUPPORTIVE END--%>
            </div>

        </div>

        <!-- ABOUT US CONTENT -->

    </div>


</asp:Content>
