﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OperationStatus;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
		public partial class RegistrationForMarathon : System.Web.UI.Page
		{
				public int totalAmontOfChildAndAdult = 0;

				protected void Page_Load(object sender, EventArgs e)
				{
						ddlNoOfChildrens_SelectedIndexChanged(sender, e);
						ddlNoOfParents_SelectedIndexChanged(sender, e);
				}

				protected void ddlNoOfChildrens_SelectedIndexChanged(object sender, EventArgs e)
				{
						SetChildData();
						List<int> childCount = new List<int>();
						List<registeredChildForMarathon> registeredChildForMarathons = new List<registeredChildForMarathon>();
						List<registeredChildForMarathon> selRegisteredChildForMarathons = (List<registeredChildForMarathon>)ViewState["registeredChildForMarathons"];
						for (int i = 1; i <= Convert.ToInt32(ddlNoOfChildrens.SelectedValue); i++)
						{
								registeredChildForMarathon registeredChildForMarathon = new registeredChildForMarathon();
								if (selRegisteredChildForMarathons != null && selRegisteredChildForMarathons.Count >= i)
								{
										registeredChildForMarathon.ChildFirstName = selRegisteredChildForMarathons[i - 1].ChildFirstName;
										registeredChildForMarathon.Age = selRegisteredChildForMarathons[i - 1].Age;
										registeredChildForMarathon.Gender = selRegisteredChildForMarathons[i - 1].Gender;
										registeredChildForMarathon.TShirtSize = selRegisteredChildForMarathons[i - 1].TShirtSize;
										registeredChildForMarathon.School = selRegisteredChildForMarathons[i - 1].School;
								}

								childCount.Add(i);
								registeredChildForMarathons.Add(registeredChildForMarathon);
						}
						//tst.Add(2)
						gvwChild.DataSource = registeredChildForMarathons;
						gvwChild.DataBind();

						calculateAmount();

				}

				protected void ddlNoOfParents_SelectedIndexChanged(object sender, EventArgs e)
				{
						SetAdultData();
						List<int> parentCount = new List<int>();

						List<registeredAdultForMarathon> registeredAdultForMarathons = new List<registeredAdultForMarathon>();
						List<registeredAdultForMarathon> selregisteredAdultForMarathon = (List<registeredAdultForMarathon>)ViewState["registeredAdultForMarathons"];


						for (int i = 1; i <= Convert.ToInt32(ddlNoOfParents.SelectedValue); i++)
						{
								registeredAdultForMarathon registeredAdultForMarathon = new registeredAdultForMarathon();
								if (selregisteredAdultForMarathon != null && selregisteredAdultForMarathon.Count >= i)
								{
										registeredAdultForMarathon.AdultName = selregisteredAdultForMarathon[i - 1].AdultName;
										registeredAdultForMarathon.Age = selregisteredAdultForMarathon[i - 1].Age;
										registeredAdultForMarathon.Gender = selregisteredAdultForMarathon[i - 1].Gender;
										registeredAdultForMarathon.TShirtSize1 = selregisteredAdultForMarathon[i - 1].TShirtSize1;
										registeredAdultForMarathon.Organization = selregisteredAdultForMarathon[i - 1].Organization;
								}

								parentCount.Add(i);
								registeredAdultForMarathons.Add(registeredAdultForMarathon);

						}
						//tst.Add(2)
						gvwParents.DataSource = registeredAdultForMarathons;
						gvwParents.DataBind();
						calculateAmount();
				}


				protected void chkIsSponsorARun_CheckedChanged(object sender, EventArgs e)
				{
						if (chkIsSponsorARun.Checked)
						{
								pnlNoOfSponsorRun.Visible = true;
						}
						else
						{
								pnlNoOfSponsorRun.Visible = false;
						}
				}

				public void calculateAmount()
				{
						int totalNoOfChild = Convert.ToInt32(ddlNoOfChildrens.SelectedValue);
						int totalNoOfAdult = Convert.ToInt32(ddlNoOfParents.SelectedValue);
						int noOfSponsorRun = Convert.ToInt32(ddlNoOfSponsorRun.SelectedValue);
						int totalNoOfParticipant = 0;

						if (chkIsSponsorARun.Checked)
						{
								totalNoOfParticipant = totalNoOfChild + totalNoOfAdult + noOfSponsorRun;
						}
						else
						{
								totalNoOfParticipant = totalNoOfChild + totalNoOfAdult;
						}
						totalAmontOfChildAndAdult = (100 * totalNoOfParticipant);
						lblTotalAmountInINR.Text = totalAmontOfChildAndAdult.ToString();
				}


				public void SetChildData()
				{
						List<registeredChildForMarathon> registeredChildForMarathons = new List<registeredChildForMarathon>();
						foreach (GridViewRow gvRow in gvwChild.Rows)
						{
								registeredChildForMarathon registeredChildForMarathon = new registeredChildForMarathon();
								TextBox txtChildName = (TextBox)gvRow.FindControl("txtChildName");
								DropDownList ddlChildAge = (DropDownList)gvRow.FindControl("ddlChildAge");
								DropDownList ddlChildGender = (DropDownList)gvRow.FindControl("ddlChildGender");
								DropDownList ddlChildTShirtSize = (DropDownList)gvRow.FindControl("ddlChildTShirtSize");
								TextBox txtChildSchool = (TextBox)gvRow.FindControl("txtChildSchool");

								registeredChildForMarathon.ChildFirstName = txtChildName.Text;
                                registeredChildForMarathon.ChildMiddleName = txtChildName.Text;
                                registeredChildForMarathon.ChildLastName = txtChildName.Text;
								registeredChildForMarathon.Age = Convert.ToInt32(ddlChildAge.SelectedValue);
								registeredChildForMarathon.Gender = ddlChildGender.SelectedValue;
								registeredChildForMarathon.TShirtSize = ddlChildTShirtSize.SelectedValue; 
									//ddlChildTShirtSize.SelectedItem.ToString();
								registeredChildForMarathon.School = txtChildSchool.Text;

								registeredChildForMarathons.Add(registeredChildForMarathon);

						}

						ViewState["registeredChildForMarathons"] = registeredChildForMarathons;
				}

				public void SetAdultData()
				{
						List<registeredAdultForMarathon> registeredAdultForMarathons = new List<registeredAdultForMarathon>();
						foreach (GridViewRow gvRow in gvwParents.Rows)
						{
								registeredAdultForMarathon registeredAdultForMarathon = new registeredAdultForMarathon();
								TextBox txtParentOrGuardianName = (TextBox)gvRow.FindControl("txtParentOrGuardianName");
								DropDownList ddlParentAge = (DropDownList)gvRow.FindControl("ddlParentAge");
								DropDownList ddlParentGender = (DropDownList)gvRow.FindControl("ddlParentGender");
								DropDownList ddlParentTShirtSize = (DropDownList)gvRow.FindControl("ddlParentTShirtSize");
								TextBox txtParentsOrganization = (TextBox)gvRow.FindControl("txtParentsOrganization");

								registeredAdultForMarathon.AdultName = txtParentOrGuardianName.Text.Trim();
								registeredAdultForMarathon.Age = Convert.ToInt32(ddlParentAge.SelectedValue);
								registeredAdultForMarathon.Gender = ddlParentGender.SelectedValue;
								registeredAdultForMarathon.TShirtSize1 = ddlParentTShirtSize.SelectedValue;
									//ddlParentTShirtSize.SelectedItem.ToString();
								registeredAdultForMarathon.Organization = txtParentsOrganization.Text.Trim();

								registeredAdultForMarathons.Add(registeredAdultForMarathon);
						}

						ViewState["registeredAdultForMarathons"] = registeredAdultForMarathons;
				}

				protected void btnPayment_Click(object sender, EventArgs e)
				{
						//if (ddlNoOfChildrens.SelectedValue == "0" && ddlNoOfParents.SelectedValue == "0")
						//{
						//    Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "addScript", "alert('There should be atleast one child or one adult register entry for marathon..!!')", true);
						//}


						int registrationForMarathonID;
						int noOfChildrens;
						int noOfAdults;
						string emailAddress;
						string confirmEmailAddress;
						string  mobileNumber1;
						string  mobileNumber2;
						string address;
						string city;
						string state;
						string country;
						string nationality;
						string additionalInformation;
						bool isSponsorARun;
						int noOfSponsorARun;
						int totalAmount;


						noOfChildrens = Convert.ToInt32(ddlNoOfChildrens.SelectedValue);
						noOfAdults = Convert.ToInt32(ddlNoOfParents.SelectedValue);
						emailAddress = txtEmailAddress.Text.Trim();
						confirmEmailAddress = txtConfirmEmailAddress.Text.Trim();
						mobileNumber1 = txtMobileNumber1.Text.Trim();
						mobileNumber2 = txtMobileNumber2.Text.Trim();
						address = txtAddress.Text.Trim();
						city = txtCity.Text.Trim();
						state = txtState.Text.Trim();
						country = ddlCountrys.SelectedValue;
						nationality = ddlNationalitys.SelectedValue;
						additionalInformation = txtAnyOtherRelevantInformation.Text.Trim();
						isSponsorARun = chkIsSponsorARun.Checked;
						noOfSponsorARun = Convert.ToInt32(ddlNoOfSponsorRun.SelectedValue);
						totalAmount = Convert.ToInt32(lblTotalAmountInINR.Text.Trim());

						if (emailAddress != confirmEmailAddress)
						{
								lblMessage.Text = "Please check terms and conditions";               
						}

						registrationForMarathon objRegistrationForMarathon = new registrationForMarathon();
						COperationStatus os = new COperationStatus();

						objRegistrationForMarathon.NoOfChildrens = noOfChildrens;
						objRegistrationForMarathon.NoOfAdults = noOfAdults;
						objRegistrationForMarathon.EmailAddress = emailAddress;
						objRegistrationForMarathon.ConfirmEmailAddress = confirmEmailAddress;
						objRegistrationForMarathon.MobileNumber1 = mobileNumber1;
						objRegistrationForMarathon.MobileNumber2 = mobileNumber2;
						objRegistrationForMarathon.Address = address;
						objRegistrationForMarathon.City = city;
						objRegistrationForMarathon.State = state;
						objRegistrationForMarathon.Country = country;
						objRegistrationForMarathon.Nationality = nationality;
						objRegistrationForMarathon.AdditionalInformation = additionalInformation;
						objRegistrationForMarathon.IsSponsorARun = isSponsorARun;
						objRegistrationForMarathon.NoOfSponsorARun = noOfSponsorARun;
						objRegistrationForMarathon.TotalAmount = totalAmount ;

						os = CRegistrationForMarathon.Instance().InsertRegistrationForMarathonDetail(objRegistrationForMarathon);

						//registeredChildForMarathon objRegisteredChildForMarathon = new registeredChildForMarathon();
						//objRegisteredChildForMarathon.RegistrationForMarathonID = objRegistrationForMarathon.RegistrationForMarathonID;
						
						//List<registeredChildForMarathon> registeredChildForMarathons = new List<registeredChildForMarathon>();
						List<registeredChildForMarathon> selRegisteredChildForMarathons = (List<registeredChildForMarathon>)ViewState["registeredChildForMarathons"];
						for (int i = 1; i <= Convert.ToInt32(ddlNoOfChildrens.SelectedValue); i++)
						{
								registeredChildForMarathon registeredChildForMarathon = new registeredChildForMarathon();
								if (selRegisteredChildForMarathons != null && selRegisteredChildForMarathons.Count >= i)
								{
										registeredChildForMarathon.ChildFirstName = selRegisteredChildForMarathons[i - 1].ChildFirstName;
										registeredChildForMarathon.Age = selRegisteredChildForMarathons[i - 1].Age;
										registeredChildForMarathon.Gender = selRegisteredChildForMarathons[i - 1].Gender;
										registeredChildForMarathon.TShirtSize = selRegisteredChildForMarathons[i - 1].TShirtSize;
										registeredChildForMarathon.School = selRegisteredChildForMarathons[i - 1].School;
										registeredChildForMarathon.RegistrationForMarathonID = objRegistrationForMarathon.RegistrationForMarathonID;
								}

								os = CRegisteredChildForMarathon.Instance().InsertRegisteredChildForMarathonDetail(registeredChildForMarathon);
								//registeredChildForMarathons.Add(registeredChildForMarathon);
						}

						List<registeredAdultForMarathon> selRegisteredAdultForMarathon = (List<registeredAdultForMarathon>)ViewState["registeredAdultForMarathons"];
						for (int j = 1; j <= Convert.ToInt32(ddlNoOfParents.SelectedValue); j++)
						{
								registeredAdultForMarathon registeredAdultForMarathon = new registeredAdultForMarathon();
								if (selRegisteredAdultForMarathon != null && selRegisteredAdultForMarathon.Count >= j)
								{
										registeredAdultForMarathon.AdultName = selRegisteredAdultForMarathon[j - 1].AdultName;
										registeredAdultForMarathon.Age = selRegisteredAdultForMarathon[j - 1].Age;
										registeredAdultForMarathon.Gender = selRegisteredAdultForMarathon[j - 1].Gender;
										registeredAdultForMarathon.TShirtSize1 = selRegisteredAdultForMarathon[j - 1].TShirtSize1;
										registeredAdultForMarathon.Organization = selRegisteredAdultForMarathon[j - 1].Organization;
										registeredAdultForMarathon.RegistrationForMarathonID = objRegistrationForMarathon.RegistrationForMarathonID;
								}

								os = CRegisteredAdultForMarathon.Instance().InsertRegisteredAdultForMarathonDetail(registeredAdultForMarathon);
						}

						if (os.Success == true)
						{

								Response.Redirect("MarathonPayURequest.aspx?Id=" + objRegistrationForMarathon.RegistrationForMarathonID);

						}
				}

				protected void gvwChild_RowDataBound(object sender, GridViewRowEventArgs e)
				{
						if (e.Row.RowType == DataControlRowType.DataRow)
						{
								registeredChildForMarathon selRegisteredChildForMarathon = (registeredChildForMarathon)e.Row.DataItem;

								DropDownList ddlChildAge = (DropDownList)e.Row.FindControl("ddlChildAge");
								DropDownList ddlChildGender = (DropDownList)e.Row.FindControl("ddlChildGender");
								//DropDownList ddlChildTShirtSize = (DropDownList)e.Row.FindControl("ddlChildTShirtSize");

								ddlChildAge.SelectedValue = Convert.ToString(selRegisteredChildForMarathon.Age);
								ddlChildGender.SelectedValue = selRegisteredChildForMarathon.Gender;
								//ddlChildTShirtSize.SelectedValue = selRegisteredChildForMarathon.TShirtSize;

							
						}
				}

				protected void gvwParents_RowDataBound(object sender, GridViewRowEventArgs e)
				{
						if (e.Row.RowType == DataControlRowType.DataRow)
						{
								registeredAdultForMarathon selRegisteredAdultForMarathon = (registeredAdultForMarathon)e.Row.DataItem;

								DropDownList ddlParentAge = (DropDownList)e.Row.FindControl("ddlParentAge");
								DropDownList ddlParentGender = (DropDownList)e.Row.FindControl("ddlParentGender");
								//DropDownList ddlParentTShirtSize = (DropDownList)e.Row.FindControl("ddlParentTShirtSize");

								ddlParentAge.SelectedValue = Convert.ToString(selRegisteredAdultForMarathon.Age);
								ddlParentGender.SelectedValue = selRegisteredAdultForMarathon.Gender;
								//ddlParentTShirtSize.SelectedValue = selRegisteredAdultForMarathon.TShirtSize;
						}
				}

				protected void ddlNoOfSponsorRun_SelectedIndexChanged(object sender, EventArgs e)
				{
						calculateAmount();
				}
		}
}