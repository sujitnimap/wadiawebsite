﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class women_applicationform : System.Web.UI.Page
    {
        public string titleId
        {
            get
            {
                if (ViewState["catid"] == null)
                    return "";
                else
                    return (string)ViewState["catid"];
            }
            set
            {
                ViewState["catid"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            lblstatus.Visible = false;

            if (!string.IsNullOrEmpty(Request.QueryString["title"]))
            {
                ViewState["title"] = Convert.ToString(Request.QueryString["title"]);

                titleId = ViewState["title"].ToString();

                lbltitle.Text = titleId;
            }


            if (lbltitle.Text == "")
            {
                app.Visible = false;
                app2.Visible = true;
            }
            else
            {
                app.Visible = true;
                app2.Visible = false;
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {

            COperationStatus os = new COperationStatus();

            try
            {
                if (Session["RandomString"].ToString() == tb_Code.Text.Trim())
                {

                    string resumefile = "";
                    string coverletter = "";
                    string gender = "";

                    if (fldresume.HasFile)
                    {
                        string filename = Path.GetFileName(fldresume.PostedFile.FileName);

                        string file = "resumes/" + filename;

                        fldresume.SaveAs(Server.MapPath("~/adminpanel/resumes/" + filename));

                        resumefile = file;
                    }
                    else
                    {
                        resumefile = "#";
                    }

                    if (fldcover.HasFile)
                    {
                        string filename = Path.GetFileName(fldcover.PostedFile.FileName);

                        string file = "coverletter/" + filename;

                        fldcover.SaveAs(Server.MapPath("~/adminpanel/coverletter/" + filename));

                        coverletter = file;
                    }
                    else
                    {
                        coverletter = "#";
                    }
                    if (rdbmale.Checked == true)
                    {
                        gender = "Male";
                    }
                    else
                    {
                        gender = "Female";
                    }


                    career careerdata = new career();

                    careerdata.name = txtfname.Text.Trim() + " " + txtlname.Text.Trim();
                    careerdata.email = txtemailid.Text.Trim();
                    careerdata.mobile = txtmobile.Text.Trim();
                    careerdata.resumefilename = resumefile;
                    careerdata.coverletter = coverletter;
                    careerdata.gender = gender;
                    careerdata.createdate = DateTime.Now;
                    careerdata.isactive = true;
                    careerdata.flag = "w";
                    if (lbltitle.Text.Trim() == "")
                    {
                        careerdata.position = ddltitle.SelectedItem.Text.Trim();
                    }
                    else
                    {
                        careerdata.position = lbltitle.Text.Trim();
                    }

                    os = CCareer.Instance().InsertCareer(careerdata);

                    if (os.Success == true)
                    {
                        lblstatus.Text = "Thank you for your application.<br>Shortlisted candidates will be contacted directly by the Hospital.";
                        lblstatus.Visible = true;
                    }
                    else
                    {
                        lblstatus.Text = os.Message;
                        lblstatus.Visible = true;
                    }

                }
                else
                {
                    lblstatus.Text = "Plese enter valid capcha";
                    lblstatus.Visible = true;
                }

            }
            catch (Exception ex)
            {
                lblstatus.Text = ex.Message;
                lblstatus.Visible = true;
            }

        }



        protected void lbRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                imgVerificationCode.ImageUrl = "capcha.aspx";
                if (iRandomImageID == 2)
                {
                    imgVerificationCode.ImageUrl = "capcha.aspx";
                    iRandomImageID = 0;
                }
                else
                {
                    imgVerificationCode.ImageUrl = "capcha1.aspx";
                    iRandomImageID = 2;
                }

                tb_Code.Focus();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message + ex.StackTrace);
            }
        }

        private int iRandomImageID
        {
            get
            {
                if (ViewState["randomImgID"] == null)
                {
                    return 0;
                }
                else
                {
                    return (int)ViewState["randomImgID"];
                }
            }
            set
            {
                ViewState["randomImgID"] = value;
            }
        }
    }
}