﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-dermatology.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_dermatology" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PAEDIATRIC DERMATOLOGY</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Outpatient Department</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
Paediatric Dermatologists are experts in providing treatment and care to children with common and rare skin disorders, whether acquired or genetic. The doctors not only treat the children but also help them and their families better understand their ailments, and how to take precautions and cope with the illness. Our skilled and experienced doctors adopt a multi-disciplinary approach where disorders are evaluated and managed in collaboration with specialists from other paediatric specialities to ensure safe and highly effective long-term management.
                                                 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
Some of the challenging diseases and conditions treated at the hospital include :

                 <br /><br />
                <div style="padding-left:10px">

•	Acne
<br />•	Alopecia
<br />•	Atopic dermatitis
<br />•	Bacterial, fungal and viral infections
<br />•	Collagen-vascular disorders
<br />•	Contact dermatitis
<br />•	Ectodermal dysplasia
<br />•	Eczema
<br />•	Genetic skin disorders
<br />•	Hair loss
<br />•	Malformations of the skin
<br />•	Moles
<br />•	Psoriasis
<br />•	Rashes
<br />•	Scabies
<br />•	Seborrheic dermatitis
<br />•	Vascular malformations
<br />•	Vitiligo



</div>

         
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>


		</div>




</asp:Content>
