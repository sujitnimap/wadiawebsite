﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Info.ascx.cs" Inherits="eBookReader.usercontrols.Info" %>

<table width="480" class="info" style="border: 1px solid #006600; margin-bottom: 20px;
    background-color: #efefef; height: 35px;" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table style="width: 100%">
                <tr>
                    <td width="45" align="center">
                        <img src="../images/img5.png" width="26" height="26" alt="Info" />
                    </td>
                    <td align="left">
                        <strong>Info:</strong>&nbsp;<asp:Label ID="lblInfo" runat="server" ForeColor="Black"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>