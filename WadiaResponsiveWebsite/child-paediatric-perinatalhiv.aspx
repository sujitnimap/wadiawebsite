﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-perinatalhiv.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_perinatalhiv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Paediatric and Perinatal HIV clinic</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 
    <strong>Overview </strong>
    <br />    <br />


The hospital runs special clinics for children suffering from HIV infections. We have experienced paediatric specialists who offer complete check-ups, evaluation and counselling to children diagnosed with or suspected of suffering from HIV infection.
Paediatric HIV patients are treated with ART and with large number of deliveries taking place in our maternity hospital, special emphasis is placed on PMTCT (Antiretroviral Therapy for Preventing Mother-To-Child Transmission of HIV). 

    <div style="height:140px"></div>

</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

     	</div>




</asp:Content>
