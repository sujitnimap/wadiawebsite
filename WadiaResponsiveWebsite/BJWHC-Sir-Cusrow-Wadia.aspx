﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-Sir-Cusrow-Wadia.aspx.cs" Inherits="WadiaResponsiveWebsite.child_cursetjee_wadia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>HISTORY</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Cusrow Wadia.png" />
<div class="docdetails">

Sir Cusrow Wadia (1869 – 1950) 
</div>
</div>

<div class="historydetails">

The oldest of Bai Jerbai and Nowrosjee Wadia’s 3 sons, Sir Cusrow Wadia, succeeded his father and became the custodian of not only his businesses but also the legacy of Philanthropy his parents left him. 
<br />
<br />
After his father, he worked towards expanding the family businesses. Taking over as the eldest son and with the help of his brother Sir Ness Wadia, Sir Cusrow led the Bombay Dyeing and Manufacturing Company to becoming the largest textile operations in India.
<br />
<br />
Christened as a “Chronic Philanthropist” Sir Cusrow’s support extended to various fields. From providing interest-free loans to the Royal Western Indian Turf Club to jointly providing for the Wadia College, Pune along with Sir Ness, to contributions to Indian agriculture, to name a few.
<br />
<br />

</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									 
									<li><a href="BJWHC-aboutus-overview.aspx">Overview</a></li>
									<li><a href="BJWHC-aboutus-mission-vision-corevalues.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="BJWHC-Aboutus-Chairman's-Message.aspx">Chairman's message</a></li>
									<%--<li><a href="child-about-awards.aspx">Awards and Achievements</a></li>--%>
										<li><a href="BJWHC-aboutus-history.aspx">History </a>

                                       <div class="leftpadsubmenu">

                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="BJWHC-Aboutus-Bai-jerbai-wadia.aspx">Bai Jerbai Wadia </a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cusrow Wadia </a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="BJWHC-Aboutus-Sir-ness-wadia.aspx">Sir Ness Wadia </a>

                                        </div>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>







		</div>
</asp:Content>
