﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-speciality-services.aspx.cs" Inherits="WadiaResponsiveWebsite.women_speciality_services" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>services</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px;">
    
   <div class="serviceheading" style="text-align:LEFT; padding-left:30px">SPECIALITY SERVICES</div>

    <div style="padding-left:30px">

     <div class="devider_20px"></div>
    
 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="12%" height="55" valign="middle"><img src="images/serviceswomen/Obstetrics.png" width="46" height="52" /></td>
    <td class="servicename" width="46%"><a href="women-obstetrics.aspx">	Obstetrics  </a></td>
    <td class="servicename" width="10%"><img src="images/serviceswomen/Tertiary Level Neonatal Intensive care Services.png" width="52" height="42" /></td>
    <td class="servicename" width="32%"><a href="women-gengynaecology.aspx">Gynaecology </a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/serviceswomen/Family Planning Clinic.png" width="46" height="46" /></td>
    <td class="servicename"><a href="women-famplanclinic.aspx">Family Planning Clinic</a></td>
    <td class="servicename"><img src="images/serviceswomen/Well Women Clinic.png" width="46" height="46" /></td>
    <td class="servicename"><a href="women-wellwomencinic.aspx"> Well Women Clinic</a></td>
  </tr>
 
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="12%" height="66px" valign="middle"><img src="images/serviceswomen/Dental Clinic.png" width="46" height="46" /></td>
    <td class="servicename" width="46%"><a href="women-dentalclinic.aspx">	Dental Clinic  </a></td>
    <td class="servicename" width="10%"><img src="images/serviceswomen/Endocrinology.png" width="46" height="42" /></td>
    <td class="servicename" width="32%"><a href="women-endocrinology.aspx">Endocrinology </a></td>
  </tr>
  <tr>
    <td valign="middle" height="66"><img src="images/serviceswomen/Endoscopic Surgeries.png" width="52" height="42" /></td>
    <td class="servicename"><a href="women-endosurgeries.aspx">Endoscopic Surgeries</a></td>
    <td class="servicename"><img src="images/serviceswomen/Foetal Medicine.png" width="46" height="46" /></td>
    <td class="servicename"><a href="women-foetalmedicine.aspx">Foetal Medicine </a></td>
  </tr>
  <tr>
    <td valign="middle" height="66"><img src="images/serviceswomen/Well Women Clinic.png" width="46" height="41" /></td>
    <td align="left" class="servicename"><a href="women-gynaoncology.aspx">Gynaecologic Oncology</a></td>
    <td align="left" class="servicename"><img src="images/serviceswomen/Urogynaecology.png" width="46" height="42" /></td>
    <td align="left" class="servicename"><a href="women-gynasurgery.aspx">Gynaecologic Surgery </a></td>
  </tr>
  <tr>
    <td valign="middle" height="66"><img src="images/serviceswomen/High Risk Pregnancy Clinic.png" width="38" height="38" /></td>
    <td class="servicename"><a href="women-highriskpregclinic.aspx">High Risk Pregnancy Clinic </a></td>
    <td class="servicename"><img src="images/serviceswomen/Immunology.png" width="46" height="43" /></td>
    <td class="servicename"><a href="women-immunology.aspx">Immunology </a></td>
  </tr>
  
  
  <tr>
    <td valign="middle" height="66"><img src="images/serviceswomen/Nutrition and Exercise Clinic.png" width="48" height="48" /></td>
    <td class="servicename"><a href="women-nutritionandexe.aspx">Nutrition and Exercise Clinic </a></td>
    <td class="servicename"><img src="images/serviceswomen/Perinatal Counselling for High Risk Foetuses and Neonates.png" width="46" height="46" /></td>
    <td class="servicename"><a href="women-perinatalcouns.aspx">Perinatal Counselling for High Risk Foetuses and Neonates </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/serviceswomen/Preconception and Premarital Counselling.png" width="47" height="47" /></td>
    <td class="servicename"><a href="women-preconcouns.aspx">   Preconception and Premarital Counselling </a></td>
    <td class="servicename"><img src="images/serviceswomen/Reproductive and infertility clinic.png" width="46" height="46" /></td>
    <td class="servicename"><a href="women-reprodclinic.aspx">Reproductive and infertility clinic </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/serviceswomen/Gynaecology.png" width="46" height="46" /></td>
    <td class="servicename"><a href="women-tertiaryservices.aspx">    Tertiary Level Neonatal Intensive care Services </a></td>
    <td class="servicename"><img src="images/serviceswomen/Urogynaecology.png" width="46" height="46" /></td>
    <td class="servicename"><a href="women-urogynaecology.aspx">Urogynaecology </a></td>
  </tr>
  
   
</table>
        <div class="devider_20px"></div>
 <div class="serviceheading" style="text-align:LEFT;">SUPPORTIVE SERVICES</div>
     <div class="devider_20px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="12%" height="55" valign="middle"><img src="images/serviceswomen/Anaesthesiology.png" width="46" height="52" /></td>
    <td class="servicename" width="46%"><a href="women-anaesthesiology.aspx">	Anaesthesiology  </a></td>
    <td class="servicename" width="10%"><img src="images/serviceswomen/Blood Bank.png" width="52" height="42" /></td>
    <td class="servicename" width="32%"><a href="women-bloodbank.aspx">Blood bank </a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/serviceswomen/Pathology Lab.png" width="46" height="46" /></td>
    <td class="servicename"><a href="women-pathologylab.aspx"> 	Pathology Lab </a></td>
    <td class="servicename"><img src="images/serviceswomen/Physiotherapy.png" width="46" height="46" /></td>
    <td class="servicename"><a href="women-physiotherapy.aspx">Physiotherapy </a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/serviceswomen/Social Service Department.png" width="52" height="44" /></td>
    <td class="servicename"><a href="women-socialservdept.aspx"> 	Social Service Department</a></td>
    <td class="servicename"><img src="images/serviceswomen/Ultrasonography.png" width="47" height="47" /></td>
    <td class="servicename"><a href="women-ultrasonography.aspx">Ultrasonography and Radiology</a></td>
  </tr>

   


</table>


</DIV>
    </div>
<%--        SUPER SPECIALITY--%>








<%--SUPPORTIVE START--%>



        
                  


<%--    SUPPORTIVE END--%>






























</div>

</div>

                   
<!-- ABOUT US CONTENT -->



</div>

		



</asp:Content>
