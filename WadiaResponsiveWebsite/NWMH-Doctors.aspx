﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Doctors.aspx.cs" Inherits="WadiaResponsiveWebsite.about_us_doctors_women" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




	<div class="row block05">
			
			<div class="row block05">
		<div class="col-full">
				


<div style="padding:0px 20px 20px 20px; width:auto">
             
    <div class="colorcontent">John Maxwell once said, "People don’t care how much you know until they know how much you care." This is a notion that we base all our work on. Our 'patient-first' philosophy stems from the understanding that providing comfort and care for each patient is as important as our highly specialized treatments. 
        <br />
Here at the Nowrosjee Wadia Maternity Hospital, we have a dedicated team of Gynaecology and Obstetrics specialists, aided by highly trained nurses to provide top quality, holistic treatment to women, with the care and attention that they deserve. 

           </div>
                    <br />


<table width="77%" align="center" cellpadding="0" cellspacing="0" class="table-doctors">

    <tr>
      <td width="32%" align="left" class="heading_table">SpecialIty</td>
      <td width="68%" class="heading_table">Name of the Doctors</td>
    </tr>


    <tr>
      <td class="doctor-content-bold"><strong>Medical Director</strong></td>
      <td class="doctor-content-bold"><strong>Dr. M. J. Jassawalla</strong></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
   <tr>
      <td align="left" class="doctor-content-bold"><strong>Professor, Head of the Unit</strong></td>
     <td class="doctor-content">Dr. K.R. Damania<br>
Dr. P.R. Satoskar<br>
Dr. P.N. Mhatre<br>
Dr. N.M. Narvekar<br>
Dr. G.D. Balsarkar
     </td>
    </tr>
        <tr>
      <td></td>
      <td></td>
    </tr>
   <%-- <tr>
      <td class="doctor-content-bold"><strong>Clinical Department</strong></td>
      <td class="doctor-content-bold"><strong>Name of the Doctors</strong></td>
    </tr>--%>
   <tr>
      <td class="doctor-content-bold"><strong>Honorary Consultant</strong></td>
     <td class="doctor-content">Dr. S.A. Bhalerao<br>
Dr.P.D.Tank<br>
Dr. S. G. Alhabadia<br>
Dr. Madhuri Patel<br>
Dr. Sujata Dalvi</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
   <tr>
      <td class="doctor-content-bold"><strong>Associate Professor</strong></td>
     <td class="doctor-content">Dr. Trupti.K. Nadkarni<br>
Dr. Vandana Bansal<br>
Dr. Rachana Dalmia<br>
Dr. Pooja .Bandekar<br>
Dr. Amol Pawar<br>
Dr. P. D. Lakhani</td>
    </tr>

</table>











</div>

















			</div>
		

</div>
    </div>
</asp:Content>
