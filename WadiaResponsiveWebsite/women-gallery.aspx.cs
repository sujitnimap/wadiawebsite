﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class women_gallary : System.Web.UI.Page
    {
        int ID = 0;
        int catid = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                ViewState["id"] = Convert.ToInt32(Request.QueryString["id"]);
                ID = Convert.ToInt32(ViewState["id"]);
            }

            if (!IsPostBack)
            {
                getalbumdata();
            }


            if (Session["catid"] != null)
            {

                catid = Convert.ToInt32(Session["catid"]);
                DataTable dt = CollectionHelper.GetDataTable(CCategory.Instance().GetCategoryDatabyid(catid));
                lbtncategorynm.Text = dt.Rows[0]["categorynm"].ToString();

            }
            else
            {
                lbtncategorynm.Visible = false;
            }

        }

        protected void getalbumdata()
        {


            IList<gallaryimg> albumdata = CGallary.Instance().GetgallaryimgData(ID);
            DataTable dt = CollectionHelper.GetDataTable(albumdata);

            DataTable albdata = CArtgallary.Instance().GetArtgallaryData(ID);

            if (albdata.Rows.Count > 0)
            {
                lblalbumname.Text = albdata.Rows[0]["albumname"].ToString();

                lbldescrp.Text = albdata.Rows[0]["description"].ToString();
            }
            else
            {
                lblalbumname.Text = "";
            }

            dlistalbumimages.DataSource = dt;
            dlistalbumimages.DataBind();

        }

        protected void lbtnCategory_Click(object sender, EventArgs e)
        {

            Response.Redirect("women-gallery-category.aspx");

        }


        protected void lbtncategorynm_Click(object sender, EventArgs e)
        {
            Response.Redirect("women-gallery-album.aspx?=" + catid);
        }

    }
}