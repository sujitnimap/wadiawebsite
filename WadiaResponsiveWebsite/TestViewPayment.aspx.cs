﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class TestViewPayment : System.Web.UI.Page
    {
        IList<paymentDetail> paymentDetailData;
        string flag = "";
        int conl = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Convert.ToString(Session["flag"]) == "")
            //{
            //    Response.Redirect("login.aspx");
            //}
            //{
            //    flag = Session["flag"].ToString();
            //}


            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        public void BindGrid()
        {
            try
            {
                //Fetch data from mysql database
                paymentDetailData = CPaymentDetail.Instance().GetPaymentDetails();

                //Bind the fetched data to gridview
                if (paymentDetailData.Count > 0)
                {
                    DataTable detailTable = ToDataTable(paymentDetailData);

                    grdvwpaymentDetails.DataSource = detailTable;
                    grdvwpaymentDetails.DataBind();
                }
                else
                {
                    grdvwpaymentDetails.DataSource = null;
                    grdvwpaymentDetails.DataBind();
                }
            }
            catch (Exception ex)
            {
                //((Label)error.FindControl("lblError")).Text = ex.Message;
                //error.Visible = true;

            }

        }

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void grdvwpaymentDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                Int32 id = Convert.ToInt32(grdvwpaymentDetails.DataKeys[index].Value.ToString());

                paymentDetailData = CPaymentDetail.Instance().GetPaymentDetails();


                IList<paymentDetail> data = paymentDetailData.Where(p => p.PaymentDetailID.Equals(id)).ToList();

                DataTable detailTable = ToDataTable(data);

                dlvpaymentDetails.DataSource = detailTable;
                dlvpaymentDetails.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }

        }

        protected void grdvwpaymentDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();

            grdvwpaymentDetails.PageIndex = e.NewPageIndex;
            grdvwpaymentDetails.DataBind();
        }

    }


}