﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-Latest-news.aspx.cs" Inherits="WadiaResponsiveWebsite.child_case_studies_aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row block05">
        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>LATEST NEWS</h2>
                </div>

                <br />

                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px">

                  <%--     <div class="devider_10px"></div>
                 This page is a collection of case-studies related to Wadia Hospitals.--%>                    

                    <div>

                        <div id="container">

                            <div class="pagination">
                                <asp:DataList RepeatDirection="Horizontal" class="page gradient"  runat="server" ID="dlPagerup"
                                    OnItemCommand="dlPagerup_ItemCommand">
                                    <ItemTemplate>

                                        <a id="pageno" runat="server">
                                            <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>
                                        </a>

                                    </ItemTemplate>
                                </asp:DataList>
                            </div>

                            <div class="devider_20px"></div>
                            <asp:DataList ID="dlistcasestudy" RepeatColumns="1" runat="server">
                                <ItemTemplate>
                                    <div class="devider_15px"></div>
                                    <asp:Label ID="lbltitle" Font-Size="17px" Font-Bold="true" Font-Names="Arial" runat="server" Text='<%#Eval("title") %>'></asp:Label>
                                    <br />
                                    <div class="devider_10px"></div>
                                    <asp:Label ID="lbldescription" Font-Size="15px" Font-Names="Arial" runat="server" Text='<%#Eval("description") %>'></asp:Label>
                                    <br />
                                    <div class="devider_10px"></div>
                                    <div class="titlewadiacontent" visible='<%#Convert.ToBoolean(Eval("isfileactive"))%>' runat ="server">
                                        <a target="_blank" href='/adminpanel/<%#Eval("filepath") %>'>Know More &nbsp;&nbsp;<img src="images/pdf.jpg" /></a>
                                        <div class="devider_10px"></div>
                                    </div>
                                    <div class="deviderline"></div>                                    
                                </ItemTemplate>
                            </asp:DataList>


                            <div class="pagination">
                                <asp:DataList RepeatDirection="Horizontal" class="page gradient" runat="server" ID="dlPagerdown"
                                    OnItemCommand="dlPagerdown_ItemCommand">
                                    <ItemTemplate>

                                        <a id="pageno" runat="server">
                                            <asp:LinkButton Enabled='<%#Eval("Enabled") %>' class="page gradient" runat="server" ID="lnkPageNo" Text='<%#Eval("Text") %>' CommandArgument='<%#Eval("Value") %>' CommandName="PageNo"></asp:LinkButton>
                                        </a>

                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </div>

                        <div class="devider_20px"></div>

                    </div>
                    <!-- ABOUT US CONTENT -->

                </div>
            </div>


        </div>




    </div>


</asp:Content>
