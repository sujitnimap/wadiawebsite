﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Careers.aspx.cs" Inherits="WadiaResponsiveWebsite.women_careers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>CAREERS</h2>
                </div>

                <br />

                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px">
                    We are always looking for individuals to join our dynamic and growing team of professionals. If you share our values and are passionate about helping us further our mission, send us your CV and a cover letter.
                    <br />
                    <br />

                    <strong>All recruitments by Wadia Hospitals are carried out directly without involving any agent or charging any fee.</strong>
                    <br />
                    <br />

                    <asp:Panel ID="pnlfornovacacy" Visible="false" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblSnNo" runat="Server" Font-Bold="true" Text='Unfortunately, we do not have any job vacancies at the moment. However, if you would like to be part of our team, please send in your application and we will keep you on our records for any new positions that open up.' />
                            </tr>
                            <tr>
                                <td>

                                    <asp:LinkButton ID="lnkbtn" Text="Apply Now" runat="server" OnClick="lnkbtn_Click"></asp:LinkButton>

                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />

                    <asp:Panel ID="pnljobvacacy" runat="server">
                        <div>We currently have the following vacancies: </div>
                        <br />
                        <asp:DataList ID="dlistjobvacancy" RepeatColumns="1" runat="server">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblSnNo" runat="Server" Font-Bold="true" Text='<%# Container.ItemIndex + 1 %>' />
                                            <asp:Label ID="lbl" runat="Server" Font-Bold="true" Text='.' />
                                            <asp:Label ID="lbltitle" runat="server" Font-Bold="true" Text='<%#Eval("title") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbldescription" runat="server" Font-Size="14px" Text='<%#Eval("description") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <asp:LinkButton ID="lnkbtn" PostBackUrl='<%#"NWMH-Careers-Apply.aspx?title="+ Eval("title") %>' Text="Apply Now" runat="server"></asp:LinkButton>

                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </ItemTemplate>
                        </asp:DataList>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="Server" Font-Bold="true" Text='If you would like to be part of our team for any other positions, please send in your application and we will keep you on our records for any new positions that open up.' />
                                </td>
                            </tr>
                            <tr>
                                <td>

                                    <asp:LinkButton ID="lnkbtnopencv" Text="Apply Now" runat="server" OnClick="lnkbtnopencv_Click"></asp:LinkButton>

                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                </div>
                <!-- ABOUT US CONTENT -->





            </div>
        </div>







    </div>

</asp:Content>
