﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-doctdetails-drmukund.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drmukund" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
				<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Mukund Thatte.jpg" />
<div class="docdetails">
Mukund R Thatte

<div class="docdetailsdesg">
Asst. Hon. Burns & Plastic

<br />
M.S. (gen.) M.Ch. (plastic)


</div>

</div>
</div>

                    <div class="areaofinterest">

Areas of interest : Hand, Brachial Plexus, and Clefts</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Research and publications</a></li>
            <%--<li><a href="#view4">Research or publications</a></li>--%>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
•	Past president Association of Plastic Surgeons of India
<br />•	Past President Indian society of Cleft lip Palate and Craniofacial anomalies
<br />•	Past President Brachial plexus Surgery Group of India
<br />•	Editor Indian Journal of Plastic Surgery 2003-08
<br />•	Editorial Board: JHS(Eur), THUES, JPRAS
<br />•	Delivered the Sir Harold Gillies oration and Murari Mukherjee Oration of association of Plastic Surgeons of India, KT Dholakia Oration of Bombay Orthopaedic Society



<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
  More than 40 publications published till date in peer-reviewed and academic journals.

    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
		







		</div>


</asp:Content>
