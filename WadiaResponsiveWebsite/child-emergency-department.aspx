﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-emergency-department.aspx.cs" Inherits="WadiaResponsiveWebsite.child_emergency_department" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>EMERGENCY DEPARTMENT</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">

    <strong>Overview </strong>
    <br />    <br />


The Emergency Department at BJWHC is equipped to receive patients at any time of the day or night It is the first point of contact for patients critical or otherwise. Children who are brought in a critical condition are stabilised here before being transferred to the relevant ward for further treatment and care. The department witnesses close to 100,000 patients annually who avail emergency care at the hospital. To ensure that your child is seen by a paediatric specialist when it is most needed, the Emergency Department has a team of senior Paediatric specialists and subspecialists on call, 24 hours a day. To provide better emergency services every time a patient visits BJWHC we formulate and implement innovative strategies from time to time to reduce patient waiting time, enhance the atmosphere in the waiting room, and improve the delivery of patient care.
    <div style="height:150px"></div>



</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        



		</div>



</asp:Content>
