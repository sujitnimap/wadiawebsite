﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-dralaric.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_dralaric" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
		<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dr alaric aroojis.jpg" />
<div class="docdetails">

Dr. Alaric John Aroojis
    <div class="docdetailsdesg">
Asst. Hon. Ped Orthopaedics
    <br />
D'ortho, MS, DNB-Ortho, Fellow Ped Ortho. 



</div>

</div>
</div>

                    <div class="areaofinterest">


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
  <b>International :</b>

                    <div style="padding-left:30px"><br />
•	Gold Medal and Research Scholarship awarded by the Ranawat Orthopaedic Research Foundation (RORF), Lenox Hill Hospital, New York, U.S.A. for Research project titled  “Anthropometric measurements to design Total Knee prosthesis for the Indian population – a CT scan and anatomic study”, for the year 1997-1998. 
Co-researchers : S.V. Vaidya, Professor & Chief of Unit, King Edward VII Memorial Hospital; Prof N.S. Laud, Shushrusha Hospital & Past President, Indian Orthopaedic Association.
<br /><br />
•	World Orthopaedic Concern – SICOT Scholarship 2002 to attend the XXII Triennial World Congress of SICOT at San Diego, USA, August 2002.
<br /><br />
•	Japanese – SICOT Scholarship 2002 to attend the XXII Triennial World Congress of SICOT at San Diego, USA, August 2002 and present scientific papers.
<br /><br />
•	APOA Paediatric Orthopaedic Travelling Fellowship 2004 to visit Paediatric Orthopaedic centers in Australia and attend the XIII Triennial Congress of the Asia-Pacific Orthopaedic Association (APOA) at Kuala Lumpur, Malaysia, 20th August – 10th September 2004.  
<br /><br />
•	IOA – SICOT Scholarship 2005 to attend the XXIII Triennial World Congress of SICOT at Istanbul, Turkey, September 2005 and present scientific papers.
</div>
<br />
<b>National :</b>


            <br />        <div style="padding-left:30px">


•	Dr. Shantilal Sheth Gold Medal awarded by the College of Physicians and Surgeons, for standing first at the Diploma in Orthopaedics (D’Ortho) examinations held in October 1995.
<br /><br />
•	Trophy awarded by the Bombay Orthopaedic Society for paper titled  “Rotation fasciocutaneous flap for neglected clubfoot - a new technique”, presented at the Young Surgeon’s Forum of the Western India Regional Orthopaedic Conference (WIROC) in Mumbai, December 1996.
<br /><br />
•	Dr. V.K. Kapur Scholarship for original research in Paediatric Surgery for project titled “Rotation fasciocutaneous flap for neglected clubfoot - a new technique”, in 1997.
<br /><br />
•	1st prize at the State Level Orthopaedic Quiz organized by the Indian Orthopaedic Association at the 15th Annual Maharashtra Orthopaedic Association Conference, Nanded, October 1998.
<br /><br />
•	National Rolling Trophy and Prize awarded by the Indian Orthopaedic Association, for securing first place among all state chapters at the National Orthopaedic Quiz conducted at the 43rd Annual Conference of the Indian Orthopaedic Association, Jabalpur, December 1998.
<br /><br />
•	Research work on “Lateral Gliding Movements of the Neck and its Utilization in the Management of Congenital Muscular Torticollis”: Silver Jubilee Commemoration Award Oration, presented to Principal researcher Dr. A.N. Johari at 44th Annual Conference of IOA, Hyderabad, 1999. 
<br /><br />
•	Travel Award by Sir Ratan Tata Trust and R.D. Birla Smarak Kosh to perform Visiting Fellowship in Paediatric Orthopaedics at specialized centers in USA, July – October 1999.
<br /><br />
•	POSI – Best Paper Award for “Epiphyseal separations in spastic cerebral palsy: is scurvy a cause?” at the 6th Conference of Paediatric Orthopaedic Society of India, Mumbai, March 2000.
<br /><br />
•	1st prize at the Paediatric Orthopaedic Quiz organized by the Paediatric Orthopaedic Society of India (POSI) at the 9th Annual POSI Conference, Vellore, January 2003. 
<br /><br />
•	Best Resident of the Year award for dedicated and meritorious service, presented at the Foundation Day Celebrations of Seth G. S Medical College & King Edward VII Memorial Hospital on 21st January 1993 by the Hon. Mayor of Bombay.
<br /><br />
•	Nominated for inclusion in Who’s Who in Medicine and Healthcare for the year 2000-2001, 2002-2003 published by Marquis Who’s Who, NJ, USA. 
</div>

<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
Life Member of <br />
•	Indian Orthopaedic Association (IOA) – LM 2815
<br /><br />•	Bombay Orthopaedic Society (BOS) – BOM/A-17
<br /><br />•	Pediatric Orthopaedic Society of India (POSI) – LM 196, which is an Alliance Member of the Pediatric Orthopaedic Society of North America (POSNA) and the International Federation of Paediatric Orthopaedic Societies (IFPOS).
<br /><br />•	World Orthopaedic Concern – LM 446 
<br /><br />•	Association of Medical Consultants, Mumbai 
<br /><br />•	Asia-Pacific Orthopaedic Association (APOA)
<br /><br />•	Paediatric Orthopaedic Specialty Section of APOA
<br /><br />•	Children’s Orthopaedic Research and Education (CORE) Group
<br /><br />•	International Clubfoot Study Group (ICFSG) 


    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
<b>International :</b>
<br /><br />
•	Rotation fasciocutaneous flap for neglected clubfoot - a new technique.                    
H D’souza, AJ Aroojis, MG Yagnik. J Pediatric Orthopaedics, USA 1998;8(3):319 22.						
<br /><br />•	Infantile periosteitis - differential diagnosis and management.                                    
AJ Aroojis, H D’souza, MG Yagnik. Postgraduate Medical Journal, London,1998;74:307-9.

<br /><br />•	Total knee replacement in Indians - are adequate sizes available?                            
SV Vaidya, AJ Aroojis, S Suralkar. J Orthopaedic Surgery, Hongkong 1997;5:65–72.

<br /><br />•	Fracture-separation of the proximal humeral epiphysis. AJ Aroojis, H D’souza, MG Yagnik. Postgraduate Medical Journal, London 1998;74(878):752 – 5.

<br /><br />•	Talectomy in arthrogryposis - analysis of results.                                                                              H D’souza, AJ Aroojis, GS Chawara. J Paediatric Orthopedics, USA 1998;18(6):760–4.
<br /><br />•	Multiple Joint Replacement in chronically neglected polyarthritic patients – problems and results. SV Vaidya, AJ Aroojis. J Orthopaedic Surgery, Hongkong, September 2000.

<br /><br />•	Arthropometric measurements to design Total Knee Prosthesis for the Indian population. SV Vaidya, CS Ranawat, AJ Aroojis, NS Laud. Journal of Arthroplasty, USA 2000 Jan;15(1):79-85.

<br /><br />•	Epiphyseal separations after neonatal osteomyelitis and septic arthritis.
AJ Aroojis, AN Johari. J Pediatric Orthopaedics, USA 2000 Jul-Aug;20(4):544-9.

<br /><br />•	Congenital vertical talus in arthrogryposis and other contractural syndromes.                    
                    
                    <br />            AJ Aroojis, M King, M Donohoe, EC Riddle, S Jay Kumar. Clin Orthop (CORR) 2005;434:26-32.

•	Classification of hip dysplasia in adolescents and young adults and treatment by triple innominate osteotomy. AJ Aroojis, SA Shah, JR Bowen (accepted by J Paediatric Orthopaedics, April 2005).
<br /><br />
•	Epiphyseal separations in children with spastic cerebral palsy.                 <br />
                                                         AJ Aroojis, SM Gajjar, AN Johari (accepted by J Pediatric Orthopaedics - B July 2005).
<br /><br />
•	Intrathecal baclofen pump and posterior spinal fusion in children with spastic cerebral palsy.   <br />  AJ Aroojis, SA Shah, KW Dabney, F Miller.

<br /><br />•	Bony landmarks to decide femoral component rotation in total knee arthroplasty  –  Do  values differ in Indians? SV Vaidya, CS Ranawat, DV Dholakia, AJ Aroojis (submitted to J Arthroplasty)         

<br /><br />•	Co-authored Chapter 1: “Epidemiology of Children’s Fractures” with Dr. Kaye E. Wilkins in Rockwood and Wilkins’ Textbook of “Fractures in Children”, Editors Kasser J, Beaty JH, Lippincott Williams and Wilkins, 5th Ed 2001.
<br /><br />

<b>National :</b>
<br /><br />
•	THESIS : Failed femur plating - a retrospective study. Dissertation submitted for Master of Surgery (MS) exams to the University of Mumbai, January 1996.
<br /><br />
•	Rotation fasciocutaneous flap for neglected clubfoot - a preliminary report.<br />
H D’souza, AJ Aroojis, MG Yagnik. J Postgraduate Medicine 1996;42(4):112-4.
<br /><br />
•	Hume’s fracture. H D’souza, AJ Aroojis, MG Yagnik. Indian J of Orthopedics 1997;31(3):217-9.
<br /><br />
•	Congenital muscular torticollis - results of treatment.  AJ Aroojis, H D’souza. <br />
      Bombay Hospital Journal 1997;39(4):687-9.
<br /><br />
•	Irreducible lateral dislocation of the elbow. M Chhaparwal, AJ Aroojis, M Divekar, S Kulkarni, SV Vaidya. J Postgraduate Medicine 1997 Jan-Mar;43(1):19-20.
<br /><br />
•	Histiocytosis X of scapula : a case report.  AJ Aroojis, KB Bhavnani, AN Johari. Bombay Hospital Journal 1998;40(1):171-3.
<br /><br />
•	Caudal regression syndrome. AJ Aroojis, KB Bhavnani, R Himmatramka. Indian J of Orthopaedics 1998;32(3):205–7.
<br /><br />
•	Caudal anaesthesia for lower limb orthopaedic surgery in children.                                                A Negi, AJ Aroojis , F  Naregal, S  Nobre. Indian J of Anaesthesia 1999;43(3):35-8.
<br /><br />
•	Post traumatic unreduced posterior fracture dislocation of the knee. <br />
SM Gajjar, SV Vaidya, AJ Aroojis. Bombay Hospital Journal 1999;41(4):768-70.
<br /><br />
•	Arthrogryposis multiplex congenita : Current concepts and review of the “wooden doll” syndrome. AJ Aroojis, SM Gajjar. Bombay Hospital Journal 1999;41(4):731-4.
<br /><br />
•	Abduction osteotomy for non-union fracture neck femur. SM Gajjar, AJ Aroojis,<br />
MG Agarwal, ML Saraf. Indian J of Orthopaedics 2000.

<br /><br />•	Current Concepts : Septic arthritis in childhood and Legg-Calve-Perthes disease. Postgraduate Instructional Course Handbook, 6th Annual Conference of POSI, March 2000.

<br /><br />•	Neonatal and Infantile Osteomyelitis and Septic Arthritis. Proceedings of the 18th COE of Orthopaedic Research and Education Foundation. Edited by Dr. D. K. Taneja, September 2000.

<br /><br />•	“Physeal Injuries”, “Elbow injuries in Children”, “Hip fractures in Children”, and “Ankle & tarsal fractures and injuries in Children”. Proceedings of the 19th COE of Orthopaedic Research and Education Foundation. Edited by Dr. D. K. Taneja, September 2001.

<br /><br />•	“Flatfoot” in the Symposium on Paediatric foot. AN Johari, AJ Aroojis, T Agrawal, FD Wadia. Orthopaedics Today 2002;4(3):158-67.

<br /><br />•	Developmental Dysplasia of the Hip. IJM Orthopaedics & Rheumatology 2003;1(1):14-23.

<br /><br />•	The Ponseti Method of Clubfoot Treatment. Asian J Orthop & Rheumatology 2005;2(3):10-15.


                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			



         </div>
</asp:Content>
