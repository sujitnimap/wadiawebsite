﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-doctdetails-drira.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drira" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Ira shah.jpg" />
<div class="docdetails">
Dr. Ira Shah



<div class="docdetailsdesg">
Associate Professor    <br />
MD (Paediatrics), DCH, FCPS, DNB, Diploma in Paediatric Infectious Diseases



</div>

</div>
</div>

     <div class="areaofinterest">

    Areas of Interest : Paediatric Infectious Diseases, Paediatric Hepatology

</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
           <%-- <li><a href="#view3">Affiliations</a></li>--%>
            <li><a href="#view4">Research or publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
     •	Editorial Board Member
                    <div style="padding-left:25px;">
o	Concise Reviews of Paediatric Infectious Diseases Journal
                        <br />
o	Paediatrics and International Child Health
                        <br />
o	AIDS Research and Therapy
                        <br />
o	JK Science
                        <br />
o	AIDS Update
                        <br />
o	Bulletin of Haffkine Institute
                   </div>
  <br />
•	Asian Editor – Journal of Paediatric Infectious Diseases (2008-2010)

                        <br />  <br />
•	Editor – Paediatric Oncall

<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
           <%-- <div id="view3">
               
               

                <p>
     Professor, Department of Paediatrics, Division of Paediatric Endocrinology

    <br /><br />
    
</p>
               
            </div>--%>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
Published over 24 papers in reputed peer reviewed national as well as international journals <br /><br />
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			





		</div>
</asp:Content>
