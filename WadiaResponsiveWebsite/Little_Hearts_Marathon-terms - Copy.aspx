﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="Little_Hearts_Marathon-terms.aspx.cs" Inherits="WadiaResponsiveWebsite.Little_Hearts_Marathon_terms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>Terms & Conditions</h2>
                </div>
                                
                <br />
                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px;">
                    &#8226; Please note Last Date of registration 4th February 2016 subject to availability.
                    <br />
                   <br />
&#8226; In case of any illness or disability please mail Fitness Certificate from your 
treating Doctor to <a href="mailto:marathon@wadiahospitals.org">marathon@wadiahospitals.org</a>.
                    <br />
                    <br />
                    • The child&#39;s age should be from 3 to 17 years (For age group 3 to 12 it is mandatory to run with parent/guardian who will take the responsibility of the child throughout the Marathon to avoid any incident.)<br />
                    <br />
                    &#8226; The Little Hearts Marathon is an event to spread awareness on Cardiac diseases: 

Prevention of cardiac diseases in children and also a fund raising event for the 

mentioned cause.

                    <br />
                    <br />
                    &#8226; Marathon will start from Siddhivinayak Temple Prabhadevi to Bai Jerbai Wadia 

Hospital for Children and all children will get certificates; there will not be any 

competitive prizes.<br />
                    <br />
                    &#8226; All registration fees are non-refundable. In the event you cannot participate, 

please consider your registration fee as generous donation towards “Little Hearts 

Marathon” alternatively you may choose to sponsor a run by writing a mail to us 

before 5th February 2016 on <a href="mailto:marathon@wadiahospitals.org">marathon@wadiahospitals.org</a>
                    <br />
                    <br />
                    &#8226; It is mandatory for all confirmed participants to come to the BJWHC on 5th and 6th 

February 2016 between 10 am to 5 pm to collect T-shirts along with valid photo 

ID and copy of invoice.<br />
                    <br />
                    &#8226; Information against all mandatory fields prefixed with ‘*’ on the entry form must 

be filled. Incomplete forms will be rejected.<br />
                    <br />
                    &#8226; This Entry Form and the right to participate in the event and the rights and 

benefits available to the applicant under this application form is at the sole 

discretion of Little Hearts Marathon and cannot be transferred to any other 

person under any circumstances, and the applicant alone shall be entitled to the 

rights and benefits arising hereunder. <br />
                    <br />
                    &#8226; Please go through all information mentioned in this website prior to submitting 

your application. Conformation of application received will be subject to the 

applicable qualification criteria and entry rules & Guidelines given herein.
                    <br />
                    <br />



                    <strong>I declare, confirm and agree as follows and I/my word</strong>
                    <br />
                    <br />
                    (i) I agree to the entry terms and guidelines.
                    <br />
                    <br />
                    (ii) The information given by me in this entry form is true and I am solely responsible for the accuracy of this information;
                    <br />
                    <br />
                    (iii) Have fully understood the risk and responsibility of participating in the event and will be participating entirely at my own risk and responsibility;
                    <br />
                    <br />
                    (iv) The security of my child is solely the responsibility of the accompanying parent/ guardian. The Organisers shall not be held responsible for any security related incident that may occur during the course of the marathon.
                    <br />
                    <br />
                    (v) Any illness/ medical condition/ ailment have to be reported to the Organisers in detail. If the parents fail to intimate the exact medical condition of the child, orself; then the Organisers shall not be held responsible for any mishap that may occur or arise due to the such illness/ medical condition/ ailment.
                    <br />
                    <br />
                    (vi) Understand the risk of participating on a course with vehicular traffic, even if the course may be regulated / policed;
                    <br />
                    <br />
                    (vii) Understand that I must be of, and must train to, an appropriate level of fitness to participate in such a physically demanding event and I have obtained a medical clearance from a registered medical practitioner, allowing me to participate in the event/s;
                    <br />
                    <br />
                    (viii) The parents/ guardians are required to strictly follow the instructions given by the Organisers and incase of the difficulty forthwith report to the Organisers.
                    <br />
                    <br />
                    (ix) For myself/ourselves and our legal representatives, waive all claims of whatsoever nature against any and all Sponsors of the event, all political entities, authorities and officials, all contractors and construction firms working on or near the course, all Committee persons, officials and volunteers, Little Hearts Marathon, Wadia Hospitals, and all other persons and entities associated with the event and the directors, employees, agents and representatives of all or any of the aforementioned including, but not limited to, any claims that might result from me participating in the event and whether on account of illness, injury, death or otherwise;
                    <br />
                    <br />
                    (x) Agree that if I am injured or taken ill or otherwise suffer any detriment whatsoever, I hereby irrevocably authorize the event officials and organizers to, at my own risk and cost, transport me to a medical facility and/or to administer emergency medical treatment and I waive all claims that might result from such transport and/or treatment or delay or deficiency therein. I shall pay or reimburse to you my medical and emergency expenses and I hereby authorize/s you to incur the same;
                    <br />
                    <br />
                    (xi) Shall provide to race officials such medical data relating to me as they may request. I agree that nothing herein shall oblige the event officials or organizers or any other person to incur any expense or to provide any transport or treatment;
                    <br />
                    <br />
                    (xii) In case of any illness or injury caused to me or death suffered by me or my due to any force majeure event including but not limited to fire, riots or other civil disturbances, earthquakes, storms, typhoons or any terrorist act, none of the sponsors of the event or any political entity or authorities and officials or any contractor or construction firms working on or near the course, or any of the Man and Mountain Meet persons, officials or volunteers, Wadia Hospitals, Little Hearts Marathon or any persons or entities associated with the event or the directors, employees, agents or representatives of all or any of the aforementioned shall be held liable by me or my representatives;
                    <br />
                    <br />
                    (xiii) Understand, agree and irrevocably permit Wadia Hospitals and Little Hearts Marathon to share the information given by me in this application, with all/any entities associated with this event, at its own discretion<br />
                    <br />
                    (xiv) Understand, agree and irrevocably permit Wadia Hospitals and Little Hearts Marathon to my photograph and record (on film, tape, hard drive, or any other method) my performance, appearance, name and/or voice and the results in perpetuity for the purpose of promoting the Event, at its own discretion.<br />
                    <br />
                    (xv) Further authorize Wadia Hospitals and Little Hearts Marathon to edit the same at discretion and to include it with performance of other and with sound effects, special effects, music etc. in any manner or media whatsoever, including without limitation, unrestricted use for purpose of publicity, advertising and sales promotion; and to use my name. Wadia Hospitals and Little Hearts Marathon owns all rights to the results and proceeds of my services in connection 
                    herewith; 
                    <br />
                    <br />
                    (xvi) I understand that it is at the discretion of Wadia Hospitals and Little Hearts Marathon to make any changes in the terms conditions and guidelines for the event.<br />
                    <br />



                </div>

            </div>




            <%--    SUPPORTIVE END--%>
        </div>

    </div>

</asp:Content>
