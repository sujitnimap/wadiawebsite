﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-doctdetails-drdominic.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drdominic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
		<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dominic.jpg" />
<div class="docdetails">
Dr. Dominic D’Silva
<div class="docdetailsdesg">
Professor
    <br />
D'Ortho, MS, DNB-Ortho



</div>

</div>
</div>

                    <div class="areaofinterest">

Professional Experience : 32 years
Areas of Interest: Paediatric Orthopaedics, Arthroscopic Surgery and Sports Medicine
</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
  •	Shantilal C. Sheth Prize for First Rank in D.Ortho. Examination held by College of Physicians and Surgeons, Bombay, 1983
<br /><br />•	Second Rank in the M.S. (Orthopaedics) Examination. Held by the University of Bombay, 1984.
<br /><br />•	Awarded Prize for the Best paper. Tumours of the Hand at the 12th Annual Conference of the Indian Society for Surgery of the Hand, Bombay, 1986.




<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	Member, Indian Orthopedic Association
<br /><br />•	Member, Pediatric Orthopedic Society of India
<br /><br />•	Member, Bombay Orthopedic Society 
<br /><br />•	Member, Indian Medical Association
<br /><br />•	Member, Association of Medical Consultants, Bombay


    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Guillain-Barre syndrome in the J.J. Hospital Research Society Journal. Vol. Xxv Pg. 171-182, 1980.
  <br /><br />•	Arthrogryposis Multiplex Congenita –an Overview in the Clinical Orthopaedics, India Vol. 6 – 1992.
  <br /><br />•	Arthroscopy – It’s role in the management of problems of the knee in the Chembur IMA Magazine, 1993.
  <br /><br />•	Bowlegs in Children in the Chembur IMA Magazine, 1997
  <br /><br />•	The Role of Arthroscopy in Children in the Journal of The Lilavati Hospital, 2000
  <br /><br />•	The Role of Arthroscopy in Children in the Holy Family Hospital Newsletter, Jan-Feb, 2001



                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			



         </div>
</asp:Content>
