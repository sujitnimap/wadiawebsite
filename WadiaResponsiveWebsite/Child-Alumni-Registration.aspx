﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="Child-Alumni-Registration.aspx.cs" Inherits="WadiaResponsiveWebsite.Child_Alumni_Registration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/jquery-1.9.1.min.js"></script>

    <script type="text/javascript">

        function cv_Gender(source, args) {
            if (document.getElementById("<%= rdbtnMale.ClientID %>").checked || document.getElementById("<%= rdbtnFemale.ClientID %>").checked) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }
        function cv_RegistrationCategory(source, args) {
            if (document.getElementById("<%= rdbtnLife.ClientID %>").checked || document.getElementById("<%= rdbtnAnnual.ClientID %>").checked || document.getElementById("<%= rdbtnNRI.ClientID %>").checked) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }

        function OnRegCatChange() {
            var totalAmount = 0;
            if ($('#<%=rdbtnLife.ClientID%>').is(":checked")) {
                totalAmount = $('#<%=hdfLifeFeesValue.ClientID%>').val();
                $('#<%=lblTotalAmountInINR.ClientID%>').text(totalAmount)
                $('#<%=hdfTotalPayableAmount.ClientID%>').val(totalAmount);

                var dvPassport = document.getElementById("tblDetails");
                var dvSubmit = document.getElementById("divSubmitButton");
                var tblNRIRegistration = document.getElementById("tblNRIRegistration");
                dvPassport.style.display = "block";
                dvSubmit.style.display = "block";
                tblNRIRegistration.style.display = "none";
                return false;
            }
            if ($('#<%=rdbtnAnnual.ClientID%>').is(":checked")) {
                totalAmount = $('#<%=hdfAnnualFeesValue.ClientID%>').val();
                $('#<%=lblTotalAmountInINR.ClientID%>').text(totalAmount)
                $('#<%=hdfTotalPayableAmount.ClientID%>').val(totalAmount);

                var dvPassport = document.getElementById("tblDetails");
                var dvSubmit = document.getElementById("divSubmitButton");
                var tblNRIRegistration = document.getElementById("tblNRIRegistration");
                dvPassport.style.display = "block";
                dvSubmit.style.display = "block";
                tblNRIRegistration.style.display = "none";
                return false;
            }
            <%--if ($('#<%=rdbtnNRI.ClientID%>').is(":checked")) {
               
            }--%>
            $('#<%=lblTotalAmountInINR.ClientID%>').text(totalAmount)
            $('#<%=hdfTotalPayableAmount.ClientID%>').val(totalAmount);
            //return false;
        }

        function ShowHideDiv(rdbtnNRI) {
            var dvPassport = document.getElementById("tblDetails");
            var dvSubmit = document.getElementById("divSubmitButton");
            var tblNRIRegistration = document.getElementById("tblNRIRegistration");

            
            dvPassport.style.display = rdbtnNRI.checked ? "none" : "block";
            dvSubmit.style.display = rdbtnNRI.checked ? "none" : "block";
            tblNRIRegistration.style.display = rdbtnNRI.checked ? "block" : "none";
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ToolkitScriptManager runat="server"></asp:ToolkitScriptManager>
    <asp:ValidationSummary ID="vsCMEReg" runat="server" DisplayMode="BulletList"
        ShowSummary="false" ShowMessageBox="true" ValidationGroup="vgAlumni" />


    <div class="row block05">
        <div class="col-full">
            <div class="wrap-col">

                <div style="border: 1px solid #ddd; padding: 10px 20px 20px 10px; border-radius: 4px;">
                    <asp:HiddenField ID="hdfTotalPayableAmount" runat="server" Value="" />
                    <div class="heading">
                        <h2>Wadia Children’s Alumni Registration</h2>
                    </div>

                    Bai Jerbai Wadia Hospital for Children,
                    <br />
                    Acharya Donde Marg, Parel, Mumbai -400012
                    <br />
                    <br />
                    
                    <table >
                        <tr>
                            <td class="tdAlumniWidth textboxcontact_list_rdb">Registration Category</td>
                            <td class="textboxcontact_list_rdb">
                                <asp:RadioButton ID="rdbtnLife" runat="server" Text="&nbsp;Life" GroupName="RegCategory" onclick="OnRegCatChange();" />&nbsp;&nbsp;&nbsp;&nbsp;                               
                            <asp:RadioButton ID="rdbtnAnnual" runat="server" Text="&nbsp;Annual" GroupName="RegCategory" onclick="OnRegCatChange();" />&nbsp;&nbsp;&nbsp;&nbsp;                                
                            <asp:RadioButton ID="rdbtnNRI" runat="server" Text="&nbsp;NRI" GroupName="RegCategory" onclick="ShowHideDiv(this)" />
                                <asp:CustomValidator ID="cvRegCat" runat="server" ErrorMessage="Please Select Registration Category"
                                    ClientValidationFunction="cv_RegistrationCategory" OnServerValidate="cv_RegistrationCategory"
                                    ForeColor="Red" ValidationGroup="vgAlumni">*</asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                    <table id="tblNRIRegistration" style="display:none" >
                        <tr>
                            <td colspan="2">For NRI: Registration Fees is $500 U.S.D. Kindly Deposit Registration Fees in below account</td>
                            
                        </tr>
                        <tr>
                            <td colspan="2"><u><b>Account Details</b></u></td>                            
                        </tr>
                        <tr>
                            <td><u><b>Account Name</b></u></td>
                            <td>"Bai Jerbai Wadia Hospital For Children"</td>
                        </tr>
                        <tr>
                            <td><u><b>Account Number</b></u></td>
                            <td>3177417760</td>
                        </tr>
                        <tr>
                            <td><u><b>IFSC Code</b></u></td>
                            <td>CBIN0281209</td>
                        </tr>
                        <tr>
                            <td><u><b>Remarks</b></u></td>
                            <td>"Wadia Children's Almuni."(WCA)</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Please contact the undersigned for any further details.
                            </td>

                        </tr>
                         <tr>
                            <td>Dr. Sumitra.Venkatesh  
                                </td>
                             <td>9821086541</td>
                        </tr>
                        <tr>
                            <td>Dr. Shakuntala Prabhu  
                                </td>
                            <td>9920110935</td>
                        </tr>
                    </table>
                    <table id="tblDetails">                      

                        <tr>
                            <td class="tdAlumniWidth">Name</td>
                            <td>
                                <asp:TextBox ID="txtName" CssClass="textboxcontact_list" required="" runat="server" placeholder="Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvName" ValidationGroup="vgAlumni" ControlToValidate="txtName" runat="server" ForeColor="Red" ErrorMessage="Please Enter Name">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="textboxcontact_list_rdb">Gender</td>
                            <td class="textboxcontact_list_rdb">
                                <asp:RadioButton ID="rdbtnMale" runat="server" Text="&nbsp;Male" GroupName="Gender" />&nbsp;&nbsp;&nbsp;&nbsp;                               
                            <asp:RadioButton ID="rdbtnFemale" runat="server" Text="&nbsp;Female" GroupName="Gender" />
                                <asp:CustomValidator ID="cvGender" runat="server" ErrorMessage="Please Select Gender"
                                    ClientValidationFunction="cv_Gender" OnServerValidate="cv_Gender"
                                    ForeColor="Red" ValidationGroup="vgAlumni">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td>
                                <asp:TextBox ID="txtDOB" CssClass="textboxcontact_list" required="" runat="server"
                                    placeholder="Date of Birth*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date of Birth'">
                                </asp:TextBox>
                                <asp:CalendarExtender ID="txtDOB_calender" runat="server" TargetControlID="txtDOB">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reftxtDOB" ValidationGroup="vgAlumni"
                                    ControlToValidate="txtDOB" runat="server" ForeColor="Red"
                                    ErrorMessage="Please Select Date Of Birth">*
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Permanent Address</td>
                            <td>
                                <asp:TextBox ID="txtPermanentAddress" TextMode="MultiLine" CssClass="textboxcontact_list" runat="server" placeholder="Permanent Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Permanent Address*'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPermanentAddress" ValidationGroup="vgAlumni" ControlToValidate="txtPermanentAddress" runat="server" ForeColor="Red" ErrorMessage="Please Enter Permanent Address">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Mobile</td>
                            <td>
                                <asp:TextBox ID="txtMobileNo" CssClass="textboxcontact_list" MaxLength="10" required=""
                                    runat="server" placeholder="Mobile Number*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile Number'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvMobileNo" ValidationGroup="vgAlumni"
                                    ControlToValidate="txtMobileNo" ForeColor="Red" runat="server" ErrorMessage="Please enter Mobile Number">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revMobileNo" Display="None" ValidationGroup="vgAlumni" ValidationExpression="^[0-9]{10}$"
                                    ControlToValidate="txtMobileNo" SetFocusOnError="true" runat="server" ErrorMessage="Enter 10 digit numeric value in Contact Number"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Tel. No</td>
                            <td>
                                <asp:TextBox ID="txtTelNo" CssClass="textboxcontact_list" MaxLength="10" required=""
                                    runat="server" placeholder="Tel. No*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Tel. No'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvTelNo" ValidationGroup="vgAlumni"
                                    ControlToValidate="txtTelNo" ForeColor="Red" runat="server" ErrorMessage="Please enter Tel No">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revTelNo" Display="None" ValidationGroup="vgAlumni" ValidationExpression="^[0-9]{10}$"
                                    ControlToValidate="txtTelNo" SetFocusOnError="true" runat="server" ErrorMessage="Enter 10 digit numeric value in Tel Number"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>
                                <asp:TextBox ID="txtEmailId" CssClass="textboxcontact_list" required="" runat="server"
                                    placeholder="Email ID*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email ID*'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmailId" ValidationGroup="vgAlumni" ControlToValidate="txtEmailId" ForeColor="Red"
                                    runat="server" ErrorMessage="Please enter Email">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEmailId" Display="None" runat="server" ControlToValidate="txtEmailId" ErrorMessage="Enter a valid Email"
                                    ValidationExpression="^([a-zA-Z0-9_\-\.]+)@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$" ValidationGroup="vgAlumni"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Professional Address</td>
                            <td>
                                <asp:TextBox ID="txtProfessionalAddress" TextMode="MultiLine" CssClass="textboxcontact_list" runat="server" placeholder="Professional Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Professional Address*'">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvProfessionalAddress" ValidationGroup="vgAlumni" ControlToValidate="txtProfessionalAddress" runat="server" ForeColor="Red" ErrorMessage="Please Enter Professional Address">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Tel. No
                            </td>
                            <td>
                                <asp:TextBox ID="txtProfTelNo" CssClass="textboxcontact_list" runat="server" placeholder="Tel. No*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Tel. No*'"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvProfTelNo" ValidationGroup="vgAlumni" ControlToValidate="txtProfTelNo" runat="server" ForeColor="Red" ErrorMessage="Please Enter Tel. No">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Details of education at BJWHC</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table id="tblEducation" border="1" style="width: 100%;">
                                    <tr>
                                        <th style="height: 40px;">College</th>
                                        <th style="height: 40px;">Degree</th>
                                        <th style="height: 40px;">Year Of Joining</th>
                                        <th style="height: 40px;">Year Of Completion</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCollege" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="College*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'College*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCollege" ValidationGroup="vgAlumni" ControlToValidate="txtCollege" runat="server" ForeColor="Red" ErrorMessage="Please Enter College">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDegree" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="Degree*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Degree*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvDegree" ValidationGroup="vgAlumni" ControlToValidate="txtDegree" runat="server" ForeColor="Red" ErrorMessage="Please Enter Degree">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtYearOfJoining" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="Joining Year*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Joining Year*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvYearOfJoining" ValidationGroup="vgAlumni" ControlToValidate="txtYearOfJoining" runat="server" ForeColor="Red" ErrorMessage="Please Enter Joining Year">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtYearOfCompletion" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="Completion Year*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Completion Year*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvYearOfCompletion" ValidationGroup="vgAlumni" ControlToValidate="txtYearOfCompletion" runat="server" ForeColor="Red" ErrorMessage="Please Enter Completion Year">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCollege1" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="College*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'College*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCollege1" ValidationGroup="vgAlumni" ControlToValidate="txtCollege1" runat="server" ForeColor="Red" ErrorMessage="Please Enter College">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDegree1" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="Degree*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Degree*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvDegree1" ValidationGroup="vgAlumni" ControlToValidate="txtDegree1" runat="server" ForeColor="Red" ErrorMessage="Please Enter Degree">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtYearOfJoining1" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="Joining Year*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Joining Year*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvYearOfJoining1" ValidationGroup="vgAlumni" ControlToValidate="txtYearOfJoining1" runat="server" ForeColor="Red" ErrorMessage="Please Enter Joining Year">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtYearOfCompletion1" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="Completion Year*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Completion Year*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvYearOfCompletion1" ValidationGroup="vgAlumni" ControlToValidate="txtYearOfCompletion1" runat="server" ForeColor="Red" ErrorMessage="Please Enter Completion Year">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <%-- <tr>
                                        <td>
                                            <asp:TextBox ID="txtCollege2" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="College*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'College*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCollege2" ValidationGroup="vgAlumni" ControlToValidate="txtCollege2" runat="server" ForeColor="Red" ErrorMessage="Please Enter College">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDegree2" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="Degree*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Degree*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvDegree2" ValidationGroup="vgAlumni" ControlToValidate="txtDegree2" runat="server" ForeColor="Red" ErrorMessage="Please Enter Degree">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtYearOfJoining2" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="Joining Year*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Joining Year*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvYearOfJoining2" ValidationGroup="vgAlumni" ControlToValidate="txtYearOfJoining2" runat="server" ForeColor="Red" ErrorMessage="Please Enter Joining Year">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtYearOfCompletion2" Width="85%" CssClass="textboxcontact_list" runat="server" placeholder="Completion Year*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Completion Year*'"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvYearOfCompletion2" ValidationGroup="vgAlumni" ControlToValidate="txtYearOfCompletion2" runat="server" ForeColor="Red" ErrorMessage="Please Enter Completion Year">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>--%>
                                </table>

                            </td>
                        </tr>

                    </table>
                    <br />
                    <div id="divSubmitButton">
                    <div style="display: inline; padding-right: 35px">
                        <strong>Registration Fee for event: </strong>
                    </div>
                    <div style="display: inline">
                        <asp:Label ID="lblTotalAmountInINR" runat="server" Text=""></asp:Label>

                        <%-- <asp:HiddenField ID="hdfLifeFees" runat="server" Value ="Rs 20,000/-" />                        
                        <asp:HiddenField ID="hdfAnnualFees" runat="server" Value ="Rs 5,000/-" />
                        <asp:HiddenField ID="hdfNRIFees" runat="server" Value ="$500 U.S. or Equivalent in rupees" />--%>
                        <asp:HiddenField ID="hdfLifeFeesValue" runat="server" Value="20000" />
                        <asp:HiddenField ID="hdfAnnualFeesValue" runat="server" Value="5000" />
                        <asp:HiddenField ID="hdfNRIFeesValue" runat="server" Value="$500" />
                    </div>
                    <br />
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    <br />
                    <asp:Button ID="btnPayment" Text="Pay now" ValidationGroup="vgAlumni" CssClass="btn" runat="server" OnClick="btnPayment_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        #tblEducation, #tblEducation td, #tblEducation th {
            border: 1px solid #ddd;
        }

        #tblEducation {
            border-collapse: collapse;
            width: 100%;
        }

            #tblEducation td {
                text-align: left;
                padding-top: 10px;
            }

            #tblEducation th {
                text-align: center;
            }
    </style>
</asp:Content>
