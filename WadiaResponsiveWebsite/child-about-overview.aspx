﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-about-overview.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_overview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



		<div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>OVERVIEW</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 
Located in the heart of Mumbai, The Bai Jerbai Wadia Hospital for Children pledges to extend its services to people, regardless of their socio-economic status. Believing strongly in the fact that quality healthcare should not be restricted to only certain sections of society, the BJWHC offers state of the art services for neonatal and paediatric care at affordable costs.
  <br />
    <br />
A teaching hospital of world-wide repute, this 300 bed hospital is propelled by a highly accomplished team of over 60 paediatrics specialists devoting themselves to the care of over 100,000 children on an outpatient basis, and approximately 10,000 children as inpatients, annually. 
  <br />
    <br />
In brightly painted wards, surrounded with their favourite Disney characters, children are treated for a wide spectrum of rare and complex conditions, in an environment that does its best to keep their spirits high. With tender care by the staff and each doctor taking a personal interest in every child’s health and well being, the hospital sees its responsibility as much more than just medical treatment. Besides providing comprehensive clinical care it also offers rehabilitation and family focused methods of promoting a healthy environment for the child, thus striving to prevent childhood diseases. 
  <br />
    <br />
BJWHC also has the largest Neonatal Intensive Care Unit (NICU) in the West Zone. In its effort to provide universal healthcare, BJWHC also acts as a tertiary level referral center, is a nodal centre for Clubfoot in Maharashtra and is the only centre for Neonatal/Paediatric Dialysis in Western India. 


     <br />
    <br />

</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
									<li><a href="child-about-overview.aspx">Overview</a></li>
									<li><a href="child-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="child-about-chairman-msg.aspx">Chairman's message</a></li>
									<%--<li><a href="child-about-awards.aspx">Awards and Achievements</a></li>--%>
										
                                    <li><a href="child-about-history.aspx">History </a>

                                        <%--<div class="leftpadsubmenu" id="foo" style="display:none">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>


		</div>
	










</asp:Content>
