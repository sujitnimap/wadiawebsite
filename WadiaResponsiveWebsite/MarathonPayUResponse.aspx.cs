﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OperationStatus;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;
using System.Data;

namespace WadiaResponsiveWebsite
{
    public partial class MarathonPayUResponse : System.Web.UI.Page
    {
        public string mihPayId = "";
        string mode = "";
        public string status = "";
        public string unMappedStatus = "";
        public String key = "";
        public String txnid = "";
        public string amount = "";
        public float discount = 0;
        float netAmountDebit = 0;
        public string productinfo = "";
        public string firstname = "";
        public string lastname = "";


        public string phone = "";
        public string email = "";
        string address1 = "";
        string address2 = "";
        string country = "";
        string city = "";
        DateTime addedDate = DateTime.Now;

        string state = "";
        string zipCode = "";

        public string udf1 = "";
        public string udf2 = "";
        public string udf3 = "";
        public string udf4 = "";
        public string udf5 = "";
        public string udf6 = "";
        public string udf7 = "";
        public string udf8 = "";
        public string udf9 = "";
        public string udf10 = "";
        public string salt = "";

        string paymentSource = "";
        string pgType = "";
        string bankRefNum = "";
        string bankCode = "";
        string field9 = "";

        string error = "";
        string errorMessage = "";

        string transactionNumber = "";
        public string hashC = "";
        public string responseHash = "";
        Boolean IsHashValid = false;

        string seprator = "|";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                try
                {                 
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    IEnumerator en = Request.Form.GetEnumerator();
                    while ((en.MoveNext()))
                    {
                        string xkey = Convert.ToString(en.Current);

                        string xval = Request.Form[xkey];
                        dict.Add(xkey, xval);
                    }

                    //gvwAllResponseData.DataSource = dict;
                    //gvwAllResponseData.DataBind();

                    salt = ConfigurationManager.AppSettings["CSalt"];
                    //salt = "eCwWELxi"; //demo creds
                    status = dict["status"];
                    udf10 = dict["udf10"];
                    udf9 = dict["udf9"];
                    udf8 = dict["udf8"];
                    udf7 = dict["udf7"];
                    udf6 = dict["udf6"];
                    udf5 = dict["udf5"];
                    udf4 = dict["udf4"];
                    udf3 = dict["udf3"];
                    udf2 = dict["udf2"];
                    udf1 = dict["udf1"];
                    email = dict["email"];
                    phone = dict["phone"];
                    firstname = dict["firstname"];
                    productinfo = dict["productinfo"];
                    amount = dict["amount"];
                    txnid = dict["txnid"];
                    key = dict["key"];
                    transactionNumber = dict["txnid"];
                    mihPayId = dict["mihpayid"];

                    country = dict["country"];
                    city = dict["city"];
                    errorMessage = dict["error_Message"];
                    error = dict["error"];
                    mode = dict["mode"];

                    netAmountDebit = float.Parse(dict["net_amount_debit"]);
                    address1 = dict["address1"];
                    paymentSource = dict["payment_source"];
                    bankRefNum = dict["bank_ref_num"];
                    bankCode = dict["bankcode"];
                    pgType = dict["PG_TYPE"];

                    responseHash = dict["hash"];

                    String hashStr = salt + seprator + status + seprator + udf10 + seprator + udf9 + seprator + udf8 + seprator + udf7 + seprator + udf6 + seprator + udf5 + seprator + udf4 + seprator + udf3 + seprator + udf2 + seprator + udf1 + seprator + email + seprator + firstname + seprator + productinfo + seprator + amount + seprator + txnid + seprator + key;
                    hashC = CreateBase64SHA512Hash(hashStr, "ISO-8859-2").ToLower();

                    if (responseHash == hashC)
                    {
                        IsHashValid = true;
                    }
                    else
                    {
                        IsHashValid = false;
                    }

                   
                    paymentDetail objpaymentDetail = new paymentDetail();
                    COperationStatus os = new COperationStatus();
                    objpaymentDetail.ChildDonateOnlineID = Convert.ToInt32(udf1);
                    objpaymentDetail.AddedDate = addedDate;
                    objpaymentDetail.ProdInfo = productinfo;
                    objpaymentDetail.MerchantKey = key;
                    objpaymentDetail.TransationID = txnid;
                    objpaymentDetail.MihPayId = mihPayId;
                    objpaymentDetail.IsHashValid = IsHashValid;

                    objpaymentDetail.FirstName = firstname;
                    objpaymentDetail.LastName = lastname;
                    objpaymentDetail.Amount = Convert.ToDouble(amount);
                    objpaymentDetail.Email = email;
                    objpaymentDetail.Phone = phone;
                    objpaymentDetail.hash = responseHash;
                    objpaymentDetail.Status = status;

                    objpaymentDetail.NetAmountDebit = netAmountDebit;
                    objpaymentDetail.Address1 = address1;
                    objpaymentDetail.PaymentSource = paymentSource;
                    objpaymentDetail.BankRefNum = bankRefNum;
                    objpaymentDetail.BankCode = bankCode;
                    objpaymentDetail.PGType = pgType;
                    objpaymentDetail.Mode = mode;
                    objpaymentDetail.UnMappedStatus = unMappedStatus;


                    objpaymentDetail.Country = "";
                    objpaymentDetail.City = city;
                    objpaymentDetail.State = state;
                    objpaymentDetail.ZipCode = zipCode;
                    objpaymentDetail.Address2 = address2;
                    objpaymentDetail.Error = error;
                    objpaymentDetail.ErrorMessage = errorMessage;

                    objpaymentDetail.UDF1 = udf1;
                    objpaymentDetail.UDF2 = udf2;
                    objpaymentDetail.UDF3 = udf3;
                    objpaymentDetail.UDF4 = udf4;
                    objpaymentDetail.UDF5 = udf5;
                    objpaymentDetail.UDF6 = udf6;
                    objpaymentDetail.UDF7 = udf7;
                    objpaymentDetail.UDF8 = udf8;
                    objpaymentDetail.UDF9 = udf9;
                    objpaymentDetail.UDF10 = udf10;

                    objpaymentDetail.Field1 = "";
                    objpaymentDetail.Field2 = "";
                    objpaymentDetail.Field3 = "";
                    objpaymentDetail.Field4 = "";
                    objpaymentDetail.Field5 = "";
                    objpaymentDetail.Field6 = "";
                    objpaymentDetail.Field7 = "";
                    objpaymentDetail.Field8 = "";
                    objpaymentDetail.Field9 = field9;

                    os = CPaymentDetail.Instance().InsertPaymentDetail(objpaymentDetail);
                    lblAddedSuccessfully.Text = os.Success.ToString();
                     

                    if (IsHashValid && status == "success")
                    {
                        lblPaymentSuccessMessage.Text = "Transaction Successfull (Note : Copy of invoice to be printed)";

                        lblTransactionNumber.Text = transactionNumber;

                        IList<registrationForMarathon> registForMarathonData = CRegistrationForMarathon.Instance().GetRegistrationForMarathonInfo(Convert.ToInt32(udf1));
                        DataTable dt = new DataTable();
                        dt = CollectionHelper.GetDataTable(registForMarathonData);


                        IList<registeredChildForMarathon> registChildForMarathon = CRegisteredChildForMarathon.Instance().GetRegisteredChildForMarathon(Convert.ToInt32(udf1));
                        DataTable dtChild = new DataTable();
                        dtChild = CollectionHelper.GetDataTable(registChildForMarathon);


                        IList<registeredAdultForMarathon> registAdultForMarathon = CRegisteredAdultForMarathon.Instance().GetRegisteredAdultForMarathon(Convert.ToInt32(udf1));
                        DataTable dtAdult = new DataTable();
                        dtAdult = CollectionHelper.GetDataTable(registAdultForMarathon);

                        hdnPayID.Value = udf1;

                        //                        #region BodyHeader

                        //                        //string body = "Hellow";

                        //                        string body = @"<table width='800px' border='1' align='center' cellpadding='0' cellspacing='0'>
                        //												<tr>
                        //														<td valign='top' class='contentbig' style='padding: 20px'>
                        //																<table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        //																		<tr>
                        //																				<td width='22%' valign='top'>
                        //																						<img src='http://wadiahospitals.org/images/Logo.png' width='100' height='100' />
                        //																				</td>
                        //																				<td width='50%' valign='top' style='padding-left: 15px'>
                        //																						<table width='99%' border='0' cellspacing='0' cellpadding='0'>
                        //																								<tr>
                        //																										<td height='39'>
                        //																												<h2 class='contentheader'>
                        //																														Wadia Hospitals</h2>
                        //																										</td>
                        //																								</tr>
                        //																								<tr>
                        //																										<td height='78' class='content' valign='top'>
                        //																										Bai Jerbai Wadia Hospital for Children, 
                        //																										<br />
                        //																										Acharya Donde Marg, Lower Parel,
                        //																										<br />
                        //																										Mumbai, Maharashtra,India, 400 012                                                     
                        //																										</td>
                        //																								</tr>
                        //																						</table>
                        //																				</td>
                        //																				<td width='35%' style='padding-left: 15px' valign='top'>
                        //																						<table width='100%' border='0' cellspacing='0' cellpadding='0'>                                                
                        //																								<tr>
                        //																										<td height='27' class='content'>
                        //																												Date
                        //																										</td>
                        //																										<td>
                        //																												<strong>: </strong>
                        //																										</td>
                        //																										<td>"
                        //                                                                                                             + DateTime.Now +
                        //                                                                                                @"</td>
                        //																								</tr>
                        //																						</table>
                        //																				</td>
                        //																		</tr>
                        //																</table>

                        //<table>
                        //<tr>
                        //<td>
                        //<br />
                        //Registration No. : <strong> " + udf1.ToString() + @"</strong>
                        //<br />
                        //Online Registration e-Receipt <strong> " + transactionNumber + @"</strong> </br>

                        //Received with thanks an amount of Rs." + dt.Rows[0]["TotalAmount"].ToString() + @" towards Participation/Donation/Sponsor a Run, for Little </br>

                        //Hearts Marathon 2017 scheduled on 12th February 2017.

                        //</td>
                        //</tr>
                        //</table>
                        //			 <br />                 
                        //<p>
                        //																		<strong>USER INFORMATION : </strong>
                        //																		<br />
                        //																</p>
                        //																<table width='100%' border='1' cellspacing='0' cellpadding='0'>
                        //																		<tr>
                        //																				<td height='24' colspan='2' class='contenthead' style='background-color: #CCC; padding-left: 5px;'>
                        //																						Billing Address
                        //																				</td>
                        //																		</tr>
                        //																		<tr>
                        //																				<td class='content' height='30' style='padding-left: 5px'>
                        //																						EmailAddress<strong> :</strong>
                        //																				</td>
                        //																				<td width='70%'>"
                        //                                                                                        + dt.Rows[0]["EmailAddress"].ToString() +
                        //                                                                             @"</td>
                        //																		</tr>
                        //																		<tr>
                        //																				<td class='content' height='30' style='padding-left: 5px'>
                        //																						Address <strong>:</strong>
                        //																				</td>
                        //																				<td>"
                        //                                                                                        + dt.Rows[0]["Address"].ToString() +
                        //                                                                            @"</td>
                        //																		</tr>
                        //																		<tr>
                        //																				<td class='content' height='30' style='padding-left: 5px'>
                        //																						Mobile Number <strong>:</strong>
                        //																				</td>
                        //																				<td>"
                        //                                                                                        + dt.Rows[0]["MobileNumber1"].ToString() +
                        //                                                                            @"</td>
                        //																		</tr> 
                        //																		<tr>
                        //																				<td class='content' height='30' style='padding-left: 5px'>
                        //																						Number Of run sponsored <strong>:</strong>
                        //																				</td>
                        //																				<td>"
                        //                                                                                        + dt.Rows[0]["NoOfSponsorARun"].ToString() +
                        //                                                                            @"</td>
                        //																		</tr>
                        //																		<tr>
                        //																				<td class='content' height='30' style='padding-left: 5px'>
                        //																						Total Amount <strong>:</strong>
                        //																				</td>
                        //																				<td>"
                        //                                                                                        + dt.Rows[0]["TotalAmount"].ToString() +
                        //                                                                            @"</td>
                        //																		</tr>
                        //																</table>
                        //																<br />
                        //																<strong>PARTICIPANTS INFORMATION</strong><br />
                        //																<br />
                        //																<table width='100%' border='1' cellspacing='0' cellpadding='0'>
                        //																		<tr>                                        
                        //																				<td width='15%' class='contenthead' align='center'>
                        //																						Name
                        //																				</td>
                        //																				<td width='15%' class='contenthead' align='center'>
                        //																						Age
                        //																				</td>
                        //																				<td width='15%' class='contenthead' align='center'>
                        //																					 Gender
                        //																				</td>
                        //																				<td width='15%' class='contenthead' align='center'>
                        //																					 T-Shirt Size
                        //																				</td> 
                        //																				<td width='30%' class='contenthead' align='center'>
                        //																					School, College or Organization
                        //																				</td>                                                                             
                        //																		</tr>";

                        //                        for (int i = 0; dtChild.Rows.Count > i; i++)
                        //                        {
                        //                            body += @"<tr style='border: none'>                                        
                        //																				<td align='center'>"
                        //                                                    + dtChild.Rows[i]["ChildName"].ToString()
                        //                                                    + @"</td>
                        //																				<td align='center'>"
                        //                                                    + dtChild.Rows[i]["Age"].ToString()
                        //                                                 +
                        //                                                 @"</td>
                        //																				<td align='center'>"
                        //                                                    + dtChild.Rows[i]["Gender"].ToString()
                        //                                                    +
                        //                                                    @"</td>
                        //																<td align='center'>"
                        //                                                    + dtChild.Rows[i]["TShirtSize"].ToString()
                        //                                                 +
                        //                                                 @"</td>
                        //															 <td align='center'>"
                        //                                                    + dtChild.Rows[i]["School"].ToString()
                        //                                                 +
                        //                                                 @"</td>
                        //														</tr>";
                        //                        }

                        //                        for (int i = 0; dtAdult.Rows.Count > i; i++)
                        //                        {
                        //                            body += @"<tr style='border: none'>                                        
                        //																				<td align='center'>"
                        //                                                    + dtAdult.Rows[i]["AdultName"].ToString()
                        //                                                    + @"</td>
                        //																				<td align='center'>"
                        //                                                    + dtAdult.Rows[i]["Age"].ToString()
                        //                                                 +
                        //                                                 @"</td>
                        //																				<td align='center'>"
                        //                                                    + dtAdult.Rows[i]["Gender"].ToString()
                        //                                                    +
                        //                                                    @"</td>
                        //																<td align='center'>"
                        //                                                    + dtAdult.Rows[i]["TShirtSize"].ToString()
                        //                                                 +
                        //                                                 @"</td>
                        //																	 <td align='center'>"
                        //                                            + dtAdult.Rows[i]["Organization"].ToString()
                        //                                         +
                        //                                         @"</td>
                        //											 </tr>";
                        //                        }

                        //                        body += @"<tr>
                        // </tr>
                        //												</table>
                        //													<br />       
                        //<p>
                        //																		<strong> DETAILS OF EVENT : </strong>
                        //																		<br />
                        //																</p>

                        //<table>
                        //<tr>
                        //<td>
                        //<strong>Date </strong> : 12th February 2017
                        //</td>
                        //</tr>

                        //<tr>
                        //<td>
                        //<strong>Time</strong>: 9 AM
                        //</td>
                        //</tr>

                        //<tr>
                        //<td>
                        //<strong>Venue</strong>: Royal Western India Turf Club Limited, Mahalaxmi Race Course,
                        //</br></br>
                        //Mumbai, Maharashtra 400034
                        //</br></br>
                        //</td>
                        //</tr>
                        //</table>

                        //</br></br>
                        //<table>  
                        //<tr>
                        //<td>  
                        //<strong> Terms, conditions and guidelines : </strong>
                        //</br>
                        //</td>
                        //</tr>

                        //<tr>
                        //<td>
                        //• Please note Last Date of registration 9th Feb 2017 till midnight subject to availability. The organiser 
                        //</br>
                        //reserves the right to change the dates.

                        //</br>
                        //• <strong> Fitness: </strong>
                        //<p>
                        // &nbsp;&nbsp;&nbsp; • In case of any illness or disability please mail Fitness Certificate from your treating certified Doctor 
                        //to <a href='#'><u>marathon@wadiahospitals.org</u></a>
                        //</br>
                        // &nbsp;&nbsp;&nbsp; • Participants must be fit to run and free from injury
                        //</p>
                        //</br>
                        //• <strong> Participation: </strong>
                        //<p>
                        // &nbsp;&nbsp;&nbsp; • The child's age should be from 4 to 18 years (For age group 4 to 10 it is mandatory to run with 
                        //parent/guardian who will take the responsibility of the child throughout the Marathon to avoid any 
                        //incident.)
                        //</br>
                        // &nbsp;&nbsp;&nbsp; • For all participants need to confirm their names
                        //<br>
                        //The Little Hearts Marathon is an event to spread awareness on Cardiac diseases: Prevention of cardiac diseases
                        //in children and also a fund raising event for the mentioned cause.
                        //</p>
                        //<br>

                        //• Marathon is in Royal Western India Turf Club Limited, Mahalaxmi Race Course, Mumbai, Maharashtra 400034. Timed Certificate, Medals and Goodie bags would be awarded to the participants.
                        //</br>
                        //• All registration fees are non-refundable. In the event you cannot participate, please consider your registration
                        //fee as generous donation towards “Little Hearts Marathon” alternatively you may choose to sponsor a run by 
                        //writing a mail to us before 2nd February 2017 on <u>marathon@wadiahospitals.org</u>
                        //</br>
                        //• It is mandatory for all confirmed participants to come to the Nowrosjee Wadia Maternity on 10th and 11th
                        //February 2017 between 10 am to 4 pm to collect T-shirts along with valid photo ID and copy of invoice.
                        //</br>
                        //• Information against all mandatory fields prefixed with ‘*’ on the entry form must be filled. Incomplete forms
                        //will be rejected.
                        //</br>
                        //• This Entry Form and the right to participate in the event and the rights and benefits available to the applicant
                        //under this application form is at the sole discretion of Little Hearts Marathon and cannot be transferred to any 
                        //other person under any circumstances, and the applicant alone shall be entitled to the rights and benefits 
                        //arising hereunder.
                        //</br>
                        //• Please go through all information mentioned in this website prior to submitting your application. Confirmation
                        //of application received will be subject to the applicable qualification criteria and entry rules & Guidelines given 
                        //herein.
                        //</br>
                        //• You can collect Goody bags with valid coupons only; participants without coupons will not be entertained.
                        //</br>

                        //<strong>I declare, confirm and agree as follows and I/my word </strong>
                        //</br>
                        //(i) I agree to the entry terms and guidelines. 
                        //</br>
                        //(ii) The information given by me in this entry form is true and I am solely responsible for the accuracy of this information; 
                        //</br>
                        //(iii) Have fully understood the risk and responsibility of participating in the event and will be participating entirely at my own risk and responsibility; 
                        //</br>
                        //(iv) The security of my child is solely the responsibility of the accompanying parent/ guardian. The Organisers shall not be held responsible for any security related incident that may occur during the course of the marathon. 
                        //</br>
                        //(v) Any illness/ medical condition/ ailment have to be reported to the Organisers in detail. If the parents fail to intimate the exact medical condition of the child, orself; then the Organisers shall not be held responsible for any mishap that may occur or arise due to the such illness/ medical condition/ ailment. 
                        //</br>
                        //(vi) Understand the risk of participating on a course with vehicular traffic, even if the course may be regulated / policed; 
                        //</br>
                        //(vii) Understand that I must be of, and must train to, an appropriate level of fitness to participate in such a physically demanding event and I have obtained a medical clearance from a registered medical practitioner, allowing me to participate in the event/s; 
                        //</br>
                        //(viii) The parents/ guardians are required to strictly follow the instructions given by the Organisers and incase of the difficulty forthwith report to the Organisers. 
                        //</br>
                        //(ix) For myself/ourselves and our legal representatives, waive all claims of whatsoever nature against any and all Sponsors of the event, all political entities, authorities and officials, all contractors and construction firms working on or near the course, all Committee persons, officials and volunteers, Little Hearts Marathon, Wadia Hospitals, and all other persons and entities associated with the event and the directors, employees, agents and representatives of all or any of the aforementioned including, but not limited to, any claims that might result from me participating in the event and whether on account of illness, injury, death or otherwise; 
                        //</br>
                        //(x) Agree that if I am injured or taken ill or otherwise suffer any detriment whatsoever, I hereby irrevocably authorize the event officials and organizers to, at my own risk and cost, transport me to a medical facility and/or to administer emergency medical treatment and I waive all claims that might result from such transport and/or treatment or delay or deficiency therein. I shall pay or reimburse to you my medical and emergency expenses and I hereby authorize/s you to incur the same; 
                        //</br>
                        //(xi) Shall provide to race officials such medical data relating to me as they may request. I agree that nothing herein shall oblige the event officials or organizers or any other person to incur any expense or to provide any transport or treatment; 
                        //</br>
                        //(xii) In case of any illness or injury caused to me or death suffered by me or my due to any force majeure event including but not limited to fire, riots or other civil disturbances, earthquakes, storms, typhoons or any terrorist act, none of the sponsors of the event or any political entity or authorities and officials or any contractor or construction firms working on or near the course, or any of the Man and Mountain Meet persons, officials or volunteers, Wadia Hospitals, Little Hearts Marathon or any persons or entities associated with the event or the directors, employees, agents or representatives of all or any of the aforementioned shall be held liable by me or my representatives; 
                        //</br>
                        //(xiii) Understand, agree and irrevocably permit Wadia Hospitals and Little Hearts Marathon to share the information given by me in this application, with all/any entities associated with this event, at its own discretion
                        //</br>
                        //(xiv) Understand, agree and irrevocably permit Wadia Hospitals and Little Hearts Marathon to my photograph and record (on film, tape, hard drive, or any other method) my performance, appearance, name and/or voice and the results in perpetuity for the purpose of promoting the Event, at its own discretion.
                        //</br>
                        //(xv) Further authorize Wadia Hospitals and Little Hearts Marathon to edit the same at discretion and to include it with performance of other and with sound effects, special effects, music etc. in any manner or media whatsoever, including without limitation, unrestricted use for purpose of publicity, advertising and sales promotion; and to use my name. Wadia Hospitals and Little Hearts Marathon owns all rights to the results and proceeds of my services in connection herewith; 
                        //</br>
                        //(xvi) I understand that it is at the discretion of Wadia Hospitals and Little Hearts Marathon to make any changes in the terms conditions and guidelines for the event.
                        //</br>

                        //</td>
                        //<tr>
                        //												</table>";

                        //                        #endregion

                        #region BodyHeader
                        string body = @"<table width='800px' border='1' align='center' cellpadding='0' cellspacing='0'>
           <tr>
							<td valign='top' class='contentbig' style='padding: 20px'>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
							<td width='22%' valign='top'>
							<img src='http://wadiahospitals.org/images/Logo.png' width='100' height='100' />
							</td>
							<td width='50%' valign='top' style='padding-left: 15px'>
							<table width='99%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
							<td height='39'>
							<h2 class='contentheader'>
							Wadia Hospitals</h2>
							</td>
							</tr>
							<tr>
							<td height='78' class='content' valign='top'>
							Bai Jerbai Wadia Hospital for Children,
							<br />
							Acharya Donde Marg, Lower Parel,
							<br />
							Mumbai, Maharashtra,India, 400 012
							</td>
							</tr>
							</table>
							</td>
							<td width='35%' style='padding-left: 15px' valign='top'>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
							<td height='27' class='content'>
							Date
							</td>
							<td>
							<strong>: </strong>
							</td>
							<td>" + DateTime.Now + @"</td>
							</tr>
							</table>
							</td>
							</tr>
							</table>

<table>
<tr>
<td>
<br />
Registration No. : <strong> " + dt.Rows[0]["RegistrationForMarathonID"].ToString() + @"</strong>
<br />
Online Registration e-Receipt <strong> " + transactionNumber + @"</strong> </br> 

Received with thanks an amount of Rs." + dt.Rows[0]["TotalAmount"].ToString() + @" towards Participation/Donation/Sponsor a Run, for Little </br>

Little Hearts Marathon 2019 scheduled on 27th January 2019.

</td>
</tr>
</table>
<br />
<p><strong>USER  INFORMATION : </strong><br /></p>
<table width='100%' border='1' cellspacing='0' cellpadding='0'>
<tr>
<td height='24' colspan='2' class='contenthead' style='background-color: #CCC; padding-left: 5px;'>
Billing Address
</td>
</tr>
<tr>
<td class='content' height='30' style='padding-left: 5px'>
Name of Participant<strong> :</strong>
</td>
<td style='padding-left:10px;font-weight:bold;' width='70%'>" + dtChild.Rows[0]["ChildFirstName"].ToString() + " "+dtChild.Rows[0]["ChildMiddleName"].ToString() + " "+dtChild.Rows[0]["ChildLastName"].ToString() + @"</td>
</tr>
<tr>
<td class='content' height='30' style='padding-left: 5px'>
Race Category<strong> :</strong>
</td>
<td style='padding-left:10px;font-weight:bold;' width='70%'>" + dt.Rows[0]["Category"].ToString()+ @"</td>
</tr>
<tr>
<td class='content' height='30' style='padding-left: 5px'>
Email Address<strong> :</strong>
</td>
<td style='padding-left:10px' width='70%'>" + dt.Rows[0]["EmailAddress"].ToString() + @"</td>
</tr>
<tr>
<td class='content' height='30' style='padding-left: 5px'>
Address <strong>:</strong>
</td><td style='padding-left:10px'>" + dt.Rows[0]["Address"].ToString() + @"</td>
</tr>
<tr>
<td class='content' height='30' style='padding-left: 5px'>
Total Amount <strong>:</strong>
</td>
<td style='padding-left:10px'>"
+ dt.Rows[0]["TotalAmount"].ToString() +
@"</td>
</tr>
<tr>
<td class='content' height='30' style='padding-left: 5px'>
Mobile Number <strong>:</strong>
</td><td style='padding-left:10px'>" + dt.Rows[0]["MobileNumber1"].ToString() + @"</td>
</tr>

</table>

<br>

<strong> DETAILS OF EVENT : </strong>
<br>

<table>
<tr>
</br>

<strong>Date </strong> : 27 th January, 2019.
</br>
<strong>Reporting Time:</strong>: 7.00 am.
</br>
<strong>Race Time:</strong>: 7:30 am.
</br>
<strong>Starting venue</strong>:  M.M.R.D.A ground, BKC, Mumbai – 400051.

</br>
 <br />
<strong> Terms, conditions and guidelines : </strong>
</br>
</br>
 Please note Last date of registration is 24 th January 2019 till midnight subject to availability. The
organizer reserves the right to change the dates.
</td>
 </tr>
<td>
</br>
 <strong> Fitness: </strong>
</br>
</br>
 Any illness / medical condition / ailment has to be reported to the organizers in detail. If the parent fail to
intimate the exact medical condition of the child or self; then the organizers shall not be held responsible
for any mishap that may occur or arise due to such illness / medical condition / ailment.
</br>
</br>
If any medical help is required before the marathon for the child, you can visit our Pediatric consultant at Bai Jerbai Wadia Hospital for Children from 10 am to 2 pm (Monday to Friday).
</br>
</br>
 <strong> Participation: </strong>
</br>
</br>
 The child’s age should be from 7 to 18 years.
</br>
</br>
 Marathon is at M.M.R.D.A ground, BKC, Mumbai – 400051 on 27th January, 2019
<br>
</br>
 Schools / organizations / societies can do bulk registrations and an authorized representative with valid receipt can collect the T-shirt & Bib on their behalf.
<br>
</br>
 Organizer will take all precautions but shall not be held liable for any unforeseen event / incident.
</br>
</br>
 T-shirt once handed over will not be exchanged.
</br>
</br>
 All registration fees are non-refundable.
</br>
</br>
 You can collect Goodie Bags with valid coupons only, after completion of the event. Participants without coupon will not be entertained.
</br>
</br>
 You can download the Timed certificate within 48 hours after completion of marathon.
</br>
</br>
 The security of the participating child is solely the responsibility of the accompanying parent / guardian. The organizer shall not be held responsible for any security related incident that may occur during the course of the marathon.
</br>
</br>
 The parent / guardian is required to strictly follow the instructions given by the organizers and incase of the difficulty forthwith report to the organizers.
</br>
</br>
Please choose the event category carefully, confirmed registrations are non-refundable, non-transferable and cannot be modified. Provide us with a secure email ID/mobile number that you can access regularly, since this will be our primary means of communication during the run up to the event.

</br>
</br> Users of email services that offer filtering/blocking of messages from unknown email address should add this email id marathon@wadiahospitals.org to their address list.

</br>
</br> We will be sending regular updates to the Mobile number you have provided in the registration form this should be not treated as spam and you shall not take any action against our bulk sms service provider.

</br></br> Any notice sent to the email address registered with the organizers shall be deemed as received by the runners.

</br></br> Please fill out only those fields that are necessary for mailing purposes. Do not provide redundant data in multiple fields. (i.e., do not list the same data for city, province and country), as this will only complicate our ability to contact you, if necessary.

</br></br> You are aware that running / long distance running is an extreme sport and can be injurious to body and health. You take full responsibility for participating in the Little Hearts Marathon 2019 event and do not hold the organizing committee or any of its members or entities responsible for any injury or accident.

</br></br> You shall consult your physician and undergo complete medical examination to assess your suitability to participate in the event.

</br></br> You also assume all risks associated with participating in this event including, but not limited to, falls, contact with other participants, the effects of the weather, including high heat or humidity, traffic and the condition of the road, arson or terrorist threats and all other risks associated with a public event.

</br></br> You agree that Little Hearts Marathon 2019 shall not be liable for any loss, damage, illness or injury that might occur as a result of your participation in the event.

</br></br> You agree to abide by the instructions provided by the organizers from time to time in the best interest of your health and event safety.

</br></br> You also agree to stop running if instructed by the Race Director or the Medical Staff or by the Aid Station Volunteers.

</br></br> You confirm that your name and media recordings taken during your participation may be used to publicize the event.

</br></br> You may acknowledge and agree that your personal information can be stored and used by Little Hearts Marathon 2019 or any other company in connection with the organization, promotion and administration of the event and for the compilation of statistical information.
</br></br> You confirm that, in the event of adverse weather conditions, major incidents or threats on the day, any of the force majeure or restriction by authority, the organizers reserve the right to stop/cancel/postpone the event. You understand that confirmed registrations and merchandise orders are non-refundable, non-transferable and cannot be modified. The organizers reserve the right to reject any application without providing reasons. Any amount collected from rejected applications alone will be refunded in full (excluding bank charges wherever applicable)

</br></br> For any reason you cannot turn up on race day, no refund of any form will be given.

</br></br> Wadia Hospitals is only a service provider and is not in any way responsible for the delivery of the event.

</br></br> If this registration is being made on behalf of a minor, I confirm that I am the parent / guardian of the child and that he/She has my permission to take part in the event. I further concur that all the above rules shall apply to him/her as if he were a major.

</br></br> Participants must personally appear for collecting the running Bibs. Bibs can be distributed in-absentee with written consent of the registered runners.

</br></br> Runners will not be allowed to stay on the course beyond the cut-off time considering the safety and health issues. We request total co-operation from runners in this regards.

</br></br>  <input type='checkbox' name='terms' value='terms'>I agree to the terms and conditions.
<p>&nbsp; 
<p>&nbsp;
<p>&nbsp;
<hr width = '20%' align = 'left' />
</ br ></ br >
   Signature & Date
   </td>

</tr>
												</table>";
                        #endregion
                        CollectionHelper.MarathonsendMail(email, "Marathon Registration", "Dear Sir/Madam ,", body);

                        CollectionHelper.MarathonsendMail("lhm2015registrations@wadiahospitals.org", "Marathon Registration", "Dear Sir/Madam ,", body);

                        dataDiv.InnerHtml = body;

                    }
                    else
                    {
                        lblPaymentSuccessMessage.Text = "Fail";
                        lblTransactionNumber.Text = transactionNumber;
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.StackTrace + "<br/> "+ ex.Message);
                }

            }
        }

        public static string CreateBase64SHA512Hash(string hashTarget, string encoding)
        {
            System.Security.Cryptography.SHA512 sha = new System.Security.Cryptography.SHA512CryptoServiceProvider();
            byte[] targetBytes = System.Text.Encoding.GetEncoding(encoding).GetBytes(hashTarget);
            byte[] hashBytes = sha.ComputeHash(targetBytes);

            StringBuilder sb = new StringBuilder(hashBytes.Length * 2);
            foreach (byte b in hashBytes)
            {
                sb.AppendFormat("{0:x2}", b);
            }
            return sb.ToString();
        }

        //protected void btnViewInvoice_Click(object sender, EventArgs e)
        //{
        //	string url = string.Format("ViewMarathonInvoice.aspx?BID={0}", Convert.ToInt32(udf1));
        //	string s = "window.open('" + url + "', 'popup_window', 'width=300,height=100,left=100,top=100,resizable=yes');";
        //	//string s = "window.open('" + string.Format("ViewMarathonInvoice.aspx?BID={0}", Convert.ToInt32(udf1)) + "', 'popup_window', 'width=300,height=100,left=100,top=100,resizable=yes');";

        //	ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

        //}
    }
}