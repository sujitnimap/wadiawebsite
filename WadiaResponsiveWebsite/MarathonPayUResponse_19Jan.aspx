﻿<%@ Page Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="MarathonPayUResponse.aspx.cs" Inherits="WadiaResponsiveWebsite.MarathonPayUResponse" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function printDiv(divID) {

            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head><title></title></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">



                <br />

                <!-- EVENT CONTENT -->


                <div style="text-align: center">
                    <%--  Message :--%>
                    <asp:Label ID="lblAddedSuccessfully" Visible="false" runat="server" Font-Size="20px" Font-Bold="true" ForeColor="#000066"></asp:Label>
                    <br />
                    <%--<strong>Transaction Number :</strong>--%>
                    <asp:Label ID="lblTransactionNumber" Text="" runat="server" Visible="false" ForeColor="#000066"></asp:Label>
                    <%--<strong>Added Successfully :</strong>--%>
                    <asp:Label ID="lblPaymentSuccessMessage" runat="server" ForeColor="#000066"></asp:Label>
                    <br />
                </div>

                <%--<asp:Button ID="btnprint" runat="server" Text="Print"  Font-Bold="true"  Width="70px" Height="30px" OnClientClick="printDiv('dataDiv')" />--%>
							
							<input id="btnprint" onclick='window.print(); return false;' type="button" value="Print" Font-Bold="true" />

                <div runat="server" id="dataDiv">
                </div>

                <!-- EVENT CONTENT -->

            </div>
        </div>


    </div>


</asp:Content>
