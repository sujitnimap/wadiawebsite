﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="about-us-doctors-women.aspx.cs" Inherits="WadiaResponsiveWebsite.about_us_doctors_women" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




	<div class="row block05">
			
			<div class="row block05">
		<div class="col-full">
					
                    <%--<div class="title"><span>List Of Doctors and Departments</span></div>--%>
                    
 <!-- Expand collapse Script -->


<!--<h2>Example 1:</h2>
<a href="#" onClick="ddaccordion.expandone('mypets', 0); return false">Expand 1st header</a> | <a href="#" onClick="ddaccordion.collapseone('mypets', 0); return false">Collapse 1st header</a> | <a href="#" onClick="ddaccordion.toggleone('mypets', 1); return false">Toggle 2nd header</a> -->

<div style="padding:0px 20px 20px 20px; width:auto">
    <%--FIRST DEPT START --%>          
    <div class="colorcontent">John Maxwell once said, "People don’t care how much you know until they know how much you care." This is a notion that we base all our work on. Our 'patient-first' philosophy stems from the understanding that providing comfort and care for each patient is as important as our highly specialized treatments. 
        <br />
Here at the Nowrosjee Wadia Maternity Hospital, we have a dedicated team of over 20 gynaecology and obstetrics specialists, aided by highly trained nurses to provide top quality, holistic treatment to women, with the care and attention that they deserve. 

           </div>
                    <br />



<h3 class="mypets">Obstetrics and Gynaecology</h3>

<div class="docmain">
<div class="thepet">



    <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/DrJassawalla-thumb.jpg" />

</div>



<div class="hor2">

<strong>Dr. Jassawalla M.Jamshed

</strong><br />
Medical Director, NWMH
  <br />
MBBS, MD, FCPS, DGO, DFP, FICOG, FIAJAGO, FICMCH, FRSH<%--, FICA--%>

</div>



</div>

     <div class="experience" >


Professional experience : Over 25 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drjassawalla.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div1" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Pravin Mhatre

</strong><br />
Hon. Professor
  <br />
MD, FCPS, DGO, DFP


</div>



</div>

     <div class="experience" >


<div class="devider_20px"></div>
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drpravin.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div2" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px">




</div>






<%--2ND HORIZONTAL--%>

 <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr N M Narvekar.jpg" />

</div>



<div class="hor2">

<strong>Dr. N.M.Narvekar

</strong><br />
XXXXXX
      <br />
XXXXXXXXXXXX

</div>



</div>

     <div class="experience" >


Professional experience : Over XXXXXXXXXXX years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drnarvekar.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div9" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. K.R.Damania

</strong><br />
XXXXX
  <br />
XXXXXXXXXx

</div>



</div>

      <div class="experience" >


Professional experience : Over XXXXXXXXXXX years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drdamania.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div10" style="display:none;">
      


</div>


</div>

</div>








</div>

<%--END 2ND HORIZONTAL--%>

    <div class="devider_20px"></div>



<%--3RD HORIZONTAL--%>




<%--2ND HORIZONTAL--%>

 <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Purnima photo.jpg" />

</div>



<div class="hor2">

<strong>Dr. Purnima R.Satoskar

</strong><br />
Professor
      <br />
MD, DNB, MRCOG
</div>



</div>

     <div class="experience" >

Professional experience : 26 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drpurnima.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div11" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. G.D.Balsarkar

</strong><br />
XXX
    
      <br />
XXXXXXXXXXXXXXXXXXX

</div>



</div>

     <div class="experience" >

Professional experience : XXXXXXX years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drbalsarkar.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div12" style="display:none;">
      


</div>


</div>

</div>








</div>

    <div class="devider_20px"></div>




<%--3RD HORIZONTAL END--%>



    <%--4TH HORI--%>




<%--2ND HORIZONTAL--%>

 <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Sarita Bhalerao.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sarita Bhalerao

</strong><br />
Hon. Clinical Assistant  <br />
MD, DGO, FCPS, DNB, DFP, FRCOG, Diploma in Pelvic Endoscopy

</div>



</div>

     <div class="experience" >


Professional experience : Over 25 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drsarita.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div13" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Madhuri Patel

</strong><br />
XXXXX
  <br />
XXXXXXXXXXXXX


</div>



</div>

     <div class="experience" >


<div class="devider_20px"></div>
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drmadhuri.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div14" style="display:none;">
      


</div>


</div>

</div>








</div>



    <div class="devider_20px"></div>


  <%--  END 4TH HORI--%>






<%--5TH HORZ--%>


 <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sujata Dalvi

</strong><br />
XXXXX
  <br />
XXXXXXXXXXXXXX

</div>



</div>

     <div class="experience" >


Professional experience : Over XXXXXXXXXxx years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drsujata.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div15" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. P.D.Tank

</strong><br />
XXXXXXXXXX
  <br />
XXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional experience : Over XXXXXXXXXxx years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drtank.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div16" style="display:none;">
      


</div>


</div>

</div>








</div>

<%--END 2ND HORIZONTAL--%>

    <div class="devider_20px"></div>



<%--END 5TH HORZ--%>





<%--6TH HORZ--%>



 <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Swati Alhabadia

</strong><br />
XXXXXXXXXXx
      <br />
XXXXXXXXXXXXXXXXXXXXXXx
</div>



</div>

     <div class="experience" >


Professional experience : Over XXXXXXXX years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drswati.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div17" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Trupti K.Nadkarni

</strong><br />
Associate Professor
      <br />
MD, DNB, FCPS, DGO


</div>



</div>

     <div class="experience" >


Professional Experience : 10 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drtrupti.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div18" style="display:none;">
      


</div>


</div>

</div>








</div>


    <div class="devider_20px"></div>



<%--END 6TH HORZ--%>



<%--7TH HORZ--%>



 <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Amol Pawar.jpg" />

</div>



<div class="hor2">

<strong>Dr. Amol Pawar

</strong><br />
XXXXXXxxx
  <br />
XXXXXXXXXXXXX

</div>



</div>

     <div class="experience" >


Professional experience : Over XXXXXX years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-dramol.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div20" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Payal D. Lakhani

</strong><br />
Associate Professor
          <br />
MD, DNBE (OBGY)

</div>



</div>

     <div class="experience" >


Professional Experience : Over 11 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drpayal.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div21" style="display:none;">
      


</div>


</div>

</div>








</div>



    <div class="devider_20px"></div>





<%--END 7TH HORZ--%>




<%--8TH HORZ--%>





 <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Vandana Bansal

</strong><br />
XXXXXXXXX
  <br />
XXXXXXXXXXXXXX
</div>



</div>

     <div class="experience" >


Professional experience : Over XXXXX years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drvandana.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div22" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Rachna Dalmia.jpg" />

</div>



<div class="hor2">

<strong>Dr. Rachana Dalmia

</strong><br />
XXXXXXXXXx
  <br />
XXXXXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional experience : Over XXXXX years


    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drrachana.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div23" style="display:none;">
      


</div>


</div>

</div>








</div>



    <div class="devider_20px"></div>


<%--END 8TH HORZ--%>






















<%--    9thD HORIZONTAL LINE STARTS--%>

    <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Pooja Bandekar.jpg" />

</div>



<div class="hor2">

<strong>Dr. Pooja K. Bandekar

</strong><br />
Associate Professor
      <br />
M.D. (Obs. & Gyn.), D.N.B.,F.C.P.S., D.G.O.,D.F.P. (Gold Medal), M.R.C.O.G. (I), (LONDON).

</div>



</div>

     <div class="experience" >


Professional experience : Over 11 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drpooja.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div3" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<%--<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Deepali Prakash Kale

</strong><br />
Assistant Professor 
  <br />
MBBS, DGO (Muhs), DGO (Cps), FCPS, DNB


</div>



</div>

     <div class="experience" >


Professional Experience : 5 Years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drdeepali.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div4" style="display:none;">
      


</div>


</div>

</div>


</div>--%>



</div>

</div>

<%--FIRST DEPT END--%>




<%--12 DEPARTMENT START--%>


<h3 class="mypets">Dental Clinic </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Abraham Isaac.png" />

</div>



<div class="hor2">

<strong>Dr. Abraham Isaac

</strong><br />
BDS
</div>



</div>

     <div class="experience" >


<%--Professional Experience : XXXXX--%>

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drabraham.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div50" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<%--<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Mansi Chawla

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drmansi.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div51" style="display:none;">
      


</div>


</div>

</div>








</div>--%>

    <div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Yoban Shetty

</strong><br />
XXXXX <br />
XXXXXXXXX XX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-dryoban.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div52" style="display:none;">
      


</div>


</div>

</div>








</div>



<div class="devider_20px"></div>




    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Prakash Jain

</strong><br />
B.D.S.

</div>


</div>

     <div class="experience" >


Professional Experience: Over 15 years


    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drprakash.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div53" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;










</div>


</div>


<%--12nd DEPARTMENT END--%>






<%--13 DEPARTMENT START--%>


<h3 class="mypets">Endocrinology </h3>

<div class="docmain">
<div class="thepet">



<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Pravin Mhatre.jpg" />

</div>



<div class="hor2">

<strong>Dr. Pravin Mhatre

</strong><br />
Hon. Professor
  <br />
MD, FCPS, DGO, DFP


</div>



</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drpravin.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div4" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;





<div class="devider_20px"></div>








</div>


</div>


<%--13nd DEPARTMENT END--%>







<%--14 DEPARTMENT START--%>


<h3 class="mypets">Endoscopic Surgeries </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Pranay Shah

</strong><br />
XXXXXX
      <br />
XXXXX XXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drpranay.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div6" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Anurag Bhate

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-dranurag.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div7" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>




    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Nitin Shah.jpg" />

</div>



<div class="hor2">

<strong>Dr. Nitin Shah

</strong><br />
XXXXX <br />
XXXXXXXXX XX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drnitin.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div8" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. N.M.Narvekar

</strong><br />
XXXXXXX
      <br />
XXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drnarvekar.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div24" style="display:none;">
      


</div>


</div>

</div>








</div>

     <%--2ND HORIZONTAL ROW ENDS STARTS--%>

<div class="devider_20px"></div>


  <%--3RD HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Pravin Mhatre.jpg" />

</div>



<div class="hor2">

<strong>Dr. P.N.Mhatre

</strong><br />
Hon. Professor <br />
MD, FCPS, DGO, DFP


</div>



</div>

     <div class="experience" >


<div class="devider_20px"></div>

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drpravin.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div25" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. G.D.Balsarkar

</strong><br />
XXXXXXX
      <br />
XXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drbalsarkar.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div26" style="display:none;">
      


</div>


</div>

</div>








</div>

     <%--3RD HORIZONTAL ROW ENDS STARTS--%>

    <div class="devider_20px"></div>


<%--4TH HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. S.A.Bhalerao

</strong><br />
Hon. Clinical Assistant <br />
MD, DGO, FCPS, DNB, DFP, FRCOG, Diploma in Pelvic Endoscopy

</div>



</div>

     <div class="experience" >


<div class="devider_20px"></div>

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drbhalerao.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div27" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Swati Alhabadia

</strong><br />
XXXXXXX
      <br />
XXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drswati.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div28" style="display:none;">
      


</div>


</div>

</div>








</div>

     <%--4TH HORIZONTAL ROW ENDS STARTS--%>


<div class="devider_20px"></div>


<%--5TH HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. P.D.Tank

</strong><br />
XXXXX <br />
XXXXXXXXX XX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <%--<a target="_blank" href="women-about-doctdetails-drtank.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div29" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Trupti Nadkarni.jpg" />

</div>



<div class="hor2">

<strong>Dr. Trupti K.Nadkarni

</strong><br />
Associate Professor
      <br />
MD, DNB, FCPS, DGO

</div>


</div>

     <div class="experience" >


Professional Experience: 10 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drtrupti.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div31" style="display:none;">
      


</div>


</div>

</div>








</div>

     <%--5TH HORIZONTAL ROW ENDS STARTS--%>

<div class="devider_20px"></div>


<%--6TH HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Amol Pawar

</strong><br />
XXXXX <br />
XXXXXXXXX XX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-dramol.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div32" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Payal D. Lakhani

</strong><br />

Associate Professor

      <br />
MD, DNBE (OBGY)

</div>


</div>

     <div class="experience" >


Professional Experience: Over 11 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drpayal.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div33" style="display:none;">
      


</div>


</div>

</div>








</div>

     <%--6TH HORIZONTAL ROW ENDS STARTS--%>









</div>


</div>


<%--14nd DEPARTMENT END--%>







<%--15TH DEPARTMENT START--%>


<h3 class="mypets">Foetal Medicine</h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. P.R.Satoskar

</strong><br />
XXXXXX
      <br />
XXXXX XXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drsatoskar.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div34" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. V Bansal

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drbansal.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div35" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>




    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. G.D.Balsarkar

</strong><br />
XXXXX <br />
XXXXXXXXX XX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drbalsarkar.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div36" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;










</div>


</div>


<%--15TH DEPARTMENT END--%>






<%--16 DEPARTMENT START--%>


<h3 class="mypets">Gynaecologic Oncology  </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Pravin Mhatre.jpg" />

</div>



<div class="hor2">

<strong>Dr. P.N Mhatre

</strong><br />
Hon. Professor
      <br />
MD, FCPS, DGO, DFP


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drpravin.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div37" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Samar Gupta

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drsamar.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div38" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>








</div>


</div>


<%--16 DEPARTMENT END--%>








<%--17 DEPARTMENT START--%>


<h3 class="mypets">Preconception and Premarital Counselling   </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Trupti Nadkarni.jpg" />

</div>



<div class="hor2">

<strong>Dr. Trupti K.Nadkarni

</strong><br />
Associate Professor
      <br />
MD, DNB, FCPS, DGO


</div>



</div>

     <div class="experience" >


Professional Experience : 10 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drtrupti.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div41" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;





<div class="devider_20px"></div>











</div>


</div>


<%--17 DEPARTMENT END--%>






<%--18 DEPARTMENT START--%>


<h3 class="mypets">Reproductive and infertility clinic </h3>

<div class="docmain">
<div class="thepet">


<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Faram Irani.jpg" />

</div>



<div class="hor2">

<strong>Dr. Faram Irani

</strong><br />
Hon. Consultant
 <br />
MD, FCPS, FCCP, DGO, DFP


</div>



</div>

     <div class="experience" >


Professional Experience : Over 25 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drfaram.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div40" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Kanchanlata Puppal

</strong><br />
XXXXXXX
      <br />
XXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drkanchanlata.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div42" style="display:none;">
      


</div>


</div>

</div>








</div>


    
<div class="devider_20px"></div>


 <%--2nd HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Scherezade Irani

</strong><br />
XXXXXX
      <br />
XXXXX XXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drscherezade.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div19" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;





<div class="devider_20px"></div>




    <%--2nd HORIZONTAL ROW ENDS HERE--%>






</div>


</div>


<%--18 DEPARTMENT END--%>








<%--19 DEPARTMENT START--%>


<h3 class="mypets">Tertiary Level Neonatal Intensive care Services </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Amdekar.jpg" />

</div>



<div class="hor2">

<strong>Dr. Amdekar Yeshwant K.

</strong><br />
FRCPH (UK), MD, DCH, MBBS
  <br />
Medical Director, BJWHC



</div>



</div>

     <div class="experience" >


Professional experience : over 40 years <%--of clinical experience --%>

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drambedkar.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div5" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Sudha Rao.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sudha Rao

</strong><br />
Professor
  <br />
MD, Fellowship in Paediatric Endocrinology


</div>



</div>

     <div class="experience" >


Professional experience : Over 22 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drsudha.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div30" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>




    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Praful Shanbhag

</strong><br />
XXXXX <br />
XXXXXXXXX XX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drpraful.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div39" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Manjula Rupani

</strong><br />
XXXXXXX
      <br />
XXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drmanjula.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div43" style="display:none;">
      


</div>


</div>

</div>








</div>

     <%--END 2ND HORIZONTAL ROW STARTS--%>

<div class="devider_20px"></div>

    <%--3RRD HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Vinay Joshi.jpg" />

</div>



<div class="hor2">

<strong>Dr. Vinay Joshi

</strong><br />
Consultant  <br />
Department of Neonatology<br />
MBBS, MD, DM 


</div>



</div>

     <div class="experience" >




    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drvinay.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div44" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Avinash Desai

</strong><br />
XXXXXXX
      <br />
XXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-dravinash.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div45" style="display:none;">
      


</div>


</div>

</div>








</div>

     <%--END 3RD HORIZONTAL ROW STARTS--%>


<div class="devider_20px"></div>

    <%--4th HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Harvinder Palaha.jpg" />

</div>



<div class="hor2">

<strong>Dr. Harvinder Palah

</strong><br />
XXXXX <br />
XXXXXXXXX XX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drharvinder.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div46" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;




     <%--END 4th HORIZONTAL ROW STARTS--%>


























</div>


</div>


<%--19 DEPARTMENT END--%>






<%--20 DEPARTMENT START--%>


<h3 class="mypets">Urogynaecology </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Manu Sobti

</strong><br />
XXXXXX
      <br />
XXXXX XXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drmanu.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div47" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Ajit Vaze

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="women-about-doctdetails-drajit.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div48" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>


</div>


</div>


<%--20nd DEPARTMENT END--%>







<%--12 DEPARTMENT START--%>


<h3 class="mypets">Well Women Clinic </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Trupti Nadkarni.jpg" />

</div>



<div class="hor2">

<strong>Dr. Trupti K.Nadkarni

</strong><br />
Associate Professor
      <br />
MD, DNB, FCPS, DGO


</div>



</div>

     <div class="experience" >


Professional Experience : 10 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drtrupti.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div49" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;






</div>


</div>


<%--21 DEPARTMENT END--%>



<%--12 DEPARTMENT START--%>


<h3 class="mypets">Pathology Lab </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Jaya Deshpande.jpg" />

</div>



<div class="hor2">

<strong>Dr. Jaya Deshpande

</strong><br />
Pathologist
      <br />
MD, PGDHMM


</div>



</div>

     <div class="experience" >
         Professional Experience : Over 35 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drjaya.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div89" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;






</div>


</div>


<%--21 DEPARTMENT END--%>











<%--22 DEPARTMENT START--%>


<h3 class="mypets">Ultrasonography</h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Purnima photo.jpg" />

</div>



<div class="hor2">

<strong>Dr. Purnima R.Satoskar

</strong><br />
Professor
      <br />
MD, DNB, MRCOG
</div>



</div>

     <div class="experience" >

Professional experience : 26 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="women-about-doctdetails-drpurnima.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div54" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Vandana

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drvandana.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div55" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>




    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. A.A.Mehta

</strong><br />
XXXXX <br />
XXXXXXXXX XX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drmehta.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div56" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. V.V.Shah

</strong><br />
XXXXXXX
      <br />
XXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drshah.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div57" style="display:none;">
      


</div>


</div>

</div>








</div>

     <%--2ND HORIZONTAL ROW ENDS STARTS--%>

<div class="devider_20px"></div>


  <%--3RD HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. M.Mehta

</strong><br />
XXXXXXXXXXx <br />
XXXXXXXXXXXXXxxx


</div>



</div>

     <div class="experience" >


<div class="devider_20px"></div>

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drmmehta.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div58" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Umesh Athawale
</strong><br />
XXXXXXX
      <br />
XXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drumesh.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div59" style="display:none;">
      


</div>


</div>

</div>








</div>

     <%--3RD HORIZONTAL ROW ENDS STARTS--%>

    <div class="devider_20px"></div>


<%--4TH HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Suyashree Palkar

</strong><br />
XXXXXXXXX
    
    <br />
XXXXXXXXXXXXXXXXxxxx
</div>



</div>

     <div class="experience" >


<div class="devider_20px"></div>

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drsuyashree.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div60" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Anirudha Badade

</strong><br />
XXXXXXX
      <br />
XXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-dranirudha.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div61" style="display:none;">
      


</div>


</div>

</div>








</div>

     <%--4TH HORIZONTAL ROW ENDS STARTS--%>


<div class="devider_20px"></div>


<%--5TH HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sameer Dixit

</strong><br />
XXXXX <br />
XXXXXXXXX XX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drsameer.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div62" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Kalpana  

</strong><br />
XXXXXXXXXX
      <br />
XXXXXXXXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XXXX years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drkalpana  .aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div63" style="display:none;">
      


</div>


</div>

</div>








</div>

     <%--5TH HORIZONTAL ROW ENDS STARTS--%>

<div class="devider_20px"></div>


</div>


</div>


<%--22nd DEPARTMENT END--%>









<%--23 DEPARTMENT START--%>


<h3 class="mypets">Radiologist </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Mehul Mehta 
</strong><br />
XXXXXX
      <br />
XXXXX XXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drmehul.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div64" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Jyothi Dundappa

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drira.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div65" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>





</div>


</div>


<%--12nd DEPARTMENT END--%>
























<!-- Expand Collapse Script ends -->


            
                    
                    
				</div>
			</div>
		

</div>
    </div>
</asp:Content>
