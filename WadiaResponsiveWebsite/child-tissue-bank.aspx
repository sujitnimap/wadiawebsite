﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-tissue-bank.aspx.cs" Inherits="WadiaResponsiveWebsite.child_tissue_bank" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="row block05">
   <div class="col-full">
      <div class="wrap-col">
         <div class="heading">
            <h2> Audiology & Speech therapy</h2>
         </div>
         <br />
         <!-- ABOUT US CONTENT -->
         <div style="line-height:20px">
            <div class="scrollcontent">
               <div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">
                  <ul class="tabs" data-persist="true">
                     <li><a href="#view2">Overview</a></li>
                     <li><a href="#view3">Our Mission</a></li>
                     <li><a href="#view4">Safety Of Allografts</a></li>
                  </ul>
                  <div class="scrollbar" id="ex3">
                     <div class="tabcontents">

                        <!---- 1st view start ---->
                        <div id="view2">
                           The tissue bank at Bai Jerbai Wadia Hospital for Children was officially constituted by the Department of Plastic Surgery & Burns on the 15th of July 2021 in compliance with the Transplantation of Human Organ and Tissue Act -1994, amended in 2011.
                            <br />
                            <br />
                            Our tissue bank follows the guidelines laid by the Regional and State Organ and Tissue Transplant Organization, Mumbai (ROTTO SOTTO) for processes that involve screening, testing, processing, storage and distribution of human tissues. This ensures that safe tissues of reliable quality for human transplantation are made available.
                            <br />
                            <br />
                            The tissue bank has successfully cleared its inspection by the state government and has received its registration <b><u>BY GOVERNMENT OF MAHARASHTRA UNDER TRANSPLANTATION OF HUMAN ORGANS ACT, 1994(42 OF 1994)</u></b> ON 12TH August 2022.
                        </div>
                        <!---- 1st view end ---->
                        <!---- 2nd view start ---->
                        <div id="view3">
                            1) To provide safe, reliable and cost effective tissues for transplant<br />
                            2) To conduct research to enhance the properties of allografts<br />
                            3) To promote public awareness about tissue donation<br />
                        </div>
                        <!---- 2nd view end ---->
                        <!---- 3rd view start ---->
                        <div id="view4">
                           All donor's blood is screened to rule out HIV/AIDS, Hepatitis B & C and Syphilis.
                           <br /><br />
                            <b>BANKED TISSUES</b>
                           <div style="padding-left:10px">
                              -    FREEZE DRIED AMNION
                              <br />-	GLYCEROL PRSERVED CADAVER SKIN
                           </div>
                           <br />
                           <b>SOURCE OF TISSUES</b>
                           <div style="padding-left:10px">
                               AMNION - Placenta are donated by mothers from Nowrosjee Wadia Maternity hospital  post birth . Amniotic Membrane is seperated from the placenta, processed, tested and preserved 
                               <br />SKIN - It is donated posthumously from deceased donors above the age of 6 at BJWHC with the consent of the next of Kin.
                           </div>
                           <br />
                           <b>BENEFITS OF BANKED AMNION & SKIN</b>
                           <div style="padding-left:10px">
                                -   EXCELLENT BIOLOGICAL DRESSING/TEMPORARY WOUND COVER.
                                <br />- PROMOTES FASTER WOUND HEALING
                                <br />- RELIEVES PAIN
                                <br />- AVOIDS REPEATED DRESSING
                                <br />- GOOD LONG TERM RESULTS
                                <br />- COST EFFECTIVE.
                           </div>
                           <br />
                           <br />
                           <b>PLEASE NOTE:</b> Antigen is destroyed during tissue processing, therefore tissue typing and anti rejection drugs are not required for recipient of tissues.
                            <br />
                            <br />
                            <b>COST:</b> We charge a nominal amount for allografts that contributes towards the cost of processing
                            <br />
                            <br />
                            <div style="text-align: center;">
                            TISSUE DONATION IS A NOBLE DEED!!!
                            <br />BE A TISSUE DONOR
                            </div>
                            <br />
                            <br />
                            <b>FOR MORE INFORMATION CONTACT:</b>
                            <br />DR. SHANKAR SRINIVASAN – IN CHARGE –TISSUE BANK
                            <br />MRS. JOVITA SALDANHA – RESEARCH OFFICER- TISSUE BANK

                            <br />
                            <br />
                            <b>BAI JERBAI WADIA HOSPITAL FOR CHILDREN</b>
                            <br />TISSUE BANK
                            <br />DEPARTMENT OF PLASTIC SURGERY AND BURNS
                            <br />PAREL, MUMBAI 400 012, INDIA
                            <br />PHONE : 022-24197200 Ext: 316 ; Email :wadiaburnsandplastic@gmail.com


                        </div>
                        <!-- 3rd view end ---->
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- ABOUT US CONTENT -->
      </div>
   </div>
</div>

</asp:Content>
