﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drsumitra.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drsumitra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr Sumitra Venkatesh.jpg" />
<div class="docdetails">
Sumitra Venkatesh
<div class="docdetailsdesg">
Associate Professor
    <br />
DCH, DNB




</div>

</div>
</div>

                    <div class="areaofinterest">


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Affiliations</a></li>
        
            <li><a href="#view3">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
   •	Life member of Paediatric Cardiac Society of India,
<br />•	Member of Pediatriconcall.com – an open access website.
<br />•	Registered Member of Mediscan Systems, Chennai since 2006  




<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	“Protocol management of ante-natally diagnosed Cardio-vascular Disorders”, Book of Indo-British Symposium on Congenital Birth defects, 2002.
  <br /><br />•	“Clinical assessment of children with Congenital heart Diseases”, Paediatrics Today, Jan 2003.
  <br /><br />•	“Paediatric Therapeutic Cardiac Catheterisation”, Paediatrics Today, April 2003.
  <br /><br />•	“Non-surgical treatment of Congenital Heart Disease”, Journal of Post Graduate Medicine, 2004.
  <br /><br />•	“Cardiac Dysfunction in HIV positive children”, Indian Paediatrics (official journal of IAP), Feb 2005.
  <br /><br />•	“Ante-natal diagnosis of Congenital heart diseases-Fetal echocardiography”, http://www.paediatriconcall.com, an online journal with open access.
  <br /><br />•	“Left Ventricular inflow tract obstructions in children”, Journal of Post Graduate Medicine.
  <br /><br />•	Authored article on “Poisoning and Bites in Children” and “Normal development in Children” for a manual on Safe Child Care for General Practitioners 
  <br /><br />•	. Clinical assessment of child with congenital heart diseases – in “ Paediatrics Today”, Oct 2003
  <br /><br />•	 Paediatric Therapeutic Cardiac Interventions – in “Paediatric Today.” March 2003
  <br /><br />•	“Paediatric transcatheter interventions ” in Journal of Physicians  Jan 2004
  <br /><br />•	 “Fetal echocardiography”, Paediatric Oncall – Jan 2004. 
  <br /><br />•	 Evaluation of efficacy & safety of Himocospaz as antispasmodic agent – in the ‘Antiseptic’ – July 2004. 
  <br /><br />•	Authored article on “Approach to a case of Congenital heart Disease” in Paediatric clinics for Post-Graduates, a manual published by B.J.Wadia Hospital for Children.
  <br /><br />•	 Compiled articles & published the following articles in “Safe Child Care Book.” – A manual for General Practitioners:
  ‘Poisoning and bites in Children’
  ‘Normal Development’ 
  <br /><br />•	“Left Ventricular Inflow Obstructions” an article in Asian J of cardiology – Feb 2005.
  <br /><br />•	Compiled and edited articles in “Clinical Examination in Paediatrics” – A Book for post graduate students, November 2004.
  <br /><br />•	“Kawasaki Disease –An Enigma” in proceedings of PCSI 2005, Hyderabad.
  <br /><br />•	‘Evaluation of neonates with congenital heart diseases’- Paediatrics Today, September 2005.
  <br /><br />•	‘Fetal Echocardiography’ – Second issue of special edition of Neonatal cardiac problems, Paediatrics Today – December 2005. 
  <br /><br />•	Fetal Echocardiography – requiring actions in newborn period “proceedings of PEDICON 2006.
  <br /><br />•	Published an article “Cardiac Rhabdomyoma in neonate”. The Indian
      Practitioner, July   2006, Vol 59 (7):455-457.
  <br /><br />•	“Paediatric ECG- Basics”- In Paediatric On call-An online-journal, 
       February 2007
  <br /><br />•	Co-authored a chapter on LV inflow obstructions in the Book: ‘Clinical Diagnosis of Congenital Heart Disease’ by Jaypee Publications,2008
  <br /><br />•	Co-authored 3 topics in IAP Specialty textbook of Paediatric Cardiology 
  <br /><br />•	Co-authored 6 chapters in Paediatric Cardiology in ‘Text book of Cardiology for
       students and practitioners’ (Published in USA-In press)
  <br /><br />•	ECG Quiz in Paediatric-on call, April 2007.
  <br /><br />•	Article on “Paediatric ECG, Arrhythmias & 2 D-Echo.” in the proceedings of 
      IAPA symposium and workshop held in Mumbai, June 2007.
  <br /><br />•	Anthracycline induced Cardiac toxicity in children –A study, published in
       Paediatric Blood and Cancer (Official international publication of SIOP), 2008
  <br /><br />•	Venkatesh, Sumitra; Taksande, Amar; Prabhu, SS.Isolated Congenital Mitral Stenosis. JK Science;Jan-Mar2009, Vol. 11 Issue 1, p53.
  <br /><br />•	Sumitra Venkatesh, Pankaj Bagesar, Shakuntala Prabhu, SnehalKulkarni . Rheumatic  Fever and rheumatic heart disease: An urban study .Abstract .405, Cardiovascular Journal of Africa , Vol 24, No 1, January/February 2013. 166.
  <br /><br />•	Sumitra V, Taksande A, Prabhu SS. Profile of congenital heart diseases in children at tertiary care hospital. Indian Practitioner. 2008;61(10):441-447.
  <br /><br />•	Sumitra Venkatesh, Shakuntala Prabhu Introduction and Basics of Recording and Interpretation of Paediatric ECG. Available at www.paediatriconcall.com
  <br /><br />•	Sumitra .Venkatesh, Shakuntala Prabhu, Nitin .Burkule “Fetal echocardiography .” available at www.paediatriconcall.com 
    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			



		</div>
</asp:Content>
