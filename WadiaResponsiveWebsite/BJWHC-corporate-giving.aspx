﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-corporate-giving.aspx.cs" Inherits="WadiaResponsiveWebsite.child_corporate_giving" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-2-3">
            <div class="wrap-col">

                <div class="heading">
                    <h2>Corporate Giving </h2>
                </div>

                <br />

                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px">




                    <%--<b>Corporate Social Responsibility</b>
<br /><br />--%>
We believe that the best way to invest for the future is to invest in the health of our country’s youth. We are constantly looking to build philanthropic partnerships with like-minded organizations who will help us reach our goal of universal healthcare for children across sections of society. We  can work closely with your CSR Committee to create a CSR activity plan that meets your objectives.


    <br />
                    <br />

                    Each contribution will help our endeavour to provide state of the art curative, preventative and rehabilitative care to children with rare and complex conditions. 

        <br />
                    <br />

                    Your partnership with us will hold testament to your inspiring commitment to serving your society and taking special care of the little ones with big dreams. 

    <br />
                    <br />

                    We also work with many Strategic Consultant Partners. If you would like to consult with a Strategic Partner please get in touch with us. 

    <br />
                    <br />

  <b>We have developed strong bonds with several corporates over the years. Here are some of our partners who have shown us endless support :
</b>                  
                    <br />
                    <br />
                    <asp:DataList ID="dlistcorporate" RepeatColumns="4" RepeatDirection="Horizontal" runat="server">
                        <ItemTemplate>
                            <div align="center">
                                <div style="padding: 8px 8px 8px 8px">
                                    <asp:Image ID="img" ImageUrl='<%# Bind("logoimg", "~/adminpanel/{0}") %>' Width="100px" Height="100px" runat="server" />
                                    <br />
                                    <asp:Label ID="lblcorporatename" runat="server" Text='<%# Eval("description")%>'></asp:Label>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                    <br />
                    <br />



                    To get in touch with our Corporate Partnerships Team email <b>csr@wadiahospitals.org</b>





                </div>
                <!-- ABOUT US CONTENT -->



            </div>
        </div>

        <div class="col-1-3">
            <div class="wrap-col">

                <div class="box">
                    <div class="heading">
                        <h2>CONTRIBUTE</h2>
                    </div>
                    <div class="content">
                        <div class="list">
                            <ul>


                                <li><a href="BJWHC-Contribute-donate-online.aspx">Donate Online</a></li>
                                <li><a href="BJWHC-Contribute-Patient-welfare.aspx">Sponsor a Patient</a></li>
                                <li><a href="BJWHC-Contribute-Equipments.aspx">Donate Equipment</a></li>

                                <li><a href="BJWHC-Contribute-In-kind.aspx">Donate in-kind </a></li>


                                <li><a href="BJWHC-corporate-giving.aspx">Corporate Giving</a></li>
                                <li><a href="BJWHC-Testimonials.aspx?catnm=Contributors">Testimonials </a></li>

                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>











    </div>
</asp:Content>
