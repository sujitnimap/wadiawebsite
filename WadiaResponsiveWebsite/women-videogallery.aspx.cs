﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class women_videogallary : System.Web.UI.Page
    {
        IList<category> catdata;

        static string flag = "w";
        static string type = "Video";

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                getcategory();
            }


        }

        public void getcategory()
        {

            catdata = CCategory.Instance().GetCategoryData(flag, type);

            DataTable dt = CollectionHelper.GetDataTable(catdata);

            dlistchildvideocategory.DataSource = dt;
            dlistchildvideocategory.DataBind();

        }

        protected void imgbtnthumnail_Click(object sender, ImageClickEventArgs e)
        {

            ImageButton button = (sender as ImageButton);

            //Get the command argument
            string commandArgument = button.CommandArgument;

            DataTable albumdata = CVideo.Instance().GetVideogridDatabycategory(flag, commandArgument);

            dlistvideoalbumdtl.DataSource = albumdata;
            dlistvideoalbumdtl.DataBind();


            pnlcatdtl.Visible = false;
            pnlalbumsdtl.Visible = true;

            lbtn2.Visible = true;
        }

        protected void imgbalbumtnthumnail_Click(object sender, ImageClickEventArgs e)
        {

            ImageButton button = (sender as ImageButton);

            //Get the command argument
            string commandArgument = button.CommandArgument;

            int id = Convert.ToInt32(commandArgument);

            DataTable albumdata = CVideoGallary.Instance().GetVideogallaryData(id);

            dlistalbumvideos.DataSource = albumdata;
            dlistalbumvideos.DataBind();

            pnlcatdtl.Visible = false;
            pnlalbumsdtl.Visible = false;
            pnlalbumdtl.Visible = true;

            lbtn2.Visible = true;
            lbtn3.Visible = true;

        }

        protected void lbtn1_Click(object sender, EventArgs e)
        {
            Response.Redirect("women-videogallery.aspx");
        }

        protected void lbtn2_Click(object sender, EventArgs e)
        {

            DataTable albumdata = CVideoGallary.Instance().GetVideogallaryData(flag, type);

            dlistvideoalbumdtl.DataSource = albumdata;
            dlistvideoalbumdtl.DataBind();


            pnlcatdtl.Visible = false;
            pnlalbumsdtl.Visible = true;
            pnlalbumdtl.Visible = false;

            lbtn2.Visible = true;

        }


    }
}