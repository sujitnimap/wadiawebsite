﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-audiology-speech.aspx.cs" Inherits="WadiaResponsiveWebsite.child_audiology_speech" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2> Audiology & Speech therapy</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
Audiologists employ various diagnostic tests to determine whether someone can hear within the normal range, and if not, which portions of hearing (high, middle, or low frequencies) are affected and to what degree. The department offers management and treatment of hearing loss and speech/language disorders to children. The department collaborates with the Paediatric ENT specialists to offer the most efficient care to each and every child.

<br />
 <br />
Paediatric Speech Therapists work with children with speech, language, communication or swallowing difficulties.

 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               

The department assesses and manages children with the following conditions :
                                                <br /><br />
                <div style="padding-left:10px">

-	Attention and listening difficulties
<br />-	Autistic spectrum disorder
<br />-	Eating, drinking and swallowing difficulties
<br />-	Global developmental delay
<br />-	Hearing impairment and history of glue ear
<br />-	Language development (understanding and using language)
<br />-	Literacy
<br />-	Speech production difficulties
<br />-	Social communication difficulties
<br />-	Stammering and stuttering
<br />-	Tracheotomy to provide temporary or permanent breathing support
<br />-	Voice loss or hoarse voice
<br /> <br />

The Department has the following advanced diagnostic tools 
<br /> 
<br />-	Behavioural Observation Audiometry
<br />-	Brainstem Evoked Response Audiometry
<br />-	Pure Tone Audiometry
<br />-	Impedance Audiometry

</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            
		</div>
</asp:Content>
