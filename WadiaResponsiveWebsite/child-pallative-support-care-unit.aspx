﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-pallative-support-care-unit.aspx.cs" Inherits="WadiaResponsiveWebsite.pallative_support_care_unit"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Palliative & Supportive Care Unit</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">

    <strong>Overview </strong>
    <br />    <br />


The Palliative & Supportive Care Unit is a multidisciplinary team dedicated to pain and palliative care for children with serious illnesses and their families. It provides in-patient care as a flying unit, out-patient care in a child-friendly play-room cum counselling room, home visits and teleconsultation guidance for children sent home on end-of-life care and bereavement care for parents, siblings and other relatives. Its mission is to improve the quality of life for all children suffering from serious illnesses and their families, by preventing and alleviating total suffering via a holistic interdisciplinary approach. Child-life activities for all the children of the hospital include Clown therapy, play therapy and a Book and Toy library.
    <div style="height:150px"></div>



</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        



		</div>



</asp:Content>
