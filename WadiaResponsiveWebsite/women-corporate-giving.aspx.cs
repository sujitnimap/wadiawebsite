﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class women_corporate_giving : System.Web.UI.Page
    {
        IList<corporategiving> corporatedata;
        static string flag = "w";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                getcorporate();
            }

        }


        public void getcorporate()
        {
            corporatedata = CCorporate.Instance().GetCorporateData(flag);

            DataTable dt = CollectionHelper.GetDataTable(corporatedata);

            if (dt.Rows.Count > 0)
            {
                dlistcorporate.DataSource = dt;
                dlistcorporate.DataBind();
            }
            else
            {
                dlistcorporate.DataSource = null;
                dlistcorporate.DataBind();
            }
        }
    }
}