﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WadiaResponsiveWebsite
{
    public partial class women_events : System.Web.UI.Page
    {
        static string flag = "w";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                getpressmediadata(1);
            }

        }


        public void getpressmediadata(int currentPage)
        {
            int pageSize = 10;
            int _TotalRowCount = 0;

            string _ConStr = ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(_ConStr))
            {

                SqlCommand cmd = new SqlCommand("sp_GetEventData", con);
                cmd.CommandType = CommandType.StoredProcedure;

                int startRowNumber = ((currentPage - 1) * pageSize) + 1;

                cmd.Parameters.AddWithValue("@StartIndex", startRowNumber);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@flag", flag);

                SqlParameter parTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                parTotalCount.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parTotalCount);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                _TotalRowCount = Convert.ToInt32(parTotalCount.Value);

                dlisteventdata.DataSource = ds;
                dlisteventdata.DataBind();

                generatePager(_TotalRowCount, pageSize, currentPage);

            }
        }

        public void generatePager(int totalRowCount, int pageSize, int currentPage)
        {
            int totalLinkInPage = 5;
            int totalPageCount = (int)Math.Ceiling((decimal)totalRowCount / pageSize);

            int startPageLink = Math.Max(currentPage - (int)Math.Floor((decimal)totalLinkInPage / 2), 1);
            int lastPageLink = Math.Min(startPageLink + totalLinkInPage - 1, totalPageCount);

            if ((startPageLink + totalLinkInPage - 1) > totalPageCount)
            {
                lastPageLink = Math.Min(currentPage + (int)Math.Floor((decimal)totalLinkInPage / 2), totalPageCount);
                startPageLink = Math.Max(lastPageLink - totalLinkInPage + 1, 1);
            }

            List<ListItem> pageLinkContainer = new List<ListItem>();

            if (startPageLink != 1)
                pageLinkContainer.Add(new ListItem("First", "1", currentPage != 1));
            for (int i = startPageLink; i <= lastPageLink; i++)
            {
                pageLinkContainer.Add(new ListItem(i.ToString(), i.ToString(), currentPage != i));
            }
            //if (lastPageLink != totalPageCount)
            //    pageLinkContainer.Add(new ListItem("Last", totalPageCount.ToString(), currentPage != totalPageCount));

            dlPagerup.DataSource = pageLinkContainer;
            dlPagerup.DataBind();

            dlPagerdown.DataSource = pageLinkContainer;
            dlPagerdown.DataBind();
        }


        protected void dlPagerup_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "PageNo")
            {
                getpressmediadata(Convert.ToInt32(e.CommandArgument));
            }
        }

        protected void dlPagerdown_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "PageNo")
            {
                getpressmediadata(Convert.ToInt32(e.CommandArgument));
            }
        }

        protected void dlisteventdata_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    string eventidFromDB = e.CommandArgument.ToString();
                    long eventid = Convert.ToInt64(e.CommandArgument);
                    string filePath = "";
                    //get path from id and then write file download code
                    string _ConStr = ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(_ConStr))
                    {

                        SqlCommand cmd = new SqlCommand("GetFilePathByEventId", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id", eventid);
                        con.Open();
                        filePath = (string)cmd.ExecuteScalar();

                        if (!string.IsNullOrEmpty(filePath))
                        {
                            //write file download code
                            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filePath + "\"");
                            Response.TransmitFile(Server.MapPath(filePath));
                            Response.End();
                        }
                        else
                        {
                            //print in alert no file exists 
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "<script>alert('No file exists for this event')</script>", true);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void dlisteventdata_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Button btnForDownload = (Button)e.Item.FindControl("btnDownload");
                string eventidFromDB = btnForDownload.CommandArgument;
                long eventid = Convert.ToInt64(eventidFromDB);
                string filePath = "";
                //get path from id and then write file download code
                string _ConStr = ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ConnectionString;
                using (SqlConnection con = new SqlConnection(_ConStr))
                {

                    SqlCommand cmd = new SqlCommand("GetFilePathByEventId", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", eventid);
                    con.Open();
                    object filepathfromDB = cmd.ExecuteScalar();
                    if (filepathfromDB == DBNull.Value)
                    {

                    }
                    else
                    {
                        filePath = Convert.ToString(filepathfromDB);
                    }
                    if (!string.IsNullOrEmpty(filePath))
                    {
                        btnForDownload.Visible = true;
                    }
                    else
                    {
                        btnForDownload.Visible = false;
                    }
                }
            }
        }
    }
}