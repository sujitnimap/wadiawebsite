﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-ophthalmology.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_ophthalmology" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Paediatric Ophthalmology </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
Paediatric Ophthalmology is a specialty concerned with eye diseases, visual development and vision care in children. The department conducts an OPD once a week. The department also conducts screening for babies from the NICU for Retinopathy of Prematurity (ROP). Apart from offering laser treatments, the doctors also administer advanced techniques such as intra-vitreal injections of ‘Avastin’ to treat severe cases of ROP.
 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               

The Ophthalmology Department treats children with the following conditions :


                                <br /><br />
                <div style="padding-left:10px">

-	Amblyopia (lazy eye)
<br />-	Red Eyes
<br />-	Nasolacrimal Duct Obstruction
<br />-	Blocked tear ducts
<br />-	Childhood/congenital glaucoma
<br />-	Congenital cataracts
<br />-	Errors of refraction (myopia nearsightedness, hypermetropia/farsightedness and astigmatism)



</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

         


		</div>




</asp:Content>
