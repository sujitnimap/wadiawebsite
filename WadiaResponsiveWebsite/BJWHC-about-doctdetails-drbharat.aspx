﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drbharat.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drbharat" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Bharat Agarwal.jpg" />
<div class="docdetails">

Dr. Bharat Radhakisan Agarwal 



<div class="docdetailsdesg">
H.O.D. & Hon. Hemato-Oncology
    <br />

MD, DCH, DNB, MNAMS

</div>

</div>
</div>

<div class="areaofinterest">

    Areas of Interest : Paediatric Haematology, Paediatric Oncology	

</div>
<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
     <%--       <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research or publications</a></li>
           <%-- <li><a href="#view5">Recent Abstracts </a></li>
            <li><a href="#view6">Editor </a></li>
            <li><a href="#view7">Chapters in Books </a></li>--%>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
        Honours & Awards
                    <div class="leftpaddocdetail">
                       •	Invited Guest Faculty for International, National and State level  conferences, symposia and workshops. Invited guest speaker for the 22nd International Congress of Paediatrics (IPA), Amsterdam 1998
<br /><br />
                        •	Paediatric Haematology & Oncology Chapter R.V. Lokeshwar Oration, November2008, New Delhi. Topic : How to improve Paediatric Oncology services in India ?
<br /><br />
•	Athavle Research Foundation Oration, October 2010, Mumbai. Topic : Clinical insights into new molecular discoveries in Paediatric Haematology.
<br /><br />
•	Japanese Foundation for Multidisciplinary Treatment of Cancer, “JFMC Fellowship” as visiting faculty to Japan, March 2011
        <br /><br />
                </div>
                </P>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
                
            •	Honorary Secretary General SIOP, 2005-2011
<br /><br />
•	Chair, Education & Training Task Force (ETTF) of SIOP, 2009-2011
<br /><br />
•	Chief Editor, SIOP Education Book & SIOP Newsletter, 2005-2011
<br /><br />
•	Secretary, Publication Committee, Paediatric Blood & Cancer, 2005-
<br /><br />
•	Chair LOC, SIOP 2007, Mumbai
<br /><br />
•	President, SIOP Asia, 2001-2004

<br /><br />
•	Editorial Board, ADC, PBC, PHO, IP, IJPP, 2002-2011
<br /><br />
•	Chairman, Paediatric Haematology & Oncology Chapter of IAP, 
<br /><br />
•	National Co-ordinator, INTP-PPO, 1995-2003
<br /><br />
•	Honorary Secretary General, Indian Academy of Paediatrics, 2004-
<br /><br />
•	Chief Organising Secretary, Pedicon 2007
<br /><br />
•	Honorary Secretary General, PHO Chapter of IAP, 1994-1996

    <br /><br />
    



       







               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
      <div id="view4">
          
          <div class="leftpaddocdetail">
                <p>
                                   
                      								  
•	Mudaliar S, Agarwal BR. Paediatric Acute Myeloid Leukaemia—is There a Scope for Change in the Developing World? Paediatric Haematology-Oncology May 2011, Vol. 28, No. 4: 253-256
<br /><br />
•	Agarwal BR, Creutzig U, Janic D, Kebudi R, Punnett A , Training Requirements for Subspecialty Programmes in Paediatric Haematology and Oncology & Standards for Training Centres. SIOP ETTF writing group, SIOP, Eindhoven, October 2010.
<br /><br />
•	Agarwal BR. Juvenile Myelomonocytic Leukaemia. Iranian Journal of Blood & Cancer 2010 Mar;2(1):47-48.
<br /><br />
•	Shah M, Agarwal BR. Recent advances in management of acute myeloid leukaemia (AML). Indian J Pediatr. 2008 Aug;75(8):831-7.
<br /><br />
•	Agarwal BR. Paediatric Haematology & Oncology in India. Indian J Pediatr. 2008;Nov  75 SS3: 1-2.
<br /><br />
•	Agarwal BR. Biological therapy for paediatric malignancy: current perspectives. Indian J Pediatr. 2008 Aug;75(8):839-44.
<br /><br />
•	Kellie SJ, Al-Lamki Z, Agarwal BR, Wali Y, Ngcamu N, Al-Sawafi N, Kurkure P. Childhood cancer: quest for a complete cure. Pediatr Blood Cancer. 2008 Dec;51(6):843-5. 
<br /><br />
•	Jain R, Sachdev A, Agarwal BR. Management of Unexplained Fever in the Neutropenic Patient. P.H.O. Bulletin, IAP. July-Sept 2007 2(1): 55-59. 
<br /><br />
•	Agarwal BR, Paediatric Haematology & Oncology in India. Pediatr Blood Cancer. 2007 Nov;49: 397. 
<br /><br />
•	Barr RD, Sala A, Wiernikowski J, Masera G, Mato G, Antillon F, Castillo L, Petrilli S, Quintana J, Ribeiro R, Agarwal BR, Hesseling P. A formulary for paediatric oncology in developing countries. Pediatr Blood Cancer. 2005 May;44(5):433-5.
<br /><br />
•	Vyas J, Agarwal BR, Dalvi RB et al. Spontaneous and Induced CFU-GM assays  in cases of childhood myeloproliferative disorders and Viral infections . Ind Pediatr.2005, 54:77-82
<br /><br />
•	Vyas J, Dalvi RB, Agarwal BR, et al. Study on ALL-1 gene alterations in Indian      childhood acute leukaemia : non-isotopic southern blotting and molecular   cytogenetics. Leuk Res 2003, 27 : 915-923
<br /><br />
•	Vyas J, Modi D, Dalvi RB, Agarwal BR. Diagnostic significance of WCP-FISH using Alu PCR probe with special reference to chromosome 11 in childhood acute   leukaemia. Ind J Hematol & Blood Trans 2003, 21 (3) : 88-91.
<br /><br />
•	Agarwal BR Anaemia in Children. J Gen Med 2002, 14 (3) : 63-74.
<br /><br />
•	Agarwal BR, Lokeshwar MR. ITP in Children : First do not harm : Ind J Hematol & Blood Trans 2000, XVIII (3), 78-80.
<br /><br />
•	Agarwal BR, Lokeshwar MR. The challenge of childhood ALL. Ind J Hematol & Blood Trans  2000, XVIII (2), 33-34.

<br /><br />

 </p>
                    
<b>Recent Abstracts </b>
<br /><br />
   <p>
                
 •	Agarwal BR et al.Improving Access to Care: “One step at a time” – What’s learnt from the Indian experience : Strategies developed in India. Abstract Book, The 9th Continental Meeting of International Society of Paediatric Oncology (SIOP) Africa, 
		Accra 2010, Pp 11-12.
<br /><br />
•	Bhatnagar S, Agarwal BR et al.Solid Tumours in Infancy – An Audit. Iranian Journal of Blood & Cancer 2010 Mar;2(1):43
<br /><br />
•	Sharma P, Agarwal BR et al. Infant Leukaemia: Perspective from Tertiary Care Paediatric Hospital. Iranian Journal of Blood & Cancer 2010 Mar;2(1):42
<br /><br />
•	Sharma P, Agarwal BR et al. CNS Presentations of ALL. Paediatric Blood & Cancer. 2010 Nov;55(5):996
<br /><br />
•	Sharma P, Agarwal BR et al. CNS Relapsed Wilms Tumors: A 5 years experience. Paediatric Blood & Cancer.2010 Nov;55(5):1001
<br /><br />
•	Kadakia P, Agarwal BR et al. 10 years experience with Neuroblastma in Children less than 18 months. Paediatric Blood & Cancer. 2010 Nov;55(5):1001
<br /><br />
•	Kadakia P, Agarwal BR et al. Primary Ovarian Tumours in a Tertiary Care Hospital. Paediatric Blood & Cancer. 2010 Nov;55(5):1004

    <br /><br />
</p>
               
<b>Editor </b>
              <br /><br />

  <p>
                
           •	Senior editor.  IAP Textbook of Paediatrics, 3rd edition, Jaypee Brothers, New Delhi, 2004.
<br /><br />
•	Editor. Child Survival & Development: Recommendations of National Consultation meeting on Child Survival & Development 20-21 November 2004, Delhi, Indian Academy of Paediatrics.
<br /><br />
•	Chief Editor. SIOP Education Book 2005. Ed: Agarwal BR, Perilongo G,  Rogers P, Strahlendorf C, Eden OB SIOP, Eindhoven, 2005, Pp. 1-124.
<br /><br />
•	Editor of Quarterly Bulletin of IAP ‘Academy Today’ for 4years from 2002-2005.
<br /><br />
•	Editor of “SIOP News”, Newsletter of International Society of Paediatric Oncology, from 2005-11
<br /><br />
•	Editor. Reference manual, National Training Project, Practical Paediatric Haematology., PHO chapter of IAP, New Delhi, 2006
<br /><br />
•	Editor. Reference manual, National Training Project, Nurses Training Programme in Practical Paediatric Haematology & Oncology., PHO chapter of IAP, New Delhi, 2006
<br /><br />
•	Editor. Reference manual Haemophilia, National Training Project, Haematology., PHO chapter of IAP, New Delhi, 2006
<br /><br />
•	Editor. IAP Speciality Series on Paediatric Haematology & Oncology. IAP, Mumbai, 2006 Pp. 1-409.
<br /><br />
•	Chief Editor. SIOP Education Book 2006. Ed: Agarwal BR, Perilongo G, Wacker P, Eden OB SIOP, Eindhoven, 2006, Pp. 1-137.
<br /><br />
•	Agarwal BR, Lokeshwar MR. Editors Section 3 Haematology Oncology. Advances in Paediatric PP 405-510 edited by Dutta AK, Sachdeva Anupam. Publishers Jaypee Brother Medical Publisher (P) Ltd, 2007 PP1103. ISBN: 81-8061-889-7 20. 
<br /><br />
•	Editor. Reference manual ITP, National Training Project, PHO chapter of IAP, New Delhi, 2007
<br /><br />
•	Editor. The Archives of The Paediatric Haematology & Oncology Chapter of IAP 1987-2007, PHO chapter of IAP, Mumbai 2007, Pp 1-126
<br /><br />
•	Chief Editor. SIOP Education Book 2007. Ed: Agarwal BR, Perilongo G,  Calaminus G, , Eden OB SIOP, Eindhoven, 2007 Pp. 1-145.
<br /><br />
•	Peer Review. Reference Manual of Thalessemial, National Training Project, PHO chapter of IAP, New Delhi, 2008
<br /><br />
•	Chief Editor. SIOP Education Book 2008. Ed: Agarwal BR, Calaminus G, Henze G, Egeler M. SIOP, Eindhoven, 2008, Pp. 1-100.
<br /><br />
•	Chief Editor. SIOP Education Book 2009. Ed: Agarwal BR, Calaminus G, Camargo B, Egeler M. SIOP, Eindhoven, 2009, Pp. 1-124.
<br /><br />
•	Chief Editor. SIOP Education Book 2010. Ed: Agarwal BR, Calaminus G, Diller L, Egeler M. SIOP, Eindhoven, 2010, Pp.1-160.


    <br /><br />
    
</p>

 
<b>Chapters in Books</b>
<br /><br />
                <p>
                
          •	Barr  RD, Ribeiro RC, Agarwal BR et al. Paediatric oncology in countries with limited resources. 
                In : Principles and Practice of paediatric oncology, 4th edition, Eds : Pizzo PA, Poplack DG, Lippincott Williams and Wilkins, Philadelphia, 2002, Pp. 1541-1552.
<br /><br />
•	Agarwal BR, Dalvi RB. Treatment of Childhood Leukemias in underprivileged countries. In :  Treatment of Acute Leukemias : New Directions for Clinical Research. 
Ed : Pui CH. Current clinical oncology series, The Humana Press Inc., New Jersey, 2003, Pp. 321-329.
<br /><br />

•	Agarwal BR. Childhood Lymphomas. Reference manual, National Training Project, Practical Paediatric Oncology. 2nd edition, PHO chapter of IAP, Mumbai, 2003, Pp 68-75.
<br /><br />
•	Agarwal BR, Dalvi RB. Blood components in paediatric oncology practice. Reference manual, National Training Project, Practical Paediatric Oncology. 2nd edition, PHO chapter of IAP, Mumbai, 2003, Pp 128-133.
<br /><br />
•	Agarwal BR, Currimbhoy ZE. Cancer in children is curable. Reference manual, National Training Project, Practical Paediatric Oncology. 2nd edition, PHO chapter of IAP, Mumbai, 2003, Pp 3-5.
<br /><br />
•	Agarwal BR. Liver tumours. Reference manual, National Training Project, Practical Paediatric Oncology. 2nd edition, PHO chapter of IAP, Mumbai, 2003, Pp 108-111.
<br /><br />
•	Agarwal BR. Aplastic Anaemia: Current Issues in Diagnosis and Management. 
Reference manual, National Training Project, Practical Paediatric Haematology, PHO chapter of IAP, New Delhi 2006, Pp 27-33.
<br /><br />
•	Shah NK, Agarwal BR. Autoimmune Haemolytic Anaemia- In IAP Speciality Series Book on Paediatric Haematology & Oncology. IAP, Mumbai, 2006 Pp. 117-123. 
<br /><br />
•	Sachdev A, Agarwal BR. IAP Guidelines on Thalassemia. In IAP National Guidelines 2006, UNICEF & IAP, Mumbai 2006 Pp. 167-202
<br /><br />
•	Sachdev A, Agarwal BR. IAP Guidelines on Haemophilia. In IAP National Guidelines 2006
		UNICEF & IAP, Mumbai 2006 Pp. 203-256
<br /><br />
•	Sachdev A, Agarwal BR. IAP Guidelines on Recombinant Activated Factor VIIa. In IAP National Guidelines 2006, UNICEF & IAP, Mumbai 2006 Pp. 257-268
<br /><br />
•	Sachdev A, Agarwal BR. IAP Guidelines on Diagnosis & Management of ITP in children. In IAP National Guidelines 2006, UNICEF & IAP, Mumbai 2006 Pp. 31-46
<br /><br />
•	Sachdev A, Agarwal BR. IAP Guidelines on Acute Lymphoblastic Leukaemia (ALL). In IAP National Guidelines 2006, UNICEF & IAP, Mumbai 2006 Pp. 93-112
<br /><br />
•	Sachdev A, Agarwal BR. IAP Guidelines on Blood Components. In IAP National Guidelines 2006, UNICEF & IAP, Mumbai 2006 Pp. 143-166
<br /><br />
•	Sachdev A, Agarwal BR. IAP Guidelines on Sickle Cell Disease. In IAP National Guidelines 2006, UNICEF & IAP, Mumbai 2006 Pp. 269-276
<br /><br />
•	Agarwal BR. Approach to a child with purpura. In ASK IAP, A series…Basics & beyond. Eds Khubchandani R, Gajendragadkar A, et al. 2006, IAP, Mumbai 2006 Pp. 65-68.
<br /><br />
•	Agarwal BR. Approach to Pallor in Children. In RAPID (Rational Algorithmic Practical IAP endorsed Diagnostic) Approach to Common Symptoms, Standardization of paediatric office practice . Eds Shah R, Agarwal B, et al. 2005, IAP, Mumbai 2005 Pp. 43-46.
<br /><br />
•	Agarwal BR, Dalvi RB. Autoimmune Haemolytic Anaemia. Chapter Editors - Lokeshwar MR, Choudhary VP - IAP Textbook of Paediatrics, Indian Academy of Paediatrics  4th Edition. Parthasarathy A (ed) PP 845-846, 2008.
<br /><br />
•	Dalvi RB, Agarwal RB. RBC enzymopathies and membranopathies. Chapter Editors - Lokeshwar MR, Choudhary VP - IAP Textbook of Paediatrics, Indian Academy of Paediatrics  4th Edition. Parthasarathy A (ed) PP 837-8839, 2008.
<br /><br />
•	Agarwal BR Role of Biological Therapy for Paediatric Tumours  in Paediatric Oncology, Surgical & Medical Aspects PP 634-643 edited by Gupta DK, Carachi R. Publishers Jaypee Brother Medical Publisher (P) Ltd., 2007 PP.589-93. ISBN: 81-8061-966-4.
<br /><br />
•	Agarwal BR Use of Biological Agents in Supportive Care in Paediatric Oncology, Surgical & Medical Aspects PP 634-643 edited by Gupta DK, Carachi R. Publishers Jaypee Brother Medical Publisher (P) Ltd., 2007 PP.603-05. ISBN: 81-8061-966-4.
<br /><br />
•	Agarwal BR, Transfusion of Blood Components in Children. Advances in Paediatric PP 493-510 edited by Dutta AK, Sachdeva Anupam. Publishers Jaypee Brother Medical Publisher (P) Ltd, 2007 PP1103. ISBN: 81-8061-889-7

                    <br /><br />

    
</p>









                </div>


            </div>
            
            <!---- 4th view end ---->     
            
            
            
            <!---- 5th view start ---->
              
              
          <%--  <div id="view5">
               
               

             
            </div>--%>
       <!-- 5th view end ---->
            
            
                <!---- 6th view start ---->
              
              
           <%-- <div id="view6">
               
               

              
               
            </div>--%>
       <!-- 6th view end ---->
            
         
          <!---- 7th view start ---->
              
              
            <div id="view7">
               
               

               
            </div>
       <!-- 7th view end ---->        
            
            
            
            
            
            
            
            
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
		
        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
									<li><a href="child-about-overview.aspx">Overview</a></li>
									<li><a href="child-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="child-about-chairman-msg.aspx">Chairman's message</a></li>
									<li><a href="child-about-awards.aspx">Awards and Achievements</a></li>
									<li><a href="child-about-history.aspx">History </a>

                                    <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>



									</li>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>


		</div>
	


</asp:Content>
