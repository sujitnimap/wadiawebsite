﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-super-speciality-services.aspx.cs" Inherits="WadiaResponsiveWebsite.women_super_speciality_services" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>services</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->


                    <div style="line-height:20px;">
    
    <div class="serviceheading">SUPER SPECIALITY SERVICES</div>

    <div style="padding-left:30px">

     <div class="devider_20px"></div>
    
 
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13%" height="66px" valign="middle"><img src="images/services/High risk OPD.png" width="46" height="46" /></td>
    <td class="servicename" width="87%"><a href="child-highrisk-opd.aspx">	High risk OPD  </a></td>
  </tr>
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Anaesthesia.png" width="46" height="42" /></td>
    <td class="servicename"><a href="child-paediatric-anaesthesia.aspx"> 	Paediatric Anaesthesia </a></td>
  </tr>
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Burns and Plastic surgery.png" width="52" height="42" /></td>
    <td align="left" class="servicename"><a href="child-paediatric-burns.aspx"> 	Paediatric Burns and Plastic surgery</a></td>
  </tr>
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Dermatology.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-paediatric-dermatology.aspx"> 	 Paediatric Dermatology	  </a></td>
  </tr>
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Cardiology.png" width="46" height="41" /></td>
    <td class="servicename"><a href="child-paediatric-cardiology.aspx"> 	 Paediatric Cardiology  (PICU)</a></td>
  </tr>
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Dentistry.png" width="46" height="42" /></td>
    <td class="servicename"><a href="child-paediatric-dentistry.aspx">     Paediatric Dentistry  </a></td>
  </tr>
  
  
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Endocrinology.png" width="38" height="38" /></td>
    <td class="servicename"><a href="child-paediatric-endocrinology.aspx">     Paediatric Endocrinology  </a></td>
  </tr>
  
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric ENT.png" width="46" height="43" /></td>
    <td class="servicename"><a href="child-paediatric-ent.aspx">    Paediatric ENT  </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Haemato-Oncology and immunology.png" width="48" height="48" /></td>
    <td class="servicename"><a href="child-paediatric-haemato.aspx">     Paediatric Haemato-Oncology and immunology  </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric- liver clinic.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-paediatric-hepato.aspx">     Paediatric-liver clinic </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Immunization clinic(1).png" width="47" height="47" /></td>
    <td class="servicename"><a href="child-paediatric-perinatalhiv.aspx">     Paediatric and Perinatal HIV clinic </a></td>
  </tr>
  
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Nephrology.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-paediatric-perinatalhiv.aspx">     Paediatric Nephrology </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Neurology.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-paediatric-neurology.aspx">     Paediatric Neurology </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Neurosurgery.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-paediatric-neurosurgery.aspx">     Paediatric Neurosurgery  </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Ophthalmology.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-paediatric-ophthalmology.aspx">    Paediatric Ophthalmology </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Orthopaedics.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-paediatric-orthopaedics.aspx">    Paediatric Orthopaedics </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Tuberculosis Clinic.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-paediatric-tuberculosis.aspx">      Paediatric Tuberculosis Clinic </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Paediatric Urology.png" width="38" height="39" /></td>
    <td class="servicename"><a href="child-paediatric-urology.aspx">      Paediatric Urology </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Well Baby Clinic.png" width="46" height="52" /></td>
    <td class="servicename"><a href="child-wellbaby-clinic.aspx">     Well Baby Clinic  </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Immunization clinic(1).png" width="47" height="47" /></td>
    <td class="servicename"><a href="child-clubfoot-clinic.aspx">     Clubfoot Clinic   </a></td>
  </tr>
  
   
  <tr>
    <td valign="middle" height="66"><img src="images/services/Epilepsy Clinic.png" width="38" height="38" /></td>
    <td class="servicename"><a href="child-epilepsy-clinic.aspx">    Epilepsy Clinic   </a></td>
  </tr>
  
  
</table>











</div>

</div>
<!-- ABOUT US CONTENT -->



                     <div class="serviceheading">SUPPORTIVE SERVICES</div>

    <div style="padding-left:30px">

     <div class="devider_20px"></div>
    
 


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13%" height="55" valign="middle"><img src="images/services/Audiology & Speech therapy.png" width="46" height="52" /></td>
    <td class="servicename" width="87%"><a href="child-audiology-speech.aspx">	Audiology & Speech therapy  </a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/services/Blood bank.png" width="52" height="42" /></td>
    <td class="servicename"><a href="child-blood-bank.aspx"> 	Blood bank </a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/services/Nutrition- Dietition.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-nutrition.aspx"> 	Nutrition- Dietition </a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/services/Physiotherapy  &  Occupational Therapy.png" width="46" height="46" /></td>
    <td class="servicename"><a href="child-physiotherapy.aspx"> 	Physiotherapy  &  Occupational Therapy  </a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/services/Pathology.png" width="52" height="44" /></td>
    <td class="servicename"><a href="child-pathology.aspx"> 	Pathology</a></td>
  </tr>
  <tr>
    <td valign="middle" height="55"><img src="images/services/Radiology.png" width="47" height="47" /></td>
    <td class="servicename"><a href="child-radiology.aspx">     Radiology  </a></td>
  </tr>

    <tr>
    <td valign="middle" height="55"><img src="images/services/Social Service Department.png" width="47" height="47" /></td>
    <td class="servicename"><a href="child-socialserv-dept.aspx">     Social Service Department  </a></td>
  </tr>



</table>






</div>

</div>




                
                    
                    
				</div>
			</div>



</asp:Content>
