﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="about-us-doctors.aspx.cs" Inherits="WadiaResponsiveWebsite.about_us_doctors" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


		<div class="row block05">
		<div class="col-full">
            <div class="wrap-col">
					
                    <%--<div class="title"><span>List Of Doctors and Departments</span></div>--%>
                    
                    
                    In the words of the great Nelson Mandela, "There can be no keener revelation of a society's soul than the way in which it treats its children". Here at the Bai Jerbai Hospital for Children, we have a dedicated team of over 60 paediatric specialists, aided by highly trained nurses who understand that giving each child the love, comfort and care that they truly deserve is as important as our highly specialized treatments. 
<!-- Expand collapse Script -->
                    <br />
                    <br />

<!--<h2>Example 1:</h2>
<a href="#" onClick="ddaccordion.expandone('mypets', 0); return false">Expand 1st header</a> | <a href="#" onClick="ddaccordion.collapseone('mypets', 0); return false">Collapse 1st header</a> | <a href="#" onClick="ddaccordion.toggleone('mypets', 1); return false">Toggle 2nd header</a> -->


              

<h3 class="mypets">Paediatric Medicine</h3>
<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr_y_ambdekar-2.jpg" />

</div>



<div class="hor2">

<strong>Dr. Amdekar Yeshwant K.

</strong><br />
FRCPH (UK), MD, DCH, MBBS
  <br />
Medical Director, BJWHC



</div>



</div>

     <div class="experience" >


Professional experience : over 40 years <%--of clinical experience --%>

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drambedkar.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div9" style="display:none;">
      


</div>


</div>

</div>








</div>
&nbsp;&nbsp;

<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Shakuntala Prabhu.jpg" />

</div>



<div class="hor2">

<strong>Dr. Shakuntala Prabhu

</strong><br />
Professor
  <br />
MD, DCH, FRCPCH



</div>



</div>

     <div class="experience" >


Professional Experience : Over 21 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drshakuntla.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div10" style="display:none;">
      


</div>


</div>

</div>








</div>


  <div class="devider_20px"></div>

<%--one horizontal line end (2 Doctors)--%>







<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Sudha Rao.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sudha Rao

</strong><br />
Professor
  <br />
MD, Fellowship in Paediatric Endocrinology


</div>



</div>

     <div class="experience" >


Professional experience : Over 22 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drsudha.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div8" style="display:none;">
      


</div>


</div>

</div>








</div>



&nbsp;&nbsp;




    <div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Rajesh Joshi.jpg" />

</div>



<div class="hor2">

<strong>Dr. Rajesh Joshi

</strong><br />
Professor
  <br />
MD, DCH, DNB, Fellowship in Endocrinology


</div>


</div>

     <div class="experience" >


Professional experience : Over 20 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drrajesh.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div7" style="display:none;">
      


</div>


</div>

</div>








</div>







<%--2nd horizontal line end (2 Doctors)--%>


    <div class="devider_20px"></div>




<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Shilpa Kulkarni

</strong><br />
XX
  <br />
XXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX XXXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drshilpa.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div11" style="display:none;">
      


</div>


</div>

</div>








</div>


&nbsp;&nbsp;


    <div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Ira shah.jpg" />

</div>



<div class="hor2">

<strong>Dr. Ira Shah

</strong><br />
Associate Professor
  <br />
MD (Paediatrics), DCH, FCPS, DNB, Diploma in Paediatric Infectious Diseases

</div>


</div>

     <div class="experience" >


Professional experience: Over 15 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drira.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div12" style="display:none;">
      


</div>


</div>

</div>








</div>




<%--3rd horizontal line end (2 Doctors)--%>


    <div class="devider_20px"></div>




<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Alpana Ohri.jpg" />

</div>



<div class="hor2">

<strong>Dr. Alpana Ohri

</strong><br />
XX
  <br />
XXXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-dralpana.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div13" style="display:none;">
      


</div>


</div>

</div>








</div>


&nbsp;&nbsp;


    <div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Sumitra Venkatesh.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sumitra Venkatesh

</strong><br />
Associate Professor
  <br />
DCH, DNB
</div>


</div>

     <div class="experience" >


Professional Experience: Over 13 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drsumitra.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div14" style="display:none;">
      


</div>


</div>

</div>








</div>



<%--4th horizontal line end (2 Doctors)--%>


    <div class="devider_20px"></div>




<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Laxmi Shobhavat.jpg" />

</div>



<div class="hor2">

<strong>Dr. Laxmi Shobhavat

</strong><br />
XX
  <br />
XXXXXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drlaxmi.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div15" style="display:none;">
      

</div>


</div>

</div>




</div>


&nbsp;&nbsp;


    <div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Parmarth Chandane

</strong><br />
XXXXX XXXXXX  <br />
XXXX XXX
</div>


</div>

     <div class="experience" >


Professional Experience : XXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drparmarth.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div16" style="display:none;">
      


</div>


</div>

</div>




</div>




<%--5th horizontal line end (2 Doctors)--%>


    <div class="devider_20px"></div>




<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Vaidehi Dande

</strong><br />
XX
  <br />
XXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drvaidehi.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div17" style="display:none;">
      

</div>


</div>

</div>




</div>


&nbsp;&nbsp;


    <div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Rafat Sayad

</strong><br />
XXXXX XXXXXX  <br />
XXXX XXXXX
</div>


</div>

     <div class="experience" >


Professional Experience : XXXX XXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drrafat.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div18" style="display:none;">
      


</div>


</div>

</div>




</div>







































</div>


</div>






 

<h3 class="mypets">General Paediatric Surgery</h3>
<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Sushmita Bhatnagar.png" />

</div>



<div class="hor2">

<strong>Dr. Sushmita Bhatnagar

</strong><br />
Professor and Head of Paediatric Surgery


  <br />
MS, M.ch, M.Phil


</div>



</div>

     <div class="experience" >


Professional Experience : 17 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drsushmitabhat.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div19" style="display:none;">
      


</div>


</div>

</div>








</div>
&nbsp;&nbsp;

<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Pradnya Bendre.jpg" />

</div>



<div class="hor2">

<strong>Dr. Pradnya Bendre

</strong><br />
Professor and Unit Head
      <br />
MS, MCh, DNB



</div>



</div>

     <div class="experience" >
         Professional Experience: 14 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drpradnya.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div20" style="display:none;">
      


</div>


</div>

</div>








</div>


  <div class="devider_20px"></div>

<%--one horizontal line end (2 Doctors)--%>







<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Suyodhan Reddy

</strong><br />
XXXXXXXX  <br />
XXXXXXXXX
</div>



</div>

     <div class="experience" >


Professional experience : XXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drsuyodhan.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div21" style="display:none;">
      


</div>


</div>

</div>








</div>



&nbsp;&nbsp;




    <div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Mukunda Ramchandra

</strong><br />
XXXXXXX  <br />
XXXXXXXX XX

</div>


</div>

     <div class="experience" >


Professional experience : XXXXXXX XX
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drmukunda.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div22" style="display:none;">
      


</div>


</div>

</div>








</div>







<%--2nd horizontal line end (2 Doctors)--%>


    <div class="devider_20px"></div>




<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Parag Karkera

</strong><br />
XX
  <br />
XXXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drparag.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div23" style="display:none;">
      


</div>


</div>

</div>








</div>


&nbsp;&nbsp;


    <div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Nargish Barsivala

</strong><br />
XXXXXXXXXX  <br />
XXXXXXXXXX XXXXXXX
</div>


</div>

     <div class="experience" >


Professional experience : XXXXXX XXX 

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drnargish.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div24" style="display:none;">
      


</div>


</div>

</div>








</div>




<%--3rd horizontal line end (2 Doctors)--%>


    <div class="devider_20px"></div>




<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sunil Kelkar

</strong><br />
XX
  <br />
XXXXXXXXXXX

</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drsunil.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div25" style="display:none;">
      


</div>


</div>

</div>








</div>


&nbsp;&nbsp;


    <div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Rajeev Redkar.png" />

</div>



<div class="hor2">

<strong>Dr. Rajeev Redkar

</strong><br />
Asst. Hon. Pediatric Surgery
      <br />
MS, Mch, DNB, FRCS, IAS
</div>


</div>

 <div class="experience" >


<%--Professional Experience: XXXXXXX--%>

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drrajeev.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div26" style="display:none;">
      


</div>


</div>

</div>








</div>



<%--4th horizontal line end (2 Doctors)--%>


    <div class="devider_20px"></div>




<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr vivek.jpg" />

</div>



<div class="hor2">

<strong>Dr. Vivek Rege

</strong><br />
Asst. Hon. Paediatric Surgery
  <br />
MS, Mch, DNB (Ped Sur)

</div>



</div>

     <div class="experience" >


Professional Experience : Over 33 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drvivek.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div27" style="display:none;">
      

</div>


</div>

</div>




</div>


&nbsp;&nbsp;


    <div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sarita S. Bhagwat

</strong><br />
XXXXX XXXXXX  <br />
XXXX XXXXXXXXXX XXX
</div>


</div>

     <div class="experience" >


Professional Experience : XXXXXX XXXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drsarika.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div28" style="display:none;">
      


</div>


</div>

</div>




</div>




<%--5th horizontal line end (2 Doctors)--%>


    <div class="devider_20px"></div>




<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Amrish Vaidya

</strong><br />
XX
  <br />
XXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-dramrish.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div29" style="display:none;">
      

</div>


</div>

</div>




</div>


&nbsp;&nbsp;


    <%--<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Rafat Sayad

</strong><br />
XXXXX XXXXXX  <br />
XXXX XXXXXXXXXX XXXXXXXXXXXXXX
</div>


</div>

     <div class="experience" >


Professional Experience : XXXXXXXXXXXXX XXXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drira.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div30" style="display:none;">
      


</div>


</div>

</div>




</div>--%>








</div>


</div>






<%--TWO DEPARTMENTS COMPLETED--%>


<%--THIRD DEPARTMENT START--%>


<h3 class="mypets">Neonatal Intensive Care Unit (NICU)
</h3>

<div class="docmain">
<div class="thepet">



    <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Sudha Rao.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sudha Rao

</strong><br />
Professor
  <br />
MD, Fellowship in Paediatric Endocrinology


</div>



</div>

     <div class="experience" >


Professional experience : Over 22 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drsudha.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div5" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Alpana Ohri.jpg" />

</div>



<div class="hor2">

<strong>Dr. Alpana Ohri

</strong><br />
XX
  <br />
XXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-dralpana.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div6" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px">


</div>

<%--    2ND HORIZONTAL LINE STARTS--%>

    <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Vaidehi Dande

</strong><br />
XXXXXXX  <br />
XXXXXXXXXXXX

</div>



</div>

     <div class="experience" >


Professional experience : XXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drvaidehi.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div34" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Praful Shanbaug

</strong><br />
XX
  <br />
    XXXXXXXXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drpraful.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div35" style="display:none;">
      


</div>


</div>

</div>


</div>



<div class="devider_20px"></div>

<%--    3RD HORIZONTAL LINE STARTS--%>

    <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Avinash Desai

</strong><br />
XXXXXXX  <br />
XXXXXXXXXXXXX XX

</div>



</div>

     <div class="experience" >


Professional experience : XXXXXXXXXXX 
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-dravinash.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div36" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Vinay Joshi.jpg" />

</div>



<div class="hor2">

<strong>Dr. Vinay Joshi

</strong><br />
Consultant
   <br /> 
     Department of Neonatology

  <br />
MBBS, MD, DM

</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drvinay.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div37" style="display:none;">
      


</div>


</div>

</div>


</div>


<div class="devider_20px"></div>


<%--    4TH HORIZONTAL LINE STARTS--%>

    <div class="doc1">

<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Manjula Rupani

</strong><br />
XXXXXXX  <br />
XXXXXXXXXXXXX XXXXXX

</div>



</div>

     <div class="experience" >


Professional experience : XXXXXXXX XXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drmanjula.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div38" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Harvinder Palaha.jpg" />

</div>



<div class="hor2">

<strong>Dr. Harvinder Palaha

</strong><br />
XX
  <br />
XXXXXXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drharvinder.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div39" style="display:none;">
      


</div>


</div>

</div>


</div>



<div class="devider_20px"></div>


<%--    5TH HORIZONTAL LINE STARTS--%>

    <div class="doc1">

<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sanjeev

</strong><br />
XXXXXXX  <br />
XXXXXXXXXXXXX XXXXXX

</div>



</div>

     <div class="experience" >


Professional experience : XXXXXX XXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drsanjeev.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div40" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;




</div>

</div>


<%--THIRD DEPARTMENT END--%>




<%--FOURTH DEPARTMENT START--%>


<h3 class="mypets">Paediatric Intensive Care Unit (PICU)</h3>

<div class="docmain">
<div class="thepet">


<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Uma Ali

</strong><br />
XXXXXXXXXXX  <br />
XXXXXXXXXXX XXXXXX

</div>



</div>

     <div class="experience" >


Professional experience : XXXXXXXX XXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-druma.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div30" style="display:none;">
      


</div>


</div>

</div>








</div>
&nbsp;&nbsp;

    <div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Laxmi Shobhavat.jpg" />

</div>



<div class="hor2">

<strong>Dr. Laxmi Shobhavat

</strong><br />
XX
  <br />
XXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drlaxmi.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div41" style="display:none;">
      

</div>


</div>

</div>




</div>

</div>


</div>



<%--FOURTH DEPARTMENT END--%>

<%--FIFTH DEPARTMENT START--%>


<h3 class="mypets">High risk OPD</h3>

<div class="docmain">
<div class="thepet">


<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Sudha Rao.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sudha Rao

</strong><br />
Professor
  <br />
MD, Fellowship in Paediatric Endocrinology


</div>



</div>

     <div class="experience" >


Professional experience : Over 22 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drsudha.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div31" style="display:none;">
      


</div>


</div>

</div>








</div>
&nbsp;&nbsp;


</div>


</div>



<%--FIFTH DEPARTMENT END--%>

<%--SIXTH DEPARTMENT START--%>


<h3 class="mypets">Paediatric Anaesthesia</h3>

<div class="docmain">
<div class="thepet">



    <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Aruna Bapat

</strong><br />
Professor
  <br />
MD, Fellowship in Paediatric Endocrinology


</div>



</div>

     <div class="experience" >


Professional experience : Over 22 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-draruna.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div32" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Pradnya Sawant

</strong><br />
XX
  <br />
XXXXXXXXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXXXXXX XXXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drpradnya.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div33" style="display:none;">
      


</div>


</div>

</div>








</div>


    <div class="devider_20px"></div>

<%--2nd horizotal row starts --%>


    <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Minal Shah

</strong><br />
XXXXXXXXXXX
  <br />
XXXXXXXXX XXXXXXXXXXX

</div>



</div>

     <div class="experience" >


Professional experience : XXXXXXXXXXX 

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drminal.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div42" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Mukta Shikhre

</strong><br />
XX
  <br />
XXXXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drmukta.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div43" style="display:none;">
      


</div>


</div>

</div>








</div>



     <div class="devider_20px"></div>

<%--3RD horizotal row starts --%>


    <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Varinder Kaur

</strong><br />
XXXXXXXXXXX
  <br />
XXXXXXXXX XXXXX

</div>



</div>

     <div class="experience" >


Professional experience : XXXX XXXXXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drvarinder.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div44" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;





</div>


</div>


<%--SIXTH DEPARTMENT END--%>




 <%--SEVENTH DEPARTMENT START--%>


<h3 class="mypets">Paediatric Burns and Plastic surgery</h3>

<div class="docmain">
<div class="thepet">



    <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Arvind  Madhusudan Vartak.jpg" />

</div>



<div class="hor2">

<strong>Dr. Arvind  Madhusudan Vartak

</strong><br />
Hon. Ped. Burns & Plastic
  <br />
M.S. General Surgery, M.S. Plastic Surgery


</div>



</div>

     <div class="experience" >


Professional Experience: 44 Years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drarvind .aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div45" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Mukund Thatte.jpg" />

</div>



<div class="hor2">

<strong>Dr. Mukund R. Thatte

</strong><br />
Asst. Hon. Burns & Plastic
  <br />
M.S. (gen.) M.Ch. (plastic)


</div>



</div>

     <div class="experience" >


Professional experience: 28 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drmukund.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div46" style="display:none;">
      


</div>


</div>

</div>








</div>


    <div class="devider_20px"></div>

<%--2nd horizotal row starts --%>


    <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Shankar Srinivasa

</strong><br />
XXXXXXXXXXX
  <br />
XXXXXXXXX XXXXXXXX
</div>



</div>

     <div class="experience" >


Professional experience : XXXXXXX XXXXXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drshankar.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div47" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;







</div>


</div>


<%--SEVENTH DEPARTMENT END--%>






 <%--EIGHT DEPARTMENT START--%>


<h3 class="mypets">Paediatric Dermatology</h3>

<div class="docmain">
<div class="thepet">



    <div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Deepak Parikh 

</strong><br />
XXXXXXXXX  <br />
XXXX XXXXXXX

</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXXX XXXXX
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drdeepak.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div1" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Manish Shah 

</strong><br />
XXXXXXXX  <br />
XXXXXX

</div>



</div>

     <div class="experience" >


Professional experience : XXXXX XX XXX
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drmanishshah.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div2" style="display:none;">
      


</div>


</div>

</div>








</div>


</div>


</div>


<%--EIGHT DEPARTMENT END--%>


 <%--NINE DEPARTMENT START--%>


<h3 class="mypets">Paediatric Cardiology </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Shakuntala Prabhu.jpg" />

</div>



<div class="hor2">

<strong>Dr. Shakuntala Prabhu

</strong><br />
Professor
  <br />
MD, DCH, FRCPCH



</div>



</div>

     <div class="experience" >


Professional Experience : Over 21 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drshakuntla.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div3" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr Sumitra Venkatesh.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sumitra Venkatesh

</strong><br />
Associate Professor
  <br />
DCH, DNB
</div>


</div>

     <div class="experience" >


Professional Experience: Over 13 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drsumitra.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div4" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>

    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Snehal Kulkarni

</strong><br />
XXXXX <br />
XXXXXXXXX XX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drsnehal.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div48" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. N.C. Joshi 

</strong><br />
XXXXXXX
      <br />
XXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drjoshi.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div49" style="display:none;">
      


</div>


</div>

</div>








</div>







</div>


</div>


<%--NINE DEPARTMENT END--%>




 <%--TEN DEPARTMENT START--%>


<h3 class="mypets">Paediatric Dentistry </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Abraham Isaac.png" />

</div>



<div class="hor2">

<strong>Dr. Abraham Isaac 

</strong><br />
XXXXXX
      <br />
XXXXX XXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drabrar.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div50" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Yoban.jpg" />

</div>



<div class="hor2">

<strong>Dr. Yoban Shetty 

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drjyothi.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div51" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>

    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr.Prakash.jpeg" />

</div>



<div class="hor2">

<strong>Dr. Prakash Jain 

</strong><br />
XXXXX <br />
XXXXXXXXX XX


</div>



</div>

     <div class="experience" >


Professional Experience : Over 15 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-dryoban.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div52" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;












</div>


</div>


<%--TEN DEPARTMENT END--%>




                    
 <%--ELEVEN DEPARTMENT START--%>


<h3 class="mypets">Paediatric Endocrinology </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Sudha Rao.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sudha Rao

</strong><br />
Professor
  <br />
MD, Fellowship in Paediatric Endocrinology


</div>



</div>

     <div class="experience" >


Professional experience : Over 22 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drsudha.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div54" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Meena Desai     

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drmeena.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div55" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>

    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Rajesh Joshi.jpg" />

</div>



<div class="hor2">

<strong>Dr. Rajesh Joshi

</strong><br />
Professor
  <br />
MD, DCH, DNB, Fellowship in Endocrinology


</div>


</div>

     <div class="experience" >


Professional experience : Over 20 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drrajesh.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div56" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Aparna Limaye

</strong><br />
XXXXXXX
      <br />
XXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XX XXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-draparna.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div57" style="display:none;">
      


</div>


</div>

</div>








</div>







</div>


</div>


<%--ELEVEN DEPARTMENT END--%>





                             
 <%--TWELVE DEPARTMENT START--%>


<h3 class="mypets">Paediatric ENT </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Divya Prabhat.png" />

</div>



<div class="hor2">

<strong>Dr. Divya Prabhat

</strong><br />
Hon. ENT Surgeon
      <br />
MS, DORL, DNB, FICS

</div>



</div>

     <div class="experience" >


Professional experience : 20 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drdivya.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div58" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Carlton Pereira   

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drcarlton.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div59" style="display:none;">
      


</div>


</div>

</div>








</div>




</div>


</div>


<%--TWELVE DEPARTMENT END--%>



                    
 <%--THIRTEEN DEPARTMENT START--%>


<h3 class="mypets">Paediatric Haemato-Oncology </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Bharat Agarwal.jpg" />

</div>



<div class="hor2">

<strong>Dr. Bharat Radhakisan Agarwal 

</strong><br />
H.O.D. & Hon. Hemato-Oncology
      <br />
MD, DCH, DNB, MNAMS

</div>



</div>

     <div class="experience" >


Professional experience : 27 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drbharat.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div60" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Nitin Shah.jpg" />

</div>



<div class="hor2">

<strong>Dr. Nitin Shah     

</strong><br />
Asst. Honorary Paediatric Haematologist Oncologist      
    <br />
MD, DCH


</div>


</div>

     <div class="experience" >


Professional Experience: 29 Years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drnitin.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div61" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>

    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Archana Swamy

</strong><br />
Professor
  <br />
XXXXXXX XXXX

</div>


</div>

     <div class="experience" >


Professional experience : XXXXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drarchana.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div62" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Mukesh Desai.jpg" />

</div>



<div class="hor2">

<strong>Dr. Mukesh Desai

</strong><br />
Hon. In Hemato-Oncology & Immunology
      <br />
MBBS, MD (Paediatrics); DCH
</div>


</div>

     <div class="experience" >


Professional Experience : Over 20 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drmukesh.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div63" style="display:none;">
      


</div>


</div>

</div>








</div>







</div>


</div>


<%--THIRTEEN DEPARTMENT END--%>




                             
 <%--FOURTEEN DEPARTMENT START--%>


<h3 class="mypets">Paediatric Hepato-biliary clinic </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Ira shah.jpg" />

</div>



<div class="hor2">

<strong>Dr. Ira Shah

</strong><br />
Associate Professor
  <br />
MD (Paediatrics), DCH, FCPS, DNB, Diploma in Paediatric Infectious Diseases

</div>


</div>

     <div class="experience" >


Professional experience: Over 15 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drira.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div64" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Sushmita Bhatnagar.png" />

</div>



<div class="hor2">

<strong>Dr. Sushmita Bhatnagar   

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drsushmita.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div65" style="display:none;">
      


</div>


</div>

</div>








</div>




</div>


</div>


<%--FOURTEEN DEPARTMENT END--%>




                    
 <%--FIFTEEN DEPARTMENT START--%>


<h3 class="mypets">Paediatric and Perinatal HIV clinic </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Ira shah.jpg" />

</div>



<div class="hor2">

<strong>Dr. Ira Shah

</strong><br />
Associate Professor
  <br />
MD (Paediatrics), DCH, FCPS, DNB, Diploma in Paediatric Infectious Diseases

</div>


</div>

     <div class="experience" >


Professional experience: Over 15 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drira.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div66" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Mamatha M lala.jpg" />

</div>



<div class="hor2">

<strong>Dr. Mamatha  M. Lala 

</strong><br />
Research Officer - HIV
      <br />
DCH, DNB


</div>


</div>

     <div class="experience" >


Professional experience : Over 13 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drmamaha.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div67" style="display:none;">
      


</div>


</div>

</div>








</div>




</div>


</div>


<%--FIFTEEN DEPARTMENT END--%>




              
 <%--SIXTEEN DEPARTMENT START--%>


<h3 class="mypets">Paediatric Nephrology </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Uma Ali

</strong><br />
XXXXXXXXXXX  <br />
XXXXXXXXXXX XXXXXX

</div>



</div>

     <div class="experience" >


Professional experience : XXXXXXXX XXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-druma.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div68" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Alpana Ohri.jpg" />

</div>



<div class="hor2">

<strong>Dr. Alpana Ohri

</strong><br />
XX
  <br />
XXXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-dralpana.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div69" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>

    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Kumud Mehta

</strong><br />
XXXXX
  <br />
XXXXXXX XXXX

</div>


</div>

     <div class="experience" >


Professional experience : XXXXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drkumud.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div70" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Shashank Parekhji

</strong><br />
XXXX
          <br />
XXXXXX XXXX

</div>


</div>

     <div class="experience" >


Professional Experience : XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drshashank.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div71" style="display:none;">
      


</div>


</div>

</div>








</div>




<div class="devider_20px"></div>

    <%--3RD HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Deokar Atul.jpg" />

</div>



<div class="hor2">

<strong>Dr. Atul Deokar

</strong><br />
Research Officer Nephrology
      <br />
MD

</div>


</div>

     <div class="experience" >


Professional experience : 13 Years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-dratul.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div72" style="display:none;">
      


</div>


</div>

</div>




</div>



</div>


</div>


<%--SIXTEEN DEPARTMENT END--%>


            
 <%--SEVENTEEN DEPARTMENT START--%>


<h3 class="mypets">Paediatric Neurology </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Shilpa Kulkarni

</strong><br />
XXXXX
      <br />
XXXXXX XXX
</div>


</div>

     <div class="experience" >


Professional experience: XXXXXXX XXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drshilpa.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div73" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Anaitha Hegde.jpg" />

</div>



<div class="hor2">

<strong>Dr. Anaita Udwadia Hegde 

</strong><br />
Hon. Consultant In Epilepsy & Neurosurgery
          <br />
MD (Ped.), MRCPCH, Fellowship Ped. Neuroscience


</div>


</div>

     <div class="experience" >


Professional Experience: 20 Years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-dranaitha.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div74" style="display:none;">
      


</div>


</div>

</div>








</div>



<div class="devider_20px"></div>

    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. K.N. Shah

</strong><br />
XXXXX
  <br />
XXXXXXX XXXX

</div>


</div>

     <div class="experience" >


Professional experience : XXXXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drshah.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div75" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;

</div>


</div>


<%--SEVENTEEN DEPARTMENT END--%>


          
 <%--EIGHTEEN DEPARTMENT START--%>


<h3 class="mypets">Paediatric Neurosurgery </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Deo Pujari

</strong><br />
XXXXX
      <br />
XXXXXX XXX
</div>


</div>

     <div class="experience" >


Professional experience: XXXXXXX XXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drdeo.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div76" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Naresh Biyani

</strong><br />
XXXXX XXXX
       <br />
XXXX XXXXXXXXX

</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drnaresh.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div77" style="display:none;">
      


</div>


</div>

</div>








</div>



<div class="devider_20px"></div>

    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Uday Andhar

</strong><br />
XXXXX
  <br />
XXXXXXX XXXX

</div>


</div>

     <div class="experience" >


Professional experience : XXXXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-druday.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div78" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;

</div>


</div>


<%--EIGHTEEN DEPARTMENT END--%>





 <%--NINETEEN DEPARTMENT START--%>


<h3 class="mypets">Paediatric Ophthalmology</h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Ashwin Kumar Sainani        

</strong><br />
XXXXX
      <br />
XXXXXX XXX
</div>


</div>

     <div class="experience" >


Professional experience: XXXXXXX XXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drashwin.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div79" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;




</div>


</div>


<%--NINETEEN DEPARTMENT END--%>



           
 <%--TWENTY DEPARTMENT START--%>


<h3 class="mypets">Paediatric Orthopaedics </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr rujuta mehta.jpg" />

</div>



<div class="hor2">

<strong>Dr. Rujuta Mehta

</strong><br />
Hon Ped. Orthopaedics
      <br />
M S (Ortho.), DNB (Ortho)

</div>



</div>

     <div class="experience" >


Professional experience : Over 22 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drrujuta.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div80" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dominic.jpg" />

</div>



<div class="hor2">

<strong>Dr. Dominic D’Silva

</strong><br />
Asst. Hon Paediatric Orthopaedics
      <br />
D'Ortho, MS, DNB-Ortho

</div>



</div>

     <div class="experience" >


Professional Experience : 32 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drdominic.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div81" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>

    <%--2ND HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr alaric aroojis.jpg" />

</div>



<div class="hor2">

<strong>Dr. Alaric John Aroojis
</strong><br />
Asst. Hon. Ped Orthopaedics
  <br />
D'ortho, MS, DNB-Ortho, Fellow Ped Ortho.

</div>


</div>

     <div class="experience" >


Professional experience : Over 18 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-dralaric.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div82" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Abhay Nene.jpg" />

</div>



<div class="hor2">

<strong>Dr. Abhay Nene

</strong><br />
Hon. Spine Surgeon
          <br />
MS (Orth.)

</div>


</div>

     <div class="experience" >


Professional Experience : 12 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drabhay.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div83" style="display:none;">
      


</div>


</div>

</div>








</div>




<div class="devider_20px"></div>

    <%--3RD HORIZONTAL ROW STARTS--%>

<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Manish Agarwal.jpg" />

</div>



<div class="hor2">

<strong>Dr. Manish Agarwal

</strong><br />
Asst. Honarary Ortho. Oncologist
      <br />
M.S. (Ortho) ; D (Ortho); D.N.B(Ortho)

</div>


</div>

     <div class="experience" >


Professional experience : Over 20 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drmanish.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div84" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Arjun Dhawale

</strong><br />
XXXXXXXXX
          <br />
XXXXXXXX XXXXX

</div>


</div>

     <div class="experience" >


Professional Experience : XXXXXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drarjun.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div85" style="display:none;">
      


</div>


</div>

</div>








</div>





</div>


</div>


<%--TWENTY DEPARTMENT END--%>





 <%--21 DEPARTMENT START--%>


<h3 class="mypets">Paediatric Tuberculosis Clinic</h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Ira shah.jpg" />

</div>



<div class="hor2">

<strong>Dr. Ira Shah

</strong><br />
Associate Professor
  <br />
MD (Paediatrics), DCH, FCPS, DNB, Diploma in Paediatric Infectious Diseases

</div>


</div>

     <div class="experience" >


Professional experience: Over 15 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drira.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div86" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;




</div>


</div>


<%--21 DEPARTMENT END--%>



 <%--22 DEPARTMENT START--%>


<h3 class="mypets">Paediatric Urology</h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Pradnya Bendre.jpg" />

</div>



<div class="hor2">

<strong>Dr. Pradnya Bendre

</strong><br />
Professor and Unit Head
      <br />
MS, MCh, DNB



</div>



</div>

     <div class="experience" >
         Professional Experience: 14 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drpradnya.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div87" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;




</div>


</div>


<%--22 DEPARTMENT END--%>



<%--23 DEPARTMENT START--%>


<h3 class="mypets">Audiology & Speech therapy</h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sadiya Ansari

</strong><br />
XXXXXXX  XXXXX
      <br />
XXX XXXXXXX



</div>



</div>

     <div class="experience" >
         Professional Experience : XXXXXXXXXXX
    </div>

<div style="width:100%">
<div class="hor11">

</div>

        

<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drsadiya.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div88" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;




</div>


</div>


<%--23 DEPARTMENT END--%>



<%--24 DEPARTMENT START--%>


<h3 class="mypets">Pathology</h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/Dr. Jaya Deshpande.jpg" />

</div>



<div class="hor2">

<strong>Dr. Jaya Deshpande

</strong><br />
Pathologist
      <br />
MD, PGDHMM


</div>



</div>

     <div class="experience" >
         Professional Experience : Over 35 years
    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drjaya.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div89" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;




</div>


</div>


<%--24 DEPARTMENT END--%>


<%--25 DEPARTMENT START--%>


<h3 class="mypets">Physiotherapy  </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Urmila Kamat

</strong><br />
XXXXXX
      <br />
XXXXXXXXXX XX
</div>



</div>

     <div class="experience" >


Professional experience : XXXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drurmila.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div90" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<%--<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Deepti Rane

</strong><br />
XX
  <br />
XXXXXXXXXXXX


</div>



</div>

     <div class="experience" >


Professional Experience : XXXXXXXXXX XXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drdeepti.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div91" style="display:none;">
      


</div>


</div>

</div>








</div>


<div class="devider_20px"></div>--%>

    <%--2ND HORIZONTAL ROW STARTS--%>

<%--<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Shruti Banjan
</strong><br />
BPT
  <br />
Physiotherapist

</div>


</div>

     <div class="experience" >


Professional experience : 3 years

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drshruti.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div92" style="display:none;">
      


</div>


</div>

</div>








</div>--%>

&nbsp;&nbsp;


<%--<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Jalpa Parikh

</strong><br />
BPT, MPT
          <br />
Physiotherapist

</div>


</div>

     <div class="experience" >

         <div style="height:25px;"></div>


    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


 <a target="_blank" href="child-about-doctdetails-drjalpa.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div93" style="display:none;">
      


</div>


</div>

</div>








</div>--%>





    <%--3RD HORIZONTAL ROW STARTS--%>

<%--<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Sharon Sonty

</strong><br />
XXXXXXXX XXX
          <br />
XXXX XXXXXXX
</div>


</div>

     <div class="experience" >


Professional experience : XXXXXXXXX XXXXXXXXXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drsharon.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>
     

      <div id="Div94" style="display:none;">
      


</div>


</div>

</div>








</div>--%>

&nbsp;&nbsp;








</div>


</div>


<%--25 DEPARTMENT END--%>


             
 <%--26 DEPARTMENT START--%>


<h3 class="mypets">Radiology </h3>

<div class="docmain">
<div class="thepet">



<div class="doc1">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Malini Nadkarni

</strong><br />
XXXXXXXXXX XXX
  <br />
XXXXXXX XXXXX
</div>


</div>

     <div class="experience" >


Professional experience: XXXXXXXXX XXX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drmalini.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div95" style="display:none;">
      


</div>


</div>

</div>








</div>

&nbsp;&nbsp;


<div class="doc2">



<div style="width:100%">
<div class="hor1">
<img src="images/doc/dr.jpg" />

</div>



<div class="hor2">

<strong>Dr. Saroj Rathod  

</strong><br />
XXXXXX
      <br />
XXX XXXXXX


</div>


</div>

     <div class="experience" >


Professional Experience: XXXXXX XX

    </div>

<div style="width:100%">
<div class="hor11">

</div>



<div class="hor22">


<%-- <a target="_blank" href="child-about-doctdetails-drsaroj.aspx" onmouseover="nXXXhpup.popup($('#hidden-table2').html(), {'width': 350});"><div class="viewProfile"></div></a>--%>
     

      <div id="Div96" style="display:none;">
      


</div>


</div>

</div>








</div>




</div>


</div>


<%--26 DEPARTMENT END--%>




<!-- Expand Collapse Script ends -->

                
				</div>
			</div>









		



</asp:Content>
