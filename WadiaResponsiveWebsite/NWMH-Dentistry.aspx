﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Dentistry.aspx.cs" Inherits="WadiaResponsiveWebsite.women_dentalclinic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Dental Clinic</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
            Taking care of dental health is of utmost importance while pregnant. Pregnancy causes hormonal changes that increase the risk of developing gum disease which, in turn, can affect the health of your developing baby. At the Dental Clinic, women can get their teeth professionally cleaned, gum tissue can be carefully examined, and any oral health problems can be treated in advance of your pregnancy. 

 <br />
 <br />


Dental treatments during the first trimester and second half of the third trimester should be avoided as much as possible. These are critical times in the baby's growth and development and it's simply wise to avoid exposing the mother to procedures that could in any way influence the baby's growth and development. However, routine dental care can be received during the second trimester. All elective dental procedures should be postponed until after the delivery. During pregnancy, it is imperative to get regular periodontal (gum) exams because pregnancy causes hormonal changes that put you at increased risk for periodontal disease and for tender gums that bleed easily – a condition called pregnancy gingivitis. 



 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

          

		</div>





</asp:Content>
