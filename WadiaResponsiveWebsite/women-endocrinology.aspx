﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-endocrinology.aspx.cs" Inherits="WadiaResponsiveWebsite.women_endocrinology" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Endocrinology</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
          This super-speciality of medicine deals with the diagnosis and treatment of diseases related to the endocrine system. The work of an endocrinologist involves the evaluation of a wide variety of symptoms as well as the treatment of diseases arising from a deficiency or excess of one or more hormones. Doctors at our hospital are trained reproductive endocrinologists who deal primarily with problems of fertility and menstrual functions.
<br /><br />
                
                <b>Some of the medical conditions the department handles include :</b>

                <br /><br />
                <div style="padding-left:25px">
•	Thyroid and adrenal disease
<br />•	Calcium disorders
<br />•	Polycystic Ovary Syndrome (PCOS)
<br />•	Turner’s syndrome
<br />•	Hypogonadism
<br />•	Metabolic bone disease



 <br />
</div>

                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

        
		</div>




</asp:Content>
