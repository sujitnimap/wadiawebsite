﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-High-risk-pregnancy-clinic.aspx.cs" Inherits="WadiaResponsiveWebsite.women_highriskpregclinic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>High Risk Pregnancy Clinic</h2></div>
                    
                    <br />

<!-- CONTENT -->

<div style="line-height:20px">


    <strong>Overview </strong>
    <br />    <br />



A pregnancy can be termed as high-risk due to the presence of a variety of conditions such as high blood pressure or diabetes, foetal problems including birth defects or genetic conditions; or pregnancy problems, such as preterm labour, bleeding or triplets. Our specialists work hand-in-hand with the patients to carefully evaluate, diagnose and monitor high-risk pregnancies. They help would-be mothers and their families to alleviate their fears and create a stress-free environment during and after the pregnancy.

<br /><br />
<b>Services Provided:</b> 
<br /><br />
    <div style="padding-left:25px">
-	Chorionic Villus Sampling
<br />-	Amniocentesis
<br />-	Percutaneous Umbilical Sampling
<br />-	First Trimester Screening
<br />-	Heparin teaching
<br />-	Insulin teaching


</div>

    <div style="height:90px"></div>



</div>
<!--  CONTENT -->


                
                    
                    
				</div>
			</div>
			




		</div>
</asp:Content>
