﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-liver-kidney-transplant.aspx.cs" Inherits="WadiaResponsiveWebsite.child_liver_kidney_transplant" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="row block05">
   <div class="col-full">
      <div class="wrap-col">
         <div class="heading">
            <h2>Liver & Kidney Transplant  </h2>
         </div>
         <br />
         <!-- ABOUT US CONTENT -->
         <div style="line-height:20px">
            <div class="scrollcontent">
               <div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">
                  <ul class="tabs" data-persist="true">
                     <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
                     <li><a href="#view2">Overview </a></li>
                     <li><a href="#view3">Treatments </a></li>
                      <li><a href="#view4">Our Doctors </a></li>
                  </ul>
                  <div class="scrollbar" id="ex3">
                     <div class="tabcontents">
                        <!---- 1st view end -->
                        <%--  <div id="view1">
                           <p>
                           Paediatric Infectious Diseases, Paediatric Hepatology
                                            <br />
                           
                           </p>
                           
                           </div>--%>
                        <!---- 1st view end ---->
                        <!---- 2nd view start ---->
                        <div id="view2">
                            <b>OVERVIEW</b>
                           <p>HPB (Hepato-Pancreatic-Biliary) surgery is a subspecialty of Pediatric surgery and is specific
                           to congenital disorders (biliary atresia, choledochal cyst) benign and malignant diseases of
                           the liver, pancreas, and bile ducts. These three organs play a vital role in the metabolic
                           function of the body and therefore are vital to the healthy functioning of the individual. Liver
                           transplants, meanwhile, are necessitated for end-stage liver failure that cannot be cured
                           with normal surgical or clinical procedures.
                            </p>
                            <br />
                            <b>WHY CHOOSE US?</b>
<p>The Department of HPB Surgery & Liver Transplant at Bai Jerbai Wadia Hospital for Children, Mumbai provides a full range of consultation and surgical procedures for all types of liver ailments. Our team of expert liver specialists offer comprehensive evaluations to assess liver and liver-related ailments while surgeons perform complex surgical & transplant procedures. We are one of India’s premier liver transplant facilities. </p>
<br />
<b>SURGERIES PERFORMED</b>
<p>The department of Pediatric and Solid Organ Transplantation at Bai Jerbai Wadia Hospital for Children performs all kinds of pediatric hepatobiliary operations, mainly intra-operative cholangiogram for infantile obstructive cholangiopathy, Kasai’s Portoentrostomy for biliary atresia, choledochal cyst excision, excision of liver tumors (benign and malignant) like hepatoblastoma, hepatoma, focal nodular hyperplasia, and mesenchymal hamartoma. These operations are performed laparoscopically as well. </p>
<br />
<b>DIAGNOSIS</b>
<p>Patients with liver or related disorders can exhibit a wide range of symptoms. Some of the common ones include – unintentional weight loss, loss of appetite, yellowing of skin & eyes, excessive fatigue, and others. A physical examination and understanding of the patient’s medical history offers the doctor’s the first insight into the patient’s liver related ailments. </p>

                        </div>
                        <!---- 2nd view end ---->
                        <!---- 3rd view start ---->
                        <div id="view3">
                           <p>
                              Liver transplants are a solution for end-stage liver failures primary caused by chronic liver
                              disease and acute liver failure. In both cases, the liver is fundamentally unable to discharge
                              the various metabolic functions that are vital for the functioning of the body and thereby
                              posing a mortal risk to the patient in case of negligence and non-attendance to the
                              condition.
                              <br /><br />
                              A liver transplant is one of the most complex procedures to be performed and therefore, is
                              considered as a last resort option only. A thorough clinical examination, blood diagnosis, and
                              complete assessment of the patient and his/her past medical condition is undertaken to
                              ascertain the viability of a liver transplant.
                              It is at the end of the study, given the severity of the condition that the doctor may decide
                              to opt for a liver transplant procedure, conditional to - if and when a donor organ becomes
                              available.
                           </p>
                            <br />
                           <b>Types of Liver Transplant
                           </b>
                           <br /><br />
                           <div style="padding-left:30px">
                              -	Living Donor Liver Transplantation
                              <br />-	Mono-segmental Liver Transplant
                              <br />-	Split Liver Transplant
                              <br />-	Auxiliary Liver Transplant
                              <br />-	Swap Liver Transplant
                              <br />-	Domino Liver Transplant
                              <br />-   Cadaver Liver Transplant

                           </div>
                        </div>
                        <!-- 3rd view end ---->
                        <!-- 4th view start --->
                         <div id="view4">
                           
                             <p>Liver transplant and liver care is a highly complex field that requires the expertise of not just an individual but an entire team of highly skilled specialists. At Bai Jerbai Wadia Hospital for Children, Mumbai, we have a multi-disciplinary team of doctors & surgeons consisting of highly trained and experienced personnel deliver the finest liver transplant care in India.</p>
                             <br />
                             <p>Our doctor’s have been trained in some of the most acclaimed institutions and come with an accomplished pedigree of clinical excellence. Having pioneered several treatment and surgical procedures, our doctor’s have insight into some of the most complex liver and liver-related ailments.</p>
                           
                        </div>
                        <!---- 4th view end ---->     
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- ABOUT US CONTENT -->
      </div>
   </div>
</div>

</asp:Content>
