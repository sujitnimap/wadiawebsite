﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class cme_registration_form_women : System.Web.UI.Page
    {        
        IList<CMERegistrationMaster> cmeregistrationmasterdata;

        string flag = "w";
        
        SqlCommand cmd = new SqlCommand();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {   
                BindCME();
            }
        }       

        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {
            CMERegistration objCMERegistration = new CMERegistration();
            COperationStatus os = new COperationStatus();

            if (rdbtnConsultant.Checked)
            {
                objCMERegistration.RegCategory = "Consultant";
            }
            if (rdbtnPGStudent.Checked)
            {
                objCMERegistration.RegCategory = "PG Student";
            }

            objCMERegistration.Name = txtName.Text.ToString();
            objCMERegistration.Designation = txtDesignation.Text.ToString();
            objCMERegistration.Speciality = txtSpeciality.Text.ToString();
            objCMERegistration.Institution = txtInstitution.Text.ToString();
            objCMERegistration.Address = txtAddress.Text.ToString();
            objCMERegistration.EmailID = txtEmailId.Text.ToString();
            objCMERegistration.MobNo = txtMobileNo.Text.ToString();
            objCMERegistration.MCIRegNo = txtMCIRegNo.Text.ToString();
            objCMERegistration.StateCouncilNo = txtStateCouncilRegNo.Text.ToString();
            objCMERegistration.FromCategory = "Maternity care";
            objCMERegistration.LastModify = DateTime.Now;

            if (hdfTotalPayableAmount.Value.ToString() == "Not applicable")
            {
                objCMERegistration.TotalAmount = 0;
            }
            else
            {
                objCMERegistration.TotalAmount = int.Parse(hdfTotalPayableAmount.Value.ToString());
            }
            objCMERegistration.PaymentCategory = ""; //ddlPaymentCategory.SelectedValue.ToString();

            os = CCMERegistration.Instance().InsertCMERegisteredDetail(objCMERegistration);

            if (os.Success == true)
            {
                Response.Redirect("CMEPayURequest.aspx?Id=" + objCMERegistration.CMEId);
            }
        }

        protected void cv_RegistrationCategory(object source, ServerValidateEventArgs args)
        {
            args.IsValid = rdbtnConsultant.Checked || rdbtnPGStudent.Checked;
        }        

        public void BindCME()
        {
            cmeregistrationmasterdata = CCMERegistrationMaster.Instance().GetCMERegistrationMasterDataByFeesCategory(flag, "Early Bird");
            DataTable detailTable = ToDataTable(cmeregistrationmasterdata);
            hdfEarlyBirdLastDate.Value = (detailTable.Rows[0]["LastRegDate"]).ToString();
            hdfEarlyBirdConsulatant.Value = (detailTable.Rows[0]["ConsultantFees"]).ToString();
            hdfEarlyBirdPGStudent.Value = (detailTable.Rows[0]["PgStudentFees"]).ToString();

            cmeregistrationmasterdata = CCMERegistrationMaster.Instance().GetCMERegistrationMasterDataByFeesCategory(flag, "Regular");
            DataTable dtDetail = ToDataTable(cmeregistrationmasterdata);
            hdfRegularLastDate.Value = (dtDetail.Rows[0]["LastRegDate"]).ToString();
            hdfRegularConsulatant.Value = (dtDetail.Rows[0]["ConsultantFees"]).ToString();
            hdfRegularPGStudent.Value = (dtDetail.Rows[0]["PgStudentFees"]).ToString();

            cmeregistrationmasterdata = CCMERegistrationMaster.Instance().GetCMERegistrationMasterDataByFeesCategory(flag, "Late");
            DataTable dtDetailLate = ToDataTable(cmeregistrationmasterdata);
            hdfLateLastDate.Value = (dtDetailLate.Rows[0]["LastRegDate"]).ToString();
            hdfLateConsulatant.Value = (dtDetailLate.Rows[0]["ConsultantFees"]).ToString();
            hdfLatePGStudent.Value = (dtDetailLate.Rows[0]["PgStudentFees"]).ToString();

        }
    }
}