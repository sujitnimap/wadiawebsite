﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drshakuntla.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drshakuntla" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row block05">
			
		<div class="col-full">
            <div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Shakuntala Prabhu.jpg" />
<div class="docdetails">
Dr. Shakuntala Prabhu
<div class="docdetailsdesg">
Professor
    <br />
MD, DCH, FRCPCH



</div>

</div>
</div>

                    <div class="areaofinterest">


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
           <%-- <li><a href="#view2">Achievements</a></li>--%>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
       <%--     <div id="view2">
         
                <p>
    •	Hargobind Medical Foundation Scholarship for year 2000 -2001 

<br />
•	J.N.Tata Scholarship for higher education Year 2000-2001.



<br /><br />

                </p>
                  
            </div>--%>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
•	Life member, Indian Academy of Paediatrics. 
<br />•	Life member, Paediatric Cardiac Society of India
<br />•	Life member, Computer Chapter, Indian Academy of Paediatrics.
<br />•	Life member, Haemato- Oncology chapter, Indian Academy of Paediatrics.
<br />•	Reviewer in Paediatric Cardiology section of Indian Pediatrics & IJPP (National Journals)
<br />•	Reviewer of Annals of Paediatric Cardiology  
<br />•	Reviewer of Journal Minerva Cardioangiologica. –International Publication
<br />•	Editorial board -  Paediatric Cardiology for “Paediatrics today .”


    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Prabhu SS, Colaco MP, Pradhan MR, Johari AN, Devany G. Massive osteolysis -Gorham’s syndrome. Indian Pediatr 1994;31:1542–1544. [PubMed]
<br /><br />•	S.S Prabhu, N.C.Joshi. Panic Attack Syndrome. Indian Pediatr 1996;33: 409-411.
<br /><br />•	Prabhu SS, Dalvi BV. Treatable cardiomyopathies. Indian J Pediatr. 2000 Apr;67(4):279-82.
<br /><br />•	Shakuntala Prabhu , S.A. Kolhapure. Evaluation of efficacy and safety of Himcospaz drops in infantile abdominal colic. The Antiseptic 2004; 101(8), 312-317. 
<br /><br />•	Shakuntala Prabhu , Sumitra Venkatesh. Acute myocarditis . IJPP .2008; 10(2) : 155
<br /><br />•	SS Prabhu, Sumitra Venkatesh , Bharat V Dalvi.  Non Surgical Treatment of Congenital Heart Disease. Post Graduate Medicine - Contents 2004 .
Chapter 25. Available  at - www.apiindia.org/contents_2004.html
<br /><br />•	Shakuntala Prabhu, Vaishnavi Iyengar, Sumitra Venkatesh, SnehalKulkarni, Uma A . Cardiovascular Profile in children with Chronic Kidney Disease . Cardiovascular  Journal Of Africa  .January/February 2013 ,Vol 24(1) ;152.
<br /><br />•	Shakuntala Prabhu, Sumitra Venkatesh, Anand Ranagol, Snehal. Kulkarni  Myocardtis in children –spectrum of presentation and outcome” abstract, CardioVascular Journal of Africa (official journal for PASCAR)  , Jan/Feb  2013,VOL 24 (1),164
<br /><br />•	Shakuntala S Prabhu, Sumitra Venkatesh, Nandkumar Kene. Cardiac status in Hypocalcemic infants .” in the proceedings of  Fifth world congress of paediatric cardiology and cardiac surgery , Cardiology in the Young, Volume 20, Issue S1, April 2010, pp 1-422
<br /><br />•	Shakuntala S Prabhu, Panchali Datta, Sumitra Venkatesh “Planned teaching in the care of children with congenital heart diseases .” in the proceedings of Fifth world congress of paediatric cardiology and cardiac surgery Cardiology in the Young, Volume 20, Issue S1, April 2010, pp 1-422 

                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
         </div>
</asp:Content>
