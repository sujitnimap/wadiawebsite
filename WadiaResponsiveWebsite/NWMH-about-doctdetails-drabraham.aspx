﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-about-doctdetails-drabraham.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_doctdetails_drabraham" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Abraham Isaac.png" />
<div class="docdetails">
Dr.  Abraham Isaac


<div class="docdetailsdesg">
BDS
</div>

</div>
</div>

     <div class="areaofinterest">

Areas of Interest : Aesthetic & cosmetic dentistry, Implantology, Surgical impaction and Full Mouth Rehabilitation.


</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Affiliations</a></li>
            <li><a href="#view3">Achievements</a></li>
       
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
   •	Vice-President, Indian Dental Association, Thane Branch, 2013- 2015.                         
<br />
                •	Representative to Council For Continuing Dental Health, Indian Dental Association, Thane Branch, 2005- 2008

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
   •	Certificate Course In Minor Oral Surgeries  (Auckland, New Zealand)
<br />•	Continuing Education Recognition Program ,Dental Tribune America, LLC , NEW YORK
<br />•	Professional Implant Training Course from Osstem Implant Research & Education Centre, Mumbai
<br />•	Fellow ship in Administrative and General Education, Manipal
<br />•	Certificate course in ‘’Amazing Aesthetics ‘’ Hypnotizing Concept II
<br />•	Certificate course in Management of Complications and Failures in Endodontics .
<br /><br />
•	Certificate course in Surgical & Non Surgical Periodontics.
<br />•	Certificate course in Management of Bypass / Angioplasty Treated Cardiac Patients By The Dentist.
<br />•	Certificate course in Tooth or Implant, Smile Anyway AND Predictable Esthetic Cervical And Posterior Restorations.

<br /><br />
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
    
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			


	</div>



</asp:Content>
