﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Services-Gynaecology.aspx.cs" Inherits="WadiaResponsiveWebsite.women_gengynaecology" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Gynaecology</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
              •	<b>OPD</b> : The gynaecology OPD (Out-Patient Department) provides complete evaluations and treatments of all gynaecological conditions, on a daily basis. All major and minor gynaecological operations are performed here. The hospital also has facilities for carrying out Pap smears, to screen the cervical canal, and colposcopies, an advanced method used to screen for cervical cancer.


                <br /><br />


•	<b>IPD</b> :  The gynaecology IPD (In-Patient Department) caters to women who need to be admitted to the hospital overnight or for an undetermined period of time, for either surgery, treatment or observation for all gynaecology related issues that they may be facing. 

                <br /><br />
<b>Services Provided: </b>
                                <br /><br />


-	Pap smears (normal and abnormal)
 <br />-	Colposcopy
 <br />-	Contraceptive management
 <br />-	Endometriosis
 <br />-	Management of abnormal uterine bleeding
 <br />-	Menopause and osteoporosis
 <br />-	Minimally invasive surgery
 <br />-	Paediatric and adolescent gynaecology
 <br />-	Pelvic relaxation
 <br />-	Premenstrual syndrome
 <br />-	Psycho-sexual problems
 <br />-	Sexually transmitted infections
 <br />-	Ultrasonography
 <br />-	Urinary incontinence
-	Urogynaecology


 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

         

		</div>
</asp:Content>
