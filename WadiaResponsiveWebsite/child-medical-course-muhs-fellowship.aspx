﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-medical-course-muhs-fellowship.aspx.cs" Inherits="WadiaResponsiveWebsite.child_medical_course_muhs_fellowship" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .title-font {
            font-family: salsa, sans-serif;
            font-size: 18px;
            color: skyblue;
            text-decoration: underline;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-2-3">
        <div class="wrap-col">
            <div class="heading">
            <h2 class="title-font">COURSES AVAILABLE</h2>
            </div>

            <div class="devider_20px"></div>

            <%--Accordion-Start--%>
                <div class="accordion" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            1 Fellowship Course in Pediatric Neurology & Epilepsy
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY/Annexure%20A%20FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY/Annexure%20B%20FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY/Annexure%20C%20FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY/Annexure%20D%20FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY/Annexure%20E%20FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY/Annexure%20F%20FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY/Annexure%20G%20FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY/Annexure%20H%20FC%20IN%20PEDIATRIC%20NEUROLOGY%20&%20EPILEPSY.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            2 Fellowship Course in Pediatric Gastroentrology, Hepatology & Nutrition
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION/Annexure%20A%20FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION/Annexure%20B%20FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION/Annexure%20C%20FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION/Annexure%20D%20FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION/Annexure%20E%20FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION/Annexure%20F%20FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION/Annexure%20G%20FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION/Annexure%20H%20FC%20IN%20PEDIATRIC%20GASTROENTOLOGY,%20HEPATOLOGY%20&%20NUTRITION.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                            3 Fellowship Course in Pediatric Endocrinology
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY/Annexure%20A%20FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY/Annexure%20B%20FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY/Annexure%20C%20FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY/Annexure%20D%20FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY/Annexure%20E%20FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY/Annexure%20F%20FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY/Annexure%20G%20FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY/Annexure%20H%20FC%20IN%20PEDIATRIC%20ENDOCRINOLOGY.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingFour">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                            4 Fellowship Course in Pediatric Nephrology
                            </button>
                        </h2>
                        <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEPHROLOGY/Annexure%20A%20FC%20IN%20PEDIATRIC%20NEPHROLOGY.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEPHROLOGY/Annexure%20B%20FC%20IN%20PEDIATRIC%20NEPHROLOGY.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEPHROLOGY/Annexure%20C%20FC%20IN%20PEDIATRIC%20NEPHROLOGY.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEPHROLOGY/Annexure%20D%20FC%20IN%20PEDIATRIC%20NEPHROLOGY.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEPHROLOGY/Annexure%20E%20FC%20IN%20PEDIATRIC%20NEPHROLOGY.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEPHROLOGY/Annexure%20F%20FC%20IN%20PEDIATRIC%20NEPHROLOGY.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEPHROLOGY/Annexure%20G%20FC%20IN%20PEDIATRIC%20NEPHROLOGY.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEPHROLOGY/Annexure%20H%20FC%20IN%20PEDIATRIC%20NEPHROLOGY.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingFive">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                            5 Fellowship Course in Neonatology
                            </button>
                        </h2>
                        <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20NEONATOLOGY/Annexure%20A%20FC%20IN%20NEONATOLOGY.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20NEONATOLOGY/Annexure%20B%20FC%20IN%20NEONATOLOGY.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20NEONATOLOGY/Annexure%20C%20FC%20IN%20NEONATOLOGY.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20NEONATOLOGY/Annexure%20D%20FC%20IN%20NEONATOLOGY.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20NEONATOLOGY/Annexure%20E%20FC%20IN%20NEONATOLOGY.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20NEONATOLOGY/Annexure%20F%20FC%20IN%20NEONATOLOGY.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20NEONATOLOGY/Annexure%20G%20FC%20IN%20NEONATOLOGY.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20NEONATOLOGY/Annexure%20H%20FC%20IN%20NEONATOLOGY.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingSix">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                            6 Fellowship Course in Pediatric Immunology
                            </button>
                        </h2>
                        <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20IMMUNOLOGY/Annexure%20A%20FC%20IN%20PEDIATRIC%20IMMUNOLOGY.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20IMMUNOLOGY/Annexure%20B%20FC%20IN%20PEDIATRIC%20IMMUNOLOGY.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20IMMUNOLOGY/Annexure%20C%20FC%20IN%20PEDIATRIC%20IMMUNOLOGY.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20IMMUNOLOGY/Annexure%20D%20FC%20IN%20PEDIATRIC%20IMMUNOLOGY.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20IMMUNOLOGY/Annexure%20E%20FC%20IN%20PEDIATRIC%20IMMUNOLOGY.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20IMMUNOLOGY/Annexure%20F%20FC%20IN%20PEDIATRIC%20IMMUNOLOGY.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20IMMUNOLOGY/Annexure%20G%20FC%20IN%20PEDIATRIC%20IMMUNOLOGY.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20IMMUNOLOGY/Annexure%20H%20FC%20IN%20PEDIATRIC%20IMMUNOLOGY.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingSeven">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                            7 Fellowship Course in Pediatric Orthopedics
                            </button>
                        </h2>
                        <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ORTHOIPEDICS/Annexure%20A%20FC%20IN%20PEDIATRIC%20ORTHOIPEDICS.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ORTHOIPEDICS/Annexure%20B%20FC%20IN%20PEDIATRIC%20ORTHOIPEDICS.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ORTHOIPEDICS/Annexure%20C%20FC%20IN%20PEDIATRIC%20ORTHOIPEDICS.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ORTHOIPEDICS/Annexure%20D%20FC%20IN%20PEDIATRIC%20ORTHOIPEDICS.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ORTHOIPEDICS/Annexure%20E%20FC%20IN%20PEDIATRIC%20ORTHOIPEDICS.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ORTHOIPEDICS/Annexure%20F%20FC%20IN%20PEDIATRIC%20ORTHOIPEDICS.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ORTHOIPEDICS/Annexure%20G%20FC%20IN%20PEDIATRIC%20ORTHOIPEDICS.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20ORTHOIPEDICS/Annexure%20H%20FC%20IN%20PEDIATRIC%20ORTHOIPEDICS.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingEight">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                            8 Fellowship Course in Pediatric Neurosurgery
                            </button>
                        </h2>
                        <div id="collapseEight" class="accordion-collapse collapse" aria-labelledby="headingEight" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROSURGERY/Annexure%20A%20FC%20IN%20PEDIATRIC%20NEUROSURGERY.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROSURGERY/Annexure%20B%20FC%20IN%20PEDIATRIC%20NEUROSURGERY.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROSURGERY/Annexure%20C%20FC%20IN%20PEDIATRIC%20NEUROSURGERY.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROSURGERY/Annexure%20D%20FC%20IN%20PEDIATRIC%20NEUROSURGERY.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROSURGERY/Annexure%20E%20FC%20IN%20PEDIATRIC%20NEUROSURGERY.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROSURGERY/Annexure%20F%20FC%20IN%20PEDIATRIC%20NEUROSURGERY.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROSURGERY/Annexure%20G%20FC%20IN%20PEDIATRIC%20NEUROSURGERY.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20NEUROSURGERY/Annexure%20H%20FC%20IN%20PEDIATRIC%20NEUROSURGERY.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingNine">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseNine" aria-expanded="true" aria-controls="collapseNine">
                            9 Fellowship Course in Pediatric Urology
                            </button>
                        </h2>
                        <div id="collapseNine" class="accordion-collapse collapse" aria-labelledby="headingNine" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20UROLOGY/Annexure%20A%20FC%20IN%20PEDIATRIC%20UROLOGY.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20UROLOGY/Annexure%20B%20FC%20IN%20PEDIATRIC%20UROLOGY.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20UROLOGY/Annexure%20C%20FC%20IN%20PEDIATRIC%20UROLOGY.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20UROLOGY/Annexure%20D%20FC%20IN%20PEDIATRIC%20UROLOGY.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20UROLOGY/Annexure%20E%20FC%20IN%20PEDIATRIC%20UROLOGY.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20UROLOGY/Annexure%20F%20FC%20IN%20PEDIATRIC%20UROLOGY.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20UROLOGY/Annexure%20G%20FC%20IN%20PEDIATRIC%20UROLOGY.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20UROLOGY/Annexure%20H%20FC%20IN%20PEDIATRIC%20UROLOGY.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTen">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTen" aria-expanded="true" aria-controls="collapseTen">
                            10 Fellowship Course in Pediatric Surgical-Oncology
                            </button>
                        </h2>
                        <div id="collapseTen" class="accordion-collapse collapse" aria-labelledby="headingTen" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY/Annexure%20A%20FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY.pdf">ANNEXURE “A” - Professional Teaching Experience Certificate for Fellowship/Certificate Courses Director/Mentor</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY/Annexure%20B%20FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY.pdf">ANNEXURE “B” - INSTITUTIONAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY/Annexure%20C%20FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY.pdf">ANNEXURE “C” - HOSPITAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY/Annexure%20D%20FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY.pdf">ANNEXURE “D” - DEPARTMENTAL INFORMATION</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY/Annexure%20E%20FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY.pdf">ANNEXURE “E” - Information of Director of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY/Annexure%20F%20FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY.pdf">ANNEXURE “F” - Information of Mentor of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY/Annexure%20G%20FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY.pdf">ANNEXURE “G” - Information of Co-ordinator of Training Centre</a>
                                    </li>
                                    <li class="mt-2">
                                        <a href="LIC%20MUHS/bjwhc-%20MUHS%20Fellowship%20(Mandate)/FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY/Annexure%20H%20FC%20IN%20PEDIATRIC%20SURGICAL%20ONCOLOGY.pdf">ANNEXURE “H” - DECLARATION</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <%--Accordion-End--%>

        </div>
    </div>

    <div class="col-1-3">
            <div class="wrap-col">

                <div class="box">
                    <div class="heading">
                        <h2 class="title-font">MEDICAL</h2>
                    </div>
                    <div class="content">
                        <div class="list">
                            <ul>


                                <li><a href="child-medical-overview.aspx">Overview  </a></li>
                                <li><a href="child-medical-directormsg.aspx">Directors message </a></li>
                                <li><a href="child-medical-courses.aspx">Courses available</a></li>
                                <li><a href="child-medical-faculty.aspx">Faculty </a></li>
                                <li><a href="child-contact-us.aspx">Contact Us  </a></li>


                                <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									<%--</li>--%>
									
                            </ul>
                        </div>
                    </div>
                </div>
                <%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
            </div>
        </div>

</asp:Content>
