﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace WadiaResponsiveWebsite
{
    public partial class women_about_awards : System.Web.UI.Page
    {
        static string flag = "w";

        string Categorynm = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            lblmessage.Visible = false;

            if (!string.IsNullOrEmpty(Request.QueryString["catnm"]))
            {
                ViewState["catnm"] = Convert.ToString(Request.QueryString["catnm"]);

                Categorynm = ViewState["catnm"].ToString();
            }


            if (!IsPostBack)
            {
                if (Categorynm == "")
                {
                    getawardsdata(1, "All");
                    Categorynm = "All";
                }
                else
                {
                    getawardsdata(1, Categorynm);
                }
            }

        }

        public void getawardsdata(int currentPage, string category)
        {
            int pageSize = 10;
            int _TotalRowCount = 0;

            string _ConStr = ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(_ConStr))
            {

                SqlCommand cmd = new SqlCommand("sp_GetAwarsddata", con);
                cmd.CommandType = CommandType.StoredProcedure;

                int startRowNumber = ((currentPage - 1) * pageSize) + 1;

                cmd.Parameters.AddWithValue("@StartIndex", startRowNumber);
                cmd.Parameters.AddWithValue("@PageSize", pageSize);
                cmd.Parameters.AddWithValue("@flag", flag);
                cmd.Parameters.AddWithValue("@categorynm", category);

                SqlParameter parTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                parTotalCount.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parTotalCount);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                _TotalRowCount = Convert.ToInt32(parTotalCount.Value);

                dlistawrdsdata.DataSource = ds;
                dlistawrdsdata.DataBind();

                if (ds.Tables[0].Rows.Count == 0)
                {
                    pnlawrds.Visible = false;

                    lblmessage.Text = "Currently, there is no awards related activities at Wadia Hospitals";
                    lblmessage.ForeColor = Color.Red;
                    lblmessage.Visible = true;
                }

                generatePager(_TotalRowCount, pageSize, currentPage);

            }
        }

        public void generatePager(int totalRowCount, int pageSize, int currentPage)
        {
            int totalLinkInPage = 5;
            int totalPageCount = (int)Math.Ceiling((decimal)totalRowCount / pageSize);

            int startPageLink = Math.Max(currentPage - (int)Math.Floor((decimal)totalLinkInPage / 2), 1);
            int lastPageLink = Math.Min(startPageLink + totalLinkInPage - 1, totalPageCount);

            if ((startPageLink + totalLinkInPage - 1) > totalPageCount)
            {
                lastPageLink = Math.Min(currentPage + (int)Math.Floor((decimal)totalLinkInPage / 2), totalPageCount);
                startPageLink = Math.Max(lastPageLink - totalLinkInPage + 1, 1);
            }

            List<ListItem> pageLinkContainer = new List<ListItem>();

            if (startPageLink != 1)
                pageLinkContainer.Add(new ListItem("First", "1", currentPage != 1));
            for (int i = startPageLink; i <= lastPageLink; i++)
            {
                pageLinkContainer.Add(new ListItem(i.ToString(), i.ToString(), currentPage != i));
            }
            //if (lastPageLink != totalPageCount)
            //    pageLinkContainer.Add(new ListItem("Last", totalPageCount.ToString(), currentPage != totalPageCount));

            dlPagerup.DataSource = pageLinkContainer;
            dlPagerup.DataBind();

            dlPagerdown.DataSource = pageLinkContainer;
            dlPagerdown.DataBind();
        }


        protected void dlPagerup_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "PageNo")
            {
                getawardsdata(Convert.ToInt32(e.CommandArgument), Categorynm);
            }
        }

        protected void dlPagerdown_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "PageNo")
            {
                getawardsdata(Convert.ToInt32(e.CommandArgument), Categorynm);
            }
        }

        protected void lnbtnDoctor_Click(object sender, EventArgs e)
        {
            getawardsdata(1, Categorynm);
        }

        protected void lnbtnManagement_Click(object sender, EventArgs e)
        {
            getawardsdata(1, Categorynm);
        }

        
    }
}