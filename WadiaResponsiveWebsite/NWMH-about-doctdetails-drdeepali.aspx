﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-about-doctdetails-drdeepali.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_doctdetails_drdeepali" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/dr.jpg" />
<div class="docdetails">
Dr. Deepali Prakash Kale




<div class="docdetailsdesg">
Assistant Professor 
    <br />
MBBS, DGO (Muhs), DGO (Cps), FCPS, DNB


</div>

</div>
</div>

     <div class="areaofinterest">

Areas of Interest : High risk pregnancy, Pelvic surgeries, Endoscopy
</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
        <%--    <li><a href="#view2">Achievements</a></li>--%>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research and publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
    Member of Mumbai Obstetrics and gynaecology society and FOGSI since 7 years<br /><br />
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Chapter on “Do’s and Don’ts in Caesarean section”	Textbook “ DO’S and Don’ts in Obstetrics and gynaecology “by Dr.Tushar Kar
<br />•	Paper on “Obstetric outcome in elderly primigravidae. How did they fare?” in BHJ Vol53, no 4
<br />•	Article on “Surgery and anaesthesia in first trimester of pregnancy “in FOGSI, FOCUS “FIRST TRIMESTER OF PREGNANCY”
<br /><br />
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			









		</div>









</asp:Content>
