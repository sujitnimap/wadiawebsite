﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class child_gallary_category : System.Web.UI.Page
    {

        IList<category> catdata;

        static string flag = "c";
        static string type = "Image";



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getcategory();
            }

        }

        public void getcategory()
        {

            catdata = CCategory.Instance().GetCategoryData(flag, type);

            DataTable dt = CollectionHelper.GetDataTable(catdata);

            dlistchildcategory.DataSource = dt;
            dlistchildcategory.DataBind();

        }


        protected void imgbtnthumnail_Click(object sender, ImageClickEventArgs e)
        {

            ImageButton button = (sender as ImageButton);

            //Get the command argument
            string commandArgument = button.CommandArgument;

            Session["categoryid"] = commandArgument;

            //DataTable albumdata = CArtgallary.Instance().GetArtgallarygridDatabycategory(flag, commandArgument);

            Response.Redirect("child-gallery-album.aspx?id=" + commandArgument);

        }
    }
}