﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WadiaResponsiveWebsite
{
    public partial class women_contact_us : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void imgbtncontact_Click(object sender, ImageClickEventArgs e)
        {


            COperationStatus os = new COperationStatus();

            try
            {
                if (Session["RandomString"].ToString() == tb_Code.Text.Trim())
                {

                    string strMailUserName = ConfigurationManager.AppSettings["SEND_TO_ADDR"];

                    CollectionHelper.sendMail(strMailUserName, ddlquerytype.SelectedItem.Text, "Dear Sir/Madam ,", ddlnameprefix.SelectedItem.Text + " " + txtfname.Text + " " + txtlname.Text + "<br>Email-Id : " + txtemailid.Text, txtcomments.Text, txtcontactno.Text);

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Thank you.');");
                    sb.Append("$('#editModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);


                    txtfname.Text = string.Empty;
                    txtemailid.Text = string.Empty;
                    txtcontactno.Text = string.Empty;
                    txtcomments.Text = string.Empty;
                    tb_Code.Text = string.Empty;
                }
                else
                {

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Enter valid capcha code.');");
                    sb.Append("$('#editModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);

                }
            }
            catch (Exception ex)
            {

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + ex.Message + "');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);

            }

        }


        protected void lbRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                imgVerificationCode.ImageUrl = "capcha.aspx";
                if (iRandomImageID == 2)
                {
                    imgVerificationCode.ImageUrl = "capcha.aspx";
                    iRandomImageID = 0;
                }
                else
                {
                    imgVerificationCode.ImageUrl = "capcha1.aspx";
                    iRandomImageID = 2;
                }

                tb_Code.Focus();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message + ex.StackTrace);
            }
        }

        private int iRandomImageID
        {
            get
            {
                if (ViewState["randomImgID"] == null)
                {
                    return 0;
                }
                else
                {
                    return (int)ViewState["randomImgID"];
                }
            }
            set
            {
                ViewState["randomImgID"] = value;
            }
        }
    }
}