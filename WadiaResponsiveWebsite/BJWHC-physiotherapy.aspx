﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-physiotherapy.aspx.cs" Inherits="WadiaResponsiveWebsite.child_physiotherapy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Physiotherapy and occupational therapy  </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Physiotherapy </a></li>
            <li><a href="#view3">Occupational </a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
The Paediatric Physiotherapy department is well known for its excellent facilities and services offered. The department conducts an OPD clinic daily for patients referred from other departments within the hospital such as Paediatric Medicine, Paediatric Orthopaedic and the Paediatric Surgery Department.
                <br /><br />



The Department offers a wide range of services such as:
                <br /><br />
<div style="padding-left:30px">
-	HRO and Early Intervention: Patients discharged from NICU of both the BJWHC and the NWMH are screened by a physiotherapist and a neonatologist. During the screening process, babies displaying delayed development are administered an Early Intervention Programme. 
<br /><br />
-	Chest Physiotherapy : Patients from the Medical and Surgical wards as well as ICUs having Cardio-Respiratory pathology are treated to improve lung function. 
<br /><br />
-	Urinary and Anal Incontinence : In collaboration with the Paediatric Surgery Department, patients with Urinary and Anal incontinence are treated with non-invasive techniques such as Faradic Stimulation and Perineal Exercises.
</div>
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
 Occupational Therapy is to develop, recover, or maintain the daily living and work skills of people with a physical, mental or developmental disability. 
 Occupational therapy interventions focus on adapting the environment, modifying the task, teaching the skill, and educating the client/family in order to 
                increase participation in and performance of daily activities, particularly those that are meaningful to the client.
Patients are often referred for Occupational Therapy from the Paediatric Orthopaedic, Paediatric Medicine, Paediatric Neurology and Paediatric Surgery 
                Departments. 
<br /><br />

Occupational Therapists use various procedures such as:
<br /><br />
                <div style="padding-left:30px">
-	Neuromuscular Development
<br />-	Sensory Integrative
<br />-	Priomuscular techniques

</div>


                
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

           

		</div>




</asp:Content>
