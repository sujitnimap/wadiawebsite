﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-nursing-school.aspx.cs" Inherits="WadiaResponsiveWebsite.women_nursing_school" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                     <div class="heading">
                  <h2>NURSING Department</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->


<div style="line-height:20px">



At our Nursing School, our goal is to provide extensive clinical experiences for students from nursing institutes affiliated to us. Students work closely with our highly skilled medical and paramedical staff for a holistic understanding of healthcare for children, thus stimulating a desire to pursue advanced studies in the field of Obstetric and Gynaecological Nursing.  

</br></br>

Recognized by the Maharashtra Nursing Council, the department is also affiliated with different hospitals in Mumbai for Obstetric and Gynaecological training. We provide a comprehensive clinical speciality experience to students of M.Sc Nursing, B.Sc Nursing, Post Basic B.Sc Nursing, General Nursing and Midwifery and Auxiliary Midwifery from various institutes. 
</br></br>


<b>The Function of the Department </b>
                    <br /><br />
                    <div style="padding-left:25px">


•	Completion of the clinical experience as per the requirements of the University / Council 

                        </br></br>

•	Completion of Internal Assessment as per requirements 
</br></br>
•	Conducting internal assessment as well as University / Council practical examinations 
</br></br>
•	Organizing educational visits for various groups of students, to see our hospital’s in-house and out-reach services 
</br></br>
•	Participating in health-education programmes and extra-curricular activities in our hospital
</br></br>
•	Placement, orientation, rotation, supervision and guidance for M.Sc, B.Sc, PBSc, GNM and ANM students in clinical areas 
</br></br>
•	Regular Continuous Nursing Education and in-service education for the trained nurses of our hospital 


</div>






</div>

<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>Nursing</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						  <li><a href="women-nursing-overview.aspx">	Overview  </a></li>
                        <li><a href="women-nursing-school.aspx"> 	Nursing Department</a></li>
                        <li><a href="women-nursing-courses.aspx"> 	Courses available</a></li>
                         <li><a href="women-contact-us.aspx">	Contact Us  </a></li>


                                     <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									</li>
									
								</ul>
							</div>
						</div>
					</div>
			



				</div>
			</div>



		</div>






</asp:Content>
