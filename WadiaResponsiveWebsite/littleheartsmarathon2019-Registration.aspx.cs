﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class littleheartsmarathon2019_Registration : System.Web.UI.Page
    {
        int totalAmount = 500;
        protected void Page_Load(object sender, EventArgs e)
        {
            txtDOB.Attributes.Add("readonly", "readonly");
        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {
            /*Below code is commented by prashant for testing*/
            
            int registrationForMarathonID;
            int noOfChildrens = 1;
            int noOfAdults;
            string emailAddress;
            string confirmEmailAddress;
            string mobileNumber1;
            string mobileNumber2;
            string address;
            string city;
            string state;
            string country;
            string nationality;
            string additionalInformation;
            bool isSponsorARun;
            int noOfSponsorARun;
            int totalAmount;
            string knowAboutMarathon;
            int tshirtquantityparent;
            string tshirtsize1;
            string tshirtsize2;
            string tshirtsize3;
            string tshirtsize4;
            string tshirtsize5;

            noOfChildrens = 1;

            //  noOfAdults = cb_EnrollSecondParent.Checked ? 2 : noOfAdults;

            emailAddress = txtParentEmailId.Text.Trim();
            mobileNumber1 = txtContactNo.Text.Trim();
            //mobileNumber2 = txtSecondParentMobileNo.Text.Trim();
            address = txtAddress.Text.Trim();
            city = txtCity.Text.Trim();
            state = txtState.Text.Trim();
            country = ddlCountrys.SelectedValue;
            nationality = ddlNationalitys.SelectedValue;
            additionalInformation = txtAnyOtherRelevantInformation.Text.Trim();
            isSponsorARun = chkIsSponsorARun.Checked;
            noOfSponsorARun = chkIsSponsorARun.Checked ? 1 : 0;
            totalAmount = Convert.ToInt32(hdn_totalAmount.Value);
            knowAboutMarathon = ddlKnowAboutMarathon.SelectedValue;
            tshirtquantityparent = Convert.ToInt32(ddlTshirtforParentQuantity.SelectedValue);
            tshirtsize1 = ddlTshirtSizeParent1.SelectedValue;
            tshirtsize2 = ddlTshirtSizeParent2.SelectedValue;
            tshirtsize3 = ddlTshirtSizeParent3.SelectedValue;
            tshirtsize4 = ddlTshirtSizeParent4.SelectedValue;
            tshirtsize5 = ddlTshirtSizeParent5.SelectedValue;

            registrationForMarathon objRegistrationForMarathon = new registrationForMarathon();
            COperationStatus os = new COperationStatus();

            objRegistrationForMarathon.Category = ddlCategory.SelectedValue;
            objRegistrationForMarathon.NoOfChildrens = noOfChildrens;
            objRegistrationForMarathon.NoOfAdults = 0;
            objRegistrationForMarathon.EmailAddress = emailAddress;
            objRegistrationForMarathon.ConfirmEmailAddress = emailAddress;
            objRegistrationForMarathon.MobileNumber1 = mobileNumber1;
            objRegistrationForMarathon.MobileNumber2 = null;
            objRegistrationForMarathon.Address = address;
            objRegistrationForMarathon.City = city;
            objRegistrationForMarathon.State = state;
            objRegistrationForMarathon.Country = country;
            objRegistrationForMarathon.Nationality = nationality;
            objRegistrationForMarathon.AdditionalInformation = additionalInformation;
            objRegistrationForMarathon.IsSponsorARun = isSponsorARun;
            objRegistrationForMarathon.NoOfSponsorARun = noOfSponsorARun;
            objRegistrationForMarathon.TotalAmount = totalAmount;
            objRegistrationForMarathon.KnowAboutMarathonBy = knowAboutMarathon;

            os = CRegistrationForMarathon.Instance().InsertRegistrationForMarathonDetail(objRegistrationForMarathon);

            registeredChildForMarathon registeredChildForMarathon = new registeredChildForMarathon();
            registeredChildForMarathon.ChildFirstName = txtChildFirstName.Text;
            registeredChildForMarathon.ChildMiddleName = txtChildMiddleName.Text;
            registeredChildForMarathon.ChildLastName = txtChildLastName.Text;
            registeredChildForMarathon.Age = Convert.ToInt32(ddlChildAge.SelectedValue);
            registeredChildForMarathon.Gender = rbGenderKids.SelectedValue;
            registeredChildForMarathon.TShirtSize = ddlTShirtSizeChild.SelectedValue;
            registeredChildForMarathon.School = txtSchoolName.Text;
            registeredChildForMarathon.DOB = DateTime.Parse(txtDOB.Text, CultureInfo.CreateSpecificCulture("en-US"));
            registeredChildForMarathon.RegistrationForMarathonID = objRegistrationForMarathon.RegistrationForMarathonID;


            os = CRegisteredChildForMarathon.Instance().InsertRegisteredChildForMarathonDetail(registeredChildForMarathon);


            List<registeredAdultForMarathon> selRegisteredAdultForMarathon = (List<registeredAdultForMarathon>)ViewState["registeredAdultForMarathons"];


            registeredAdultForMarathon registerFirstParent = new registeredAdultForMarathon();
            registerFirstParent.AdultName = txtFirstParentName.Text;
            registerFirstParent.Age = 0;
            registerFirstParent.Gender = "";
            registerFirstParent.TShirtSize1 = tshirtsize1;
            registerFirstParent.Organization = "";
            registerFirstParent.QuantityTshirt = tshirtquantityparent;
            registerFirstParent.TShirtSize2 = tshirtsize2;
            registerFirstParent.TShirtSize3 = tshirtsize3;
            registerFirstParent.TShirtSize4 = tshirtsize4;
            registerFirstParent.TShirtSize5 = tshirtsize5;
            registerFirstParent.RegistrationForMarathonID = objRegistrationForMarathon.RegistrationForMarathonID;


            os = CRegisteredAdultForMarathon.Instance().InsertRegisteredAdultForMarathonDetail(registerFirstParent);



            if (os.Success == true)
            {

                Response.Redirect("MarathonPayURequest.aspx?Id=" + objRegistrationForMarathon.RegistrationForMarathonID);              
            }
            
           
            
            
        }
    }
}