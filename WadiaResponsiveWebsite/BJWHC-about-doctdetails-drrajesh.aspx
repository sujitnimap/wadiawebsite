﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drrajesh.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drrajesh" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Rajesh Joshi.jpg" />
<div class="docdetails">
Dr. Rajesh Joshi

<div class="docdetailsdesg">
Professor
    <br />
MD, DCH, DNB, Fellowship in Endocrinology


</div>

</div>
</div>

                    <div class="areaofinterest">

    Areas of Interest : Paediatrics, Paediatric Endocrinology

</div>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
        <%--   <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Research or publications</a></li>
           <%-- <li><a href="#view4"></a></li>
           --%>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
          <%-- <div id="view1">
                
                <p>
      Paediatrics, Paediatric Endocrinology              <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
·      Postgraduate teacher (guide) for post graduate students in Paediatrics (M.D. and D.C.H  courses ) to University of Mumbai.

<br /><br />·      Examiner for MBBS (Paediatrics)

<br /><br />·      In charge of the Changing Diabetes in Children project at Wadia Hospital

<br /><br />·      Played a key role in starting support groups for diabetic and growth hormone deficient patients. 

<br /><br />

                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

<p>
    
·      Study of Precocious Puberty in Children treated with Medroxyprogesterone acetate. R R Joshi, Sudha Rao. Bombay Hospital journal

<br /><br />·      A study of measles antibody levels from birth till 9 months of age: correlation with maternal titres and maternal nutrition. RR Joshi, PS Gambhir.  Bombay Hospital journal

<br /><br />·      Progressive systemic sclerosis. RR Joshi, V Venkat. Bombay Hospital journal

<br /><br />·      Viper bites in children. Rajesh Joshi, V Venkatramani. Bombay Hospital journal

<br /><br />·      Rickettsial infections seen in rural India. Rajesh Joshi, Abhilasha Punde, Alpana Ohri. Bombay Hospital journal

<br /><br />·      Thiamine responsive anaemia, diabetes mellitus and sensorineural deafness(Rogers syndrome) – a case report

<br /><br /><b>Rajesh Joshi, Currhimbhoy zeenat, Sudha Rao. Bombay Hospital journal</b>

<br /><br />·      Vitamin D dependent rickets type II- Case reports. Rajesh Joshi. Bombay Hospital Journal

<br /><br />·      Etiology and clinical profile of ambiguous genitalia an overview of 10 years experience.

<br /><br /><b>Joshi RR, Rao S, Desai M.Indian Pediatr.</b>

<br /><br />·      Polyglandular autoimmune syndrome-type I.

<br /><br />Joshi RR, Rao S, Prabhu SS.Indian Pediatr.

<br /><br />·      Propionic acidemia presenting as diabetic ketoacidosis.

<br /><br /><b>Joshi R, Phatarpekar A.Indian Pediatr.</b>

<br /><br />·      Trypanosomiasis in an infant from India

<br /><br /><b>Shah I, Ali U, Andankar P, Joshi R. J Vector Borne Dis.</b>

<br /><br />·      Pseudohypoparathyroidism type 1B with hypothyroidism. Rajesh Joshi, Muzna Kapdi. India Pediatrics

<br /><br />·      Clinical and etiological profile of refractory rickets from western India. Rajesh Joshi, Shailesh Patil, Sudha Rao.  Indian J Paediatrics

<br /><br />·      OSTEOPETRORICKETS- Osteopetrosis with rickets, a rare paradoxical association –Umesh Kalane, Rajesh Joshi.  The Internet Journal of  Paediatrics and Neonatology

<br /><br />·      Hypercalcemia due to hypervitaminosis D: report of seven patients. Joshi R. J Trop Pediatr.

 

<br /><br />·      Neonatal diabetes mellitus due to L233F mutation in KCNJ11 gene. Joshi R, Phatarpekar A.World J Pediatr.

<br /><br />·      Subacute combined degeneration of the spinal cord in an adolescent girl. Mohanty S, Shobhavat L, Joshi R.J Trop Pediatr.

<br /><br />·      Haemophagocytic lymphohistiocytosis: a case series from Mumbai. Joshi R, Phatarpekar A, Currimbhoy Z, Desai M. Ann Trop Paediatr.

<br /><br />·      Profile of dengue patients admitted to a tertiary care hospital in Mumbai Joshi R, Baid V.  Turk J Pediatr

<br /><br />·       Hypomagnesemia with secondary hypocalcemia due to TRPM6 gene mutation. Rajesh Joshi, Ankur Phatarpekar. Sri Lanka Journal of Child Health

<br /><br />·      Idiopathic pulmonary hemosiderosis- A rare cause of anaemia. Rajesh Joshi, Muznah Kapdi. Sri Lanka Journal of Child Health.

<br /><br />·      Endocrine complications in children with beta thalassemia major- Rajesh Joshi, Ankur Phatarpekar .Sri Lanka Journal of Child Health.

<br /><br />·      Identification of novel mutations in STAR gene in patients with lipoid congenital adrenal hyperplasia: a first report from India. Vasudevan L, Joshi R, Das DK, Rao S, Sanghavi D, Babu S, Tamhankar PM. J Clin Res Pediatr Endocrinol.

 

 

<br /><br />·      Written chapter on “Systemic illness and short stature” in Clinical Paediatric Endocrinology for practicing Paediatricians ( Indian Academy of Paediatrics speciality series)

 

<br /><br />·      Other publications: Article on “Approach to a case with short stature” published in quarterly journal of  Mumbai branch of Indian Academy of Paediatrics.

<br /><br />·      Articles published in Safe Child Care, Manual for General practitioners(Topics: Breast feeding and weaning; Normal diet) and  in manual- Paediatric Clinics for Postgraduates, published during CME for postgraduates(Topics: Paraplegia ; GBS) 




    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			



		</div>
</asp:Content>
