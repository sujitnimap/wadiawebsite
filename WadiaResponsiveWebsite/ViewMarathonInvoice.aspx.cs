﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class ViewMarathonInvoice : System.Web.UI.Page
    {
        public string udf1 = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            udf1 = Request.QueryString["BID"];

            //To check are we getting numeric value in string
            //int valueParsed;
            //if(Int32.TryParse(Request.QueryString["IID"], out valueParsed))
            //{
            //	udf1 = Request.QueryString["CID"];			
            //}


            IList<registrationForMarathon> registForMarathonData = CRegistrationForMarathon.Instance().GetRegistrationForMarathonInfo(Convert.ToInt32(udf1));
            DataTable dt = new DataTable();
            dt = CollectionHelper.GetDataTable(registForMarathonData);


            IList<registeredChildForMarathon> registChildForMarathon = CRegisteredChildForMarathon.Instance().GetRegisteredChildForMarathon(Convert.ToInt32(udf1));
            DataTable dtChild = new DataTable();
            dtChild = CollectionHelper.GetDataTable(registChildForMarathon);


            IList<registeredAdultForMarathon> registAdultForMarathon = CRegisteredAdultForMarathon.Instance().GetRegisteredAdultForMarathon(Convert.ToInt32(udf1));
            DataTable dtAdult = new DataTable();
            dtAdult = CollectionHelper.GetDataTable(registAdultForMarathon);

            IList<paymentDetail> paymentDetailsdata = CPaymentDetail.Instance().GetPaymentDetailInfo(Convert.ToInt32(udf1));

            //IList<paymentDetail> paymentDetail = paymentDetailData.Where(p => p.PaymentDetailID.Equals(Convert.ToInt32(udf1))).ToList();


            #region BodyHeader

            // string body = "Hellow";

            string body = @"<table width='800px' border='1' align='center' cellpadding='0' cellspacing='0'>
           <tr>
							<td valign='top' class='contentbig' style='padding: 20px'>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
							<td width='22%' valign='top'>
							<img src='http://wadiahospitals.org/images/Logo.png' width='100' height='100' />
							</td>
							<td width='50%' valign='top' style='padding-left: 15px'>
							<table width='99%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
							<td height='39'>
							<h2 class='contentheader'>
							Wadia Hospitals</h2>
							</td>
							</tr>
							<tr>
							<td height='78' class='content' valign='top'>
							Bai Jerbai Wadia Hospital for Children,
							<br />
							Acharya Donde Marg, Lower Parel,
							<br />
							Mumbai, Maharashtra,India, 400 012
							</td>
							</tr>
							</table>
							</td>
							<td width='35%' style='padding-left: 15px' valign='top'>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
							<td height='27' class='content'>
							Date
							</td>
							<td>
							<strong>: </strong>
							</td>
							<td>" + DateTime.Now + @"</td>
							</tr>
							</table>
							</td>
							</tr>
							</table>

<table>
<tr>
<td>
<br />
Registration No. : <strong> " + udf1.ToString() + @"</strong>
<br />
Online Registration e-Receipt <strong> " + paymentDetailsdata[0].TransationID + @"</strong> </br>

Received with thanks an amount of Rs." + dt.Rows[0]["TotalAmount"].ToString() + @" towards Participation/Donation/Sponsor a Run, for Little </br>

Little Hearts Marathon 2019 scheduled on 27th January 2019.

</td>
</tr>
</table>
<br />
<p><strong>USER  INFORMATION : </strong><br /></p>
<table width='100%' border='1' cellspacing='0' cellpadding='0'>

<td height='24' colspan='2' class='contenthead' style='background-color: #CCC; padding-left: 5px;'>
Billing Address
</td>
</tr>
<tr>
<td class='content' height='30' style='padding-left: 5px'>
Name of Participant<strong> :</strong>
</td>
<td style='padding-left:10px;font-weight:bold;' width='70%'>" + dtChild.Rows[0]["ChildFirstName"].ToString() + " " + dtChild.Rows[0]["ChildMiddleName"].ToString() + " " + dtChild.Rows[0]["ChildLastName"].ToString() + @"</td>
</tr>
<tr>
<tr>
<td class='content' height='30' style='padding-left: 5px'>
Race Category<strong> :</strong>
</td>
<td style='padding-left:10px;font-weight:bold;' width='70%'>" + dt.Rows[0]["Category"].ToString() + @"</td>
</tr>
<tr>
<td class='content' height='30' style='padding-left: 5px'>
EmailAddress<strong> :</strong>
</td>
<td style='padding-left:10px'>" + dt.Rows[0]["EmailAddress"].ToString() + @"</td>
</tr>
<tr>
<td class='content' height='30' style='padding-left: 5px'>
Address <strong>:</strong>
</td><td style='padding-left:10px' width='70%'>" + dt.Rows[0]["Address"].ToString() + @"</td>
</tr>
<tr>
<td class='content' height='30' style='padding-left: 5px'>
Total Amount <strong>:</strong>
</td>
<td style='padding-left:10px' width='70%'>"
+ dt.Rows[0]["TotalAmount"].ToString() +
@"</td>
</tr>
<tr>
<td class='content' height='30' style='padding-left: 10px'>
Mobile Number <strong>:</strong>
</td><td style='padding-left:10px' width='70%'>" + dt.Rows[0]["MobileNumber1"].ToString() + @"</td>
</tr>

</table>

<br>

<strong> DETAILS OF EVENT : </strong>
<br>

<table>
<tr>
</br>

<strong>Date </strong> : 27 th January, 2019.
</br>
<strong>Reporting Time:</strong> 7.00 am.
</br>
<strong>Race Time:</strong> 7:30 am.
</br>
<strong>Starting venue</strong>:  M.M.R.D.A ground, BKC, Mumbai – 400051.

</br>
 <br />
<strong> Terms, conditions and guidelines : </strong>
</br>
</br>
 Please note Last date of registration is 24 th January 2019 till midnight subject to availability. The
organizer reserves the right to change the dates.
</td>
 </tr>
<td>
</br>
 <strong> Fitness: </strong>
</br>
</br>
 Any illness / medical condition / ailment has to be reported to the organizers in detail. If the parent fail to
intimate the exact medical condition of the child or self; then the organizers shall not be held responsible
for any mishap that may occur or arise due to such illness / medical condition / ailment.
</br>
</br>
If any medical help is required before the marathon for the child, you can visit our Pediatric consultant at Bai Jerbai Wadia Hospital for Children from 10 am to 2 pm (Monday to Friday).
</br>
</br>
 <strong> Participation: </strong>
</br>
</br>
 The child’s age should be from 7 to 18 years.
</br>
</br>
 Marathon is at M.M.R.D.A ground, BKC, Mumbai – 400051 on 27th January, 2019
<br>
</br>
 Schools / organizations / societies can do bulk registrations and an authorized representative with valid receipt can collect the T-shirt & Bib on their behalf.
<br>
</br>
 Organizer will take all precautions but shall not be held liable for any unforeseen event / incident.
</br>
</br>
 T-shirt once handed over will not be exchanged.
</br>
</br>
 All registration fees are non-refundable.
</br>
</br>
 You can collect Goodie Bags with valid coupons only, after completion of the event. Participants without coupon will not be entertained.
</br>
</br>
 You can download the Timed certificate within 48 hours after completion of marathon.
</br>
</br>
 The security of the participating child is solely the responsibility of the accompanying parent / guardian. The organizer shall not be held responsible for any security related incident that may occur during the course of the marathon.
</br>
</br>
 The parent / guardian is required to strictly follow the instructions given by the organizers and incase of the difficulty forthwith report to the organizers.
</br>
</br>
Please choose the event category carefully, confirmed registrations are non-refundable, non-transferable and cannot be modified. Provide us with a secure email ID/mobile number that you can access regularly, since this will be our primary means of communication during the run up to the event.

</br>
</br> Users of email services that offer filtering/blocking of messages from unknown email address should add this email id marathon@wadiahospitals.org to their address list.

</br>
</br> We will be sending regular updates to the Mobile number you have provided in the registration form this should be not treated as spam and you shall not take any action against our bulk sms service provider.

</br></br> Any notice sent to the email address registered with the organizers shall be deemed as received by the runners.

</br></br> Please fill out only those fields that are necessary for mailing purposes. Do not provide redundant data in multiple fields. (i.e., do not list the same data for city, province and country), as this will only complicate our ability to contact you, if necessary.

</br></br> You are aware that running / long distance running is an extreme sport and can be injurious to body and health. You take full responsibility for participating in the Little Hearts Marathon 2019 event and do not hold the organizing committee or any of its members or entities responsible for any injury or accident.

</br></br> You shall consult your physician and undergo complete medical examination to assess your suitability to participate in the event.

</br></br> You also assume all risks associated with participating in this event including, but not limited to, falls, contact with other participants, the effects of the weather, including high heat or humidity, traffic and the condition of the road, arson or terrorist threats and all other risks associated with a public event.

</br></br> You agree that Little Hearts Marathon 2019 shall not be liable for any loss, damage, illness or injury that might occur as a result of your participation in the event.

</br></br> You agree to abide by the instructions provided by the organizers from time to time in the best interest of your health and event safety.

</br></br> You also agree to stop running if instructed by the Race Director or the Medical Staff or by the Aid Station Volunteers.

</br></br> You confirm that your name and media recordings taken during your participation may be used to publicize the event.

</br></br> You may acknowledge and agree that your personal information can be stored and used by Little Hearts Marathon 2019 or any other company in connection with the organization, promotion and administration of the event and for the compilation of statistical information.
</br></br> You confirm that, in the event of adverse weather conditions, major incidents or threats on the day, any of the force majeure or restriction by authority, the organizers reserve the right to stop/cancel/postpone the event. You understand that confirmed registrations and merchandise orders are non-refundable, non-transferable and cannot be modified. The organizers reserve the right to reject any application without providing reasons. Any amount collected from rejected applications alone will be refunded in full (excluding bank charges wherever applicable)

</br></br> For any reason you cannot turn up on race day, no refund of any form will be given.

</br></br> Wadia Hospitals is only a service provider and is not in any way responsible for the delivery of the event.

</br></br> If this registration is being made on behalf of a minor, I confirm that I am the parent / guardian of the child and that he/She has my permission to take part in the event. I further concur that all the above rules shall apply to him/her as if he were a major.

</br></br> Participants must personally appear for collecting the running Bibs. Bibs can be distributed in-absentee with written consent of the registered runners.

</br></br> Runners will not be allowed to stay on the course beyond the cut-off time considering the safety and health issues. We request total co-operation from runners in this regards.

</br></br>  <input type='checkbox' name='terms' value='terms'>I agree to the terms and conditions.
<p>&nbsp; 
<p>&nbsp;
<p>&nbsp;
<hr width = '20%' align = 'left' />
</ br ></ br >
   Signature & Date

</td>

</tr>
												</table>";

            #endregion


            dataDiv.InnerHtml = body;

        }
    }
}