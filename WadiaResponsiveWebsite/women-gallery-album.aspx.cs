﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class women_gallery_album : System.Web.UI.Page
    {
        static string flag = "w";

        string ID = "";
        int catid = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                ViewState["id"] = Convert.ToInt32(Request.QueryString["id"]);
                ID = Convert.ToString(ViewState["id"]);
            }


            if (!IsPostBack)
            {
                getalbums();
            }

            if (Session["catid"] != null)
            {

                catid = Convert.ToInt32(Session["catid"]);
                DataTable dt = CollectionHelper.GetDataTable(CCategory.Instance().GetCategoryDatabyid(catid));
                lbtncategorynm.Text = dt.Rows[0]["categorynm"].ToString();

            }
            else
            {
                lbtncategorynm.Visible = false;
            }

        }

        public void getalbums()
        {

            if (ID != "")
            {

                DataTable albumdata = CArtgallary.Instance().GetArtgallarygridDatabycategory(flag, ID);

                dlistalbumdtl.DataSource = albumdata;
                dlistalbumdtl.DataBind();
            }
            else
            {
                DataTable albumdata = CArtgallary.Instance().GetArtgallarygridData(flag);

                dlistalbumdtl.DataSource = albumdata;
                dlistalbumdtl.DataBind();
            }

        }


        protected void imgbalbumtnthumnail_Click(object sender, ImageClickEventArgs e)
        {

            ImageButton button = (sender as ImageButton);

            //Get the command argument
            string commandArgument = button.CommandArgument;

            int id = Convert.ToInt32(commandArgument);


            Response.Redirect("women-gallery.aspx?id=" + commandArgument);

        }

        protected void lbtnCategory_Click(object sender, EventArgs e)
        {

            Response.Redirect("women-gallery-category.aspx");

        }

        

        protected void lbtncategorynm_Click(object sender, EventArgs e)
        {
            Response.Redirect("women-gallery-album.aspx?id=" + catid);
        }
    }
}