﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-Nutrition.aspx.cs" Inherits="WadiaResponsiveWebsite.child_nutrition" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2> NUTRITION - DIETITION  </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         

Our experts believe that proper nutrition in childhood can go a long way in reinforcing lifelong eating habits that eventually contribute to your children’s overall well being and health and help them grow up to their full potential. The hospital has an experienced dietician to plan out the special dietary needs of children. 
<br>
<br>

To ensure that your child receives a wholesome and balanced meal, it is important to adequately provide food in a child’s diet from all the necessary food groups. These are:
<br>
<br>

<strong>•	Grains </strong>- whole grains, are good sources of fibre, iron, magnesium, selenium, and several B vitamins, including thiamin, riboflavin, niacin, and folate.
<br>
<br>

<strong>•	Vegetables</strong> - Vegetables are usually a good source of fibre, potassium, folate, vitamin A, vitamin E, and vitamin C.
<br>
<br>
<strong>•	Fruits</strong> - most children like fruits, which are usually a good source of potassium, fibre, vitamin C, and folate.
<br>
<br>

<strong>•	Milk</strong> - this food group is important because it provides children with calcium, potassium, vitamin D, and protein in their diet. 
<br>
<br>
<strong>•	Meat and Beans</strong> - in addition to meat and dry beans, this food group also includes poultry, fish, eggs, and nuts (including peanut butter), which are usually a good source of protein, iron, vitamin E, zinc, magnesium, and several B vitamins, including niacin, thiamine, riboflavin, and B6. 
<br>
<br>

<strong>•	Oils</strong> - In general, your children should eat mostly polyunsaturated or monounsaturated oils and fats, avoiding saturated fats, trans fats, and cholesterol.
<br>
(Source: About.com)



 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               

At the hospital, we provide treatment and advice to children with:
                              <br /><br />
                <div style="padding-left:10px">

•	Food intolerances
                    <br />
•	Obesity
                    <br />

•	Faltering growth
                    <br />

•	Fussy eaters and behavioural problems around eating
                    <br />

•	Coeliac disease
                    <br />

•	Common childhood conditions
  <br />
</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

         








		</div>




</asp:Content>
