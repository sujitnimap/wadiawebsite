﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Foetal-Medicine.aspx.cs" Inherits="WadiaResponsiveWebsite.women_foetalmedicine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Foetal Medicine</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
       Foetal Medicine is a branch of obstetrics that focuses on monitoring and treatment of women with high-risk pregnancies. Our expert foetal-medicine specialists take care of pregnant women who have special medical problems (e.g. heart or kidney disease, hypertension, diabetes, and thrombophilia), are at risk for pregnancy-related complications (e.g. preterm labour, pre-eclampsia, and twin or triplet pregnancies), or have foetuses that are at risk.
 <br />
 <br />

       <b>Services Provided : </b>
 <br />
 <br />

<div style="padding-left:25px">
       
-	Umbilical cord blood sampling
<br />-	Intrauterine Foetal transfusion
<br />-	Foetal thoracic and bladder catheter placement
<br />-	Active management of twin to twin transfusion syndrome
<br />-	Genetic Counselling.
<br />-	Interventional Procedures like Amniocentesis, Cordocentesis, and Chorionic Villus sampling.
<br />-	Selective Foetal Reduction.
</div>


 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

          


		</div>
</asp:Content>
