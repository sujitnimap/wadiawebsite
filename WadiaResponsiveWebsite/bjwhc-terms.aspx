﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="bjwhc-terms.aspx.cs" Inherits="WadiaResponsiveWebsite.bjwhc_terms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2> Terms & Conditions</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px;">
    
   
The website is owned by Wadia Hospitals. Your access to this website is subject to these terms and conditions, the Privacy Statement, notices, disclaimers and any other terms and conditions or other statements contained on this website (referred to collectively as “Terms and Conditions”). By using this website you agree to be subject to the Terms and Conditions

    <br /><br />



<strong>General conditions</strong>
      <br /><br />
These terms and conditions shall be governed by and construed in accordance with the Indian Laws. Any dispute arising under these terms and conditions shall be subject to the jurisdiction of the courts of India at Mumbai.
<br /><br />


These Terms and Condition can be modified at any time by Wadia Hospitals you agree to continue to be bound by these Terms and Conditions as modified. We will give you notice of these changes by publishing revised Terms and Conditions on this website – we will not separately notify you of these changes.

    <br /><br />


<strong>Copyright</strong>
    <br /><br />
Except where necessary for viewing the documents or information on the website on your browser, or as permitted under the Copyright Act or other applicable laws or these Terms and Conditions, no documents or information on this website may be reproduced, adapted, uploaded to a third party, linked to, framed, performed in public, distributed, stored, published, displayed or transmitted in any form by any process and you may not create derivative works from any part of this website or commercialize any information obtained from any part of this website without the specific written consent of Wadia Hospitals or, in the case of third party material, from the owner of the copyright in that material.
    <br /><br />

<strong>Online Donations</strong>
<br /><br />
All online donations are exempted under section 80G of Income Tax Act. The exemption certificates along with donation receipts will be sent by post to users who have requested for the same during online Donation. Donations and payments once made will not be reversed. 

<br /><br />
<strong>Disclaimer and limitation of liability</strong>
<br /><br />
To the maximum extent permitted by law, Wadia Hospitals will not be liable in any way for any loss or damage suffered by you through use or access to this website. Our liability for negligence, breach of contract or contravention of any law as a result of our failure to provide this website or any part of it, or for any problems with this website, which cannot be lawfully excluded, is limited, at our option and to the maximum extent permitted by law, to resupplying this website or any part of it to you, or to paying for the resupply of this website or any part of it to you.
<br /><br />




</div>

</div>




<%--    SUPPORTIVE END--%>












</div>

</div>



</asp:Content>
