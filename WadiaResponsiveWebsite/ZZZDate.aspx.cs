﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WadiaResponsiveWebsite
{
    public partial class ZZZDate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string dtTm = DateTime.Now.ToString("yyyyMMdd_HHmmssffff", CultureInfo.InvariantCulture);

            lblLeng.Text = dtTm;
            lblDate.Text = dtTm + "  " + dtTm.Length;

        }
    }
}