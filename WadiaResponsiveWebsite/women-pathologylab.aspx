﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-pathologylab.aspx.cs" Inherits="WadiaResponsiveWebsite.women_pathologylab" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2> 	Pathology Lab  </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
The Pathology department offers round the clock diagnostic services to both BJWHC & NWMH. Our Pathology lab offers a full range of tests and services, using state of the art technology for the most accurate results. All attempts are made to provide reports within 2 hours of receiving the samples.
<br /><br />
    The Pathology Lab is equipped to conduct a variety of tests for the speciality departments at both the hospitals. 
    Some of the tests conducted at the lab include :

    <br /><br />

    •	<b>Haematology</b>
    <br /><br />
<div style="padding-left:25px">
-	Bone marrow and peripheral smear examination
<br />
                -	Routine haematological investigations such as processing routine haematological investigations such as haemoglobin, total and differential leucocyte counts, platelets count, red cell indices

    <br /><br />
</div>


•	<b>Bio chemistry</b>
   <br /><br />
<div style="padding-left:25px">
-	Assays for hormones and immunosuppressants
<br />-	Therapeutic drug monitoring
<br />-	Heavy and toxic metals in biological fluid and tissues, 
<br />-	Trace elements, esoteric lipids and intermediates in cholesterol biosynthesis, 
<br />-	Clinical toxicology
</div>
                    <br />
•	<b>Microbiology</b> 
    <br /><br />
<div style="padding-left:25px">

-	Blood culture
<br />-	Chlamydia antibodies - serum
<br />-	Enterovirus antibodies - serum
<br />-	Fungal microscopy and culture - skin, hair, nails
<br />-	Genital swab
<br />-	Syphilis testing

    <br /><br />
    </div>


•	<b>Histopathology</b>
    <br /><br />
   <div style="padding-left:25px">
  
-	Fine needle aspiration cytology (FNAC) and screening tests for cancer
<br />-	Immunohistochemistry for all routine diagnostic and predictive markers 

    <br /><br />
       </div>

•	<b>Immunology</b> 
    <br /><br />
     <div style="padding-left:25px">

-	Smooth Muscle Antibodies
<br />-	Mitochondrial Antibodies
<br />-	Gastric Parietal Cell Antibodies
<br />-	Cryoglobulin Test

 <br />
 <br />
         </div>
                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

           




		</div>


</asp:Content>
