﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-about-vision.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_vision" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
    
    		<div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading"><h2>MISSION, VISION AND CORE VALUES</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
    
    
    <div class="titlecontent">	Mission </div>
    <br />
Our mission is to carry forward the philanthropic legacy of the Wadia family and deliver world-class treatments in Obstetric and Gynaecological care at affordable costs.
    <br /><br />

<div class="titlecontent">Vision</div>
<br />
Our vision is to provide top of the line affordable healthcare services at par with international standards through preventive, curative, intensive and 
    rehabilitative methods along with spreading awareness about the same through imparting quality health education. It is also our endeavour as a teaching Institute to provide highest quality of education to aspiring doctors.    <br /><br />

<div class="titlecontent">Core Values</div>
<br />
We believe our team can make a huge impact on the lives of our patients and caregivers by adopting a compassionate and positive attitude. We are 
    committed to working together to embed a culture and environment that encourages the following values: 
<br />
<br />
<div class="leftbigpad">
&#8226;	<strong>Patients First</strong>
<br />
<div class="leftpad">

Our patients are the centre of everything we do. Our foremost priority is to ensure that our trained professionals meet the needs and expectations of every patient by adopting a ‘Patients First’ approach. It is our duty to ensure patient safety and quality of care for every patient.

</div>
<br />
&#8226;<strong>	Integrity</strong>
<br />
<div class="leftpad">
We pride ourselves in being ethical and responsible in our thoughts and actions. We appreciate and encourage openness, honesty and accountability in every interaction with our patients, caregivers as well as co-workers.

</div>
<br />

&#8226;	<strong>Excellence</strong>
<br />
<div class="leftpad">

Together as a team we strive to provide the best quality healthcare for our patients and are devoted to continuously improving our methods and embracing the best practices.

</div>
<br />

&#8226;	<strong>Teamwork</strong>
<br />
<div class="leftpad">

We encourage and highly value teamwork across the organization to ensure that each member contributes and adds value to the environment and services provided by the hospital.



</div>
<br />

&#8226;	<strong>Caring</strong>
<br />
<div class="leftpad">

At the Wadia Hospital, we treat those we serve as well as each other with love, compassion and kindness thus creating an atmosphere within the organization that promotes and instils these values.

</div>
<br />
</div>

</div>
<!-- ABOUT US CONTENT -->



	</div>
			</div>
			
                <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>About Us</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="women-about-overview.aspx">Overview</a></li>
									<li><a href="women-about-vision.aspx">Mission, Vision, Core Values</a></li>
									<li><a href="women-about-chairman-msg.aspx">Chairman's message</a></li>
									<%--<li><a href="women-about-awards.aspx">Awards and Achievements</a></li>--%>
									<li><a href="women-about-history.aspx">History </a>
                                        <%-- <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-nusserwanji-wadia.aspx">Nowrosjee Nusserwanji Wadia (1849 – 99)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="women-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>


									</li>
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>



		</div>
	


</asp:Content>
