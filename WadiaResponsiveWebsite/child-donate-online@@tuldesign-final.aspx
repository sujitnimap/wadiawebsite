﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-donate-online.aspx.cs" Inherits="WadiaResponsiveWebsite.child_donate_online" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        var specialKeys = new Array();

        specialKeys.push(8); //Backspace

        function IsNumeric(e) {

            var keyCode = e.which ? e.which : e.keyCode

            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);

            return ret;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-2-3">
            <div class="wrap-col">

                <div class="heading">
                    <h2>Donate Online </h2>
                </div>
                <br />
                <!-- ABOUT US CONTENT -->
                <div style="line-height: 20px">
                    As a hospital working primarily towards bridging the gaps and providing quality healthcare to every section of the society, we offer subsidized treatments to our patients without ever compromising on quality. To ensure we continue to do so, we rely on aid from you. Regardless of the size of your contribution, your support makes a huge difference to our work and mission. 
                    <br />
                    <br />
                    All donations made to Wadia Hospitals by individuals and organisations are acknowledged in the Annual report and are eligible for a 50% tax exemption under section 80G.
                    <br />
                    <br />
                    Donating online is quick and easy. Fill in the form below to send us your contribution:
                    <br />
                    <br />
                    <div style="border: 1px solid #ddd; padding: 10px 20px 20px 10px; border-radius: 4px;">

                        <div style="width: 99%; background-color: #999; height: 20px; color: #FFF; vertical-align: middle; padding: 8px 6px 10px 10px;">
                            General Online Donation Form 

                        </div>
                        <div class="devider_20px"></div>

                      <%--  <div>
                            <div style="display: inline; padding-right: 60px">

                                <strong>Donation to</strong>
                            </div>

                      

                            <asp:RadioButton ID="rdbTMH" GroupName="donationTo" Checked="true" Text="TMH" runat="server" />
                            <asp:RadioButton ID="rdbACTREC" GroupName="donationTo" Text="ACTREC" runat="server" />
                        </div>
                        <div class="devider_20px"></div>--%>





                        <div>
                            <div style="display: inline; padding-right: 35px">

                                <strong>Donation From</strong>
                            </div>

                            <div style="display: inline">
                                <asp:DropDownList ID="ddlDonationFrom" runat="server" >
                                    <asp:ListItem Text ="Individual" Selected="True" Value="1"></asp:ListItem>
                                    <asp:ListItem Text ="Corporation" Value="2"></asp:ListItem>
                                    <asp:ListItem Text ="Others" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                                </div>
                            <%--<div style="display: inline">
                                Individual  &nbsp;&nbsp;<asp:CheckBox ID="chkIndividual" runat="server" />
                            </div>
                            <div style="display: inline">
                                Corporation  &nbsp;&nbsp;<asp:CheckBox ID="chkCorporation" runat="server" />
                            </div>
                            <div style="display: inline">
                                Others  &nbsp;&nbsp;<asp:CheckBox ID="chkDonationFromOthers" runat="server" />
                            </div>--%>
                        </div>

                        <div class="devider_20px"></div>

                        <div style="width: 99%; background-color: #999; height: 20px; color: #FFF; vertical-align: middle; padding: 8px 6px 10px 10px;">
                            For donation by Company and others 
                        </div>

                        <div class="devider_20px"></div>

                        <asp:TextBox ID="txtNameOfOragnisation" CssClass="textboxcontact" runat="server" placeholder="Name Of Organisation*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name Of Organisation'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtNameOfOragnisation" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtNameOfOragnisation" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <%--<div class="devider_10px"></div>--%>

                        <asp:TextBox ID="txtFirstNameOfContactPerson" CssClass="textboxcontact" runat="server" placeholder="First Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtFirstNameOfContactPerson" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtFirstNameOfContactPerson" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <asp:TextBox ID="txtLastNameOfContactPerson" CssClass="textboxcontact" runat="server" placeholder="Last Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtLastNameOfContactPerson" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtLastNameOfContactPerson" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>


                          <asp:TextBox ID="txtCity" CssClass="textboxcontact" runat="server" placeholder="City*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'City' "></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtCity" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtCity" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>


                      <%--  <div class="devider_10px"></div>--%>
                        <asp:TextBox ID="txtPostalAddress" CssClass="textboxcontact" runat="server" placeholder="Postal Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Postal Address'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtPostalAddress" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtPostalAddress" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                       <%-- <div class="devider_10px"></div>--%>


                        <asp:TextBox ID="txtPostalCode" CssClass="textboxcontact" runat="server" placeholder="Postal Code*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Postal Code'"></asp:TextBox>
                        <div class="devider_10px"></div>

                      
                        <div class="devider_10px"></div>

                        <asp:DropDownList CssClass="textboxcontact_list" ID="ddlCountrys" runat="server">
                            <asp:ListItem>India</asp:ListItem>
                            <asp:ListItem>Nepal</asp:ListItem>
                            <asp:ListItem>Pakistan</asp:ListItem>
                        </asp:DropDownList>

                        <div class="devider_10px"></div>
                        <div class="devider_10px"></div>

                        <asp:TextBox ID="txtEmail" CssClass="textboxcontact" runat="server" placeholder="Email*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtEmail" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtEmail" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                       <%-- <div class="devider_10px"></div>--%>

                        <asp:TextBox ID="txtPanNumbers" CssClass="textboxcontact" runat="server" placeholder="Pan Number*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Pan Number'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtPanNumbers" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtPanNumbers" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <%--<div class="devider_10px"></div>--%>

                        <asp:TextBox ID="txtTelephone" CssClass="textboxcontact" runat="server" placeholder="Telephone*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Telephone'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtTelephone" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtTelephone" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <%--<div class="devider_10px"></div>--%>

                        <asp:TextBox ID="txtMobile" CssClass="textboxcontact" runat="server" onkeypress="return IsNumeric(event);" MaxLength="15" placeholder="Mobile*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtMobile" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtMobile" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <%--<div class="devider_10px"></div>--%>

                        <asp:TextBox ID="txtMessageToBePrintedOnReceipt" CssClass="textboxcontact" runat="server" placeholder="Message to be printed on Receipt(optional) " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Message'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtMessageToBePrintedOnReceipt" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtMessageToBePrintedOnReceipt" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                      <%--  <div class="devider_10px"></div>--%>


                        <div style="width: 99%; background-color: #999; height: 20px; color: #FFF; vertical-align: middle; padding: 8px 6px 10px 10px;">
                            Payment Details 
                        </div>

                        <div class="devider_20px"></div>

                        <asp:TextBox ID="txtAmount" CssClass="textboxcontact" runat="server" placeholder="Amount*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Amount'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rerftxtAmount" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtAmount" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <%--<div class="devider_10px"></div>--%>

                        <div>
                            <div style="display: inline; padding-right: 60px">

                                <strong>Donation Towards</strong>
                            </div>

                            <div style="display: inline; padding-right: 20px">
                                <%--Patient Welfare Fund  &nbsp;&nbsp;<asp:CheckBox ID="chkPatientWelfareFund" runat="server" />--%>
                                <asp:RadioButton ID="rbPatientWelfareFund" GroupName="donationTowards" Checked="true" Text="Patient Welfare Fund" runat="server" />

                            </div>

                            <div style="display: inline; padding-right: 20px">
                                <%--Cancer Research   &nbsp;&nbsp;<asp:CheckBox ID="chkCancerResearch" runat="server" />--%>
                                <asp:RadioButton ID="rbCancerResearch" GroupName="donationTowards" Text="Cancer Research" runat="server" />

                            </div>

                            <div style="display: inline; padding-right: 20px">
                               <%-- Others  &nbsp;&nbsp;<asp:CheckBox ID="chkDonationTowardsOthers" runat="server" />--%>
                                <asp:RadioButton ID="rbDonationTowardsOthers" GroupName="donationTowards" Text="Others" runat="server" />

                            </div>

                        </div>

                        <div class="devider_20px"></div>

                        <asp:TextBox ID="txtDonationTowardsOther" CssClass="textboxcontact" runat="server" placeholder="If others(specify)" onfocus="this.placeholder = ''" onblur="this.placeholder = 'if others(specify)'"></asp:TextBox>


                        <div class="devider_20px"></div>

                        <div>
                            <div style="display: inline; padding-right: 30px">

                                <strong>*Do you require Income-tax exemption certificate under section 80G? </strong>
                            </div>
                            <div class="devider_10px"></div>
                            <div style="display: inline; padding-right: 20px">
                               <%-- Yes   &nbsp;&nbsp;<asp:CheckBox ID="chkIsIncomTaxCertificateRequiredYes" runat="server" />--%>
                                <asp:RadioButton ID="rbIsIncomTaxCertificateRequiredYes" GroupName="incomeTaxExemptionCertificate" Checked="true" Text="Yes" runat="server" />

                            </div>

                    

                            <div style="display: inline">
                               <%-- No   &nbsp;&nbsp;<asp:CheckBox ID="chkIsIncomTaxCertificateRequiredNo" runat="server" />--%>
                                <asp:RadioButton ID="rbIsIncomTaxCertificateRequiredNo" GroupName="incomeTaxExemptionCertificate" Checked="true" Text="No" runat="server" />

                            </div>
                        </div>

                        <div class="devider_20px"></div>
<%--                        <div class="devider_10px"></div>--%>
                        <div>
                            <div style="display: inline; padding-right: 30px">

                                <strong>If yes tax exemption required U/S  </strong>
                            </div>

                            <div style="display: inline; padding-right: 20px">
                                <%--80G   &nbsp;&nbsp;<asp:CheckBox ID="chkIncomTaxExemptionOption80" runat="server" />--%>
                                <asp:RadioButton ID="rbIncomTaxExemptionOption80" GroupName="taxExemption" Checked="true" Text="80G" runat="server" />
                            </div>

                            <div style="display: inline; padding-right: 20px">
                                <%--35 (1) (ii)   &nbsp;&nbsp;<asp:CheckBox ID="chkIncomTaxExemptionOption35" runat="server" />--%>
                            <asp:RadioButton ID="rbIncomTaxExemptionOption35" GroupName="taxExemption" Text="35 (1) (ii) " runat="server" />

                            </div>
                            
                        </div>


                        <div class="devider_20px"></div>

                        <asp:TextBox ID="txtDonationReceipt" CssClass="textboxcontact" runat="server" placeholder="Donation receipt in favour of*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Donation receipt in favour of'"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reftxtDonationReceipt" ValidationGroup="vChildDonationOnlineAdd" ControlToValidate="txtDonationReceipt" runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>

                        <%--<div class="devider_20px"></div>--%>


                        <div>

                            <strong>Note:</strong>
                            <div class="devider_10px"></div>
                            The receipt and exemption income tax certificate if any will be sent by post to the address specified.
                            <div class="devider_20px"></div>
                            Donation once made will not be refunded.

                        </div>




                        <div class="devider_20px"></div>
                        <div>
                            <div style="display: inline; padding-right: 30px">

                                <strong>Mode of Payment </strong>
                            </div>
                            <div style="display: inline">
                                Credit Card   &nbsp;&nbsp;<asp:CheckBox ID="chkCreditCard" Checked="true" runat="server" Visible="false" />

                            </div>

                        </div>
                        <div class="devider_20px"></div>
                        <div>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </div>
                        <div class="devider_20px"></div>
                        <div style="text-align: left">
                            <%--<img src="images/SubmitButton.png" 

                            <div class="more"><a href="#" class="make_payment">Save and Make Payment</a></div>/>--%>

                            <asp:Button ID="btnSaveAndMakePayment" runat="server" Text="Save and Make Payment" ValidationGroup="vChildDonationOnlineAdd" CssClass="btn btn-info" OnClick="btnSaveAndMakePayment_Click" />

                        </div>

                    </div>
                </div>
                <!-- ABOUT US CONTENT -->

            </div>
        </div>

        <div class="col-1-3">
            <div class="wrap-col">

                <div class="box">
                    <div class="heading">
                        <h2>CONTRIBUTE</h2>
                    </div>
                    <div class="content">
                        <div class="list">
                            <ul>


                                <li><a href="<%--child-donate-online.aspx--%>#">Donate Online</a></li>
                                <li><a href="child-sponsor-patient.aspx">Sponsor a Patient</a></li>
                                <li><a href="child-donate-equipment.aspx">Donate Equipment</a></li>

                                <li><a href="child-donate-inkind.aspx">Donate in-kind </a></li>


                                <li><a href="child-corporate-giving.aspx">Corporate Giving</a></li>
                                <li><a href="child-testimonials.aspx?catnm=Contributors">Testimonials </a></li>



                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
</asp:Content>
