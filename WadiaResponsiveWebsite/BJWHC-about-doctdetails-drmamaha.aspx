﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-about-doctdetails-drmamaha.aspx.cs" Inherits="WadiaResponsiveWebsite.child_about_doctdetails_drmamaha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr. Mamatha M lala.jpg" />
<div class="docdetails">
Dr. Mamatha  M. Lala

<div class="docdetailsdesg">
Research Officer - HIV
    <br />
DCH, DNB

</div>

</div>
</div>

<div class="areaofinterest">

Areas of Interest : Perinatal & Paediatric HIV/ AIDS, Prevention of Mother to Child Transmission of HIV, Issues & challenges of Orphans & Vulnerable children, Tuberculosis, Paediatric Infectious Diseases

</div>



<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
          <%--  <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Achievements</a></li>
            <li><a href="#view3">Affiliations</a></li>
            <li><a href="#view4">Research or publications</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
            <%--<div id="view1">
                
                <p>
                
     Perinatal & Paediatric HIV/ AIDS, Prevention of Mother to Child Transmission of HIV, Issues & challenges of Orphans & Vulnerable children, Tuberculosis, Paediatric Infectious Diseases, Expedition Medicine, Adventure & Extreme Sports
         
                    
                    <br />
              
                </p>
                
            </div>--%>
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
                <p>
      
                •	Gold Medallist in Forensic Medicine during Medical schooling
                    <br /><br />
•	Recipient of the prestigious Rotary International Woman of Substance Award 2014
<br /><br />
•	Recipient of the prestigious HIV Trust Scholarship & Hargobind Medical Scholarships
<br /><br />
•	Awarded Gold Medal from Jodhpur National University School of Medicine and Health for contribution to society as “ Doctor with a Difference” – humanitarian work for orphans and vulnerable children of the HIV epidemic + Excellence in Adventure Sports.
<br /><br />
•	Undergone Training/Fellowships in various centres of Excellence around the world 
<br /><br />
•	Published a comprehensive care reference book on Children with HIV – “Principles of Perinatal & Paediatric HIV/ AIDS”, the first of it’s kind from the Indian subcontinent, which is a compilation of immense knowledge & experience from experts in the field from all over the world & includes as many as 47 chapters addressing in depth all issues related to children with HIV/AIDS including medical, social, psychological, legal & nursing issues & prevention of Mother to Child Transmission of HIV;
<br /><br />
•	Number of publications in reputed peer reviewed journals; 
<br /><br />
•	Numerous presentations in various National & International Conferences. 
<br /><br />


                </p>
                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
               

                <p>
                
           •	Medical Advisor & Consultant Paediatrician in various NGOs & orphanages taking care of children infected & affected by HIV/AIDS since 2006 to date
<br /><br />

                    <div style="padding-left:25px">

o	Committed Communities Development Trust [CCDT]
<br /><br />
o	Society for Human & Environmental Development [SHED]
<br /><br />
o	Mumbai Smiles
<br /><br />
o	Médecins sans Frontières (MSF), Pédiatrique HIV & TB Project, Mumbai (2008 – 11)  
</div>
    <br /><br />
    
</p>
               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       <div id="view4">
                
         
                <p>
                
•	Mamatha M. Lala, Current Treatment guidelines for HIV-infected Children. Supplement-Indian J Pediatr. 2014;81:S1-S21. 
<br /><br />
•	Editor of ‘Principles of Perinatal & Paediatric HIV/AIDS’ – a reference book on management of Perinatal & Paediatric HIV/ AIDS, 2012 – the first of it’s kind from the Indian sub continent; Publishers: Jaypee Brothers Medical Publishers (P) Ltd, India, UK & USA.
<br /><br />
•	Mamatha M. Lala, Rashid H. Merchant. Prevention on Parent to Child Transmission of HIV – What is new? Symposium on Paediatric HIV/AIDS. Indian J Pediatr. 2012 Nov;79(11):1491-500. 
<br /><br />
•	Rashid H Merchant, Mamatha M Lala. Common Clinical Problems in Children Living with HIV/AIDS: Systemic Approach. Symposium on Paediatric HIV/AIDS.  Indian J Pediatr. 2012 Nov;79(11):1506-13. 
<br /><br />
•	Ira Shah, Shradha Gupta, Dhaval M Shah, Harshal Dhabe, Mamatha Lala. Renal manifestations of HIV infected highly active antiretroviral therapy naive children in India. World journal of paediatrics : WJP. 08/2012; 8(3):252-5.
<br /><br />
•	Mothi S.N, Swamy V.H.T, Karpagam S, Lala Mamatha M, R.G Khedkar. Adolescents living with HIV in India – the clock is ticking... Symposium on Paediatric HIV/AIDS. Indian J Pediatr. 2012 Dec;79(12):1642-7. 
<br /><br />
•	Mothi SN, Karpagam S, Swamy V, Mamatha ML, Sarvode SM. Paediatric HIV - trends & challenges. Indian J Med Res 2011;134:912-9.
<br /><br />
•	I. Shah, Lala, Mamatha M., H Dhabe. Renal manifestations in HIV infected HAART naive Children: A preliminary study from India. World Journal of Paediatrics. 01/2011;
<br /><br />
•	Rashid H. Merchant, Mumtaz Sharif, Mamatha M. Lala. Clinical Manifestations of HIV/ AIDS in Children: an Overview. In: Principles of Perinatal & Paediatric HIV/AIDS. Mamatha M. Lala, Rashid H. Merchant (Editors): Jaypee Brothers Medical Publishers (P) Ltd, India, UK & USA, 2011.
<br /><br />
•	Mamatha M. Lala, Rashid H. Merchant. Pulmonary Manifestations in Children with HIV. In: Principles of Perinatal & Paediatric HIV/AIDS. Mamatha M. Lala, Rashid H. Merchant (Editors): Jaypee Brothers Medical Publishers (P) Ltd, India, UK & USA, 2011.
<br /><br />
•	Mamatha M. Lala, Rashid H. Merchant, Kaizad R. Damania. Vertical Transmission of HIV: An update. In: Principles of Perinatal & Paediatric HIV/AIDS. Mamatha M. Lala, Rashid H. Merchant (Editors): Jaypee Brothers Medical Publishers (P) Ltd, India, UK & USA, 2011. 
<br /><br />
•	Cherie- Ann Pereira, Mamatha M. Lala, John Farley. Adherence to Anti Retrovial Therapy in Children – Challenges & Strategies. In: Principles of Perinatal & Paediatric HIV/AIDS. Mamatha M. Lala, Rashid H. Merchant (Editors): Jaypee Brothers Medical Publishers (P) Ltd, India, UK & USA, 2011.  
<br /><br />
•	Ira Shah, Soumya Swaminathan, geetha Ramachandran, A.K Hemanth Kumar, Apurva Goray, Udith Chadda, Swati Tayal, Mamatha Lala. Serum Nevirapine and Efavirenz Concentrations and Effect of Concomitant Use of Rifampicin in HIV Infected Children on Antiretroviral Therapy. Indian Paediatrics 2011, 48:943-47.
<br /><br />
•	Damania Kaizad R, Tank Parikshit D, Lala Mamatha M. Recent Trends in Mother To Child Transmission of HIV in Pregnancy. J Obstet Gynecol India Vol. 60, No. 5 : September / October 2010 pg 395 – 402. 
<br /><br />
•	Mamatha M. Lala, Rashid H. Merchant. Vertical Transmission of HIV – An update. Indian J Pediatr ;2010, 77:1270–1276
<br /><br />
•	Shah, I., Lala, M.M., Dhabe, H., Katira, B. Assessment of adherence to antiretroviral therapy in HIV infected children – A Preliminary Indian study. International Paediatrics; 2009
<br /><br />
•	Mamatha M. Lala. HIV related Lung Diseases in Children. In: Manual on Tuberculosis, HIV & Lung Diseases: A Practical Approach. Arora, VK, (Editor) India: Jaypee Brothers Medical Publishers; 2009. pp 320- 335 
<br /><br />
•	Ira Shah, Mamatha M. Lala, Harshal Dhabe, Bhushan Katira. Assessment of adherence to Anti Retroviral Therapy in HIV infected children. PEDICON, Mumbai, 2007[Abstract ID/34(P)]
<br /><br />
•	Lala M.M, Gaur S. “Is 18 months a good enough cut off point for the diagnosis of perinatal HIV infection in a resource limited setting??”  International AIDS Society IAS Conference, Sydney, Australia. July 2007. (Abstract TUPEB024).
<br /><br />
•	Mamatha M. Lala. HIV and Tuberculosis co infection in Children. In: Practical Approach to Tuberculosis Management. Arora VK, (Editor) India: Jaypee Brothers Medical Publishers; 2006. pp 224-238.
<br /><br />
•	Suryanarayana L, Mamatha M. Lala. Tuberculosis among children- Evidence based issues in a community Survey. In: Practical Approach to Tuberculosis Management. Arora VK, (Editor) India: Jaypee Brothers Medical Publishers; 2006. pp. 21-26. 
<br /><br />
•	Mamatha Lala, Rashid H. Merchant. Prevention of Mother to Child Transmission of HIV- an overview. Indian J Med Res 121, April 2005, 489-501
<br /><br />
•	V .K Arora, M .L Seetharaman, S Ramkumar, T. V Mamatha, K. S. V. K Subbarao, Amit Banerjee, C Ratnakar, A. J Veliath. Bronchogenic Carcinoma- Clinico - Pathological Pattern In South Indian Population Lung India, VIII: No. 3, 133-138, 1990.
<br /><br />
•	V .K Arora, M .L Seetharaman, T. V Mamatha. Dyspnoea, subcutaneous emphysema and abnormal chest radiography following pleurocentesis. Indian Journal of Chest Diseases and Allied Sciences, 33: No.4, 201-204, 1991.
<br /><br />
                </p>
               
            </div>
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			
        




		</div>






</asp:Content>
