﻿using OperationStatus;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class NWMH : System.Web.UI.Page
    {
        IList<casestudy> casestudydata;
        //IList<testimonial> testmnldata;
        IList<ourevent> eventdata;

        static string flag = "w";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                getcasestudy();

                //gettestimnial();

                getupcomingevent();
                getpastevent();
            }

        }


        public void getcasestudy()
        {
            casestudydata = CCasestudy.Instance().GetCasestudyData(flag);

            var list = (from t in casestudydata orderby t.createdate descending select t).Take(2);

            DataTable dt = CollectionHelper.GetDataTable(list);


            dlistcasestudy.DataSource = dt;
            dlistcasestudy.DataBind();

        }


        //public void gettestimnial()
        //{
        //    testmnldata = CTestimonial.Instance().GetTestimonialData(flag);

        //    var list = (from t in testmnldata orderby t.createdate descending select t).Take(1);

        //    DataTable dt = CollectionHelper.GetDataTable(list);

        //    if (dt.Rows.Count > 0)
        //    {
        //        lbltestmnl.Text = dt.Rows[0]["testimonialdescrp"].ToString();
        //        lbltestmnl.Visible = true;
        //    }
        //    else
        //    {
        //        lbltestmnl.Visible = false;
        //    }

        //}


        public void getevent()
        {
            eventdata = CEvent.Instance().GetEventData(flag);

            var list = (from t in eventdata orderby t.createdate descending select t).Take(1);

            DataTable dt = CollectionHelper.GetDataTable(list);

            //if (dt.Rows.Count > 0)
            //{
            //    lblevent.Text = dt.Rows[0]["description"].ToString();
            //    lblevent.Visible = true;
            //}
            //else
            //{
            //    lblevent.Text = "No event found.";
            //    lblevent.Visible = true;
            //}
        }

        public void getupcomingevent()
        {
            DataTable UpcomigEventdata = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ToString()))
            {
                SqlCommand cmd = new SqlCommand("sp_GetUpcomigEvent", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Open();
                da.Fill(UpcomigEventdata);
                conn.Close();
            }

            if (UpcomigEventdata.Rows.Count > 0)
            {
                dlistupcomingevent.DataSource = UpcomigEventdata;
                dlistupcomingevent.DataBind();

                pnlforupcomingevent.Visible = true;
            }
            else
            {
                dlistupcomingevent.DataSource = null;
                dlistupcomingevent.DataBind();

                pnlforupcomingevent.Visible = false;
            }

        }


        public void getpastevent()
        {
            DataTable PastEventdata = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WadiaDBConstr"].ToString()))
            {
                SqlCommand cmd = new SqlCommand("sp_GetPastEvent", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag", flag);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Open();
                da.Fill(PastEventdata);
                conn.Close();
            }

            if (PastEventdata.Rows.Count > 0)
            {
                dlistpastevent.DataSource = PastEventdata;
                dlistpastevent.DataBind();

                pnlforpassevent.Visible = true;
            }
            else
            {
                dlistpastevent.DataSource = null;
                dlistpastevent.DataBind();

                pnlforpassevent.Visible = false;
            }

        }

        protected void imgbtnsubscribe_Click(object sender, ImageClickEventArgs e)
        {
            COperationStatus os = new COperationStatus();

            subscribe subcrb = new subscribe();
            subcrb.emailid = txtsubscribe.Text.Trim();
            subcrb.createdate = DateTime.Now;


            os = CEvent.Instance().InsertSubcribe(subcrb);


            if (os.Success == true)
            {
                string message = os.Message;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<script type = 'text/javascript'>");
                sb.Append("window.onload=function(){");
                sb.Append("alert('");
                sb.Append(message);
                sb.Append("')};");
                sb.Append("</script>");
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());


                txtsubscribe.Text = string.Empty;
            }
            else
            {
                string message = os.Message;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<script type = 'text/javascript'>");
                sb.Append("window.onload=function(){");
                sb.Append("alert('");
                sb.Append(message);
                sb.Append("')};");
                sb.Append("</script>");
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());


                txtsubscribe.Text = string.Empty;
            }
        }
    }
}