﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WadiaResponsiveWebsite
{
    public partial class child_donate_equipment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void imgbtncontact_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                string strMailUserName = ConfigurationManager.AppSettings["SEND_TO_ADDR"];

                CollectionHelper.sendMail(strMailUserName, "Child-donate-equipment", "Dear Sir/Madam ,", txtname.Text + "<br>Email-Id : " + txtemail.Text.Trim(), txtmessage.Text, txtcontactno.Text);

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Thank you.');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);


                txtname.Text = string.Empty;
                txtemail.Text = string.Empty;
                txtcontactno.Text = string.Empty;
                txtmessage.Text = string.Empty;

            }
            catch (Exception ex)
            {

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('" + ex.Message + "');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);

            }
        }
    }
}