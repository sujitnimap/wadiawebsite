﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Social-Service-Department.aspx.cs" Inherits="WadiaResponsiveWebsite.women_socialservdept" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Social Service Department</h2></div>
                    
                    <br />

<!-- CONTENT -->

<div style="line-height:20px">

    <strong>Overview </strong>
    <br />    <br />


At the NWMH, we understand the stress of illness to not only the patient but also their families. Our Social Service Department works towards counselling the patients and family members and help reduce their stress. The department also identifies patients who need financial support based on their socio-economic conditions and works towards generating financial aid. 
<br /><br />
    
    We work with the relevant regional, national or international organizations to provide information and services for :
    <br /><br />
     <div style="padding-left:25px">
-	HIV / AIDS
<br />-	Women’s Reproductive Health and Nutrition 
<br />-	Health of Neonates
<br />-	Rights of Women and Children 
<br />-	Adoption 
<br />-	Medico-legal cases relating to the police or juvenile courts
	
</div>
    <div style="height:90px"></div>



</div>
<!--  CONTENT -->


                
                    
                    
				</div>
			</div>
			


        



		</div>




</asp:Content>
