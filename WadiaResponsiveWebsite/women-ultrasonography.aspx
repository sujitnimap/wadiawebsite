﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="women-ultrasonography.aspx.cs" Inherits="WadiaResponsiveWebsite.women_ultrasonography" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Ultrasonography </h2></div>

                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
             <li><a href="#view3">Services</a></li>

                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
This is an ultrasound-based technique used to create diagnostic images that help visualize internal structures of the body. 
                We have an independent Ultrasonography Department that is well equipped for all varieties of ultra sonography services. 
                The hospital also offers Colour Doppler, useful for detection of complex pregnancy problems. The Visual Acoustic stimulation Test (V.A.S.T) helps in foetal surveillance studies to detect foetal anomalies & malformations whose identification and future prognosis needs to be dealt with in detail. The specialized screening is done by experts in the field of foetal medicine.


 <br />
 <br />


             

</div>


                  
            </div>

               <div id="view3">
               
                   <div style="padding:0 10px 0 10px"
  <b> Services Provided:</b> 
                <br />
 <br />

     <div style="padding-left:25px">
•	<b>Obstetric Ultrasonography</b>
                <br />
 <br />

Sonography is used to visualize the embryo or foetus in the mother's uterus, to gain a variety of information regarding the health of the mother and of the foetus. 
<br />
 <br />

•	<b>Gynaecological Pelvic Imaging</b>
                <br />
 <br />

Involves the assessment of the female pelvic organs
                <br />
 <br />


•	<b>Folliculometry</b>
                <br />
 <br />

These are a series of Ultrasounds used to determine how follicles are growing in the ovaries.
<br />
 <br />


•	<b>Foetal anatomical Anomaly scan</b>
                <br />
 <br />

Primarily used to assess foetal anatomy and detect the presence of any foetal anomalies.
                <br />
 <br />


•	<b>Doppler ultrasonography</b><br />
 <br />

Doppler ultrasonography is a non-invasive diagnostic procedure that changes sound waves into an image that can be viewed on a monitor
<br />
 <br />


•	<b>Foetal 2 D Echocardiography</b><br />
 <br />

Fetal echocardiography is an ultrasound test performed to evaluate the heart of the fetus during pregnancy and is often carried out as early as 20-24 weeks into the pregnancy.
<br />
 <br />

•	<b>1st Trimester Genetic scans</b><br />
 <br />

This is a noninvasive evaluation procedure that combines a maternal blood-screening test with an ultrasound evaluation of the foetus to identify risk for specific chromosomal abnormalities.
<br />
 <br />

•	<b>Biophysical Profile</b><br />
 <br />

A biophysical profile is a prenatal ultrasound evaluation of foetal well being involving a scoring system, with the score being termed Manning's score. 
         It is often carried out either when a non-stress test is non reactive, or for any other obstetrical indications.
<br />
 <br />

Imaging is a medical speciality that is used to both diagnose and treat diseases within the body. At NWMH, we use the most sophisticated equipment in imaging technology such as X–Ray and radiography, to diagnose or treat diseases. 
<br />
 <br />
               
            </div>



            </div>
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

       
		</div>
</asp:Content>
