﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-wellbaby-clinic.aspx.cs" Inherits="WadiaResponsiveWebsite.child_wellbaby_clinic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Well Baby Clinic</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">

    <strong>Overview </strong>
    <br />    <br />


Well baby clinics provide regular check-ups for babies. It provides a much-needed support system for first time parents and offers the opportunity to raise issues and questions that are worrying them about their young ones. Trained nurses and medical professionals offer advice and information on:
<br /><br />


-	Parenting
<br />-	Breastfeeding
<br />-	Nutrition and dietary advice
<br />-	Vaccination
<br />-	Infant/child growth development stages



The well baby clinics also give an opportunity for the medical team to identify and flag off any concerns about the babies’ health and refer them to appropriate specialists within the hospital.
<br /><br />
The hospital runs the Well Baby Clinic thrice a week and provides free immunization services and dietary advice.

<br /><br />

</div>
<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        



		</div>
</asp:Content>
