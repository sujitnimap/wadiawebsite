﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="child-paediatric-burns.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_burns" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PAEDIATRIC BURNS AND PLASTIC SURGERY</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Outpatient Department</a></li>
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
The hospital has a top of the line Operation Theatre and a ward for children with burns or those requiring plastic surgery. The hospital has developed an innovative method to offer low-cost, effective postoperative care. Convalescing children are applied with potato peel dressings and are fed a special diet of banana curd, which helps the healing process. Such a low cost and effective therapy has been acclaimed and practiced by other centres in the country. 

                 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               
Paediatric Plastic surgeons provide pre and postoperative care to children with a range of problems from complicated craniofacial defects to hand surgeries to vascular malformations such as:
                 <br /><br />
                <div style="padding-left:10px">
-	Breathing and other respiratory disorders
<br />-	Cleft lip and palate
<br />-	Congenital nevi
<br />-	Congenital ear abnormalities
<br />-	Craniofacial abnormalities
<br />-	Extremity reconstruction
<br />-	Surgical management of traumatic injuries
<br />-	Trunk reconstruction
<br />-	Vascular malformations

</div>

         
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

      

		</div>







</asp:Content>
