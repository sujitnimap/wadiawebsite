﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-speciality-services.aspx.cs" Inherits="WadiaResponsiveWebsite.child_speciality_services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">

        <div class="col-full">
            <div class="wrap-col">

                <div class="heading">
                    <h2>services</h2>
                </div>

                <br />

                <!-- ABOUT US CONTENT -->

                <div style="line-height: 20px;">

                    <div class="serviceheading">SPECIALITY SERVICES</div>

                    <div style="padding-left: 30px">

                        <div class="devider_20px"></div>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="12%" height="60" valign="middle">
                                    <img src="images/services/General Paediatric Medicine.png" width="46" height="52" /></td>
                                <td class="servicename" width="50%"><a href="BJWHC-paediatric-medicine.aspx">General Paediatric Medicine  </a></td>
                                <td width="11%" height="60" valign="middle">
                                    <img src="images/services/General Paediatric Surgery.png" width="52" height="42" /></td>
                                <td width="27%" class="servicename"><a href="BJWHC-paediatric-surgery.aspx">General Paediatric Surgery </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="60">
                                    <img src="images/services/Emergency Department.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="BJWHC-Emergency-department.aspx">Emergency Department</a></td>
                                <td valign="middle" height="60">
                                    <img src="images/services/NICU.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="BJWHC-NICU.aspx">Neonatal Intensive Care Unit (NICU)  </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="60">
                                    <img src="images/services/(PICU).png" width="52" height="44" /></td>
                                <td class="servicename"><a href="BJWHC-PICU.aspx">The Paediatric Intensive Care Unit (PICU)</a></td>
                                <td valign="middle" height="60">
                                    <img src="images/services/Immunization clinic.png" width="47" height="47" /></td>
                                <td class="servicename"><a href="BJWHC-Immunization-clinic.aspx">Immunization clinic  </a></td>
                            </tr>

                        </table>

                    </div>
                </div>

                <div class="devider_30px"></div>
                <div style="line-height: 20px;">

                    <div class="serviceheading" style="padding-left: 0px">SUPER SPECIALITY SERVICES</div>

                    <div style="padding-left: 30px">

                        <div class="devider_30px"></div>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="12%" height="66px" valign="middle">
                                    <img src="images/services/High risk OPD.png" width="46" height="46" /></td>
                                <td class="servicename" width="50%"><a href="BJWHC-High-riskOPD.aspx">High risk OPD  </a></td>
                                <td class="servicename" width="11%">
                                    <img src="images/services/Paediatric Anaesthesia.png" width="46" height="42" /></td>
                                <td class="servicename" width="27%"><a href="BJWHC-paediatric-anaesthesia.aspx">Paediatric Anaesthesia </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/services/Paediatric Burns and Plastic surgery.png" width="52" height="42" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-burns.aspx">Paediatric Burns and Plastic surgery</a></td>
                                <td class="servicename">
                                    <img src="images/services/Paediatric Dermatology.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-dermatology.aspx">Paediatric Dermatology </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/services/Paediatric Cardiology.png" width="46" height="41" /></td>
                                <td align="left" class="servicename"><a href="BJWHC-paediatric-cardiology.aspx">Paediatric Cardiology </a>
                                </td>
                                <td align="left" class="servicename">
                                    <img src="images/services/Paediatric Dentistry.png" width="46" height="42" /></td>
                                <td align="left" class="servicename"><a href="BJWHC-paediatric-dentistry.aspx">Paediatric Dentistry </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/services/Paediatric Endocrinology.png" width="38" height="38" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-endocrinology.aspx">Paediatric Endocrinology </a></td>
                                <td class="servicename">
                                    <img src="images/services/Paediatric ENT.png" width="46" height="43" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-ent.aspx">Paediatric ENT </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/services/Paediatric Haemato-Oncology and immunology.png" width="48" height="48" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-haematology.aspx">Paediatric Haemato-Oncology and immunology </a></td>
                                <td class="servicename">
                                    <img src="images/services/Paediatric- liver clinic.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-liverclinic.aspx">Paediatric-liver clinic </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/services/Paediatric and Perinatal HIV clinic.png" width="47" height="47" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-perinatal-hivclinic.aspx">Paediatric and Perinatal HIV clinic </a></td>
                                <td class="servicename">
                                    <img src="images/services/Paediatric Nephrology.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-nephrology.aspx">Paediatric Nephrology </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/services/Paediatric Neurology.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-neurology.aspx">Paediatric Neurology </a></td>
                                <td class="servicename">
                                    <img src="images/services/Paediatric Neurosurgery.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-neurosurgery.aspx">Paediatric Neurosurgery </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/services/Paediatric Ophthalmology.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-ophthalmology.aspx">Paediatric Ophthalmology </a></td>
                                <td class="servicename">
                                    <img src="images/services/Paediatric Orthopaedics.png" width="46" height="46" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-orthopaedics.aspx">Paediatric Orthopaedics </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/services/Paediatric Tuberculosis Clinic.png" width="46" height="46" /></td>

                                <td class="servicename"><a href="BJWHC-paediatric-tuberculosis.aspx">Paediatric Tuberculosis Clinic </a></td>
                                <td class="servicename">
                                    <img src="images/services/Paediatric Urology.png" width="38" height="39" /></td>
                                <td class="servicename"><a href="BJWHC-paediatric-urology.aspx">Paediatric Urology </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/services/Well Baby Clinic.png" width="46" height="52" /></td>
                                <td class="servicename"><a href="BJWHC-Well-baby-clinic.aspx">Well Baby Clinic </a></td>
                                <td class="servicename">
                                    <img src="images/services/Clubfoot Clinic.png" width="47" height="47" /></td>
                                <td class="servicename"><a href="BJWHC-clubfoot-clinic.aspx">Clubfoot Clinic </a></td>
                            </tr>
                            <tr>
                                <td valign="middle" height="66">
                                    <img src="images/services/Epilepsy Clinic.png" width="38" height="38" /></td>
                                <td class="servicename"><a href="BJWHC-Epilepsy-clinic.aspx">Epilepsy Clinic </a></td>
                                <td class="servicename">&nbsp;</td>
                                <td class="servicename">&nbsp;</td>
                            </tr>
                        </table>

                    </div>

                </div>


                <%-- SUPER SPECIALITY ENDS--%>

                <%--SUPPORTIVE START--%>

                <div class="devider_10px"></div>

                <div class="serviceheading">SUPPORTIVE SERVICES</div>

                <div style="padding-left: 30px">

                    <div class="devider_30px"></div>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="12%" height="55" valign="middle">
                                <img src="images/services/Audiology & Speech therapy.png" width="46" height="52" /></td>
                            <td class="servicename" width="50%"><a href="BJWHC-audiology-speech.aspx">Audiology & Speech therapy  </a></td>
                            <td class="servicename" width="11%">
                                <img src="images/services/Blood bank.png" width="52" height="42" /></td>
                            <td class="servicename" width="27%"><a href="BJWHC-blood-bank.aspx">Blood bank </a></td>
                        </tr>
                        <tr>
                            <td valign="middle" height="55">
                                <img src="images/services/Nutrition- Dietition.png" width="46" height="46" /></td>
                            <td class="servicename"><a href="BJWHC-Nutrition.aspx">Nutrition- Dietition </a></td>
                            <td class="servicename">
                                <img src="images/services/Physiotherapy  &  Occupational Therapy.png" width="46" height="46" /></td>
                            <td class="servicename"><a href="BJWHC-physiotherapy.aspx">Physiotherapy  &amp;  Occupational Therapy </a></td>
                        </tr>
                        <tr>
                            <td valign="middle" height="55">
                                <img src="images/services/Pathology.png" width="52" height="44" /></td>
                            <td class="servicename"><a href="BJWHC-pathology.aspx">Pathology</a></td>
                            <td class="servicename">
                                <img src="images/services/Radiology.png" width="47" height="47" /></td>
                            <td class="servicename"><a href="BJWHC-radiology.aspx">Radiology</a></td>
                        </tr>
                        <tr>
                            <td valign="middle" height="55">
                                <img src="images/services/Social Service Department.png" width="47" height="47" /></td>
                            <td class="servicename"><a href="BJWHC-physiotherapy.aspx"></a><a href="BJWHC-social-service-department.aspx">Social Service Department </a></td>
                            <td class="servicename">&nbsp;</td>
                            <td class="servicename">&nbsp;</td>
                        </tr>



                    </table>






                </div>

            </div>




            <%--    SUPPORTIVE END--%>
        </div>

    </div>


    <!-- ABOUT US CONTENT -->



</asp:Content>
