﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-paediatric-orthopaedics.aspx.cs" Inherits="WadiaResponsiveWebsite.child_paediatric_orthopaedics" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Paediatric Orthopaedics </h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
            <li><a href="#view3">Treatments</a></li>
           
           
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
The Paediatric Orthopaedics Department at BJWHC treats more than 400 patients a month. Orthopaedic surgeons treat disorders of bone, joint, spine and 
                limb development in children as well as emergencies such as Septic Arthritis and Fractures. Besides managing general orthopaedic problems 
                in children, the department also has experts in the field of Paediatric spine diseases and Paediatric bone cancers.
                Such super specialised services dedicated to children are scarce and are a boon to underprivileged patients. 
                <br<br />

 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            <div id="view3">
               

The well-equipped department is capable of successfully handling a wide range of complex and high-risk surgeries, such as: 

                                <br /><br />
                <div style="padding-left:10px">

-	PMR
<br />-	CDH –open reduction internal fixation
<br />-	Perthes’ VDRO
<br />-	Other ORIF SCFE-open reduction and osteotomy
<br />-	Tendon transfers
<br />-	Faciotomy and SEMLS (Single Event Multilevel Surgery)
<br />-	Ilizarov Hip reconstruction
<br />-	Ender’s Nailing Cheilectomy
<br />-	Acetabular procedures
<br />-	Cong Pseudarthrosis of Tibia Sprengel’s Scapuloplasty
<br />-	T A release
<br />-	JESS fixators
<br />-	Syndactyly
<br />-	Polydactyly
<br />-	Camptodactyly release
<br />-	Corrective osteotomies for genu varum valgum
<br />-	Arthrograms
<br />-	Tumour excision
<br />-	Torticollis release
<br />-	Centralisation for radial club had
<br />-	Quadriceps plasty 
<br />-	Bone grafting 


</div>


               
            </div>
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            
</div>
</asp:Content>
