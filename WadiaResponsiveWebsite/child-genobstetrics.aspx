﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="child-genobstetrics.aspx.cs" Inherits="WadiaResponsiveWebsite.child_genobstetrics" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Obstetrics</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->

<div style="line-height:20px">
 


<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Overview</a></li>
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
               
              •	OPD: The Obstetrics OPD (Out-Patient Department) provides comprehensive evaluation, diagnosis, management and counselling to women with complications during their pregnancies. Prenatal diagnosis and management is available for conditions such as Rh isoimmunisation and thalassemia.

                <br /><br />


•	IPD: The IPD (In-Patient Department) for Obstetrics caters to women who need to be admitted to the hospital overnight or for an undetermined period of time for treatment, observation or surgery during the course of their pregnancy. We have top of the line equipment for intensive monitoring of our patients and also have efficient processes in place for emergency deliveries or caesarean section surgeries. 

                <br /><br />
Services Provided : 
                                <br /><br />


-	Management of normal pregnancies
<br />-	Total preconception and pregnancy care
<br />-	Ultrasonography
<br />-	Antenatal care (before birth)
<br />-	Intrapartum care (during labour and delivery)
<br />-	Postnatal care (after birth)

 <br />
 <br />

                  
            </div>
            
             <!---- 2nd view end ---->
             
             
         
               
            
        </div>
    </div>


</div>

</div>










</div>
<!-- ABOUT US CONTENT -->

               
                    
				</div>
			</div>

            <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>SPECIALISTS</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
									<li><a href="women-about-doctdetails-drambedkar.aspx">Dr. M.J.Jassawalla</a></li>
									<li><a href="child-about-doctdetails-drshakuntla.aspx">Dr. Shakuntala Prabhu</a></li>
									<li><a href="child-about-doctdetails-drsudha.aspx">Dr. Sudha Rao</a></li>
									<li><a href="child-about-doctdetails-drrajesh.aspx">Dr. Rajesh Joshi</a></li>
									<li><a href="child-about-doctdetails-drshilpa.aspx">Dr. Shilpa Kulkarni </a>

                                        <li><a href="child-about-doctdetails-drira.aspx">Dr. Ira Shah </a>
                                        <li><a href="child-about-doctdetails-dralpana.aspx">Dr. Alpana Ohri </a>
                                        <li><a href="child-about-doctdetails-drsumitra.aspx">Dr. Sumitra Venkatesh </a>
                                        <li><a href="child-about-doctdetails-drlaxmi.aspx">Dr. Laxmi Shobhavat </a>
                                        <li><a href="child-about-doctdetails-drparmarth.aspx">Dr. Parmarth Chandane </a>
                                        <li><a href="child-about-doctdetails-drvaidehi.aspx">Dr. Vaidehi Dande </a>
                                        <li><a href="child-about-doctdetails-drrafat.aspx">Dr. Rafat Sayad </a>
                                       
									</li>
									
								</ul>
							</div>
						</div>
					</div>
				




				</div>
			</div>


		</div>
	







</asp:Content>
