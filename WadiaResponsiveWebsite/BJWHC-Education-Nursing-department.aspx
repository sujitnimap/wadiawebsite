﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-Education-Nursing-department.aspx.cs" Inherits="WadiaResponsiveWebsite.child_nursing_school" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                  <div class="heading">
                  <h2>NURSING Department</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->


<div style="line-height:20px">

    
<b>The BJWHC Nursing department </b>
    <br /><br />
At our Nursing department, our goal is to provide extensive clinical experiences for students from nursing institutes affiliated to us. Students work closely with our highly skilled medical and paramedical staff for a holistic understanding of healthcare for children, thus stimulating a desire to pursue advanced studies in the field of Paediatric and Neonatal Nursing.  

 <br /><br />
    Recognized by the Maharashtra Nursing Council, the department is also affiliated with different hospitals in Mumbai for Paediatric and 
    Neonatal training. We provide a comprehensive clinical speciality experience to students of M.Sc Nursing, B.Sc Nursing, Post Basic B.Sc Nursing and General Nursing Midwifery and from various institutes. 

<br /><br /><b>The Function of the Department </b>
<br /><br />
    <div style="padding-left:30px">
•	Completion of the clinical experience as per the requirements of the University / Council 
<br />•	Completion of Internal Assessment as per requirements 
<br />•	Conducting internal assessment as well as University / Council practical examinations 
<br />•	Organizing educational visits for various groups of students, to see our hospital’s in-house and out-reach services 
<br />•	Participating in health-education programmes and extra-curricular activities in our hospital
<br />•	Placement, orientation, rotation, supervision and guidance for M.Sc, B.Sc, PBSc, GNM and ANM students in clinical areas 
<br />•	Regular Continuous Nursing Education and in-service education for the trained nurses of our hospital 
   </div><br />



</div>

<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>Nursing</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                   
						<li><a href="BJWHC-Education-Medical-overview.aspx">	Overview  </a></li>
                         <li><a href="BJWHC-Education-Medical-directors-message.aspx"> Directors message </a></li>
                         <li><a href="BJWHC-Education-Nursing-Courses.aspx"> 	Courses available</a></li>
                         <li><a href="BJWHC-contact-us.aspx">	Contact Us  </a></li>


                                     <%--   <div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>

									
								</ul>
							</div>
						</div>
					</div>
					


				</div>
			</div>



		</div>


</asp:Content>
