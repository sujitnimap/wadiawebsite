﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="BJWHC-Education-Nursing-Courses.aspx.cs" Inherits="WadiaResponsiveWebsite.child_nursing_courses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div class="row block05">
			
			<div class="col-2-3">
				<div class="wrap-col">
					
                        <div class="heading">
                  <h2>COURSES AVAILABLE</h2></div>
                    
                    <br />

<!-- ABOUT US CONTENT -->


<div style="line-height:20px">

<%--    <h1>Cources Available</h1>
<div class="devider_20px"></div>--%>


<table width="70%" cellspacing="0" cellpadding="0" style="border: 1px solid #666">
  <tr>
    <td height="40" style="border: 1px solid #666; padding:5px"><b>SR. NO.</b></td>
    <td style="border: 1px solid #666; padding:5px"><b>COURSE</b> </td>
    <td style="border: 1px solid #666; padding:5px"><b>DURATION</b></td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">1</td>
    <td style="border: 1px solid #666; padding:5px">General Nursing Midwifery Course </td>
    <td style="border: 1px solid #666; padding:5px">1 – 2 months </td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">2</td>
    <td style="border: 1px solid #666; padding:5px">B.Sc Nursing </td>
    <td style="border: 1px solid #666; padding:5px">1 – 2 months </td>
  </tr>
  <tr>
    <td style="border: 1px solid #666; padding:5px">3</td>
    <td style="border: 1px solid #666; padding:5px">M.Sc Nursing </td>
    <td style="border: 1px solid #666; padding:5px">1 – 2 months </td>
  </tr>
</table>

     <br /><br />

   <b> *Out stations Institutes can also avail the residential/hostel facilities at BJWHC</b>


    <br /><br /><br /><br /><br /><br />

</div>

<!-- ABOUT US CONTENT -->


                
                    
                    
				</div>
			</div>
			


        <div class="col-1-3">
				<div class="wrap-col">
					
					<div class="box">
						<div class="heading"><h2>Nursing</h2></div>
						<div class="content">
							<div class="list">
								<ul>

                                    <li><a href="BJWHC-Education-Medical-overview.aspx">	Overview  </a></li>
                         <li><a href="BJWHC-Education-Medical-directors-message.aspx"> Directors message </a></li>
                         <li><a href="BJWHC-Education-Nursing-Courses.aspx"> 	Courses available</a></li>
                         <li><a href="BJWHC-contact-us.aspx">	Contact Us  </a></li>


                                     <%--<div class="leftpadsubmenu">
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-jerbai-wadia.aspx">Bai Jerbai Wadia (1852 – 1926)</a><br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-cursetjee-wadia.aspx">Sir Cursetjee Wadia (1869 – 1950)</a> <br />
                                           <img style="padding-top:5px" src="images/submenu.jpg" /> <a href="child-ness-wadia.aspx">Sir Ness Wadia (1873 – 1952)</a>

                                        </div>--%>
									
								</ul>
							</div>
						</div>
					</div>
					<%--<div class="box">
						<div class="heading"><h2>Archive</h2></div>
						<div class="content">
							<div class="list">
								<ul>
									<li><a href="#">April 2013</a></li>
									<li><a href="#">March 2013</a></li>
									<li><a href="#">February 2013</a></li>
									<li><a href="#">January 2013</a></li>
								</ul>
							</div>
						</div>
					</div>--%>
				</div>
			</div>



		</div>







</asp:Content>
