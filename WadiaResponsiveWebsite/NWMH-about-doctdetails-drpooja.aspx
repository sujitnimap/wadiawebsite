﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-about-doctdetails-drpooja.aspx.cs" Inherits="WadiaResponsiveWebsite.women_about_doctdetails_drpooja" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>PROFILE</h2></div>
                    
                  

<!-- ABOUT US CONTENT -->

<div class="doctorbox">

<img src="images/doc/Dr Pooja Bandekar.jpg" />
<div class="docdetails">
Dr. Pooja K. Bandekar


<div class="docdetailsdesg">
Associate Professor   <br />
M.D. (Obs. & Gyn.), D.N.B.,F.C.P.S., D.G.O.,D.F.P. (Gold Medal), M.R.C.O.G. (I), (LONDON).

</div>

</div>
</div>

     <%--<div class="areaofinterest">

Areas of Interest : Cardiovascular & Pulmonary pathology, Autopsy, Paediatric pathology


</div>--%>

<div class="scrollcontent">
<div style="width: 100%; margin: 0 auto; padding: 10px 0 40px;">

    
        <ul class="tabs" data-persist="true">
       <%--    <li><a href="#view1">Areas of Interest</a></li>--%>
            <li><a href="#view2">Research or publications</a></li>
          
                    </ul>

<div class="scrollbar" id="ex3">
        <div class="tabcontents">
         <!---- 1st view end -->
         <%--  <div id="view1">
                
                <p>
  Paediatric Infectious Diseases, Paediatric Hepatology
                                 <br />
              
                </p>
                
            </div>--%>
            
            
             <!---- 1st view end ---->
             
              <!---- 2nd view start ---->
             
            <div id="view2">
         
- An article on “MANAGEMENT OF ATONIC P.P.H.published in the “ INTERNATIONAL JOURNAL OF OBSTETRICS AND GYNAECOLOGY ”,India, ,Jan-Feb 2005,Vol. 8 No.1. 
<br />- An article on Review of sex ratio at birth published Journal Of Obstetrics & Gynaecology in Of India Vol. 64, No. 1, Jan-Feb 2014.
<div class="devider_20px"></div>
<b>SCIENTIFIC PAPERS </b>
<div class="devider_20px"></div>
1) Health awareness amongst nursing personnel.
<br />2) Difference in outcome between spontaneous and iatrogenic preterm deliveries.
<br />3) Improvement in the outcome of preterm deliveries with increasing weeks of gestation. 
<br />4) Gestational incontinence and its co relation with pregnancy
<br />                  
            </div>
            
             <!---- 2nd view end ---->
             
              <!---- 3rd view start ---->
              
              
            
       <!-- 3rd view end ---->
       
       <!-- 4th view start --->
       
       
            
            <!---- 4th view end ---->     
            
            
         
               
            
        </div>
    </div>


</div>

</div>


<!-- ABOUT US CONTENT RIGHT -->

           
                    
                    
				</div>
			</div>
			


	</div>
			





</asp:Content>
