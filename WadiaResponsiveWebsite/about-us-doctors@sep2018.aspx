﻿<%@ Page Title="" Language="C#" MasterPageFile="~/childmaster.Master" AutoEventWireup="true" CodeBehind="about-us-doctors.aspx.cs" Inherits="WadiaResponsiveWebsite.about_us_doctors" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    
<div class="row block05">		
<div class="row block05">
<div class="col-full">
				



<div style="padding:0px 20px 20px 20px; width:auto">
    <%--FIRST DEPT START --%>          
    <div class="colorcontent">  
In the words of the great Nelson Mandela, "There can be no keener revelation of a society's soul than the way in which it treats its children". Here at the Bai Jerbai Hospital for Children, we have a dedicated team of over 60 paediatric specialists, aided by highly trained nurses who understand that giving each child the love, comfort and care that they truly deserve is as important as our highly specialized treatments. 
           </div>
                    <br />



		<table width="77%" cellpadding="0" cellspacing="0"class="table-doctors" >

    <tr>
      <td width="44%" class="heading_table">SpecialIty</td>
      <td width="56%" class="heading_table">Name of the Doctors</td>
    </tr>

  
    <tr>
      <td class="doctor-content-bold"><strong>Pediatrician & Medical Director</strong></td>
      <td class="doctor-content">Dr. Shakuntala Prabhu</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="doctor-content-bold"><strong>Paediatric Medicine</strong></td>
      <td class="doctor-content">Dr. Sudha Rao<br>
Dr. Shakuntala Prabhu<br>
Dr. Rajesh Joshi<br>
Dr. Ira Shah<br>
Dr. Sumitra Venkatesh<br>
Dr. Laxmi Shobhavat<br>
Dr. Vaidehi Dande<br>
Dr. Shilpa Kulkarni<br>
Dr. Alpana Ohri<br>
Dr.Shivkumar Lalvani<br>
Dr. Parmarth Chandane<br>
Dr. Nikhil Pawar
      </td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="doctor-content-bold"><strong>Paediatric Surgery</strong></td>
      <td class="doctor-content">Dr. Pradnya Bendre<br>
      Dr. Sushmita Bhatnagar<br>
Dr. S.S. Bhagwat<br>
Dr. Vivek Rege<br>
Dr. Amrish Vaidya<br>
Dr. Suyodhan Reddy<br>
Dr. Parag Karkera<br>
Dr. Sushmita Bhatnagar<br>
Dr. Mukunda Ramchandra<br>
Dr. Waingankar 

</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="doctor-content-bold"><strong>Anesthesia</strong></td>
      <td class="doctor-content">Dr. Pradnya Sawant<br>
Dr. Mukta Shikhre<br>
Dr. Varinder Kaur<br>
Dr. Minal Shah<br>
Dr. Vandana Rajesh<br>
Dr. Aruna bapat</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="doctor-content-bold"><strong>Plastic Surgery</strong></td>
      <td class="doctor-content">Dr. Shankar</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="doctor-content-bold"><strong>Dermatology</strong></td>
      <td class="doctor-content">Dr. Deepak Parikh<br>
			Dr. Manish Shah</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      
      <td class="doctor-content-bold"><strong>ENT</strong></td>
      <td class="doctor-content">Dr. Divya Prabhat<br>
		Dr. Carlton Pereira</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      <td class="doctor-content-bold"><strong>Hematology</strong></td>
      <td class="doctor-content">Dr. Bharat Agarwal<br>
Dr. Archana Swami<br>
Dr. Nitin Shah<br>
Dr. Sangita Mudaliar</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      
      <td class="doctor-content-bold"><strong>Immunology</strong></td>
      <td class="doctor-content">Dr. Mukesh Desai</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      <td class="doctor-content-bold"><strong>HIV</strong></td>
      <td class="doctor-content">Dr. Mamata Lalla</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="doctor-content-bold"><strong>Endocrinology</strong></td>
      <td class="doctor-content">Dr. Aparna Limaye</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      <td class="doctor-content-bold"><strong>Microbiology</strong></td>
      <td class="doctor-content">Dr. Sunita Deshpande</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      
      <td class="doctor-content-bold"><strong>Radiologist</strong></td>
      <td class="doctor-content">Dr. Malini Nadkarni</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      <td class="doctor-content-bold"><strong>Nephrology</strong></td>
      <td class="doctor-content">Dr. Atul Deokar<br>
Dr. Shashank parekhji<br>
Dr. Amish Udani</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="doctor-content-bold"><strong>Neurology</strong></td>
      <td class="doctor-content">Dr. Anaita Hegde</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      <td class="doctor-content-bold"><strong>Genetics</strong></td>
      <td class="doctor-content">Dr. Aparna Parikh</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      
      <td class="doctor-content-bold"><strong>Neurosurgery</strong></td>
      <td class="doctor-content">Dr. C.E. Deopujari<br>
Dr. Naresh Biyani<br>
Dr. V. P. Udhani<br>
Dr. Udhay Andhar</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      <td class="doctor-content-bold"><strong>NICU</strong></td>
      <td class="doctor-content">Dr. Alpana Utture</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      
      <td class="doctor-content-bold"><strong>Opthalmology</strong></td>
      <td class="doctor-content">Dr. AshwinKumar Sainani</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      <td class="doctor-content-bold"><strong>Surgery</strong></td>

      <td class="doctor-content">Dr. Sunil Kelkar</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
    <tr>
      
      <td class="doctor-content-bold"><strong>Orthopedics</strong></td>
      <td class="doctor-content">Dr. Rujuta Mehta<br>
      <!--Dr. Chasanal Rathod<br>-->
Dr. Abhay Nene<br>
Dr. A. Aroojis<br>
Dr. D.D. D'silva</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      
      <td class="doctor-content-bold"><strong>Burns</strong></td>
      <td class="doctor-content">Dr. A.M. Vartak</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      
      <td class="doctor-content-bold"><strong>Pathology</strong></td>
      <td class="doctor-content">Dr. Jaya Deshpande</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      
      <td class="doctor-content-bold"><strong>PICU</strong></td>
      <td class="doctor-content">Dr. Parmanand A</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      
      <td class="doctor-content-bold"><strong>PICU & Nephrology</strong></td>
      <td class="doctor-content">Dr. Uma Ali</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      
      <td class="doctor-content-bold"><strong>Plastic Surgery</strong></td>
      <td class="doctor-content">Dr. Nitin Mokal<br>
Dr. M.R. Thatte</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      <td class="doctor-content-bold"><strong>Psycology</strong></td>
      <td class="doctor-content">Dr. Jalpa Bhuta</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      <td class="doctor-content-bold"><strong>Sonography</strong></td>
      <td class="doctor-content">Dr. Anirudh Badae<br>
Dr. Saroj Chavan</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      <td class="doctor-content-bold"><strong>Cardiology</strong></td>
      <td class="doctor-content">

Dr. Biswa Panda - Cardiac Surgeon<br>
Dr. Jayshree Mishra - Paediatric Cardiologist <br>
Dr. Vislon - Paediatric Cardiac Anaesthetist and Intensivist <br>
Dr. Sanjay Prabhu - Senior Consultant Pediatrician <br>








</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
      <tr>
      <td class="doctor-content-bold"><strong>Psychology</strong></td>
      <td class="doctor-content">Dr. Aparana Raharkar</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
    </tr>
        <tr>
      <td class="doctor-content-bold"><strong>Physiotherapy</strong></td>
      <td class="doctor-content">Dr. Urmila Kamat</td>
    </tr>

</table>



</div>	




</div>
</div>
</div>
</asp:Content>
