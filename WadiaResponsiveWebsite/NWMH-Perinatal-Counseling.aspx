﻿<%@ Page Title="" Language="C#" MasterPageFile="~/womenmaster.Master" AutoEventWireup="true" CodeBehind="NWMH-Perinatal-Counseling.aspx.cs" Inherits="WadiaResponsiveWebsite.women_perinatalcouns" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    
    <div class="row block05">
			
			<div class="col-full">
				<div class="wrap-col">
					
                    <div class="heading">
                  <h2>Perinatal Counselling for High Risk Foetuses and Neonates </h2></div>
                    
                    <br />

<!-- CONTENT -->

<div style="line-height:20px">


    <strong>Overview </strong>
    <br />    <br />



Our Perinatal Counselling team consists of experienced psychologists that specialize in counselling women during the period around childbirth. This counselling helps women deal with anxiety and depression during their pregnancy as well as after delivery (the most crucial time being 5 months before and one month after delivery). This stress could be caused by various factors including fear of pregnancy, loss of pregnancy, stillbirth or neo-natal loss, postpartum depression etc.
<br /><br />

   <b> Services Provided:</b>
 <br /><br />    
-	Specialised inpatient mother and baby units
<br />-	Specialised community perinatal mental health teams
<br />-	Parent and infant mental health services
<br />-	Clinical psychology services linked to the hospitals






</div>

    <div style="height:90px"></div>



</div>
<!--  CONTENT -->


                
                    
                    
				</div>
			</div>
			







		</div>
</asp:Content>
