﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WadiaWebsiteDAL;
using WadiaWebsiteDAL.Implementation;

namespace WadiaResponsiveWebsite
{
    public partial class women_careers : System.Web.UI.Page
    {
        IList<job> jobdata;

        static string flag = "w";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                getjobdata();
            }

        }


        public void getjobdata()
        {

            jobdata = CJob.Instance().GetJobData(flag);

            DataTable dt = CollectionHelper.GetDataTable(jobdata);

            if (dt.Rows.Count > 0)
            {
                dlistjobvacancy.DataSource = dt;
                dlistjobvacancy.DataBind();
            }
            else
            {
                //dlistjobvacancy.DataSource = null;
                //dlistjobvacancy.DataBind();

                pnlfornovacacy.Visible = true;
                pnljobvacacy.Visible = false;
            }
        }

        protected void lnkbtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("women-applicationform.aspx");
        }

        protected void lnkbtnopencv_Click(object sender, EventArgs e)
        {
            Response.Redirect("women-applicationform.aspx");
        }
    }
}